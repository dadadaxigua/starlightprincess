
import { _decorator, Component, Node, assetManager, director, SpriteAtlas, JsonAsset, EditBox, Button, sys, Label, Prefab, instantiate, macro, dynamicAtlasManager, game } from 'cc';
import req from './network/req';
import userData from './network/userData';
import { assetMgr } from './lobby/game/core/sAssetMgr';
import { sConfigMgr } from './lobby/game/core/sConfigMgr';
import { sUtil } from './lobby/game/core/sUtil';
const { ccclass, property } = _decorator;

macro.CLEANUP_IMAGE_CACHE = false;
dynamicAtlasManager.enabled = true;
dynamicAtlasManager.maxFrameSize = 4096;
dynamicAtlasManager.textureSize = 4096;
game.frameRate = 60;

@ccclass('entryAdapter')
export class entryAdapter extends Component {
    @property(JsonAsset)
    clientConfig : JsonAsset

    @property(EditBox)
    mEditbox : EditBox;

    @property(Button)
    loginBtn : Button;

    @property(Prefab)
    btnPrefab : Prefab;

    @property(Prefab)
    recordPrefab : Prefab;

    @property([Prefab])
    lobbyPrefab = [];

    @property([SpriteAtlas])
    languageSpriteAtlas = [];

    touchEnable = true;

    start () {

        globalThis.betTypeConfig = this.clientConfig.json;

        globalThis.subGameBtnPrefab = this.btnPrefab;

        globalThis.recordPrefabNode = this.recordPrefab;

        globalThis.lobbyPrefabs = this.lobbyPrefab;
       
        director.on('authSuc',()=>{
            assetManager.loadBundle('149',(err,bundle)=>{
                if(bundle){
                    bundle.loadScene('main_hor',(err,scene)=>{
                        if(scene){
                            director.runScene(scene);
                        }
                    });
                }
            });
        },this);

        req.init();
        globalThis.testNetWork();
        
        director.on('socketconnected',(e)=>{
            console.log('socketconnected');
            req.auth();
        },this);

        let deviceID = sys.localStorage.getItem('ddddeviceid');
        if(deviceID){
            this.mEditbox.string = deviceID;
        }


        let language = 'EN';
        if(globalThis.GetLanguageType){
            language = globalThis.GetLanguageType();
        }

        if(this.languageSpriteAtlas){
            for (let i = 0; i < this.languageSpriteAtlas.length; i++) {
                const at = this.languageSpriteAtlas[i];
                if(at.name == language){
                    assetMgr.SetSpriteFramesToDic(at);
                    break;
                }
            }
        }

        this.loginBtn.node.on('click',()=>{
            if(this.mEditbox.string && this.touchEnable){
                this.touchEnable = false;
                req.getAppConfig('2966',(res,err)=>{
                    userData.SetAppConfigData(res);
                    let autoLoginToken = userData.GetAutoLoginToken();
                    let hostStr = userData.GetSocketHost();
                    let host = JSON.parse(hostStr);
                    // if(autoLoginToken && host){
                    //     globalThis.userToken = autoLoginToken;
                    //     req.gameServerCon(autoLoginToken,host,'ddddd123');
                    // }else{
                        req.DeviceLogin(this.mEditbox.string,globalThis.appID,'','','',(res,err)=>{
                            if(!err){
                                sys.localStorage.setItem('ddddeviceid',this.mEditbox.string);
                                globalThis.userToken = res.data.loginToken;
                                userData.SaveAutoLoginToken(res.data.loginToken);
                                userData.SaveSocketHost(JSON.stringify(res.data.hosts));
                                req.gameServerCon(res.data.loginToken,res.data.hosts,this.mEditbox.string);
                            }
                            this.touchEnable = true;
                        });
                    // }
                });
            }
        },this);

        this.loginBtn.node.emit('click');
    }
}

globalThis.getSubGameBtn = function (gameid,aligmentType,aligmentPixel,gameVersion,customColor,cb) {
    if (cb) {
        let obj = instantiate(globalThis.subGameBtnPrefab);
        if(obj){
            obj['gameid'] = gameid;
            obj['aligmentType'] = aligmentType;
            obj['aligmentPixel'] = aligmentPixel;
            obj['gameVersion'] = gameVersion;
            obj['customColor'] = customColor;
            if (cb) {
                cb(obj);
            }
        }
    }
}

globalThis.getSubGameBtnByName = function (gameid, belongName, aligmentType, aligmentPixel, gameVersion, customColor, cb) {
    if (cb) {
        let obj = instantiate(globalThis.subGameBtnPrefab);
        if(obj){
            obj['gameid'] = gameid;
            obj['aligmentType'] = aligmentType;
            obj['aligmentPixel'] = aligmentPixel;
            obj['gameVersion'] = gameVersion;
            obj['customColor'] = customColor;
            if (cb) {
                cb(obj);
            }
        }
    }
}

globalThis.getSubGamePrefabByPath = function (path : string, cb) {
    if(path && cb){
        for (let i = 0; i < globalThis.lobbyPrefabs.length; i++) {
            const p : Prefab = globalThis.lobbyPrefabs[i];
            const fileName = path.substring(path.lastIndexOf('/') + 1,path.length);
            if(fileName){
                if(fileName == p.data.name){
                    let obj = instantiate(p);
                    if(obj){
                        cb(obj);
                    }
                    break;
                }
            }
        }
    }
}

globalThis.getClientConfig = function(cb){
    if(cb){
        cb(globalThis.betTypeConfig);
    }
}
let index =  1;
globalThis.getSubGameOneBetTestData = function(){
    let boxData = sConfigMgr.instance.getBoxData();
    // return boxData[1]['round_type14'][40][1];
    //return boxData[1]['round_type0'][4830][0];
    //return boxData[1]['round_type0'][46][3];
   // return boxData[1]['round_type0'][0][76];
  //return boxData[1]['round_type1'][1903][0];
   //return boxData[1]['round_type0'][4830][0];
    // index++;
    // if(index % 2 == 0){
      //return boxData[1]['round_type0'][400][0];
    // }else{
    //     return boxData[1]['round_type0'][0][index];
    // }
};

// function randomNum(minNum, maxNum) {
//     switch (arguments.length) {
//         case 1:
//             return parseInt((Math.random() * minNum + 1).toString(), 10);
//             break;/
//         case 2:
//             return parseInt(Math.random() * (maxNum - minNum + 1) + minNum, 10);
//             //或者 Math.floor(Math.random()*( maxNum - minNum + 1 ) + minNum );
//             break;
//         default:
//             return 0;
//             break;
//     }
// }


globalThis.toast = function(str){
    console.log(str);
}

globalThis.GetLanguageType = function(){
    return 'PT';
}

globalThis.GetDataByKey = function (id, key, key1, key2) {
    return '';
}

globalThis.getSoltGameRecord = function(cb) {
    cb(instantiate(globalThis.recordPrefabNode));
}

globalThis.getServerTime = function(){
    return Date.now();
}

globalThis.popString = function (str, scale = 1) {
    console.log('popString:'+str);
}

globalThis.getCurJackPotId = function(){
    return 14601;
}
globalThis.getCoinRate = function(){
    return 100;
}
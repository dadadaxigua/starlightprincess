
import { _decorator, director, Game, game, Label, Node } from 'cc';
import { sUtil } from '../../../../game/core/sUtil';
const { ccclass, property } = _decorator;

interface sBonusEntity {
    targetLabels: Label[];
    jackpot_id: number;
}

interface sBonusData {
    jackpot_id: number;
    game_id: number;
    last_jackpot: number;
    jackpot_speed: number;
    last_jackpot_time: number;
    serverTime: number;
    enable: boolean
}

@ccclass('sBonusCtr')
export class sBonusCtr {
    private gameid: number = 0;
    private demc: number = 2;

    private bonusList: sBonusEntity[] = [];
    private bonusMap = new Map<number, sBonusEntity>();

    public static bonusDataMap = new Map<number, sBonusData>();

    public static loaded = false;
    private static bonusCtr: sBonusCtr;

    private mInterval : number = null;


    public start(sec : number) {
        this.update(0);
        this.mInterval = setInterval(()=>{
            this.update(0);
        },sec * 1000);
    }

    public clear(){
        clearInterval(this.mInterval);
    }

    public static Init(finishCall = null,sec = 0.1){
        // console.log('sBonusCtr Init');
        const bonusCtrinit = () => {
            // if (!sBonusCtr.bonusCtr || !sBonusCtr.bonusCtr.isValid || !sBonusCtr.bonusCtr.node || !sBonusCtr.bonusCtr.node.isValid) {
            //     globalThis.getSubGamePrefabByPath('bonus/bonusCtr', (obj: Node) => {
            //         if (obj) {
            //             const scene = director.getScene();
            //             if (scene && scene.children && scene.children.length > 0) {
            //                 scene.children[0].addChild(obj);
            //                 const com = obj.getComponent(sBonusCtr);
            //                 sBonusCtr.bonusCtr = com;
            //                 if (finishCall) {
            //                     finishCall();
            //                 }
            //             }
            //         }
            //     });
            // }
            if(sBonusCtr.bonusCtr){
                sBonusCtr.bonusCtr.clear();
            }
            sBonusCtr.bonusCtr = new sBonusCtr();
            sBonusCtr.bonusCtr.start(sec);
            if (finishCall) {
                finishCall();
            }                  
        };
        if (!sBonusCtr.loaded) {
            // if(globalThis.getCoinRate && globalThis.getCoinRate() > 1){
            // }
            globalThis.subGameSocketReq('user.jackpotHandler.jackpot', {
                
            }, (err, res) => {
                if (!err && res && res.data) {
                    sBonusCtr.loaded = true;
                    if (res.data && Array.isArray(res.data)) {
                        for (let i = 0; i < res.data.length; i++) {
                            const data = res.data[i];
                            sBonusCtr.AddJackpotData(data.jackpot_id, data.game_id, data.last_jackpot, data.jackpot_speed, data.last_jackpot_time);
                        }
                    }
                    bonusCtrinit();
                } else {

                }
            });
        } else {
            bonusCtrinit();
        }
    }

    public static ViewUpdate(){
        if(sBonusCtr.bonusCtr){
            sBonusCtr.bonusCtr.update(0);
        }
    }

    public static AddJackpotData(_jackpot_id: number, _game_id: number, _last_jackpot: number, _jackpot_speed: number, _last_jackpot_time: number) {
        const jackpot: sBonusData = {
            jackpot_id: _jackpot_id,
            game_id: _game_id,
            last_jackpot: _last_jackpot,
            jackpot_speed: _jackpot_speed,
            last_jackpot_time: _last_jackpot_time,
            serverTime: globalThis.getServerTime(),
            enable: true
        };
        // console.log('sJackpotEntityViewCtr,true : ',_jackpot_id);
        director.emit('sJackpotEntityViewCtr', _jackpot_id, true);
        sBonusCtr.bonusDataMap.set(_jackpot_id, jackpot);
    }

    public static AddOneJackpotEntity(jackpotId: number, labels: Label[]) {
        // console.log('sBonusCtr Bind');
        if (sBonusCtr.bonusCtr && sBonusCtr.bonusCtr.bonusMap) {
            const bonusEntity = sBonusCtr.bonusCtr.bonusMap.get(jackpotId);
            if (bonusEntity) {
                bonusEntity.targetLabels = labels;
            } else {
                const bonus: sBonusEntity = {
                    targetLabels: labels,
                    jackpot_id: jackpotId
                };
                sBonusCtr.bonusCtr.bonusList.push(bonus);
                sBonusCtr.bonusCtr.bonusMap.set(jackpotId, bonus);
            }
        }
    }

    public static JackPotBonusEnable(jackpotId: number) {
        if (sBonusCtr.bonusDataMap) {
            const jackpot = sBonusCtr.bonusDataMap.get(jackpotId);
            if (jackpot) {
                return jackpot.enable;
            }
        }
        return false;
    }

    public update(dt: number): void {
        if (this.bonusList) {
            const localTime = globalThis.getServerTime();
            for (let i = 0; i < this.bonusList.length; i++) {
                const bonus = this.bonusList[i];
                if (bonus) {
                    const bonusData = sBonusCtr.bonusDataMap.get(bonus.jackpot_id);
                    if (bonusData) {
                        if (bonusData.enable) {
                            // bonusData.serverTime += dt;
                            if (bonus.targetLabels) {
                                for (let l = 0; l < bonus.targetLabels.length; l++) {
                                    const targetLabel = bonus.targetLabels[l];
                                    if (targetLabel && targetLabel.node && targetLabel.node.activeInHierarchy && targetLabel.node.isValid) {
                                        targetLabel.string = sUtil.AddCommas((localTime - bonusData.last_jackpot_time) * bonusData.jackpot_speed / 1000 + bonusData.last_jackpot, this.demc);
                                        // console.log('bonusData.serverTime:',bonusData.serverTime);
                                        // console.log('targetLabel.string:',(bonusData.serverTime - bonusData.last_jackpot_time) + ', '+targetLabel.string);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

director.on('socket_backToFront_game', () => {
    const serverTime = globalThis.getServerTime();
    if (sBonusCtr.bonusDataMap) {
        sBonusCtr.bonusDataMap.forEach((bonus, key) => {
            bonus.serverTime = serverTime;
        });
    }
});

director.on('socketConnectToGame', (type) => {
    if (type == 1) {
        globalThis.subGameSocketReq('user.jackpotHandler.jackpot', {

        }, (err, res) => {
            if (!err && res && res.data) {
                sBonusCtr.loaded = true;
                if (sBonusCtr.bonusDataMap) {
                    sBonusCtr.bonusDataMap.forEach((value, key) => {
                        value.enable = false;
                    });
                }
                if (res.data && Array.isArray(res.data)) {
                    for (let i = 0; i < res.data.length; i++) {
                        const data = res.data[i];
                        sBonusCtr.AddJackpotData(data.jackpot_id, data.game_id, data.last_jackpot, data.jackpot_speed, data.last_jackpot_time);
                    }
                }
                const bonusDataList: number[] = [];
                sBonusCtr.bonusDataMap.forEach((value, key) => {
                    if (!value.enable) {
                        bonusDataList.push(key);
                        // console.log('sJackpotEntityViewCtr,false : ',key);
                        director.emit('sJackpotEntityViewCtr', key, false);
                    }
                });
                if (bonusDataList.length > 0) {
                    for (let i = 0; i < bonusDataList.length; i++) {
                        const bonusKey = bonusDataList[i];
                        sBonusCtr.bonusDataMap.delete(bonusKey);
                    }
                }
            } else {

            }
        });
    } else if (type == 2) {
        
    }
}, this);

director.on('onBroadcastNotice', (data) => {
    if (data) {
        if (data.type == 'jackpot') {
            if (data.msg && data.msg.content) {
                const msg = JSON.parse(data.msg.content);
                if (msg && Array.isArray(msg)) {
                    if (sBonusCtr.bonusDataMap) {
                        sBonusCtr.bonusDataMap.forEach((value, key) => {
                            value.enable = false;
                        });
                    }
                    for (let i = 0; i < msg.length; i++) {
                        const item = msg[i];
                        if (item) {
                            const _bonus = sBonusCtr.bonusDataMap.get(item.jackpot_id);
                            if (_bonus) {
                                _bonus.enable = true;
                                _bonus.serverTime = globalThis.getServerTime();
                                _bonus.last_jackpot_time = item.last_jackpot_time;
                                _bonus.last_jackpot = item.last_jackpot;
                                _bonus.jackpot_speed = item.jackpot_speed;
                            } else {
                                sBonusCtr.AddJackpotData(item.jackpot_id, item.game_id, item.last_jackpot, item.jackpot_speed, item.last_jackpot_time);
                            }
                        }
                    }
                }
                const bonusDataList: number[] = [];
                sBonusCtr.bonusDataMap.forEach((value, key) => {
                    if (!value.enable) {
                        bonusDataList.push(key);
                        // console.log('sJackpotEntityViewCtr,false : ',key);
                        director.emit('sJackpotEntityViewCtr', key, false);
                    }
                });
                if (bonusDataList.length > 0) {
                    for (let i = 0; i < bonusDataList.length; i++) {
                        const bonusKey = bonusDataList[i];
                        sBonusCtr.bonusDataMap.delete(bonusKey);
                    }
                }
            }
        }
    }
});

globalThis.jackPotBonusLabelInit = function (finishCall = null,sec = 0.1) {
    sBonusCtr.Init(finishCall,sec);
}

globalThis.jackPotBonusLabelInitBind = function (jackpotId: number, labels: Label[]) {
    sBonusCtr.AddOneJackpotEntity(jackpotId, labels);
}

globalThis.jackPotBonusLabelUpdate = function () {
    sBonusCtr.ViewUpdate();
}

globalThis.jackPotBonusEnable = function (jackpotId: number) {
    return sBonusCtr.JackPotBonusEnable(jackpotId);
}



import { _decorator, Component, Node, Button, Animation, UIOpacity, v3, director, tween } from 'cc';
import { sGameEntity } from './sGameEntity';
import sAudioMgr from '../../../game/core/sAudioMgr';
import { UIFreeWinBetInfo } from './UIFreeWinBetInfo';
const { ccclass, property } = _decorator;
 
@ccclass('sSlotPGGameEntityAdapter')
export class sSlotPGGameEntityAdapter extends sGameEntity {

    protected start() {
        super.start();
        let country = '';
        if(globalThis.getCountry){
            const con = globalThis.getCountry();
            if(con){
                country = con;
            }
        }
        if(country == 'VNM'){
            if(this.selectBetBtn){
                const vnIcon = this.selectBetBtn.node.getChildByName('vnIcon');
                if(vnIcon){
                    vnIcon.active = true;
                }
            }
        }else{
            if(this.selectBetBtn){
                const icon = this.selectBetBtn.node.getChildByName('icon');
                if(icon){
                    icon.active = true;
                }
            }
        }
        // const audioData = globalThis.getSubGameAudioVolume();
        // if (audioData) {
        //     if (audioData.audioVolume == 1) {
        //         this.betBtns.node.getChildByPath('menuBtn/sound_off').active = true;
        //     } else {
        //         this.betBtns.node.getChildByPath('menuBtn/sound_off').active = false;
        //     }
        // }
        this.loadFreeBuyView();
    }

    protected turboBtnNodeClick(value : boolean){
        if(this.turboBtnNode){
            if(value){
                const turboOff = this.turboBtnNode.node.getChildByName('turbo_off');
                turboOff.active = false;
                const turboOn = this.turboBtnNode.node.getChildByName('turbo_on');
                turboOn.active = true;
                turboOn.getChildByName('turbo').active = true;
                turboOn.getChildByName('turbo').getComponent(Animation).play();
            }else{
                const turboOn = this.turboBtnNode.node.getChildByName('turbo_on');
                turboOn.getChildByName('turbo').getComponent(Animation).stop();
                turboOn.getChildByName('turbo').active = false;
                turboOn.active = false;
                const turboOff = this.turboBtnNode.node.getChildByName('turbo_off');
                turboOff.active = true;
            }
        }
    }

    protected addBtnNodeStateUpdate(visible : boolean){
        if(this.addBtnNode){
            this.changeRenderAlpha(this.addBtnNode.node,visible ? 255 : 125);
            // this.addBtnNode.getComponent(UIOpacity).opacity = visible ? 255 : 125;
        }
    }

    protected minusBtnNodeStateUpdate(visible : boolean){
        if(this.minusBtnNode){
            this.changeRenderAlpha(this.minusBtnNode.node,visible ? 255 : 125);
            // this.minusBtnNode.getComponent(UIOpacity).opacity = visible ? 255 : 125;
        }
    }

    protected autoPlayBtnNodeStateUpdate(visible : boolean){
        if(this.autoPlayBtnNode){
            this.autoPlayBtnNode.getComponent(UIOpacity).opacity = visible ? 255 : 125;
        }
    }

    protected autoPlayBtnClick(){
        if(this.betBtnState == 'normal' && this.autoBetCount == -2){
            console.log('this.autoBetCount:',this.autoBetCount);
            this.setGameBtnState('disable');
            this.autoSpin.node.active = true;
            this.autoSpin.appearViewAnima();
            let mUserInfo = globalThis.getUserInfo();
            if (mUserInfo) {
                this.autoSpin.betInfoSet(mUserInfo.coin, this.betAmount * this.lineAmount, 0)
            }
        }
    }

    protected menuBtnClick(){
        const animaTime = 0.15;
        if (this.touchEnable) {
            this.touchEnable = false;
            if (this.betBtns.node.active) {
                this.cleanTweenList();
                this.betBtnNode.node.active = false;
                director.emit('subGameMenuView', true);
                this.betBtns.node.position = v3(0, 70, 0);
                this.betBtns.opacity = 255;
                this.pushOneTween(tween(this.betBtns).to(animaTime, { opacity: 0 }, { easing: 'sineOut' }).start());
                this.pushOneTween(tween(this.betBtns.node).to(animaTime, { position: v3(0, -330, 0) }, { easing: 'sineOut' }).call(() => {
                    this.betBtns.node.active = false;
                    this.touchEnable = true;
                }).start());
                this.menuBtns.node.active = true;
                this.menuBtns.node.position = v3(0, -330, 0);
                this.menuBtns.opacity = 0;
                this.menuState = 'menu';
                this.pushOneTween(tween(this.menuBtns).to(animaTime, { opacity: 255 }, { easing: 'sineOut' }).start());
                this.pushOneTween(tween(this.menuBtns.node).to(animaTime, { position: v3(0, 70, 0) }, { easing: 'sineOut' }).start());
            }
            const audioData = globalThis.getSubGameAudioVolume();
            if (audioData) {
                if (audioData.audioVolume == 1) {
                    this.menuBtns.node.getChildByPath('btns/sound/open').active = true;
                    this.menuBtns.node.getChildByPath('btns/sound/close').active = false;
                } else {
                    this.menuBtns.node.getChildByPath('btns/sound/close').active = true;
                    this.menuBtns.node.getChildByPath('btns/sound/open').active = false;
                }
            }
        }
    }

    protected closeBtnClick(){
        const animaTime = 0.15;
        if (this.touchEnable) {
            this.touchEnable = false;
            if (!this.betBtns.node.active) {
                this.cleanTweenList();
                this.betBtnNode.node.active = true;
                director.emit('subGameMenuView', false);
                // director.emit('subGameReplayingView',false);
                this.menuBtns.node.position = v3(0, 70, 0);
                this.menuBtns.opacity = 255;
                this.pushOneTween(tween(this.menuBtns).to(animaTime, { opacity: 0 }, { easing: 'sineOut' }).start());
                this.pushOneTween(tween(this.menuBtns.node).to(animaTime, { position: v3(0, -330, 0) }, { easing: 'sineOut' }).call(() => {
                    this.menuBtns.node.active = false;
                    this.touchEnable = true;
                }).start());
                this.betBtns.node.active = true;
                this.betBtns.node.position = v3(0, -330, 0);
                this.betBtns.opacity = 0;
                this.menuState = 'bet';
                this.pushOneTween(tween(this.betBtns).to(animaTime, { opacity: 255 }, { easing: 'sineOut' }).start());
                this.pushOneTween(tween(this.betBtns.node).to(animaTime, { position: v3(0, 70, 0) }, { easing: 'sineOut' }).start());
            }
        }
    }

    protected soundBtnClick(){
        if(this.audioTouchEnable){
            this.audioTouchEnable = false;
            const audioData = globalThis.getSubGameAudioVolume();
            if (audioData) {
                if (audioData.audioVolume == 1) {
                    globalThis.subGameAudioVolumeCtr(false);
                    this.menuBtns.node.getChildByPath('btns/sound/close').active = true;
                    this.menuBtns.node.getChildByPath('btns/sound/open').active = false;
                    // this.betBtns.node.getChildByPath('menuBtn/sound_off').active = true;
                } else {
                    globalThis.subGameAudioVolumeCtr(true);
                    this.menuBtns.node.getChildByPath('btns/sound/open').active = true;
                    this.menuBtns.node.getChildByPath('btns/sound/close').active = false;
                    // this.betBtns.node.getChildByPath('menuBtn/sound_off').active = true;
                }
            }
            this.scheduleOnce(()=>{
                this.audioTouchEnable = true;
            },1);
        }
    }

    protected soundTip(value : boolean){
        // this.betBtns.node.getChildByPath('menuBtn/sound_off').active = value;
    }

    protected addBtnClick(){
        globalThis.subGamePlayShotAudio('subGameClick');
        super.addBtnClick();
    }
    
    protected minusBtnClick(){
        globalThis.subGamePlayShotAudio('subGameClick');
        super.minusBtnClick();
    }

    protected spinBtnClick(){
        
    }

    protected freeWinBingo(){
        this.node.active = false;
        this.gameMode = 'freeWin';
        if(this.viewMode == 'record'){
            if(this.recordBtns){
                this.recordBtns.node.active = false;
            }
        }
    }

    protected freeWinOver(){
        if(this.viewMode == 'record'){
            this.recordBtnTouchEnable = false;
            this.scheduleOnce(()=>{
                this.recordBtnTouchEnable = true;
            },3);
        }
        this.node.active = true;
        this.recordState = 'idle';
        this.freeWinBtn.active = false;
        this.gameMode = 'normal';
    }

    protected freeWinBegain(){
        this.freeWinBtn.active = true;
        if(this.viewMode == 'record'){
            if(this.recordBtns){
                this.recordBtns.node.active = true;
            }
        }
    }

    protected freeWinBtnInfo(rate){
        let _rate = 0;
        if(rate){
            _rate = rate;
        }
        if(this.freeWinBtn){
            this.freeWinBtn.getComponent(UIFreeWinBetInfo).updateRate(_rate);
        }
    }
}
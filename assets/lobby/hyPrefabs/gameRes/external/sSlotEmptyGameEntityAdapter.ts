
import { _decorator, Component, director, Node } from 'cc';
import { sGameEntity } from './sGameEntity';
const { ccclass, property } = _decorator;

@ccclass('sSlotEmptyGameEntityAdapter')
export class sSlotEmptyGameEntityAdapter extends sGameEntity {

    protected onLoad() {
        super.onLoad();
        
        director.on('slotEmptySpinClick',()=>{
            if(!this.playerBetLimit()){
                globalThis.webFullScreen && globalThis.webFullScreen();
                this.betActionBtnClick(this.betType);
            }
        },this);

        director.on('slotEmptyTurboBtnClick',()=>{
            this.turboBtnAction();
        },this);

        director.on('slotEmptyAutoSpinBtnClick',()=>{
            this.autoPlayBtnClick();
        },this);
        
        director.on('slotEmptyMinusBtnClick',()=>{
            this.minusBtnClick();
        },this);

        director.on('slotEmptyAddBtnClick',()=>{
            this.addBtnClick();
        },this);

        director.on('uiTipsOpen',(mode,tex)=>{
            if(mode == 'text'){
                if(globalThis.toast){
                    globalThis.toast(tex,1);
                }
            }
        },this);
    }

    protected autoPlayBtnClick(){
        if(this.betBtnState == 'normal'){
            if(this.betClickMode == 'normal'){
                this.setGameBtnState('disable');
                director.emit('viewChange','UIAutoSpin',-1);
            }else if(this.betClickMode == 'autoBet'){
                director.emit('viewChange', 'autoSpinCancel');
            }
        }else{
            if(this.betClickMode == 'autoBet'){
                director.emit('viewChange', 'autoSpinCancel');
            }
        }
    }

    protected updateBetInfo() {
        super.updateBetInfo();
        director.emit('slotUpdateBetInfo',this.betAmount);
    }
}
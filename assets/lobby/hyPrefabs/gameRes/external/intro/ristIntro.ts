
import { _decorator, assetManager, Component, EventTouch, Input, instantiate, Node, Size, size, Sprite, SpriteFrame, tween, UITransform, v2, v3, Vec2, Vec3 } from 'cc';
import { sComponent } from '../../../../game/core/sComponent';
const { ccclass, property } = _decorator;

@ccclass('ristIntro')
export class ristIntro extends sComponent {
    @property(Node)
    mainNode : Node;
    @property(UITransform)
    scrollView : UITransform;
    @property(UITransform)
    content : UITransform;
    @property(Node)
    itemCopy : Node;
    @property(Node)
    toggleCopy : Node;
    @property(Node)
    toggles : Node;
    @property(Node)
    leftBtn : Node;
    @property(Node)
    rightBtn : Node;
    @property(Node)
    closeArea : Node;

    private itemSize : Size;
    private scrollXStart = 0;
    private isScolling = false;
    private hasScroll = false;
    private currentIndex = 0;
    private imgTotal = 0;
    private get CurrentIndex(){return this.currentIndex;}
    private set CurrentIndex(value){this.currentIndex = value < 0 ? 0 : (value < this.imgTotal ? value : this.imgTotal - 1);}
    private touchEnable = false;

    protected start () {
        if(this.closeArea){
            this.closeArea.on('click',()=>{
                this.node.destroy();
            },this);
        }
        console.log('this.scrollView:'+this.scrollView.contentSize.x);
        this.scheduleOnce(()=>{
            console.log('this.scrollView:'+this.scrollView.contentSize.x);
        },1);
        this.scrollView.node.on(Input.EventType.TOUCH_START,(event : EventTouch)=>{
            this.scrollXStart = event.getLocationX();
        },this);
        this.scrollView.node.on(Input.EventType.TOUCH_MOVE,(event : EventTouch)=>{
            if(this.touchEnable && Math.abs(event.getLocationX() - this.scrollXStart) > 30){
                if(!this.hasScroll && !this.isScolling){
                    this.hasScroll = true
                    this.isScolling = true;
                    this.rightScroll(event.getLocationX() - this.scrollXStart > 0 ? -1 : 1);
                }
            }
        },this);
        let endCall = (event : EventTouch)=>{
            this.hasScroll = false;
        };
        this.scrollView.node.on(Input.EventType.TOUCH_END,endCall,this);
        this.scrollView.node.on(Input.EventType.TOUCH_CANCEL,endCall,this);

        this.leftBtn.on('click',()=>{
            if(this.touchEnable){
                this.rightScroll(-1);
            }
        },this);

        this.rightBtn.on('click',()=>{
            if(this.touchEnable){
                this.rightScroll(1);
            }
        },this);

        this.onView('intro/rule/' + (globalThis.GetLanguageType ? globalThis.GetLanguageType() : 'EN'));
    }

    protected setSize(){
        if(this.scrollView){
            this.itemSize = size(this.scrollView.contentSize);
        }
    }

    rightScroll(dir){
        if(dir == 1){
            this.CurrentIndex++;
        }else{
            this.CurrentIndex--;
        }
        tween(this.content.node).to(0.2,{position:v3(-(this.CurrentIndex * this.itemSize.x),0,0)},{easing: 'sineOut' }).call(()=>{
            this.isScolling = false;
        }).start();
        this.setToggleInde(this.CurrentIndex);
        this.arrowBtnUpdate(this.CurrentIndex);
    }

    arrowBtnUpdate(index){
        if(index == 0){
            this.leftBtn.children[0].active = false;
            this.rightBtn.children[0].active = true;
        }else if(index == this.imgTotal - 1){
            this.leftBtn.children[0].active = true;
            this.rightBtn.children[0].active = false;
        }else{
            this.leftBtn.children[0].active = true;
            this.rightBtn.children[0].active = true;
        }
    }

    protected setToggleInde(index){
        this.cleanTweenList();
        for (let i = 0; i < this.toggles.children.length; i++) {
            let element = this.toggles.children[i];
            let fore = element.children[1];
            if(index == i){
                this.pushOneTween(tween(fore).to(0.2,{scale : Vec3.ONE},{easing: 'sineOut' }).start());
            }else{
                this.pushOneTween(tween(fore).to(0.2,{scale : Vec3.ZERO},{easing: 'sineOut' }).start());
            }  
        }
    }

    protected onView(path){
        if(this.itemCopy && this.content){
            const bundle = assetManager.getBundle(globalThis.currentPlayingGameID);
            if(bundle){
                bundle.loadDir(path,SpriteFrame, (err, assets) =>{
                    if(assets && Array.isArray(assets) && assets.length > 0){
                        if(assets.length > 1){
                            this.leftBtn.active = true;
                            this.rightBtn.active = true;
                            this.toggles.active = true;
                        }
                        this.setSize();
                        let dic = assets.sort((a,b)=>{
                            let indexs1 = a.name.split('_');
                            let indexs2 = b.name.split('_');
                            if(indexs1 && indexs2){
                                let num1 = parseInt(indexs1[indexs1.length - 1]);
                                let num2 = parseInt(indexs2[indexs2.length - 1]);
                                if(!isNaN(num1) && !isNaN(num2)){
                                    return num1 - num2;
                                }
                            }
                            return 0;
                        });
                        for (let i = 0; i < dic.length; i++) {
                            const asset = dic[i];
                            let obj : Node = instantiate(this.itemCopy);
                            obj.setParent(this.itemCopy.parent,false);
                            obj.active = true;
                            obj.position = v3(i * this.itemSize.x + this.itemSize.x / 2,0,0);
                            obj.getComponent(UITransform).setContentSize(this.itemSize.x,this.itemSize.y);
                            const img = obj.getChildByName('img');
                            if(img){
                                img.getComponent(Sprite).spriteFrame = asset;
                            }
                            if(this.toggleCopy && this.toggles){
                                let tol : Node = instantiate(this.toggleCopy);
                                tol.setParent(this.toggles,false);
                                tol.active = true;
                            }
                        }
                        this.imgTotal = dic.length;
                        this.content.setContentSize(dic.length * this.itemSize.x,this.itemSize.y);
                        this.setToggleInde(this.CurrentIndex);
                        this.touchEnable = true;
                    }
                });
            }
        }
    }
}
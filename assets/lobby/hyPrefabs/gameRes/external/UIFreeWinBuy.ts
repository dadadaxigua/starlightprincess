
import { _decorator, Component, Node, Label, director } from 'cc';
import { sComponent } from '../../../game/core/sComponent';
const { ccclass, property } = _decorator;

@ccclass('UIFreeWinBuy')
export class UIFreeWinBuy extends sComponent {
    @property(Node)
    startBtn : Node;
    @property(Node)
    cancelNode : Node;
    @property(Label)
    coinLabel : Label;
    @property(Node)
    mainNode : Node;

    private mBetType = -1;
    private mCoin = 0;
    private canBuy = false;
    private tipStr;

    start () {
        this.startBtn.on('click',()=>{
            if(this.canBuy){
                this.mainNode.active = false;
                if(this.mBetType != -1){
                    director.emit('subGameBetActionBtnClick',this.mBetType,this.mCoin);
                }
            }else{
                // director.emit('uiTipsOpen', 'text', 'tipStr');
                // if(this.tipStr){
                //     director.emit('uiTipsOpen', 'text', this.tipStr);
                // }
                globalThis.goldNotenough && globalThis.goldNotenough();
            }
        },this);

        this.cancelNode.on('click',()=>{
            this.mBetType = -1;
            this.mainNode.active = false;
            director.emit('subGameBetActionBtnCancel');
        },this);
    }

    public viewInit(coin,betType,_canBuy,_tipStr){
        this.coinLabel.string = this.AddCommas(coin);
        this.mCoin = coin;
        this.mBetType = betType;
        this.canBuy = _canBuy;
        this.tipStr = _tipStr;
        this.mainNode.active = true;
    }
}



import { _decorator, Component, Node } from 'cc';
import { btnInternationalManager } from './btnInternationalManager';
const { ccclass, property } = _decorator;

 
@ccclass('btnInternationalizationScript')
export class btnInternationalizationScript extends Component {
    @property
    public componentName = 'cc.Label';
    @property
    public propertyName = 'string';
    @property
    public key = '';

    onLoad () {

    }

    init(){
        let _label = this.node.getComponent(this.componentName);
        if(_label){
            _label[this.propertyName] = btnInternationalManager.GetDataByKey(this.key);
        }
    }

    start () {
        this.init();
    }
}
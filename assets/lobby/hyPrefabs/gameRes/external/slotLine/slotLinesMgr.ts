
import { _decorator, color, Color, Component, director, instantiate, Material, math, MeshRenderer, Node, primitives, tween, utils, v2, v3, Vec2, Vec3 } from 'cc';
import { sComponent } from '../../../../game/core/sComponent';
import { sObjPool } from '../../../../game/core/sObjPool';
import { sUtil } from '../../../../game/core/sUtil';

const { ccclass, property } = _decorator;

@ccclass('slotLinesMgr')
export class slotLinesMgr extends sComponent {
    @property(Node)
    lineCopy : Node;
    @property(Node)
    linesContainer : Node;
    @property(Material)
    renderMat : Material;

    private borderSize : Vec2;
    private initialIndexPos : Vec2;
    private lineIndexSize : Vec2;

    private lineSize = 0.02;
    private pixelSize : Vec3;
    private pixelRealXSize : number;
    private pos : Vec3;

    private lineList = [];

    // protected update(dt: number): void {
    //     let iii = 255;
    //     this.renderMat.setProperty('mainColor',color(255,255,255,iii--));
    // }

    protected onLoad() {
        sObjPool.SetObj('slotLine',()=>{
            if(this.lineCopy){
                const line : Node = instantiate(this.lineCopy);
                if(line){
                    return line.getComponent(MeshRenderer);
                }
            }
            return null;
        });

        director.on('slotLineOpen',(dataArr : number[][][],_color : Color)=>{
            if(dataArr && Array.isArray(dataArr)){
                for (let i = 0; i < dataArr.length; i++) {
                    const datas = dataArr[i];
                    if(datas && Array.isArray(datas)){
                        this.setOneLineByIndexPath(datas,_color);
                    }
                }
            }
            // let iii = 255;
            // sUtil.TweenColorFrame(color(255,255,255,255),color(255,255,255,0),3,0,()=>{},(_color : Color)=>{
            //     console.log('_color:'+_color);
            //     this.renderMat.setProperty('mainColor',color(255,255,255,iii--));
            // });
            
            // this.renderMat.setProperty('mainColor',color(255,255,255,120));
            // this.scheduleOnce(()=>{
            //     this.renderMat.setProperty('mainColor',color(255,255,255,244));
            // },2);
        },this);

        director.on('slotLineClose',()=>{
            if(this.lineList && this.lineList.length > 0){
                for (let i = 0; i < this.lineList.length; i++) {
                    const line : MeshRenderer = this.lineList[i];
                    const h = line.getRenderMaterial(0).passes[0].getHandle('mainColor'); 
                    this.removeIDTween(line.node.uuid);
                    this.pushIDTween(sUtil.TweenColorFrame(color(255,255,255,255),color(255,255,255,0),0.3,0,()=>{
                        sObjPool.Enqueue('slotLine',line);
                        line.node.active = false;
                    },(_color : Color)=>{
                        line.getRenderMaterial(0).passes[0].setUniform(h,_color);
                    }),line.node.uuid);
                }
                this.lineList = [];
            }
        },this);



    }

    protected start () {
        
    }

    public Init(boxSumSize : Vec2,pixelSize : Vec3,pos : Vec3){
        if(boxSumSize && boxSumSize.x > 0 && boxSumSize.y > 0){
            this.borderSize = v2(boxSumSize.x,boxSumSize.y);
            this.initialIndexPos = v2(1 / (this.borderSize.x * 2) - 0.5,1 / (this.borderSize.y * 2) - 0.5);
            this.lineIndexSize = v2(1 / (this.borderSize.x), 1 / (this.borderSize.y));
            // this.initialIndexPos = v2(this.lineIndexSize.x - 0.5,this.lineIndexSize.y - 0.5);
            this.pixelSize = v3(pixelSize);
            this.pixelRealXSize = pixelSize.x / pixelSize.y;
            this.pos = v3(pos);
            console.log('this.borderSize:'+this.borderSize+' this.initialPos:'+this.initialIndexPos+' ');
        }else{
            console.warn('slotLinesMgr init error');
        }
    }

    private setOneLineByIndexPath(indexs : number[][],_color : Color){
        // console.log('setOneLineByIndexPath:'+_color);
        if(indexs && Array.isArray(indexs) && indexs.length > 1){
            const lineRender : MeshRenderer = sObjPool.Dequeue('slotLine');
            if(lineRender){
                this.lineList.push(lineRender);
                lineRender.node.parent = this.linesContainer;
                lineRender.node.active = true;
                lineRender.node.position = this.pos;
                lineRender.node.scale = v3(this.pixelSize.y,this.pixelSize.y,1);
                const h = lineRender.getRenderMaterial(0).passes[0].getHandle('mainColor'); 
                lineRender.getRenderMaterial(0).passes[0].setUniform(h,color(255,255,255,0));
                this.removeIDTween(lineRender.node.uuid);
                this.pushIDTween(sUtil.TweenColorFrame(color(255,255,255,0),color(255,255,255,255),0.3,0,()=>{},(_color : Color)=>{
                    lineRender.getRenderMaterial(0).passes[0].setUniform(h,_color);
                }),lineRender.node.uuid);
                if(indexs[0] && Array.isArray(indexs[0]) && indexs[0].length > 1 && indexs[1] && Array.isArray(indexs[1]) && indexs[1].length > 1){
                    const positions = [
                        -this.pixelRealXSize / 2,this.initialIndexPos.y + indexs[0][1] * this.lineIndexSize.y - this.lineSize * 0.5,0,
                        -this.pixelRealXSize / 2,this.initialIndexPos.y + indexs[0][1] * this.lineIndexSize.y + this.lineSize * 0.5,0
                    ];
                    const uvs = [
                        0,1,
                        0,0
                    ];
                    for (let i = 0; i < indexs.length; i++) {
                        const index = indexs[i];
                        if(index && Array.isArray(index) && index.length == 2){
                            // positions.push(
                            //     this.initialIndexPos.x + index[0] * this.lineIndexSize.x,this.initialIndexPos.y + index[1] * this.lineIndexSize.y,0,
                            //     this.initialIndexPos.x + index[0] * this.lineIndexSize.x - (i == 0 ? this.lineSize * 0.3 : 0),this.initialIndexPos.y + index[1] * this.lineIndexSize.y + (i == 0 ? this.lineSize * 0.85 : this.lineSize),0
                            // );
                            positions.push(
                                (this.initialIndexPos.x + index[0] * this.lineIndexSize.x) * this.pixelRealXSize,
                                this.initialIndexPos.y + index[1] * this.lineIndexSize.y - this.lineSize * 0.5,
                                0,

                                (this.initialIndexPos.x + index[0] * this.lineIndexSize.x) * this.pixelRealXSize,
                                this.initialIndexPos.y + index[1] * this.lineIndexSize.y + this.lineSize * 0.5,
                                0
                            );
                            uvs.push(
                                this.initialIndexPos.x + index[0] * this.lineIndexSize.x + 0.5,1,
                                this.initialIndexPos.x + index[0] * this.lineIndexSize.x + 0.5,0
                            );
                        }
                    }
                    positions.push(
                        this.pixelRealXSize / 2,this.initialIndexPos.y + indexs[indexs.length - 1][1] * this.lineIndexSize.y - this.lineSize * 0.5,0,
                        this.pixelRealXSize / 2,this.initialIndexPos.y + indexs[indexs.length - 1][1] * this.lineIndexSize.y + this.lineSize * 0.5,0
                    );
                    uvs.push(
                        1,1,
                        1,0
                    );

                    const indicesLength = (positions.length / 3 - 2) / 2;
                    const indices = [];
                    for (let i = 0; i < indicesLength; i++)
                    {
                        indices.push(
                            0 + i * 2 , 2 + i * 2 , 1 + i * 2,
                            2 + i * 2 , 3 + i * 2 , 1 + i * 2
                        );
                    }

                    const colors = [];
                    if(_color){
                        const colorsLength = positions.length / 3;
                        for (let i = 0; i < colorsLength; i++) {
                            // console.log('color:'+_color);
                            colors.push(_color.r / 255,_color.g / 255,_color.b / 255,_color.a / 255);
                            // colors.push(1,0.9,0,1);
                        }
                    }

                    for (let i = 0; i < indexs.length; i++) {
                        const index = indexs[i];
                        if(index.length == 2){
                            // console.log(index[1]);
                        }
                    }

                    if(positions && positions.length / 6 >= 3){
                        const len = positions.length / 6 - 1;
                        const rrr = 0.3;
                        for (let i = 1; i < len; i++) {
                            const lastDownYPos = positions[(i - 1) * 6 + 1];
                            const currentDownYPos = positions[i * 6 + 1];
                            const nextDownYPos = positions[(i + 1) * 6 + 1];
                            const leftSlope = (currentDownYPos - lastDownYPos) / this.lineIndexSize.y;
                            const rightSlope = (nextDownYPos - currentDownYPos) / this.lineIndexSize.y;
                            let upDeltaX = 0,upDeltaY = 0;
                            if(leftSlope == 0 || rightSlope == 0){
                                if(leftSlope != 0){
                                    upDeltaX = this.lineSize * rrr * -leftSlope;
                                }
                                if(rightSlope != 0){
                                    upDeltaX = this.lineSize * rrr * -rightSlope;
                                }
                            }else{
                                // console.log('yyyyy:'+(Math.abs(leftSlope) + Math.abs(rightSlope)) / 2);
                                const mLeftSlope = Math.abs(leftSlope),mRightSlope = Math.abs(rightSlope);
                                if(mLeftSlope != mRightSlope){
                                    const sss = Math.abs(leftSlope - rightSlope);
                                    if(leftSlope > 0){
                                        if(mLeftSlope > mRightSlope){
                                            upDeltaX = -this.lineSize * rrr * Math.pow(sss,0.1);
                                        }else{
                                            upDeltaX = this.lineSize * rrr * Math.pow(sss,0.1);
                                        }
                                    }else if(leftSlope < 0){
                                        if(mLeftSlope > mRightSlope){
                                            upDeltaX = this.lineSize * rrr * Math.pow(sss,0.1);
                                        }else{
                                            upDeltaX = -this.lineSize * rrr * Math.pow(sss,0.1);
                                        }
                                    }
                                    upDeltaY = Math.pow(sss,0.5) * this.lineSize * rrr;
                                }else{
                                    upDeltaY = (Math.abs(leftSlope) + Math.abs(rightSlope)) / 2 * this.lineSize * rrr * 2;
                                }
                            }
                            positions[i * 6 + 3] += upDeltaX;
                            positions[i * 6 + 4] += upDeltaY;

                            // uvs[i * 4] += downDeltaX;
                            uvs[i * 4 + 2] += upDeltaX;
                            uvs[i * 4 + 3] -= upDeltaY;

                            // console.log('currentDownYPos:'+(currentDownYPos / this.lineIndexSize.y),'leftSlope:'+(leftSlope),'rightSlope:'+rightSlope);
                            // console.log('upDeltaX:'+upDeltaX+',upDeltaY:'+upDeltaY);

                        }
                    }

                    let model : primitives.IGeometry = {
                        "positions":positions,
                        "uvs":uvs,
                        "indices":indices,
                        "colors":colors
                    };



                    // const ppp = [
                    //     -1,-0.5,0,
                    //     -1,0.5,0,
                    //     -0.25,-0.5,0,
                    //     -0.25,0.5,0,
                    //     0,-0.5,0,
                    //     0,0.5,0,
                    //     0.25,-0.5,0,
                    //     0.25,0.5,0,
                    //     0.5,-0.5,0,
                    //     0.5,0.5,0,
                    // ];


                    // const indicesppLength = (ppp.length / 3 - 2) / 2;
                    // const indicespp = [];
                    // for (let i = 0; i < indicesppLength; i++)
                    // {
                    //     indicespp.push(
                    //         0 + i * 2 , 2 + i * 2 , 1 + i * 2,
                    //         2 + i * 2 , 3 + i * 2 , 1 + i * 2
                    //     );
                    // }
                    // let ss : primitives.IGeometry = {
                    //     "positions":ppp
                    //     ,
                    //     "uvs":
                    //     [
                    //         0,1,
                    //         0,0,
                    //         0.25,1,
                    //         0.25,0,
                    //         0.5,1,
                    //         0.5,0,
                    //         0.75,1,
                    //         0.75,0,
                    //         1,1,
                    //         1,0
                            
                    //     ],
                    //     "indices":
                    //     indicespp,
                    // };

                    
                    // lineRender.mesh = utils.createMesh(ss,null,{calculateBounds : false});
                    lineRender.mesh = utils.createMesh(model,null,{calculateBounds : false});
                }
            }
        }
    }
}


import { _decorator, Component, Node, director, Animation, UIOpacity, tween, Tween, Label, v3, UITransform, size, SpriteFrame, TERRAIN_HEIGHT_BASE, Sprite } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('UIPopup')
export class UIPopup extends Component {
    @property(Label)
    tipLabel : Label;
    @property(Node)
    iconNode : Node;
    @property(UITransform)
    bgTrans : UITransform;
    @property(SpriteFrame)
    turboOnSpriteFrame : SpriteFrame;
    @property(SpriteFrame)
    turboOffSpriteFrame : SpriteFrame;

    onLoad(){
        this.node.active = false;

        director.on('uiTipsOpen',(mode,tex)=>{
            if(mode == 'text'){
                this.node.active = true;
                this.tipLabel.string = tex;
                this.tipLabel.updateRenderData(true);
                this.tipLabel.node.position = v3(0,0,0);
                this.bgTrans.contentSize = size(this.tipLabel.node.getComponent(UITransform).contentSize.x + 50,this.bgTrans.contentSize.height);
                this.iconNode.active = false;
                this.node.getComponent(Animation).play();
                let opa = this.node.getComponent(UIOpacity);
                opa.opacity = 255;
                Tween.stopAllByTarget(opa);
                tween(opa).delay(3).to(0.5,{opacity : 0}).call(()=>{
                    this.node.active = false;
                }).start().target(opa);
            }
            else if(mode == 'turbo'){
                this.node.active = true;
                this.tipLabel.string = tex;
                this.tipLabel.updateRenderData(true);
                this.tipLabel.node.position = v3(20,0,0);
                this.bgTrans.contentSize = size(this.tipLabel.node.getComponent(UITransform).contentSize.x + 100,this.bgTrans.contentSize.height);
                this.iconNode.active = true;
                this.iconNode.position = v3(-this.bgTrans.contentSize.x / 2 + 40,0,0);
                this.iconNode.getComponent(Sprite).spriteFrame = this.turboOnSpriteFrame;
                this.node.getComponent(Animation).play();
                let opa = this.node.getComponent(UIOpacity);
                opa.opacity = 255;
                Tween.stopAllByTarget(opa);
                tween(opa).delay(3).to(0.5,{opacity : 0}).call(()=>{
                    this.node.active = false;
                }).start().target(opa);
            }
            else if(mode == 'normal'){
                this.node.active = true;
                this.tipLabel.string = tex;
                this.tipLabel.updateRenderData(true);
                this.tipLabel.node.position = v3(20,0,0);
                this.bgTrans.contentSize = size(this.tipLabel.node.getComponent(UITransform).contentSize.x + 100,this.bgTrans.contentSize.height);
                this.iconNode.active = true;
                this.iconNode.position = v3(-this.bgTrans.contentSize.x / 2 + 40,0,0);
                this.iconNode.getComponent(Sprite).spriteFrame = this.turboOffSpriteFrame;
                this.node.getComponent(Animation).play();
                let opa = this.node.getComponent(UIOpacity);
                opa.opacity = 255;
                Tween.stopAllByTarget(opa);
                tween(opa).delay(3).to(0.5,{opacity : 0}).call(()=>{
                    this.node.active = false;
                }).start().target(opa);
            }
        },this);
    }

    start () {

    }

}


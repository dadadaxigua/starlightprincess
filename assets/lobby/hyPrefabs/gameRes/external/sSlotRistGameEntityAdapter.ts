
import { _decorator, Component, Node, Button, Animation, UIOpacity, director, v3, tween, Sprite, color, Color, Label, Vec3, quat } from 'cc';
import { sGameEntity } from './sGameEntity';
import sAudioMgr from '../../../game/core/sAudioMgr';
const { ccclass, property } = _decorator;

@ccclass('sSlotRistGameEntityAdapter')
export class sSlotRistGameEntityAdapter extends sGameEntity {
    @property(Label)
    betDetailLabel : Label;
    @property(Button)
    maxBtn : Button;
    @property(Button)
    infoBtn : Button;
    @property(Sprite)
    spinRender : Sprite;

    private userCoin = 0;
    private winTotal = 0;
    public firstRate = 0;

    private rotationSpeed = quat(0,0,-0.01);
    private rollType = 0;//0 : normal bet  1 : auto bet

    protected onLoad(){
        super.onLoad();

        // this.scheduleOnce(()=>{
        //     this.freeWinBingo();
        // },2);

        director.on('betBtnState',state=>{
            if(this.spinRender){
                if(state == 'disable'){
                    this.spinRender.grayscale = true;
                }else if(state == 'enable'){
                    if(this.rollType == 0){
                        this.spinRender.grayscale = false;
                    }
                }
            }
        },this);

        director.on('betBtnClick',(betSpeedMode)=>{
            sAudioMgr.PlayShotAudio('slots_button_spin');
            this.rotationSpeed.z = -0.13;
            if(this.spinRender){
                tween(this.spinRender.node).to(0.1,{scale : v3(0.85,0.85,0.85)},{easing:'cubicOut'}).to(0.1,{scale : Vec3.ONE},{easing:'cubicIn'}).start();
            }
            if(this.rollType == 0){
             
            }else if(this.rollType == 1){

            }
        },this);

        director.on('readyToBet',()=>{
            if(this.rollType == 0){
                this.rotationSpeed.z = -0.01;
                if(this.spinRender){
                    
                }  
            }
        },this);

        director.on('autoBetInfoUpdate',value=>{
            if(value == -1){
                this.rollType = 1;
                if(this.spinRender){
                    this.spinRender.grayscale = true;
                }
            }else if(value > 0){
                this.rollType = 1;
                if(this.spinRender){
                    this.spinRender.grayscale = true;
                }
            }else{
                this.rollType = 0;
                if(this.spinRender && this.canBet){
                    this.spinRender.grayscale = false;
                }
            }
        },this);

        director.on('uiTipsOpen',(mode,tex)=>{
            if(mode == 'text'){
                if(globalThis.toast){
                    globalThis.toast(tex,1);
                }
            }
        },this);

        director.on('freeWinBetShowCoin',(rate)=>{
            let mUserInfo = this.getUserInfo();
            if (mUserInfo) {
                this.winTotal += rate * this.betAmount;
                this.userCoin = mUserInfo.coin + rate * this.betAmount;
                if(rate > 0){
                    this.betUserInfoUpdateAnima(this.userCoin, null, this.winTotal, 0.2);
                }
            }        
        },this);

        director.on('viewChange', (type, value) => {
            if (type == 'autoSpinCancel') {
                this.rollType = 0;
                this.rotationSpeed.z = -0.01;
                const toggle = this.autoPlayBtnNode.node.getChildByName('toggle').getComponent(Sprite);
                toggle.color = color(48,48,48,155);
            }
        },this);
        
        this.maxBtn.node.on('click',()=>{
            sAudioMgr.PlayShotAudio('slots_button_max_bet');
            if (this.clientConfig) {
                let betTypes = this.clientConfig.bet_types;
                if (betTypes) {
                    for (let i = betTypes.length - 1; i >= 0; i--) {
                        const element = betTypes[i];
                        if (this.gameid == element.game_id) {
                            this.betAmount = element.bet_amount;
                            this.lineAmount = element.bet_multiple;
                            let mUserInfo = this.getUserInfo();
                            if (mUserInfo) {
                                director.emit('betUserInfoUpdate', mUserInfo.coin, this.betAmount * this.lineAmount, 0);
                                director.emit('betTypeChange', element.bet_type);
                                this.betLabel.node.setScale(1.5, 1.5, 1.5);
                                tween(this.betLabel.node).to(0.6, { scale: Vec3.ONE }, { easing: 'elasticOut' }).start();
                                this.updateBetInfo();
                                this.saveBetAmountValue();
                                break;
                            }
                        }
                    }
                }
            }
        },this);

        this.infoBtn.node.on('click',()=>{
            sAudioMgr.PlayShotAudio('btnClick');
            const scene = director.getScene();
            if(scene && scene.children && scene.children.length > 0){
                director.emit('slotRistIntro',scene.children[0]);
            }
        },this);
    }

    protected turboBtnNodeClick(value : boolean){
        if(this.turboBtnNode){
            sAudioMgr.PlayShotAudio('slots_button_commonBtnClick_1');
            if(value){
                const turboOff = this.turboBtnNode.node.getChildByName('turbo_off');
                turboOff.active = false;
                const turboOn = this.turboBtnNode.node.getChildByName('turbo_on');
                turboOn.active = true;
                const toggle = this.turboBtnNode.node.getChildByName('toggle').getComponent(Sprite);
                toggle.color = Color.WHITE;
            }else{
                const turboOn = this.turboBtnNode.node.getChildByName('turbo_on');
                turboOn.active = false;
                const turboOff = this.turboBtnNode.node.getChildByName('turbo_off');
                turboOff.active = true;
                const toggle = this.turboBtnNode.node.getChildByName('toggle').getComponent(Sprite);
                toggle.color = color(48,48,48,155);
            }
        }
    }

    protected addBtnNodeStateUpdate(visible : boolean){
        if(this.addBtnNode){
            this.addBtnNode.getComponent(UIOpacity).opacity = visible ? 255 : 125;
        }
    }

    protected minusBtnNodeStateUpdate(visible : boolean){
        if(this.minusBtnNode){
            this.minusBtnNode.getComponent(UIOpacity).opacity = visible ? 255 : 125;
        }
    }

    protected autoPlayBtnNodeStateUpdate(visible : boolean){
        // if(!visible){
        //     const toggle = this.autoPlayBtnNode.node.getChildByName('toggle').getComponent(Sprite);
        //     toggle.color = color(48,48,48,155);
        // }
        // if(this.autoPlayBtnNode){
            // this.autoPlayBtnNode.getComponent(UIOpacity).opacity = visible ? 255 : 125;
        // }
    }

    protected autoPlayBtnClick(){
        sAudioMgr.PlayShotAudio('slots_button_commonBtnClick_1');
        if(this.canBet){
            if(this.betBtnState == 'normal'){
                if(this.betClickMode == 'normal'){
                    this.setGameBtnState('disable');
                    const toggle = this.autoPlayBtnNode.node.getChildByName('toggle').getComponent(Sprite);
                    toggle.color = Color.WHITE;
                    director.emit('viewChange','UIAutoSpin',-1);
                }else if(this.betClickMode == 'autoBet'){
                    const toggle = this.autoPlayBtnNode.node.getChildByName('toggle').getComponent(Sprite);
                    toggle.color = color(48,48,48,155);
                    director.emit('viewChange', 'autoSpinCancel');
                }
            }else{
                if(this.betClickMode == 'autoBet'){
                    const toggle = this.autoPlayBtnNode.node.getChildByName('toggle').getComponent(Sprite);
                    toggle.color = color(48,48,48,155);
                    director.emit('viewChange', 'autoSpinCancel');
                }
            }
        }else{
            this.betClickCoinNotEnough();
        }
    }

    protected menuBtnClick(){
        director.emit('subGameMenuView', true);
        const audioData = globalThis.getSubGameAudioVolume();
        if (audioData) {
            if (audioData.audioVolume == 1) {
                this.soundBtn.node.getChildByName('open').active = true;
                this.soundBtn.node.getChildByName('close').active = false;
            } else {
                this.soundBtn.node.getChildByName('close').active = true;
                this.soundBtn.node.getChildByName('open').active = false;
            }
        }
        this.menuBtns.node.active = true;
    }

    protected closeBtnClick(){
        director.emit('subGameMenuView', false);
        this.menuBtns.node.active = false;
    }

    protected soundBtnClick(){
        if(this.audioTouchEnable){
            this.audioTouchEnable = false;
            const audioData = globalThis.getSubGameAudioVolume();
            if (audioData) {
                if (audioData.audioVolume == 1) {
                    globalThis.subGameAudioVolumeCtr(false);
                    this.soundBtn.node.getChildByName('close').active = true;
                    this.soundBtn.node.getChildByName('open').active = false;
                } else {
                    globalThis.subGameAudioVolumeCtr(true);
                    this.soundBtn.node.getChildByName('open').active = true;
                    this.soundBtn.node.getChildByName('close').active = false;
                }
            }
            this.scheduleOnce(()=>{
                this.audioTouchEnable = true;
            },1);
        }
    }

    protected soundTip(value : boolean){
        // this.betBtns.node.getChildByPath('menuBtn/sound_off').active = value;
    }

    protected updateBetInfo() {
        super.updateBetInfo();
        this.betDetailLabel.string = `${this.AddCommas(this.betAmount)}X${this.lineAmount}`;
    }

    protected addBtnClick(){
        sAudioMgr.PlayShotAudio('slots_button_add_bet');
        super.addBtnClick();
    }
    
    protected minusBtnClick(){
        sAudioMgr.PlayShotAudio('slots_button_limit_bet');
        super.minusBtnClick();
    }

    protected spinBtnClick(){
        // sAudioMgr.PlayShotAudio('slots_button_spin');
    }

    protected freeWinBingo(){
        this.setGameBtnState('disable');
        this.infoBtn.interactable = false;
        this.infoBtn.getComponent(UIOpacity).opacity = 125;
        this.maxBtn.interactable = false;
        this.maxBtn.getComponent(UIOpacity).opacity = 125;
        this.gameMode = 'freeWin';
        if(this.viewMode == 'record'){
            if(this.recordBtns){
                this.recordBtns.node.active = false;
            }
        }
    }

    protected freeWinOver(){
        this.winTotal = 0;
        this.setGameBtnState('enable');
        this.infoBtn.interactable = true;
        this.infoBtn.getComponent(UIOpacity).opacity = 255;
        this.maxBtn.interactable = true;
        this.maxBtn.getComponent(UIOpacity).opacity = 255;
        if(this.viewMode == 'record'){
            this.recordBtnTouchEnable = false;
            this.scheduleOnce(()=>{
                this.recordBtnTouchEnable = true;
            },3);
        }
        this.recordState = 'idle';
        this.gameMode = 'normal';
    }

    protected freeWinBegain(){
        if(this.viewMode == 'record'){
            if(this.recordBtns){
                this.recordBtns.node.active = true;
            }
        }
        this.userCoin = this.beforeUserCoin;
        this.winTotal = this.firstRate > 0 ? this.firstRate * this.betAmount : 0;
    }

    protected freeWinBtnInfo(rate){
        if(rate){
            this.firstRate = rate;
        }else{
            this.firstRate = 0;
        }
    }


    protected setGameBtnState(state) {
        super.setGameBtnState(state);
        if (state == 'enable') {
            this.maxBtn.getComponent(UIOpacity).opacity = 255;
            this.maxBtn.interactable = true;
        } else if (state == 'disable') {
            this.maxBtn.getComponent(UIOpacity).opacity = 125;
            this.maxBtn.interactable = false;
        }
    }

    protected errToOneRes(){
        const toggle = this.autoPlayBtnNode.node.getChildByName('toggle').getComponent(Sprite);
        toggle.color = color(48,48,48,155);
        super.errToOneRes();
    }

    update (deltaTime: number) {
        if(this.spinRender){
            this.spinRender.node.rotate(this.rotationSpeed);
        }
    }
}


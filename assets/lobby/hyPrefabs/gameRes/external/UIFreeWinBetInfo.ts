
import { _decorator, Component, Node, Label, v3, tween, Vec3, director } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('UIFreeWinBetInfo')
export class UIFreeWinBetInfo extends Component {
    @property(Label)
    ownLabel : Label;
    @property(Label)
    betLabel : Label;
    @property(Label)
    winLabel : Label;

    public firstRate = 0;

    private betAmount = 0;
    private lineAmount = 0;
    private userCoin = 0;
    private winTotal = 0;

    start () {
        let country = '';
        if(globalThis.getCountry){
            const con = globalThis.getCountry();
            if(con){
                country = con;
            }
        }
        if(country == 'VNM'){
            if(this.betLabel){
                const vnIcon = this.betLabel.node.parent.getChildByName('vnIcon');
                if(vnIcon){
                    vnIcon.active = true;
                }
            }
        }else{
            if(this.betLabel){
                const icon = this.betLabel.node.parent.getChildByName('icon');
                if(icon){
                    icon.active = true;
                }
            }
        }
        director.on('freeWinBetShowCoin',(rate)=>{
            this.winTotal += rate * this.betAmount;
            this.userCoin += rate * this.betAmount;
            // console.log('freeWinBetShowCoin:'+rate+',,,'+this.userCoin);
            if(rate > 0){
                this.betUserInfoUpdateAnima(this.userCoin,null,this.winTotal);
            }
        },this);
    }

    onEnable(){
        this.lineAmount = globalThis.GameBtnEntity.lineAmount;
        this.betAmount = globalThis.GameBtnEntity.CurrentBetAmount;
        this.userCoin = globalThis.GameBtnEntity.beforeUserCoin;
        console.log('this.firstRate:'+this.firstRate);
        this.winTotal = this.firstRate > 0 ? this.firstRate * this.betAmount : 0;
        this.userCoin += this.winTotal;
        let mUserInfo = globalThis.getUserInfo();
        if(mUserInfo){
            this.betUserInfoUpdate(this.userCoin,this.betAmount*this.lineAmount,this.winTotal);
        }
    }

    betUserInfoUpdate(own,bet,win) {
        if(own || own == 0){
            this.ownLabel.node['rollAnimaValue'] = own;
            this.ownLabel.string = this.AddCommas(own);
        }
        if(bet || bet == 0){
            this.betLabel.node['rollAnimaValue'] = bet;
            this.betLabel.string = this.AddCommas(bet);
        }
        if(win || win == 0){
            this.winLabel.node['rollAnimaValue'] = win;
            this.winLabel.string = this.AddCommas(win);
        }
    }

    updateRate(rate){
        this.firstRate = rate;
    }

    betUserInfoUpdateAnima(own,bet,win,animaTime = 0.2,callBack = null) {
        if(own || own == 0){
            this.CommasLabelAnima(this.ownLabel,own,animaTime);
        }
        if(bet || bet == 0){
            this.CommasLabelAnima(this.betLabel,bet,animaTime);
        }
        if(win || win == 0){
            this.CommasLabelAnima(this.winLabel,win,animaTime);
        }
        if(callBack != null){
            this.scheduleOnce(callBack,animaTime);
        }
    }

    CommasLabelAnima(label,value,time){
        if(label && (label.node['rollAnimaValue'] || label.node['rollAnimaValue'] == 0)){
            const valueOri = label.node['rollAnimaValue'];
            const mTween = label.node['rollAnimaTween'];
            if(mTween){
                mTween.stop();
            }
            let tweenTargetVec3 = v3(valueOri,valueOri,valueOri);
            // console.log('tweenTargetVec3:'+tweenTargetVec3);
            label.node['rollAnimaTween'] = tween(tweenTargetVec3).to(time,v3(value,value,value),{"onUpdate":(target : Vec3)=>{
                if(label){
                    label.node['rollAnimaValue'] = Math.floor(target.x)
           
                    label.string = this.AddCommas(Math.floor(target.x));
                }
            }, easing : 'quadOut'}).call(()=>{
                if(label){
                    label.node['rollAnimaValue'] = Math.floor(value)
                    label.string = this.AddCommas(Math.floor(value));
                }
            }).start();
        }
    }

    public AddCommas (money: any) {
        if(!money){
            return "0";
        }else{
            let mon;
            if(typeof money == "string"){
                mon = Number(money);
            }else{
                mon = money;
            }
            let coinRate = 1;
            if(globalThis.getCoinRate){
                coinRate = globalThis.getCoinRate();
            }
            let byte = coinRate.toString().length - 1;;
            let showMoney = (mon / coinRate).toFixed(byte);
            let tempmoney = String(showMoney);
            var left = tempmoney.split('.')[0],
                right = tempmoney.split('.')[1];
            right = right ? (right.length >= byte ? '.' + right.substring(0, byte) : '.' + right) : '';
            var temp = left.split('').reverse().join('').match(/(\d{1,3})/g);
            if (temp) {
                return (Number(tempmoney) < 0 ? "-" : "") + temp.join(',').split('').reverse().join('') + right;
            } else {
                return '';
            }
        }
    }
}

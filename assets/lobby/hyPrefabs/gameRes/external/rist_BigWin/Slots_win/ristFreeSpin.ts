
import { _decorator, Animation, Button, Component, director, Label, Node } from 'cc';
import { sComponent } from '../../../../../game/core/sComponent';
import sAudioMgr from '../../../../../game/core/sAudioMgr';
import { sUtil } from '../../../../../game/core/sUtil';
const { ccclass, property } = _decorator;

@ccclass('ristFreeSpin')
export class ristFreeSpin extends sComponent {
    @property(Button)
    clickBtn : Button;
    @property(Animation)
    mAnimation: Animation;
    @property(Label)
    spinLabel: Label;

    private touchEnable = false;
    private targetNum = 0;

    protected start () {
        this.node.on('freeSpinAnima', this.freeSpinAnima, this);

        this.clickBtn.node.on('click',()=>{
            if(this.touchEnable){
                this.touchEnable = false;
                sAudioMgr.PlayShotAudio('btnClick');
                const mainNode = this.node.getChildByName('main');
                if(mainNode){
                    this.mAnimation.on(Animation.EventType.FINISHED,()=>{
                        mainNode.active = false;
                        director.emit('ristFreeSpinClose');
                    },this);
                    this.mAnimation.play('Opa_Close');
                }
            }
        },this);

        director.on('ristFreeSpinNum',()=>{
            console.log('ristFreeSpinNum');
            sUtil.TweenLabel(3,this.targetNum,this.spinLabel,1,0,'quadOut',0,null,true);
        },this);
    }

    protected freeSpinAnima(num : number){
        const mainNode = this.node.getChildByName('main');
        if (mainNode) {
            this.targetNum = num;
            mainNode.active = true;
            this.SetUIFront();
            this.mAnimation.off(Animation.EventType.FINISHED);
            this.removeScheduleDic();
            this.spinLabel.string = '3';
            this.mAnimation.play('FreeSpins');
            this.scheduleOnce(()=>{
                sAudioMgr.PlayShotAudio('slots_spins_bonus');
            },0.6);
            this.scheduleOnce(()=>{
                this.touchEnable = true;
            },2);
            this.pushIDSchedule(()=>{
                if(this.touchEnable){
                    this.touchEnable = false;
                    this.mAnimation.on(Animation.EventType.FINISHED,()=>{
                        mainNode.active = false;
                        director.emit('ristFreeSpinClose');
                    },this);
                    this.mAnimation.play('Opa_Close');
                }
            },5,'close');
        }
    }
}
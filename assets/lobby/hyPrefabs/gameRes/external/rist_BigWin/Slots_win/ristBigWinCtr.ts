
import { _decorator, Animation, Component, director, Label, Node, ParticleSystem, tween, v3, Vec3 } from 'cc';
import { sComponent } from '../../../../../game/core/sComponent';
import { sUtil } from '../../../../../game/core/sUtil';
import sAudioMgr from '../../../../../game/core/sAudioMgr';
const { ccclass, property } = _decorator;

@ccclass('ristBigWinCtr')
export class ristBigWinCtr extends sComponent {
    @property(Animation)
    mAnimation: Animation;
    @property(Label)
    coinLabel: Label;

    private targetModel = '';

    protected start() {
        this.node.on('bigWinAnima', this.bigWinAnima, this);
    }

    public bigWinAnima(coin, rate) {
        // console.log('coin:'+coin);
        // console.log('rate:'+rate);
        const multiple = globalThis.GameBtnEntity.lineAmount; 
        if (coin > 0 && rate > 0 && multiple > 0) {
            const mainNode = this.node.getChildByName('main');
            if (mainNode) {
                mainNode.active = true;
                this.mAnimation.off(Animation.EventType.FINISHED);
                this.cleanTweenList();
                this.cleanTweenDic();
                this.removeScheduleDic();
                this.SetUIFront();
                const readRate = rate / multiple;
                const bigWin = 10;
                const megaWin = 20;
                const superWin = 50;
                this.targetModel = 'bigwin';
                let targetCoin = coin;
                let line = coin / rate;
                let megaCoin = 0;
                let superCoin = 0;
                if (readRate >= bigWin && readRate < megaWin) {
                    
                }
                else if (readRate >= megaWin && readRate < superWin) {
                    this.targetModel = 'megawin';
                    targetCoin = line * megaWin * multiple;
                    megaCoin = coin;
                }
                else if (readRate >= superWin) {
                    this.targetModel = 'superwin';
                    targetCoin = line * megaWin * multiple;
                    megaCoin = line * superWin * multiple;
                    superCoin = coin;
                }
                this.mAnimation.play('Bigwin');

                if (this.targetModel == 'bigwin') {
                    sAudioMgr.PlayShotAudio('ristBtnBigWin_1');
                    sUtil.TweenLabel(0, targetCoin, this.coinLabel, 2.5, 0, 'quadOut', 0, () => {
                        this.coinLabelAnima();
                        this.closeAnima();
                    });
                } else if (this.targetModel == 'megawin') {
                    sAudioMgr.PlayShotAudio('ristBtnBigWin_1');
                    sUtil.TweenLabel(0, targetCoin, this.coinLabel, 2.5, 0, 'quadOut', 0, () => {
                        this.coinLabelAnima();
                    });
                    this.pushIDSchedule(() => {
                        sAudioMgr.PlayShotAudio('ristBtnBigWin_2');
                        this.mAnimation.play('Hugewin');
                        sUtil.TweenLabel(targetCoin, megaCoin, this.coinLabel, 3, 0, 'quadOut', 0, () => {
                            this.coinLabelAnima();
                            this.closeAnima();
                        });
                    }, 4, 'megawin');
                } else if (this.targetModel == 'superwin') {
                    sAudioMgr.PlayShotAudio('ristBtnBigWin_1');
                    sUtil.TweenLabel(0, targetCoin, this.coinLabel, 2.5, 0, 'quadOut', 0, () => {
                        this.coinLabelAnima();
                    });
                    this.pushIDSchedule(() => {
                        sAudioMgr.PlayShotAudio('ristBtnBigWin_2');
                        this.mAnimation.play('Hugewin');
                        sUtil.TweenLabel(targetCoin, megaCoin, this.coinLabel, 3, 0, 'quadOut', 0, () => {
                            this.coinLabelAnima();
                        });
                        this.pushIDSchedule(() => {
                            sAudioMgr.PlayShotAudio('ristBtnBigWin_3');
                            this.mAnimation.stop();
                            this.mAnimation.play('Superwin');
                            this.scheduleOnce(() => {
                                console.log(this.node);
                            }, 1);
                            sUtil.TweenLabel(megaCoin, superCoin, this.coinLabel, 3, 0, 'quadOut', 0, () => {
                                this.coinLabelAnima();
                                this.closeAnima();
                            });
                        }, 5, 'superwin');
                    }, 5, 'megawin');
                }
            }
        }
    }

    private coinLabelAnima() {
        sAudioMgr.PlayShotAudio('ristBtnBigWin_ding');
        this.mAnimation.play('End');
        // if(this.coinLabel){
        //     this.pushOneTween(tween(this.coinLabel.node).to(0.18,{scale : v3(1.1,1.1,1.1)},{easing : 'sineOut'}).to(0.14,{scale : Vec3.ONE},{easing : 'sineIn'}).start());
        // }
    }

    private closeAnima() {
        this.pushIDSchedule(() => {
            // this.node.children[0].active = false;
            this.mAnimation.on(Animation.EventType.FINISHED, () => {
                // this.node.active = false;
                this.node.children[0].active = false;
                director.emit('ristBigWinFinish');
            }, this);
            this.mAnimation.play('Close');
        }, 1.5, 'close');
    }

}


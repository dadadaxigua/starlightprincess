
import { _decorator, Animation, Button, Component, director, Label, Node } from 'cc';
import { sComponent } from '../../../../../game/core/sComponent';
import sAudioMgr from '../../../../../game/core/sAudioMgr';
const { ccclass, property } = _decorator;
 
@ccclass('ristTotalWin')
export class ristTotalWin extends sComponent {
    @property(Animation)
    mAnimation: Animation;
    @property(Label)
    coinLabel: Label;
    @property(Button)
    clickBtn : Button;
    
    private touchEnable = false;
    private closing = false;

    protected start () {
        this.node.on('totalWinAnima',this.totalWinAnima,this);

        this.clickBtn.node.on('click',()=>{
            if(!this.closing && this.touchEnable){
                sAudioMgr.PlayShotAudio('btnClick');
                this.closing = true;
                const mainNode = this.node.getChildByName('main');
                if(mainNode){
                    this.mAnimation.on(Animation.EventType.FINISHED,()=>{
                        mainNode.active = false;
                        director.emit('ristTotalWinFinish');
                    },this);
                    this.mAnimation.play('Opa_Close');
                }
            }
        },this);
    }

    private totalWinAnima(coin : number,waitTime : number){
        if(coin > 0 && waitTime > 0){
            const mainNode = this.node.getChildByName('main');
            if(mainNode){
                sAudioMgr.PlayShotAudio('ristBtnBigWin_3');
                this.touchEnable = false;
                this.closing = false;
                this.mAnimation.off(Animation.EventType.FINISHED);
                this.removeScheduleDic();
                this.SetNodeFront(this.node);
                mainNode.active = true;
                this.mAnimation.play('Totalwin');
                this.coinLabel.string = this.AddCommas(coin);
                this.scheduleOnce(()=>{
                    this.touchEnable = true;
                },2);
                this.pushIDSchedule(()=>{
                    if(!this.closing){
                        this.closing = true;
                        this.mAnimation.on(Animation.EventType.FINISHED,()=>{
                            mainNode.active = false;
                            director.emit('ristTotalWinFinish');
                        },this);
                        this.mAnimation.play('Opa_Close');
                    }
                },waitTime,'close');
            }
        }
    }
}

import { _decorator, Component, Node, Label, color, EventTouch, Widget, UITransform, tween, v3, UIOpacity, director, Sprite, Renderable2D, Button, Color } from 'cc';
const { ccclass, property } = _decorator;
 
@ccclass('UIAutoSpin')
export class UIAutoSpin extends Component {
    @property(Node)
    btnNode : Node;
    @property(Node)
    mainNode : Node;
    @property(Node)
    contentNode : Node;
    @property(Node)
    closeBtn : Node;
    @property(Node)
    comfirmBtn : Node;

    @property(Label)
    ownLabel : Label;
    @property(Label)
    betLabel : Label;
    @property(Label)
    winLabel : Label;

    btnColor : Color;
    autoSpinCount = 0;
    touchEnable = true;

    onLoad(){
        this.node.getComponent(Widget).updateAlignment();
    }

    start () {
        let country = '';
        if(globalThis.getCountry){
            const con = globalThis.getCountry();
            if(con){
                country = con;
            }
        }
        if(country == 'VNM'){
            if(this.betLabel){
                const vnIcon = this.betLabel.node.parent.getChildByName('vnIcon');
                if(vnIcon){
                    vnIcon.active = true;
                }
            }
        }else{
            if(this.betLabel){
                const icon = this.betLabel.node.parent.getChildByName('icon');
                if(icon){
                    icon.active = true;
                }
            }
        }

        this.closeBtn.on('click',()=>{
            globalThis.subGamePlayShotAudio('subGameClick');
            this.autoSpinCount = 0;
            this.disappearViewAnima();
        },this);

        this.comfirmBtn.on('click',()=>{
            globalThis.subGamePlayShotAudio('subGameClick');
            this.disappearViewAnima();
        },this);
    }

    onEnable(){
        this.comfirmBtn.getComponent(Button).interactable = false;
        const col = this.comfirmBtn.getComponent(Sprite).color;
        this.btnColor = color(col.r,col.g,col.b,255);
        this.comfirmBtn.getComponent(Sprite).color = color(this.btnColor.r,this.btnColor.g,this.btnColor.b,117);
        if(this.btnNode){
            for (let i = 0; i < this.btnNode.children.length; i++) {
                const element = this.btnNode.children[i];
                const label = element.children[1].getComponent(Renderable2D);
                if(label){
                    label.color = color(134,134,134,255);
                }
            }
        }
    }

    btnClick(event: EventTouch, customEventData: number){
        globalThis.subGamePlayShotAudio('subGameClick');
        this.autoSpinCount = customEventData;
        this.comfirmBtn.getComponent(Button).interactable = true;
        this.comfirmBtn.getComponent(Sprite).color = color(this.btnColor.r,this.btnColor.g,this.btnColor.b,255);
        if(this.btnNode){
            for (let i = 0; i < this.btnNode.children.length; i++) {
                const element = this.btnNode.children[i];
                const label = element.children[1].getComponent(Renderable2D);
                if(label){
                    label.color = color(134,134,134,255);
                }
            }
        }
        let target : Node =  event.target;
        if(target){
            target.children[1].getComponent(Renderable2D).color = color(this.btnColor.r,this.btnColor.g,this.btnColor.b,255);
        }
    }

    betInfoSet(coin,bet,win){
        this.ownLabel.string = this.AddCommas(coin);
        this.betLabel.string = this.AddCommas(bet);
        this.winLabel.string = this.AddCommas(win);
    }

    appearViewAnima(){
        const viewHeight = this.node.getComponent(UITransform).contentSize.height;
        const mainNode = this.mainNode;
        const panelHeight = mainNode.getComponent(UITransform).height;
        // const opa = this.contentNode.getComponent(UIOpacity);
        // opa.opacity = 0;
        tween(mainNode).to(0.3,{position:v3(0,panelHeight - viewHeight/2,0)},{easing : 'sineOut'}).call(()=>{
            // tween(opa).to(0.2,{opacity:255}).call(()=>{

            // }).start();
        }).start();
    }
    disappearViewAnima(){
        if(this.touchEnable){
            this.touchEnable = false;
            const viewHeight = this.node.getComponent(UITransform).contentSize.height;
            const mainNode = this.mainNode;
            tween(mainNode).to(0.3,{position:v3(0,-viewHeight/2,0)},{easing : 'sineOut'}).call(()=>{
                this.touchEnable = true;
                this.node.active = false;
                director.emit('viewChange','UIAutoSpin',this.autoSpinCount);
            }).start();
        }
    }

    public AddCommas (money: any) {
        if(!money){
            return "0";
        }else{
            let mon;
            if(typeof money == "string"){
                mon = Number(money);
            }else{
                mon = money;
            }
            let coinRate = 1;
            if(globalThis.getCoinRate){
                coinRate = globalThis.getCoinRate();
            }
            let byte = coinRate.toString().length - 1;;
            let showMoney = (mon / coinRate).toFixed(byte);
            let tempmoney = String(showMoney);
            var left = tempmoney.split('.')[0],
                right = tempmoney.split('.')[1];
            right = right ? (right.length >= byte ? '.' + right.substring(0, byte) : '.' + right) : '';
            var temp = left.split('').reverse().join('').match(/(\d{1,3})/g);
            if (temp) {
                return (Number(tempmoney) < 0 ? "-" : "") + temp.join(',').split('').reverse().join('') + right;
            } else {
                return '';
            }
        }
    }
}

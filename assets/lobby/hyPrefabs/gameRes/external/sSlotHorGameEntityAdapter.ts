
import { _decorator, assetManager, Button, Component, director, JsonAsset, Label, Node, Sprite, UIOpacity, Vec2 } from 'cc';
import { sGameEntity } from './sGameEntity';
import sAudioMgr from '../../../game/core/sAudioMgr';
import { sUtil } from '../../../game/core/sUtil';
import { btnInternationalManager } from './btnInternationalManager';
import { sIntroView } from '../../../game/core/sIntroView';
const { ccclass, property } = _decorator;

 
@ccclass('sSlotHorGameEntityAdapter')
export class sSlotHorGameEntityAdapter extends sGameEntity {
    btnRenderNode : Node;
    ruleNode : Node;
    shopNode : Button;
    @property
    ruleViewSize = 1000;

    protected onLoad () {
        try {
            this.betLabel = this.node.getChildByPath('betInfo/betAmountLabel').getComponent(Label);
            this.winLabel = this.node.getChildByPath('betInfo/winLabel').getComponent(Label);
            this.verLabel = this.node.parent.getChildByPath('ver').getComponent(Label);
            this.turboBtnNode = this.node.getChildByPath('betBtns/turbo').getComponent(Button);
            this.minusBtnNode = this.node.getChildByPath('betBtns/minus').getComponent(Button);
            this.addBtnNode = this.node.getChildByPath('betBtns/add').getComponent(Button);
            this.autoPlayBtnNode = this.node.getChildByPath('betBtns/auto').getComponent(Button);
            this.betBtnNode = this.node.getChildByPath('btn').getComponent(Button);
            this.menuBtn = this.node.parent.getChildByName('menuBtn').getComponent(Button);
            this.rulesBtn = this.node.getChildByPath('betBtns/info').getComponent(Button);
            this.btnRenderNode = this.node.getChildByPath('betBtns/Spin_Btn');
        } catch (error) {
            console.error('sSlotHorGameEntityAdapter node error : ',error);
        }
        super.onLoad();

        director.on('betBtnState',state=>{
            if(state == 'disable'){
                this.spinViewCtr(false);
            }else if(state == 'enable'){
                this.spinViewCtr(true);
            }
        },this);

        director.on('betBtnClick',(betSpeedMode)=>{
            sAudioMgr.PlayShotAudio('button');
            this.spinViewCtr(false);
        },this);

        director.on('autoBetInfoUpdate',value=>{
            // if(value == -1){
            //     this.spinViewCtr(false);
            // }else if(value > 0){
            //     this.spinViewCtr(false);
            // }else{
            //     this.spinViewCtr(true);
            // }
        },this);

        director.on('viewChange', (type, value) => {
            if (type == 'autoSpinCancel') {
                const toggle = this.autoPlayBtnNode.node.getChildByName('toggle');
                toggle.getChildByName('on').active = false;
                toggle.getChildByName('off').active = true;
            }
        },this);

        director.on('rollStop',()=>{
            if(this.shopNode && this.shopNode.node && this.shopNode.node.isValid){
                this.shopNode.interactable = true;
                this.shopNode.node.children[0].getComponent(Sprite).grayscale = false;
            }
        },this);

        director.on('slotSubGameRollBegin',()=>{
            if(this.shopNode && this.shopNode.node && this.shopNode.node.isValid){
                this.shopNode.interactable = false;
                this.shopNode.node.children[0].getComponent(Sprite).grayscale = true;
            }
        },this);
    }

    protected start() {
        Promise.all([new Promise((resolve,reject)=>{
            globalThis.getSubGamePrefabByPath('prefabs/subGameUserCoin',(newObj : Node)=>{
                if(newObj){
                    newObj.parent = this.node.parent;
                    this.ownLabel = newObj.getChildByName('Label').getComponent(Label);
                    this.shopNode = newObj.getChildByName('shop').getComponent(Button);
                }
                resolve(1);
            });
        }),new Promise((resolve,reject)=>{
            globalThis.getSubGamePrefabByPath('prefabs/subGameSettingDetail',(newObj : Node)=>{
                if(newObj){
                    newObj.parent = this.node.parent.parent;
                    this.menuBtns = newObj.getComponent(UIOpacity);
                    this.soundBtn = this.menuBtns.node.getChildByPath('main/items/sound').getComponent(Button);
                    this.closeBtn = this.menuBtns.node.getChildByPath('close').getComponent(Button);
                    this.quitBtn = this.menuBtns.node.getChildByPath('main/items/exit').getComponent(Button);
                    newObj.active = false;
                }
                resolve(2);
            });
        }),new Promise((resolve,reject)=>{
            globalThis.getSubGamePrefabByPath('prefabs/UIFreeWinBuy',(newObj : Node)=>{
                if(newObj){
                    newObj.parent = this.node.parent.parent;
                    this.freeWinBuy = newObj;
                }
                resolve(3);
            });
        }),new Promise((resolve,reject)=>{
            globalThis.getSubGamePrefabByPath('prefabs/UISubGameIntro',(newObj : Node)=>{
                if(newObj){
                    newObj.parent = this.node.parent.parent;
                    newObj.active = false;
                    this.ruleNode = newObj;
                    if(this.ruleViewSize != 1000){
                        this.ruleNode.getComponent(sIntroView).changeSize(this.ruleViewSize);
                    }
                }
                resolve(4);
            });
        }),new Promise((resolve,reject)=>{
            const lua = globalThis.GetLanguageType();
            if(lua){
                sUtil.getLobbyAssetInSubGame('config/'+lua,JsonAsset,(err,asset)=>{
                    if(asset){
                        btnInternationalManager.SetLanguegeData(asset.name, asset.json);
                    }
                    resolve(5);
                });
            }else{
                resolve(5);
            }
        })]).then((res)=>{
            super.start();
            if(this.shopNode){
                this.shopNode.node.on('click',()=>{
                    globalThis.openStore && globalThis.openStore();
                },this);
            }
        });
    }

    private spinViewCtr(value){
        this.btnRenderNode.getComponent(Sprite).grayscale = !value;
        // this.btnRenderNode.children[0].getComponent(Sprite).grayscale = !value;
    }

    protected autoPlayBtnClick(){
        if(this.canBet){
            if(this.betBtnState == 'normal'){
                if(this.betClickMode == 'normal'){
                    this.setGameBtnState('disable');
                    const toggle = this.autoPlayBtnNode.node.getChildByName('toggle');
                    toggle.getChildByName('on').active = true;
                    toggle.getChildByName('off').active = false;
                    director.emit('viewChange','UIAutoSpin',-1);
                }else if(this.betClickMode == 'autoBet'){
                    const toggle = this.autoPlayBtnNode.node.getChildByName('toggle');
                    toggle.getChildByName('on').active = false;
                    toggle.getChildByName('off').active = true;
                    director.emit('viewChange', 'autoSpinCancel');
                }
            }else{
                if(this.betClickMode == 'autoBet'){
                    const toggle = this.autoPlayBtnNode.node.getChildByName('toggle');
                    toggle.getChildByName('on').active = false;
                    toggle.getChildByName('off').active = true;
                    director.emit('viewChange', 'autoSpinCancel');
                }
            }
        }else{
            this.betClickCoinNotEnough();
        }
    }

    protected errToOneRes(){
        const toggle = this.autoPlayBtnNode.node.getChildByName('toggle');
        toggle.getChildByName('on').active = false;
        toggle.getChildByName('off').active = true;
        super.errToOneRes();
    }

    protected menuBtnClick(){
        director.emit('subGameMenuView', true);
        const audioData = globalThis.getSubGameAudioVolume();
        if (audioData) {
            if (audioData.audioVolume == 1) {
                this.soundBtn.node.getChildByName('open').active = true;
                this.soundBtn.node.getChildByName('close').active = false;
            } else {
                this.soundBtn.node.getChildByName('close').active = true;
                this.soundBtn.node.getChildByName('open').active = false;
            }
        }
        this.menuBtns.node.active = true;
    }

    protected soundBtnClick(){
        if(this.audioTouchEnable){
            this.audioTouchEnable = false;
            const audioData = globalThis.getSubGameAudioVolume();
            if (audioData) {
                if (audioData.audioVolume == 1) {
                    globalThis.subGameAudioVolumeCtr(false);
                    this.soundBtn.node.getChildByName('close').active = true;
                    this.soundBtn.node.getChildByName('open').active = false;
                } else {
                    globalThis.subGameAudioVolumeCtr(true);
                    this.soundBtn.node.getChildByName('open').active = true;
                    this.soundBtn.node.getChildByName('close').active = false;
                }
            }
            this.scheduleOnce(()=>{
                this.audioTouchEnable = true;
            },1);
        }
    }

    protected closeBtnClick(){
        director.emit('subGameMenuView', false);
        this.menuBtns.node.active = false;
    }

    protected rulesBtnClick(){
        this.ruleNode.active = true;
    }

    protected addBtnClick(){
        super.addBtnClick();
        sAudioMgr.PlayShotAudio('button');
    }
    
    protected minusBtnClick(){
        super.minusBtnClick();
        sAudioMgr.PlayShotAudio('button');
    }

    protected turboBtnNodeClick(value : boolean){
        if(this.turboBtnNode){
            if(value){
                const turboOff = this.turboBtnNode.node.getChildByName('turbo_off');
                turboOff.active = false;
                const turboOn = this.turboBtnNode.node.getChildByName('turbo_on');
                turboOn.active = true;
            }else{
                const turboOn = this.turboBtnNode.node.getChildByName('turbo_on');
                turboOn.active = false;
                const turboOff = this.turboBtnNode.node.getChildByName('turbo_off');
                turboOff.active = true;
            }
        }
    }
}

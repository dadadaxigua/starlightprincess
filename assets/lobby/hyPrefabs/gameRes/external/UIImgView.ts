
import { _decorator, Component, Node, assetManager, SpriteFrame, instantiate, Sprite, UITransform, UIOpacity, tween, v3, Widget, Label, director } from 'cc';
const { ccclass, property } = _decorator;


@ccclass('UIImgView')
export class UIImgView extends Component {
    @property(Node)
    imgCopyNode: Node;
    @property(Node)
    closeBtn: Node;
    @property(Label)
    titleLabel: Label;

    touchEnable = true;

    onLoad() {
        this.node.getComponent(Widget).updateAlignment();
    }

    start() {
        this.closeBtn.on('click', () => {
            globalThis.subGamePlayShotAudio('subGameClick');
            this.disappearViewAnima();
        }, this);
    }

    onView(path, title) {
        this.titleLabel.string = title;
        const bundle = assetManager.getBundle(globalThis.currentPlayingGameID);
        if (bundle) {
            bundle.loadDir(path, SpriteFrame, (err, assets) => {
                // console.log(assets);
                if (assets && Array.isArray(assets) && assets.length > 0) {
                    let dic = assets.sort((a, b) => {
                        let indexs1 = a.name.split('_');
                        let indexs2 = b.name.split('_');
                        if (indexs1 && indexs2) {
                            let num1 = parseInt(indexs1[indexs1.length - 1]);
                            let num2 = parseInt(indexs2[indexs2.length - 1]);
                            if (!isNaN(num1) && !isNaN(num2)) {
                                return num1 - num2;
                            }
                        }
                        return 0;
                    });
                    for (let i = 0; i < dic.length; i++) {
                        const asset = dic[i];
                        let img: Node = instantiate(this.imgCopyNode);
                        img.setParent(this.imgCopyNode.parent, false);
                        img.active = true;
                        img.getComponent(Sprite).spriteFrame = asset;
                    }
                }
            });
        }
    }

    appearViewAnima() {
        const viewUITransform = this.node.getComponent(UITransform);
        const mainNode = this.node.getChildByName('main').getComponent(UITransform);
        mainNode.contentSize = viewUITransform.contentSize;
        mainNode.node.position = v3(0, -viewUITransform.contentSize.height / 2, 0);
        tween(mainNode.node).to(0.3, { position: v3(0, viewUITransform.contentSize.height / 2, 0) }, { easing: 'sineOut' }).call(() => {

        }).start();
    }

    disappearViewAnima() {
        if (this.touchEnable) {
            this.touchEnable = false;
            const viewUITransform = this.node.getComponent(UITransform);
            const mainNode = this.node.getChildByName('main').getComponent(UITransform);
            tween(mainNode.node).to(0.3, { position: v3(0, -viewUITransform.contentSize.height / 2, 0) }, { easing: 'sineOut' }).call(() => {
                this.touchEnable = true;
                this.node.active = false;
                for (let i = 1; i < this.imgCopyNode.parent.children.length; i++) {
                    const element = this.imgCopyNode.parent.children[i];
                    element.destroy();
                }
                director.emit('slotDisappearView','UIImgView');
            }).start();
        }
    }

}


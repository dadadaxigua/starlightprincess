
import { _decorator, Component, Node } from 'cc';
 
export class btnInternationalManager {
    private static jsonData = new Map<string,any>();
    private static mData;

    public static GetDataByKey(key){
        if(globalThis.GetLanguageType){
            if(!btnInternationalManager.mData){
                btnInternationalManager.mData = btnInternationalManager.jsonData.get(globalThis.GetLanguageType());
            }
            if(btnInternationalManager.mData){
                const value = btnInternationalManager.mData[key];
                if(value){
                    return value.text;
                }
            }
        }
        return '';
    }

    public static SetLanguegeData(type : string,data){
        if(type && data){
            btnInternationalManager.jsonData.set(type,data);
        }
    }

    public static Clear(){
        btnInternationalManager.mData = null;
        btnInternationalManager.jsonData.clear();
    }
}


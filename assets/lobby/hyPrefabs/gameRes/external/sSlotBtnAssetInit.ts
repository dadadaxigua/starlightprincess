
import { _decorator, assetManager, AudioClip, Component, director, Node, SpriteAtlas } from 'cc';
import sAudioMgr from '../../../game/core/sAudioMgr';
import { assetMgr } from '../../../game/core/sAssetMgr';
const { ccclass, property } = _decorator;

 
@ccclass('sSlotBtnAssetInit')
export class sSlotBtnAssetInit extends Component {
    @property({tooltip:'音频的资源引用',type:[AudioClip]})
    gameAudioClip = [];
    @property
    isLoadLanguageImg = true;

    protected onLoad() {
        if (this.gameAudioClip && this.gameAudioClip.length) {
            for (let i = 0; i < this.gameAudioClip.length; i++) {
                const clip = this.gameAudioClip[i];
                sAudioMgr.SetAudioClip(clip);
            }
        }
    }

    protected start(){
        let language = 'EN';
        if(globalThis.GetLanguageType){
            language = globalThis.GetLanguageType();
        }
        const bundle = globalThis.lobbyBundle;
        if(bundle){
            bundle.load('hyPrefabs/gameRes/external/config/img/'+language,SpriteAtlas,(err,spriteAtlas)=>{
                if(spriteAtlas){
                    assetMgr.SetSpriteFramesToDic(spriteAtlas);
                }else{
                    // console.warn('subGameLanguageImg:'+err);
                }
            });
        }
    }
}



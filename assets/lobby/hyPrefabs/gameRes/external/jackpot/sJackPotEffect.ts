
import { _decorator, AudioClip, Component, director, Label, Node, sp, tween, UIOpacity } from 'cc';
import { sComponent } from '../../../../game/core/sComponent';
import sAudioMgr from '../../../../game/core/sAudioMgr';
import { sUtil } from '../../../../game/core/sUtil';

const { ccclass, property } = _decorator;

 
@ccclass('sJackPotEffect')
export class sJackPotEffect extends sComponent {
    @property(Node)
    mainNode : Node;
    @property(sp.Skeleton)
    spineAnima : sp.Skeleton;
    @property(UIOpacity)
    maskOpa : UIOpacity;
    @property(Label)
    coinLabel : Label;
    @property({tooltip:'音频的资源引用',type:[AudioClip]})
    gameAudioClip = [];

    private touchEnable = false;
    private coinValueEnd = 0;

    protected start () {
        // this.scheduleOnce(()=>{
        //     director.emit('slotJackPotBingo',1004223);
        // },2);
        if (this.gameAudioClip && this.gameAudioClip.length) {
            for (let i = 0; i < this.gameAudioClip.length; i++) {
                const clip = this.gameAudioClip[i];
                sAudioMgr.SetAudioClip(clip);
            }
        }

        this.maskOpa.node.on('click',()=>{
            if(this.touchEnable){
                this.touchEnable = false;
                this.coinLabel.string = this.AddCommas(this.coinValueEnd);
                this.cleanTweenList();
                this.spineAnima.setCompleteListener(null);
                this.scheduleOnce(()=>{
                    this.coinLabel.string = '';
                    this.viewClose();
                },3);
            }
        },this);

        director.on('slotJackPotBingo',(coinValue : number)=>{
            if(coinValue > 0){
                sAudioMgr.PlayShotAudio('jackpot');
                this.SetUIFront();
                this.coinValueEnd = coinValue;
                this.cleanTweenList();
                this.mainNode.active = true;
                this.maskOpa.opacity = 0;
                this.pushOneTween(tween(this.maskOpa).to(0.3,{opacity : 255}).start());
                this.spineAnima.node.active = true;
                this.spineAnima.setAnimation(0,'a1',false);
                this.spineAnima.setCompleteListener(()=>{
                    this.spineAnima.setAnimation(0,'a2',true);
                    this.touchEnable = true;
                });
                this.coinLabel.string = '0';
                this.pushOneTween(sUtil.TweenLabel(0,coinValue,this.coinLabel,5,0,'quadOut',0,()=>{
                    this.touchEnable = false;
                    this.scheduleOnce(()=>{
                        this.coinLabel.string = '';
                        this.viewClose();
                    },3);
                }));
            }else{
                director.emit('slotJackPotEtClose');
            }
        },this);
    }

    private viewClose(){
        this.cleanTweenList();
        this.pushOneTween(tween(this.maskOpa).to(0.3,{opacity : 0}).call(()=>{
            this.mainNode.active = false;
        }).start());
        this.spineAnima.setCompleteListener(null);
        this.spineAnima.node.active = false;
        director.emit('slotJackPotEtClose');
    }

}

import { _decorator, Component, Node, sys, math, ScrollView, director, Prefab, instantiate, game, resources, UITransform, tween, v3, Widget, Label } from 'cc';
import { ScrollViewCycle } from '../../../../scripts/util/scrollViewCycle';
import { gameRecordImgFollow } from './gameRecordImgFollow';
import { gameRecordItem } from './gameRecordItem';
const { ccclass, property } = _decorator;


@ccclass('gameRecord')
export class gameRecord extends Component {
    @property(Node)
    clostBtn: Node;
    @property(ScrollView)
    scrolView: ScrollView;
    @property(Node)
    content: Node;
    @property(Node)
    noHistoryNode: Node;
    @property(ScrollViewCycle)
    scrollViewCycle: ScrollViewCycle;
    @property(gameRecordImgFollow)
    gameRecordImgFollow: gameRecordImgFollow;
    @property(Label)
    timezoneLab: Label;
    gameId='';
    tableId= '';
    type='';
    recordDatas: [];
    onLoad() {
        this.addBtnListen();
        if(globalThis.getTimezone) this.timezoneLab.string = globalThis.getTimezone()
        director.on('closeGameRecord', (gameId) => {
            if (gameId = this.gameId) {
                if (this.type == 'solt') {
                    this.disappearViewAnima()
                } else {
                    this.node.destroy()
                }

            }
        }, this)
        director.on('updateGameRecord', (gameId) => {
            if (gameId == this.gameId) {
                this.recordDatas = globalThis.gameRecordMgr.getRecordByGameId(this.gameId);
                this.noHistoryNode.active = false;
                this.scrollViewCycle.getComponent(ScrollView)!.stopAutoScroll();
                this.scrollViewCycle.resetContentY();
                this.scrollViewCycle.changeHeight(this.recordDatas.length);
            }
        }, this);
    }
    init() {
        if(this.type == 'solt'){
            this.recordDatas = globalThis.gameRecordMgr.getRecordByGameId(this.gameId);
            if (this.recordDatas.length == 0) {
                this.noHistoryNode.active = true;
            } else {
                this.noHistoryNode.active = false;
            }
            this.scollItemInit()
        }else if(this.type == 'poker'){
            globalThis.gameRecordMgr.getRecordByServer(this.tableId,(data)=>{
                this.recordDatas =data;
                if (this.recordDatas.length == 0) {
                    this.noHistoryNode.active = true;
                } else {
                    this.noHistoryNode.active = false;
                }
                this.scollItemInit()
            });

        }
 
    }

    scollItemInit() {
        this.scrollViewCycle.init(0, (item, index) => {
            this.updateItemInfo(item, index);
        });
        this.scrollViewCycle.getComponent(ScrollView)!.stopAutoScroll();
        this.scrollViewCycle.resetContentY();
        this.scrollViewCycle.changeHeight(this.recordDatas.length);
    }

    addBtnListen() {
        this.clostBtn.on('click', () => {
            director.emit('slotDisappearView','gameRecord');
            this.node.destroy()
        }, this)
    }

    updateItemInfo(item, index) {
        if (!this.recordDatas || this.recordDatas.length < 0) return;
        let data = this.recordDatas[index];
        if (!this.recordDatas[index]) return;
        item.getComponent(gameRecordItem).init(data, this.type);
        this.gameRecordImgFollow.updateItem(item, data, index, this.gameId, this.type,this.tableId);
    }

    appearViewAnima() {
        const viewUITransform = this.node.getComponent(UITransform);
        const mainNode = this.node.getChildByName('main').getComponent(UITransform);
        mainNode.contentSize = viewUITransform.contentSize;
        mainNode.node.position = v3(0, -viewUITransform.contentSize.height / 2, 0);
        tween(mainNode.node).to(0.3, { position: v3(0, viewUITransform.contentSize.height / 2, 0) }, { easing: 'sineOut' }).call(() => {

        }).start();
    }

    touchEnable = true;
    disappearViewAnima() {
        if (this.touchEnable) {
            this.touchEnable = false;
            const viewUITransform = this.node.getComponent(UITransform);
            const mainNode = this.node.getChildByName('main').getComponent(UITransform);
            tween(mainNode.node).to(0.3, { position: v3(0, -viewUITransform.contentSize.height / 2, 0) }, { easing: 'sineOut' }).call(() => {
                this.touchEnable = true;
                this.node.destroy()
            }).start();
        }
    }
}
class GameRecordMgr {
    private recordMap = {};
    private curSaveIndexMap: any = {};
    private saveRecordByGameId(gameId, record: any) {
        let records: any = this.getRecordByGameId(gameId);
        let curSaveIndexs = this.getCurSaveIndexs(gameId);
        let uid = globalThis.getUserInfo().uid;
        let maxLength = this.getRecordMaxSaveLength()
        let startIndex = curSaveIndexs[0];
        let endIndex = curSaveIndexs[1];
        record.id = endIndex;
        let key = `record-${gameId}-${uid}-${endIndex++}`;
        sys.localStorage.setItem(key, JSON.stringify(record))
        records.unshift(record)
        let needDelIndex: any = null;
        if (endIndex - startIndex > maxLength) {
            needDelIndex = startIndex;
            let removeKey = `record-${gameId}-${uid}-${startIndex}`;
            startIndex++;
            sys.localStorage.removeItem(removeKey)
            records.pop();
        }
        this.saveCurSaveIndexs(gameId, [startIndex, endIndex]);
        this.recordMap[gameId] = records;
        director.emit('updateGameRecord', gameId);
        return {
            delIndex: needDelIndex,
            saveIndex: curSaveIndexs[1]
        }
    }

    private getCurSaveIndexs(gameId) {
        let curSaveIndexs = this.curSaveIndexMap[gameId];
        if (!curSaveIndexs) {
            let uid = globalThis.getUserInfo().uid;
            let recordSaveIndexJson = sys.localStorage.getItem(`${gameId}-${uid}-recordSaveIndex`);
            try {
                curSaveIndexs = JSON.parse(recordSaveIndexJson);
            } catch (error) {
            }
        }
        if (!curSaveIndexs) {
            curSaveIndexs = [0, 0];
            this.curSaveIndexMap[gameId] = curSaveIndexs;
        }

        return curSaveIndexs;
    }

    public pairRecordById(gameId, id, record) {
        let records: any = this.getRecordByGameId(gameId);
        for (let i = 0; i < records.length; i++) {
            if (records[i].id == id) {
                records[i] = record;
            }
        }
        director.emit('updateGameRecord', gameId);
        let uid = globalThis.getUserInfo().uid;
        let key = `record-${gameId}-${uid}-${id}`;
        record.id = id;
        sys.localStorage.setItem(key, JSON.stringify(record))
    }

    private saveCurSaveIndexs(gameId, curSaveIndexs) {
        let uid = globalThis.getUserInfo().uid;
        this.curSaveIndexMap[gameId] = curSaveIndexs;
        sys.localStorage.setItem(`${gameId}-${uid}-recordSaveIndex`, JSON.stringify(curSaveIndexs))
    }

    public getRecordByGameId(gameId) {
        let records: any = this.recordMap[gameId];
        let uid = globalThis.getUserInfo().uid;
        if (!records) records = [];
        if (!records || records.length == 0) {
            let curSaveIndexs = this.getCurSaveIndexs(gameId);
            let startIndex = +curSaveIndexs[0], endIndex = +curSaveIndexs[1];
            if (endIndex == 0) return [];
            for (let i = startIndex; i < endIndex; i++) {
                let key = `record-${gameId}-${uid}-${i}`;
                let recordJson = sys.localStorage.getItem(key)
                if (recordJson) {
                    try {
                        let record = JSON.parse(recordJson);
                        records.unshift(record)
                    } catch (error) {
                    }
                }

            }

        }
        this.recordMap[gameId] = records;
        return records;
    }

    public oneSoltRecordCome(gameId, record: any) {
        this.saveRecordByGameId(gameId, record)
    }

    public getRecordMaxSaveLength() {
        return 50;
    }

    public createGameRecordNode(parent, gameId, type, cb,tableId) {

        if (type == 'solt') {
            globalThis.getSoltGameRecord((node) => {
                node.parent = parent;
                node.getComponent(gameRecord).gameId = gameId;
                node.getComponent(gameRecord).type = type;
                node.getComponent(gameRecord).init();
                node.active = true;
                node.getComponent(Widget).updateAlignment();
                node.getComponent(gameRecord).appearViewAnima();
            })
        } else if (type == 'poker') {
            globalThis.getPokerGameRecord((node) => {
                node.parent = parent;
                node.getComponent(gameRecord).gameId = gameId;
                node.getComponent(gameRecord).type = type;
                node.getComponent(gameRecord).tableId = tableId;
                node.getComponent(gameRecord).init();
                node.active = true;
                if (cb) cb(node)
            });
        }
    }
    serverRecordMap = {}
    getRecordByServer(tableId, callback) { 
        if (!globalThis.miniTableGameGetRecord) return [];
        let uid = globalThis.getUserInfo().uid;
        if(!this.serverRecordMap[tableId+'-'+uid]){
            this.serverRecordMap[tableId+'-'+uid] = this.getLocalServerRecord(tableId)
        }
        let time = null;
        if (this.serverRecordMap[tableId+'-'+uid] && this.serverRecordMap[tableId+'-'+uid].length){
            time = this.serverRecordMap[tableId+'-'+uid][0].bet_time;
        }
        globalThis.subGameSocketReq('miniTable.mainHandler.gameRecord', {
            table_id: tableId,
            start_time: time
        },(err, res) => {
            if (res && res.code == 200) {
                this.serverRecordMap[tableId+'-'+uid] = [...res.data, ...this.serverRecordMap[tableId+'-'+uid]]
                this.setLocalServerRecord(tableId)
                callback(this.serverRecordMap[tableId+'-'+uid])
            } else {
                callback([])
                // if (globalThis.toast) {
                //     globalThis.toast(ResLoader.loadText(501, 'getRecordFailed'), 1.5)
                // }
            }
        });

    }
    getRecordDetailsByRoundId(tableId,roundId){
        let uid = globalThis.getUserInfo().uid;
        let arr = this.serverRecordMap[tableId+'-'+uid];
        if(!arr) return null;
        for (let i = 0; i < arr.length; i++) {
            if(arr[i].round_id == roundId) return arr[i]
        }
        return null;
    }
    getLocalServerRecord(tableId) {
        let uid = globalThis.getUserInfo().uid;
        let key = `gameLocalServerRecord-${tableId}-${uid}`
        let localData = localStorage.getItem(key);
        try {
            let data = JSON.parse(localData)
            if(!data) data = []
            return data
        } catch (error) {
            return [];
        }
    }
    setLocalServerRecord(tableId) {
        let uid = globalThis.getUserInfo().uid;
        let key = `gameLocalServerRecord-${tableId}-${uid}`
        let data = this.serverRecordMap[tableId+'-'+uid];
        localStorage.setItem(key, JSON.stringify(data))
    }
}

globalThis.gameRecordMgr = new GameRecordMgr();


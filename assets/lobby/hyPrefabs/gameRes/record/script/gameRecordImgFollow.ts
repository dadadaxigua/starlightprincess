
import { _decorator, Component, Node, UITransform, instantiate, NodePool, Color, Sprite } from 'cc';
import { gameRecordImgItem } from './gameRecordImgItem';
const { ccclass, property } = _decorator;
 
@ccclass('gameRecordImgFollow')
export class gameRecordImgFollow extends Component  {

    @property(Node)
    followNode: Node;

    @property(Node)
    item: Node;

    itemPool: NodePool = null;

    init() {
        this.itemPool = new NodePool();
        this.node.getComponent(UITransform).contentSize = this.followNode.getComponent(UITransform).contentSize;
        this.node.getComponent(UITransform).anchorPoint = this.followNode.getComponent(UITransform).anchorPoint;
        this.node.position = this.followNode.position;
    }

    creatItem() {
        let item = null;
        if (this.itemPool.size() > 0) {
            item = this.itemPool.get();
        } else {
            item = instantiate(this.item);
        }
        item.active = true;
        item.parent = this.node;
        return item;
    }

    recycleItem(item){
        item.active = false;
        this.itemPool.put(item);
    }

    clear(){
        for (let key in this.itemMap) {
            this.recycleItem(this.itemMap[key])
        }
        this.itemMap = {};
    }
   
    itemMap = {};
  
    updateItem(item,data,index,gameId,recordType = 'solt',tableId) {
        if(!this.itemPool){
            this.init();
        }
        let copyItem:Node|null = null;
        if(this.itemMap[item.uuid]){
            copyItem = this.itemMap[item.uuid];
            this.itemMap[item.uuid].position = item.position;
        }else{
             copyItem =  this.creatItem();
            this.itemMap[item.uuid] = copyItem;
            this.itemMap[item.uuid].position = item.position;
        }
        copyItem.active = true;
        let profit = data.profit;
        if(recordType == 'poker'){
            profit = data.coin_change;
        }
        copyItem.getComponent(gameRecordImgItem).init(index,gameId,data.id,recordType,profit,tableId,data.round_id)
        this.node.getComponent(UITransform).contentSize = this.followNode.getComponent(UITransform).contentSize;
        this.node.getComponent(UITransform).anchorPoint = this.followNode.getComponent(UITransform).anchorPoint;
        this.node.position = this.followNode.position;
    }
    update () {
        if(this.node.position.x!=this.followNode.position.x||this.node.position.y!=this.followNode.position.y){
         this.node.position = this.followNode.position;
        }
     }
   
}


import { _decorator, Component, Node, Label, Color, math } from 'cc';
import { sUtil } from '../../../../game/core/sUtil';
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = gameRecordItem
 * DateTime = Thu Nov 03 2022 11:51:03 GMT+0800 (中国标准时间)
 */

@ccclass('gameRecordItem')
export class gameRecordItem extends Component {
    @property(Label)
    timeLab: Label;
    
    @property(Label)
    betLab: Label;
    @property(Label)
    profitLab: Label;

    colors = [
        new Color('36f06a'),
        new Color('f03636'),
        new Color('ffffff')
    ]

    onLoad() {

    }

    init(data, type) {
        if (type == 'poker') {
            this.initPokerItem(data)
        }else if(type == 'solt'){
            this.initSoltItem(data)
        }
    }

    initPokerItem(data){
        let date = new Date(+data.bet_time)
        let m = +date.getMinutes();
        let s = +date.getSeconds();
        let mTime = m < 10 ? "0" + m : m;
        let sTime = s < 10 ? "0" + s : s;
        let timeStr1 = `${date.getHours()}:${mTime}:${sTime}`
        let timeStr2 = `${date.getMonth() + 1}/${date.getDate()}`
        this.timeLab.string = `${timeStr2} ${timeStr1}`;
        if (data.bet_amount || data.bet_amount == 0) {
            this.betLab.string = sUtil.AddCommas(data.bet_amount);
        } else {
            this.betLab.string = '-';
        }
        if (data.coin_change >= 0) {
            this.profitLab.color = this.colors[0];
        } else {
            this.profitLab.color = this.colors[1];
        }
        let sub = data.coin_change >= 0 ? '+' : '-';
        sub = data.coin_change == 0 ? '' : sub
        if (data.coin_change || data.coin_change == 0) {
            this.profitLab.string = sub + sUtil.AddCommas(Math.abs(data.coin_change));
        } else {
            this.profitLab.color = this.colors[2];
            this.profitLab.string = '-';
        }
    }

    initSoltItem(data){
        let date = new Date(data.time)
        let m = +date.getMinutes();
        let s = +date.getSeconds();
        let mTime = m < 10 ? "0" + m : m;
        let sTime = s < 10 ? "0" + s : s;
        let timeStr1 = `${date.getHours()}:${mTime}:${sTime}`
        let timeStr2 = `${date.getMonth() + 1}/${date.getDate()}`
        this.timeLab.string = `${timeStr2}\n${timeStr1}`;

        if (data.bet || data.bet == 0) {
            this.betLab.string = sUtil.AddCommas(data.bet);
        } else {
            this.betLab.string = '-';
        }
        if (data.profit >= 0) {
            this.profitLab.color = this.colors[0];
        } else {
            this.profitLab.color = this.colors[1];
        }
        let sub = data.profit >= 0 ? '+' : '-';
        sub = data.profit == 0 ? '' : sub
        if (data.profit || data.profit == 0) {
            this.profitLab.string = sub + sUtil.AddCommas(Math.abs(data.profit));
        } else {
            this.profitLab.color = this.colors[2];
            this.profitLab.string = '-';
        }
    }
}

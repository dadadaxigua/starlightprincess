
import { _decorator, Component, Node, Sprite, director, Color } from 'cc';
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = gameRecordImgItem
 * DateTime = Thu Nov 03 2022 13:25:56 GMT+0800 (中国标准时间)
 */
 
@ccclass('gameRecordImgItem')
export class gameRecordImgItem extends Component {
    @property(Sprite)
    sprite: Sprite;

    @property(Node)
    replayBtn: Node;

    id:string;
    gameId:string;
    tableId:string;
    roundId:string;

    onLoad(){
        this.replayBtn.on('click',()=>{
            console.log('id:',this.id)
            director.emit('gameRecordReplay',{
                id:this.id,
                gameId:this.gameId,
                tableId:this.tableId,
                roundId:this.roundId
            })
        },this)
    }

    colors = [
        new Color("343440"),
        new Color("30303c")
    ]
    init(index,gameId,id,recordType,profit,tableId,roundId){
        if(recordType == 'solt'){
            this.sprite.color = this.colors[index%2];
        }else if(recordType == 'poker'){
            if(profit == null){
                this.replayBtn.active = false;
            }else{
                this.replayBtn.active = true;
            }
            this.sprite.node.active = index%2 == 0?true:false;
        }
        this.tableId = tableId;
        this.gameId = gameId;
        this.id = id;
        this.roundId = roundId;
    }

    // update (deltaTime: number) {
    //     // [4]
    // }
}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.4/manual/zh/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.4/manual/zh/scripting/decorator.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.4/manual/zh/scripting/life-cycle-callbacks.html
 */

import { _decorator, Component, Label } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('InternationalizationScript1')
export class InternationalizationScript1 extends Component {
    @property
    public componentName = 'cc.Label';
    @property
    public propertyName = 'string';
    @property
    public jsonName = 'mulTiLN';
    @property
    public module = '';
    @property
    public key = '';

    onLoad () {

    }

    init(){
        let _label = this.node.getComponent(this.componentName);
        if(_label){
            // _label[this.propertyName] = inMgr.GetDataByKey(this.module,this.key);
            _label[this.propertyName] = globalThis.GetDataByKey(this.jsonName,this.module,this.key);
        }
    }

    start () {
        this.init();
    }

}

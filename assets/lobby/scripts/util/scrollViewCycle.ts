import { _decorator, Component, Node, Widget, UITransform, instantiate, v3 } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('ScrollViewCycle')
export class ScrollViewCycle extends Component {
    @property(Node)
    public contentNode: Node;
    @property(Node)
    itemForCopy: Node;
    @property
    public deltaItemName = 5;

    scrollView: any;
    itemCallBack: any;
    viewItemNumCallBack: any;
    dataSum: any;
    itemHeight: any;
    contentStartY: any;
    viewItemNum: any;
    itemNum: any;
    contentStartHeight: any;

    onLoad() {

    }

    start() {
        this.node.on('scrolling', () => {
            this.updateItem();
        }, this);

        this.node.on('scroll-ended', () => {
            if (this.contentNode.position.y >= this.contentStartY) {
                let contentY = this.contentNode.position.y - this.contentStartY;
                let startIndex = Math.floor(contentY / this.itemHeight);
                this.contentNode.getComponent(UITransform).height = this.contentStartHeight + startIndex * this.itemHeight;
            }
        }, this);
    }

    setConfig() {
        let _widget = this.node.getComponentsInChildren(Widget);
        if (_widget) {
            for (let i = 0; i < _widget.length; i++) {
                _widget[i].updateAlignment();
                _widget[i].enabled = false;
            }
        }
        this.scrollView = null;
        this.itemCallBack = null;
        this.viewItemNumCallBack = null;
        this.dataSum = 0;
        this.contentStartY = this.contentNode.position.y;
        let copyItem = null;
        if (this.itemForCopy) {
            copyItem = this.itemForCopy;
        } else {
            copyItem = this.contentNode.children[0];
        }
        this.itemHeight = copyItem.getComponent(UITransform).height;
        this.viewItemNum = (this.node.getChildByName('view').getComponent(UITransform).height / this.itemHeight);
        let creatNum = Math.floor(this.node.getComponent(UITransform).height / this.itemHeight) + this.deltaItemName;
        for (let i = 0; i < creatNum; i++) {
            let item = instantiate(copyItem);
            item.parent = this.contentNode;
            item.position = v3(0, -this.itemHeight / 2 + (-this.itemHeight) * (i + 1), 0);
        }
        this.itemNum = this.contentNode.children.length;
        this.contentStartHeight = (creatNum + 1) * this.itemHeight;
    }

    init(num: any, callBack: any) {
        this.setConfig();
        this.dataSum = num;
        this.itemCallBack = callBack;
        for (let i = 0; i < this.contentNode.children.length; i++) {
            if (i < this.dataSum) {
                this.contentNode.children[i].active = true;
                if (this.itemCallBack) {
                    this.itemCallBack(this.contentNode.children[i], i);
                }
            } else {
                this.contentNode.children[i].active = false;
            }
        }
        this.contentNode.getComponent(UITransform).height = (this.dataSum) * this.itemHeight;
        this.updateItem();
    }

    changeHeight(num: any) {
        this.dataSum = num;
        for (let i = 0; i < this.contentNode.children.length; i++) {
            if (i < this.dataSum) {
                this.contentNode.children[i].active = true;
            } else {
                this.contentNode.children[i].active = false;
            }
        }
        this.contentNode.getComponent(UITransform).height = (this.dataSum) * this.itemHeight;
        console.log('--------------- change height int', this.contentNode.getComponent(UITransform).height, this.dataSum)
        this.updateItem();
    }

    resetContentY() {
        this.contentNode.position = v3(0, this.contentStartY, 0);
        this.updateItem();
    }

    onViewItemNumCallBack(callback: any) {
        this.viewItemNumCallBack = callback;
    }

    updateItem() {
        // console.log('this.contentNode.position.y:'+this.contentNode.position.y);
        if (this.contentNode.position.y >= this.contentStartY) {
            let contentY = this.contentNode.position.y - this.contentStartY;
            let startIndex = Math.floor(contentY / this.itemHeight);
            if (startIndex <= this.dataSum - this.itemNum) {
                this.contentNode.getComponent(UITransform).height = this.contentStartHeight + startIndex * this.itemHeight;
            } else {
                startIndex = this.dataSum - this.itemNum;
                this.contentNode.getComponent(UITransform).height = this.contentStartHeight + (this.dataSum - this.itemNum) * this.itemHeight;
            }
            let startIndexClamp = this.clamp(startIndex, this.contentNode.children.length);
            if (this.viewItemNumCallBack) {
                this.viewItemNumCallBack(startIndex + this.viewItemNum);
            }
            let i = 0;
            let index: any = startIndexClamp;
            let modul = startIndex;
            //    console.log('startIndex:'+startIndex);
            while (i < this.contentNode.children.length) {
                let item = this.contentNode.children[index];
                if (startIndex <= this.dataSum - this.itemNum) {
                    item.position = v3(0, -this.itemHeight / 2 + (-this.itemHeight) * modul, 0);
                }
                if (this.itemCallBack) {
                    this.itemCallBack(item, startIndex + i);
                }
                index++;
                if (index >= this.contentNode.children.length) {
                    index = 0;
                    // item.active = false;
                }
                modul++;
                i++;
            }
        }
    }

    clamp(value: number, length: number) {
        let count = Math.floor(value / length);
        return value - count * length;
    }

}


/**
 * 注意：已把原脚本注释，由于脚本变动过大，转换的时候可能有遗落，需要自行手动转换
 */
/**
 * 引用示例 ： 
 * var  koi = require(${path}/entry);
 * koi.Message.
 * new koi.socketInfo.Package().send()
 * koi.globalDta.user.mySeatIndex
 */

import { _decorator,sys,director} from 'cc';
import _socketInfo from './koi/game/game-entry';
import _globalDta from './global/global-entry';

var obj = {
    /**
     * 获取自1970年来的time ms值 , 与服务端约定所有time ms为2017-10-1以来
     */
    ticks : function(ticks=null){
        if(ticks)
            return new Date(2017,9,1).getTime()+ticks;
        else
            return new Date().getTime() - new Date(2017,9,1).getTime();
    },

    uid : function(){
        return this.ticks() + Math.floor(Math.random()*10000);
    },

    random : function(num0,num,enableEndPoint=false){
        if(!num){
            num = num0;
            num0 = 0;
        }
        if(enableEndPoint == true)
            num ++ ;
        return num0 + Math.floor(Math.random()*(num-num0));
    },

    hypotenuseLength : function(a,b){
        return Math.floor(Math.sqrt(Math.pow(a,2)+Math.pow(b,2)));
    },

    degree : function(a,b){
        return -Math.atan2((b.y||b[1]||0)-(a.y||a[1]||0) ,(b.x||b[0]||0) - (a.x||a[0]||0))*180/Math.PI;
    },

    isOutScreen : function(p,cooType = this.Consts.Coordinate.Cocos){
        var x = p[0]||p.x;
        var y = p[1]||p.y;
        if(cooType == this.Consts.Coordinate.Center){
            x+= 1920/2;
            y+= 1020/2;
        }
        return !(x >= 0 && x <= 1920 && y >=0 && y<=1020);
    },


    /**
     * '-200,872_207,451_33,850_1641,7,5.404_721,583_940,802'
     * =>
     * (6) [[-200,872], [207,451], [33,850], [1641,7,5.404], [721,583], [940,802]]
     */
    getArrFromPointsString : function(str){
        var res;
        if(typeof str == 'string'){
            res = str.split('_');
            for(let i=0;i<res.length;i++){
                res[i] = res[i].split(',');
                res[i][0] = parseFloat(res[i][0]);
                res[i][1] = parseFloat(res[i][1]);
            }
            return res;
        }
        else if(str instanceof Array)
            return str;
    },

    toCenterCoordinate(p){
        if(p[0])
            return [p[0]-1920/2,p[1]-1080/2];
        else if(p.x)
            return {x:p.x-1920/2,y:p.y-1080/2};
    },
   
    // 储存信息           
    //                     参数        localName缓存名称  localInformationName:本地信息名称     data传入的数据      isReplaceAll 是否全部替换      success 成功回调
    /*
       如果只需要替换部分内容isReplaceAll 不谢  或者设置为false即可
    */ 
    /**
     * 储存信息       
     * @param {*} res={}//传入对象参数
     * @param {*} res.localName localStorage缓存名称，默认为null,需要保存本地缓存 传入缓存名称即可
     * @param {*} res.localInformationName 本地待合并信息名称  默认为null,需要保存本地信息 传入本地信息名称即可
     * @param {*} res.data 传入的数据
     * @param {*} res.isReplaceAll 是否全部替换  默认false,只替换传入的部分数值，  =true整个替换已存的对应信息
     * @param {*} res.success :function(),//成功回调   ,需要时调用
     * @example koi.saveData  
     */
    saveData:function(res={itemName:'',localInformationName:null,data:null,isReplaceAll:false,success:function(){}}){
        //获取本地缓存信息   如果不存在,或者isReplaceAll=true全部保存，就直接保存信息，
        // let localStorageItem=this.getLocalStorage({itemName:res.itemName});
        if((!res.localInformationName)||res.isReplaceAll){
            if(res.localInformationName&&!(res.localInformationName=='undefined'&&res.localInformationName=='null')){
                res.localInformationName=res.data;
            }
            sys.localStorage.setItem(res.itemName ,JSON.stringify(res.data));
            if(res.success && typeof res.success=='function'){
                res.success();
            }
            return;
        }
        //循环并替换参数
        for(let i in res.data){
            if(res.localInformationName&&!(res.localInformationName=='undefined'&&res.localInformationName=='null')){
                res.localInformationName[i]=res.data[i];
            }
        }
        console.log('保存信息',res.localInformationName)
        sys.localStorage.setItem(res.itemName ,JSON.stringify(res.localInformationName));
        if(res.success&& typeof res.success=='function'){
            res.success();
        }
    },
    //获取本地信息        itemName
    /**
     * 储存信息       
     * @param {*} res={}//传入对象参数
     * @param {*} res.itemName localStorage缓存名称，
     * @example koi.getLocalStorage  
     */
    getLocalStorage:function(ret={itemName:''}){
        let localItem= sys.localStorage.getItem(ret.itemName);
        if(localItem&&localItem!='undefined'){
            return JSON.parse(localItem);
        }else{
            return null;
        }
        
    },
    //获取当前场景名称 
    /**
     */
    getSceneName(){
        var scene = director.getScene();
        if(scene && scene.isValid){
          return scene.name
        }else{
          return false;
        }
              
    },
};

globalThis.__koi={
    globalDta : _globalDta,
    socketInfo : _socketInfo
};
export default  {
    _obj : obj,
    globalDta : _globalDta,
    socketInfo : _socketInfo
};

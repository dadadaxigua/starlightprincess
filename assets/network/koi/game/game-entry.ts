

import userClient from '../game/protos/user/clientProtos'
import userServer from '../game/protos/user/serverProtos'
import userPush from '../game/protos/user/pushProtos'

import lobbyClient from '../game/protos/lobby/clientProtos.lobby'
import lobbyServer from '../game/protos/lobby/serverProtos.lobby'

import chatClient from '../game/protos/chat/clientProtos'
import chatServer from '../game/protos/chat/serverProtos'

import PackageM from './global/pomelo-package'
import chatPushProtos from './protos/chat/chatPushProtos'

class gameEntry {
    static isObject(item) {
        return (item && typeof item === 'object' && !Array.isArray(item));
    }
    static mergeDeep(target, ...sources) {
        if (!sources.length) return target;
        const source = sources.shift();
        if (gameEntry.isObject(target) && gameEntry.isObject(source)) {
            for (const key in source) {
                if (gameEntry.isObject(source[key])) {
                    if (!target[key]) Object.assign(target, {
                        [key]: {}
                    });
                    gameEntry.mergeDeep(target[key], source[key]);
                } else {
                    Object.assign(target, {
                        [key]: source[key]
                    });
                }
            }
        }
    
        return gameEntry.mergeDeep(target, ...sources);
    }

    static init(){
        // console.log('initinitinitinit');
        // globalThis._clientProtos = gameEntry.mergeDeep(globalThis._clientProtos || {}, authClient);
        // globalThis._serverProtos = gameEntry.mergeDeep(globalThis._serverProtos || {}, authServer);
        // globalThis._pushProtos = gameEntry.mergeDeep(globalThis._pushProtos || {}, authPush);


        globalThis._clientProtos = gameEntry.mergeDeep(globalThis._clientProtos || {}, userClient);
        globalThis._serverProtos = gameEntry.mergeDeep(globalThis._serverProtos || {}, userServer);
        globalThis._pushProtos = gameEntry.mergeDeep(globalThis._pushProtos || {}, userPush);


        globalThis._clientProtos = gameEntry.mergeDeep(globalThis._clientProtos || {}, lobbyClient);
        globalThis._serverProtos = gameEntry.mergeDeep(globalThis._serverProtos || {}, lobbyServer);
        // cc._pushProtos = mergeDeep(cc._pushProtos || {}, require('./protos/lobby/pushProtos.lobby.js'));

        globalThis._clientProtos = gameEntry.mergeDeep(globalThis._clientProtos || {}, chatClient);
        globalThis._serverProtos = gameEntry.mergeDeep(globalThis._serverProtos || {}, chatServer);
        globalThis._pushProtos = gameEntry.mergeDeep(globalThis._pushProtos || {}, chatPushProtos);
        
        // globalThis.decodeIO_encoder = globalThis.protobufjs.Root.fromJSON(globalThis._clientProtos);
        // globalThis.decodeIO_decoder = globalThis.protobufjs.Root.fromJSON(globalThis._serverProtos);
        // globalThis.push_decoder = globalThis.protobufjs.Root.fromJSON(globalThis._pushProtos);
    }
}

gameEntry.init();
globalThis.package = PackageM;

export default {
    Package : PackageM,
    GameUrls:[],//connector urls
    PayUrls:[], //php api
    AuthUrls:[], //user sign in, sign up
    SerUrls:[], //chat upload
    PhpUrils:[]
   
};

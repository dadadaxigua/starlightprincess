
import { _decorator,sys,director} from 'cc';
import queue from '../global/queue';

//it's pomelo state , include gate & connector
const State = {
    Broken: 1, //链接断开
    Connected: 2, //已连接
    Closed: 3,
    // GateDisconnecting: 6,
    // ConnectorConnecting: 7,
    Connecting: 4, //连接中 ，connect gate , and connector return package
    Closing: 5,
    Ready : 6,
};

var koi = {
    ticks: function (ticks = null) {
        if (ticks)
            return new Date(2017, 9, 1).getTime() + ticks;
        else
            return new Date().getTime() - new Date(2017, 9, 1).getTime();
    },

    uid: function () {
        return this.ticks() + Math.floor(Math.random() * 10000);
    },

    getUIdDeviceId(type) {
        let pomeloLoginInfo = JSON.parse(sys.localStorage.getItem('pomeloLoginInfo') || '[]');
        if (pomeloLoginInfo && pomeloLoginInfo.length != 0) {
            var uId = pomeloLoginInfo[type];
            return uId;
        }
    },

    random: function (num0, num, enableEndPoint = false) {
        if (!num) {
            num = num0;
            num0 = 0;
        }
        if (enableEndPoint == true)
            num++;
        return num0 + Math.floor(Math.random() * (num - num0));
    }
}

var instance = null; 
var pomelo = globalThis.pomelo || globalThis.__v_pomelo;

var iterateIndex = -1;
var __counter = 0;
class Socket {
    timeOut! : any;
    connParas! : any;
    status! : any;
    gateUri! : any;
    disconnectCb! : any;
    enable! : any;
    netState! : any;
    netStateTimeout! : any;
    pomelo! : any;

    constructor() {
        // this.addr = koi.gameUrls[koi.random(gameUrls.length)];
        this.timeOut = null;
        this.connParas = null;
        this.gateUri = null;
        this.disconnectCb = null;
        if(!pomelo){
            pomelo = globalThis.pomelo || globalThis.__v_pomelo;
        }
        // pomelo.Emitter(null);
        pomelo.on('error', this.instanceError);
        pomelo.on('close', this.instanceClose);
        pomelo.on('message', this.instanceMessage);
        pomelo.on('kick', this.instanceKick);
        if (!instance) {
            instance = this;
            // this.connect();
        }

        this.enable = true;

        this.netState = null;
        this.netStateTimeout = null;

        director.on('NetState', (e) => {
            let _netState = instance.netState;
            instance.netState = e.detail.key;
            console.log(new Date().toLocaleTimeString('en-US'), 'nettestpp 网络', _netState, " -> ", instance.netState, instance.connector && instance.connector.host, (_netState != instance.netState && _netState));
            switch (instance.netState) {
                case '1':
                case '2':
                    this.enable = true;
                    if (_netState) {
                        instance.status = State.Broken;
                        instance.disconnect();
                    }
                    break;
                case '3':
                    iterateIndex = -1;
                    this.enable = false;
                    console.log(new Date().toLocaleTimeString('en-US'), 'nettestpp 无网络');
                    break;
            }
        });

        director.on('msgClear', (e) => {
            console.log('msgClear');
            if(this.timeOut != null){
                clearTimeout(this.timeOut);
            }
        });
 
        director.on('uri_pomelo_done', (e) => {
            this.connParas = {
                access_token : e.access_token,
                device_id : e.device_id,
                hosts:e.hosts
            }
            this.enable = true;
            iterateIndex = -1;
            instance.status = State.Ready;
            
            if (!instance.connector || instance.connector.host == '127.0.0.1') {
                instance.connect();
            } else {
                // instance.disconnect();
            }
        })
        director.on('closeConnect', (e) => {
            instance.closeConnect();
        })
        return instance;
    }

    connect() {
        instance.disconnectCb = null;
        if (!this.enable) {
            return;
        }
        if ([State.Closed, State.Connecting].indexOf(instance.status) >= 0) {
            return;
        }
        instance.status = State.Connecting;
        instance._hiConnector();
    }

    instanceError(evt) {
        pomelo.disconnect();
        instance.status = State.Broken;
        console.log(new Date().toLocaleTimeString('en-US'), 'nettestpp instanceError @status=', instance.status, evt ? evt.code : '');
        director.emit('instancesocket_closed');
    }

    instanceClose(evt) {
        console.log(new Date().toLocaleTimeString('en-US'), 'nettestpp instanceClose', instance.status);
        instance.disconnectCb && instance.disconnectCb();
        instance.disconnectCb = null;
        //主动disconnect不处理
        if ([State.Closed].indexOf(instance.status) < 0) {
            instance.status = State.Broken;
            setTimeout(()=>{
                instance.connect();
            },10)
        }
    }

    instanceKick(data) {
        console.log(new Date().toLocaleTimeString('en-US'), 'nettestpp instanceKick @status=');
        pomelo.disconnect();
        iterateIndex = -1;
        instance.status = State.Closed;
        if(data.reason == "-1000"){
            //logout,no need socket_kicked
            return;
        }
        director.emit('socket_kicked', data);
    }

    closeConnect(){
        pomelo.disconnect();
        iterateIndex = -1;
        instance.status = State.Closed;
    }

    disconnect() {
        if (pomelo.disconnect() == -1) {
            iterateIndex = -1;
            instance.connect();
        }
    }

    instanceMessage(data) {
        queue.buffer.messageIn2.push(data);
    }

    send(path, data, cb) {
        console.log('SocketSendPath', path);
        switch (instance.status) {
            case State.Connected:
                console.log(new Date().toLocaleTimeString('en-US'), `~`.repeat(50), 'nettestpp send @State.Connected', path);
                __counter = 0;
                pomelo.request(path, data, cb);
                return;
            default:
                // console.log(`~`.repeat(100));
                // console.log(`~`.repeat(100), 'nettestpp timeout after ', 500 + (__counter < 10 ? __counter : 10) * 300, 'ms', instance.status, path);
                // this.timeOut = setTimeout(() => {
                //     instance.send(path, data, cb);
                // }, 500 + (__counter++ < 10 ? __counter : 10) * 300);
                // return;
        }
    }

    socketIsConnect(){
        return instance.status == State.Connected;
    }

    _hiConnector() {
        const gameUrls = this.connParas.hosts;
        if (iterateIndex == -1) {
            iterateIndex = 0;
        } else
            iterateIndex++;

        if (iterateIndex > gameUrls.length * 3) {
            iterateIndex = -1;
            this.enable = false;
            director.emit('loadconfig.promptRefreash');
            return;
        }

        var addr = gameUrls[iterateIndex % gameUrls.length];
        if (typeof addr == 'string') {
            let m = addr.match(/(\d+.\d+.\d+.\d+):(\d+)/);
            if (!m){
                let m2 = addr.split(':');
                if (!m2)
                    return;
                addr = {
                    host: '//'+m2[0],
                    port: m2[1]
                };
            }else{
                addr = {
                    host: '//'+m[1],
                    port: m[2]
                };
            }
        }
        // if(addr.host == '127.0.0.1'){
        //     addr.port = globalThis.xdnPortsAlias[addr.port];
        // }

        console.log(new Date().toLocaleTimeString('en-US'), 'nettestpp _hiConnector', iterateIndex, addr.host, addr.port, gameUrls);
        instance.gateUri = `wss://${addr.host}:${addr.port}`;
        pomelo.init({
            host: addr.host,
            port: addr.port,
            access_token:this.connParas.access_token,
            device_id:this.connParas.device_id,
        }, function () {
            instance.status = State.Connected;
            director.emit('socketconnected');
            // pomelo.request("connector.entryHandler.entry", {}, function (err, res) {
            //     iterateIndex = -1;
            //     // console.log(new Date().toLocaleTimeString('en-US',{ hour12: false }),'nettestpp hiconnector success! sid = ',res.data.sid);
            //     instance.status = State.Connected;
            //     cc.director.emit('instancesocket_connected');

            // });
        });



    }


}
export default Socket;

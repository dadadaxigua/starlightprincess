import Socket from './pomelo-socket';
import queue from './queue';



function _checkMessageIn2(){
	if(queue.buffer.messageIn2.length > 0){
		var arr = queue.buffer.messageIn2.splice(0);
		var err ,res;
		for(let i = 0; i<arr.length; i++){
			res = arr[i];
			for(let key in  __packFunc[res.route]){
				// console.log('queue.buffer.messageIn3',JSON.stringify(res),__packFunc[res.route][key],'__packFunc  dating','key',key,'res.route',res.route )
				err = res.code == 0 ? null :  {code : res.code ,message : res.message};
				__packFunc[res.route][key](err,res);
			}
		}
	}
}

let __packFunc = {};
let __packListenInterval = setInterval(_checkMessageIn2,2);
// console.log('nettestpp __packListenInterval=',__packListenInterval);

class Package{
	path! : any;
	req! : any;

	constructor(path ,req){
		this.path = path;
        this.req = req;
	}

	/**
	 * 注册包的监听函数
	 * 
	 * @static
	 * @param {any} cmdKind 
	 * @param {any} typeObj 
	 * @param {any} cb 
	 * 
	 * @memberof Package
	 */
	static init(){
		if(globalThis.inSocket == null){
			globalThis.inSocket = new Socket();
		}
	}

	static addPackListener(path,cb){
		__packFunc[path] = __packFunc[path] || {};
		// console.log('__packFunc addPackListener',JSON.stringify(__packFunc),'__packFunc addPackListener path',path)
		var random = path + '-' + new Date().getTime() + '-' + (Math.random()*100000);
		__packFunc[path][random] = cb;
		return [path,random];
	}
	static addPackListenerArr(that,listener){
		that.__ListenerUIDs = that.__ListenerUIDs || [];
		// console.log(JSON.stringify(listener),that.name,'__packFunc addPackListenerArr')
		for(let i=0;i<listener.length;i++){
			let path = listener[i].path;
			// console.log(path,that.name,'__packFunc addPackListenerArr')
			that.__ListenerUIDs.push(Package.addPackListener(
				path,
				(err,res)=>{
					if(typeof that[listener[i].filter] == 'function'){
						that[listener[i].filter](err,res,listener[i].cb);
					}
					else if(typeof that[listener[i].cb] == 'function')
                    	that[listener[i].cb](err,res);
                }
			));
		}
		// console.log('listener  __packFunc',JSON.stringify(listener))
	}
	static removePackListenerArr(that){
		var obj;
		if(that.__ListenerUIDs && that.__ListenerUIDs.length > 0 ){
			for(let i =0;i<that.__ListenerUIDs.length;i++){
				obj = that.__ListenerUIDs[i];
				delete __packFunc[obj[0]][obj[1]];
			}
				
		}
	}
	static removePackListener(path,func){
		if(func && func.__UID){
			delete __packFunc[path][func.__UID];
		}
	}

	send(cb){
		if(globalThis.inSocket){
        	globalThis.inSocket.send(this.path,this.req,cb);
		}
    }
}

export default Package;

export default{
  "nested": {
    "chat": {
      "nested": {
        "chatHandler": {
          "nested": {
            "chatOneByOne": {
              "fields": {
                "uid": {
                  "type": "string",
                  "id": 1
                },
                "type": {
                  "type": "int32",
                  "id": 2
                },
                "msg": {
                  "type": "string",
                  "id": 3
                }
              }
            },
            "chatDesk": {
              "fields": {
                "type": {
                  "type": "int32",
                  "id": 1
                },
                "id": {
                  "type": "int32",
                  "id": 2
                }
              }
            },
            "get1by1Msg": {
              "fields": {
                "type": {
                  "type": "int32",
                  "id": 1
                },
                "id": {
                  "type": "int32",
                  "id": 2
                }
              }
            },
            "sendChatMsg": {
              "fields": {
                "target_uid": {
                  "type": "string",
                  "id": 1
                },
                "msg": {
                  "type": "string",
                  "id": 2
                },
                "type": {
                  "type": "string",
                  "id": 3
                }
              }
            },
            "getAllUnreadMsg": {
              "fields": {}
            }
          }
        }
      }
    }
  }
}
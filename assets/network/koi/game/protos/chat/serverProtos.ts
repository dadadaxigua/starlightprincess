export default{
  "nested": {
    "chat": {
      "nested": {
        "chatHandler": {
          "nested": {
            "chatOneByOne": {
              "fields": {
                "code": {
                  "type": "int32",
                  "id": 1
                },
                "data": {
                  "type": "Data",
                  "id": 2
                },
                "message": {
                  "type": "string",
                  "id": 3
                }
              },
              "nested": {
                "Data": {
                  "fields": {}
                }
              }
            },
            "chatDesk": {
              "fields": {
                "code": {
                  "type": "int32",
                  "id": 1
                },
                "data": {
                  "type": "Data",
                  "id": 2
                },
                "message": {
                  "type": "string",
                  "id": 3
                }
              },
              "nested": {
                "Data": {
                  "fields": {}
                }
              }
            },
            "get1by1Msg": {
              "fields": {
                "code": {
                  "type": "int32",
                  "id": 1
                },
                "data": {
                  "type": "Data",
                  "id": 2
                },
                "message": {
                  "type": "string",
                  "id": 3
                }
              },
              "nested": {
                "Data": {
                  "fields": {
                    "msgArr": {
                      "type": "string",
                      "id": 1
                    }
                  }
                }
              }
            },
            "sendChatMsg": {
              "fields": {
                "code": {
                  "type": "int32",
                  "id": 1
                },
                "data": {
                  "type": "Data",
                  "id": 2
                },
                "message": {
                  "type": "string",
                  "id": 3
                }
              },
              "nested": {
                "Data": {
                  "fields": {}
                }
              }
            },
            "getAllUnreadMsg": {
              "fields": {
                "code": {
                  "type": "int32",
                  "id": 1
                },
                "data": {
                  "type": "Data",
                  "id": 2
                },
                "message": {
                  "type": "string",
                  "id": 3
                }
              },
              "nested": {
                "Data": {
                  "fields": {
                    "unreadMsg": {
                      "type": "string",
                      "id": 1
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}
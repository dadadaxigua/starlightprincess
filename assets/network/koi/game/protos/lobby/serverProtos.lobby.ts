/**
 * 注意：已把原脚本注释，由于脚本变动过大，转换的时候可能有遗落，需要自行手动转换
 */
export default{
  "nested": {
    "connector": {
      "nested": {
        "lobbyHandler": {
          "nested": {
            "queueEnter": {
              "fields": {
                "code": {
                  "type": "int32",
                  "id": 1
                },
                "data": {
                  "type": "Data",
                  "id": 2
                },
                "message": {
                  "type": "string",
                  "id": 3
                }
              },
              "nested": {
                "Data": {
                  "fields": {}
                }
              }
            },
            "leave": {
              "fields": {
                "code": {
                  "type": "int32",
                  "id": 1
                },
                "data": {
                  "type": "Data",
                  "id": 2
                },
                "message": {
                  "type": "string",
                  "id": 3
                }
              },
              "nested": {
                "Data": {
                  "fields": {}
                }
              }
            },
            "getServerTime": {
              "fields": {
                "code": {
                  "type": "int32",
                  "id": 1
                },
                "data": {
                  "type": "Data",
                  "id": 2
                },
                "message": {
                  "type": "string",
                  "id": 3
                }
              },
              "nested": {
                "Data": {
                  "fields": {
                    "serverTime": {
                      "type": "double",
                      "id": 1
                    }
                  }
                }
              }
            },
            "oneDesk": {
              "fields": {
                "deskId": {
                  "type": "string",
                  "id": 1
                }
              }
            },
            "oneRoom": {
              "fields": {
                "roomId": {
                  "type": "int32",
                  "id": 1
                },
                "type": {
                  "type": "int32",
                  "id": 2
                },
                "queueType": {
                  "type": "int32",
                  "id": 3
                },
                "name": {
                  "type": "string",
                  "id": 4
                },
                "icon": {
                  "type": "int32",
                  "id": 5
                },
                "roomScore": {
                  "type": "int32",
                  "id": 6
                },
                "deskList": {
                  "rule": "repeated",
                  "type": "oneDesk",
                  "id": 7
                }
              }
            },
            "getRoomList": {
              "fields": {
                "code": {
                  "type": "int32",
                  "id": 1
                },
                "data": {
                  "rule": "repeated",
                  "type": "oneRoom",
                  "id": 2
                },
                "message": {
                  "type": "string",
                  "id": 3
                }
              }
            },
            "getDeskList": {
              "fields": {
                "code": {
                  "type": "int32",
                  "id": 1
                },
                "data": {
                  "type": "Data",
                  "id": 2
                },
                "message": {
                  "type": "string",
                  "id": 3
                }
              }
            },
            "checkNode": {
              "fields": {
                "enter": {
                  "type": "bool",
                  "id": 1
                },
                "gameId": {
                  "type": "int32",
                  "id": 2
                },
                "roomId": {
                  "type": "int32",
                  "id": 3
                },
                "deskId": {
                  "type": "string",
                  "id": 4
                }
              }
            }
          }
        }
      }
    }
  }
}

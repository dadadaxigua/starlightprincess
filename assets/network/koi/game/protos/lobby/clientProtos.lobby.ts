/**
 * 注意：已把原脚本注释，由于脚本变动过大，转换的时候可能有遗落，需要自行手动转换
 */
export default {
  "nested": {
    "connector": {
      "nested": {
        "lobbyHandler": {
          "nested": {
            "queueEnter": {
              "fields": {
                "gameId": {
                  "type": "int32",
                  "id": 1
                },
                "roomList": {
                  "rule": "repeated",
                  "type": "int32",
                  "id": 2
                },
                "deskId": {
                  "type": "string",
                  "id": 3
                },
                "queueType": {
                  "type": "int32",
                  "id": 4
                },
                "queueKey": {
                  "type": "string",
                  "id": 5
                }
              }
            },
            "leave": {
              "fields": {}
            },
            "getServerTime": {
              "fields": {}
            },
            "getRoomList": {
              "fields": {
                "gameId": {
                  "type": "int32",
                  "id": 1
                },
                "type": {
                  "type": "int32",
                  "id": 2
                },
                "withDesk": {
                  "type": "bool",
                  "id": 3
                }
              }
            },
            "getDeskList": {
              "fields": {
                "gameId": {
                  "type": "int32",
                  "id": 1
                },
                "roomId": {
                  "type": "int32",
                  "id": 2
                }
              }
            },
            "checkNode": {
              "fields": {
                "gameId": {
                  "type": "int32",
                  "id": 1
                }
              }
            }
          }
        }
      }
    }
  }
}

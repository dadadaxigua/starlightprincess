/**
 * 注意：已把原脚本注释，由于脚本变动过大，转换的时候可能有遗落，需要自行手动转换
 */
export default {
  "nested": {
    "lobby": {
      "nested": {
        "msg": {
          "nested": {
            "error": {
              "fields": {
                "code": {
                  "type": "int32",
                  "id": 1
                },
                "data": {
                  "type": "Data",
                  "id": 2
                },
                "message": {
                  "type": "string",
                  "id": 3
                }
              },
              "nested": {
                "Data": {
                  "fields": {}
                }
              }
            },
            "oneDesk": {
              "fields": {
                "deskId": {
                  "type": "string",
                  "id": 1
                }
              }
            },
            "oneRoom": {
              "fields": {
                "roomId": {
                  "type": "int32",
                  "id": 1
                },
                "type": {
                  "type": "int32",
                  "id": 2
                },
                "queueType": {
                  "type": "int32",
                  "id": 3
                },
                "name": {
                  "type": "string",
                  "id": 4
                },
                "icon": {
                  "type": "int32",
                  "id": 5
                },
                "roomScore": {
                  "type": "int32",
                  "id": 6
                },
                "enter": {
                  "rule": "repeated",
                  "type": "int32",
                  "id": 7
                },
                "gameOption": {
                  "type": "int32",
                  "id": 8
                },
                "cost": {
                  "type": "int32",
                  "id": 9
                },
                "costCurrencyType": {
                  "type": "int32",
                  "id": 10
                },
                "deskList": {
                  "rule": "repeated",
                  "type": "oneDesk",
                  "id": 11
                },
                "sort": {
                  "type": "int32",
                  "id": 12
                },
                "playerCount": {
                  "type": "int32",
                  "id": 13
                }
              }
            },
            "Lock": {
              "fields": {
                "uid": {
                  "type": "int64",
                  "id": 1
                },
                "gameId": {
                  "type": "int32",
                  "id": 2
                },
                "roomId": {
                  "type": "int32",
                  "id": 3
                },
                "deskId": {
                  "type": "string",
                  "id": 4
                }
              }
            },
            "notice": {
              "fields": {
                "code": {
                  "type": "int32",
                  "id": 1
                },
                "action": {
                  "type": "string",
                  "id": 2
                },
                "msg": {
                  "type": "string",
                  "id": 3
                },
                "data": {
                  "type": "Data",
                  "id": 4
                },
                "serverTime": {
                  "type": "double",
                  "id": 5
                }
              },
              "nested": {
                "Data": {
                  "fields": {
                    "roomList": {
                      "rule": "repeated",
                      "type": "oneRoom",
                      "id": 1
                    },
                    "deskList": {
                      "rule": "repeated",
                      "type": "oneDesk",
                      "id": 2
                    },
                    "balance": {
                      "type": "double",
                      "id": 3
                    },
                    "lockInfo": {
                      "type": "Lock",
                      "id": 4
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}

export default{
  "nested": {
    "user": {
      "nested": {
        "msg": {
          "nested": {
            "itemChange": {
              "fields": {
                "item_arr": {
                  "rule": "repeated",
                  "type": "ItemArr",
                  "id": 1
                }
              },
              "nested": {
                "ItemArr": {
                  "fields": {
                    "item_id": {
                      "type": "int32",
                      "id": 1
                    },
                    "change": {
                      "type": "int32",
                      "id": 2
                    },
                    "reason": {
                      "type": "string",
                      "id": 3
                    },
                    "remarks": {
                      "type": "string",
                      "id": 4
                    },
                    "num": {
                      "type": "int32",
                      "id": 5
                    }
                  }
                }
              }
            },
            "propChange": {
              "fields": {
                "prop_arr": {
                  "rule": "repeated",
                  "type": "propObj",
                  "id": 1
                }
              },
              "nested": {
                "propObj": {
                  "fields": {
                    "type": {
                      "type": "string",
                      "id": 1
                    },
                    "change": {
                      "type": "double",
                      "id": 2
                    },
                    "now_value": {
                      "type": "double",
                      "id": 3
                    }
                  }
                }
              }
            },
            "coinChange": {
              "fields": {
                "coin": {
                  "type": "double",
                  "id": 1
                },
                "change": {
                  "type": "double",
                  "id": 2
                },
                "reason": {
                  "type": "string",
                  "id": 3
                },
                "category": {
                  "type": "string",
                  "id": 4
                }
              }
            },
            "awardNotice": {
              "fields": {
                "award_id": {
                  "type": "string",
                  "id": 1
                },
                "type": {
                  "type": "string",
                  "id": 2
                }
              }
            },
            "payInfo": {
              "fields": {
                "amount": {
                  "type": "double",
                  "id": 1
                },
                "code": {
                  "type": "int32",
                  "id": 2
                },
                "order_id": {
                  "type": "string",
                  "id": 3
                },
                "pay_url": {
                  "type": "string",
                  "id": 4
                },
                "time_stamp": {
                  "type": "int64",
                  "id": 5
                }
              }
            }
          }
        }
      }
    }
  }
}
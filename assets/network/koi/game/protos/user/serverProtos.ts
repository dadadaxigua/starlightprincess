export default {
  "nested": {
    "connector": {
      "nested": {
        "authHandler": {
          "nested": {
            "auth": {
              "oneofs": {
                "_data": {
                  "oneof": [
                    "data"
                  ]
                },
                "_message": {
                  "oneof": [
                    "message"
                  ]
                }
              },
              "fields": {
                "code": {
                  "type": "int32",
                  "id": 1
                },
                "data": {
                  "type": "Data",
                  "id": 2,
                  "options": {
                    "proto3_optional": true
                  }
                },
                "message": {
                  "type": "string",
                  "id": 3,
                  "options": {
                    "proto3_optional": true
                  }
                }
              },
              "nested": {
                "Data": {
                  "fields": {}
                }
              }
            }
          }
        }
      }
    },
    "user": {
      "nested": {
        "awardHandler": {
          "nested": {
            "exchange": {
              "fields": {
                "code": {
                  "type": "int32",
                  "id": 1
                },
                "data": {
                  "type": "Data",
                  "id": 2
                },
                "message": {
                  "type": "string",
                  "id": 3
                }
              },
              "nested": {
                "Data": {
                  "fields": {
                    "now_draw": {
                      "type": "int32",
                      "id": 1
                    },
                    "reward_id": {
                      "type": "int32",
                      "id": 2
                    }
                  }
                }
              }
            },
            "gainAward": {
              "fields": {
                "code": {
                  "type": "int32",
                  "id": 1
                },
                "data": {
                  "type": "Data",
                  "id": 2
                },
                "message": {
                  "type": "string",
                  "id": 3
                }
              },
              "nested": {
                "Data": {
                  "fields": {}
                }
              }
            }
          }
        },
        "itemHandler": {
          "nested": {
            "getItemList": {
              "fields": {
                "code": {
                  "type": "int32",
                  "id": 1
                },
                "data": {
                  "type": "Data",
                  "id": 2
                },
                "message": {
                  "type": "string",
                  "id": 3
                }
              },
              "nested": {
                "ItemData": {
                  "fields": {
                    "item_id": {
                      "type": "int32",
                      "id": 1
                    },
                    "num": {
                      "type": "int32",
                      "id": 2
                    }
                  }
                },
                "Data": {
                  "fields": {
                    "list": {
                      "rule": "repeated",
                      "type": "ItemData",
                      "id": 1
                    }
                  }
                }
              }
            },
            "transferItem": {
              "fields": {
                "code": {
                  "type": "int32",
                  "id": 1
                },
                "data": {
                  "type": "Data",
                  "id": 2
                },
                "message": {
                  "type": "string",
                  "id": 3
                }
              },
              "nested": {
                "Data": {
                  "fields": {}
                }
              }
            }
          }
        },
        "relationHandler": {
          "nested": {
            "addFriend": {
              "fields": {
                "code": {
                  "type": "int32",
                  "id": 1
                },
                "data": {
                  "type": "Data",
                  "id": 2
                },
                "message": {
                  "type": "string",
                  "id": 3
                }
              },
              "nested": {
                "Data": {
                  "fields": {}
                }
              }
            },
            "applyDispose": {
              "fields": {
                "code": {
                  "type": "int32",
                  "id": 1
                },
                "data": {
                  "type": "Data",
                  "id": 2
                },
                "message": {
                  "type": "string",
                  "id": 3
                }
              },
              "nested": {
                "Data": {
                  "fields": {}
                }
              }
            },
            "deleteFriend": {
              "fields": {
                "code": {
                  "type": "int32",
                  "id": 1
                },
                "data": {
                  "type": "Data",
                  "id": 2
                },
                "message": {
                  "type": "string",
                  "id": 3
                }
              },
              "nested": {
                "Data": {
                  "fields": {}
                }
              }
            },
            "changeRemark": {
              "fields": {
                "code": {
                  "type": "int32",
                  "id": 1
                },
                "data": {
                  "type": "Data",
                  "id": 2
                },
                "message": {
                  "type": "string",
                  "id": 3
                }
              },
              "nested": {
                "Data": {
                  "fields": {}
                }
              }
            }
          }
        },
        "taskHandler": {
          "nested": {
            "gainTaskReward": {
              "fields": {
                "code": {
                  "type": "int32",
                  "id": 1
                },
                "data": {
                  "type": "Data",
                  "id": 2
                },
                "message": {
                  "type": "string",
                  "id": 3
                }
              },
              "nested": {
                "Data": {
                  "fields": {}
                }
              }
            },
            "groupList": {
              "fields": {
                "code": {
                  "type": "int32",
                  "id": 1
                },
                "data": {
                  "rule": "repeated",
                  "type": "ListData",
                  "id": 2
                },
                "message": {
                  "type": "string",
                  "id": 3
                }
              },
              "nested": {
                "ListData": {
                  "fields": {
                    "task_group_id": {
                      "type": "int32",
                      "id": 1
                    },
                    "state": {
                      "type": "int32",
                      "id": 2
                    }
                  }
                }
              }
            },
            "groupInfo": {
              "fields": {
                "code": {
                  "type": "int32",
                  "id": 1
                },
                "data": {
                  "type": "Data",
                  "id": 2
                },
                "message": {
                  "type": "string",
                  "id": 3
                }
              },
              "nested": {
                "TaskInfoData": {
                  "fields": {
                    "award_id": {
                      "type": "string",
                      "id": 1
                    },
                    "state": {
                      "type": "int32",
                      "id": 2
                    },
                    "task_unique_id": {
                      "type": "string",
                      "id": 3
                    }
                  }
                },
                "Data": {
                  "fields": {
                    "group_score": {
                      "type": "int32",
                      "id": 1
                    },
                    "task_info_arr": {
                      "rule": "repeated",
                      "type": "TaskInfoData",
                      "id": 2
                    },
                    "progress": {
                      "type": "int32",
                      "id": 3
                    }
                  }
                }
              }
            }
          }
        },
        "userHandler": {
          "nested": {
            "getUserInfo": {
              "fields": {
                "code": {
                  "type": "int32",
                  "id": 1
                },
                "data": {
                  "type": "Data",
                  "id": 2
                },
                "message": {
                  "type": "string",
                  "id": 3
                }
              },
              "nested": {
                "Data": {
                  "fields": {
                    "uid": {
                      "type": "string",
                      "id": 1
                    },
                    "channel_code": {
                      "type": "string",
                      "id": 2
                    },
                    "invite_uid": {
                      "type": "string",
                      "id": 3
                    },
                    "nick_name": {
                      "type": "string",
                      "id": 4
                    },
                    "name_modify_mark": {
                      "type": "int32",
                      "id": 5
                    },
                    "avatar": {
                      "type": "string",
                      "id": 6
                    },
                    "region": {
                      "type": "string",
                      "id": 7
                    },
                    "vip_level": {
                      "type": "int32",
                      "id": 8
                    },
                    "vip_exp": {
                      "type": "double",
                      "id": 9
                    },
                    "max_bet": {
                      "type": "int32",
                      "id": 10
                    },
                    "coin": {
                      "type": "double",
                      "id": 11
                    },
                    "slots_count": {
                      "type": "int32",
                      "id": 12
                    },
                    "max_rate": {
                      "type": "double",
                      "id": 13
                    },
                    "income": {
                      "type": "double",
                      "id": 14
                    },
                    "is_new_msg": {
                      "type": "int32",
                      "id": 15
                    },
                    "state": {
                      "type": "int32",
                      "id": 16
                    },
                    "bet_required": {
                      "type": "double",
                      "id": 17
                    },
                    "bet_start": {
                      "type": "double",
                      "id": 18
                    },
                    "platform": {
                      "type": "string",
                      "id": 19
                    },
                    "his_input": {
                      "type": "double",
                      "id": 20
                    },
                    "bet_required_reason": {
                      "type": "string",
                      "id": 21
                    },
                    "bet_required_start_time": {
                      "type": "int64",
                      "id": 22
                    },
                    "introduce": {
                      "type": "string",
                      "id": 23
                    },
                    "team_achieve": {
                      "type": "double",
                      "id": 24
                    },
                    "pay_set": {
                      "type": "string",
                      "id": 25
                    },
                    "unique_time": {
                      "type": "int64",
                      "id": 26
                    },
                    "register_coin_qua": {
                      "type": "int32",
                      "id": 27
                    },
                    "pay_mode": {
                      "type": "int32",
                      "id": 28
                    },
                    "desk_id": {
                      "type": "string",
                      "id": 29
                    },
                    "table_id": {
                      "type": "string",
                      "id": 30
                    },
                    "create_time": {
                      "type": "int64",
                      "id": 31
                    },
                    "last_game_time": {
                      "type": "int64",
                      "id": 32
                    },
                    "coin_in_first": {
                      "type": "int64",
                      "id": 33
                    },
                    "role": {
                      "type": "int32",
                      "id": 34
                    },
                    "has_award": {
                      "type": "int32",
                      "id": 35
                    }
                  }
                }
              }
            },
            "getPlayerInfo": {
              "fields": {
                "code": {
                  "type": "int32",
                  "id": 1
                },
                "data": {
                  "type": "Data",
                  "id": 2
                },
                "message": {
                  "type": "string",
                  "id": 3
                }
              },
              "nested": {
                "Data": {
                  "fields": {
                    "nick_name": {
                      "type": "string",
                      "id": 1
                    },
                    "avatar": {
                      "type": "string",
                      "id": 2
                    },
                    "region": {
                      "type": "string",
                      "id": 3
                    },
                    "vip_level": {
                      "type": "int32",
                      "id": 4
                    },
                    "coin": {
                      "type": "double",
                      "id": 5
                    },
                    "slots_count": {
                      "type": "int32",
                      "id": 6
                    },
                    "max_rate": {
                      "type": "double",
                      "id": 7
                    },
                    "income": {
                      "type": "double",
                      "id": 8
                    },
                    "special_num": {
                      "type": "int32",
                      "id": 9
                    },
                    "state": {
                      "type": "int32",
                      "id": 10
                    },
                    "team_num": {
                      "type": "int32",
                      "id": 11
                    },
                    "direct_num": {
                      "type": "int32",
                      "id": 12
                    },
                    "award_commission": {
                      "type": "double",
                      "id": 13
                    },
                    "team_achieve": {
                      "type": "double",
                      "id": 14
                    },
                    "interact_type": {
                      "type": "int32",
                      "id": 15
                    },
                    "uid": {
                      "type": "string",
                      "id": 16
                    },
                    "introduce": {
                      "type": "string",
                      "id": 17
                    },
                    "create_time": {
                      "type": "int64",
                      "id": 18
                    },
                    "last_game_time": {
                      "type": "int64",
                      "id": 19
                    }
                  }
                }
              }
            },
            "getUserInfoLight": {
              "fields": {
                "code": {
                  "type": "int32",
                  "id": 1
                },
                "data": {
                  "type": "Data",
                  "id": 2
                },
                "message": {
                  "type": "string",
                  "id": 3
                }
              },
              "nested": {
                "Data": {
                  "fields": {
                    "coin": {
                      "type": "double",
                      "id": 1
                    },
                    "yesterday_commission": {
                      "type": "double",
                      "id": 2
                    },
                    "lock_coin": {
                      "type": "double",
                      "id": 3
                    }
                  }
                }
              }
            },
            "getOrderRecord": {
              "fields": {
                "code": {
                  "type": "int32",
                  "id": 1
                },
                "data": {
                  "type": "Data",
                  "id": 2
                },
                "message": {
                  "type": "string",
                  "id": 3
                }
              },
              "nested": {
                "Data": {
                  "fields": {
                    "list": {
                      "rule": "repeated",
                      "type": "Order",
                      "id": 1
                    },
                    "time_stamp": {
                      "type": "int64",
                      "id": 2
                    },
                    "is_new": {
                      "type": "bool",
                      "id": 3
                    }
                  },
                  "nested": {
                    "Order": {
                      "fields": {
                        "uid": {
                          "type": "string",
                          "id": 1
                        },
                        "coin_amount": {
                          "type": "double",
                          "id": 2
                        },
                        "create_time": {
                          "type": "int64",
                          "id": 3
                        },
                        "order_id": {
                          "type": "string",
                          "id": 4
                        },
                        "status": {
                          "type": "int32",
                          "id": 5
                        },
                        "currency_amount": {
                          "type": "double",
                          "id": 6
                        }
                      }
                    }
                  }
                }
              }
            },
            "userApply": {
              "fields": {
                "code": {
                  "type": "int32",
                  "id": 1
                },
                "data": {
                  "type": "Data",
                  "id": 2
                },
                "message": {
                  "type": "string",
                  "id": 3
                }
              },
              "nested": {
                "Data": {
                  "fields": {}
                }
              }
            },
            "modifyInfo": {
              "fields": {
                "code": {
                  "type": "int32",
                  "id": 1
                },
                "data": {
                  "type": "Data",
                  "id": 2
                },
                "message": {
                  "type": "string",
                  "id": 3
                }
              },
              "nested": {
                "Data": {
                  "fields": {}
                }
              }
            },
            "paySubmit": {
              "fields": {
                "code": {
                  "type": "int32",
                  "id": 1
                },
                "data": {
                  "type": "Data",
                  "id": 2
                },
                "message": {
                  "type": "string",
                  "id": 3
                }
              },
              "nested": {
                "Data": {
                  "fields": {
                    "amount": {
                      "type": "double",
                      "id": 1
                    },
                    "order_id": {
                      "type": "string",
                      "id": 2
                    },
                    "pay_url": {
                      "type": "string",
                      "id": 3
                    }
                  }
                }
              }
            },
            "gainAward": {
              "fields": {
                "code": {
                  "type": "int32",
                  "id": 1
                },
                "data": {
                  "type": "Data",
                  "id": 2
                },
                "message": {
                  "type": "string",
                  "id": 3
                }
              },
              "nested": {
                "Data": {
                  "fields": {
                    "coin_amount": {
                      "type": "double",
                      "id": 1
                    }
                  }
                }
              }
            },
            "transferItem": {
              "fields": {
                "code": {
                  "type": "int32",
                  "id": 1
                },
                "data": {
                  "type": "Data",
                  "id": 2
                },
                "message": {
                  "type": "string",
                  "id": 3
                }
              },
              "nested": {
                "Data": {
                  "fields": {
                    "item_id": {
                      "type": "int32",
                      "id": 1
                    },
                    "balance": {
                      "type": "int32",
                      "id": 2
                    },
                    "tax": {
                      "type": "double",
                      "id": 3
                    }
                  }
                }
              }
            }
          }
        },
        "payHandler": {
          "nested": {
            "seniorPaySubmit": {
              "fields": {
                "code": {
                  "type": "int32",
                  "id": 1
                },
                "data": {
                  "type": "Data",
                  "id": 2
                },
                "message": {
                  "type": "string",
                  "id": 3
                }
              },
              "nested": {
                "Data": {
                  "fields": {
                    "amount": {
                      "type": "double",
                      "id": 1
                    },
                    "order_id": {
                      "type": "string",
                      "id": 2
                    },
                    "pay_url": {
                      "type": "string",
                      "id": 3
                    }
                  }
                }
              }
            },
            "normalPaySubmitV2": {
              "fields": {
                "code": {
                  "type": "int32",
                  "id": 1
                },
                "data": {
                  "type": "Data",
                  "id": 2
                },
                "message": {
                  "type": "string",
                  "id": 3
                }
              },
              "nested": {
                "Data": {
                  "fields": {
                    "amount": {
                      "type": "double",
                      "id": 1
                    },
                    "order_id": {
                      "type": "string",
                      "id": 2
                    },
                    "pay_url": {
                      "type": "string",
                      "id": 3
                    },
                    "pay_account_info": {
                      "type": "string",
                      "id": 4
                    },
                    "time_stamp": {
                      "type": "int64",
                      "id": 5
                    }
                  }
                }
              }
            }
          }
        },
        "signInHandler": {
          "nested": {
            "signIn": {
              "fields": {
                "code": {
                  "type": "int32",
                  "id": 1
                },
                "data": {
                  "type": "Data",
                  "id": 2
                },
                "message": {
                  "type": "string",
                  "id": 3
                }
              },
              "nested": {
                "Data": {
                  "fields": {
                    "sign_in_days": {
                      "type": "int32",
                      "id": 1
                    },
                    "last_sign_in_time": {
                      "type": "int64",
                      "id": 2
                    }
                  }
                }
              }
            },
            "getSignInInfo": {
              "fields": {
                "code": {
                  "type": "int32",
                  "id": 1
                },
                "data": {
                  "type": "Data",
                  "id": 2
                },
                "message": {
                  "type": "string",
                  "id": 3
                }
              },
              "nested": {
                "Data": {
                  "fields": {
                    "sign_in_days": {
                      "type": "int32",
                      "id": 1
                    },
                    "last_sign_in_time": {
                      "type": "int64",
                      "id": 2
                    }
                  }
                }
              }
            }
          }
        },
        "redpackHandler": {
          "nested": {
            "hit": {
              "fields": {
                "code": {
                  "type": "int32",
                  "id": 1
                },
                "data": {
                  "type": "Data",
                  "id": 2
                },
                "message": {
                  "type": "string",
                  "id": 3
                }
              },
              "nested": {
                "Data": {
                  "fields": {
                    "amount": {
                      "type": "double",
                      "id": 1
                    },
                    "total_amount": {
                      "type": "double",
                      "id": 2
                    }
                  }
                }
              }
            }
          }
        }

      }
    }
  }
}
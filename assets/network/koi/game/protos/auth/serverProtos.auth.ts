/**
 * 注意：已把原脚本注释，由于脚本变动过大，转换的时候可能有遗落，需要自行手动转换
 */
export default {
  "nested": {
    "connector": {
      "nested": {
        "authHandler": {
          "nested": {
            "auth": {
              "fields": {
                "code": {
                  "type": "int32",
                  "id": 1
                },
                "data": {
                  "type": "Data",
                  "id": 2
                },
                "message": {
                  "type": "string",
                  "id": 3
                }
              },
              "nested": {
                "Data": {
                  "fields": {}
                }
              }
            }
          }
        }
      }
    }
  }
}

import gameM from '../global/game';
import userM from '../global/user';
import settingM from '../global/setting';
// /**
//  * 全局数据的引用
//  */
export default {
    game : gameM,
    user : userM,
    // nodes_cache : require('./node-cache'),
    setting : settingM
};

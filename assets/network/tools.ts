
/**
 * 最常用的服务器请求（登录、加入、查询等）、sdk调用等
 */
import koi from './entry';

var XDNSource = [];

export default {
    XDNSource: [],

    forBreak(arr, iterator, callback) {
        callback = callback || function () { };
        if (!arr.length) {
            return callback();
        }
        var completed = 0;

        var iterate = function () {
            console.log('tools.forBreak 3 iterate', completed,);
            try {
                iterator(arr[(completed++) % arr.length*3], function (err, res) {
                    if (completed >= arr.length * 3 || res !== undefined) {
                        callback(null, res);
                        if (res !== undefined) {
                            callback = function () { };
                        }
                        return;
                    } else {
                        return iterate();
                    }
                });
            } catch (e) {
                console.error(e);
                if (completed >= arr.length) {
                    callback(e);
                    // callback = function() {};
                    return;
                } else {
                    return iterate();
                }
            }
        };
        iterate();
    },

    forBreakCount(arr, iterator, callback,count) {
        callback = callback || function () { };
        if (!arr.length) {
            return callback();
        }
        var completed = 0;

        var iterate = function () {
            console.log('tools.forBreak 3 iterate', completed,);
            try {
                iterator(arr[(completed++) % arr.length], function (err, res) {
                    if (completed >= arr.length * count || res !== undefined) {
                        callback(null, res);
                        if (res !== undefined) {
                            callback = function () { };
                        }
                        return;
                    } else {
                        return iterate();
                    }
                });
            } catch (e) {
                console.error(e);
                if (completed >= arr.length) {
                    callback(e);
                    // callback = function() {};
                    return;
                } else {
                    return iterate();
                }
            }
        };
        iterate();
    },

    getJSON: function ({ url, paras }, next) {
        if (typeof paras == 'function') {
       
            paras = null;
        }
        var xhr = new XMLHttpRequest();
        xhr.onloadend = function () {
            if (xhr){
                if ((xhr.status >= 200 && xhr.status < 300) || xhr.status == 304) {
                    try {
                        var r = JSON.parse(xhr.responseText);
                        console.log('httpTest(getJSON):',r);
                        next && next(null, r);
                        // return next = null;
                    } catch (e) {
                        console.error(e);
                        next && next(e);
                        // return next = null;
                    }
                } else {
                    next && next();
                    // return next = null;
                }
            }
        };
        xhr.onerror = function (err) {
            next && next(err);
            // return next = null;
        };
        xhr.ontimeout = function (err) {
            next && next(err);
            // return next = null;
        };
        if (!url.startsWith('http://')) {
            url = 'http://' + url;
        }
        url = url.replace(/127.0.0.1:(\d+)/, (m, o) => {
            return '127.0.0.1:' + (globalThis.xdnPortsAlias[o] || o)
        });
        xhr.timeout = 7000;
        if (paras) {
            var q = "";
            for (let key in paras) {
                if (paras[key] !== undefined) {
                    q += `&${key}=${encodeURIComponent(paras[key])}`
                }
            }
            url += '?' + q.substring(1);
        }
        console.log('PostURL',url);
        xhr.open("GET", url, true);
        if(globalThis.loginToken){
            xhr.setRequestHeader("authorization",globalThis.loginToken);
        }
        if(globalThis.uid){
            xhr.setRequestHeader("http-auth-uid",globalThis.uid);
        }
        xhr.send();
    },

    getWXJSON: function ({ url, paras }, path, next) {
        if (typeof paras == 'function') {
            paras = null;
        }
        var xhr = new XMLHttpRequest();
        xhr.onloadend = function () {
            if (xhr){
                if ((xhr.status >= 200 && xhr.status < 300) || xhr.status == 304) {
                    try {
                        var r = JSON.parse(xhr.responseText);
                        next && next(null, r);
                        // return next = null;
                    } catch (e) {
                        next && next(e);
                        // return next = null;
                    }
                } else {
                    next && next();
                    // return next = null;
                }
            }
        };
        xhr.onerror = function (err) {
            next && next(err);
            // return next = null;
        };
        xhr.ontimeout = function (err) {
            next && next(err);
            // return next = null;
        };
        // if (!url.startsWith('http://')) {
        //     url = 'http://' + url;
        // }
        url += path;
        url = url.replace(/127.0.0.1:(\d+)/, (m, o) => {
            return '127.0.0.1:' + (globalThis.xdnPortsAlias[o] || o)
        });
        xhr.timeout = 3000;
        if (paras) {
            var q = "";
            for (let key in paras) {
                if (paras[key] !== undefined) {
                    q += `&${key}=${encodeURIComponent(paras[key])}`
                }
            }
            url += '?' + q.substring(1);
        }
        console.log('PostURL',url);
        xhr.open("GET", url, true);
        xhr.send();
    },

    post({ url, paras,token}, next) {
        var xhr = new XMLHttpRequest();
        xhr.onloadend = function () {
            if (xhr){
                if ((xhr.status >= 200 && xhr.status < 300) || xhr.status == 304) {
                    // try {
                        var r = JSON.parse(xhr.responseText);
                        next && next(null, r);
                        // return next = null;
                    // } catch (e) {
                    //     console.log("errorText:"+xhr.responseText);
                    //     console.error('e:'+e);
                    //     console.error('jsone:'+JSON.stringify(e));
                    //     next && next(e);
                    //     // return next = null;
                    // }
                } else {
                    next && next('error',null);
                    // return next = null;
                }
            }
            
        };
        // xhr.onreadystatechange = ()=> {
        //     if (!xhr) return;
        //     if ((xhr.status >= 200 && xhr.status < 300 && xhr.responseText) || xhr.status == 304) {
        //         try {
        //             // console.log('xhr.responseText:',xhr.responseText);
        //             var r = JSON.parse(xhr.responseText);
        //             next && next(null, r);
        //             return next = null;
        //         } catch (e) {
        //             console.error(e);
        //             next && next(e);
        //             return next = null;
        //         }
        //     } 
        //     // else {
        //     //     next && next();
        //     //     return next = null;
        //     // }
        // };
        xhr.onerror = function (err) {
            next && next(err);
            // return next = null;
        };
        xhr.ontimeout = function (err) {
            // console.log('xhr.ontimeout:');
            next && next(err);
            // return next = null;
        };
        // if (!url.startsWith('http://')) {
        //     url = 'http://' + url;
        // }
        url = url.replace(/127.0.0.1:(\d+)/, (m, o) => {
            return '127.0.0.1:' + (globalThis.xdnPortsAlias[o] || o)
        });
        console.log('PostURL',url +','+JSON.stringify(paras));
        xhr.open("POST", url, true);
        xhr.timeout = 15000;
        // if(cc.loginToken){
        //     xhr.setRequestHeader("authorization",cc.loginToken);
        // }
        // if(cc.uid){
        //     xhr.setRequestHeader("http-auth-uid",cc.uid);
        // }
        xhr.setRequestHeader("Content-Type", "application/json");
        if(token){
            // console.error('cc.userToken:'+cc.userToken);
            xhr.setRequestHeader("token", token);
        }
        // var p = [];
        // for (let key in paras) {
        //     if (paras[key] !== undefined) {
        //         p.push(`${key}=${encodeURIComponent(paras[key])}`);
        //     }
        // }
        // console.log('....:'+p.join('&'));
        xhr.send(JSON.stringify(paras));
    },

    get({ url, paras,token}, next) {
        var xhr = new XMLHttpRequest();
        xhr.onloadend = function () {
            if (xhr){
                if ((xhr.status >= 200 && xhr.status < 300) || xhr.status == 304) {
                    var r = JSON.parse(xhr.responseText);
                    next && next(null, r);     
                } else {
                    next && next('error',null);
                }
            }
            
        };
        xhr.onerror = function (err) {
            next && next(err);
        };
        xhr.ontimeout = function (err) {
            next && next(err);
        };
        // if (!url.startsWith('https://')) {
        //     url = 'https://' + url;
        // }
        // if (!url.startsWith('http://')) {
        //     url = 'http://' + url;
        // }
        // url = url.replace(/127.0.0.1:(\d+)/, (m, o) => {
        //     return '127.0.0.1:' + (globalThis.xdnPortsAlias[o] || o)
        // });
        console.log('GetURL:',url +','+JSON.stringify(paras));
        xhr.open("GET", url, true);
        xhr.timeout = 15000;
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.send(JSON.stringify(paras));
    },

    getReq({ url, paras,token}, next) {
        var xhr = new XMLHttpRequest();
        xhr.onloadend = function () {
            if (xhr){
                if ((xhr.status >= 200 && xhr.status < 300) || xhr.status == 304) {
                    // try {
                        var r = JSON.parse(xhr.responseText);
                        next && next(null, r);

                } else {
                    next && next();
                    // return next = null;
                }
            }
            
        };
        xhr.onerror = function (err) {
            next && next(err);
            // return next = null;
        };
        xhr.ontimeout = function (err) {
            // console.log('xhr.ontimeout:');
            next && next(err);
            // return next = null;
        };
        if (!url.startsWith('http://')) {
            url = 'http://' + url;
        }
        url = url.replace(/127.0.0.1:(\d+)/, (m, o) => {
            return '127.0.0.1:' + (globalThis.xdnPortsAlias[o] || o)
        });
        console.log('PostURL',url +','+JSON.stringify(paras));
        xhr.open("GET", url, true);
        xhr.timeout = 15000;
        xhr.setRequestHeader("Content-Type", "application/json");
        if(token){
            // console.error('cc.userToken:'+cc.userToken);
            xhr.setRequestHeader("token", token);
        }

        xhr.send(JSON.stringify(paras));
    },


    postJSON({ url, paras }, next ) { 
        var xhr = new XMLHttpRequest();
        xhr.onloadend = function () {
            if (!xhr) return;
            if ((xhr.status >= 200 && xhr.status < 300) || xhr.status == 304) {
                try {
                    var r = JSON.parse(xhr.responseText);
                    next && next(null, r);
                    return next = null;
                } catch (e) {
                    next && next(e);
                    console.error(e);
                    return next = null;
                }
            } else {
                next && next();
                return next = null;
            }
        };
        xhr.onerror = function (err) {
            next && next(err);
            return next = null;
        };
        xhr.ontimeout = function (err) {
            next && next(err);
            return next = null;
        };
        if (!url.startsWith('http://')) {
            url = 'http://' + url;
        }
        url = url.replace(/127.0.0.1:(\d+)/, (m, o) => {
            return '127.0.0.1:' + (globalThis.xdnPortsAlias[o] || o)
        });
        console.log(new Date().toLocaleTimeString('en-US')+' nettestPostURL', url);
        console.info(new Date().toLocaleTimeString('en-US')+' nettestPostURL', url);
        xhr.open("POST", url, true);       
        // var timeout = 7000;
        // if(url.indexOf('getPayUrlV2') != -1){
        //     timeout = 10000;
        // }
        xhr.timeout = 7000;
        if(globalThis.loginToken){
            xhr.setRequestHeader("authorization",globalThis.loginToken);
        }
        if(globalThis.uid){
            xhr.setRequestHeader("http-auth-uid",globalThis.uid);
        }
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.send(JSON.stringify(paras));
    },



    connectedToInternet(cb) {
        // var getUrlHead = function (url, next) {
        //     var xhr = new XMLHttpRequest();
        //     xhr.onload = function () {
        //         console.log('connectedToInternet success')
        //         return next(null, 'getSuccess');
        //     };
        //     xhr.onerror = function (err) {
        //         next(err);
        //         // console.log('发送 onerror', JSON.stringify(xhr))
        //         xhr = null;
        //     };
        //     xhr.ontimeout = function (err) {
        //         next(err);
        //         // console.log('发送 ontimeout', JSON.stringify(xhr))
        //         xhr = null;
        //     };
        //     if (!url.startsWith('http://')) {
        //         url = 'http://' + url;
        //     }
        //     url = url.replace(/127.0.0.1:(\d+)/, (m, o) => {
        //         return '127.0.0.1:' + (cc.xdnPortsAlias[o] || o)
        //     });

        //     xhr.open("GET", url, true);
        //     xhr.send();
        // }
        var getUrlHead = function ({ url, paras }, next) {
            if (typeof paras == 'function') {
                cb = paras;
                paras = null;
            }
            var xhr = new XMLHttpRequest();
            xhr.onloadend = function () {
                console.log('connectedToInternet',xhr.status)
                if (xhr){
                    if ((xhr.status >= 200 && xhr.status < 300) || xhr.status == 304) {
                        console.log('connectedToInternet success')
                        return next(null, 'getSuccess');
                    }else{
                        next && next();
                        return next = null;
                    }
                }
            };
            xhr.onerror = function (err) {
                next && next(err);
                // return next = null;
            };
            xhr.ontimeout = function (err) {
                next && next(err);
                // return next = null;
            };
            if (!url.startsWith('http://')) {
                url = 'http://' + url;
            }
            url = url.replace(/127.0.0.1:(\d+)/, (m, o) => {
                return '127.0.0.1:' + (globalThis.xdnPortsAlias[o] || o)
            });
            xhr.timeout = 7000;
            if (paras) {
                var q = "";
                for (let key in paras) {
                    if (paras[key] !== undefined) {
                        q += `&${key}=${encodeURIComponent(paras[key])}`
                    }
                }
                url += '?' + q.substring(1);
            }
            console.log('PostURL',url);
            xhr.open("GET", url, true);
            xhr.send();
        }


       
    },

    connectedToNetLink(urlPath,cb) {
        var getUrlHead = function ({ url, paras }, next) {
            if (typeof paras == 'function') {
                cb = paras;
                paras = null;
            }
            var xhr = new XMLHttpRequest();
            xhr.onloadend = function () {
                console.log('connectedToNetLink',xhr.status)
                if (xhr){
                    if ((xhr.status >= 200 && xhr.status < 300) || xhr.status == 304) {
                        console.log('connectedToNetLink success')
                        return next(null, 'getSuccess');
                    }else{
                        next && next();
                        return next = null;
                    }
                }
            };
            xhr.onerror = function (err) {
                next && next(err);
                // return next = null;
            };
            xhr.ontimeout = function (err) {
                next && next(err);
                // return next = null;
            };
            if (!url.startsWith('http://')) {
                url = 'http://' + url;
            }
            url = url.replace(/127.0.0.1:(\d+)/, (m, o) => {
                return '127.0.0.1:' + (globalThis.xdnPortsAlias[o] || o)
            });
            xhr.timeout = 7000;
            if (paras) {
                var q = "";
                for (let key in paras) {
                    if (paras[key] !== undefined) {
                        q += `&${key}=${encodeURIComponent(paras[key])}`
                    }
                }
                url += '?' + q.substring(1);
            }
            console.log('PostURL',url);
            xhr.open("GET", url, true);
            xhr.send();
        }
    },

    connectedToBaidu(cb) {
        var getUrlHead = function (url, next) {
            var xhr = new XMLHttpRequest();
            xhr.onload = function () {
                console.log('connectedToInternet success')
                return next(null, 'getSuccess');
            };
            xhr.onerror = function (err) {
                next(err);
                // console.log('发送 onerror', JSON.stringify(xhr))
                xhr = null;
            };
            xhr.ontimeout = function (err) {
                next(err);
                // console.log('发送 ontimeout', JSON.stringify(xhr))
                xhr = null;
            };

            xhr.open("GET", url, true);
            xhr.send();
        }
        this.forBreak(['http://www.baidu.com'], getUrlHead, (err, res) => {
            console.log('--connectedToInternet', err, res);
            cb(err, res);
        })
    },

    getLocalIP(cb){
        var getUrlHead = function (url, next) {
            var xhr = new XMLHttpRequest();
            xhr.onload = function () {
                if ((xhr.status >= 200 && xhr.status < 300) || xhr.status == 304) {
                    try {
                        console.log("getIP1:",xhr.responseText);                    
                        if(xhr.responseText){    
                            var str = xhr.responseText;
                            // str = '{'+str.split('{')[1].split('}')[0]+'}';
                            console.log('iptest',JSON.parse(str));
                            var r = JSON.parse(str);
                            next(null, r);
                        }else{
                            return next();
                        }
                    } catch (e) {
                        return next(e);
                    }
                } else {
                    next();
                }
                xhr = null;
            }
            xhr.onerror = function (err) {
                next(err);
                // console.log('发送 onerror', JSON.stringify(xhr))
                xhr = null;
            };
            xhr.ontimeout = function (err) {
                next(err);
                // console.log('发送 ontimeout', JSON.stringify(xhr))
                xhr = null;
            };

            xhr.open("GET", url, true);
            xhr.send();
        }
        //http://pv.sohu.com/cityjson?ie=utf-8
        this.forBreak(['http://ip.taobao.com/service/getIpInfo.php?ip=myip'], getUrlHead, (err, res) => {
            console.log('--GetIP', err, res);
            // cc.cIP = res.cip;
            // cc.cName = res.cname;
            globalThis.cIP = res.data.ip;
            globalThis.cName = res.data.region+res.data.city;
            globalThis.Isp = res.data.isp;
            console.log('IPinfo',globalThis.cIP,globalThis.cName,globalThis.Isp);
            if(cb){
                cb();
            }
        })
    },

};


import { _decorator, Component, Node, Vec3 } from 'cc';
import { sBoxEntity } from '../lobby/game/core/sBoxEntity';
import { sBoxItemBase } from '../lobby/game/core/sBoxItemBase';
import { sObjPool } from '../lobby/game/core/sObjPool';
import { sBoxAssetInit } from '../lobby/game/core/sBoxAssetInit';
import sAudioMgr from '../lobby/game/core/sAudioMgr';
import { StarlightPrincess_Thunder } from './StarlightPrincess_Thunder';
const { ccclass, property } = _decorator;

class BoxObj {
    box: sBoxItemBase;
    overEffect : sBoxItemBase;
}
@ccclass('StarlightPrincess_Entity')
export class StarlightPrincess_Entity extends sBoxEntity {
    protected boxObj : BoxObj = new BoxObj();
    private thundered = false;
    //是否可以消除该盒子
    get IsDisappearBox(){
        if(this.boxStateNow == 'win'&&this.SymbolValue!=1&&this.SymbolValue<2000){
            return true;
        }
        return false; 
    }
    public EntityInit(){
        super.EntityInit();

        this.RegisterEnitityAction('thunder',()=>{
            //if(!this.thundered){
                //console.log('格子位置：',this.viewItemIndex.x,this.viewItemIndex.y);
                //if(this.viewItemIndex.y >= 0 && this.viewItemIndex.y <= 4){
                    //this.thundered = true;
                    if(this.BoxStateNow != 'idle'){
                        this.UpdateData('idle','settlement');
                    }
                    let spineAnimaName = 'gool_summon_effect_1';
                    const rSymbol =  this.symbolValue;
                    if(rSymbol > 2000 && rSymbol < 2007){
                        spineAnimaName = 'gool_summon_effect_1';
                    }else if(rSymbol >= 2007 && rSymbol < 2011){
                        spineAnimaName = 'gool_summon_effect_2';
                    }else if(rSymbol >=2011  && rSymbol < 2014){
                        spineAnimaName = 'gool_summon_effect_3';
                    }else{
                        spineAnimaName = 'gool_summon_effect_4';
                    }
                    const pos = this.GetBoxWorldPosition();
                    pos.x += 80;
                    pos.y +=290; 
                    const thunder : Node = sObjPool.Dequeue('thunder');
                    if(thunder && thunder.isValid){
                        thunder.parent = sBoxAssetInit.instance.getTargetNodeByName('effectLayer');
                        thunder.worldPosition = pos;
                        thunder.active = true;
                        sAudioMgr.PlayShotAudio('mult_appear');
                        
                        thunder.getComponent(StarlightPrincess_Thunder).play(spineAnimaName);
                    }
                //}
            //}
        });
    }

    // protected JackpotBingo(){
    //     if(this.IsJackpotBox){
    //         this.UpdateData('win','settlement');
    //     }
    // }

    public UpdateBoxPosition(pos: Vec3) {
        super.UpdateBoxPosition(pos);
        this.boxObj.box.node.position = pos;
        this.boxObj.overEffect.node.position = pos;
    }

    public UpdateData(boxState,tableState){
        this.boxStateNow = boxState;
        this.boxData = this.getSymbolData(this.SymbolValue);

        if(this.boxData){
            const renderData = this.boxData[boxState];
            
            // if(this.SymbolValue == 88 && boxState == 'idle'){
            //     if(this.ViewItemIndex.y >= 0 && this.ViewItemIndex.y <= 4){
            //         this.boxObj.box.boxItemUpdate(renderData,this.SymbolValue,boxState,tableState);
            //     }
            // }else{
            //     this.boxObj.box.boxItemUpdate(renderData,this.SymbolValue,boxState,tableState);
            // }
            if(this.symbolValue== 1&&this.ViewItemIndex.y >= 0 && this.ViewItemIndex.y <= 4){
                this.boxObj.box.clearItem(this.SymbolValue,boxState,tableState);
                this.boxObj.box.node.active = false;

                this.boxObj.overEffect.node.active = true;
                this.boxObj.overEffect.boxItemUpdate(renderData,this.SymbolValue,boxState,tableState);
            }else{
                this.boxObj.overEffect.clearItem(this.SymbolValue,boxState,tableState);
                this.boxObj.overEffect.node.active = false;

                this.boxObj.box.boxItemUpdate(renderData,this.SymbolValue,boxState,tableState);
                this.boxObj.box.node.active = true;

            };
            
            // if(boxState == 'win' && this.symbolValue <=2000 && this.symbolValue != 1){
            //     this.boxObj.overEffect.node.position = this.boxPos;
            //     this.boxObj.overEffect.node.active = true;
            //     this.boxObj.overEffect.boxItemUpdate(renderData,this.SymbolValue,boxState,tableState);
            // }

            let pstr=this.SymbolValue.toString();
            pstr=pstr.slice(2);

            if(boxState == 'idle' && this.symbolValue == 1){
                if(this.ViewItemIndex.y >= 0 && this.ViewItemIndex.y <= 4){
                    sAudioMgr.PlayShotAudio('scatter_appear');
                };
            }else if(boxState == 'idle' && this.symbolValue > 2000){
                if(this.ViewItemIndex.y >= 0 && this.ViewItemIndex.y <= 4){
                    let str='scatter_appear_voice'+parseInt(pstr);
                    sAudioMgr.PlayShotAudio(str);
                };
            };
        };
    }

    public ClearItem(boxState,tableState){
        super.ClearItem(boxState,tableState);
        this.thundered = false;
        if(this.boxObj.box){
            this.boxObj.box.clearItem(null,boxState,tableState);
        }
        if(this.boxObj.overEffect){
            this.boxObj.overEffect.node.active = false;
            this.boxObj.overEffect.clearItem(null,boxState,tableState);
        }
    }
}


import { _decorator, Color, color, Component, Node, Sprite } from 'cc';
import { sBoxItemBase } from '../lobby/game/core/sBoxItemBase';
import { sUtil } from '../lobby/game/core/sUtil';
const { ccclass, property } = _decorator;


 
@ccclass('StarlightPrincess_OverEffect')
export class StarlightPrincess_OverEffect extends sBoxItemBase {
    @property(Sprite)
    lightNode : Sprite;
    @property(Sprite)
    boomNode : Sprite;

    protected start () {
        super.start();
    }

    boxItemUpdate(renderData,symbolValue,boxState,tableState){
        if(boxState == 'win'){
            this.lightNode.color = color(255,255,255,0);
            this.lightNode.node.active = true;;;
            sUtil.TweenColor(Color.WHITE,this.lightNode,0.2,0);
            this.pushOneSchedule(()=>{
                sUtil.TweenColor(color(255,255,255,0),this.lightNode,0.2,0);
            },1);
            this.pushOneSchedule(()=>{
                this.lightNode.node.active = false;
                this.boomNode.node.active = true;
            },1.2);
        }
    }

    clearItem(symbolValue,rollState,tableState){
        this.removeAllSchedule();
        this.lightNode.node.active = false;
        this.boomNode.node.active = false;
    }
}


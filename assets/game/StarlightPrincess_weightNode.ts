
import { _decorator, Component, Node, UITransform, v3, View, view } from 'cc';
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = StarlightPrincess_weightNode
 * DateTime = Tue Oct 10 2023 17:23:55 GMT+0800 (中国标准时间)
 * Author = dadadaxigua
 * FileBasename = StarlightPrincess_weightNode.ts
 * FileBasenameNoExtension = StarlightPrincess_weightNode
 * URL = db://assets/game/StarlightPrincess_weightNode.ts
 * ManualUrl = https://docs.cocos.com/creator/3.4/manual/zh/
 *
 */
 
@ccclass('StarlightPrincess_weightNode')
export class StarlightPrincess_weightNode extends Component {
    // [1]
    // dummy = '';
    @property(Node)
    leftNode:Node
    @property(Node)
    RightNode:Node
    @property(Node)
    stpr_freegame_idle:Node
    protected onLoad(): void {
        let posX1=view.getVisibleSize().x/2-415;
        let posX2=view.getVisibleSize().x/2-415;
        this.leftNode.getComponent(UITransform).setContentSize(posX1,600);
        this.RightNode.getComponent(UITransform).setContentSize(posX2,600);
        
        let stpr_freeNodeX=(this.stpr_freegame_idle.getComponent(UITransform).contentSize.x)*0.72;
        let scaleX=(view.getVisibleSize().x/stpr_freeNodeX)+0.4;
        this.stpr_freegame_idle.scale=v3(scaleX,scaleX,scaleX);
        // if(scaleX>1){
        //     for(let i=1;i<=10;i++){
        //         if(stpr_freeNodeX*(scaleX+i*0.1)>view.getVisibleSize().x){
        //             scaleX=scaleX+i*0.1;
        //             this.stpr_freegame_idle.scale=v3(scaleX,scaleX,scaleX);
        //         }else{

        //         };
        //     };
        // };
    };
    start () {
        // [3]
        
    }

    // update (deltaTime: number) {
    //     // [4]
    // }
}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.4/manual/zh/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.4/manual/zh/scripting/decorator.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.4/manual/zh/scripting/life-cycle-callbacks.html
 */


import { _decorator, Color, Component, director, game, Label, Node, Scene, Sprite, SpriteFrame, tween, UIOpacity, UITransform, v3, Vec3 } from 'cc';
import { sComponent } from '../lobby/game/core/sComponent';
import { sObjPool } from '../lobby/game/core/sObjPool';
import { sUtil } from '../lobby/game/core/sUtil';
import sAudioMgr from '../lobby/game/core/sAudioMgr';
import { assetMgr } from '../lobby/game/core/sAssetMgr';
const { ccclass, property } = _decorator;

@ccclass('StarlightPrincess_Board')
export class StarlightPrincess_Board extends sComponent {
    @property(UIOpacity)
    normalInfoNode : UIOpacity;
    @property(UIOpacity)
    tableOpa : UIOpacity;
    @property(Label)
    baseLabel : Label;
    @property(Node)
    multiNode : Node;

    @property([SpriteFrame])
    boradSprites = [];
 
    private index = 0;
    private mode : string = 'idle'; //idle win multi
    private targetImg : Sprite = null;
    private totalRate = 0;
    private coinLabelValue = 0;
    private oneRoundMultiValue = 0;


    protected start () {
        this.BoardAction();

        // this.scheduleOnce(()=>{
        //     director.emit('winBetRes',1000);
        //     // this.scheduleOnce(()=>{
        //     //     // director.emit('winBetRes',60);
        //     //     this.multiLabelAnima(v3(0,0,0),10);
        //     // },2);
        // },2);

        director.on('winMultiBox',(data)=>{
            if(data){
                this.multiLabelAnima(data.actionType,data.pos,data.multi,data.fontSize);
            };
        },this);

        director.on('winBetRes',(rate : number)=>{
            if(rate > 0){
                // console.log('winBetRes:',rate);
                this.totalRate += rate;
                this.showWinCoin(this.totalRate * globalThis.GameBtnEntity.CurrentBetAmount);

                // this.showWinCoin(this.totalRate * 10);
            }else if(rate == -1){
                director.emit('titleWinSettleEnd');
            }else if(rate==-2){
                
            };

            // this.showWinCoin(30);
        },this);

        director.on('slotSubGameRollBegin', () => {
            this.totalRate = 0;
            this.coinLabelValue = 0;
            this.oneRoundMultiValue = 0;
            if(this.mode != 'idle'){
                this.cleanTweenDic();
                this.removeIDTween('imgSprite');
                this.removeIDSchedule('showBoard');
                this.BoardAction();
                this.removeIDTween('table');
                this.pushIDTween(tween(this.tableOpa).to(0.3,{opacity : 0}).call(()=>{
                    this.tableOpa.node.active = false;
                }).start(),'table');
            }
        }, this);

    }

    BoardAction(){
        if(this.boradSprites && this.boradSprites.length > 0){
            this.mode = 'idle';
            this.normalInfoNode.node.active = true;
            this.removeIDTween('normalInfoNode');
            this.pushIDTween(tween(this.normalInfoNode).to(0.3,{opacity : 255}).start(),'normalInfoNode');
            if(this.index >= this.boradSprites.length){
                this.index = 0;
            }
            this.showBoard(this.boradSprites[this.index]);
            this.index++;
        }
    }

    showWinCoin(coinNum : number){
        this.removeIDSchedule('showBoard');
        if(coinNum > 0){
            this.mode = 'win';
            // if(this.targetImg){
            //     const imgTemp = this.targetImg;
            //     this.pushIDTween(sUtil.TweenColor(Color.TRANSPARENT,imgTemp,0.3,0,()=>{
            //         sObjPool.Enqueue(imgTemp.node.name,imgTemp.node);
            //     }),'imgSprite2');
            // }

            this.removeIDTween('normalInfoNode');
            this.pushIDTween(tween(this.normalInfoNode).to(0.3,{opacity : 0}).start(),'normalInfoNode');

            if(!this.tableOpa.node.active){
                this.removeIDTween('table');
                this.tableOpa.opacity = 0;
                this.tableOpa.node.active = true;
                this.pushIDTween(tween(this.tableOpa).to(0.3,{opacity : 255}).start(),'table');
            }
            const lastCoin = this.coinLabelValue;
            this.coinLabelValue = coinNum;
            this.baseLabel.node.position=v3(0,-3,0);
            this.multiNode.active=false;
            sUtil.TweenLabel(lastCoin,this.coinLabelValue,this.baseLabel,1);
        }
    }

    showBoard(spriteFrame : SpriteFrame){
        if(this.targetImg){
            const tempImg = this.targetImg;
            sObjPool.Enqueue('bannerImg',tempImg.node);
            tempImg.node.active = false;
            this.targetImg = null;
        }
        if(this.normalInfoNode && spriteFrame){
            // console.log('bannerImgcoun : ',sObjPool.Count('bannerImg'));
            const img : Node = sObjPool.Dequeue('bannerImg');
            if(img && img.isValid){
                img.parent = this.normalInfoNode.node;
                img.position = v3(0,0,0);
                img.active = true;
                const imgSprite = img.getComponent(Sprite);
                if(imgSprite){
                    this.tableOpa.node.active = false;
                    this.targetImg = imgSprite;
                    imgSprite.spriteFrame = assetMgr.GetAssetByName(spriteFrame.name+'_int');;
                    imgSprite.color = Color.TRANSPARENT;
                    this.pushIDTween(sUtil.TweenColor(Color.WHITE,imgSprite,0.3,0),'imgSprite');
                    this.pushIDSchedule(()=>{
                        this.BoardAction();
                    },5,'showBoard');
                }
            }
        }
    }

    private multiLabelAnima(actionType : string, pos : Vec3,multiValue : number,fontSize : number){
        //console.log('倍数飞行');
        if(pos && multiValue){
            console.log('倍数符号：',this.oneRoundMultiValue,multiValue)
            this.oneRoundMultiValue += multiValue;
            const baseLabelWidth = this.baseLabel.getComponent(UITransform).contentSize;
            const multi = this.multiNode.getChildByName('multi');
            const notation = this.multiNode.getChildByName('notation');
            const multiTran = multi.getComponent(UITransform);
            const multiScript = multi.getComponent(Label);
            const multiLabel : Node = sObjPool.Dequeue('boardMultiLabel');
            if(multiLabel && multiLabel.isValid){
                
                multiLabel.scale = Vec3.ONE;
                multiLabel.parent = this.node;
                multiLabel.worldPosition = pos;
                this.SetNodeFront(multiLabel);
                multiLabel.active = true;
                let endPos = v3(multi.worldPosition.x + multiTran.contentSize.x / 2,multi.worldPosition.y,0);
                const multiLabelScript = multiLabel.getComponent(Label);
                multiLabelScript.fontSize = fontSize;
                multiLabelScript.string = multiValue + 'x';
                // this.removeIDTween('multiLabel');
                sAudioMgr.PlayShotAudio('mult_fly',1);


                this.pushIDTween(tween(this.baseLabel.node).delay(0.8).to(0.2,{position : v3(-35,-3,0)},{easing : 'cubicOut'}).call(()=>{
                    
                }).start(),'multiLabelAnima');


                this.pushIDTween(tween(multiLabel).sequence(tween(multiLabel).to(0.53,{scale : v3(2,2,2)},{easing : 'cubicOut'}),
                tween(multiLabel).parallel(tween(multiLabel).to(0.5,{scale : v3(0.8,0.8,1)},{easing : 'linear'}),
                tween(multiLabel).call(()=>{
                }).to(0.5,{worldPosition : endPos},{easing : 'sineIn'}))).call(()=>{

                    if(this.mode != 'multi'){
                        this.mode = 'multi';
                        const baseLabelWidth = this.baseLabel.getComponent(UITransform).contentSize;
                        this.multiNode.position = v3( baseLabelWidth.x / 2 +20,5,0);
                        this.removeIDTween('multiLabelAnima');
                        
                        //sAudioMgr.PlayShotAudio('total multiplier_result_petvoice',1);
                        
                        this.multiNode.active = true;
                        notation.active = true;
                    };
                    //this.scheduleOnce(()=>{
                    multiScript.string = this.oneRoundMultiValue.toString();
                    //},0.4);
                    
                    console.log('倍数符号：',multiScript.string)
                    tween(multi).to(0.15,{scale : v3(1.6,1.6,1.6)},{easing : 'sineOut'}).to(0.1,{scale : Vec3.ONE}).start();
                    sObjPool.Enqueue('boardMultiLabel',multiLabel);
                    multiLabel.active = false;
                    if(actionType == 'close'){
                        tween(this.multiNode).delay(0.3).call(()=>{
                            notation.active = false;
                            
                        }).to(0.3,{position : Vec3.ZERO},{easing : 'cubicIn'}).call(()=>{
                            this.multiNode.active = false;
                            sAudioMgr.PlayShotAudio('total multiplier_result_petvoice');
                        }).start();

                        tween(this.baseLabel.node).delay(0.3).call(()=>{
                           
                        }).to(0.3,{position :v3(0,-3,0)},{easing : 'cubicIn'}).call(()=>{
                            this.baseLabel.string = this.AddCommas(this.coinLabelValue * this.oneRoundMultiValue);
                            
                            tween(this.baseLabel.node).to(0.15,{scale : v3(1.3,1.3,1.3)},{easing : 'sineOut'}).to(0.1,{scale : Vec3.ONE}).call(()=>{
                                sAudioMgr.PlayShotAudio('total multiplier_result',1);
                                director.emit('StarlightPrincessLabelEnd');
                            }).start();
                        }).start();
                    };
                }).start(),'multiLabel');
            }
        }
    }

    
}

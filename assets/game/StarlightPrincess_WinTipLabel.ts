
import { _decorator, Component, Label, Node, tween, UIOpacity, v3, Vec3 } from 'cc';
import { sComponent } from '../lobby/game/core/sComponent';
import { sObjPool } from '../lobby/game/core/sObjPool';
const { ccclass, property } = _decorator;

@ccclass('StarlightPrincess_WinTipLabel')
export class StarlightPrincess_WinTipLabel extends sComponent {
    @property(UIOpacity)
    mainOpa : UIOpacity;
    @property(Label)
    coinLabel : Label;

    protected start () {
        // this.scheduleOnce(()=>{
        //     this.play('1.2');
        // },2);
    }

    public play(str : string){
        this.cleanTweenList(); 
        this.coinLabel.string = str; 
        this.mainOpa.opacity = 0;
        this.node.scale = Vec3.ZERO;
        this.pushOneTween(tween(this.mainOpa).to(0.3,{opacity : 255}).start());
        this.pushOneTween(tween(this.node).by(0.3,{scale : v3(1,1,1),position : v3(0,15,0)}).call(()=>{
            this.pushOneTween(tween(this.mainOpa).to(1,{opacity : 0},{easing : 'cubicIn'}).start());
            this.pushOneTween(tween(this.node).by(1,{scale : v3(-0.6,-0.6,-0.6),position : v3(0,80,0)}).call(()=>{
                this.node.active = false;
                sObjPool.Enqueue(this.node.name,this.node);
            }).start());
        }).start());
    }
}



import { _decorator, Button, Component, director, Label, Node, ParticleSystem, sp, tween, UIOpacity, v3, Vec3 } from 'cc';
import { sComponent } from '../lobby/game/core/sComponent';
import { sUtil } from '../lobby/game/core/sUtil';
import sAudioMgr from '../lobby/game/core/sAudioMgr';
const { ccclass, property } = _decorator;


 
@ccclass('StarlightPrincess_BigWin')
export class StarlightPrincess_BigWin extends sComponent {
    @property(Node)
    mainNode : Node;
    @property(Node)
    CoinNode : Node;
    @property(sp.Skeleton)
    targetSpine : sp.Skeleton;
    @property(UIOpacity)
    borderNode : UIOpacity;
    @property(UIOpacity)
    maskOpa : UIOpacity;
    @property(Button)
    closeBtn : Button;

    private touchEnable = false;
    private targetCoinValue = 0;
    private animaType = 'nice'; // nice,mega,super,sensational
    private bgmp = 'bgm_mg';
    protected start () {
        director.on('freeWinBegain',()=>{
            this.bgmp = 'bgm_fs';
        },this);

        director.on('freeWinOver',()=>{
            this.bgmp = 'bgm_mg';
        },this);
        director.on('StarlightPrincessBigWin',(rate)=>{
            let spineAnimanName = '_nice_';
            sAudioMgr.StopBGAudio();
            if(rate >= 200 && rate < 400){
                sAudioMgr.PlayBG('bgm_nicewin');
                spineAnimanName = '_nice_';
                this.animaType = 'nice';
            }else if(rate >= 400 && rate < 600){
                sAudioMgr.PlayBG('bgm_megawin');
                spineAnimanName = '_mega_';
                this.animaType = 'mega';
            }else if(rate >= 600 && rate < 1000){
                sAudioMgr.PlayBG('bgm_superwin');
                sAudioMgr.PlayShotAudio('bgm_superwin_voice');
                spineAnimanName = '_superb_';
                this.animaType = 'super';
            }else if(rate >= 1000){
                sAudioMgr.PlayBG('bgm_sensationalwin');
                sAudioMgr.PlayShotAudio('bgm_sensationalwin_voice');
                spineAnimanName = '_sensational_';
                this.animaType = 'sensational';
            }else{
                director.emit('StarlightPrincessBigWinEnd');
            };
            // this.viewOpen(spineAnimanName,rate * 12); 
            this.viewOpen(spineAnimanName,rate* globalThis.GameBtnEntity.CurrentBetAmount);
            //this.viewOpen(spineAnimanName,rate* 20);
        },this);

        
        
        // this.scheduleOnce(()=>{
        //    director.emit('StarlightPrincessBigWin',200);
        // },2);

        this.closeBtn.node.on('click',()=>{
            if(this.touchEnable){
                this.touchEnable = false;
                this.cleanTweenList();
                for(let i=0;i<this.CoinNode.children.length;i++){
                    this.CoinNode.children[i].getComponent(ParticleSystem).loop=false;
                };
                this.removeIDSchedule('coinLable');
                const coinLabelNode = this.borderNode.node.getChildByName('label');
                if(coinLabelNode){
                    sAudioMgr.StopBGAudio();
                    if(this.animaType == 'nice'){
                        sAudioMgr.PlayBG('bgm_nicewin_end',false);
                    }else if(this.animaType == 'mega'){
                        sAudioMgr.PlayBG('bgm_megawin_end',false);
                    }else if(this.animaType == 'super'){
                        sAudioMgr.PlayBG('bgm_superwin_end',false);
                    }else if(this.animaType == 'sensational'){
                        sAudioMgr.PlayBG('bgm_sensationalwin_end',false);
                    };
                    const coinLable = coinLabelNode.getComponent(Label);
                    coinLable.string = this.AddCommas(this.targetCoinValue);
                    //this.pushOneTween(tween(coinLable.node).to(0.3,{scale : v3(1.3,1.3,1.3)},{easing : 'sineOut'}).to(0.3,{scale : Vec3.ONE},{easing : 'sineIn'}).union().repeatForever().start());
                    //this.pushOneTween(tween(this.borderNode.node).to(0.6,{scale : v3(1.2,1.2,1.2)},{easing : 'sineOut'}).to(0.6,{scale : Vec3.ONE},{easing : 'sineIn'}).union().repeatForever().start());
                };
                this.scheduleOnce(()=>{
                    director.emit('StopLabelanima');
                    this.viewClose();
                },3);
            }
        },this);
    }


    private viewOpen(spineAnima : string,coin : number){
        this.targetCoinValue = coin;
        if(this.targetSpine && this.borderNode){
            this.SetNodeFront(this.node);
            this.removeIDSchedule('touch');
            this.touchEnable = false;
            this.cleanTweenList();
            this.SetUIFront();
            this.mainNode.active = true;
            this.targetSpine.node.active = true;
            this.targetSpine.setAnimation(0,'stpr_bigwin'+spineAnima+'in',false);
            for(let i=0;i<this.CoinNode.children.length;i++){
                this.CoinNode.children[i].getComponent(ParticleSystem).loop=true;
            };
            this.maskOpa.opacity = 0;
            this.pushOneTween(tween(this.maskOpa).to(0.3,{opacity : 255}).start());
            let IsCom=0;
            IsCom+=1;
            const coinLabelNode = this.borderNode.node.getChildByName('label');
            if(coinLabelNode&&IsCom==1){
                const coinLable = coinLabelNode.getComponent(Label);
                let bigWinTime = 12;
                if(this.animaType == 'nice'){
                    this.CoinNode.active=true;
                    this.CoinNode.children[0].active=false;
                    this.CoinNode.children[1].active=false;
                    this.CoinNode.children[2].active=true;
                }else if(this.animaType == 'mega'){
                    bigWinTime = 12;
                    this.CoinNode.active=true;
                    this.CoinNode.children[0].active=false;
                    this.CoinNode.children[1].active=false;
                    this.CoinNode.children[2].active=true;
                }else if(this.animaType == 'super'){
                    bigWinTime = 12;
                    this.CoinNode.active=true;
                    this.CoinNode.children[0].active=true;
                    this.CoinNode.children[1].active=true;
                    this.CoinNode.children[2].active=true;
                }else if(this.animaType == 'sensational'){
                    bigWinTime = 12;
                    this.CoinNode.active=true;
                    this.CoinNode.children[0].active=true;
                    this.CoinNode.children[1].active=true;
                    this.CoinNode.children[2].active=true;
                };
                coinLable.string='0';
                this.scheduleOnce(()=>{
                    this.pushOneTween(sUtil.TweenLabel(0,coin,coinLable,bigWinTime,0,'quadOut',0,()=>{
                        sAudioMgr.StopBGAudio();
                        if(this.animaType == 'nice'){
                            sAudioMgr.PlayBG('bgm_nicewin_end',false);
                        }else if(this.animaType == 'mega'){
                            sAudioMgr.PlayBG('bgm_megawin_end',false);
                        }else if(this.animaType == 'super'){
                            sAudioMgr.PlayBG('bgm_superwin_end',false);
                        }else if(this.animaType == 'sensational'){
                            sAudioMgr.PlayBG('bgm_sensationalwin_end',false);
                        };
                        for(let i=0;i<this.CoinNode.children.length;i++){
                            this.CoinNode.children[i].getComponent(ParticleSystem).loop=false;
                            //this.CoinNode.children[i].getComponent(ParticleSystem).stop();
                        };
    
                        //this.pushOneTween(tween(coinLable.node).to(0.3,{scale : v3(1.3,1.3,1.3)},{easing : 'sineOut'}).to(0.3,{scale : Vec3.ONE},{easing : 'sineIn'}).union().repeatForever().start());
                        this.pushIDSchedule(()=>{
                            this.viewClose();
                        },1,'coinLable');
                    }));
                },0.7);
                
            };
            this.targetSpine.setCompleteListener(()=>{
                this.targetSpine.setAnimation(0,'stpr_bigwin'+spineAnima+'loop',true);
            });
            
            this.borderNode.opacity =0;
            this.borderNode.node.position = v3(0,-120,0);
            this.borderNode.node.scale = v3(1.4,1.4,1.4);
            this.pushOneTween(tween(this.borderNode).delay(0.6).to(0.2,{opacity : 255}).start());
            this.pushOneTween(tween(this.borderNode.node).delay(0.6).to(0.2,{position : v3(0,-240,0)}).call(()=>{
               // this.pushOneTween(tween(this.borderNode.node).to(0.6,{scale : v3(1.2,1.2,1.2)},{easing : 'sineOut'}).to(0.6,{scale : Vec3.ONE},{easing : 'sineIn'}).union().repeatForever().start());
            }).start());
            this.pushIDSchedule(()=>{
                this.touchEnable = true;
            },0.5,'touch');
        }
    }

    private viewClose(){
        this.CoinNode.active=false;
        if(this.targetSpine && this.borderNode){
            const animaName = this.targetSpine.animation;
            if(animaName.length > 1){
                //const rName = animaName.slice(0,2);
                //if(rName){
                    if(this.animaType == 'nice'){
                        this.targetSpine.setAnimation(0,'stpr_bigwin_nice_out' ,false);
                    }else if(this.animaType == 'mega'){
                        this.targetSpine.setAnimation(0,'stpr_bigwin_mega_out',false);
                    }else if(this.animaType == 'super'){
                        this.targetSpine.setAnimation(0,'stpr_bigwin_superb_out',false);
                    }else if(this.animaType == 'sensational'){
                        this.targetSpine.setAnimation(0,'stpr_bigwin_sensational_out',false);
                    };
                     
                    this.targetSpine.setCompleteListener(()=>{
                       
                        this.scheduleOnce(()=>{
                            for(let i=0;i<this.CoinNode.children.length;i++){
                                //this.CoinNode.children[i].getComponent(ParticleSystem).loop=false;
                                this.CoinNode.children[i].getComponent(ParticleSystem).clear();
                                this.CoinNode.children[i].getComponent(ParticleSystem).stop();
                            };
                            sAudioMgr.StopAudio();
                            sAudioMgr.StopBGAudio();
                            sAudioMgr.PlayBG(this.bgmp);
                            this.scheduleOnce(()=>{
                               
                                this.mainNode.active = false;
                                director.emit('StarlightPrincessBigWinEnd');
                            },0.5)
                            
                        },0.5);
                        
                    });  
                //};
            };
            this.cleanTweenList();
            this.removeIDSchedule('coinLable');
            this.pushOneTween(tween(this.maskOpa).to(0.3,{opacity : 0}).start());
            this.pushOneTween(tween(this.borderNode).to(0.3,{opacity : 0}).call(()=>{
                this.borderNode.node.getChildByName('label').getComponent(Label).string ='0';
            }).start());
                // this.pushOneTween(tween(this.borderNode.node).to(0.2,{position : v3(0,-120,0),scale : v3(1.6,1.6,1.6)}).call(()=>{
                    
                // }).start());
            //},0.5);
    
        };
    };


}

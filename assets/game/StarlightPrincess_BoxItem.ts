
import { _decorator, color, Color, Component, director, Label, Node, Size, sp, Sprite, SpriteFrame, UITransform, v3 } from 'cc';
import { sBoxItemBase } from '../lobby/game/core/sBoxItemBase';
import { assetMgr } from '../lobby/game/core/sAssetMgr';
import { sObjPool } from '../lobby/game/core/sObjPool';
import { sConfigMgr } from '../lobby/game/core/sConfigMgr';
import { sUtil } from '../lobby/game/core/sUtil';
const { ccclass, property } = _decorator;

 
@ccclass('StarlightPrincess_BoxItem')
export class StarlightPrincess_BoxItem extends sBoxItemBase {
    @property(Sprite)
    renderSprite : Sprite;
    @property(UITransform)
    renderTransform : UITransform;
    @property(Label)
    multiLabel : Label;

    @property(Node)
    SpineLayer : Node;
    @property(Node)
    winFrame : Node;
    @property(Node)
    disappearAnimation : Node;
    protected skeleton : sp.Skeleton;
    private isThundered = false;
    private isMultied = false;

    boxItemUpdate(renderData,symbolValue,boxState,tableState){
        if(renderData){
            if(renderData.type == 'img'){
                this.imgRenderSet(boxState,symbolValue,renderData,tableState);
            }else if(renderData.type == 'spine'){
                this.spineRenderSet(boxState,symbolValue,renderData,tableState);
            };


            if(symbolValue > 2000){
                if(renderData.multi&&!this.isMultied ){
                    this.multiLabel.node.active = true;
                    this.SetNodeFront(this.multiLabel.node);
                    this.multiLabel.string = renderData.multi + 'x';
                    //let pos=this.multiLabel.node.position;
                    //this.multiLabel.node.setPosition(pos.x,pos.y+2,pos.z);
                };
            }else{
                this.isMultied=false;
            };


            if(boxState == 'win'){
                if(symbolValue >2000){
                    this.isMultied = true;
                    director.emit('winMultiBox',{actionType : tableState,pos : this.multiLabel.node.worldPosition,multi : renderData.multi,fontSize : this.multiLabel.fontSize});
                    this.multiLabel.node.active = false;
                }else{
                    this.isMultied=false;
                    if(symbolValue!=1){
                        this.winFrame.active=true;
                        this.winFrame.getComponent(Sprite).color=color(255,255,255,0);
                        sUtil.TweenColor(color(255,255,255,255),this.winFrame.getComponent(Sprite),0.2,0);
                        this.scheduleOnce(()=>{
                            this.winFrame.active=false;
                            
                            this.scheduleOnce(()=>{
                                this.disappearAnimation.active=true;
                                this.disappearAnimation
                                this.disappearAnimation.getComponent(sp.Skeleton).setAnimation(0,"stpr_symbol_all_fx_disappearance",false);
                                this.disappearAnimation.getComponent(sp.Skeleton).setCompleteListener(()=>{
                                    this.disappearAnimation.active=false;
                                });
                                const boxDatab = this.getSymbolData(symbolValue);
                                renderData= boxDatab['out'];
                                this.skeleton.timeScale=2.5;
                                this.skeleton.setAnimation(0,renderData.animation_name,false);
                                this.skeleton.setCompleteListener(()=>{
                                    this.skeleton.node.active=false;
                                });
                            },0.2);
                        },1.4);
                        // this.scheduleOnce(()=>{
                            
                        // },0.2);
                        
                    };
                };
            }else{
                this.winFrame.active=false;
            };
        };
        // if(this.isMultied){f
        //     if(boxState == 'rolling'){
        //         this.isMultied = false;
        //     }
        // }
    };

    imgRenderSet(boxState,symbolValue,renderData,tableState){
        let renderSize : Size = null;
        const spriteFrame : SpriteFrame = assetMgr.GetAssetByName(renderData.render_name);
        this.renderSprite.node.active = true;
        this.renderSprite.spriteFrame = spriteFrame;
        // renderSize = size(spriteFrame.rect.width,spriteFrame.rect.height);
        // console.log('spriteFrame:'+spriteFrame.name +','+renderSize.x+','+renderSize.y);
        if(renderData.position){
            this.renderSprite.node.position = v3(renderData.position.x,renderData.position.y,0);
        };
        if(renderData.contentSize){
            // this.renderTransform.contentSize = size(renderData.contentSize.x,renderData.contentSize.y);
        }else{
            if(renderSize){
                // this.renderTransform.contentSize = renderSize;
            };
        };
        if(renderData.scale){
            this.renderSprite.node.scale = v3(renderData.scale,renderData.scale,renderData.scale);
        };
        // let pos=this.multiLabel.node.position;
        // this.multiLabel.node.setPosition(pos.x,pos.y+2,pos.z);
        if(boxState == 'win'){
            // this.renderSprite.node.scale = v3(2,2,2);
        };
    };

    spineRenderSet(rollState,symbolValue,renderData,tableState){
        if(this.skeleton){
            this.skeleton.node.active = false;
            sObjPool.Enqueue(renderData.render_name,this.skeleton);
            this.skeleton = null;
        };
        this.skeleton = sObjPool.Dequeue(renderData.render_name);
        if(this.skeleton){
            this.skeleton.node.setParent(this.SpineLayer,false);
            if(renderData.loop && renderData.loop == -1){
                this.skeleton.loop = true;
            }else{
                this.skeleton.loop = false;
            };
            if(symbolValue > 2000 && rollState=='win'){
                this.skeleton.getComponent(sp.Skeleton).timeScale=0.8;
            }else{
                this.skeleton.getComponent(sp.Skeleton).timeScale=1;
            };
            
            if(this.skeleton.animation=='out'){
                //this.skeleton.getComponent(sp.Skeleton).timeScale=0.1;
            };
            if(renderData.position){
                this.skeleton.node.position = v3(renderData.position.x,renderData.position.y,0);
            };
            // if(renderData.scale){
            //     if(this.skeleton.node.scale.x != renderData.scale){
            //         this.skeleton.node.scale = v3(renderData.scale,renderData.scale,1);
            //     }
            // }else{
            //     if(this.skeleton.node.scale.x != 1){
            //         this.skeleton.node.scale = v3(1,1,1);
            //     }
            // }
            this.skeleton.node.active = true;
            // if(renderData.timeScale){
            //     this.skeleton.timeScale = renderData.timeScale;
            // };
           
            this.skeleton.animation = renderData.animation_name;
            if(symbolValue <= 2000 && symbolValue != 1){
                if(this.skeleton.animation=='out'){
                    this.skeleton.setCompleteListener(()=>{
                        this.skeleton.node.active = false;
                    });
                };
            };
        }
        this.renderSprite.node.active = false;
    };

    clearItem(symbolValue, rollState, tableState) {
        this.cleanTweenList();
        this.multiLabel.node.active = false;
        this.renderSprite.color = Color.WHITE;
        if (this.skeleton) {
            this.skeleton.setCompleteListener(null);
            this.skeleton.node.active = false;
            sObjPool.Enqueue(this.skeleton.node.name, this.skeleton);
            this.skeleton = null;
        };
    };

    public getSymbolData(symbolValue){
        try{
            if(isNaN(symbolValue)){
                return sConfigMgr.instance.GetSymbolBoxImg(symbolValue.OLD);
            }else{
                return sConfigMgr.instance.GetSymbolBoxImg(symbolValue);
            };
        }catch(e){
            console.error('getSymbolData error' + e);
        };
    };

}

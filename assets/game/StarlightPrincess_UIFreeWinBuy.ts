
import { _decorator, Component, Node, Label, director, sp } from 'cc';
import { UIFreeWinBuy } from '../lobby/hyPrefabs/gameRes/external/UIFreeWinBuy';

const { ccclass, property } = _decorator;

@ccclass('StarlightPrincess_UIFreeWinBuy')
export class StarlightPrincess_UIFreeWinBuy extends UIFreeWinBuy {

    @property(Node)
    SpinNode : Node;

    private mBetType1 = -1;
    private mCoin1 = 0;
    private canBuy1 = false;
    protected onLoad() {
        director.on('sSlotBetInfoUpdate',(info)=>{
            console.log('sSlotBetInfoUpdate2',info);
            if(info){
                if(info.betAmount){
                    this.coinLabel.string = this.AddCommas(info.betAmount * 50*20);
                };
            };
        },this);
    }
    start () {
        this.startBtn.on('click',()=>{
            if(this.canBuy1){
                this.SpinNode.getComponent(sp.Skeleton).setAnimation(0,"stpr_buy_feature_center_yes",false);
                this.SpinNode.getComponent(sp.Skeleton).setCompleteListener(()=>{
                    this.scheduleOnce(()=>{
                        if(this.mBetType1 != -1){
                            director.emit('subGameBetActionBtnClick',this.mBetType1,this.mCoin1);
                        };
                        this.mainNode.active = false;
                    },0.5);
                    
                    this.SpinNode.getComponent(sp.Skeleton).setAnimation(0,"stpr_buy_feature_center_out",false);
                    this.SpinNode.getComponent(sp.Skeleton).setCompleteListener(()=>{
                        
                    });
                });
                
               
            }else{
                globalThis.goldNotenough && globalThis.goldNotenough();
            }
        },this);

        this.cancelNode.on('click',()=>{
            this.mBetType1 = -1;
            this.SpinNode.getComponent(sp.Skeleton).setAnimation(0,"stpr_buy_feature_center_no",false);
            this.scheduleOnce(()=>{
                this.mainNode.active = false;
                director.emit('subGameBetActionBtnCancel');
            },0.5);
            this.SpinNode.getComponent(sp.Skeleton).setCompleteListener(()=>{
                
                this.SpinNode.getComponent(sp.Skeleton).setAnimation(0,"stpr_buy_feature_center_out",false);
                this.SpinNode.getComponent(sp.Skeleton).setCompleteListener(()=>{
                   
                });
            });
        },this);

        // director.on('sSlotBetInfoUpdate',(info)=>{
        //     console.log('sSlotBetInfoUpdate2',info);
        //     if(info){
        //         if(info.betAmount){
        //             this.coinLabel.string = this.AddCommas(info.betAmount * 50*20);
        //         };
        //     };
        // },this); 
    }

    viewInit(coin,betType,_canBuy,_tipStr){
        //this.coinLabel.string = this.AddCommas(coin);
        this.mCoin1 = coin;
        this.mBetType1 = betType;
        this.canBuy1 = _canBuy;
        this.mainNode.active = true;

        this.SpinNode.getComponent(sp.Skeleton).setAnimation(0,"stpr_buy_feature_center_in",false);
        this.SpinNode.getComponent(sp.Skeleton).setCompleteListener(()=>{
            //this.SpinNode.getComponent(sp.Skeleton).setAnimation(0,"stpr_buy_feature_center_loop",false);
        });
        
    }
}


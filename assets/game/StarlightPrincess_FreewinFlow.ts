
import { _decorator, Component, director, Label, Node, tween, v3, Vec3 } from 'cc';
import { sFreeWinFlow } from '../lobby/game/core/sFreeWinFlow';
import { sConfigMgr } from '../lobby/game/core/sConfigMgr';
import { sBoxEntityAsset, sBoxMgr } from '../lobby/game/core/sBoxMgr';
import { sUtil } from '../lobby/game/core/sUtil';
import { sBoxEntity } from '../lobby/game/core/sBoxEntity';
import sAudioMgr from '../lobby/game/core/sAudioMgr';
const { ccclass, property } = _decorator;

 
@ccclass('StarlightPrincess_FreewinFlow')
export class StarlightPrincess_FreewinFlow extends sFreeWinFlow {
    @property(Node)
    effectLayer : Node;
    @property(Label)
    multiLabel : Label;
    @property(Label)
    leftFreeCountLabel : Label;

    private nowMode = 'normal';
    private multiValue = 0;
    private freeTotalRate = 0;

    private symbolToMulti = {
        2001 : 2,
        2002 : 3,
        2003 : 4,
        2004 : 5,
        2005 : 6,
        2006 : 8,
        2007 : 10,
        2008 : 12,
        2009 : 15,
        2010 : 20,
        2011 : 25,
        2012 : 50,
        2013 : 100,
        2014 : 250,
        2015 : 500
    }

    start() {
        super.start();
        director.on('freeWinBegain',()=>{
            this.multiLabel.string = '';
            this.multiValue = 0;
        },this);
        director.on('freeWinOver',()=>{
            this.multiLabel.string = '';
            this.multiValue = 0;
        },this);
        director.on('StarlightPrincessLabelEnd',()=>{
            this.multiLabel.string = this.multiValue + 'x';
            this.removeIDTween('multiLabel');
            this.pushIDTween(tween(this.multiLabel.node).to(0.15,{scale : v3(1.4,1.4,1.4)},{easing : 'sineOut'}).to(0.1,{scale : Vec3.ONE},{easing : 'sineIn'}).start(),'multiLabel');
        },this);

        director.on('freeWinAction',(value)=>{
            this.leftFreeCountLabel.string = (value < 0 ? 0 : value).toString();
        },this);

        this.scheduleOnce(()=>{
            //director.emit('StarlightTotleViewOpen',2000);
        },2);
    }

    clearUI() {
        super.clearUI();
        this.removeScheduleDic();
    }

    rollFirstRoundAction(round, speedMode, clickMode) {
        director.off('rollOneColumnBump');
        director.off('byWayWinLightEnd');
        director.off('titleWinSettleEnd');
        director.off('rollStopOneRoundCall');
        this.freeWinIndex = 0;
        this.freeTotalRate = 0;
        this.tatalWin=0;
        if(round){
            let timestop=0;
            const round_symbol_map = round['round_symbol_map'];
            for(let xx of round_symbol_map[0]){
                for(let yy=0;yy<xx.length;yy++){
                    //console.log('symbel:',xx[yy]);
                    if(yy>=1&&yy<=5&&xx[yy]>2000){
                        timestop=0;
                        director.emit('controlHuaXianZi','NormalIdle');
                        break;
                    };
                };
                if(timestop>0)break;
            };
            this.normalFlowDelayCall(() => {

                this.scheduleOnce(()=>{
                    let needWinLight = round.rate > 0 ? true : false;
                    const round_symbol_map = round['round_symbol_map'];
                    sConfigMgr.instance.SetResData(round_symbol_map);
                    const events = sConfigMgr.instance.GetAllEventConfigByHitListOnCheck(round);
                    if(events){
                        const lastEvents = events[events.length - 1];
                        for (let i = 0; i < lastEvents.length; i++) {
                            const _event = lastEvents[i];
                            if (_event.event_type == 'changeRound') {
                                if(_event['round_change']){
                                    this.freeWinCount = _event['round_change'].num;
                                    
                                }
                                this.leftFreeCountLabel.string = (this.freeWinCount - this.freeWinIndex).toString();
                                
                            };
                        };
                    };
    
    
                    director.on('rollOneColumnBump',(col)=>{
                        //sAudioMgr.PlayShotAudio('luodi',0.4);
                        const colBoxs = sBoxMgr.instance.GetColAllBox(col);
                        let shunxu=0;
                        let shunxu1=0;
                        if(colBoxs){
                            for (let rY = 0; rY < colBoxs.length; rY++) {
                                const rSymbol = colBoxs[rY];
                                if(rSymbol.SymbolValue > 2000){
                                    if(rSymbol.ViewItemIndex.y >= 0 && rSymbol.ViewItemIndex.y <= 4){
                                        rSymbol.DoEnitityAction('thunder');
                                        console.log('召唤一道落雷1')
                                        if(rSymbol.SymbolValue == 1){
                                            shunxu1++;
                                            if(shunxu1==1){
                                                director.emit('controlHuaXianZi','NormalIdle');
                                                this.playmultMp();
                                            };
                                        };
                                    };
                                }else if(rSymbol.SymbolValue == 1){
                                    shunxu++;
                                    sAudioMgr.PlayShotAudio('scatter_appear'+shunxu);
                                    if(shunxu==1){
                                        this.playscatterMp();
                                    };
                                };;
                            };
                        };
                    },this);
    
                    sUtil.once('byWayWinLightEnd',()=>{
                        sUtil.once('titleWinSettleEnd',()=>{
                            const rate = round.rate;
                            this.freeTotalRate += rate;
                            if(rate > 200){
                                
                                this.scheduleOnce(()=>{
                                    director.emit('StarlightPrincessBigWin',rate);
                                
                                    console.log('大于200：无加倍倍数显示2',rate)
                                    director.emit('betUserInfoUpdateWinAnima',rate-this.tatalWin);
                                },0.8);
                                sUtil.once('StarlightPrincessBigWinEnd',()=>{
                                    
                                    sBoxMgr.instance.setAllViewTargetBoxArrayState(id =>id == 1,'win','settlement');
                                    sAudioMgr.PlayShotAudio('scatter_anima',0.8);
                                    sAudioMgr.bgMusicOut(0.3,0);
                                    this.scheduleOnce(() => {
                                        sAudioMgr.PlayBG('bgm_interlude');
                                        director.emit('freeWinBingo', this.freeWinCount,round.rate);
                                    }, 3);
                                });
                            }else{
                                //director.emit('betUserInfoUpdateWinAnima',this.freeTotalRate);
                                sBoxMgr.instance.setAllViewTargetBoxArrayState(id =>id == 1,'win','settlement');
                                sAudioMgr.PlayShotAudio('scatter_anima',0.8);
                                sAudioMgr.bgMusicOut(0.3,0);
                                this.scheduleOnce(() => {
                                    sAudioMgr.PlayBG('bgm_interlude');
                                    director.emit('freeWinBingo', this.freeWinCount,round.rate);
                                }, 3);
                            }
                        });
                        director.emit('winBetRes',-1);
                    });
    
                    sUtil.once('rollStopOneRoundCall', () => {
                        director.off('rollOneColumnStop');
                        director.off('rollOneColumnBump');
                        if(needWinLight){
    
                            this.byWayBoxLightCtr(round,'normal');
                        }else{
                            sBoxMgr.instance.setAllViewTargetBoxArrayState(id =>id == 1,'win','settlement');
                            sAudioMgr.PlayShotAudio('scatter_anima',0.8);
                            sAudioMgr.bgMusicOut(0.3,0);
                                this.scheduleOnce(() => {
                                    sAudioMgr.PlayBG('bgm_interlude');
                                    director.emit('freeWinBingo', this.freeWinCount,0);
                                }, 3);
                            }
                    });
        
                    if (speedMode == 'turbo') {
                        sBoxMgr.instance.rollStopAllColumn();
                    } else if (speedMode == 'normal') {
                        sBoxMgr.instance.rollStopAllColumn();
                    }
                },timestop);
                


            });
        }
    }

    rollStopAction(round, rollSpeedMode, clickMode) {
        this.nowMode = 'freeWin';
        director.off('rollOneColumnBump');
        director.off('byWayWinLightEnd');
        director.off('titleWinSettleEnd');
        director.off('rollStopOneRoundCall');
        if(round){
            let timestop=0;
            const round_symbol_map = round['round_symbol_map'];
            for(let xx of round_symbol_map[0]){
                for(let yy=0;yy<xx.length;yy++){
                    //console.log('symbel:',xx[yy]);
                    if(yy>=1&&yy<=5&&xx[yy]>2000){
                        timestop=0;
                        director.emit('controlHuaXianZi','NormalIdle');
                        break;
                    };
                };
                if(timestop>0)break;
            };

            this.normalFlowDelayCall(() => {
                this.scheduleOnce(()=>{
                    this.tatalWin=0;
                    let needWinLight = round.rate > 0 ? true : false;
                    const round_symbol_map = round['round_symbol_map'];
                    sConfigMgr.instance.SetResData(round_symbol_map);
                    // const events = sConfigMgr.instance.GetAllEventConfigByHitListOnCheck(round);
                    // if(events){
                    //     const lastEvents = events[events.length - 1];
                    //     for (let i = 0; i < lastEvents.length; i++) {
                    //         const _event = lastEvents[i];
                    //         if (_event.event_type == 'changeRound') {
                    //             this.freeWinCount = _event['round_change'].num;
                    //         }
                    //     }
                    // }
                    const allEvents = sConfigMgr.instance.GetAllEventConfigByHitListOnCheck(round);
                    let freeCount = 0;
                    if (allEvents && allEvents.length > 0) {
                        for (let e = 0; e < allEvents.length; e++) {
                            const eventArr = allEvents[e];
                            for (let f = 0; f < eventArr.length; f++) {
                                const _fEvent = eventArr[f];
                                if(e == allEvents.length - 1){
                                    if(_fEvent.event_type == 'changeRound'){
                                        if(_fEvent['round_change']){
                                            freeCount = _fEvent['round_change'].num;
                                        };
                                        
                                    };
                                };
                                // if(_fEvent.event_type == 'boxAnima'){
                                //     needWinLight = true;
                                // }
                            };
                        };
                    };
    
    
                    director.on('rollOneColumnBump',(col)=>{
                        sAudioMgr.PlayShotAudio('luodi',0.4);
                        const colBoxs = sBoxMgr.instance.GetColAllBox(col);
                        let shunxu=0;
                        let shunxu1=0;
                        if(colBoxs){
                            for (let rY = 0; rY < colBoxs.length; rY++) {
                                const rSymbol = colBoxs[rY];
                                if(rSymbol.SymbolValue > 2000 ){
                                    if(rSymbol.ViewItemIndex.y >= 0 && rSymbol.ViewItemIndex.y <= 4){
                                        rSymbol.DoEnitityAction('thunder');
                                        console.log('召唤一道落雷2')
                                        if(rSymbol.SymbolValue == 1){
                                            shunxu1++;
                                            if(shunxu1==1){
                                                director.emit('controlHuaXianZi','NormalIdle');
                                                this.playmultMp();
                                            };
                                        };
                                    }
                                }else if(rSymbol.SymbolValue == 1){
                                    shunxu++;
                                    sAudioMgr.PlayShotAudio('scatter_appear'+shunxu);
                                    if(shunxu==1){
                                        this.playscatterMp();
                                    };
                                };
                            }
                        }
                    },this);
    
                    sUtil.once('byWayWinLightEnd',()=>{
                        sUtil.once('titleWinSettleEnd',()=>{
                            const rate = round.rate;
                            this.freeTotalRate += rate;
                            if(rate >= 200){
                                this.scheduleOnce(()=>{
                                    director.emit('StarlightPrincessBigWin',rate);
                                
                                    console.log('大于200：无加倍倍数显示2',rate)
                                    director.emit('betUserInfoUpdateWinAnima',rate-this.tatalWin);
                                },0.8);
                                
                                sUtil.once('StarlightPrincessBigWinEnd',()=>{
                                    if(freeCount > 0){
                                        sUtil.once('bingoFreewinInFreeEnd',()=>{
                                            this.freeWinCount += freeCount;
                                            this.leftFreeCountLabel.string = (this.freeWinCount - this.freeWinIndex).toString();
                                            this.scheduleOnce(() => {
                                                //console.log('免费游戏round结束freeCount1')
                                            director.emit('freeWinOneRoundEnd');
                                            }, 0.5);
                                        });
                                        sAudioMgr.PlayShotAudio('scatter_anima');
                                        sBoxMgr.instance.setAllViewTargetBoxArrayState(id =>id == 1,'win','settlement');
                                        this.scheduleOnce(()=>{
                                            director.emit('bingoFreewinInFree',freeCount);
                                        },2.5)
                                        
                                    }else{
                                        this.scheduleOnce(() => {
                                            //console.log('免费游戏round结束freeCount2')
                                            director.emit('freeWinOneRoundEnd');
                                        }, 0.5);
                                    };
                                });
                            }else{
                                //director.emit('betUserInfoUpdateWinAnima',this.freeTotalRate);
                                if(freeCount > 0){
                                    sUtil.once('bingoFreewinInFreeEnd',()=>{
                                        this.freeWinCount += freeCount;
                                        this.leftFreeCountLabel.string = (this.freeWinCount - this.freeWinIndex).toString();
                                        this.scheduleOnce(() => {
                                            console.log('免费游戏round结束freeCount1')
                                            director.emit('freeWinOneRoundEnd');
                                        }, 0.5);
                                    });
                                    sAudioMgr.PlayShotAudio('scatter_anima');
                                        sBoxMgr.instance.setAllViewTargetBoxArrayState(id =>id == 1,'win','settlement');
                                        this.scheduleOnce(()=>{
                                            director.emit('bingoFreewinInFree',freeCount);
                                        },2.5)
                                }else{
                                    this.scheduleOnce(() => {
                                        console.log('免费游戏round结束freeCount2')
                                        director.emit('freeWinOneRoundEnd');
                                    }, 0.5);
                                };
                                
                            };
    
                            
                          
                        });
                        director.emit('winBetRes',-1);
                    });
    
                    sUtil.once('rollStopOneRoundCall', () => {
                        director.off('rollOneColumnStop');
                        director.off('rollOneColumnBump');
                        if(needWinLight){
                            this.byWayBoxLightCtr(round,'normal',false);
                        }else{
                            if(freeCount > 0){
                                sUtil.once('bingoFreewinInFreeEnd',()=>{
                                    this.freeWinCount += freeCount;
                                    this.leftFreeCountLabel.string = (this.freeWinCount - this.freeWinIndex).toString();
                                    this.scheduleOnce(() => {
                                        console.log('免费游戏round结束3')
                                        director.emit('freeWinOneRoundEnd');
                                    }, 0.5);
                                });
                                sAudioMgr.PlayShotAudio('scatter_anima');
                                sBoxMgr.instance.setAllViewTargetBoxArrayState(id =>id == 1,'win','settlement');
                                this.scheduleOnce(()=>{
                                    director.emit('bingoFreewinInFree',freeCount);
                                },2.5);
                            }else{
                                this.scheduleOnce(() => {
                                    console.log('免费游戏round结束4')
                                    director.emit('freeWinOneRoundEnd');
                                }, 0.5);
                            }
                        }
                    });
        
                    sBoxMgr.instance.rollStopAllColumn();
                },timestop);
                


            });
        }
    }

    tatalWin=0;
    byWayBoxLightCtr(round,rollSpeedMode,isFirst = true) {
        let boxWinIndex = 1;
        director.off('dropAllBoxFinish');
        const eventArr = sConfigMgr.instance.GetAllEventOptConfigByHitListOnCheck(round);
        // console.log('eventArr:'+JSON.stringify(eventArr));
        const boxSum = sBoxMgr.instance.boxSum;
        let isTotalWin = false;
        let isEnd = false;
        let IsMulit=false;
        if (eventArr && Array.isArray(eventArr)) {
            let eventIndex = 0;
            const boxAnimaCall = () => {
                if (eventIndex < eventArr.length) {
                    isEnd = eventIndex == (eventArr.length - 1);
                    const nowData = eventArr[eventIndex++];
                    if (nowData && nowData.eventData && nowData.eventData.length > 0) {
                        let isWin = false;
                        const events = nowData.eventData;
                        let resAct = {};
                        let winSymbol = {};
                        let winRate = 0;
                        for (let i = 0; i < events.length; i++) {
                            const _event = events[i];
                            if (_event.event_type == 'boxAnima') {
                                if(_event.rate_change && _event.rate_change.num){
                                    winRate += _event.rate_change.num;
                                }
                                if(_event.bonus_symbol){
                                    winSymbol[_event.bonus_symbol] = true;
                                }
                                isWin = true;
                                isTotalWin = true;
                                let act = resAct[_event.bonus_symbol];
                                if (!act) {
                                    act = {};
                                    act.bonus_symbol = _event.bonus_symbol;
                                    act.act_pos = {};
                                    act.rate = 0;
                                }
                                if (_event.rate_change) {
                                    if (act.rate == 0) {
                                        act.rate = _event.rate_change.num;
                                    } else {
                                        if (_event.rate_change.type == 0) {
                                            act.rate += _event.rate_change.num;
                                        } else if (_event.rate_change.type == 1) {
                                            act.rate *= _event.rate_change.num;
                                        }
                                    }
                                }
                                if (_event.act_pos) {
                                    const poss = _event['act_pos'].split(',');
                                    if (poss) {
                                        for (let a = 0; a < poss.length; a++) {
                                            const value = poss[a];
                                            act.act_pos[value] = true;
                                        }
                                    }
                                }
                                resAct[_event.bonus_symbol] = act;
                            }else if(_event.event_type == 'changeRound'){
                                //isWin = true;
                                
                                if(_event['rate_change']){
                                    isTotalWin = true;
                                    winRate+=_event['rate_change'].num
                                }
                                
                            };
                        };
                        
                       

                        const resActKeys = Object.keys(resAct);
                        let allActPos = {};
                        if (resActKeys) {
                            for (let i = 0; i < resActKeys.length; i++) {
                                const element = resAct[resActKeys[i]];
                                const itemKeys = Object.keys(element.act_pos);
                                director.emit('ws',{symbol : element.bonus_symbol,rate : element.rate, num : itemKeys.length,boxPos : itemKeys[Math.trunc(itemKeys.length / 2)]});
                                for (let j = 0; j < itemKeys.length; j++) {
                                    const key = itemKeys[j];
                                    allActPos[key] = true;
                                };
                                director.emit('slotWinBoxItemInfo',{symbol : element.bonus_symbol,rate : element.rate, num : itemKeys.length,boxPos : itemKeys[Math.trunc(itemKeys.length / 2)]});
                                // this.byWayboxResWinAction(element);
                            };
                            // let boxActArr: any[][] = [];
                            for (let i = 0; i < sBoxMgr.instance.boxArray.length; i++) {
                                const arr = sBoxMgr.instance.boxArray[i];
                                for (let m = 0; m < arr.length; m++) {
                                    if(arr[m].ViewItemIndex.y >= 0 && arr[m].ViewItemIndex.y <= 4){
                                        //console.log('symbolvalue:',sBoxMgr.instance.boxArray,i,m,sBoxMgr.instance.getBoxEntityByXY(i,m),sBoxMgr.instance.getBoxEntityByXY(i,m).SymbolValue);
                                        if(arr[m].SymbolValue>2000){
                                            IsMulit=true;
                                            console.log('加倍存在：',IsMulit);
                                            break;
                                        };
                                    };
                                };
                                if(IsMulit){
                                    break;
                                };
                            };

                            const allKeys = Object.keys(allActPos);
                            let xx=0;
                            for (let i = 0; i < allKeys.length; i++) {
                                const element = allKeys[i];
                                const act = element.split('_');
                                if (act && act.length == 2) {
                                    const act1 = parseInt(act[0]);
                                    const act2 = parseInt(act[1]);
                                    //console.log('消除的格子',act1,act2);
                                    if (act1 && act2 || act1 == 0 || act2 == 0) {
                                        if(sBoxMgr.instance.getBoxEntityByXY(act1,act2).SymbolValue>2000){
                                            xx++;
                                            this.scheduleOnce(()=>{
                                                console.log('星星图案mult',act1,act2)
                                                sBoxMgr.instance.updateBoxDataByXY(act1, act2, 'win', 'settlement');
                                            },0.1*xx);
                                        }else{
                                            sBoxMgr.instance.updateBoxDataByXY(act1, act2, 'win', 'settlement');
                                        };
                                    };
                                };
                            };
                            director.emit('winBetRes',winRate);
                            if(round.rate<200&&!IsMulit){
                                console.log('小于200：无加倍倍数显示3',winRate)
                                director.emit('betUserInfoUpdateWinAnima',winRate);
                            }else if(!IsMulit){
                                if(eventIndex != (eventArr.length - 1)){
                                    this.tatalWin+=winRate;
                                    director.emit('betUserInfoUpdateWinAnima',winRate);
                                };
                            }else if(IsMulit){
                                this.tatalWin+=winRate;
                                director.emit('betUserInfoUpdateWinAnima',winRate);
                            };
                            
                            if (isWin) {
                                const winSymbolKeys = Object.keys(winSymbol);
                                if(winSymbolKeys && winSymbolKeys.length > 0){
                                    for (let i = 0; i < winSymbolKeys.length; i++) {
                                        const winSymbolKey = winSymbolKeys[i];
                                    };
                                };
                                // this.scheduleOnce(()=>{
                                //     sAudioMgr.PlayShotAudio('boxWinTurnRound');
                                // },1);
                                // director.emit('winBetRateRes',winRate * this.getMultiple(eventIndex - 1));
                                sBoxMgr.instance.blackCurtainAnima();
                                if(boxWinIndex > 10){
                                    boxWinIndex = 10; 
                                }
                                //this.scheduleOnce(() => {
                                    sAudioMgr.PlayAudio('freegame_small_win');
                                    //sAudioMgr.PlayShotAudio('small_win_effect');
                                    
                                    this.scheduleOnce(()=>{
                                        sAudioMgr.StopAudio();
                                        sAudioMgr.PlayShotAudio('freegame_small_win_end'+eventIndex);
                                        
                                        this.scheduleOnce(()=>{
                                            sAudioMgr.PlayShotAudio('symbol_clear'+eventIndex);
                                        },1);
                                    },0.6);
                                    sBoxMgr.instance.blackCurtainAnima(-1, false);
                                //}, 0.5);
                                
                                this.scheduleOnce(() => {
                                    // director.emit('titleSumLightMsg','normal',eventIndex);
                                    sConfigMgr.instance.TurnNextResRoundData();
                                    sBoxMgr.instance.ReplenishBoxEntity();
                                    let xx=0;
                                    sBoxMgr.instance.DropAllEntityBoxAfterRemove((strikeBox : sBoxEntity)=>{
                                        sAudioMgr.PlayShotAudio('boxDropStrike');
                                        if(strikeBox){
                                            if(strikeBox.SymbolValue > 2000 ){
                                                if(strikeBox.ViewItemIndex.y > 4 ){
                                                    xx++;
                                                    strikeBox.DoEnitityAction('thunder');
                                                    console.log('召唤一道落雷3')
                                                    if(xx==1){
                                                        director.emit('controlHuaXianZi','NormalIdle');
                                                    };
                                                };
                                            };
                                        };
                                    },0.02);
                                },2.5);
                            };
                        };
                    };
                    if(isEnd){
                        if(isTotalWin && nowData.opt && nowData.opt.multiData && nowData.opt.multiData.length > 0){
                            let multiValue = this.multiValue;
                            const multiboxes = [];
                            for (let o = 0; o < nowData.opt.multiData.length; o++) {
                                const multiData = nowData.opt.multiData[o];
                                if(multiData.multi > 0){
                                    multiValue += multiData.multi;
                                }
                             
                                if(multiData && multiData.pos && multiData.pos.length > 0){
                                    for (let l = 0; l < multiData.pos.length; l++) {
                                        const mPos = multiData.pos[l];
                                        if(mPos){
                                            const mPosArr = mPos.split('_');
                                            if(mPosArr && mPosArr.length == 2){
                                                multiboxes.push(sBoxMgr.instance.getBoxEntityByXY(parseInt(mPosArr[0]),parseInt(mPosArr[1])));
                                            };
                                        };
                                    };
                                };
                            };
                            if(multiboxes.length > 0){
                                const mulCall = ()=>{
                                    let mulIndex = 0;
                                    this.pushOneSchedule(()=>{
                                        const mBox : sBoxEntity = multiboxes[mulIndex++];
                                        if(mBox){
                                            mBox.UpdateData('win',mulIndex == multiboxes.length ? 'close' : 'open');
                                        }
                                    },0,multiboxes.length,1.5,0,true);
                                    this.scheduleOnce(()=>{
                                        if(round.rate>200){

                                        }else{
                                            console.log('小于200：有加倍倍数显示4',round.rate)
                                            director.emit('betUserInfoUpdateWinAnima',round.rate-this.tatalWin);
                                        };
                                        director.emit('byWayWinLightEnd');
                                    },multiboxes.length * 1.5);
                                };
                                if(multiValue > 0){
                                    if(this.multiValue > 0){
                                        sAudioMgr.PlayShotAudio('fs_Multifly');
                                        this.playtotalMultMp3();
                                        let str=this.multiLabel.string.slice(0,this.multiLabel.string.length);
                                        director.emit('winMultiBox',{actionType : 'open',pos : this.multiLabel.node.worldPosition,multi : parseInt(str),fontSize : 25});
                                    }; 
                                    this.scheduleOnce(mulCall,this.multiValue == 0 ? 0 : 1.5);
                                }else{
                                    mulCall();
                                };
                            }else{
                                director.emit('byWayWinLightEnd');
                            };
                            if(!isFirst){
                                this.multiValue = multiValue;
                            };
                        }else{
                            //if (eventArr.length > 1) {
                                director.emit('byWayWinLightEnd');
                            //};
                        };
                    };
                };
            };
            boxAnimaCall();
            director.on('dropAllBoxFinish', ()=>{
                // console.log('dropAllBoxFinish');
                boxAnimaCall();
                // console.log('isEnd:',isEnd);
            }, this);
        }
    }

    protected freeWinTotleViewOpen(winTotalCoin,winRealTotalCoin){
        director.emit('StarlightTotleViewOpen',winRealTotalCoin,this.freeWinCount);

        // if(this.totalWinPrefab){
        //     const obj = instantiate(this.totalWinPrefab);
        //     obj.setParent(this.node.parent);
        //     obj.getComponent(UIBase).DataInit({coin:winTotalCoin,count : this.freeWinCount});
        //     director.emit('freeWinEndRes',winTotalCoin,winRealTotalCoin);
        // }else{
        //     director.emit('freeWinOver');
        //     director.emit('rollStop');
        // }
    }
    playscatterMp(){
        let num=sUtil.RandomInt(0,11);
        if(num>=0&&num<9){
            sAudioMgr.PlayShotAudio('scatter_appear_voice'+num);
        }
    }
    playmultMp(){
        let num=sUtil.RandomInt(1,11);
        if(num>=0&&num<10){
            sAudioMgr.PlayShotAudio('mult_appear_voice'+num);
        }
    }

    playtotalMultMp3(){
        let num=sUtil.RandomInt(1,8);
        if(num>=0&&num<9){
            sAudioMgr.PlayShotAudio('total multiplier_voice'+num);
        };
    }
}

import { _decorator, Button, Component, director, Label, Node, Sprite, tween, v2, v3, Vec3 } from 'cc';
import { sComponent } from '../lobby/game/core/sComponent';
import { sObjPool } from '../lobby/game/core/sObjPool';
import { assetMgr } from '../lobby/game/core/sAssetMgr';
import { sConfigMgr } from '../lobby/game/core/sConfigMgr';
import { sUtil } from '../lobby/game/core/sUtil';
const { ccclass, property } = _decorator;

 
@ccclass('StarlightPrincess_History')
export class StarlightPrincess_History extends sComponent {
    @property(Node)
    itemsNode : Node;
    @property(Button)
    upBtn : Button;
    @property(Button)
    downBtn : Button;

    @property(Sprite)
    upSprite : Sprite;
    @property(Sprite)
    downSprite : Sprite;

    private itemsList : Node[] = [];
    private viewItenMaxNum =5;

    private rollIndex = 0;

    private touchEnable = false;

    protected start () {
        director.on('slotSubGameRollBegin',()=>{
            this.removeAllItem();
            this.removeIDTween('itemsNode');
            this.touchEnable = false;
            this.rollIndex = 0;
            this.itemsNode.position = Vec3.ZERO;
            this.btnStateCtr();
        },this);

        director.on('slotWinBoxItemInfo',(obj)=>{
            if(obj){
                this.createItem(obj.symbol,obj.rate,obj.num);
            };
        },this);

        this.upBtn.node.on('click',()=>{
            if(this.touchEnable){
                this.ScrollItemView(1);
            };
        },this);

        this.downBtn.node.on('click',()=>{
            if(this.touchEnable){
                this.ScrollItemView(-1);
            };
        },this);

      
    }

    private createItem(symbol,rate,num){
        this.touchEnable = false;
        // if(this.itemsList.length >= 5){
        //     for (let i = 0; i < this.itemsList.length; i++) {
        //         const tItem = this.itemsList[i];
        //         this.removeIDTween('createItem_'+tItem.uuid);
        //         this.pushIDTween(tween(tItem).to(0.2,{position : v3(0,(4 - this.itemsList.length + i) * 46,0)},{easing : 'cubicIn'}).start(),'createItem_'+tItem.uuid);
        //     }
        // }
        if(this.itemsList.length >= this.viewItenMaxNum){
            this.removeIDTween('itemsNode');
            this.pushIDTween(tween(this.itemsNode).to(0.2,{position : v3(0,(this.viewItenMaxNum - 1 - this.itemsList.length) * 40,0)}).start(),'itemsNode');
        }
        const historyItem : Node = sObjPool.Dequeue('historyItem');
        if(historyItem && historyItem.isValid){
            historyItem.parent = this.itemsNode;
            // const index = this.itemsList.length >= 4 ? 4 : this.itemsList.length;
            const index = this.itemsList.length;
            historyItem.position = v3(0,240 + index * 40+15,0);
            historyItem.active = true;
            const renderData = sConfigMgr.instance.GetSymbolBoxImg(symbol);
            if(renderData){
                const icon = historyItem.getChildByName('icon');
                if(icon){
                    icon.getComponent(Sprite).spriteFrame = assetMgr.GetAssetByName(renderData.rest.render_name);
                };
                //console.log('history:',this.AddCommas(Math.floor(rate * globalThis.GameBtnEntity.CurrentBetAmount)));
                historyItem.getChildByName('num').getComponent(Label).string = num;
                historyItem.getChildByName('moy').getComponent(Label).string = this.AddCommas(Math.floor(rate * globalThis.GameBtnEntity.CurrentBetAmount))
                //sUtil.AddCommas(rate * globalThis.GameBtnEntity.CurrentBetAmount);
            };
            this.removeIDTween('createItem_'+historyItem.uuid);
            this.pushIDTween(tween(historyItem).delay(index * 0.05).to(0.3,{position : v3(0,index * 40+15,0)},{easing : 'cubicIn'})
            .by(0.1,{position : v3(0,10,0)},{easing:'cubicOut'})
            .by(0.1,{position : v3(0,-10,0)},{easing:'cubicIn'}).call(()=>{
                this.btnStateCtr();
                this.touchEnable = true;
            }).start(),'createItem_'+historyItem.uuid);
            this.itemsList.push(historyItem);
            this.rollIndex = this.viewItenMaxNum - this.itemsList.length;
        };
       
    }

    private removeAllItem(){
        for (let i = 0; i < this.itemsList.length; i++) {
            const tItem = this.itemsList[i];
            this.removeIDTween('createItem_'+tItem.uuid);
            this.pushIDTween(tween(tItem).delay(i * 0.05).by(0.3,{position : v3(0,-200,0)},{easing : 'cubicIn'})
            .call(()=>{
                sObjPool.Enqueue('historyItem',tItem);
                tItem.active = false;
            })
            .start(),'createItem_'+tItem.uuid);
        }
        this.itemsList = [];
    }

    private ScrollItemView(value){
        this.touchEnable = false;
        this.rollIndex += value;
        if(this.rollIndex > 0){
            this.rollIndex = 0;
        }else if(this.rollIndex < this.viewItenMaxNum - this.itemsList.length){
            this.rollIndex = this.viewItenMaxNum - this.itemsList.length;
        }
        this.removeIDTween('itemsNode');
        this.pushIDTween(tween(this.itemsNode).to(0.2,{position : v3(0,this.rollIndex * 40,0)}).call(()=>{
            this.touchEnable = true;
            this.btnStateCtr();
        }).start(),'itemsNode');
    }

    private btnStateCtr(){
        this.upSprite.grayscale = true;
        this.downSprite.grayscale = true;
        this.upBtn.interactable = false;
        this.downBtn.interactable = false;
        if(this.itemsList && this.itemsList.length > this.viewItenMaxNum){
            if(this.itemsNode.position.y >= 0){
                this.downSprite.grayscale = false;
                this.downBtn.interactable = true;
            }else if(this.itemsNode.position.y <= (this.viewItenMaxNum - this.itemsList.length) * 40){
                this.upSprite.grayscale = false;
                this.upBtn.interactable = true;
            }else{
                this.upSprite.grayscale = false;
                this.downSprite.grayscale = false;
                this.upBtn.interactable = true;
                this.downBtn.interactable = true;
            }
        }
    }


}
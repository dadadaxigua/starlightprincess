
import { _decorator, Component, director, Label, Node, tween, Vec3 } from 'cc';
import { sSoltFreeWinBuyCtr } from '../lobby/game/core/sSoltFreeWinBuyCtr';
const { ccclass, property } = _decorator;

@ccclass('StarlightPrincess_FreeWinBuyCtr')
export class StarlightPrincess_FreeWinBuyCtr extends sSoltFreeWinBuyCtr {
    @property(Label)
    coinLabel : Label;
 
    private touchEnabel = true;

    protected onLoad() {
        director.on('sSlotBetInfoUpdate',(info)=>{
            console.log('sSlotBetInfoUpdate1',info);
            if(info){
                if(info.betAmount){
                    this.coinLabel.string = this.AddCommas(info.betAmount * 50*20);
                };
            };
        },this);
    }

    protected start() {
        super.start();

        this.freeWinButton.node.on('click',()=>{
            //director.emit('slotJackPotBingo',1004223);
            if(this.touchEnabel){
                this.touchEnabel = false;
                this.cleanTweenList();
                // this.node.scale = Vec3.ONE;
                // this.pushOneTween(tween(this.node).to(0.15,{scale : Vec3.ZERO}).start());
            }
        },this);

        director.on('subGameBetActionBtnCancel',()=>{
            this.touchEnabel = true;
            this.cleanTweenList();
            // this.pushOneTween(tween(this.node).to(0.15,{scale : Vec3.ONE}).start());
        },this);

        director.on('subGameBetActionBtnClick',()=>{
            this.touchEnabel = true;
            this.cleanTweenList();
            // this.pushOneTween(tween(this.node).to(0.15,{scale : Vec3.ONE}).start());
        },this);
    }
}


import { _decorator, Component, director, Node, sp } from 'cc';
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = StarlightPrincess_Huaxianzi
 * DateTime = Thu Sep 21 2023 13:46:33 GMT+0800 (中国标准时间)
 * Author = dadadaxigua
 * FileBasename = StarlightPrincess_Huaxianzi.ts
 * FileBasenameNoExtension = StarlightPrincess_Huaxianzi
 * URL = db://assets/game/StarlightPrincess_Huaxianzi.ts
 * ManualUrl = https://docs.cocos.com/creator/3.4/manual/zh/
 *
 */
 
@ccclass('StarlightPrincess_Huaxianzi')
export class StarlightPrincess_Huaxianzi extends Component {
    // [1]
    // dummy = '';

    // [2]
    @property(Node)
    spawnAnima:Node
    // @property
    // serializableDummy = 0;
    private arr1=["stpr_character_01","stpr_character_action"];
    private arr2 =["stpr_character_02"];
    private arr3=["stpr_character_03_win"];

    start(){

        director.on('controlHuaXianZi',(str)=>{
            if(str=='NormalAction'){
                this.node.children[3].active=true;
                this.node.children[3].getComponent(sp.Skeleton).setAnimation(0,"stpr_character_ol_fx",false);
                this.node.children[3].getComponent(sp.Skeleton).setCompleteListener(()=>{
                    this.node.children[3].active=false;
                });

                this.node.children[0].active=true;
                this.node.children[1].active=false;
                this.node.children[2].active=false;
                this.scheduleOnce(()=>{
                    let strs=this.node.children[0].getComponent(sp.Skeleton).animation;
                    let num=1;
                    if(strs=="stpr_character_action"){
                        num=0;
                    };
                    this.node.children[0].getComponent(sp.Skeleton).animation=this.arr1[num];
                    this.node.children[0].active=true;
                    this.node.children[1].active=false;
                    this.node.children[2].active=false;
                },0.2);
            }else if(str=='NormalIdle'){
                this.scheduleOnce(()=>{
                    this.node.children[3].active=false;
                    this.node.children[0].active=true;
                    this.node.children[1].active=false;
    
                    this.node.children[3].active=true;
                    this.node.children[3].getComponent(sp.Skeleton).setAnimation(0,"stpr_character_ol_fx",false);
                    this.node.children[3].getComponent(sp.Skeleton).setCompleteListener(()=>{
                        this.node.children[3].active=false;
                    });
                    this.scheduleOnce(()=>{
                        this.node.children[0].active=false;
                        this.node.children[2].active=true;
                        this.node.children[2].getComponent(sp.Skeleton).setAnimation(0,this.arr3[0],true);
                    },0.25);
                    this.scheduleOnce(()=>{
                        this.node.children[3].active=true;
                        this.node.children[3].getComponent(sp.Skeleton).setAnimation(0,"stpr_character_ol_fx",false);
                        this.node.children[3].getComponent(sp.Skeleton).setCompleteListener(()=>{
                            this.node.children[3].active=false;
                        });

                        this.scheduleOnce(()=>{
                            this.node.children[1].active=false;
                            this.node.children[2].active=false;
                            this.node.children[0].active=true;
                            this.node.children[0].getComponent(sp.Skeleton).setAnimation(0.1,"stpr_character_01",true);
                        },0.25);
                    },1);
                },0.3);
                
                

               
            }else if(str=='FreeSpin'){
                this.node.children[1].getComponent(sp.Skeleton).animation=this.arr2[0];
                this.node.children[0].active=false;
                this.node.children[1].active=false;
                this.node.children[2].active=false;

                this.node.children[3].active=true;
                this.node.children[3].getComponent(sp.Skeleton).setCompleteListener(()=>{
                    this.node.children[3].active=false;
                });

                this.node.children[1].active=true;
                this.node.children[0].active=false;
                this.node.children[2].active=false;
            };;
        },this);
    };
};


import { _decorator, Button, Component, director, instantiate, JsonAsset, Label, Layout, Node, Prefab, Sprite, Tween, tween, UIOpacity, UITransform, v3, Vec3, Widget } from 'cc';
import { sSlotHorGameEntityAdapter } from '../lobby/hyPrefabs/gameRes/external/sSlotHorGameEntityAdapter';
import { sUtil } from '../lobby/game/core/sUtil';
import { sFreeWinBuyView } from '../lobby/game/core/sFreeWinBuyView';
import { UIFreeWinBuy } from '../lobby/hyPrefabs/gameRes/external/UIFreeWinBuy';
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = Starlight_sSlotHorGameEntityAdapter
 * DateTime = Thu Oct 12 2023 14:54:31 GMT+0800 (中国标准时间)
 * Author = dadadaxigua
 * FileBasename = Starlight_sSlotHorGameEntityAdapter.ts
 * FileBasenameNoExtension = Starlight_sSlotHorGameEntityAdapter
 * URL = db://assets/game/Starlight_sSlotHorGameEntityAdapter.ts
 * ManualUrl = https://docs.cocos.com/creator/3.4/manual/zh/
 *
 */
 
@ccclass('Starlight_sSlotHorGameEntityAdapter')
export class Starlight_sSlotHorGameEntityAdapter extends sSlotHorGameEntityAdapter {
    // [1]
    // dummy = '';
    @property(Label)
    winStringLabel : Label;
    @property(Node)
    winStringLabel1 : Node;
    @property(Prefab)
    freebuyNode: Prefab;
    private callBack = null;
    protected onLoad(): void {
        super.onLoad();
        //this.currentBetAmount=this.betAmount;
        // const freebuy = instantiate(this.node.parent.getChildByPath('UIFreeWinBuy1'));
        // freebuy.parent = this.node.parent;
        // freebuy.active = true;
        // this.selfFreeWinBuyView = freebuy.getComponent(sFreeWinBuyView);
        
        
       // this.winLabel = this.node.getChildByPath('betInfo/Node/winLabel').getComponent(Label);
        //this.ruleViewSize = 1200;
    }
    start () {
        super.start();
        // Promise.all([new Promise((resolve,reject)=>{
        //     globalThis.getSubGamePrefabByPath('prefabs/subGameUserCoin',(newObj : Node)=>{
        //         if(newObj){
        //             newObj.parent = this.node.parent;
        //             this.ownLabel = newObj.getChildByName('Label').getComponent(Label);
        //             this.shopNode = newObj.getChildByName('shop').getComponent(Button);
        //         }
        //         resolve(1);
        //     });
        // }),new Promise((resolve,reject)=>{
        //     globalThis.getSubGamePrefabByPath('prefabs/subGameSettingDetail',(newObj : Node)=>{
        //         if(newObj){
        //             newObj.parent = this.node.parent.parent;
        //             this.menuBtns = newObj.getComponent(UIOpacity);
        //             this.soundBtn = this.menuBtns.node.getChildByPath('main/items/sound').getComponent(Button);
        //             this.closeBtn = this.menuBtns.node.getChildByPath('close').getComponent(Button);
        //             this.quitBtn = this.menuBtns.node.getChildByPath('main/items/exit').getComponent(Button);
        //             newObj.active = false;
        //         }
        //         resolve(2);
        //     });
        // }),new Promise((resolve,reject)=>{
        //     const lua = globalThis.GetLanguageType();
        //     if(lua){
        //         sUtil.getLobbyAssetInSubGame('config/'+lua,JsonAsset,(err,asset)=>{
        //             if(asset){
        //                 btnInternationalManager.SetLanguegeData(asset.name, asset.json);
        //             }
        //             resolve(5);
        //         });
        //     }else{
        //         resolve(5);
        //     }
        // })]).then((res)=>{
        //     super.super.start();
        //     if(this.shopNode){
        //         this.shopNode.node.on('click',()=>{
        //             globalThis.openStore && globalThis.openStore();
        //         },this);
        //     }
        // });


        // [3]
        
        // director.off('rollStop',null,sGameEntity);
        // director.off('rollStopEndTrigger',null,sGameEntity);


        //director.targetOff()
        ///this.freeWinBuy=this.node.parent.getChildByPath('UIFreeWinBuy1');
        // director.off('betUserInfoUpdateWinAnima');
        // director.on('betUserInfoUpdateWinAnima', (rate) => {
        //     this.currentBetAmount=this.betAmount;
        //     this.betUserInfoUpdateAnima(null, null, rate * this.currentBetAmount, 0.2);
        // }, this);
       
        this.freeWinBuy= instantiate(this.freebuyNode);
        //this.ruleNode=this.node.parent.getChildByName('UIIntro');
        //this.loadFreeBuyView();
        const freebuy = instantiate(this.freebuyNode);
        freebuy.parent = this.node.parent;
        freebuy.active = true;
        
        this.selfFreeWinBuyView= freebuy.getComponent(UIFreeWinBuy);
        // director.off('subGameFreeWinBuyBtnClick');
        // director.on('subGameFreeWinBuyBtnClick', () => {
        //     if (this.betBtnState == 'normal' && this.betClickMode == 'normal') {
        //         if (this.clientConfig) {
        //             let bet_types = this.clientConfig.bet_types;
        //             if (bet_types && bet_types.length > 0) {
        //                 for (let i = 0; i < bet_types.length; i++) {
        //                     const element = bet_types[i];
        //                     if (element.game_id == this.gameid) {
        //                         if (element.bet_amount == this.betAmount && element.game_mode == 2) {
        //                             let mUserInfo = this.getUserInfo();
        //                             if (mUserInfo) {
        //                                 let tipStr = '';
        //                                 let canBuy = true;
        //                                 let maxBet = mUserInfo.max_bet;
        //                                 let userCoin = mUserInfo.coin;
        //                                 let sumAmount = element.bet_amount * element.bet_multiple;
        //                                 let sum = element.bet_amount;
        //                                 if (sumAmount > userCoin) {
        //                                     console.log('no userCoin');
        //                                     canBuy = false;
        //                                     tipStr = globalThis.GetDataByKey && globalThis.GetDataByKey("mulTiLN", "common", '5001');
        //                                 } else if (sum > maxBet) {
        //                                     console.log('no maxBet');
        //                                     canBuy = false;
        //                                     tipStr = btnInternationalManager.GetDataByKey('maxBetNotEnough');
        //                                 }
        //                                 this.freeBuyBetAmount1 = element.bet_amount;
        //                                 this.freeBuyLineMultiple1 = element.bet_multiple;
        //                                 this.currentBetAmount=element.bet_amount;
        //                                 console.log('subGameFreeWinBuyBtnClick');
        //                                 if(this.selfFreeWinBuyView){
        //                                     this.selfFreeWinBuyView.viewInit(element.bet_amount * element.bet_multiple, element.bet_type, canBuy, tipStr);
        //                                 }else if(this.freeWinBuy){
        //                                     this.freeWinBuy=this.node.parent.getChildByPath('UIFreeWinBuy');
        //                                     const freeWinBuy = this.freeWinBuy.getComponent(UIFreeWinBuy);
        //                                     if(freeWinBuy){
        //                                        // this.currentBetAmount=this.freeBuyBetAmount;
        //                                         freeWinBuy.viewInit(element.bet_amount * element.bet_multiple, element.bet_type, canBuy, tipStr);
        //                                     };
        //                                 };
        //                             };
        //                             break;
        //                         };
        //                     };
        //                 };
        //             };
        //         };
        //     };
        // }, this); 
    };

    // rulesBtnClick(){
    //     this.ruleNode.active = true;
    // };
    protected rulesBtnClick(){
        //this.ruleNode.scale=v3(1.28,1.28,0.78125);
        
        // let pNodeimg=this.ruleNode.getChildByPath('main/ScrollView/view/content/img')
        // let contNode=this.ruleNode.getChildByPath('main/ScrollView/view/content');
        // let pviewNod=this.ruleNode.getChildByPath('main/ScrollView/view');
        // let pcloseNode=this.ruleNode.getChildByPath('main/close');
        // let pbgNode=this.ruleNode.getChildByPath('main/bg');
        // let ScrollViewNode=this.ruleNode.getChildByPath('main/ScrollView');

        // pcloseNode.addComponent(Widget);
        // pcloseNode.getComponent(Widget).isAlignRight=true;
        // pcloseNode.getComponent(Widget).isAlignTop=true;
        // pcloseNode.getComponent(Widget)!.isAbsoluteRight= false;
        // pcloseNode.getComponent(Widget)!.isAbsoluteTop = false;
        // pcloseNode.getComponent(Widget)!.right=0.11;
        // pcloseNode.getComponent(Widget)!.top=0.1;

        // this.setNodeWidget(pbgNode,0.11,0.11,0.08,0.08);
        // //this.setNodeWidget(ScrollViewNode,0.11,0.11,0.2,0.1);
        // pviewNod.active=false;
        // pviewNod.active=true;
        // let pviewHeight=pviewNod.getComponent(UITransform).contentSize.x;
        // contNode.getComponent(Layout).affectedByScale=true;



        // console.log(pbgNode);

        // pNodeimg.scale=v3((pviewHeight/1280),(pviewHeight/1280),0.78125);
        this.ruleNode.active = true;
    };

    betUserInfoUpdate(own, bet, win) {
        if (this.ownLabel) {
            if (own || own == 0) {
                this.ownLabel.node['rollAnimaValue'] = own;
                this.ownLabel.string = this.AddCommas(Math.trunc(own));
            }
        }
        if (this.betLabel) {
            if (bet || bet == 0) {
                this.betLabel.node['rollAnimaValue'] = bet;
                this.betLabel.string = this.AddCommas(Math.trunc(bet));
            }
        };
        //if(own==null){
            if(win==null){
                this.winLabel.node['rollAnimaValue'] = 0;
                console.log('重置winLabel1');
                this.winLabel.node.active=false;
                this.winStringLabel.node.active=true
                this.winStringLabel1.active=false
                this.winLabel.string=0+'';
            }else{
                if (win && win > 0) {
                    //console.log('win1:',win);
                    this.winLabel.node['rollAnimaValue'] = win;
                    console.log('重置winLabel2');
                    this.winLabel.string = this.AddCommas(Math.trunc(win));
                    this.winStringLabel1.active=true
                }else{
                    this.winLabel.node['rollAnimaValue'] = 0;
                    console.log('重置winLabel-1');
                    //console.log('win2:',win);
                   // this.winLabel.string=0+'';
                    
                };
            };
        //};
        
    };

    betUserInfoUpdateAnima(own, bet, win, animaTime = 1, callBack = null) {
        if (own || own == 0) {
            this.CommasLabelAnima(this.ownLabel, Math.trunc(own), animaTime);
        };
        if (bet || bet == 0) {
            this.CommasLabelAnima(this.betLabel, Math.trunc(bet), animaTime);
        }; 
        if(own==null){
            if(win==null){
                this.winLabel.node['rollAnimaValue'] = 0;
                console.log('重置winLabel3');
                this.winLabel.string=0+'';
            }else{
                if (win &&win > 0) {
                    this.winLabel.node.active=true;
                    this.winStringLabel.node.active=false;
                    this.winStringLabel1.active=true
                    //console.log('win3:',win);
                    let time=0.6;
                    if(win>=this.currentBetAmount*200){
                        time=12;
                    };
                    if(!this.winLabel.node['rollAnimaValue']){
                        console.log('重置winLabel4');
                        this.winLabel.node['rollAnimaValue']=0;
                    };
                    //this.winLabel.string = this.AddCommas(Math.floor(win));
                    let num=parseInt(this.winLabel.string);
                    console.log('win:',win);
                    console.log('num:',num);
                    console.log('this.winLabel.string:',this.winLabel.string);
                    console.log('rollAnimaValue',this.winLabel.node['rollAnimaValue']);
                    //sUtil.TweenLabel(this.winLabel.node['rollAnimaValue'],Math.trunc(win+this.winLabel.node['rollAnimaValue']),this.winLabel,time);
    
                    this.updateWinLab(this.winLabel.node['rollAnimaValue'],this.winLabel, Math.trunc(win+this.winLabel.node['rollAnimaValue']),time,this.callBack);
                    
                    //this.winLabel.node['rollAnimaValue'] += win;
                    this.winLabel.node['rollAnimaValue']+=Math.trunc(win);
                    //this.CommasLabelAnima(this.winLabel, Math.trunc(bet), animaTime);
                    director.off('StopLabelanima');
                    sUtil.once('StopLabelanima',()=>{
                        //this.winLabel.node['rollAnimaValue'] += win;
                        this.updateLabTween.stop();
                        this.winLabel.string=this.AddCommas(Math.floor(this.winLabel.node['rollAnimaValue']));
                    });
                }else{
                    if(!this.winLabel.node['rollAnimaValue']){
                        console.log('重置winLabel5');
                        this.winLabel.node['rollAnimaValue']=0;
                        this.winLabel.string=0+'';
                    }else{
                        this.winLabel.node['rollAnimaValue'] = 0;
                        console.log('重置winLabel6');
                    };
                    //console.log('win4:',win);
                    // this.winLabel.node.active=false;
                    // this.winStringLabel.node.active=true;
                    // this.winStringLabel1.node.active=false
                };
            };
        };
        
        

        if (callBack != null) {
            this.scheduleOnce(callBack, animaTime);
        };
    };

    // updateLabTween: Tween<any>;
    // updateWinLab(lastCoin: number,winLabel: Label, coin: number, time: number,cb:Function) {
    //     let tweenTargetVec3 = v3(lastCoin, lastCoin, lastCoin);
    //     this.updateLabTween && this.updateLabTween.stop()
    //     this.updateLabTween = tween(tweenTargetVec3).to(time, v3(coin, coin, coin), {
    //         "onUpdate": (target: Vec3) => {
    //             if (winLabel) { 
    //                 //winLabel.string = sUtil.AddCommas(Math.floor(target.x));
    //                 winLabel.string = sUtil.AddCommas(Math.trunc(target.x));
    //             };
    //         }, easing: undefined
    //     }).call(() => {
    //         winLabel!.string = this.AddCommas(Math.trunc(coin));
    //         //this.winLabel.node['rollAnimaValue'] += coin;
    //         if(cb) cb();
    //     }).start();
    // };

    updateLabTween: Tween<any>;
    updateWinLab(lastCoin: number,winLabel: Label, coin: number, time: number,cb:Function) {
        let tweenTargetVec3 = v3(lastCoin, lastCoin, lastCoin);
        let demc = -1;
        if(globalThis.getCoinRate && globalThis.getCoinRate() > 1){
            demc = 2;
        }
        this.updateLabTween && this.updateLabTween.stop()
        this.updateLabTween = tween(tweenTargetVec3).to(time, v3(coin, coin, coin), {
            "onUpdate": (target: Vec3) => {
                if (winLabel) { 
                    //winLabel.string = sUtil.AddCommas(Math.floor(target.x));
                    winLabel.string = false ? Math.trunc(target.x).toString() : (0 > 0 ? sUtil.changeNumberToKW((target.x),0) : (sUtil.AddCommas(target.x,demc)).toString());
                    //winLabel.string = sUtil.AddCommas(Math.trunc(target.x));
                };
            }, easing: undefined
        }).call(() => {
            if (winLabel) {
                winLabel.string = false ? Math.trunc(coin).toString() : (0 > 0 ? sUtil.changeNumberToKW((coin),0) : (sUtil.AddCommas(coin,demc)).toString());
            };
            //this.winLabel.node['rollAnimaValue'] += coin;
            if(cb) cb();
        }).start();
    };
}
//     tween(tweenTargetVec3).to(_time, v3(value, value, value), {
//         "onUpdate": (target: Vec3) => {
//             if (targetLabel) {
//                 targetLabel.string = isInt ? Math.trunc(target.x).toString() : (needKWNum > 0 ? sUtil.changeNumberToKW((target.x),needKWNum) : (sUtil.AddCommas(target.x,demc)).toString());
//             }
//         }, easing: ease
//     }).call(() => {
//         if (targetLabel) {
//             targetLabel.string = isInt ? Math.trunc(value).toString() : (needKWNum > 0 ? sUtil.changeNumberToKW((value),needKWNum) : (sUtil.AddCommas(value,demc)).toString());
//         }
//         if(finishCall){
//             finishCall();
//         }
//     }).start();
// }

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.4/manual/zh/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.4/manual/zh/scripting/decorator.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.4/manual/zh/scripting/life-cycle-callbacks.html
 */

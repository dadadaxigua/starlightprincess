
import { _decorator, Component, director, Label, Node, sp, tween, UIOpacity, v3 } from 'cc';
import { sComponent } from '../lobby/game/core/sComponent';
import sAudioMgr from '../lobby/game/core/sAudioMgr';
import { UIBase } from '../lobby/game/core/UIBase';
import { sUtil } from '../lobby/game/core/sUtil';
const { ccclass, property } = _decorator;


@ccclass('StarlightPrincess_TotalWin')
export class StarlightPrincess_TotalWin extends UIBase {
    @property(Node)
    freeBtn : Node;
    @property(Label)
    numLabel : Label;
    @property(Label)
    freenum : Label;
    @property(Node)
    contentNode : Node;
    @property(sp.Skeleton)
    popSpine : sp.Skeleton;
    @property(sp.Skeleton)
    thunderSpine : sp.Skeleton;
    @property({type:[Node]})
    LabelNode = [];
    private touchEnable = true;

    protected start () {
        this.freeBtn.on('click',()=>{
            if(this.touchEnable){
                sAudioMgr.PlayShotAudio('thunder');
                this.cleanTweenList();

                this.touchEnable = false;


                sAudioMgr.PlayShotAudio('freeWinTransitBtn');
                this.popSpine.setAnimation(0,'stpr_banner_out',false);
                this.scheduleOnce(()=>{
                    this.playThunderAnima();
                },0.4)
                
                
                for(let jindex=0;jindex<this.LabelNode.length;jindex++){
                    this.LabelNode[jindex].active=true;
                    this.pushIDTween(tween(this.LabelNode[jindex].getComponent(UIOpacity)).to(0.4,{opacity:0}).start(),'LabelNode2');
                };

                this.popSpine.setCompleteListener(()=>{
                    
                    this.popSpine.node.active=false;
                    this.contentNode.active = false;
                    this.scheduleOnce(()=>{
                        director.emit('freeWinOver');
                        director.emit('rollStop');
                        sAudioMgr.StopBGAudio();
                        sAudioMgr.PlayBG('bgm_mg');
                        // this.scheduleOnce(()=>{
                        //     this.touchEnable=true;
                        //     director.emit('StarlightTotleViewOpen',800000,13);
                        // },2)
                    },1);
                });
            }
        },this);

        director.on('StarlightTotleViewOpen',(coin,num)=>{
            let demc = -1;
            if(globalThis.getCoinRate && globalThis.getCoinRate() > 1){
                demc = 2;
            };

            sAudioMgr.bgMusicOut(0.4,0);
            this.contentNode.active = true;
            this.freenum.string=num;
            //this.popSpine.timeScale = 2.5;
            this.popSpine.node.active=true;
            this.popSpine.setAnimation(0,'stpr_banner_in',false);

            sAudioMgr.PlayShotAudio('freegame_end');
            sAudioMgr.PlayShotAudio('freegame_voice_congratulations');
            //this.scheduleOnce(()=>{
            let fs_Node= this.LabelNode[3];
            let fslabel= this.LabelNode[4];

            if(globalThis.GetLanguageType()=='EN'){
                fslabel.position=v3(-103,-151,0);
            }else if(globalThis.GetLanguageType()=='VN'){
                fslabel.position=v3(-164,-154,0);
            }else if(globalThis.GetLanguageType()=='PT'){
                fslabel.position=v3(-215,-152,0);
            }else if(globalThis.GetLanguageType()=='TH'){
                fslabel.position=v3(-55,-152,0);
            };
            fslabel.active=true;
            fslabel.getComponent(UIOpacity).opacity=0;
            // this.scheduleOnce(()=>{
                
            // },0.3);
            
            for(let jindexx=0;jindexx<this.LabelNode.length;jindexx++){
                //this.scheduleOnce(()=>{
                    //this.pushIDSchedule(()=>{
                    this.LabelNode[jindexx].active=true;
                    this.LabelNode[jindexx].getComponent(UIOpacity).opacity=0;
                    if(jindexx==3){
                        this.pushIDTween(tween(this.LabelNode[jindexx])
                        .delay(0.4).call(()=>{
                            sAudioMgr.PlayBG('bgm_interlude');
                            sAudioMgr.PlayShotAudio('freegame_frame_open');
                        })
                        .to(0.2,{scale:v3(1.2,1.2,1.2)})
                        .to(0.1,{scale:v3(0.8,0.8,0.8)}).start(),'LabelNode3');
                        
                        //console.log('this.LabelNode',this.LabelNode);
                        this.pushIDTween(tween(this.LabelNode[jindexx].getComponent(UIOpacity)).delay(0.6).to(0.2*jindexx+0.1,{opacity:255}).start(),'LabelNode1');
                    }else{
                        this.pushIDTween(tween(this.LabelNode[jindexx])
                        .delay(0.4)
                        .to(0.2,{scale:v3(1.4,1.4,1.4)})
                        .to(0.1,{scale:v3(1,1,1)}).start(),'LabelNode3');
                        
                        //console.log('this.LabelNode',this.LabelNode);
                        this.pushIDTween(tween(this.LabelNode[jindexx].getComponent(UIOpacity)).delay(0.6).to(0.2*jindexx+0.1,{opacity:255}).start(),'LabelNode1');
                    }
                   
                    //},0.03*jindexx+0.3,'xx');
                    
                //},0.03*jindexx+0.3);
                
            };


            
             

            this.popSpine.setCompleteListener(()=>{
                this.popSpine.timeScale = 1;
                this.popSpine.setAnimation(0,'stpr_banner_loop',true);
            });
          
            this.numLabel.string = sUtil.AddCommas(coin,demc);
            
            this.node.setSiblingIndex(this.node.parent.children.length - 1);
            this.unscheduleAllCallbacks();
            this.cleanTweenList();
            this.scheduleOnce(()=>{
                this.freeBtn.emit('click');
            },5);
        },this);

        director.on('freeWinBegain',()=>{
            this.touchEnable = true;
        },this);

        // this.scheduleOnce(()=>{
        //     director.emit('StarlightTotleViewOpen',800000,13);
        // },2)
        
    };

    private playThunderAnima(){
        if(this.thunderSpine){
            this.thunderSpine.node.active = true;
            this.thunderSpine.setAnimation(0,'stpr_transition',false);
            sAudioMgr.PlayShotAudio('freegame_transitions',1);
            this.thunderSpine.setCompleteListener(()=>{
                this.thunderSpine.node.active = false;
            });
        }
    };

}

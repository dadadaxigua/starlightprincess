
import { _decorator, Component, director, Label, Node, sp, tween, UIOpacity, v3 } from 'cc';
import { sComponent } from '../lobby/game/core/sComponent';
import sAudioMgr from '../lobby/game/core/sAudioMgr';
const { ccclass, property } = _decorator;

 
@ccclass('StarlightPrincess_UIFreeSpinEntry')
export class StarlightPrincess_UIFreeSpinEntry extends sComponent {
    @property(Node)
    freeBtn : Node;
    @property(Label)
    numLabel : Label;
    @property(Node)
    contentNode : Node;
    @property(Node)
    LabelNode : Node;
    @property(sp.Skeleton)
    popSpine : sp.Skeleton;
    @property(sp.Skeleton)
    thunderSpine : sp.Skeleton;

    private touchEnable = true;

    protected start () {
        this.freeBtn.on('click',()=>{
            if(this.touchEnable){
                this.unscheduleAllCallbacks();
                this.cleanTweenList();
                this.touchEnable = false;
                sAudioMgr.PlayShotAudio('thunder');
                sAudioMgr.StopAudio(1);
                this.popSpine.setAnimation(0,'stpr_banner_out',false);

                this.LabelNode.active=true;
            for(let nNode of this.LabelNode.children){
                this.pushIDTween(tween(nNode.getComponent(UIOpacity)).delay(0.3).to(0.5,{opacity:0}).start(),'LabelNode22');
            };

                this.scheduleOnce(()=>{
                    //this.contentNode.active = false;
                    sAudioMgr.StopBGAudio();
                    this.playThunderAnima();
                },0.2);
                this.popSpine.setCompleteListener(()=>{
                    this.popSpine.node.active=false;

                });
            }
        },this);

        director.on('freeWinOver',()=>{
            this.touchEnable = true;
        },this);

        director.on('freeWinBingo',(count,rate)=>{
            this.contentNode.active = true;
           // sAudioMgr.StopBGAudio();
        //    this.scheduleOnce(()=>{
        //     sAudioMgr.PlayShotAudio('freegame_frame_open');
        //     },0.3);
            

            sAudioMgr.PlayShotAudio('freegame_voice_freespins');

            this.popSpine.node.active=true;
            this.popSpine.setAnimation(0,'stpr_banner_in',false);
            this.popSpine.setCompleteListener(()=>{
                this.popSpine.timeScale = 1;
                this.popSpine.setAnimation(0,'stpr_banner_loop',true);
                
                sAudioMgr.PlayAudio('fsinout_bgm');
            });

            this.LabelNode.active=true;
            let x=0;
            for(let nNode of this.LabelNode.children){
                
                nNode.getComponent(UIOpacity).opacity=0;
                this.pushIDTween(tween(nNode.getComponent(UIOpacity)).delay(0.3).call(()=>{
                    x++;
                    if(x==1){
                        sAudioMgr.PlayShotAudio('freegame_frame_open',0.8);
                    };
                }).delay(0.5).to(0.2,{opacity:255}).start(),'LabelNode1');
                this.pushIDTween(tween(nNode).delay(0.8).to(0.2,{scale:v3(1.5,1.5,1.5)}).to(0.2,{scale:v3(1,1,1)}).start(),'LabelNode2');
            };

            director.emit('freeWinBtnInfo',rate);
            this.setLabelNum(count);
            this.node.setSiblingIndex(this.node.parent.children.length - 1);
            this.unscheduleAllCallbacks();
            this.cleanTweenList();
            this.scheduleOnce(()=>{
                this.freeBtn.emit('click');
            },3);
        },this);


        
        // this.scheduleOnce(()=>{
        //     director.emit('freeWinBingo',15,50000);
        // },2);
        director.on('bingoFreewinInFree',()=>{

        },this);
    }

    onEnable(){
        
    }


    setLabelNum(num){
        this.numLabel.string = num.toString();
    }

    private playThunderAnima(){
        if(this.thunderSpine){
            this.thunderSpine.node.active = true;
            this.thunderSpine.setAnimation(0,'stpr_transition',false);
            sAudioMgr.PlayShotAudio('freegame_transitions',1);
            



            this.thunderSpine.setCompleteListener(()=>{
                this.thunderSpine.node.active = false;
                this.contentNode.active = false;
                
                sAudioMgr.PlayShotAudio('freegame_voice_Letgo');
                sAudioMgr.PlayBG('bgm_fs');
                director.emit('freeWinBegain');
                // this.scheduleOnce(()=>{
                //     director.emit('freeWinBingo',15,50000);
                // },2);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              1   
            });
        }
    }

}


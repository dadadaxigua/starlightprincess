
import { _decorator, Component, director, Node, v3, Vec3 } from 'cc';
import { sNormalFlow } from '../lobby/game/core/sNormalFlow';
import { sBoxMgr } from '../lobby/game/core/sBoxMgr';
import sAudioMgr from '../lobby/game/core/sAudioMgr';
import { sUtil } from '../lobby/game/core/sUtil';
import { sConfigMgr } from '../lobby/game/core/sConfigMgr';
import { sObjPool } from '../lobby/game/core/sObjPool';
import { sBoxEntity } from '../lobby/game/core/sBoxEntity';
const { ccclass, property } = _decorator;

@ccclass('StarlightPrincess_NormalFlow')
export class StarlightPrincess_NormalFlow extends sNormalFlow {
    @property(Node)
    effectLayer : Node;

    start () {
        super.start();
        // this.scheduleOnce(()=>{
        //     sBoxMgr.instance.updateBoxDataByXY(1, 0, 'win', 'settlement');
        //     sBoxMgr.instance.updateBoxDataByXY(1, 1, 'win', 'settlement');
        //     sBoxMgr.instance.updateBoxDataByXY(1, 2, 'win', 'settlement');
        //     sBoxMgr.instance.updateBoxDataByXY(1, 3, 'win', 'settlement');
        //     sBoxMgr.instance.updateBoxDataByXY(1, 4, 'win', 'settlement');
        // },2);
        director.on('betBtnClick',(betSpeedMode)=>{
            if(betSpeedMode!='turbo'){
                sAudioMgr.PlayAudio('normal_spinning',true);
            };
        },this);
    }

    clearUI() {
        super.clearUI();
        this.removeScheduleDic();
    }
    rollStopTurboAction(round,rollSpeedMode,clickMode){
        director.off('rollOneColumnBump');
        director.off('byWayWinLightEnd');
        director.off('titleWinSettleEnd');
        director.off('rollStopOneRoundCall');
        director.off('StarlightPrincessBigWinEnd');
        // const isJackpot = this.jackpotAmount > 0;
        // const isJackpot = true;
        this.tatalWin=0;
        const _boxViewSize = sBoxMgr.instance.getBoxViewSize();
        const _boxSize = sBoxMgr.instance.getBoxSize();
        const round_symbol_map = round['round_symbol_map'];
        director.on('rollOneColumnStop',col=>{
            if(col == 5){
                sAudioMgr.PlayShotAudio('normal_spin_end',1);
                
                sAudioMgr.StopAudio();
            };
        },this);

        if (round) {
            let timestop=0;
            const round_symbol_map = round['round_symbol_map'];
            for(let xx of round_symbol_map[0]){
                for(let yy=0;yy<xx.length;yy++){
                    //console.log('symbel:',xx[yy]);
                    if(yy>=1&&yy<=5&&xx[yy]>2000){
                        timestop=0;
                        director.emit('controlHuaXianZi','NormalIdle');
                        break;
                    };
                };
                if(timestop>0)break;
            };
            
            this.normalFlowDelayCall(() => {
                this.scheduleOnce(()=>{
                    let needWinLight = round.rate > 0 ? true : false;
                    const round_symbol_map = round['round_symbol_map'];
                    sConfigMgr.instance.SetResData(round_symbol_map);
                    const hits = round.hit_events_list;
                    const config = sConfigMgr.instance.getConfig();
                    const events = config['event_config'];
                    for (let i = 0; i < hits.length; i++) {
                        const _event = hits[i];
                        if(_event && Array.isArray(_event)){
                            for (let j = 0; j < _event.length; j++) {
                                const eve = _event[j];
                                const hitEvent = events[eve];
                                if(hitEvent && hitEvent.event_type == 'bonusGame'){
                                    // isJackpot = true;
                                };
                            };
                        };
                    };
    
                  
    
                    director.on('rollOneColumnBump',(col)=>{
                        sAudioMgr.PlayShotAudio('luodi',0.4);
                        const colBoxs = sBoxMgr.instance.GetColAllBox(col);
                        let shunxu=0;
                        let shunxu1=0;
                        if(colBoxs){
                            for (let rY = 0; rY < colBoxs.length; rY++) {
                                const rSymbol = colBoxs[rY];
                                if(rSymbol.SymbolValue > 2000){
                                    if(rSymbol.ViewItemIndex.y >= 0 && rSymbol.ViewItemIndex.y <= 4){
                                        rSymbol.DoEnitityAction('thunder');
                                        console.log('召唤一道落雷4')
                                        if(rSymbol.SymbolValue == 1){
                                            shunxu1++;
                                            if(shunxu1==1){
                                                this.playmultMp();
                                            };
                                        };
                                    };
                                }else if(rSymbol.SymbolValue == 1){
                                    shunxu++;
                                    sAudioMgr.PlayShotAudio('scatter_appear'+shunxu);
                                    if(shunxu==1){
                                        this.playscatterMp();
                                    };
                                };;
                            };
                        };
                    },this);
    
                    sUtil.once('byWayWinLightEnd',()=>{
                        sUtil.once('titleWinSettleEnd',()=>{
                                let sBoxMax=false;
                                // sBoxMgr.instance.setAllViewTargetBoxActionArrayState((sbox : sBoxEntity)=>{
                                //     if(sbox.SymbolValue>2000){
                                //         sBoxMax=true;
                                //     };
                                //     return (sbox.SymbolValue>2000);
                                // },'win','settlement');
    
                                let times=(sBoxMax)?2.5:0.5;
                                this.scheduleOnce(()=>{
                                    const rate = round.rate;
                                    if(rate >= 200){
                                        director.emit('betUserInfoUpdateWinAnima',rate-this.tatalWin);
                                        director.emit('StarlightPrincessBigWin',rate);
                                        sUtil.once('StarlightPrincessBigWinEnd',()=>{
                                             this.scheduleOnce(() => {
                                                console.log('rollstop1');
                                                director.emit('rollStop');
                                            }, 0.5);
                                        });
                                    }else{
                                        this.scheduleOnce(() => {
                                            console.log('rollstop2');
                                            director.emit('rollStop');
                                        }, 0.5);
                                    };
                                },times);
                            
                        });
                        director.emit('winBetRes',-1);
                    });
    
                    sUtil.once('rollStopOneRoundCall', () => {
                        director.off('rollOneColumnStop');
                        director.off('rollOneColumnBump');
                        if(needWinLight){
                            // this.scheduleOnce(()=>{
                            //     this.byWayBoxLightCtr(round,'normal');
                            // },1);
                            this.byWayBoxLightCtr(round,'normal');
                        }else{
                            console.log('rollstop3');
                            director.emit('rollStop');
                        }
                    });
                    sBoxMgr.instance.rollStopAllColumn();
                },timestop);
                
            });
        };
    };
    rollStopAction(round,rollSpeedMode,clickMode){
        director.off('rollOneColumnBump');
        director.off('byWayWinLightEnd');
        director.off('titleWinSettleEnd');
        director.off('rollStopOneRoundCall');
        director.off('StarlightPrincessBigWinEnd');
        // const isJackpot = this.jackpotAmount > 0;
        // const isJackpot = true;
        this.tatalWin=0;
        const _boxViewSize = sBoxMgr.instance.getBoxViewSize();
        const _boxSize = sBoxMgr.instance.getBoxSize();
        const round_symbol_map = round['round_symbol_map'];
        director.on('rollOneColumnStop',col=>{
            if(col == 0){
                sAudioMgr.StopAudio();
            };
        },this);


        if (round) {
           
            this.normalFlowDelayCall(() => {
                let timestop=0;
                const round_symbol_map = round['round_symbol_map'];
                for(let xx of round_symbol_map[0]){
                    for(let yy=0;yy<xx.length;yy++){
                        //console.log('symbel:',xx[yy]);
                        if(yy>=1&&yy<=5&&xx[yy]>2000){
                            timestop=0;
                            director.emit('controlHuaXianZi','NormalIdle');
                            break;
                        };
                    };
                    if(timestop>0)break;
                };
    



                this.scheduleOnce(()=>{
                    let needWinLight = round.rate > 0 ? true : false;
                    sConfigMgr.instance.SetResData(round_symbol_map);
                    const hits = round.hit_events_list;
                    const config = sConfigMgr.instance.getConfig();
                    const events = config['event_config'];
                    for (let i = 0; i < hits.length; i++) {
                        const _event = hits[i];
                        if(_event && Array.isArray(_event)){
                            for (let j = 0; j < _event.length; j++) {
                                const eve = _event[j];
                                const hitEvent = events[eve];
                                if(hitEvent && hitEvent.event_type == 'bonusGame'){
                                    // isJackpot = true;
                                };
                            };
                        };
                    };
                    
                    director.on('rollOneColumnBump',(col)=>{
                        sAudioMgr.PlayShotAudio('normal_spin_col_end',1);
                        const colBoxs = sBoxMgr.instance.GetColAllBox(col);
                        let shunxu=0;
                        let shunxu1=0;
                        if(colBoxs){
                            for (let rY = 0; rY < colBoxs.length; rY++) {
                                const rSymbol = colBoxs[rY];
                                if(rSymbol.SymbolValue > 2000){
                                    if(rSymbol.ViewItemIndex.y >= 0 && rSymbol.ViewItemIndex.y <= 4){
                                        rSymbol.DoEnitityAction('thunder');
                                        console.log('召唤一道落雷5')
                                        if(rSymbol.SymbolValue == 1){
                                            shunxu1++;
                                            if(shunxu1==1){
                                                this.playmultMp();
                                            };
                                        };
                                    };
                                }else if(rSymbol.SymbolValue == 1){
                                    shunxu++;
                                    sAudioMgr.PlayShotAudio('scatter_appear'+shunxu);
                                    if(shunxu==1){
                                        this.playscatterMp();
                                    };
                                };
                            };
                        };
                    },this);
    
                    sUtil.once('byWayWinLightEnd',()=>{
                        sUtil.once('titleWinSettleEnd',()=>{
                                let sBoxMax=false;
                                // sBoxMgr.instance.setAllViewTargetBoxActionArrayState((sbox : sBoxEntity)=>{
                                //     if(sbox.SymbolValue>2000){
                                //         sBoxMax=true;
                                //     };
                                //     return (sbox.SymbolValue>2000);
                                // },'win','settlement');
    
                                let times=(sBoxMax)?3:0.5;
                                this.scheduleOnce(()=>{
                                    const rate = round.rate;
                                    if(rate >= 200){
                                        director.emit('betUserInfoUpdateWinAnima',rate-this.tatalWin);
                                        director.emit('StarlightPrincessBigWin',rate);
                                        sUtil.once('StarlightPrincessBigWinEnd',()=>{
                                            director.emit('rollStop');
                                        });
                                    }else{
                                        this.scheduleOnce(() => {
                                            console.log('rollstop2');
                                            director.emit('rollStop');
                                        }, 0.5);
                                    };
                                },times);
                            
                        });
                        director.emit('winBetRes',-1);
                    });
    
                    sUtil.once('rollStopOneRoundCall', () => {
                        director.off('rollOneColumnStop');
                        director.off('rollOneColumnBump');
                        if(needWinLight){
                            // this.scheduleOnce(()=>{
                            //     this.byWayBoxLightCtr(round,'normal');
                            // },1);
                            this.byWayBoxLightCtr(round,'normal');
                        }else{
                            //director.emit('betUserInfoUpdateWinAnima',round.rate);
                            director.emit('rollStop');
                        }
                    });
                    sBoxMgr.instance.rollStopAllColumn();
                },timestop);
               
            });
        };
    };

    
    tatalWin=0;
    byWayBoxLightCtr(round,rollSpeedMode) {
        director.off('dropAllBoxFinish');
        const eventArr = sConfigMgr.instance.GetAllEventOptConfigByHitListOnCheck(round);
        // console.log('eventArr:'+JSON.stringify(eventArr));
        const boxSum = sBoxMgr.instance.boxSum;
        let isEnd = false;
        let isTotalWin = false;
        let IsMulit=false;
        let winRate = 0;
        if (eventArr && Array.isArray(eventArr)) {
            let eventIndex = 0;
            const boxAnimaCall = () => {
                let isWin = false;
                if (eventIndex < eventArr.length) {
                    isEnd = eventIndex == (eventArr.length - 1);
                    const nowData = eventArr[eventIndex++];
                    if (nowData && nowData.eventData && nowData.eventData.length > 0) {
                        const events = nowData.eventData;
                        let resAct = {};
                        let winSymbol = {};
                        winRate = 0;
                        for (let i = 0; i < events.length; i++) {
                            const _event = events[i];
                            if (_event.event_type == 'boxAnima') {
                                if(_event.rate_change && _event.rate_change.num){
                                    winRate += _event.rate_change.num;
                                }
                                if(_event.bonus_symbol){
                                    winSymbol[_event.bonus_symbol] = true;
                                }
                                isWin = true;
                                isTotalWin = true;
                                let act = resAct[_event.bonus_symbol];
                                if (!act) {
                                    act = {};
                                    act.bonus_symbol = _event.bonus_symbol;
                                    act.act_pos = {};
                                    act.rate = 0;
                                }
                                if (_event.rate_change) {
                                    if (act.rate == 0) {
                                        act.rate = _event.rate_change.num;
                                    } else {
                                        if (_event.rate_change.type == 0) {
                                            act.rate += _event.rate_change.num;
                                        } else if (_event.rate_change.type == 1) {
                                            act.rate *= _event.rate_change.num;
                                        }
                                    }
                                }
                                if (_event.act_pos) {
                                    const poss = _event['act_pos'].split(',');
                                    if (poss) {
                                        for (let a = 0; a < poss.length; a++) {
                                            const value = poss[a];
                                            act.act_pos[value] = true;
                                        };
                                    };
                                };
                                resAct[_event.bonus_symbol] = act;
                            };
                        };
                        const resActKeys = Object.keys(resAct);
                        let allActPos = {};
                        if (resActKeys) {
                            for (let i = 0; i < resActKeys.length; i++) {
                                const element = resAct[resActKeys[i]];
                                const itemKeys = Object.keys(element.act_pos);
                                for (let j = 0; j < itemKeys.length; j++) {
                                    const key = itemKeys[j];
                                    allActPos[key] = true;
                                };
                                //console.log('element',element);
                                director.emit('slotWinBoxItemInfo',{symbol : element.bonus_symbol,rate : element.rate, num : itemKeys.length,boxPos : itemKeys[Math.trunc(itemKeys.length / 2)]});
                                // this.byWayboxResWinAction(element);
                            }
                            // let boxActArr: any[][] = [];
                            const allKeys = Object.keys(allActPos);
                            let xx=0;
                            for (let i = 0; i < allKeys.length; i++) {
                                const element = allKeys[i];
                                const act = element.split('_');
                                if (act && act.length == 2) {
                                    const act1 = parseInt(act[0]);
                                    const act2 = parseInt(act[1]);
                                    //console.log('消除的格子',act1,act2);
                                    if (act1 && act2 || act1 == 0 || act2 == 0) {
                                        if(sBoxMgr.instance.getBoxEntityByXY(act1,act2).SymbolValue>2000){
                                            xx++;

                                            this.scheduleOnce(()=>{
                                                sBoxMgr.instance.updateBoxDataByXY(act1, act2, 'win', 'settlement');
                                            },0.1*xx);
                                        }else{
                                            sBoxMgr.instance.updateBoxDataByXY(act1, act2, 'win', 'settlement');
                                        };
                                    };
                                };
                            };
                            for (let i = 0; i < sBoxMgr.instance.boxArray.length; i++) {
                                const arr = sBoxMgr.instance.boxArray[i];
                                for (let m = 0; m < arr.length; m++) {
                                    if(arr[m].ViewItemIndex.y >= 0 && arr[m].ViewItemIndex.y <= 4){
                                        //console.log('symbolvalue:',sBoxMgr.instance.boxArray,i,m,sBoxMgr.instance.getBoxEntityByXY(i,m),sBoxMgr.instance.getBoxEntityByXY(i,m).SymbolValue);
                                        if(arr[m].SymbolValue>2000){
                                            IsMulit=true;
                                            console.log('加倍存在：',IsMulit);
                                            break;
                                        };
                                    };
                                };
                                if(IsMulit){
                                    break;
                                };
                            };

                            director.emit('winBetRes',winRate);
                            //director.emit('betUserInfoUpdateWinAnima',winRate);
                            //this.tatalWin+=winRate;

                            if(round.rate<200&&!IsMulit){
                                console.log('小于200：无加倍倍数显示3',winRate)
                                director.emit('betUserInfoUpdateWinAnima',winRate);
                            }else if(!IsMulit){
                                if(eventIndex != (eventArr.length - 1)){
                                    this.tatalWin+=winRate;
                                    director.emit('betUserInfoUpdateWinAnima',winRate);
                                };
                            }else if(IsMulit){
                                this.tatalWin+=winRate;
                                director.emit('betUserInfoUpdateWinAnima',winRate);
                            };

                            if (isWin) {
                                const winSymbolKeys = Object.keys(winSymbol);
                                if(winSymbolKeys && winSymbolKeys.length > 0){
                                    for (let i = 0; i < winSymbolKeys.length; i++) {
                                        const winSymbolKey = winSymbolKeys[i];
                                    };
                                };
                                sAudioMgr.PlayAudio('small_win');
                                this.scheduleOnce(()=>{
                                    sAudioMgr.StopAudio();
                                },0.6);
                                sAudioMgr.PlayShotAudio('small_win_effect');
                                if(rollSpeedMode=='trubo'){
                                    sAudioMgr.StopAudio();
                                    sAudioMgr.PlayShotAudio('small_win_end');
                                    this.scheduleOnce(()=>{
                                        sAudioMgr.PlayShotAudio('symbol_clear'+eventIndex);
                                    },1);
                                }else{
                                    this.scheduleOnce(()=>{
                                        sAudioMgr.PlayShotAudio('small_win_end');
                                        this.scheduleOnce(()=>{
                                            sAudioMgr.PlayShotAudio('symbol_clear'+eventIndex);
                                        },1);
                                    },0.6);
                                };
                                // director.emit('winBetRateRes',winRate * this.getMultiple(eventIndex - 1));
                                sBoxMgr.instance.blackCurtainAnima();

                                sBoxMgr.instance.blackCurtainAnima(-1, false);
                                this.scheduleOnce(() => {
                                    // director.emit('titleSumLightMsg','normal',eventIndex);
                                    sConfigMgr.instance.TurnNextResRoundData();
                                    sBoxMgr.instance.ReplenishBoxEntity();
                                    let conturhua=0;
                                    sBoxMgr.instance.DropAllEntityBoxAfterRemove((strikeBox : sBoxEntity)=>{
                                        sAudioMgr.PlayShotAudio('normal_dropgrid');
                                        if(strikeBox){
                                            if(strikeBox.SymbolValue > 2000){
                                                if(strikeBox.ViewItemIndex.y > 4 ){
                                                    conturhua++;
                                                    console.log('召唤一道落雷6')
                                                    strikeBox.DoEnitityAction('thunder');
                                                    if(conturhua==1){
                                                        director.emit('controlHuaXianZi','NormalIdle');
                                                    };
                                                };
                                            };
                                        };
                                    },0.02);
                                },2.5);
                            };
                        };
                    } else {
                        if(isEnd){
                            let multiTotal = 0;
                            if(isTotalWin && nowData.opt && nowData.opt.multiData && nowData.opt.multiData.length > 0){
                                const multiboxes = [];
                                for (let o = 0; o < nowData.opt.multiData.length; o++) {
                                    const multiData = nowData.opt.multiData[o]; 
                                    if(multiData && multiData.pos && multiData.pos.length > 0){
                                        multiTotal += multiData.multi;
                                        for (let l = 0; l < multiData.pos.length; l++) {
                                            const mPos = multiData.pos[l];
                                            if(mPos){
                                                const mPosArr = mPos.split('_');
                                                if(mPosArr && mPosArr.length == 2){
                                                    multiboxes.push(sBoxMgr.instance.getBoxEntityByXY(parseInt(mPosArr[0]),parseInt(mPosArr[1])));
                                                }
                                            }
                                        }
                                    }
                                }
                                if(multiboxes.length > 0){
                                    let mulIndex = 0;
                                    this.pushOneSchedule(()=>{
                                        const mBox : sBoxEntity = multiboxes[mulIndex++];
                                        if(mBox){
                                            mBox.UpdateData('win',mulIndex == multiboxes.length ? 'close' : 'open');
                                        }
                                    },0,multiboxes.length,1.5,0,true);
                                    this.scheduleOnce(()=>{
                                        if(round.rate>200){

                                        }else{
                                            console.log('小于200：有加倍倍数显示4',round.rate)
                                            director.emit('betUserInfoUpdateWinAnima',round.rate-this.tatalWin);
                                        };
                                        director.emit('byWayWinLightEnd');
                                    },multiboxes.length * 1.5);
                                }else{
                                    director.emit('byWayWinLightEnd');
                                };
                            }else{
                                if (eventArr.length > 1) {
                                    director.emit('byWayWinLightEnd');
                                };
                            };
                        };
                    };
                };
            };
            boxAnimaCall();
            director.on('dropAllBoxFinish', ()=>{
                // console.log('dropAllBoxFinish');
                boxAnimaCall();
                // console.log('isEnd:',isEnd);
            }, this);
        };
    };
    playscatterMp(){
        let num=sUtil.RandomInt(1,11);
        if(num>=1&&num<9){
            sAudioMgr.PlayShotAudio('scatter_appear_voice'+num);
        }
    };

    playmultMp(){
        let num=sUtil.RandomInt(1,11);
        if(num>=0&&num<10){
            sAudioMgr.PlayShotAudio('mult_appear_voice'+num);
        }
    }

}

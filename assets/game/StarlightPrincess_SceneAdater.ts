
import { _decorator, assetManager, Camera, Color, color, Component, director, gfx, instantiate, Label, Node, Prefab, ResolutionPolicy, sp, tween, v3, view } from 'cc';
import { sComponent } from '../lobby/game/core/sComponent';
import sAudioMgr from '../lobby/game/core/sAudioMgr';
import { sUtil } from '../lobby/game/core/sUtil';
import { sBoxMgr } from '../lobby/game/core/sBoxMgr';
import { sObjPool } from '../lobby/game/core/sObjPool';
import { sBoxAssetInit } from '../lobby/game/core/sBoxAssetInit';
import { soltGameCheckUtil_149 } from './soltGameCheckLine_149';
import { StarlightPrincess_WinTipLabel } from './StarlightPrincess_WinTipLabel';
import { StarlightPrincess_Entity } from './StarlightPrincess_Entity';
const { ccclass, property } = _decorator;
 
@ccclass('StarlightPrincess_SceneAdater')
export class StarlightPrincess_SceneAdater extends sComponent {
    @property(Prefab)
    freebuyNode: Prefab;
    @property([Node])
    normalNodes = [];
    @property([Node])
    freeWinNodes = [];
    @property(Node)
    freebuybutton:Node;
    // @property(Label)
    // jackpotLabel : Label;

    protected onLoad(){
        globalThis.currentPlayingGameID = 149;
        if(!globalThis.boxEntityCtr){
            globalThis.boxEntityCtr = {};
        }
        globalThis.boxEntityCtr[globalThis.currentPlayingGameID] = StarlightPrincess_Entity;
    }

    protected start () {
        globalThis.soltGameCheckWon = soltGameCheckUtil_149;
        sAudioMgr.AudioInit();
        sAudioMgr.SetBGVolume(0.5);
        sAudioMgr.PlayBG('bgm_mg',true);

        view.setDesignResolutionSize(1280, 720, ResolutionPolicy.FIXED_HEIGHT);
        director.on('CorrectResolution', () => {
            view.setDesignResolutionSize(1280, 720, ResolutionPolicy.FIXED_HEIGHT);
        }, this);

        // director.emit('bonusLabelInit',globalThis.currentPlayingGameID,[this.jackpotLabel]);
        // console.log(this.node.getChildByName('Camera').getComponent(Camera).orthoHeight);

        // sUtil.getSubGameBtnByName(this.bottomPrefab,globalThis.currentPlayingGameID,'subGameBtn_Rist','none',0,'20231011_7',color(251,224,76,255),(subGameBtn : Node)=>{
        //     if(subGameBtn){
        //         subGameBtn.setParent(this.node);
        //         director.emit('getSubGameBtnByPathEnd');
        //     };
        // });

        // sUtil.getSubGameBtnByPath('subGameBtn_StarlightPrincess',globalThis.currentPlayingGameID,'none',0,'20230726_4',Color.WHITE,subGameBtn=>{
        //     if(subGameBtn){
        //         subGameBtn.setParent(this.node);
        //         this.SetNodeFront(subGameBtn);
        //         director.emit('getSubGameBtnByPathEnd');
        //     };
        // });
            
        const bundle = assetManager.getBundle('149');
        if(bundle){
            console.log('bundle');
            bundle.load('subGameBtn_Starlight',Prefab,(err,_prefab)=>{
                console.log('err:',JSON.stringify(err));
                console.log('_prefab',_prefab);
                if(_prefab){
                    console.log('_prefab');
                    let obj = instantiate(_prefab);
                    if(obj){
                        obj['gameid'] = 149;
                        obj['aligmentType'] = 'none';
                        obj['aligmentPixel'] = 0;
                        obj['gameVersion'] = '20231019_5';
                        obj['customColor'] = Color.WHITE;
                        if (_prefab) {
                            console.log('加载btn成功');
                            obj.setParent(this.node);
                            this.SetNodeFront(obj);
                            director.emit('getSubGameBtnByPathEnd');
                        } else {
                            console.log('加载btn失败');
                        };
                    }
                }
            });
        };
    //    console.log('StarlightPrincess_SceneAdater');
    //     try { 
    //         sUtil.getSubGameBtnByPath('subGameBtn_toa', globalThis.currentPlayingGameID, 'none', 0, '20231018_7', Color.WHITE, subGameBtn => {
    //                     if (subGameBtn) {
    //                         console.log('加载btn1成功');
    //                         subGameBtn.setParent(this.node);
    //                         this.SetNodeFront(subGameBtn);
    //                         director.emit('getSubGameBtnByPathEnd');
    //                     } else {
    //                         console.log('加载btn1失败');
    //                     };
    //         });
    //     } catch (error) {
    //         console.log('加载错误1' + error);
    //     };


        director.on('freeWinBegain',()=>{
            this.scheduleOnce(()=>{
                for (let i = 0; i < this.normalNodes.length; i++) {
                    const element = this.normalNodes[i];
                    element.active = false;
                };


                for (let i = 0; i < this.freeWinNodes.length; i++) {
                    const element = this.freeWinNodes[i];
                    element.active = true;
                    if(element.name=='stpr_freegame_idle'){
                        console.log('免费模式地图',element);
                        element.getComponent(sp.Skeleton).setAnimation(0,'stpr_freegame_idle',true);
                    };
                };
            },0.6);
        },this);

        director.on('freeWinOver',()=>{
            for (let i = 0; i < this.normalNodes.length; i++) {
                const element = this.normalNodes[i];
                element.active = true;
            };
            for (let i = 0; i < this.freeWinNodes.length; i++) {
                const element = this.freeWinNodes[i];
                element.active = false;
            };
        },this);

        director.on('uiTipsOpen',(mode,tex)=>{
            sUtil.toast(tex);
        },this);

        director.on('slotWinBoxItemInfo',(obj)=>{
            if(obj){
                //console.log('boxPos:',obj.boxPos);
                if(obj.boxPos){
                    const posStr = obj.boxPos.split('_');
                    if(posStr && posStr.length == 2){
                        const boxPos = sBoxMgr.instance.getBoxWorldPosByXY(parseInt(posStr[0]),parseInt(posStr[1]));
                        if(boxPos){
                            const boxWinTip : Node = sObjPool.Dequeue('boxWinTipLabel');
                            if(boxWinTip && boxWinTip.isValid){
                                boxWinTip.parent = sBoxAssetInit.instance.getTargetNodeByName('effectLayer');
                                boxWinTip.worldPosition = v3(boxPos.x,boxPos.y - 48,0);
                                boxWinTip.active = true;
                                boxWinTip.getComponent(StarlightPrincess_WinTipLabel).play(this.AddCommas(obj.rate * globalThis.GameBtnEntity.CurrentBetAmount));
                            }
                        }
                    }
                }
            }
        },this);

        
    }
}

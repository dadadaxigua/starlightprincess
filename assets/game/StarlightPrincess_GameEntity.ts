
import { _decorator, Button, color, Color, Component, director, Label, Node, Sprite, Tween, tween, UIOpacity, v3, Vec3, Widget } from 'cc';
import { sGameEntity } from '../lobby/hyPrefabs/gameRes/external/sGameEntity';
import sAudioMgr from '../lobby/game/core/sAudioMgr';
import { btnInternationalManager } from '../lobby/hyPrefabs/gameRes/external/btnInternationalManager';
import { sUtil } from '../lobby/game/core/sUtil';
const { ccclass, property } = _decorator;


@ccclass('StarlightPrincess_GameEntity')
export class StarlightPrincess_GameEntity extends sGameEntity {
    @property(Node)
    btnRenderNode : Node;
    @property(Button)
    infoBtn : Button;
    @property(Node)
    ruleNode : Node;
    @property(Label)
    betDetailLabel : Label;

    @property(Label)
    winStringLabel : Label;
    @property(Label)
    winStringLabel1 : Label;


    @property({type:[Button]})
    Btns = [];
    private betSpeedModeTemp1 = 'normal'; //normal,turbo 
    private shopNode:Button;
    private pTween=null;
    private callBack = null;
    protected onLoad () {
        super.onLoad();

        director.on('betBtnState',state=>{
            if(state == 'disable'){
                this.spinViewCtr(false);
            }else if(state == 'enable'){
                this.spinViewCtr(true);
            }
        },this);

        director.on('betBtnClick',(betSpeedMode)=>{
            sAudioMgr.PlayShotAudio('button');
            this.spinViewCtr(false);
        },this);

        director.on('autoBetInfoUpdate',value=>{
            // if(value == -1){
            //     this.spinViewCtr(false);
            // }else if(value > 0){
            //     this.spinViewCtr(false);
            // }else{
            //     this.spinViewCtr(true);
            // }
        },this);

        director.on('viewChange', (type, value) => {
            if (type == 'autoSpinCancel') {
                const toggle = this.autoPlayBtnNode.node.getChildByName('toggle');
                toggle.getChildByName('on').active = false;
                toggle.getChildByName('off').active = true;
            };
        },this);
    }
    protected start(): void {
        Promise.all([new Promise((resolve,reject)=>{
            globalThis.getSubGamePrefabByPath('prefabs/subGameUserCoin',(newObj : Node)=>{
                if(newObj){
                    newObj.parent = this.node.parent;
                    this.ownLabel = newObj.getChildByName('Label').getComponent(Label);
                    this.shopNode = newObj.getChildByName('shop').getComponent(Button);
                    newObj.getComponent(Widget).isAlignLeft=true;
                    newObj.getComponent(Widget).isAlignTop=true;
                    newObj.getComponent(Widget).left=-20;
                    newObj.getComponent(Widget).top=-10;
                };
                resolve(1);
            });
        })]).then((res)=>{
            super.start();
            if(this.shopNode){
                this.shopNode.node.on('click',()=>{
                    globalThis.openStore && globalThis.openStore();
                },this);
            };
        });
    }
    private spinViewCtr(value){
        this.btnRenderNode.getComponent(Sprite).grayscale = !value;
        this.btnRenderNode.children[0].getComponent(Sprite).grayscale = !value;
    }

    freeWinBegain(){
        // if(this.viewMode == 'record'){
        //     if(this.recordBtns){
        //         this.recordBtns.node.active = true;
        //     }
        // }
        // this.userCoin = this.beforeUserCoin;
        // this.winTotal = this.firstRate > 0 ? this.firstRate * this.betAmount : 0;
        for(let btn of this.Btns){
            btn.node.getComponent(UIOpacity).opacity=100;
            btn.getComponent(Button).interactable=false;
        }
    }

    freeWinOver(){
        // if(this.viewMode == 'record'){
        //     if(this.recordBtns){
        //         this.recordBtns.node.active = true;
        //     }
        // }
        // this.userCoin = this.beforeUserCoin;
        // this.winTotal = this.firstRate > 0 ? this.firstRate * this.betAmount : 0;
        for(let btn of this.Btns){
            btn.node.getComponent(UIOpacity).opacity=255;
            btn.getComponent(Button).interactable=true;
        }
    }

    // turboBtnAction() {
    //     globalThis.subGamePlayShotAudio('subGameClick');
    //     if (this.betSpeedModeTemp1 == 'normal') {
    //         this.autoPlayBtnNode.getComponent(UIOpacity).opacity=100;
    //         this.betSpeedModeTemp1 = 'turbo';
    //         this.turboBtnNodeClick(true);
    //     } else {
    //         this.autoPlayBtnNode.getComponent(UIOpacity).opacity=100;
    //         this.betSpeedModeTemp1 = 'normal';
    //         this.turboBtnNodeClick(false);
    //     }
    //     director.emit('uiTipsOpen', this.betSpeedModeTemp1, this.betSpeedModeTemp1 == 'normal' ? btnInternationalManager.GetDataByKey('closeTurboSpinMode') : btnInternationalManager.GetDataByKey('openTurboSpinMode'));
    // }
    turboBtnNodeClick(value: boolean) {
        // this.turboBtnNode.getComponent(UIOpacity).opacity=(value)?100:255;
        if(value){
            this.turboBtnNode.node.getChildByName('off').active=false;
            this.turboBtnNode.node.getChildByName('on').active=true;
        }else{
            this.turboBtnNode.node.getChildByName('off').active=true;
            this.turboBtnNode.node.getChildByName('on').active=false;
        }
    }
    protected autoPlayBtnClick(){
        if(this.canBet){
            if(this.betBtnState == 'normal'){
                if(this.betClickMode == 'normal'){
                    this.setGameBtnState('disable');
                    //this.autoPlayBtnNode.getComponent(UIOpacity).opacity=100;

                    const toggle = this.autoPlayBtnNode.node.getChildByName('toggle');
                    toggle.getChildByName('on').active = true;
                    toggle.getChildByName('off').active = false;
                    director.emit('viewChange','UIAutoSpin',-1);
                }else if(this.betClickMode == 'autoBet'){
                    const toggle = this.autoPlayBtnNode.node.getChildByName('toggle');
                    //this.autoPlayBtnNode.getComponent(UIOpacity).opacity=255;
                    toggle.getChildByName('on').active = false;
                    toggle.getChildByName('off').active = true;
                    director.emit('viewChange', 'autoSpinCancel');
                }
            }else{
                if(this.betClickMode == 'autoBet'){
                    //this.autoPlayBtnNode.getComponent(UIOpacity).opacity=255;
                    const toggle = this.autoPlayBtnNode.node.getChildByName('toggle');
                    toggle.getChildByName('on').active = false;
                    toggle.getChildByName('off').active = true;
                    director.emit('viewChange', 'autoSpinCancel');
                }
            }
        }else{
            this.betClickCoinNotEnough();
        }
    }

    protected errToOneRes(){
        const toggle = this.autoPlayBtnNode.node.getChildByName('toggle');
        toggle.getChildByName('on').active = false;
        toggle.getChildByName('off').active = true;
        super.errToOneRes();
    }

    protected menuBtnClick(){
        director.emit('subGameMenuView', true);
        const audioData = globalThis.getSubGameAudioVolume();
        if (audioData) {
            if (audioData.audioVolume == 1) {
                this.soundBtn.node.getChildByName('open').active = true;
                this.soundBtn.node.getChildByName('close').active = false;
            } else {
                this.soundBtn.node.getChildByName('close').active = true;
                this.soundBtn.node.getChildByName('open').active = false;
            }
        }
        this.menuBtns.node.active = true;
    }

    protected soundBtnClick(){
        if(this.audioTouchEnable){
            this.audioTouchEnable = false;
            const audioData = globalThis.getSubGameAudioVolume();
            if (audioData) {
                if (audioData.audioVolume == 1) {
                    globalThis.subGameAudioVolumeCtr(false);
                    this.soundBtn.node.getChildByName('close').active = true;
                    this.soundBtn.node.getChildByName('open').active = false;
                } else {
                    globalThis.subGameAudioVolumeCtr(true);
                    this.soundBtn.node.getChildByName('open').active = true;
                    this.soundBtn.node.getChildByName('close').active = false;
                }
            }
            this.scheduleOnce(()=>{
                this.audioTouchEnable = true;
            },1);
        }
    }

    protected closeBtnClick(){
        director.emit('subGameMenuView', false);
        this.menuBtns.node.active = false;
    }

    protected rulesBtnClick(){
        this.ruleNode.active = true;
    }

    protected addBtnClick(){
        super.addBtnClick();
        sAudioMgr.PlayShotAudio('button');
    }

    protected minusBtnClick(){
        super.minusBtnClick();
        sAudioMgr.PlayShotAudio('button');
    }

    protected updateBetInfo() {
        super.updateBetInfo();
        this.betDetailLabel.string = `${this.AddCommas(this.betAmount)} x ${20}`;
    }

    betUserInfoUpdate(own, bet, win) {
        if (this.ownLabel) {
            if (own || own == 0) {
                this.ownLabel.node['rollAnimaValue'] = own;
                this.ownLabel.string = this.AddCommas(Math.trunc(own));
            }
        }
        if (this.betLabel) {
            if (bet || bet == 0) {
                this.betLabel.node['rollAnimaValue'] = bet;
                this.betLabel.string = this.AddCommas(Math.trunc(bet));
            }
        };
        
        if(win==null){
            this.winLabel.node['rollAnimaValue'] = 0;
            console.log('重置winLabel1');
            this.winLabel.node.active=false;
            this.winStringLabel.node.active=true
            this.winStringLabel1.node.active=false
            this.winLabel.string=0+'';
        }else{
            if (win && win > 0) {
                //console.log('win1:',win);
                this.winLabel.node['rollAnimaValue'] = win;
                console.log('重置winLabel2');
                this.winLabel.string = this.AddCommas(Math.trunc(win));
                this.winStringLabel1.node.active=true
            }else{
                //console.log('win2:',win);
               // this.winLabel.string=0+'';
                
            }
        };
    }

    betUserInfoUpdateAnima(own, bet, win, animaTime = 1, callBack = null) {
        if (own || own == 0) {
            this.CommasLabelAnima(this.ownLabel, Math.trunc(own), animaTime);
        };
        if (bet || bet == 0) {
            this.CommasLabelAnima(this.betLabel, Math.trunc(bet), animaTime);
        }; 
        
        if(win==null){
            this.winLabel.node['rollAnimaValue'] = 0;
            console.log('重置winLabel3');
            this.winLabel.string=0+'';
        }else{
            if (win &&win > 0) {
                this.winLabel.node.active=true;
                this.winStringLabel.node.active=false;
                this.winStringLabel1.node.active=true
                //console.log('win3:',win);
                let time=0.5;
                if(win>=this.currentBetAmount*200){
                    time=12;
                };
                if(!this.winLabel.node['rollAnimaValue']){
                    //c//onsole.log('重置winLabel4');
                    this.winLabel.node['rollAnimaValue']=0;
                };
                //this.winLabel.string = this.AddCommas(Math.floor(win));
                let num=parseInt(this.winLabel.string);
                //console.log('win:',win);
                //console.log('num:',num);
                //console.log('this.winLabel.string:',this.winLabel.string);
                //console.log('rollAnimaValue1',this.winLabel.node['rollAnimaValue']);
                //sUtil.TweenLabel(this.winLabel.node['rollAnimaValue'],Math.trunc(win+this.winLabel.node['rollAnimaValue']),this.winLabel,time);

                this.updateWinLab(this.winLabel.node['rollAnimaValue'],this.winLabel, Math.trunc(win+this.winLabel.node['rollAnimaValue']),time,this.callBack);
                
                //this.winLabel.node['rollAnimaValue'] += win;
                this.scheduleOnce(()=>{
                    this.winLabel.node['rollAnimaValue']+=Math.trunc(win);
                    //console.log('rollAnimaValue2',this.winLabel.node['rollAnimaValue']);
                },time);
                director.off('StopLabelanima');
                sUtil.once('StopLabelanima',()=>{
                    //this.winLabel.node['rollAnimaValue'] += win;
                    this.updateLabTween.stop();
                    this.winLabel.string=this.AddCommas(Math.floor(win));
                });
            }else{
                if(!this.winLabel.node['rollAnimaValue']){
                    //console.log('重置winLabel5');
                    this.winLabel.node['rollAnimaValue']=0;
                    this.winLabel.string=0+'';
                };
                //console.log('win4:',win);
                // this.winLabel.node.active=false;
                // this.winStringLabel.node.active=true;
                // this.winStringLabel1.node.active=false
            };
        };
        

        if (callBack != null) {
            this.scheduleOnce(callBack, animaTime);
        };
    };
    updateLabTween: Tween<any>;
    updateWinLab(lastCoin: number,winLabel: Label, coin: number, time: number,cb:Function) {
        let tweenTargetVec3 = v3(lastCoin, lastCoin, lastCoin);
        this.updateLabTween && this.updateLabTween.stop()
        this.updateLabTween = tween(tweenTargetVec3).to(time, v3(coin, coin, coin), {
            "onUpdate": (target: Vec3) => {
                if (winLabel) { 
                    //winLabel.string = sUtil.AddCommas(Math.floor(target.x));
                    winLabel.string = sUtil.AddCommas(Math.trunc(target.x));
                };
            }, easing: undefined
        }).call(() => {
            winLabel!.string = this.AddCommas(Math.trunc(coin));
            //this.winLabel.node['rollAnimaValue'] += coin;
            if(cb) cb();
        }).start();
    };


    // updateLabTween: Tween<any>;
    // CommasLabelAnima(label, value, time) {
    //     if (label) {
    //         const valueOri = label.node['rollAnimaValue'];
    //         const mTween = this.updateLabTween;
    //         if (mTween) {
    //             mTween.stop();
    //         };

    //         if(value>0){
    //             director.off('StopLabelanima');
    //             sUtil.once('StopLabelanima',()=>{
    //                 if(mTween){
    //                     mTween.stop();
    //                     //this.unscheduleAllCallbacks();
    //                     tween(tweenTargetVec3).stop();
    //                 };
                    
    //                 label.string = this.AddCommas(Math.floor(value));
    //             })
    //         };

    //         let tweenTargetVec3 = v3(valueOri, valueOri, valueOri);
    //         // console.log('tweenTargetVec3:'+tweenTargetVec3);
    //         this.updateLabTween = tween(tweenTargetVec3).to(time, v3(value, value, value), {
    //             "onUpdate": (target: Vec3) => {
    //                 if (label) {
    //                     //label.node['rollAnimaValue'] = Math.floor(target.x)
    //                     label.string = this.AddCommas(Math.floor(target.x));
    //                 };
    //             }, easing: 'quadOut'
    //         }).call(() => {
    //             if (label) {
    //                 //console.log('rollAnimaValue',label.node['rollAnimaValue']);
    //                 //label.node['rollAnimaValue'] = Math.floor(value)
    //                 label.string = this.AddCommas(Math.floor(value));
    //             };
    //         }).start();
    //     };
    // };
    CommasLabelAnima(label, value, time) {
        if (label && (label.node['rollAnimaValue'] || label.node['rollAnimaValue'] == 0)) {
            const valueOri = label.node['rollAnimaValue'];
            const mTween = label.node['rollAnimaTween'];
            if (mTween) {
                mTween.stop();
            };
            if(value>0){
                //director.off('StopLabelanima');
                // sUtil.once('StopLabelanima',()=>{
                //     if(mTween){
                //         mTween.stop();
                //     };
                    
                //     label.string = this.AddCommas(Math.floor(value));
                // })
            };
            let tweenTargetVec3 = v3(valueOri, valueOri, valueOri);
            // console.log('tweenTargetVec3:'+tweenTargetVec3);
            label.node['rollAnimaTween'] = tween(tweenTargetVec3).to(12, v3(value, value, value), {
                "onUpdate": (target: Vec3) => {
                    if (label) {
                        label.node['rollAnimaValue'] = Math.floor(target.x)
                        //label.string = this.AddCommas(Math.floor(target.x));
                    }
                }, easing: 'quadOut'
            }).call(() => {
                if (label) {
                    label.node['rollAnimaValue'] = Math.floor(value)
                    label.string = this.AddCommas(Math.floor(value));
                }
            }).start();
        }
    }
    protected MaxBetBtnClick() {
        if (this.clientConfig && this.betBtnState == 'normal') {
            let findMax = false;
            let bet_types = this.clientConfig.bet_types;
            if (bet_types && bet_types.length > 0) {
                for (let i = 0; i < bet_types.length; i++) {
                    const element = bet_types[i];
                    console.log('addBtnClick:',JSON.stringify(element));
                    if (element.game_mode == 1 && element.game_id == this.gameid) {
                        if (element.bet_amount > this.betAmount) {
                            this.betAmount = element.bet_amount;
                            this.betType = element.bet_type;
                            director.emit('sSlotBetInfoUpdate', { betAmount: this.betAmount, multiple: this.lineAmount });
                            if (this.betLabel) {
                                this.betLabel.node.setScale(1.5, 1.5, 1.5);
                                tween(this.betLabel.node).to(0.01, { scale: Vec3.ONE }, { easing: 'elasticOut' }).start();
                            }
                            this.updateBetInfo();
                            findMax = true;
                            this.saveBetAmountValue();
                            //break;
                        };
                    };
                };
            };
            // if (!findMax) {
            //     director.emit('uiTipsOpen', 'text', btnInternationalManager.GetDataByKey('maximunBet'));
            // }
        }
    }
}

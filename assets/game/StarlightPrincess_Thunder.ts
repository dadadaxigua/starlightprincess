
import { _decorator, Component, Node, sp } from 'cc';
import { sComponent } from '../lobby/game/core/sComponent';
import { sObjPool } from '../lobby/game/core/sObjPool';
import sAudioMgr from '../lobby/game/core/sAudioMgr';
const { ccclass, property } = _decorator;


@ccclass('StarlightPrincess_Thunder')
export class StarlightPrincess_Thunder extends sComponent {
    @property(sp.Skeleton)
    thunder : sp.Skeleton;

    protected start () {

    }

    public play(animaName){
        if(this.thunder && animaName){
            sAudioMgr.PlayShotAudio('multiStop');
            this.thunder.setAnimation(0,animaName,false);
            this.thunder.setCompleteListener(()=>{
                sObjPool.Enqueue(this.node.name,this.node);
                this.node.active = false; 
            });
        }
    }
}


import { _decorator, Component, director, Label, Node, sp, tween, UIOpacity, v3 } from 'cc';
import { sComponent } from '../lobby/game/core/sComponent';
import sAudioMgr from '../lobby/game/core/sAudioMgr';
const { ccclass, property } = _decorator;

 
@ccclass('StarlightPrincess_UIFreewinBingoFree')
export class StarlightPrincess_UIFreewinBingoFree extends sComponent {
    @property(Node)
    freeBtn : Node;
    @property(Label)
    numLabel : Label;
    @property(Node)
    contentNode : Node;
    @property(sp.Skeleton)
    popSpine : sp.Skeleton;
    @property(sp.Skeleton)
    thunderSpine : sp.Skeleton;
    @property(Node)
    LabelNode : Node;
    private touchEnable = true;

    protected start () {
        this.freeBtn.on('click',()=>{
            if(this.touchEnable){
                this.unscheduleAllCallbacks();
                this.cleanTweenList();
                this.touchEnable = false;
                //this.popSpine.timeScale = 2.5;
                this.popSpine.setAnimation(0,'stpr_banner_out',false);
                //this.scheduleOnce(()=>{
                this.LabelNode.active=true;
                for(let nNode of this.LabelNode.children){
                    nNode.getComponent(UIOpacity).opacity=255;
                    this.pushIDTween(tween(nNode.getComponent(UIOpacity)).to(0.5,{opacity:0}).start(),'LabelNode1');
                };
                //},0.2)
                this.popSpine.setCompleteListener(()=>{
                    director.emit('bingoFreewinInFreeEnd');
                    this.contentNode.active = false;
                    this.touchEnable = true;
                    //this.popSpine.setCompleteListener(()=>{
                    this.popSpine.node.active=false;
                    //});
                    // this.scheduleOnce(()=>{
                    //     director.emit('bingoFreewinInFree',15);
                    // },2)
                });
            }
        },this);

        director.on('bingoFreewinInFree',(count)=>{
            this.contentNode.active = true;
            //sAudioMgr.PlayShotAudio('freegame_frame_open',1);

            this.popSpine.node.active=true;
            this.popSpine.setAnimation(0,'stpr_banner_in',false);
            this.popSpine.setCompleteListener(()=>{
                this.popSpine.timeScale = 1;
                this.popSpine.setAnimation(0,'stpr_banner_loop',true);
            });

            this.LabelNode.active=true;
            let x=0;
            for(let nNode of this.LabelNode.children){
                
                nNode.getComponent(UIOpacity).opacity=0;
                this.pushIDTween(tween(nNode.getComponent(UIOpacity)).delay(0.3).call(()=>{
                    x++;
                    if(x==1){
                        sAudioMgr.PlayShotAudio('freegame_frame_open',0.8);
                    };
                }).delay(0.5).to(0.2,{opacity:255}).start(),'LabelNode1');
                this.pushIDTween(tween(nNode).delay(0.8).to(0.2,{scale:v3(1.5,1.5,1.5)}).to(0.2,{scale:v3(1,1,1)}).start(),'LabelNode2');
            };

            sAudioMgr.PlayAudio('fsinout_bgm');
            this.setLabelNum(count);
            this.node.setSiblingIndex(this.node.parent.children.length - 1);
            this.unscheduleAllCallbacks();
            this.cleanTweenList();
            this.scheduleOnce(()=>{
                this.freeBtn.emit('click');
            },3);
        },this);
        // this.scheduleOnce(()=>{
        //     director.emit('bingoFreewinInFree',15);
            
        // },3)
        //
    }

    onEnable(){
 
    }


    setLabelNum(num){
        this.numLabel.string = num.toString();
    }

    private playThunderAnima(){
        if(this.thunderSpine){
            this.thunderSpine.node.active = true;
            //sAudioMgr.PlayAudio('freegame_transitions');
            this.thunderSpine.setAnimation(0,'stpr_transition',false);
            this.thunderSpine.setCompleteListener(()=>{
                this.thunderSpine.node.active = false;
            });
        }
    }

}


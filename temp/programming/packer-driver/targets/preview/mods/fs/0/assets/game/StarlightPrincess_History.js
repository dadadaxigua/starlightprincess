System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2", "__unresolved_3", "__unresolved_4"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, Button, director, Label, Node, Sprite, tween, v3, Vec3, sComponent, sObjPool, assetMgr, sConfigMgr, _dec, _dec2, _dec3, _dec4, _dec5, _dec6, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _descriptor5, _temp, _crd, ccclass, property, StarlightPrincess_History;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfsComponent(extras) {
    _reporterNs.report("sComponent", "../lobby/game/core/sComponent", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsObjPool(extras) {
    _reporterNs.report("sObjPool", "../lobby/game/core/sObjPool", _context.meta, extras);
  }

  function _reportPossibleCrUseOfassetMgr(extras) {
    _reporterNs.report("assetMgr", "../lobby/game/core/sAssetMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsConfigMgr(extras) {
    _reporterNs.report("sConfigMgr", "../lobby/game/core/sConfigMgr", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      Button = _cc.Button;
      director = _cc.director;
      Label = _cc.Label;
      Node = _cc.Node;
      Sprite = _cc.Sprite;
      tween = _cc.tween;
      v3 = _cc.v3;
      Vec3 = _cc.Vec3;
    }, function (_unresolved_2) {
      sComponent = _unresolved_2.sComponent;
    }, function (_unresolved_3) {
      sObjPool = _unresolved_3.sObjPool;
    }, function (_unresolved_4) {
      assetMgr = _unresolved_4.assetMgr;
    }, function (_unresolved_5) {
      sConfigMgr = _unresolved_5.sConfigMgr;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "7f90fqRFR5DJ5gedyPUEE+x", "StarlightPrincess_History", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("StarlightPrincess_History", StarlightPrincess_History = (_dec = ccclass('StarlightPrincess_History'), _dec2 = property(Node), _dec3 = property(Button), _dec4 = property(Button), _dec5 = property(Sprite), _dec6 = property(Sprite), _dec(_class = (_class2 = (_temp = class StarlightPrincess_History extends (_crd && sComponent === void 0 ? (_reportPossibleCrUseOfsComponent({
        error: Error()
      }), sComponent) : sComponent) {
        constructor() {
          super(...arguments);

          _initializerDefineProperty(this, "itemsNode", _descriptor, this);

          _initializerDefineProperty(this, "upBtn", _descriptor2, this);

          _initializerDefineProperty(this, "downBtn", _descriptor3, this);

          _initializerDefineProperty(this, "upSprite", _descriptor4, this);

          _initializerDefineProperty(this, "downSprite", _descriptor5, this);

          _defineProperty(this, "itemsList", []);

          _defineProperty(this, "viewItenMaxNum", 5);

          _defineProperty(this, "rollIndex", 0);

          _defineProperty(this, "touchEnable", false);
        }

        start() {
          director.on('slotSubGameRollBegin', () => {
            this.removeAllItem();
            this.removeIDTween('itemsNode');
            this.touchEnable = false;
            this.rollIndex = 0;
            this.itemsNode.position = Vec3.ZERO;
            this.btnStateCtr();
          }, this);
          director.on('slotWinBoxItemInfo', obj => {
            if (obj) {
              this.createItem(obj.symbol, obj.rate, obj.num);
            }

            ;
          }, this);
          this.upBtn.node.on('click', () => {
            if (this.touchEnable) {
              this.ScrollItemView(1);
            }

            ;
          }, this);
          this.downBtn.node.on('click', () => {
            if (this.touchEnable) {
              this.ScrollItemView(-1);
            }

            ;
          }, this);
        }

        createItem(symbol, rate, num) {
          this.touchEnable = false; // if(this.itemsList.length >= 5){
          //     for (let i = 0; i < this.itemsList.length; i++) {
          //         const tItem = this.itemsList[i];
          //         this.removeIDTween('createItem_'+tItem.uuid);
          //         this.pushIDTween(tween(tItem).to(0.2,{position : v3(0,(4 - this.itemsList.length + i) * 46,0)},{easing : 'cubicIn'}).start(),'createItem_'+tItem.uuid);
          //     }
          // }

          if (this.itemsList.length >= this.viewItenMaxNum) {
            this.removeIDTween('itemsNode');
            this.pushIDTween(tween(this.itemsNode).to(0.2, {
              position: v3(0, (this.viewItenMaxNum - 1 - this.itemsList.length) * 40, 0)
            }).start(), 'itemsNode');
          }

          var historyItem = (_crd && sObjPool === void 0 ? (_reportPossibleCrUseOfsObjPool({
            error: Error()
          }), sObjPool) : sObjPool).Dequeue('historyItem');

          if (historyItem && historyItem.isValid) {
            historyItem.parent = this.itemsNode; // const index = this.itemsList.length >= 4 ? 4 : this.itemsList.length;

            var index = this.itemsList.length;
            historyItem.position = v3(0, 240 + index * 40 + 15, 0);
            historyItem.active = true;
            var renderData = (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
              error: Error()
            }), sConfigMgr) : sConfigMgr).instance.GetSymbolBoxImg(symbol);

            if (renderData) {
              var icon = historyItem.getChildByName('icon');

              if (icon) {
                icon.getComponent(Sprite).spriteFrame = (_crd && assetMgr === void 0 ? (_reportPossibleCrUseOfassetMgr({
                  error: Error()
                }), assetMgr) : assetMgr).GetAssetByName(renderData.rest.render_name);
              }

              ; //console.log('history:',this.AddCommas(Math.floor(rate * globalThis.GameBtnEntity.CurrentBetAmount)));

              historyItem.getChildByName('num').getComponent(Label).string = num;
              historyItem.getChildByName('moy').getComponent(Label).string = this.AddCommas(Math.floor(rate * globalThis.GameBtnEntity.CurrentBetAmount)); //sUtil.AddCommas(rate * globalThis.GameBtnEntity.CurrentBetAmount);
            }

            ;
            this.removeIDTween('createItem_' + historyItem.uuid);
            this.pushIDTween(tween(historyItem).delay(index * 0.05).to(0.3, {
              position: v3(0, index * 40 + 15, 0)
            }, {
              easing: 'cubicIn'
            }).by(0.1, {
              position: v3(0, 10, 0)
            }, {
              easing: 'cubicOut'
            }).by(0.1, {
              position: v3(0, -10, 0)
            }, {
              easing: 'cubicIn'
            }).call(() => {
              this.btnStateCtr();
              this.touchEnable = true;
            }).start(), 'createItem_' + historyItem.uuid);
            this.itemsList.push(historyItem);
            this.rollIndex = this.viewItenMaxNum - this.itemsList.length;
          }

          ;
        }

        removeAllItem() {
          var _this = this;

          var _loop = function _loop(i) {
            var tItem = _this.itemsList[i];

            _this.removeIDTween('createItem_' + tItem.uuid);

            _this.pushIDTween(tween(tItem).delay(i * 0.05).by(0.3, {
              position: v3(0, -200, 0)
            }, {
              easing: 'cubicIn'
            }).call(() => {
              (_crd && sObjPool === void 0 ? (_reportPossibleCrUseOfsObjPool({
                error: Error()
              }), sObjPool) : sObjPool).Enqueue('historyItem', tItem);
              tItem.active = false;
            }).start(), 'createItem_' + tItem.uuid);
          };

          for (var i = 0; i < this.itemsList.length; i++) {
            _loop(i);
          }

          this.itemsList = [];
        }

        ScrollItemView(value) {
          this.touchEnable = false;
          this.rollIndex += value;

          if (this.rollIndex > 0) {
            this.rollIndex = 0;
          } else if (this.rollIndex < this.viewItenMaxNum - this.itemsList.length) {
            this.rollIndex = this.viewItenMaxNum - this.itemsList.length;
          }

          this.removeIDTween('itemsNode');
          this.pushIDTween(tween(this.itemsNode).to(0.2, {
            position: v3(0, this.rollIndex * 40, 0)
          }).call(() => {
            this.touchEnable = true;
            this.btnStateCtr();
          }).start(), 'itemsNode');
        }

        btnStateCtr() {
          this.upSprite.grayscale = true;
          this.downSprite.grayscale = true;
          this.upBtn.interactable = false;
          this.downBtn.interactable = false;

          if (this.itemsList && this.itemsList.length > this.viewItenMaxNum) {
            if (this.itemsNode.position.y >= 0) {
              this.downSprite.grayscale = false;
              this.downBtn.interactable = true;
            } else if (this.itemsNode.position.y <= (this.viewItenMaxNum - this.itemsList.length) * 40) {
              this.upSprite.grayscale = false;
              this.upBtn.interactable = true;
            } else {
              this.upSprite.grayscale = false;
              this.downSprite.grayscale = false;
              this.upBtn.interactable = true;
              this.downBtn.interactable = true;
            }
          }
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "itemsNode", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "upBtn", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "downBtn", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "upSprite", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor5 = _applyDecoratedDescriptor(_class2.prototype, "downSprite", [_dec6], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=StarlightPrincess_History.js.map
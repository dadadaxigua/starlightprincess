System.register(["cc"], function (_export, _context) {
  "use strict";

  var _cclegacy, sInternationalManager, _crd;

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  _export("sInternationalManager", void 0);

  return {
    setters: [function (_cc) {
      _cclegacy = _cc.cclegacy;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "9e1f29JYxtAdo8uH/Euui/g", "sInternationalManager", undefined);

      _export("sInternationalManager", sInternationalManager = class sInternationalManager {
        static GetDataByKey(key) {
          if (globalThis.GetLanguageType) {
            if (!sInternationalManager.mData) {
              sInternationalManager.mData = sInternationalManager.jsonData.get(globalThis.GetLanguageType());
            }

            if (sInternationalManager.mData) {
              var value = sInternationalManager.mData[key];

              if (value) {
                return value.text;
              }
            }
          }

          return '';
        }

        static SetLanguegeData(type, data) {
          if (type && data) {
            sInternationalManager.jsonData.set(type, data);
          }
        }

        static Clear() {
          sInternationalManager.mData = null;
          sInternationalManager.jsonData.clear();
        }

      });

      _defineProperty(sInternationalManager, "jsonData", new Map());

      _defineProperty(sInternationalManager, "mData", void 0);

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=sInternationalManager.js.map
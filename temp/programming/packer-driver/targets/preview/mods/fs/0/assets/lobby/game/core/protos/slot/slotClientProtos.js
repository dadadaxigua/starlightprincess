System.register(["cc"], function (_export, _context) {
  "use strict";

  var _cclegacy, _crd;

  return {
    setters: [function (_cc) {
      _cclegacy = _cc.cclegacy;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "962eeS04IZKt6EJl7rgc/UQ", "slotClientProtos", undefined);

      _export("default", {
        "nested": {
          "slot": {
            "nested": {
              "mainHandler": {
                "nested": {
                  "bet": {
                    "fields": {
                      "game_id": {
                        "type": "int32",
                        "id": 1
                      },
                      "bet_type": {
                        "type": "int32",
                        "id": 2
                      },
                      "bet_time": {
                        "type": "int64",
                        "id": 3
                      },
                      "game_mode": {
                        "type": "int32",
                        "id": 4
                      },
                      "bet_amount": {
                        "type": "double",
                        "id": 5
                      },
                      "input": {
                        "type": "double",
                        "id": 6
                      }
                    }
                  },
                  "report": {
                    "fields": {
                      "round_tag": {
                        "rule": "repeated",
                        "type": "int32",
                        "id": 1
                      },
                      "bet_time": {
                        "type": "int64",
                        "id": 2
                      },
                      "result_id": {
                        "type": "string",
                        "id": 3
                      },
                      "game_id": {
                        "type": "int32",
                        "id": 4
                      }
                    }
                  }
                }
              }
            }
          }
        }
      });

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=slotClientProtos.js.map
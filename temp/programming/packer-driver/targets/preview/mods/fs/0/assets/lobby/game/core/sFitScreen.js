System.register(["cc"], function (_export, _context) {
  "use strict";

  var _cclegacy, view, size, sFitScreen, _crd;

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  _export("sFitScreen", void 0);

  return {
    setters: [function (_cc) {
      _cclegacy = _cc.cclegacy;
      view = _cc.view;
      size = _cc.size;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "17b37ULzM1KxKbKewW+cfHD", "sFitScreen", undefined);

      _export("sFitScreen", sFitScreen = class sFitScreen {
        static Init() {
          sFitScreen.windowSize = size(view.getVisibleSize()); // sFitScreen.windowSize.width -= 600;
          // sFitScreen.windowSize.height -= 400;

          console.log('sFitScreen.windowSize:' + sFitScreen.windowSize);
        }

        static GetWindowSize() {
          return sFitScreen.windowSize;
        }

        static IsInsideWindow(worldPos) {
          if (sFitScreen.windowSize) {
            if (worldPos.x < sFitScreen.windowSize.x && worldPos.x > 0 && worldPos.y > 0 && worldPos.y < sFitScreen.windowSize.y) {
              return true;
            }
          }

          return false;
        }

      });

      _defineProperty(sFitScreen, "windowSize", void 0);

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=sFitScreen.js.map
System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2", "__unresolved_3", "__unresolved_4", "__unresolved_5", "__unresolved_6"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, assetManager, Camera, Color, director, gfx, Tween, sAudioMgr, sComponent, sInternationalManager, sObjPool, subGameReq, sUtil, assetMgr, _crd, timeCall;

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _reportPossibleCrUseOfsAudioMgr(extras) {
    _reporterNs.report("sAudioMgr", "./sAudioMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsComponent(extras) {
    _reporterNs.report("sComponent", "./sComponent", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsInternationalManager(extras) {
    _reporterNs.report("sInternationalManager", "./sInternationalManager", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsObjPool(extras) {
    _reporterNs.report("sObjPool", "./sObjPool", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsubGameReq(extras) {
    _reporterNs.report("subGameReq", "./subGameReq", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsUtil(extras) {
    _reporterNs.report("sUtil", "./sUtil", _context.meta, extras);
  }

  _export("assetMgr", void 0);

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      assetManager = _cc.assetManager;
      Camera = _cc.Camera;
      Color = _cc.Color;
      director = _cc.director;
      gfx = _cc.gfx;
      Tween = _cc.Tween;
    }, function (_unresolved_2) {
      sAudioMgr = _unresolved_2.default;
    }, function (_unresolved_3) {
      sComponent = _unresolved_3.sComponent;
    }, function (_unresolved_4) {
      sInternationalManager = _unresolved_4.sInternationalManager;
    }, function (_unresolved_5) {
      sObjPool = _unresolved_5.sObjPool;
    }, function (_unresolved_6) {
      subGameReq = _unresolved_6.subGameReq;
    }, function (_unresolved_7) {
      sUtil = _unresolved_7.sUtil;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "3d286a/7EpNM6Z8sYCGV4lD", "sAssetMgr", undefined);

      _export("assetMgr", assetMgr = class assetMgr {
        static SetSpriteFramesArrayToDic(atlas) {
          if (atlas && atlas.length > 0) {
            for (var i = 0; i < atlas.length; i++) {
              var element = atlas[i];
              var sprites = element.getSpriteFrames();

              if (sprites && sprites.length > 0) {
                for (var n = 0; n < sprites.length; n++) {
                  var sprite = sprites[n];
                  assetMgr.assetDic[sprite.name] = sprite;
                }
              }
            }
          }
        }

        static SetSpriteFramesToDic(atlas) {
          if (atlas) {
            var sprites = atlas.getSpriteFrames();

            if (sprites && sprites.length > 0) {
              for (var n = 0; n < sprites.length; n++) {
                var sprite = sprites[n];
                assetMgr.assetDic[sprite.name] = sprite;
              }
            }
          }
        }

        static SetSortSpriteFramesArrayToDic(atlas) {
          if (atlas && atlas.length > 0) {
            for (var i = 0; i < atlas.length; i++) {
              var element = atlas[i];
              var sprites = element.getSpriteFrames();

              if (sprites && sprites.length > 0) {
                var dic = sprites.sort((a, b) => {
                  var indexs1 = a.name.split('_');
                  var indexs2 = b.name.split('_');

                  if (indexs1 && indexs2) {
                    var num1 = parseInt(indexs1[indexs1.length - 1]);
                    var num2 = parseInt(indexs2[indexs2.length - 1]);

                    if (!isNaN(num1) && !isNaN(num2)) {
                      return num1 - num2;
                    }
                  }

                  return 0;
                });
                assetMgr.assetDic[element.name] = dic;
              }
            }
          }
        }

        static SetSingleSpriteFramesArrayToDic(sprites) {
          if (sprites && sprites.length > 0) {
            for (var i = 0; i < sprites.length; i++) {
              var sprite = sprites[i];
              assetMgr.assetDic[sprite.name] = sprite;
            }
          }
        }

        static SetAssembleSortSpriteFramesArrayToDic(atlas) {
          if (atlas && atlas.length > 0) {
            var resDic = {};

            for (var i = 0; i < atlas.length; i++) {
              var element = atlas[i];
              var sprites = element.getSpriteFrames();

              if (sprites && sprites.length > 0) {
                for (var s = 0; s < sprites.length; s++) {
                  var _sprite = sprites[s];

                  var last_Index = _sprite.name.lastIndexOf('_');

                  if (last_Index >= 0) {
                    var sName = _sprite.name.substring(0, last_Index); // const index = _sprite.name.substring(last_Index,_sprite.name.length);


                    var res = resDic[sName];

                    if (!res) {
                      res = [];
                      resDic[sName] = res;
                    }

                    res.push(_sprite);
                  }
                }
              }
            } // console.log(resDic);


            var resDicKeys = Object.keys(resDic);

            if (resDicKeys && resDicKeys.length > 0) {
              for (var d = 0; d < resDicKeys.length; d++) {
                var key = resDicKeys[d];
                var _sprites = resDic[key];

                if (_sprites && Array.isArray(_sprites) && _sprites.length > 0) {
                  var dic = _sprites.sort((a, b) => {
                    var indexs1 = a.name.split('_');
                    var indexs2 = b.name.split('_');

                    if (indexs1 && indexs2) {
                      var num1 = parseInt(indexs1[indexs1.length - 1]);
                      var num2 = parseInt(indexs2[indexs2.length - 1]);

                      if (!isNaN(num1) && !isNaN(num2)) {
                        return num1 - num2;
                      }
                    }

                    return 0;
                  });

                  assetMgr.assetDic[key] = dic;
                }
              }
            }
          }
        }

        static SetSkeletonDataArrayToDic(skeletons) {
          if (skeletons && skeletons.length > 0) {
            for (var i = 0; i < skeletons.length; i++) {
              var element = skeletons[i];
              assetMgr.assetDic[element.name] = element;
            }
          }
        }
        /**
         * @zh
         * 用key名字获取资源
         */


        static GetAssetByName(key) {
          return assetMgr.assetDic[key];
        }

        static GetBundle() {
          if (assetMgr.mBundle) {
            return assetMgr.mBundle;
          } else {
            assetMgr.mBundle = assetManager.getBundle(globalThis.currentPlayingGameID);
            return assetMgr.mBundle;
          }
        }

        static Clear() {
          assetMgr.assetDic = {};
          assetMgr.mBundle = null;
        }

      });

      _defineProperty(assetMgr, "mBundle", null);

      _defineProperty(assetMgr, "assetDic", {});

      timeCall = null;

      globalThis.subGameBackToHall = function () {
        if (!timeCall) {
          var _scene = director.getScene();

          if (_scene) {
            var _node = _scene.children[0];

            if (_node) {
              var comps = _node.getComponentsInChildren(_crd && sComponent === void 0 ? (_reportPossibleCrUseOfsComponent({
                error: Error()
              }), sComponent) : sComponent);

              if (comps && comps.length > 0) {
                for (var i = 0; i < comps.length; i++) {
                  var element = comps[i];
                  element.clean();
                }
              }

              var _camera = _node.getComponentInChildren(Camera);

              if (_camera) {
                _camera.clearFlags = gfx.ClearFlagBit.COLOR;
                _camera.clearColor = Color.BLACK;
                _camera.visibility = 0;
              }
            }
          }

          (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
            error: Error()
          }), sAudioMgr) : sAudioMgr).clearAudioTween();
          assetMgr.Clear();
          (_crd && sObjPool === void 0 ? (_reportPossibleCrUseOfsObjPool({
            error: Error()
          }), sObjPool) : sObjPool).Clear();
          (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
            error: Error()
          }), sAudioMgr) : sAudioMgr).Clear();
          (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
            error: Error()
          }), sUtil) : sUtil).Clear();
          Tween.stopAll();
          (_crd && sInternationalManager === void 0 ? (_reportPossibleCrUseOfsInternationalManager({
            error: Error()
          }), sInternationalManager) : sInternationalManager).Clear();
          (_crd && subGameReq === void 0 ? (_reportPossibleCrUseOfsubGameReq({
            error: Error()
          }), subGameReq) : subGameReq).Clear();
          director.emit('backToHall');
          timeCall = setTimeout(() => {
            timeCall = null;

            if (globalThis.backToHall) {
              globalThis.backToHall();
            }
          }, 200);
        }
      };

      globalThis.subGameClear = function () {
        var _scene = director.getScene();

        if (_scene) {
          var _node = _scene.children[0];

          if (_node) {
            var comps = _node.getComponentsInChildren(_crd && sComponent === void 0 ? (_reportPossibleCrUseOfsComponent({
              error: Error()
            }), sComponent) : sComponent);

            if (comps && comps.length > 0) {
              for (var i = 0; i < comps.length; i++) {
                var element = comps[i];
                element.clean();
              }
            }

            var _camera = _node.getComponentInChildren(Camera);

            if (_camera) {
              _camera.clearFlags = gfx.ClearFlagBit.COLOR;
              _camera.clearColor = Color.BLACK;
              _camera.visibility = 0;
            }
          }
        }

        (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
          error: Error()
        }), sAudioMgr) : sAudioMgr).clearAudioTween();
        assetMgr.Clear();
        (_crd && sObjPool === void 0 ? (_reportPossibleCrUseOfsObjPool({
          error: Error()
        }), sObjPool) : sObjPool).Clear();
        (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
          error: Error()
        }), sAudioMgr) : sAudioMgr).Clear();
        Tween.stopAll();
        (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
          error: Error()
        }), sUtil) : sUtil).Clear();
        (_crd && sInternationalManager === void 0 ? (_reportPossibleCrUseOfsInternationalManager({
          error: Error()
        }), sInternationalManager) : sInternationalManager).Clear();
        (_crd && subGameReq === void 0 ? (_reportPossibleCrUseOfsubGameReq({
          error: Error()
        }), subGameReq) : subGameReq).Clear();
        director.emit('backToHall');
      };

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=sAssetMgr.js.map
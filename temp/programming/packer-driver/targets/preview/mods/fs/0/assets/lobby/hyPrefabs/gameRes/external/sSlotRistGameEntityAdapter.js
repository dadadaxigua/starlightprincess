System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, Button, UIOpacity, director, v3, tween, Sprite, color, Color, Label, Vec3, quat, sGameEntity, sAudioMgr, _dec, _dec2, _dec3, _dec4, _dec5, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _temp, _crd, ccclass, property, sSlotRistGameEntityAdapter;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfsGameEntity(extras) {
    _reporterNs.report("sGameEntity", "./sGameEntity", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsAudioMgr(extras) {
    _reporterNs.report("sAudioMgr", "../../../game/core/sAudioMgr", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      Button = _cc.Button;
      UIOpacity = _cc.UIOpacity;
      director = _cc.director;
      v3 = _cc.v3;
      tween = _cc.tween;
      Sprite = _cc.Sprite;
      color = _cc.color;
      Color = _cc.Color;
      Label = _cc.Label;
      Vec3 = _cc.Vec3;
      quat = _cc.quat;
    }, function (_unresolved_2) {
      sGameEntity = _unresolved_2.sGameEntity;
    }, function (_unresolved_3) {
      sAudioMgr = _unresolved_3.default;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "e62cbZYQdFE+54xAeacIxTW", "sSlotRistGameEntityAdapter", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("sSlotRistGameEntityAdapter", sSlotRistGameEntityAdapter = (_dec = ccclass('sSlotRistGameEntityAdapter'), _dec2 = property(Label), _dec3 = property(Button), _dec4 = property(Button), _dec5 = property(Sprite), _dec(_class = (_class2 = (_temp = class sSlotRistGameEntityAdapter extends (_crd && sGameEntity === void 0 ? (_reportPossibleCrUseOfsGameEntity({
        error: Error()
      }), sGameEntity) : sGameEntity) {
        constructor() {
          super(...arguments);

          _initializerDefineProperty(this, "betDetailLabel", _descriptor, this);

          _initializerDefineProperty(this, "maxBtn", _descriptor2, this);

          _initializerDefineProperty(this, "infoBtn", _descriptor3, this);

          _initializerDefineProperty(this, "spinRender", _descriptor4, this);

          _defineProperty(this, "userCoin", 0);

          _defineProperty(this, "winTotal", 0);

          _defineProperty(this, "firstRate", 0);

          _defineProperty(this, "rotationSpeed", quat(0, 0, -0.01));

          _defineProperty(this, "rollType", 0);
        }

        //0 : normal bet  1 : auto bet
        onLoad() {
          super.onLoad(); // this.scheduleOnce(()=>{
          //     this.freeWinBingo();
          // },2);

          director.on('betBtnState', state => {
            if (this.spinRender) {
              if (state == 'disable') {
                this.spinRender.grayscale = true;
              } else if (state == 'enable') {
                if (this.rollType == 0) {
                  this.spinRender.grayscale = false;
                }
              }
            }
          }, this);
          director.on('betBtnClick', betSpeedMode => {
            (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
              error: Error()
            }), sAudioMgr) : sAudioMgr).PlayShotAudio('slots_button_spin');
            this.rotationSpeed.z = -0.13;

            if (this.spinRender) {
              tween(this.spinRender.node).to(0.1, {
                scale: v3(0.85, 0.85, 0.85)
              }, {
                easing: 'cubicOut'
              }).to(0.1, {
                scale: Vec3.ONE
              }, {
                easing: 'cubicIn'
              }).start();
            }

            if (this.rollType == 0) {} else if (this.rollType == 1) {}
          }, this);
          director.on('readyToBet', () => {
            if (this.rollType == 0) {
              this.rotationSpeed.z = -0.01;

              if (this.spinRender) {}
            }
          }, this);
          director.on('autoBetInfoUpdate', value => {
            if (value == -1) {
              this.rollType = 1;

              if (this.spinRender) {
                this.spinRender.grayscale = true;
              }
            } else if (value > 0) {
              this.rollType = 1;

              if (this.spinRender) {
                this.spinRender.grayscale = true;
              }
            } else {
              this.rollType = 0;

              if (this.spinRender && this.canBet) {
                this.spinRender.grayscale = false;
              }
            }
          }, this);
          director.on('uiTipsOpen', (mode, tex) => {
            if (mode == 'text') {
              if (globalThis.toast) {
                globalThis.toast(tex, 1);
              }
            }
          }, this);
          director.on('freeWinBetShowCoin', rate => {
            var mUserInfo = this.getUserInfo();

            if (mUserInfo) {
              this.winTotal += rate * this.betAmount;
              this.userCoin = mUserInfo.coin + rate * this.betAmount;

              if (rate > 0) {
                this.betUserInfoUpdateAnima(this.userCoin, null, this.winTotal, 0.2);
              }
            }
          }, this);
          director.on('viewChange', (type, value) => {
            if (type == 'autoSpinCancel') {
              this.rollType = 0;
              this.rotationSpeed.z = -0.01;
              var toggle = this.autoPlayBtnNode.node.getChildByName('toggle').getComponent(Sprite);
              toggle.color = color(48, 48, 48, 155);
            }
          }, this);
          this.maxBtn.node.on('click', () => {
            (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
              error: Error()
            }), sAudioMgr) : sAudioMgr).PlayShotAudio('slots_button_max_bet');

            if (this.clientConfig) {
              var betTypes = this.clientConfig.bet_types;

              if (betTypes) {
                for (var i = betTypes.length - 1; i >= 0; i--) {
                  var element = betTypes[i];

                  if (this.gameid == element.game_id) {
                    this.betAmount = element.bet_amount;
                    this.lineAmount = element.bet_multiple;
                    var mUserInfo = this.getUserInfo();

                    if (mUserInfo) {
                      director.emit('betUserInfoUpdate', mUserInfo.coin, this.betAmount * this.lineAmount, 0);
                      director.emit('betTypeChange', element.bet_type);
                      this.betLabel.node.setScale(1.5, 1.5, 1.5);
                      tween(this.betLabel.node).to(0.6, {
                        scale: Vec3.ONE
                      }, {
                        easing: 'elasticOut'
                      }).start();
                      this.updateBetInfo();
                      this.saveBetAmountValue();
                      break;
                    }
                  }
                }
              }
            }
          }, this);
          this.infoBtn.node.on('click', () => {
            (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
              error: Error()
            }), sAudioMgr) : sAudioMgr).PlayShotAudio('btnClick');
            var scene = director.getScene();

            if (scene && scene.children && scene.children.length > 0) {
              director.emit('slotRistIntro', scene.children[0]);
            }
          }, this);
        }

        turboBtnNodeClick(value) {
          if (this.turboBtnNode) {
            (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
              error: Error()
            }), sAudioMgr) : sAudioMgr).PlayShotAudio('slots_button_commonBtnClick_1');

            if (value) {
              var turboOff = this.turboBtnNode.node.getChildByName('turbo_off');
              turboOff.active = false;
              var turboOn = this.turboBtnNode.node.getChildByName('turbo_on');
              turboOn.active = true;
              var toggle = this.turboBtnNode.node.getChildByName('toggle').getComponent(Sprite);
              toggle.color = Color.WHITE;
            } else {
              var _turboOn = this.turboBtnNode.node.getChildByName('turbo_on');

              _turboOn.active = false;

              var _turboOff = this.turboBtnNode.node.getChildByName('turbo_off');

              _turboOff.active = true;

              var _toggle = this.turboBtnNode.node.getChildByName('toggle').getComponent(Sprite);

              _toggle.color = color(48, 48, 48, 155);
            }
          }
        }

        addBtnNodeStateUpdate(visible) {
          if (this.addBtnNode) {
            this.addBtnNode.getComponent(UIOpacity).opacity = visible ? 255 : 125;
          }
        }

        minusBtnNodeStateUpdate(visible) {
          if (this.minusBtnNode) {
            this.minusBtnNode.getComponent(UIOpacity).opacity = visible ? 255 : 125;
          }
        }

        autoPlayBtnNodeStateUpdate(visible) {// if(!visible){
          //     const toggle = this.autoPlayBtnNode.node.getChildByName('toggle').getComponent(Sprite);
          //     toggle.color = color(48,48,48,155);
          // }
          // if(this.autoPlayBtnNode){
          // this.autoPlayBtnNode.getComponent(UIOpacity).opacity = visible ? 255 : 125;
          // }
        }

        autoPlayBtnClick() {
          (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
            error: Error()
          }), sAudioMgr) : sAudioMgr).PlayShotAudio('slots_button_commonBtnClick_1');

          if (this.canBet) {
            if (this.betBtnState == 'normal') {
              if (this.betClickMode == 'normal') {
                this.setGameBtnState('disable');
                var toggle = this.autoPlayBtnNode.node.getChildByName('toggle').getComponent(Sprite);
                toggle.color = Color.WHITE;
                director.emit('viewChange', 'UIAutoSpin', -1);
              } else if (this.betClickMode == 'autoBet') {
                var _toggle2 = this.autoPlayBtnNode.node.getChildByName('toggle').getComponent(Sprite);

                _toggle2.color = color(48, 48, 48, 155);
                director.emit('viewChange', 'autoSpinCancel');
              }
            } else {
              if (this.betClickMode == 'autoBet') {
                var _toggle3 = this.autoPlayBtnNode.node.getChildByName('toggle').getComponent(Sprite);

                _toggle3.color = color(48, 48, 48, 155);
                director.emit('viewChange', 'autoSpinCancel');
              }
            }
          } else {
            this.betClickCoinNotEnough();
          }
        }

        menuBtnClick() {
          director.emit('subGameMenuView', true);
          var audioData = globalThis.getSubGameAudioVolume();

          if (audioData) {
            if (audioData.audioVolume == 1) {
              this.soundBtn.node.getChildByName('open').active = true;
              this.soundBtn.node.getChildByName('close').active = false;
            } else {
              this.soundBtn.node.getChildByName('close').active = true;
              this.soundBtn.node.getChildByName('open').active = false;
            }
          }

          this.menuBtns.node.active = true;
        }

        closeBtnClick() {
          director.emit('subGameMenuView', false);
          this.menuBtns.node.active = false;
        }

        soundBtnClick() {
          if (this.audioTouchEnable) {
            this.audioTouchEnable = false;
            var audioData = globalThis.getSubGameAudioVolume();

            if (audioData) {
              if (audioData.audioVolume == 1) {
                globalThis.subGameAudioVolumeCtr(false);
                this.soundBtn.node.getChildByName('close').active = true;
                this.soundBtn.node.getChildByName('open').active = false;
              } else {
                globalThis.subGameAudioVolumeCtr(true);
                this.soundBtn.node.getChildByName('open').active = true;
                this.soundBtn.node.getChildByName('close').active = false;
              }
            }

            this.scheduleOnce(() => {
              this.audioTouchEnable = true;
            }, 1);
          }
        }

        soundTip(value) {// this.betBtns.node.getChildByPath('menuBtn/sound_off').active = value;
        }

        updateBetInfo() {
          super.updateBetInfo();
          this.betDetailLabel.string = this.AddCommas(this.betAmount) + "X" + this.lineAmount;
        }

        addBtnClick() {
          (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
            error: Error()
          }), sAudioMgr) : sAudioMgr).PlayShotAudio('slots_button_add_bet');
          super.addBtnClick();
        }

        minusBtnClick() {
          (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
            error: Error()
          }), sAudioMgr) : sAudioMgr).PlayShotAudio('slots_button_limit_bet');
          super.minusBtnClick();
        }

        spinBtnClick() {// sAudioMgr.PlayShotAudio('slots_button_spin');
        }

        freeWinBingo() {
          this.setGameBtnState('disable');
          this.infoBtn.interactable = false;
          this.infoBtn.getComponent(UIOpacity).opacity = 125;
          this.maxBtn.interactable = false;
          this.maxBtn.getComponent(UIOpacity).opacity = 125;
          this.gameMode = 'freeWin';

          if (this.viewMode == 'record') {
            if (this.recordBtns) {
              this.recordBtns.node.active = false;
            }
          }
        }

        freeWinOver() {
          this.winTotal = 0;
          this.setGameBtnState('enable');
          this.infoBtn.interactable = true;
          this.infoBtn.getComponent(UIOpacity).opacity = 255;
          this.maxBtn.interactable = true;
          this.maxBtn.getComponent(UIOpacity).opacity = 255;

          if (this.viewMode == 'record') {
            this.recordBtnTouchEnable = false;
            this.scheduleOnce(() => {
              this.recordBtnTouchEnable = true;
            }, 3);
          }

          this.recordState = 'idle';
          this.gameMode = 'normal';
        }

        freeWinBegain() {
          if (this.viewMode == 'record') {
            if (this.recordBtns) {
              this.recordBtns.node.active = true;
            }
          }

          this.userCoin = this.beforeUserCoin;
          this.winTotal = this.firstRate > 0 ? this.firstRate * this.betAmount : 0;
        }

        freeWinBtnInfo(rate) {
          if (rate) {
            this.firstRate = rate;
          } else {
            this.firstRate = 0;
          }
        }

        setGameBtnState(state) {
          super.setGameBtnState(state);

          if (state == 'enable') {
            this.maxBtn.getComponent(UIOpacity).opacity = 255;
            this.maxBtn.interactable = true;
          } else if (state == 'disable') {
            this.maxBtn.getComponent(UIOpacity).opacity = 125;
            this.maxBtn.interactable = false;
          }
        }

        errToOneRes() {
          var toggle = this.autoPlayBtnNode.node.getChildByName('toggle').getComponent(Sprite);
          toggle.color = color(48, 48, 48, 155);
          super.errToOneRes();
        }

        update(deltaTime) {
          if (this.spinRender) {
            this.spinRender.node.rotate(this.rotationSpeed);
          }
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "betDetailLabel", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "maxBtn", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "infoBtn", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "spinRender", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=sSlotRistGameEntityAdapter.js.map
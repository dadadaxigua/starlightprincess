System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, Animation, UIOpacity, v3, director, tween, sGameEntity, UIFreeWinBetInfo, _dec, _class, _crd, ccclass, property, sSlotPGGameEntityAdapter;

  function _reportPossibleCrUseOfsGameEntity(extras) {
    _reporterNs.report("sGameEntity", "./sGameEntity", _context.meta, extras);
  }

  function _reportPossibleCrUseOfUIFreeWinBetInfo(extras) {
    _reporterNs.report("UIFreeWinBetInfo", "./UIFreeWinBetInfo", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      Animation = _cc.Animation;
      UIOpacity = _cc.UIOpacity;
      v3 = _cc.v3;
      director = _cc.director;
      tween = _cc.tween;
    }, function (_unresolved_2) {
      sGameEntity = _unresolved_2.sGameEntity;
    }, function (_unresolved_3) {
      UIFreeWinBetInfo = _unresolved_3.UIFreeWinBetInfo;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "b8441l6lyZBLafA+XTH46P9", "sSlotPGGameEntityAdapter", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("sSlotPGGameEntityAdapter", sSlotPGGameEntityAdapter = (_dec = ccclass('sSlotPGGameEntityAdapter'), _dec(_class = class sSlotPGGameEntityAdapter extends (_crd && sGameEntity === void 0 ? (_reportPossibleCrUseOfsGameEntity({
        error: Error()
      }), sGameEntity) : sGameEntity) {
        start() {
          super.start();
          var country = '';

          if (globalThis.getCountry) {
            var con = globalThis.getCountry();

            if (con) {
              country = con;
            }
          }

          if (country == 'VNM') {
            if (this.selectBetBtn) {
              var vnIcon = this.selectBetBtn.node.getChildByName('vnIcon');

              if (vnIcon) {
                vnIcon.active = true;
              }
            }
          } else {
            if (this.selectBetBtn) {
              var icon = this.selectBetBtn.node.getChildByName('icon');

              if (icon) {
                icon.active = true;
              }
            }
          } // const audioData = globalThis.getSubGameAudioVolume();
          // if (audioData) {
          //     if (audioData.audioVolume == 1) {
          //         this.betBtns.node.getChildByPath('menuBtn/sound_off').active = true;
          //     } else {
          //         this.betBtns.node.getChildByPath('menuBtn/sound_off').active = false;
          //     }
          // }


          this.loadFreeBuyView();
        }

        turboBtnNodeClick(value) {
          if (this.turboBtnNode) {
            if (value) {
              var turboOff = this.turboBtnNode.node.getChildByName('turbo_off');
              turboOff.active = false;
              var turboOn = this.turboBtnNode.node.getChildByName('turbo_on');
              turboOn.active = true;
              turboOn.getChildByName('turbo').active = true;
              turboOn.getChildByName('turbo').getComponent(Animation).play();
            } else {
              var _turboOn = this.turboBtnNode.node.getChildByName('turbo_on');

              _turboOn.getChildByName('turbo').getComponent(Animation).stop();

              _turboOn.getChildByName('turbo').active = false;
              _turboOn.active = false;

              var _turboOff = this.turboBtnNode.node.getChildByName('turbo_off');

              _turboOff.active = true;
            }
          }
        }

        addBtnNodeStateUpdate(visible) {
          if (this.addBtnNode) {
            this.changeRenderAlpha(this.addBtnNode.node, visible ? 255 : 125); // this.addBtnNode.getComponent(UIOpacity).opacity = visible ? 255 : 125;
          }
        }

        minusBtnNodeStateUpdate(visible) {
          if (this.minusBtnNode) {
            this.changeRenderAlpha(this.minusBtnNode.node, visible ? 255 : 125); // this.minusBtnNode.getComponent(UIOpacity).opacity = visible ? 255 : 125;
          }
        }

        autoPlayBtnNodeStateUpdate(visible) {
          if (this.autoPlayBtnNode) {
            this.autoPlayBtnNode.getComponent(UIOpacity).opacity = visible ? 255 : 125;
          }
        }

        autoPlayBtnClick() {
          if (this.betBtnState == 'normal' && this.autoBetCount == -2) {
            console.log('this.autoBetCount:', this.autoBetCount);
            this.setGameBtnState('disable');
            this.autoSpin.node.active = true;
            this.autoSpin.appearViewAnima();
            var mUserInfo = globalThis.getUserInfo();

            if (mUserInfo) {
              this.autoSpin.betInfoSet(mUserInfo.coin, this.betAmount * this.lineAmount, 0);
            }
          }
        }

        menuBtnClick() {
          var animaTime = 0.15;

          if (this.touchEnable) {
            this.touchEnable = false;

            if (this.betBtns.node.active) {
              this.cleanTweenList();
              this.betBtnNode.node.active = false;
              director.emit('subGameMenuView', true);
              this.betBtns.node.position = v3(0, 70, 0);
              this.betBtns.opacity = 255;
              this.pushOneTween(tween(this.betBtns).to(animaTime, {
                opacity: 0
              }, {
                easing: 'sineOut'
              }).start());
              this.pushOneTween(tween(this.betBtns.node).to(animaTime, {
                position: v3(0, -330, 0)
              }, {
                easing: 'sineOut'
              }).call(() => {
                this.betBtns.node.active = false;
                this.touchEnable = true;
              }).start());
              this.menuBtns.node.active = true;
              this.menuBtns.node.position = v3(0, -330, 0);
              this.menuBtns.opacity = 0;
              this.menuState = 'menu';
              this.pushOneTween(tween(this.menuBtns).to(animaTime, {
                opacity: 255
              }, {
                easing: 'sineOut'
              }).start());
              this.pushOneTween(tween(this.menuBtns.node).to(animaTime, {
                position: v3(0, 70, 0)
              }, {
                easing: 'sineOut'
              }).start());
            }

            var audioData = globalThis.getSubGameAudioVolume();

            if (audioData) {
              if (audioData.audioVolume == 1) {
                this.menuBtns.node.getChildByPath('btns/sound/open').active = true;
                this.menuBtns.node.getChildByPath('btns/sound/close').active = false;
              } else {
                this.menuBtns.node.getChildByPath('btns/sound/close').active = true;
                this.menuBtns.node.getChildByPath('btns/sound/open').active = false;
              }
            }
          }
        }

        closeBtnClick() {
          var animaTime = 0.15;

          if (this.touchEnable) {
            this.touchEnable = false;

            if (!this.betBtns.node.active) {
              this.cleanTweenList();
              this.betBtnNode.node.active = true;
              director.emit('subGameMenuView', false); // director.emit('subGameReplayingView',false);

              this.menuBtns.node.position = v3(0, 70, 0);
              this.menuBtns.opacity = 255;
              this.pushOneTween(tween(this.menuBtns).to(animaTime, {
                opacity: 0
              }, {
                easing: 'sineOut'
              }).start());
              this.pushOneTween(tween(this.menuBtns.node).to(animaTime, {
                position: v3(0, -330, 0)
              }, {
                easing: 'sineOut'
              }).call(() => {
                this.menuBtns.node.active = false;
                this.touchEnable = true;
              }).start());
              this.betBtns.node.active = true;
              this.betBtns.node.position = v3(0, -330, 0);
              this.betBtns.opacity = 0;
              this.menuState = 'bet';
              this.pushOneTween(tween(this.betBtns).to(animaTime, {
                opacity: 255
              }, {
                easing: 'sineOut'
              }).start());
              this.pushOneTween(tween(this.betBtns.node).to(animaTime, {
                position: v3(0, 70, 0)
              }, {
                easing: 'sineOut'
              }).start());
            }
          }
        }

        soundBtnClick() {
          if (this.audioTouchEnable) {
            this.audioTouchEnable = false;
            var audioData = globalThis.getSubGameAudioVolume();

            if (audioData) {
              if (audioData.audioVolume == 1) {
                globalThis.subGameAudioVolumeCtr(false);
                this.menuBtns.node.getChildByPath('btns/sound/close').active = true;
                this.menuBtns.node.getChildByPath('btns/sound/open').active = false; // this.betBtns.node.getChildByPath('menuBtn/sound_off').active = true;
              } else {
                globalThis.subGameAudioVolumeCtr(true);
                this.menuBtns.node.getChildByPath('btns/sound/open').active = true;
                this.menuBtns.node.getChildByPath('btns/sound/close').active = false; // this.betBtns.node.getChildByPath('menuBtn/sound_off').active = true;
              }
            }

            this.scheduleOnce(() => {
              this.audioTouchEnable = true;
            }, 1);
          }
        }

        soundTip(value) {// this.betBtns.node.getChildByPath('menuBtn/sound_off').active = value;
        }

        addBtnClick() {
          globalThis.subGamePlayShotAudio('subGameClick');
          super.addBtnClick();
        }

        minusBtnClick() {
          globalThis.subGamePlayShotAudio('subGameClick');
          super.minusBtnClick();
        }

        spinBtnClick() {}

        freeWinBingo() {
          this.node.active = false;
          this.gameMode = 'freeWin';

          if (this.viewMode == 'record') {
            if (this.recordBtns) {
              this.recordBtns.node.active = false;
            }
          }
        }

        freeWinOver() {
          if (this.viewMode == 'record') {
            this.recordBtnTouchEnable = false;
            this.scheduleOnce(() => {
              this.recordBtnTouchEnable = true;
            }, 3);
          }

          this.node.active = true;
          this.recordState = 'idle';
          this.freeWinBtn.active = false;
          this.gameMode = 'normal';
        }

        freeWinBegain() {
          this.freeWinBtn.active = true;

          if (this.viewMode == 'record') {
            if (this.recordBtns) {
              this.recordBtns.node.active = true;
            }
          }
        }

        freeWinBtnInfo(rate) {
          var _rate = 0;

          if (rate) {
            _rate = rate;
          }

          if (this.freeWinBtn) {
            this.freeWinBtn.getComponent(_crd && UIFreeWinBetInfo === void 0 ? (_reportPossibleCrUseOfUIFreeWinBetInfo({
              error: Error()
            }), UIFreeWinBetInfo) : UIFreeWinBetInfo).updateRate(_rate);
          }
        }

      }) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=sSlotPGGameEntityAdapter.js.map
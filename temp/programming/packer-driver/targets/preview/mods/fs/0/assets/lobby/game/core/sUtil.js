System.register(["cc"], function (_export, _context) {
  "use strict";

  var _cclegacy, assetManager, Color, director, game, instantiate, math, Node, Prefab, sys, tween, v3, Vec3, sUtil, _crd;

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  _export("sUtil", void 0);

  return {
    setters: [function (_cc) {
      _cclegacy = _cc.cclegacy;
      assetManager = _cc.assetManager;
      Color = _cc.Color;
      director = _cc.director;
      game = _cc.game;
      instantiate = _cc.instantiate;
      math = _cc.math;
      Node = _cc.Node;
      Prefab = _cc.Prefab;
      sys = _cc.sys;
      tween = _cc.tween;
      v3 = _cc.v3;
      Vec3 = _cc.Vec3;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "69a6e8RwHhI4IcpsfHu3xSE", "sUtil", undefined);

      _export("sUtil", sUtil = class sUtil {
        static get DirectorMsgObj() {
          if (!sUtil.directorMsgObj) {
            sUtil.directorMsgObj = new Node();
            var scene = director.getScene();

            if (scene && scene.children && scene.children.length > 0) {
              scene.children[0].addChild(sUtil.directorMsgObj);
            }
          }

          return sUtil.directorMsgObj;
        }

        static once(msgName, call) {
          director.once(msgName, call, sUtil.DirectorMsgObj);
        }

        static onceByNode(msgName, call, _node) {
          sUtil.MsgObjs[_node.uuid] = _node;
          director.once(msgName, call, _node);
        }

        static off(msgName, call, _node) {
          director.off(msgName, call, _node);
        }

        static AddCommas(money, demc) {
          if (demc === void 0) {
            demc = 0;
          }

          if (!money) {
            return "0";
          } else {
            var mon;

            if (typeof money == "string") {
              mon = Number(money);
            } else {
              mon = money;
            }

            var coinRate = 1,
                byte = 1,
                rate = 1;

            if (globalThis.getCoinRate && globalThis.getCoinRate() > 0) {
              coinRate = globalThis.getCoinRate();
            }

            if (coinRate >= 10) {
              byte = coinRate.toString().length - 1;
              rate = 100;
            }

            var showMoney = mon; // demc = 0;

            if (byte == 1) {
              if (demc == 0) {
                showMoney = mon;
              } else if (demc == -1) {
                showMoney = Math.trunc(mon);
              }
            } else {
              if (demc == 0) {
                showMoney = mon / coinRate;
              } else if (demc > 0) {
                showMoney = mon / coinRate; // showMoney = (Math.floor(showMoney * Math.pow(10,demc)) / Math.pow(10,demc));

                showMoney = showMoney.toFixed(demc);
              } else if (demc == -1) {
                showMoney /= coinRate;
                showMoney = Math.trunc(showMoney);
              }
            } // console.log('showMoney:'+showMoney);


            var tempmoney = String(showMoney);
            var left = tempmoney.split('.')[0],
                right = tempmoney.split('.')[1];
            right = right ? right.length >= byte ? '.' + right.substring(0, byte) : '.' + right : '';
            var temp = left.split('').reverse().join('').match(/(\d{1,3})/g);

            if (temp) {
              return (Number(tempmoney) < 0 ? "-" : "") + temp.join(',').split('').reverse().join('') + right;
            } else {
              return '';
            }
          }
        }

        static AddCommasNoRate(money, demc) {
          if (demc === void 0) {
            demc = 0;
          }

          if (!money) {
            return "0";
          } else {
            var mon;

            if (typeof money == "string") {
              mon = Number(money);
            } else {
              mon = money;
            }

            var coinRate = 1,
                byte = 1,
                rate = 1;
            var showMoney = mon; // demc = 0;

            if (byte == 1) {
              if (demc == 0) {
                showMoney = mon;
              } else if (demc == -1) {
                showMoney = Math.trunc(mon);
              }
            } else {
              showMoney = mon / coinRate * rate;

              if (demc == 0) {
                showMoney = showMoney;
                showMoney /= rate;
              } else if (demc > 0) {
                showMoney /= rate;
                showMoney = (Math.trunc(showMoney * Math.pow(10, demc)) / Math.pow(10, demc)).toFixed(demc);
              }
            } // console.log('showMoney:'+showMoney);


            var tempmoney = String(showMoney);
            var left = tempmoney.split('.')[0],
                right = tempmoney.split('.')[1];
            right = right ? right.length >= byte ? '.' + right.substring(0, byte) : '.' + right : '';
            var temp = left.split('').reverse().join('').match(/(\d{1,3})/g);

            if (temp) {
              return (Number(tempmoney) < 0 ? "-" : "") + temp.join(',').split('').reverse().join('') + right;
            } else {
              return '';
            }
          }
        }

        static RandomInt(min, max) {
          return Math.trunc(Math.random() * (max - min)) + min;
        }

        static Random(min, max) {
          return Math.random() * (max - min) + min;
        }

        static bezierTo(target, duration, delayNum, startCall, c1, c2, to, opts) {
          var temp = v3(0, 0, 0);
          opts = opts || Object.create(null);
          /**
           * @desc 二阶贝塞尔
           * @param {number} t 当前百分比
           * @param {} p1 起点坐标
           * @param {} cp 控制点
           * @param {} p2 终点坐标
           * @returns {any}
           */

          var twoBezier = (t, p1, cp, p2) => {
            var x = (1 - t) * (1 - t) * p1.x + 2 * t * (1 - t) * cp.x + t * t * p2.x;
            var y = (1 - t) * (1 - t) * p1.y + 2 * t * (1 - t) * cp.y + t * t * p2.y;
            return v3(x, y, 0);
          };

          opts.progress = (start, end, current, ratio) => {
            target.worldPosition = twoBezier(ratio, c1, c2, to);
            return ratio;
          }; // opts.onUpdate = (arg: Vec3, ratio: number) => {
          //     target.worldPosition = twoBezier(ratio, c1, c2, to);
          // };


          return tween(temp).delay(delayNum).call(startCall).to(duration, Vec3.ONE, opts);
        }

        static bezierLocalTo(target, duration, delayNum, startCall, c1, c2, to, opts) {
          var temp = v3(0, 0, 0);
          opts = opts || Object.create(null);
          /**
           * @desc 二阶贝塞尔
           * @param {number} t 当前百分比
           * @param {} p1 起点坐标
           * @param {} cp 控制点
           * @param {} p2 终点坐标
           * @returns {any}
           */

          var twoBezier = (t, p1, cp, p2) => {
            var x = (1 - t) * (1 - t) * p1.x + 2 * t * (1 - t) * cp.x + t * t * p2.x;
            var y = (1 - t) * (1 - t) * p1.y + 2 * t * (1 - t) * cp.y + t * t * p2.y;
            return v3(x, y, 0);
          };

          opts.progress = (start, end, current, ratio) => {
            target.position = twoBezier(ratio, c1, c2, to);
            return ratio;
          }; // opts.onUpdate = (arg: Vec3, ratio: number) => {
          //     target.position = twoBezier(ratio, c1, c2, to);
          // };


          return tween(temp).delay(delayNum).call(startCall).to(duration, Vec3.ONE, opts);
        }

        static GetAngleByPos(target1, target2) {
          var dir = v3(target1.x - target2.x, target1.y - target2.y, 0);
          var angle = math.toDegree(Math.atan2(dir.y, dir.x));
          return angle;
        }

        static GetPosByAngle(r) {
          return v3(Math.cos(math.toRadian(r)), Math.sin(math.toRadian(r)), 0);
        }

        static changeNumberToKW(number, num) {
          if (num === void 0) {
            num = 0;
          }

          var temp = 0,
              res = 0;

          if (typeof number == "string") {
            res = Number(number);
            temp = Math.abs(res);
          } else {
            res = number;
            temp = Math.abs(res);
          }

          if (globalThis.getCoinRate && globalThis.getCoinRate() > 0) {
            temp = temp / globalThis.getCoinRate();
          }

          var str = "";

          if (temp >= 1000000000) {
            str = num == 0 ? Math.trunc(temp / 1000000000) + "B" : Math.trunc(temp / (1000000000 / Math.pow(10, num))) / Math.pow(10, num) + "B";
          } else if (temp >= 1000000) {
            str = num == 0 ? Math.trunc(temp / 1000000) + "M" : Math.trunc(temp / (1000000 / Math.pow(10, num))) / Math.pow(10, num) + "M";
          } else if (temp >= 1000) {
            str = num == 0 ? Math.trunc(temp / 1000) + "K" : Math.trunc(temp / 1000 * Math.pow(10, num)) / Math.pow(10, num) + "K";
          } else {
            str = num == 0 ? Math.trunc(temp).toString() : (Math.trunc(temp * Math.pow(10, num)) / Math.pow(10, num)).toString();
          }

          if (res < 0) {
            str = '-' + str;
          }

          return str;
        }

        static changeNumberToKWNoRate(number, num) {
          if (num === void 0) {
            num = 0;
          }

          var temp = 0,
              res = 0;

          if (typeof number == "string") {
            res = Number(number);
            temp = Math.abs(res);
          } else {
            res = number;
            temp = Math.abs(res);
          }

          var str = "";

          if (temp >= 1000000000) {
            str = num == 0 ? Math.trunc(temp / 1000000000) + "B" : Math.trunc(temp / (1000000000 / Math.pow(10, num))) / Math.pow(10, num) + "B";
          } else if (temp >= 1000000) {
            str = num == 0 ? Math.trunc(temp / 1000000) + "M" : Math.trunc(temp / (1000000 / Math.pow(10, num))) / Math.pow(10, num) + "M";
          } else if (temp >= 1000) {
            str = num == 0 ? Math.trunc(temp / 1000) + "K" : Math.trunc(temp / 1000 * Math.pow(10, num)) / Math.pow(10, num) + "K";
          } else {
            str = num == 0 ? Math.trunc(temp).toString() : (Math.trunc(temp * Math.pow(10, num)) / Math.pow(10, num)).toString();
          }

          if (res < 0) {
            str = '-' + str;
          }

          return str;
        }

        static QuatValueSet(from, to) {
          to.x = from.x;
          to.y = from.y;
          to.z = from.z;
          to.w = from.w;
        }

        static Vec3ValueSet(from, to) {
          to.x = from.x;
          to.y = from.y;
          to.z = from.z;
        }

        static TweenColor(_color, _target, _time, _delay, finishCall) {
          if (_time === void 0) {
            _time = 1;
          }

          if (_delay === void 0) {
            _delay = 0;
          }

          if (finishCall === void 0) {
            finishCall = null;
          }

          if (_target && _target.node && _target.node.isValid) {
            var c = new Color(_target.color);
            return tween(c).delay(_delay).to(_time, {
              r: _color.r,
              g: _color.g,
              b: _color.b,
              a: _color.a
            }, {
              "onUpdate": function onUpdate(target) {
                if (_target && _target.node && _target.node.isValid) {
                  _target.color = target;
                }
              }
            }).call(finishCall).start();
          } else {
            console.error('target is null');
          }

          return null;
        }

        static TweenColorFrame(_oriColor, _targetColor, _time, _delay, finishCall, frameCall) {
          if (_time === void 0) {
            _time = 1;
          }

          if (_delay === void 0) {
            _delay = 0;
          }

          if (finishCall === void 0) {
            finishCall = null;
          }

          var c = new Color(_oriColor);
          return tween(c).delay(_delay).to(_time, {
            r: _targetColor.r,
            g: _targetColor.g,
            b: _targetColor.b,
            a: _targetColor.a
          }, {
            "onUpdate": function onUpdate(target) {
              if (frameCall) {
                frameCall(target);
              }
            }
          }).call(finishCall).start();
        }

        static TweenLabel(startValue, value, targetLabel, _time, needKWNum, ease, delayTime, finishCall, isInt) {
          if (needKWNum === void 0) {
            needKWNum = 0;
          }

          if (ease === void 0) {
            ease = 'quadOut';
          }

          if (delayTime === void 0) {
            delayTime = 0;
          }

          if (finishCall === void 0) {
            finishCall = null;
          }

          if (isInt === void 0) {
            isInt = false;
          }

          var tweenTargetVec3 = v3(startValue, startValue, startValue);
          var demc = -1;

          if (globalThis.getCoinRate && globalThis.getCoinRate() > 1) {
            demc = 2;
          }

          return tween(tweenTargetVec3).delay(delayTime).to(_time, v3(value, value, value), {
            "onUpdate": target => {
              if (targetLabel) {
                targetLabel.string = isInt ? Math.trunc(target.x).toString() : needKWNum > 0 ? sUtil.changeNumberToKW(target.x, needKWNum) : sUtil.AddCommas(target.x, demc).toString();
              }
            },
            easing: ease
          }).call(() => {
            if (targetLabel) {
              targetLabel.string = isInt ? Math.trunc(value).toString() : needKWNum > 0 ? sUtil.changeNumberToKW(value, needKWNum) : sUtil.AddCommas(value, demc).toString();
            }

            if (finishCall) {
              finishCall();
            }
          }).start();
        }

        static Vec3Distance(a, b) {
          if (a && b) {
            return (a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y);
          }
        }

        static SkeletonPlay(ske, playCount, animaName, completeCall, loop) {
          if (loop === void 0) {
            loop = false;
          }

          if (ske) {
            if (loop) {
              ske.setAnimation(0, animaName, true);
            } else {
              var count = 0;
              ske.setCompleteListener(() => {
                count++;

                if (count >= playCount) {
                  if (completeCall) {
                    completeCall();
                  }
                } else {
                  if (ske) {
                    ske.setAnimation(0, animaName, false);
                  }
                }
              });
              ske.setAnimation(0, animaName, false);
            }
          }
        }

        static playerBetLimit(gameid) {
          if (globalThis.checkGameIsLimit && gameid) {
            var res = globalThis.checkGameIsLimit(gameid);

            if (res) {
              if (res.limit) {
                if (res.tip && globalThis.toast) {
                  globalThis.toast(res.tip);
                }

                return true;
              }
            }
          }

          return false;
        }

        static toast(tex) {
          if (globalThis.toast && tex) {
            globalThis.toast(tex, 1);
          }
        }

        static Clear() {
          if (sUtil.directorMsgObj) {
            director.removeAll(sUtil.directorMsgObj); // sUtil.directorMssgObj.destroy();

            sUtil.directorMsgObj = null;
          }

          if (sUtil.MsgObjs) {
            var keys = Object.keys(sUtil.MsgObjs);

            for (var i = 0; i < keys.length; i++) {
              var element = keys[i];
              var _node = sUtil.MsgObjs[element];
              director.removeAll(_node);
            }
          }
        }

        static GamePerforAnalysis(_actionName, _param) {
          if (_param === void 0) {
            _param = '';
          }

          try {
            if (globalThis.infoCollect) {
              var _serverTime = globalThis.getServerTime();

              var _uid = 0;
              var mUserInfo = globalThis.getUserInfo();

              if (mUserInfo) {
                _uid = mUserInfo.uid;
              }

              globalThis.infoCollect("GamePerforAnalysis", _uid, {
                gameid: globalThis.nowInGame,
                uid: _uid,
                frameRate: 1 / game.deltaTime,
                os: sys.os,
                netType: sys.getNetworkType(),
                serverTime: _serverTime,
                actionName: _actionName,
                param: _param
              });
            }
          } catch (error) {
            console.error(error);
          }
        }

        static webInfoCollect(_name) {
          if (globalThis.infoCollect) {
            var _uid = 0;
            var mUserInfo = globalThis.getUserInfo();

            if (mUserInfo) {
              _uid = mUserInfo.uid;
            }

            globalThis.infoCollect(_name, _uid, {
              frameRate: 1 / game.deltaTime,
              gameid: globalThis.nowInGame
            });
          }
        }

        static getSubGameBtnByName(subGameBtnPrefab, gameid, belongName, aligmentType, aligmentPixel, gameVersion, customColor, cb) {
          if (cb) {
            var obj = instantiate(subGameBtnPrefab);

            if (obj) {
              obj['gameid'] = gameid;
              obj['aligmentType'] = aligmentType;
              obj['aligmentPixel'] = aligmentPixel;
              obj['gameVersion'] = gameVersion;
              obj['customColor'] = customColor;

              if (cb) {
                cb(obj);
              }
            }
          }
        }

        static getSubGameBtnByPath(subGameBtnPath, gameid, aligmentType, aligmentPixel, gameVersion, customColor, cb) {
          if (cb && gameid && subGameBtnPath) {
            var bundle = assetManager.getBundle(gameid.toString());

            if (bundle) {
              bundle.load(subGameBtnPath, Prefab, (err, _prefab) => {
                if (_prefab) {
                  var obj = instantiate(_prefab);

                  if (obj) {
                    obj['gameid'] = gameid;
                    obj['aligmentType'] = aligmentType;
                    obj['aligmentPixel'] = aligmentPixel;
                    obj['gameVersion'] = gameVersion;
                    obj['customColor'] = customColor;

                    if (cb) {
                      cb(obj);
                    }
                  }
                }
              });
            } else {
              cb(null);
            }
          }
        }

        static getAssetInSubGame(bundleID, path, type, onComplete) {
          var bundle = assetManager.getBundle(bundleID);

          if (bundle) {
            bundle.load(path, type, onComplete);
          } else {
            if (onComplete) {
              onComplete(null, null);
            }
          }
        }

        static getLobbyAssetInSubGame(path, type, onComplete) {
          var bundle = assetManager.getBundle('lobby');

          if (bundle) {
            bundle.load('hyPrefabs/gameRes/external/' + path, type, onComplete);
          } else {
            if (onComplete) {
              onComplete(null, null);
            }
          }
        }

        static CopyArray(arr) {
          if (arr && Array.isArray(arr)) {
            var resArr = [];

            for (var i = 0; i < arr.length; i++) {
              var resArr1 = [];
              resArr.push(resArr1);

              for (var j = 0; j < arr[i].length; j++) {
                if (Array.isArray(arr[i][j])) {
                  var resArr2 = [];
                  resArr1.push(resArr2);

                  for (var z = 0; z < arr[i][j].length; z++) {
                    resArr2.push(arr[i][j][z]);
                  }
                } else {
                  resArr1.push(arr[i][j]);
                }
              }
            }

            return resArr;
          }
        }

        static IsEditor() {
          if (globalThis.webLanguage || sys.isMobile) {
            return false;
          }

          return true;
        }

      });

      _defineProperty(sUtil, "directorMsgObj", void 0);

      _defineProperty(sUtil, "MsgObjs", {});

      globalThis.GamePerforAnalysis = sUtil.GamePerforAnalysis;

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=sUtil.js.map
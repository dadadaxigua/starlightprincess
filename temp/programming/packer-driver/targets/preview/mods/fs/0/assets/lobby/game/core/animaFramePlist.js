System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, SpriteAtlas, assetMgr, sComponent, _dec, _dec2, _class, _class2, _descriptor, _temp, _crd, ccclass, property, animaFramePlist;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfassetMgr(extras) {
    _reporterNs.report("assetMgr", "./sAssetMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsComponent(extras) {
    _reporterNs.report("sComponent", "./sComponent", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      SpriteAtlas = _cc.SpriteAtlas;
    }, function (_unresolved_2) {
      assetMgr = _unresolved_2.assetMgr;
    }, function (_unresolved_3) {
      sComponent = _unresolved_3.sComponent;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "b80e0zITaJEvoQAQNYfpdGD", "animaFramePlist", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("animaFramePlist", animaFramePlist = (_dec = ccclass('animaFramePlist'), _dec2 = property([SpriteAtlas]), _dec(_class = (_class2 = (_temp = class animaFramePlist extends (_crd && sComponent === void 0 ? (_reportPossibleCrUseOfsComponent({
        error: Error()
      }), sComponent) : sComponent) {
        constructor() {
          super(...arguments);

          _initializerDefineProperty(this, "plistAtlas", _descriptor, this);
        }

        start() {
          (_crd && assetMgr === void 0 ? (_reportPossibleCrUseOfassetMgr({
            error: Error()
          }), assetMgr) : assetMgr).SetSortSpriteFramesArrayToDic(this.plistAtlas);
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "plistAtlas", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return [];
        }
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=animaFramePlist.js.map
System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2", "__unresolved_3", "__unresolved_4", "__unresolved_5"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, Color, director, Label, Node, Sprite, SpriteFrame, tween, UIOpacity, UITransform, v3, Vec3, sComponent, sObjPool, sUtil, sAudioMgr, assetMgr, _dec, _dec2, _dec3, _dec4, _dec5, _dec6, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _descriptor5, _temp, _crd, ccclass, property, StarlightPrincess_Board;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfsComponent(extras) {
    _reporterNs.report("sComponent", "../lobby/game/core/sComponent", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsObjPool(extras) {
    _reporterNs.report("sObjPool", "../lobby/game/core/sObjPool", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsUtil(extras) {
    _reporterNs.report("sUtil", "../lobby/game/core/sUtil", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsAudioMgr(extras) {
    _reporterNs.report("sAudioMgr", "../lobby/game/core/sAudioMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfassetMgr(extras) {
    _reporterNs.report("assetMgr", "../lobby/game/core/sAssetMgr", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      Color = _cc.Color;
      director = _cc.director;
      Label = _cc.Label;
      Node = _cc.Node;
      Sprite = _cc.Sprite;
      SpriteFrame = _cc.SpriteFrame;
      tween = _cc.tween;
      UIOpacity = _cc.UIOpacity;
      UITransform = _cc.UITransform;
      v3 = _cc.v3;
      Vec3 = _cc.Vec3;
    }, function (_unresolved_2) {
      sComponent = _unresolved_2.sComponent;
    }, function (_unresolved_3) {
      sObjPool = _unresolved_3.sObjPool;
    }, function (_unresolved_4) {
      sUtil = _unresolved_4.sUtil;
    }, function (_unresolved_5) {
      sAudioMgr = _unresolved_5.default;
    }, function (_unresolved_6) {
      assetMgr = _unresolved_6.assetMgr;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "5d16emtYXZMXbgD011qoznr", "StarlightPrincess_Board", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("StarlightPrincess_Board", StarlightPrincess_Board = (_dec = ccclass('StarlightPrincess_Board'), _dec2 = property(UIOpacity), _dec3 = property(UIOpacity), _dec4 = property(Label), _dec5 = property(Node), _dec6 = property([SpriteFrame]), _dec(_class = (_class2 = (_temp = class StarlightPrincess_Board extends (_crd && sComponent === void 0 ? (_reportPossibleCrUseOfsComponent({
        error: Error()
      }), sComponent) : sComponent) {
        constructor() {
          super(...arguments);

          _initializerDefineProperty(this, "normalInfoNode", _descriptor, this);

          _initializerDefineProperty(this, "tableOpa", _descriptor2, this);

          _initializerDefineProperty(this, "baseLabel", _descriptor3, this);

          _initializerDefineProperty(this, "multiNode", _descriptor4, this);

          _initializerDefineProperty(this, "boradSprites", _descriptor5, this);

          _defineProperty(this, "index", 0);

          _defineProperty(this, "mode", 'idle');

          _defineProperty(this, "targetImg", null);

          _defineProperty(this, "totalRate", 0);

          _defineProperty(this, "coinLabelValue", 0);

          _defineProperty(this, "oneRoundMultiValue", 0);
        }

        start() {
          this.BoardAction(); // this.scheduleOnce(()=>{
          //     director.emit('winBetRes',1000);
          //     // this.scheduleOnce(()=>{
          //     //     // director.emit('winBetRes',60);
          //     //     this.multiLabelAnima(v3(0,0,0),10);
          //     // },2);
          // },2);

          director.on('winMultiBox', data => {
            if (data) {
              this.multiLabelAnima(data.actionType, data.pos, data.multi, data.fontSize);
            }

            ;
          }, this);
          director.on('winBetRes', rate => {
            if (rate > 0) {
              // console.log('winBetRes:',rate);
              this.totalRate += rate;
              this.showWinCoin(this.totalRate * globalThis.GameBtnEntity.CurrentBetAmount); // this.showWinCoin(this.totalRate * 10);
            } else if (rate == -1) {
              director.emit('titleWinSettleEnd');
            } else if (rate == -2) {}

            ; // this.showWinCoin(30);
          }, this);
          director.on('slotSubGameRollBegin', () => {
            this.totalRate = 0;
            this.coinLabelValue = 0;
            this.oneRoundMultiValue = 0;

            if (this.mode != 'idle') {
              this.cleanTweenDic();
              this.removeIDTween('imgSprite');
              this.removeIDSchedule('showBoard');
              this.BoardAction();
              this.removeIDTween('table');
              this.pushIDTween(tween(this.tableOpa).to(0.3, {
                opacity: 0
              }).call(() => {
                this.tableOpa.node.active = false;
              }).start(), 'table');
            }
          }, this);
        }

        BoardAction() {
          if (this.boradSprites && this.boradSprites.length > 0) {
            this.mode = 'idle';
            this.normalInfoNode.node.active = true;
            this.removeIDTween('normalInfoNode');
            this.pushIDTween(tween(this.normalInfoNode).to(0.3, {
              opacity: 255
            }).start(), 'normalInfoNode');

            if (this.index >= this.boradSprites.length) {
              this.index = 0;
            }

            this.showBoard(this.boradSprites[this.index]);
            this.index++;
          }
        }

        showWinCoin(coinNum) {
          this.removeIDSchedule('showBoard');

          if (coinNum > 0) {
            this.mode = 'win'; // if(this.targetImg){
            //     const imgTemp = this.targetImg;
            //     this.pushIDTween(sUtil.TweenColor(Color.TRANSPARENT,imgTemp,0.3,0,()=>{
            //         sObjPool.Enqueue(imgTemp.node.name,imgTemp.node);
            //     }),'imgSprite2');
            // }

            this.removeIDTween('normalInfoNode');
            this.pushIDTween(tween(this.normalInfoNode).to(0.3, {
              opacity: 0
            }).start(), 'normalInfoNode');

            if (!this.tableOpa.node.active) {
              this.removeIDTween('table');
              this.tableOpa.opacity = 0;
              this.tableOpa.node.active = true;
              this.pushIDTween(tween(this.tableOpa).to(0.3, {
                opacity: 255
              }).start(), 'table');
            }

            var lastCoin = this.coinLabelValue;
            this.coinLabelValue = coinNum;
            this.baseLabel.node.position = v3(0, -3, 0);
            this.multiNode.active = false;
            (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
              error: Error()
            }), sUtil) : sUtil).TweenLabel(lastCoin, this.coinLabelValue, this.baseLabel, 1);
          }
        }

        showBoard(spriteFrame) {
          if (this.targetImg) {
            var tempImg = this.targetImg;
            (_crd && sObjPool === void 0 ? (_reportPossibleCrUseOfsObjPool({
              error: Error()
            }), sObjPool) : sObjPool).Enqueue('bannerImg', tempImg.node);
            tempImg.node.active = false;
            this.targetImg = null;
          }

          if (this.normalInfoNode && spriteFrame) {
            // console.log('bannerImgcoun : ',sObjPool.Count('bannerImg'));
            var img = (_crd && sObjPool === void 0 ? (_reportPossibleCrUseOfsObjPool({
              error: Error()
            }), sObjPool) : sObjPool).Dequeue('bannerImg');

            if (img && img.isValid) {
              img.parent = this.normalInfoNode.node;
              img.position = v3(0, 0, 0);
              img.active = true;
              var imgSprite = img.getComponent(Sprite);

              if (imgSprite) {
                this.tableOpa.node.active = false;
                this.targetImg = imgSprite;
                imgSprite.spriteFrame = (_crd && assetMgr === void 0 ? (_reportPossibleCrUseOfassetMgr({
                  error: Error()
                }), assetMgr) : assetMgr).GetAssetByName(spriteFrame.name + '_int');
                ;
                imgSprite.color = Color.TRANSPARENT;
                this.pushIDTween((_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                  error: Error()
                }), sUtil) : sUtil).TweenColor(Color.WHITE, imgSprite, 0.3, 0), 'imgSprite');
                this.pushIDSchedule(() => {
                  this.BoardAction();
                }, 5, 'showBoard');
              }
            }
          }
        }

        multiLabelAnima(actionType, pos, multiValue, fontSize) {
          //console.log('倍数飞行');
          if (pos && multiValue) {
            console.log('倍数符号：', this.oneRoundMultiValue, multiValue);
            this.oneRoundMultiValue += multiValue;
            var baseLabelWidth = this.baseLabel.getComponent(UITransform).contentSize;
            var multi = this.multiNode.getChildByName('multi');
            var notation = this.multiNode.getChildByName('notation');
            var multiTran = multi.getComponent(UITransform);
            var multiScript = multi.getComponent(Label);
            var multiLabel = (_crd && sObjPool === void 0 ? (_reportPossibleCrUseOfsObjPool({
              error: Error()
            }), sObjPool) : sObjPool).Dequeue('boardMultiLabel');

            if (multiLabel && multiLabel.isValid) {
              multiLabel.scale = Vec3.ONE;
              multiLabel.parent = this.node;
              multiLabel.worldPosition = pos;
              this.SetNodeFront(multiLabel);
              multiLabel.active = true;
              var endPos = v3(multi.worldPosition.x + multiTran.contentSize.x / 2, multi.worldPosition.y, 0);
              var multiLabelScript = multiLabel.getComponent(Label);
              multiLabelScript.fontSize = fontSize;
              multiLabelScript.string = multiValue + 'x'; // this.removeIDTween('multiLabel');

              (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                error: Error()
              }), sAudioMgr) : sAudioMgr).PlayShotAudio('mult_fly', 1);
              this.pushIDTween(tween(this.baseLabel.node).delay(0.8).to(0.2, {
                position: v3(-35, -3, 0)
              }, {
                easing: 'cubicOut'
              }).call(() => {}).start(), 'multiLabelAnima');
              this.pushIDTween(tween(multiLabel).sequence(tween(multiLabel).to(0.53, {
                scale: v3(2, 2, 2)
              }, {
                easing: 'cubicOut'
              }), tween(multiLabel).parallel(tween(multiLabel).to(0.5, {
                scale: v3(0.8, 0.8, 1)
              }, {
                easing: 'linear'
              }), tween(multiLabel).call(() => {}).to(0.5, {
                worldPosition: endPos
              }, {
                easing: 'sineIn'
              }))).call(() => {
                if (this.mode != 'multi') {
                  this.mode = 'multi';
                  var _baseLabelWidth = this.baseLabel.getComponent(UITransform).contentSize;
                  this.multiNode.position = v3(_baseLabelWidth.x / 2 + 20, 5, 0);
                  this.removeIDTween('multiLabelAnima'); //sAudioMgr.PlayShotAudio('total multiplier_result_petvoice',1);

                  this.multiNode.active = true;
                  notation.active = true;
                }

                ; //this.scheduleOnce(()=>{

                multiScript.string = this.oneRoundMultiValue.toString(); //},0.4);

                console.log('倍数符号：', multiScript.string);
                tween(multi).to(0.15, {
                  scale: v3(1.6, 1.6, 1.6)
                }, {
                  easing: 'sineOut'
                }).to(0.1, {
                  scale: Vec3.ONE
                }).start();
                (_crd && sObjPool === void 0 ? (_reportPossibleCrUseOfsObjPool({
                  error: Error()
                }), sObjPool) : sObjPool).Enqueue('boardMultiLabel', multiLabel);
                multiLabel.active = false;

                if (actionType == 'close') {
                  tween(this.multiNode).delay(0.3).call(() => {
                    notation.active = false;
                  }).to(0.3, {
                    position: Vec3.ZERO
                  }, {
                    easing: 'cubicIn'
                  }).call(() => {
                    this.multiNode.active = false;
                    (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                      error: Error()
                    }), sAudioMgr) : sAudioMgr).PlayShotAudio('total multiplier_result_petvoice');
                  }).start();
                  tween(this.baseLabel.node).delay(0.3).call(() => {}).to(0.3, {
                    position: v3(0, -3, 0)
                  }, {
                    easing: 'cubicIn'
                  }).call(() => {
                    this.baseLabel.string = this.AddCommas(this.coinLabelValue * this.oneRoundMultiValue);
                    tween(this.baseLabel.node).to(0.15, {
                      scale: v3(1.3, 1.3, 1.3)
                    }, {
                      easing: 'sineOut'
                    }).to(0.1, {
                      scale: Vec3.ONE
                    }).call(() => {
                      (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                        error: Error()
                      }), sAudioMgr) : sAudioMgr).PlayShotAudio('total multiplier_result', 1);
                      director.emit('StarlightPrincessLabelEnd');
                    }).start();
                  }).start();
                }

                ;
              }).start(), 'multiLabel');
            }
          }
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "normalInfoNode", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "tableOpa", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "baseLabel", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "multiNode", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor5 = _applyDecoratedDescriptor(_class2.prototype, "boradSprites", [_dec6], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return [];
        }
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=StarlightPrincess_Board.js.map
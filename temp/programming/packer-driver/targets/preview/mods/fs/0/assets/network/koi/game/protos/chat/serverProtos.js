System.register(["cc"], function (_export, _context) {
  "use strict";

  var _cclegacy, _crd;

  return {
    setters: [function (_cc) {
      _cclegacy = _cc.cclegacy;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "a5601t5/WBAF4O3yX54zE/v", "serverProtos", undefined);

      _export("default", {
        "nested": {
          "chat": {
            "nested": {
              "chatHandler": {
                "nested": {
                  "chatOneByOne": {
                    "fields": {
                      "code": {
                        "type": "int32",
                        "id": 1
                      },
                      "data": {
                        "type": "Data",
                        "id": 2
                      },
                      "message": {
                        "type": "string",
                        "id": 3
                      }
                    },
                    "nested": {
                      "Data": {
                        "fields": {}
                      }
                    }
                  },
                  "chatDesk": {
                    "fields": {
                      "code": {
                        "type": "int32",
                        "id": 1
                      },
                      "data": {
                        "type": "Data",
                        "id": 2
                      },
                      "message": {
                        "type": "string",
                        "id": 3
                      }
                    },
                    "nested": {
                      "Data": {
                        "fields": {}
                      }
                    }
                  },
                  "get1by1Msg": {
                    "fields": {
                      "code": {
                        "type": "int32",
                        "id": 1
                      },
                      "data": {
                        "type": "Data",
                        "id": 2
                      },
                      "message": {
                        "type": "string",
                        "id": 3
                      }
                    },
                    "nested": {
                      "Data": {
                        "fields": {
                          "msgArr": {
                            "type": "string",
                            "id": 1
                          }
                        }
                      }
                    }
                  },
                  "sendChatMsg": {
                    "fields": {
                      "code": {
                        "type": "int32",
                        "id": 1
                      },
                      "data": {
                        "type": "Data",
                        "id": 2
                      },
                      "message": {
                        "type": "string",
                        "id": 3
                      }
                    },
                    "nested": {
                      "Data": {
                        "fields": {}
                      }
                    }
                  },
                  "getAllUnreadMsg": {
                    "fields": {
                      "code": {
                        "type": "int32",
                        "id": 1
                      },
                      "data": {
                        "type": "Data",
                        "id": 2
                      },
                      "message": {
                        "type": "string",
                        "id": 3
                      }
                    },
                    "nested": {
                      "Data": {
                        "fields": {
                          "unreadMsg": {
                            "type": "string",
                            "id": 1
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      });

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=serverProtos.js.map
System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2", "__unresolved_3", "__unresolved_4", "__unresolved_5"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, director, Label, Node, tween, v3, Vec3, sFreeWinFlow, sConfigMgr, sBoxMgr, sUtil, sAudioMgr, _dec, _dec2, _dec3, _dec4, _class, _class2, _descriptor, _descriptor2, _descriptor3, _temp, _crd, ccclass, property, StarlightPrincess_FreewinFlow;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfsFreeWinFlow(extras) {
    _reporterNs.report("sFreeWinFlow", "../lobby/game/core/sFreeWinFlow", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsConfigMgr(extras) {
    _reporterNs.report("sConfigMgr", "../lobby/game/core/sConfigMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsBoxMgr(extras) {
    _reporterNs.report("sBoxMgr", "../lobby/game/core/sBoxMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsUtil(extras) {
    _reporterNs.report("sUtil", "../lobby/game/core/sUtil", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsBoxEntity(extras) {
    _reporterNs.report("sBoxEntity", "../lobby/game/core/sBoxEntity", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsAudioMgr(extras) {
    _reporterNs.report("sAudioMgr", "../lobby/game/core/sAudioMgr", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      director = _cc.director;
      Label = _cc.Label;
      Node = _cc.Node;
      tween = _cc.tween;
      v3 = _cc.v3;
      Vec3 = _cc.Vec3;
    }, function (_unresolved_2) {
      sFreeWinFlow = _unresolved_2.sFreeWinFlow;
    }, function (_unresolved_3) {
      sConfigMgr = _unresolved_3.sConfigMgr;
    }, function (_unresolved_4) {
      sBoxMgr = _unresolved_4.sBoxMgr;
    }, function (_unresolved_5) {
      sUtil = _unresolved_5.sUtil;
    }, function (_unresolved_6) {
      sAudioMgr = _unresolved_6.default;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "fdc726Gwu9Kd7Gs4brByQRX", "StarlightPrincess_FreewinFlow", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("StarlightPrincess_FreewinFlow", StarlightPrincess_FreewinFlow = (_dec = ccclass('StarlightPrincess_FreewinFlow'), _dec2 = property(Node), _dec3 = property(Label), _dec4 = property(Label), _dec(_class = (_class2 = (_temp = class StarlightPrincess_FreewinFlow extends (_crd && sFreeWinFlow === void 0 ? (_reportPossibleCrUseOfsFreeWinFlow({
        error: Error()
      }), sFreeWinFlow) : sFreeWinFlow) {
        constructor() {
          super(...arguments);

          _initializerDefineProperty(this, "effectLayer", _descriptor, this);

          _initializerDefineProperty(this, "multiLabel", _descriptor2, this);

          _initializerDefineProperty(this, "leftFreeCountLabel", _descriptor3, this);

          _defineProperty(this, "nowMode", 'normal');

          _defineProperty(this, "multiValue", 0);

          _defineProperty(this, "freeTotalRate", 0);

          _defineProperty(this, "symbolToMulti", {
            2001: 2,
            2002: 3,
            2003: 4,
            2004: 5,
            2005: 6,
            2006: 8,
            2007: 10,
            2008: 12,
            2009: 15,
            2010: 20,
            2011: 25,
            2012: 50,
            2013: 100,
            2014: 250,
            2015: 500
          });

          _defineProperty(this, "tatalWin", 0);
        }

        start() {
          super.start();
          director.on('freeWinBegain', () => {
            this.multiLabel.string = '';
            this.multiValue = 0;
          }, this);
          director.on('freeWinOver', () => {
            this.multiLabel.string = '';
            this.multiValue = 0;
          }, this);
          director.on('StarlightPrincessLabelEnd', () => {
            this.multiLabel.string = this.multiValue + 'x';
            this.removeIDTween('multiLabel');
            this.pushIDTween(tween(this.multiLabel.node).to(0.15, {
              scale: v3(1.4, 1.4, 1.4)
            }, {
              easing: 'sineOut'
            }).to(0.1, {
              scale: Vec3.ONE
            }, {
              easing: 'sineIn'
            }).start(), 'multiLabel');
          }, this);
          director.on('freeWinAction', value => {
            this.leftFreeCountLabel.string = (value < 0 ? 0 : value).toString();
          }, this);
          this.scheduleOnce(() => {//director.emit('StarlightTotleViewOpen',2000);
          }, 2);
        }

        clearUI() {
          super.clearUI();
          this.removeScheduleDic();
        }

        rollFirstRoundAction(round, speedMode, clickMode) {
          director.off('rollOneColumnBump');
          director.off('byWayWinLightEnd');
          director.off('titleWinSettleEnd');
          director.off('rollStopOneRoundCall');
          this.freeWinIndex = 0;
          this.freeTotalRate = 0;
          this.tatalWin = 0;

          if (round) {
            var timestop = 0;
            var round_symbol_map = round['round_symbol_map'];

            for (var xx of round_symbol_map[0]) {
              for (var yy = 0; yy < xx.length; yy++) {
                //console.log('symbel:',xx[yy]);
                if (yy >= 1 && yy <= 5 && xx[yy] > 2000) {
                  timestop = 0;
                  director.emit('controlHuaXianZi', 'NormalIdle');
                  break;
                }

                ;
              }

              ;
              if (timestop > 0) break;
            }

            ;
            this.normalFlowDelayCall(() => {
              this.scheduleOnce(() => {
                var needWinLight = round.rate > 0 ? true : false;
                var round_symbol_map = round['round_symbol_map'];
                (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
                  error: Error()
                }), sConfigMgr) : sConfigMgr).instance.SetResData(round_symbol_map);
                var events = (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
                  error: Error()
                }), sConfigMgr) : sConfigMgr).instance.GetAllEventConfigByHitListOnCheck(round);

                if (events) {
                  var lastEvents = events[events.length - 1];

                  for (var i = 0; i < lastEvents.length; i++) {
                    var _event = lastEvents[i];

                    if (_event.event_type == 'changeRound') {
                      if (_event['round_change']) {
                        this.freeWinCount = _event['round_change'].num;
                      }

                      this.leftFreeCountLabel.string = (this.freeWinCount - this.freeWinIndex).toString();
                    }

                    ;
                  }

                  ;
                }

                ;
                director.on('rollOneColumnBump', col => {
                  //sAudioMgr.PlayShotAudio('luodi',0.4);
                  var colBoxs = (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                    error: Error()
                  }), sBoxMgr) : sBoxMgr).instance.GetColAllBox(col);
                  var shunxu = 0;
                  var shunxu1 = 0;

                  if (colBoxs) {
                    for (var rY = 0; rY < colBoxs.length; rY++) {
                      var rSymbol = colBoxs[rY];

                      if (rSymbol.SymbolValue > 2000) {
                        if (rSymbol.ViewItemIndex.y >= 0 && rSymbol.ViewItemIndex.y <= 4) {
                          rSymbol.DoEnitityAction('thunder');
                          console.log('召唤一道落雷1');

                          if (rSymbol.SymbolValue == 1) {
                            shunxu1++;

                            if (shunxu1 == 1) {
                              director.emit('controlHuaXianZi', 'NormalIdle');
                              this.playmultMp();
                            }

                            ;
                          }

                          ;
                        }

                        ;
                      } else if (rSymbol.SymbolValue == 1) {
                        shunxu++;
                        (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                          error: Error()
                        }), sAudioMgr) : sAudioMgr).PlayShotAudio('scatter_appear' + shunxu);

                        if (shunxu == 1) {
                          this.playscatterMp();
                        }

                        ;
                      }

                      ;
                      ;
                    }

                    ;
                  }

                  ;
                }, this);
                (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                  error: Error()
                }), sUtil) : sUtil).once('byWayWinLightEnd', () => {
                  (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                    error: Error()
                  }), sUtil) : sUtil).once('titleWinSettleEnd', () => {
                    var rate = round.rate;
                    this.freeTotalRate += rate;

                    if (rate > 200) {
                      this.scheduleOnce(() => {
                        director.emit('StarlightPrincessBigWin', rate);
                        console.log('大于200：无加倍倍数显示2', rate);
                        director.emit('betUserInfoUpdateWinAnima', rate - this.tatalWin);
                      }, 0.8);
                      (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                        error: Error()
                      }), sUtil) : sUtil).once('StarlightPrincessBigWinEnd', () => {
                        (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                          error: Error()
                        }), sBoxMgr) : sBoxMgr).instance.setAllViewTargetBoxArrayState(id => id == 1, 'win', 'settlement');
                        (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                          error: Error()
                        }), sAudioMgr) : sAudioMgr).PlayShotAudio('scatter_anima', 0.8);
                        (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                          error: Error()
                        }), sAudioMgr) : sAudioMgr).bgMusicOut(0.3, 0);
                        this.scheduleOnce(() => {
                          (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                            error: Error()
                          }), sAudioMgr) : sAudioMgr).PlayBG('bgm_interlude');
                          director.emit('freeWinBingo', this.freeWinCount, round.rate);
                        }, 3);
                      });
                    } else {
                      //director.emit('betUserInfoUpdateWinAnima',this.freeTotalRate);
                      (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                        error: Error()
                      }), sBoxMgr) : sBoxMgr).instance.setAllViewTargetBoxArrayState(id => id == 1, 'win', 'settlement');
                      (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                        error: Error()
                      }), sAudioMgr) : sAudioMgr).PlayShotAudio('scatter_anima', 0.8);
                      (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                        error: Error()
                      }), sAudioMgr) : sAudioMgr).bgMusicOut(0.3, 0);
                      this.scheduleOnce(() => {
                        (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                          error: Error()
                        }), sAudioMgr) : sAudioMgr).PlayBG('bgm_interlude');
                        director.emit('freeWinBingo', this.freeWinCount, round.rate);
                      }, 3);
                    }
                  });
                  director.emit('winBetRes', -1);
                });
                (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                  error: Error()
                }), sUtil) : sUtil).once('rollStopOneRoundCall', () => {
                  director.off('rollOneColumnStop');
                  director.off('rollOneColumnBump');

                  if (needWinLight) {
                    this.byWayBoxLightCtr(round, 'normal');
                  } else {
                    (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                      error: Error()
                    }), sBoxMgr) : sBoxMgr).instance.setAllViewTargetBoxArrayState(id => id == 1, 'win', 'settlement');
                    (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                      error: Error()
                    }), sAudioMgr) : sAudioMgr).PlayShotAudio('scatter_anima', 0.8);
                    (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                      error: Error()
                    }), sAudioMgr) : sAudioMgr).bgMusicOut(0.3, 0);
                    this.scheduleOnce(() => {
                      (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                        error: Error()
                      }), sAudioMgr) : sAudioMgr).PlayBG('bgm_interlude');
                      director.emit('freeWinBingo', this.freeWinCount, 0);
                    }, 3);
                  }
                });

                if (speedMode == 'turbo') {
                  (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                    error: Error()
                  }), sBoxMgr) : sBoxMgr).instance.rollStopAllColumn();
                } else if (speedMode == 'normal') {
                  (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                    error: Error()
                  }), sBoxMgr) : sBoxMgr).instance.rollStopAllColumn();
                }
              }, timestop);
            });
          }
        }

        rollStopAction(round, rollSpeedMode, clickMode) {
          this.nowMode = 'freeWin';
          director.off('rollOneColumnBump');
          director.off('byWayWinLightEnd');
          director.off('titleWinSettleEnd');
          director.off('rollStopOneRoundCall');

          if (round) {
            var timestop = 0;
            var round_symbol_map = round['round_symbol_map'];

            for (var xx of round_symbol_map[0]) {
              for (var yy = 0; yy < xx.length; yy++) {
                //console.log('symbel:',xx[yy]);
                if (yy >= 1 && yy <= 5 && xx[yy] > 2000) {
                  timestop = 0;
                  director.emit('controlHuaXianZi', 'NormalIdle');
                  break;
                }

                ;
              }

              ;
              if (timestop > 0) break;
            }

            ;
            this.normalFlowDelayCall(() => {
              this.scheduleOnce(() => {
                this.tatalWin = 0;
                var needWinLight = round.rate > 0 ? true : false;
                var round_symbol_map = round['round_symbol_map'];
                (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
                  error: Error()
                }), sConfigMgr) : sConfigMgr).instance.SetResData(round_symbol_map); // const events = sConfigMgr.instance.GetAllEventConfigByHitListOnCheck(round);
                // if(events){
                //     const lastEvents = events[events.length - 1];
                //     for (let i = 0; i < lastEvents.length; i++) {
                //         const _event = lastEvents[i];
                //         if (_event.event_type == 'changeRound') {
                //             this.freeWinCount = _event['round_change'].num;
                //         }
                //     }
                // }

                var allEvents = (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
                  error: Error()
                }), sConfigMgr) : sConfigMgr).instance.GetAllEventConfigByHitListOnCheck(round);
                var freeCount = 0;

                if (allEvents && allEvents.length > 0) {
                  for (var e = 0; e < allEvents.length; e++) {
                    var eventArr = allEvents[e];

                    for (var f = 0; f < eventArr.length; f++) {
                      var _fEvent = eventArr[f];

                      if (e == allEvents.length - 1) {
                        if (_fEvent.event_type == 'changeRound') {
                          if (_fEvent['round_change']) {
                            freeCount = _fEvent['round_change'].num;
                          }

                          ;
                        }

                        ;
                      }

                      ; // if(_fEvent.event_type == 'boxAnima'){
                      //     needWinLight = true;
                      // }
                    }

                    ;
                  }

                  ;
                }

                ;
                director.on('rollOneColumnBump', col => {
                  (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                    error: Error()
                  }), sAudioMgr) : sAudioMgr).PlayShotAudio('luodi', 0.4);
                  var colBoxs = (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                    error: Error()
                  }), sBoxMgr) : sBoxMgr).instance.GetColAllBox(col);
                  var shunxu = 0;
                  var shunxu1 = 0;

                  if (colBoxs) {
                    for (var rY = 0; rY < colBoxs.length; rY++) {
                      var rSymbol = colBoxs[rY];

                      if (rSymbol.SymbolValue > 2000) {
                        if (rSymbol.ViewItemIndex.y >= 0 && rSymbol.ViewItemIndex.y <= 4) {
                          rSymbol.DoEnitityAction('thunder');
                          console.log('召唤一道落雷2');

                          if (rSymbol.SymbolValue == 1) {
                            shunxu1++;

                            if (shunxu1 == 1) {
                              director.emit('controlHuaXianZi', 'NormalIdle');
                              this.playmultMp();
                            }

                            ;
                          }

                          ;
                        }
                      } else if (rSymbol.SymbolValue == 1) {
                        shunxu++;
                        (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                          error: Error()
                        }), sAudioMgr) : sAudioMgr).PlayShotAudio('scatter_appear' + shunxu);

                        if (shunxu == 1) {
                          this.playscatterMp();
                        }

                        ;
                      }

                      ;
                    }
                  }
                }, this);
                (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                  error: Error()
                }), sUtil) : sUtil).once('byWayWinLightEnd', () => {
                  (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                    error: Error()
                  }), sUtil) : sUtil).once('titleWinSettleEnd', () => {
                    var rate = round.rate;
                    this.freeTotalRate += rate;

                    if (rate >= 200) {
                      this.scheduleOnce(() => {
                        director.emit('StarlightPrincessBigWin', rate);
                        console.log('大于200：无加倍倍数显示2', rate);
                        director.emit('betUserInfoUpdateWinAnima', rate - this.tatalWin);
                      }, 0.8);
                      (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                        error: Error()
                      }), sUtil) : sUtil).once('StarlightPrincessBigWinEnd', () => {
                        if (freeCount > 0) {
                          (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                            error: Error()
                          }), sUtil) : sUtil).once('bingoFreewinInFreeEnd', () => {
                            this.freeWinCount += freeCount;
                            this.leftFreeCountLabel.string = (this.freeWinCount - this.freeWinIndex).toString();
                            this.scheduleOnce(() => {
                              //console.log('免费游戏round结束freeCount1')
                              director.emit('freeWinOneRoundEnd');
                            }, 0.5);
                          });
                          (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                            error: Error()
                          }), sAudioMgr) : sAudioMgr).PlayShotAudio('scatter_anima');
                          (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                            error: Error()
                          }), sBoxMgr) : sBoxMgr).instance.setAllViewTargetBoxArrayState(id => id == 1, 'win', 'settlement');
                          this.scheduleOnce(() => {
                            director.emit('bingoFreewinInFree', freeCount);
                          }, 2.5);
                        } else {
                          this.scheduleOnce(() => {
                            //console.log('免费游戏round结束freeCount2')
                            director.emit('freeWinOneRoundEnd');
                          }, 0.5);
                        }

                        ;
                      });
                    } else {
                      //director.emit('betUserInfoUpdateWinAnima',this.freeTotalRate);
                      if (freeCount > 0) {
                        (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                          error: Error()
                        }), sUtil) : sUtil).once('bingoFreewinInFreeEnd', () => {
                          this.freeWinCount += freeCount;
                          this.leftFreeCountLabel.string = (this.freeWinCount - this.freeWinIndex).toString();
                          this.scheduleOnce(() => {
                            console.log('免费游戏round结束freeCount1');
                            director.emit('freeWinOneRoundEnd');
                          }, 0.5);
                        });
                        (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                          error: Error()
                        }), sAudioMgr) : sAudioMgr).PlayShotAudio('scatter_anima');
                        (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                          error: Error()
                        }), sBoxMgr) : sBoxMgr).instance.setAllViewTargetBoxArrayState(id => id == 1, 'win', 'settlement');
                        this.scheduleOnce(() => {
                          director.emit('bingoFreewinInFree', freeCount);
                        }, 2.5);
                      } else {
                        this.scheduleOnce(() => {
                          console.log('免费游戏round结束freeCount2');
                          director.emit('freeWinOneRoundEnd');
                        }, 0.5);
                      }

                      ;
                    }

                    ;
                  });
                  director.emit('winBetRes', -1);
                });
                (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                  error: Error()
                }), sUtil) : sUtil).once('rollStopOneRoundCall', () => {
                  director.off('rollOneColumnStop');
                  director.off('rollOneColumnBump');

                  if (needWinLight) {
                    this.byWayBoxLightCtr(round, 'normal', false);
                  } else {
                    if (freeCount > 0) {
                      (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                        error: Error()
                      }), sUtil) : sUtil).once('bingoFreewinInFreeEnd', () => {
                        this.freeWinCount += freeCount;
                        this.leftFreeCountLabel.string = (this.freeWinCount - this.freeWinIndex).toString();
                        this.scheduleOnce(() => {
                          console.log('免费游戏round结束3');
                          director.emit('freeWinOneRoundEnd');
                        }, 0.5);
                      });
                      (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                        error: Error()
                      }), sAudioMgr) : sAudioMgr).PlayShotAudio('scatter_anima');
                      (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                        error: Error()
                      }), sBoxMgr) : sBoxMgr).instance.setAllViewTargetBoxArrayState(id => id == 1, 'win', 'settlement');
                      this.scheduleOnce(() => {
                        director.emit('bingoFreewinInFree', freeCount);
                      }, 2.5);
                    } else {
                      this.scheduleOnce(() => {
                        console.log('免费游戏round结束4');
                        director.emit('freeWinOneRoundEnd');
                      }, 0.5);
                    }
                  }
                });
                (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                  error: Error()
                }), sBoxMgr) : sBoxMgr).instance.rollStopAllColumn();
              }, timestop);
            });
          }
        }

        byWayBoxLightCtr(round, rollSpeedMode, isFirst) {
          var _this = this;

          if (isFirst === void 0) {
            isFirst = true;
          }

          var boxWinIndex = 1;
          director.off('dropAllBoxFinish');
          var eventArr = (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
            error: Error()
          }), sConfigMgr) : sConfigMgr).instance.GetAllEventOptConfigByHitListOnCheck(round); // console.log('eventArr:'+JSON.stringify(eventArr));

          var boxSum = (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
            error: Error()
          }), sBoxMgr) : sBoxMgr).instance.boxSum;
          var isTotalWin = false;
          var isEnd = false;
          var IsMulit = false;

          if (eventArr && Array.isArray(eventArr)) {
            var eventIndex = 0;

            var boxAnimaCall = () => {
              if (eventIndex < eventArr.length) {
                isEnd = eventIndex == eventArr.length - 1;
                var nowData = eventArr[eventIndex++];

                if (nowData && nowData.eventData && nowData.eventData.length > 0) {
                  var isWin = false;
                  var events = nowData.eventData;
                  var resAct = {};
                  var winSymbol = {};
                  var winRate = 0;

                  for (var i = 0; i < events.length; i++) {
                    var _event = events[i];

                    if (_event.event_type == 'boxAnima') {
                      if (_event.rate_change && _event.rate_change.num) {
                        winRate += _event.rate_change.num;
                      }

                      if (_event.bonus_symbol) {
                        winSymbol[_event.bonus_symbol] = true;
                      }

                      isWin = true;
                      isTotalWin = true;
                      var act = resAct[_event.bonus_symbol];

                      if (!act) {
                        act = {};
                        act.bonus_symbol = _event.bonus_symbol;
                        act.act_pos = {};
                        act.rate = 0;
                      }

                      if (_event.rate_change) {
                        if (act.rate == 0) {
                          act.rate = _event.rate_change.num;
                        } else {
                          if (_event.rate_change.type == 0) {
                            act.rate += _event.rate_change.num;
                          } else if (_event.rate_change.type == 1) {
                            act.rate *= _event.rate_change.num;
                          }
                        }
                      }

                      if (_event.act_pos) {
                        var poss = _event['act_pos'].split(',');

                        if (poss) {
                          for (var a = 0; a < poss.length; a++) {
                            var value = poss[a];
                            act.act_pos[value] = true;
                          }
                        }
                      }

                      resAct[_event.bonus_symbol] = act;
                    } else if (_event.event_type == 'changeRound') {
                      //isWin = true;
                      if (_event['rate_change']) {
                        isTotalWin = true;
                        winRate += _event['rate_change'].num;
                      }
                    }

                    ;
                  }

                  ;
                  var resActKeys = Object.keys(resAct);
                  var allActPos = {};

                  if (resActKeys) {
                    for (var _i = 0; _i < resActKeys.length; _i++) {
                      var element = resAct[resActKeys[_i]];
                      var itemKeys = Object.keys(element.act_pos);
                      director.emit('ws', {
                        symbol: element.bonus_symbol,
                        rate: element.rate,
                        num: itemKeys.length,
                        boxPos: itemKeys[Math.trunc(itemKeys.length / 2)]
                      });

                      for (var j = 0; j < itemKeys.length; j++) {
                        var key = itemKeys[j];
                        allActPos[key] = true;
                      }

                      ;
                      director.emit('slotWinBoxItemInfo', {
                        symbol: element.bonus_symbol,
                        rate: element.rate,
                        num: itemKeys.length,
                        boxPos: itemKeys[Math.trunc(itemKeys.length / 2)]
                      }); // this.byWayboxResWinAction(element);
                    }

                    ; // let boxActArr: any[][] = [];

                    for (var _i2 = 0; _i2 < (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                      error: Error()
                    }), sBoxMgr) : sBoxMgr).instance.boxArray.length; _i2++) {
                      var arr = (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                        error: Error()
                      }), sBoxMgr) : sBoxMgr).instance.boxArray[_i2];

                      for (var m = 0; m < arr.length; m++) {
                        if (arr[m].ViewItemIndex.y >= 0 && arr[m].ViewItemIndex.y <= 4) {
                          //console.log('symbolvalue:',sBoxMgr.instance.boxArray,i,m,sBoxMgr.instance.getBoxEntityByXY(i,m),sBoxMgr.instance.getBoxEntityByXY(i,m).SymbolValue);
                          if (arr[m].SymbolValue > 2000) {
                            IsMulit = true;
                            console.log('加倍存在：', IsMulit);
                            break;
                          }

                          ;
                        }

                        ;
                      }

                      ;

                      if (IsMulit) {
                        break;
                      }

                      ;
                    }

                    ;
                    var allKeys = Object.keys(allActPos);
                    var xx = 0;

                    for (var _i3 = 0; _i3 < allKeys.length; _i3++) {
                      var _element = allKeys[_i3];

                      var _act = _element.split('_');

                      if (_act && _act.length == 2) {
                        (function () {
                          var act1 = parseInt(_act[0]);
                          var act2 = parseInt(_act[1]); //console.log('消除的格子',act1,act2);

                          if (act1 && act2 || act1 == 0 || act2 == 0) {
                            if ((_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                              error: Error()
                            }), sBoxMgr) : sBoxMgr).instance.getBoxEntityByXY(act1, act2).SymbolValue > 2000) {
                              xx++;

                              _this.scheduleOnce(() => {
                                console.log('星星图案mult', act1, act2);
                                (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                                  error: Error()
                                }), sBoxMgr) : sBoxMgr).instance.updateBoxDataByXY(act1, act2, 'win', 'settlement');
                              }, 0.1 * xx);
                            } else {
                              (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                                error: Error()
                              }), sBoxMgr) : sBoxMgr).instance.updateBoxDataByXY(act1, act2, 'win', 'settlement');
                            }

                            ;
                          }

                          ;
                        })();
                      }

                      ;
                    }

                    ;
                    director.emit('winBetRes', winRate);

                    if (round.rate < 200 && !IsMulit) {
                      console.log('小于200：无加倍倍数显示3', winRate);
                      director.emit('betUserInfoUpdateWinAnima', winRate);
                    } else if (!IsMulit) {
                      if (eventIndex != eventArr.length - 1) {
                        this.tatalWin += winRate;
                        director.emit('betUserInfoUpdateWinAnima', winRate);
                      }

                      ;
                    } else if (IsMulit) {
                      this.tatalWin += winRate;
                      director.emit('betUserInfoUpdateWinAnima', winRate);
                    }

                    ;

                    if (isWin) {
                      var winSymbolKeys = Object.keys(winSymbol);

                      if (winSymbolKeys && winSymbolKeys.length > 0) {
                        for (var _i4 = 0; _i4 < winSymbolKeys.length; _i4++) {
                          var winSymbolKey = winSymbolKeys[_i4];
                        }

                        ;
                      }

                      ; // this.scheduleOnce(()=>{
                      //     sAudioMgr.PlayShotAudio('boxWinTurnRound');
                      // },1);
                      // director.emit('winBetRateRes',winRate * this.getMultiple(eventIndex - 1));

                      (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                        error: Error()
                      }), sBoxMgr) : sBoxMgr).instance.blackCurtainAnima();

                      if (boxWinIndex > 10) {
                        boxWinIndex = 10;
                      } //this.scheduleOnce(() => {


                      (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                        error: Error()
                      }), sAudioMgr) : sAudioMgr).PlayAudio('freegame_small_win'); //sAudioMgr.PlayShotAudio('small_win_effect');

                      this.scheduleOnce(() => {
                        (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                          error: Error()
                        }), sAudioMgr) : sAudioMgr).StopAudio();
                        (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                          error: Error()
                        }), sAudioMgr) : sAudioMgr).PlayShotAudio('freegame_small_win_end' + eventIndex);
                        this.scheduleOnce(() => {
                          (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                            error: Error()
                          }), sAudioMgr) : sAudioMgr).PlayShotAudio('symbol_clear' + eventIndex);
                        }, 1);
                      }, 0.6);
                      (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                        error: Error()
                      }), sBoxMgr) : sBoxMgr).instance.blackCurtainAnima(-1, false); //}, 0.5);

                      this.scheduleOnce(() => {
                        // director.emit('titleSumLightMsg','normal',eventIndex);
                        (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
                          error: Error()
                        }), sConfigMgr) : sConfigMgr).instance.TurnNextResRoundData();
                        (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                          error: Error()
                        }), sBoxMgr) : sBoxMgr).instance.ReplenishBoxEntity();
                        var xx = 0;
                        (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                          error: Error()
                        }), sBoxMgr) : sBoxMgr).instance.DropAllEntityBoxAfterRemove(strikeBox => {
                          (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                            error: Error()
                          }), sAudioMgr) : sAudioMgr).PlayShotAudio('boxDropStrike');

                          if (strikeBox) {
                            if (strikeBox.SymbolValue > 2000) {
                              if (strikeBox.ViewItemIndex.y > 4) {
                                xx++;
                                strikeBox.DoEnitityAction('thunder');
                                console.log('召唤一道落雷3');

                                if (xx == 1) {
                                  director.emit('controlHuaXianZi', 'NormalIdle');
                                }

                                ;
                              }

                              ;
                            }

                            ;
                          }

                          ;
                        }, 0.02);
                      }, 2.5);
                    }

                    ;
                  }

                  ;
                }

                ;

                if (isEnd) {
                  if (isTotalWin && nowData.opt && nowData.opt.multiData && nowData.opt.multiData.length > 0) {
                    var multiValue = this.multiValue;
                    var multiboxes = [];

                    for (var o = 0; o < nowData.opt.multiData.length; o++) {
                      var multiData = nowData.opt.multiData[o];

                      if (multiData.multi > 0) {
                        multiValue += multiData.multi;
                      }

                      if (multiData && multiData.pos && multiData.pos.length > 0) {
                        for (var l = 0; l < multiData.pos.length; l++) {
                          var mPos = multiData.pos[l];

                          if (mPos) {
                            var mPosArr = mPos.split('_');

                            if (mPosArr && mPosArr.length == 2) {
                              multiboxes.push((_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                                error: Error()
                              }), sBoxMgr) : sBoxMgr).instance.getBoxEntityByXY(parseInt(mPosArr[0]), parseInt(mPosArr[1])));
                            }

                            ;
                          }

                          ;
                        }

                        ;
                      }

                      ;
                    }

                    ;

                    if (multiboxes.length > 0) {
                      var mulCall = () => {
                        var mulIndex = 0;
                        this.pushOneSchedule(() => {
                          var mBox = multiboxes[mulIndex++];

                          if (mBox) {
                            mBox.UpdateData('win', mulIndex == multiboxes.length ? 'close' : 'open');
                          }
                        }, 0, multiboxes.length, 1.5, 0, true);
                        this.scheduleOnce(() => {
                          if (round.rate > 200) {} else {
                            console.log('小于200：有加倍倍数显示4', round.rate);
                            director.emit('betUserInfoUpdateWinAnima', round.rate - this.tatalWin);
                          }

                          ;
                          director.emit('byWayWinLightEnd');
                        }, multiboxes.length * 1.5);
                      };

                      if (multiValue > 0) {
                        if (this.multiValue > 0) {
                          (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                            error: Error()
                          }), sAudioMgr) : sAudioMgr).PlayShotAudio('fs_Multifly');
                          this.playtotalMultMp3();
                          var str = this.multiLabel.string.slice(0, this.multiLabel.string.length);
                          director.emit('winMultiBox', {
                            actionType: 'open',
                            pos: this.multiLabel.node.worldPosition,
                            multi: parseInt(str),
                            fontSize: 25
                          });
                        }

                        ;
                        this.scheduleOnce(mulCall, this.multiValue == 0 ? 0 : 1.5);
                      } else {
                        mulCall();
                      }

                      ;
                    } else {
                      director.emit('byWayWinLightEnd');
                    }

                    ;

                    if (!isFirst) {
                      this.multiValue = multiValue;
                    }

                    ;
                  } else {
                    //if (eventArr.length > 1) {
                    director.emit('byWayWinLightEnd'); //};
                  }

                  ;
                }

                ;
              }

              ;
            };

            boxAnimaCall();
            director.on('dropAllBoxFinish', () => {
              // console.log('dropAllBoxFinish');
              boxAnimaCall(); // console.log('isEnd:',isEnd);
            }, this);
          }
        }

        freeWinTotleViewOpen(winTotalCoin, winRealTotalCoin) {
          director.emit('StarlightTotleViewOpen', winRealTotalCoin, this.freeWinCount); // if(this.totalWinPrefab){
          //     const obj = instantiate(this.totalWinPrefab);
          //     obj.setParent(this.node.parent);
          //     obj.getComponent(UIBase).DataInit({coin:winTotalCoin,count : this.freeWinCount});
          //     director.emit('freeWinEndRes',winTotalCoin,winRealTotalCoin);
          // }else{
          //     director.emit('freeWinOver');
          //     director.emit('rollStop');
          // }
        }

        playscatterMp() {
          var num = (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
            error: Error()
          }), sUtil) : sUtil).RandomInt(0, 11);

          if (num >= 0 && num < 9) {
            (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
              error: Error()
            }), sAudioMgr) : sAudioMgr).PlayShotAudio('scatter_appear_voice' + num);
          }
        }

        playmultMp() {
          var num = (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
            error: Error()
          }), sUtil) : sUtil).RandomInt(1, 11);

          if (num >= 0 && num < 10) {
            (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
              error: Error()
            }), sAudioMgr) : sAudioMgr).PlayShotAudio('mult_appear_voice' + num);
          }
        }

        playtotalMultMp3() {
          var num = (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
            error: Error()
          }), sUtil) : sUtil).RandomInt(1, 8);

          if (num >= 0 && num < 9) {
            (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
              error: Error()
            }), sAudioMgr) : sAudioMgr).PlayShotAudio('total multiplier_voice' + num);
          }

          ;
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "effectLayer", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "multiLabel", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "leftFreeCountLabel", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=StarlightPrincess_FreewinFlow.js.map
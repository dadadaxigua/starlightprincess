System.register(["__unresolved_0", "cc", "__unresolved_1"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, Node, Label, director, sComponent, _dec, _dec2, _dec3, _dec4, _dec5, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _temp, _crd, ccclass, property, UIFreeWinBuy;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfsComponent(extras) {
    _reporterNs.report("sComponent", "../../../game/core/sComponent", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      Node = _cc.Node;
      Label = _cc.Label;
      director = _cc.director;
    }, function (_unresolved_2) {
      sComponent = _unresolved_2.sComponent;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "07046gMriFP84vrCWIZAdNZ", "UIFreeWinBuy", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("UIFreeWinBuy", UIFreeWinBuy = (_dec = ccclass('UIFreeWinBuy'), _dec2 = property(Node), _dec3 = property(Node), _dec4 = property(Label), _dec5 = property(Node), _dec(_class = (_class2 = (_temp = class UIFreeWinBuy extends (_crd && sComponent === void 0 ? (_reportPossibleCrUseOfsComponent({
        error: Error()
      }), sComponent) : sComponent) {
        constructor() {
          super(...arguments);

          _initializerDefineProperty(this, "startBtn", _descriptor, this);

          _initializerDefineProperty(this, "cancelNode", _descriptor2, this);

          _initializerDefineProperty(this, "coinLabel", _descriptor3, this);

          _initializerDefineProperty(this, "mainNode", _descriptor4, this);

          _defineProperty(this, "mBetType", -1);

          _defineProperty(this, "mCoin", 0);

          _defineProperty(this, "canBuy", false);

          _defineProperty(this, "tipStr", void 0);
        }

        start() {
          this.startBtn.on('click', () => {
            if (this.canBuy) {
              this.mainNode.active = false;

              if (this.mBetType != -1) {
                director.emit('subGameBetActionBtnClick', this.mBetType, this.mCoin);
              }
            } else {
              // director.emit('uiTipsOpen', 'text', 'tipStr');
              // if(this.tipStr){
              //     director.emit('uiTipsOpen', 'text', this.tipStr);
              // }
              globalThis.goldNotenough && globalThis.goldNotenough();
            }
          }, this);
          this.cancelNode.on('click', () => {
            this.mBetType = -1;
            this.mainNode.active = false;
            director.emit('subGameBetActionBtnCancel');
          }, this);
        }

        viewInit(coin, betType, _canBuy, _tipStr) {
          this.coinLabel.string = this.AddCommas(coin);
          this.mCoin = coin;
          this.mBetType = betType;
          this.canBuy = _canBuy;
          this.tipStr = _tipStr;
          this.mainNode.active = true;
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "startBtn", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "cancelNode", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "coinLabel", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "mainNode", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=UIFreeWinBuy.js.map
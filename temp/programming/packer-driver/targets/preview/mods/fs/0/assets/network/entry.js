System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, sys, director, _socketInfo, _globalDta, _crd, obj;

  function _reportPossibleCrUseOf_socketInfo(extras) {
    _reporterNs.report("_socketInfo", "./koi/game/game-entry", _context.meta, extras);
  }

  function _reportPossibleCrUseOf_globalDta(extras) {
    _reporterNs.report("_globalDta", "./global/global-entry", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      sys = _cc.sys;
      director = _cc.director;
    }, function (_unresolved_2) {
      _socketInfo = _unresolved_2.default;
    }, function (_unresolved_3) {
      _globalDta = _unresolved_3.default;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "11498d14zJDnLDFTzBNhjD0", "entry", undefined);

      obj = {
        /**
         * 获取自1970年来的time ms值 , 与服务端约定所有time ms为2017-10-1以来
         */
        ticks: function ticks(_ticks) {
          if (_ticks === void 0) {
            _ticks = null;
          }

          if (_ticks) return new Date(2017, 9, 1).getTime() + _ticks;else return new Date().getTime() - new Date(2017, 9, 1).getTime();
        },
        uid: function uid() {
          return this.ticks() + Math.floor(Math.random() * 10000);
        },
        random: function random(num0, num, enableEndPoint) {
          if (enableEndPoint === void 0) {
            enableEndPoint = false;
          }

          if (!num) {
            num = num0;
            num0 = 0;
          }

          if (enableEndPoint == true) num++;
          return num0 + Math.floor(Math.random() * (num - num0));
        },
        hypotenuseLength: function hypotenuseLength(a, b) {
          return Math.floor(Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2)));
        },
        degree: function degree(a, b) {
          return -Math.atan2((b.y || b[1] || 0) - (a.y || a[1] || 0), (b.x || b[0] || 0) - (a.x || a[0] || 0)) * 180 / Math.PI;
        },
        isOutScreen: function isOutScreen(p, cooType) {
          if (cooType === void 0) {
            cooType = this.Consts.Coordinate.Cocos;
          }

          var x = p[0] || p.x;
          var y = p[1] || p.y;

          if (cooType == this.Consts.Coordinate.Center) {
            x += 1920 / 2;
            y += 1020 / 2;
          }

          return !(x >= 0 && x <= 1920 && y >= 0 && y <= 1020);
        },

        /**
         * '-200,872_207,451_33,850_1641,7,5.404_721,583_940,802'
         * =>
         * (6) [[-200,872], [207,451], [33,850], [1641,7,5.404], [721,583], [940,802]]
         */
        getArrFromPointsString: function getArrFromPointsString(str) {
          var res;

          if (typeof str == 'string') {
            res = str.split('_');

            for (var i = 0; i < res.length; i++) {
              res[i] = res[i].split(',');
              res[i][0] = parseFloat(res[i][0]);
              res[i][1] = parseFloat(res[i][1]);
            }

            return res;
          } else if (str instanceof Array) return str;
        },

        toCenterCoordinate(p) {
          if (p[0]) return [p[0] - 1920 / 2, p[1] - 1080 / 2];else if (p.x) return {
            x: p.x - 1920 / 2,
            y: p.y - 1080 / 2
          };
        },

        // 储存信息           
        //                     参数        localName缓存名称  localInformationName:本地信息名称     data传入的数据      isReplaceAll 是否全部替换      success 成功回调

        /*
           如果只需要替换部分内容isReplaceAll 不谢  或者设置为false即可
        */

        /**
         * 储存信息       
         * @param {*} res={}//传入对象参数
         * @param {*} res.localName localStorage缓存名称，默认为null,需要保存本地缓存 传入缓存名称即可
         * @param {*} res.localInformationName 本地待合并信息名称  默认为null,需要保存本地信息 传入本地信息名称即可
         * @param {*} res.data 传入的数据
         * @param {*} res.isReplaceAll 是否全部替换  默认false,只替换传入的部分数值，  =true整个替换已存的对应信息
         * @param {*} res.success :function(),//成功回调   ,需要时调用
         * @example koi.saveData  
         */
        saveData: function saveData(res) {
          if (res === void 0) {
            res = {
              itemName: '',
              localInformationName: null,
              data: null,
              isReplaceAll: false,
              success: function success() {}
            };
          }

          //获取本地缓存信息   如果不存在,或者isReplaceAll=true全部保存，就直接保存信息，
          // let localStorageItem=this.getLocalStorage({itemName:res.itemName});
          if (!res.localInformationName || res.isReplaceAll) {
            if (res.localInformationName && !(res.localInformationName == 'undefined' && res.localInformationName == 'null')) {
              res.localInformationName = res.data;
            }

            sys.localStorage.setItem(res.itemName, JSON.stringify(res.data));

            if (res.success && typeof res.success == 'function') {
              res.success();
            }

            return;
          } //循环并替换参数


          for (var i in res.data) {
            if (res.localInformationName && !(res.localInformationName == 'undefined' && res.localInformationName == 'null')) {
              res.localInformationName[i] = res.data[i];
            }
          }

          console.log('保存信息', res.localInformationName);
          sys.localStorage.setItem(res.itemName, JSON.stringify(res.localInformationName));

          if (res.success && typeof res.success == 'function') {
            res.success();
          }
        },
        //获取本地信息        itemName

        /**
         * 储存信息       
         * @param {*} res={}//传入对象参数
         * @param {*} res.itemName localStorage缓存名称，
         * @example koi.getLocalStorage  
         */
        getLocalStorage: function getLocalStorage(ret) {
          if (ret === void 0) {
            ret = {
              itemName: ''
            };
          }

          var localItem = sys.localStorage.getItem(ret.itemName);

          if (localItem && localItem != 'undefined') {
            return JSON.parse(localItem);
          } else {
            return null;
          }
        },

        //获取当前场景名称 

        /**
         */
        getSceneName() {
          var scene = director.getScene();

          if (scene && scene.isValid) {
            return scene.name;
          } else {
            return false;
          }
        }

      };
      globalThis.__koi = {
        globalDta: _crd && _globalDta === void 0 ? (_reportPossibleCrUseOf_globalDta({
          error: Error()
        }), _globalDta) : _globalDta,
        socketInfo: _crd && _socketInfo === void 0 ? (_reportPossibleCrUseOf_socketInfo({
          error: Error()
        }), _socketInfo) : _socketInfo
      };

      _export("default", {
        _obj: obj,
        globalDta: _crd && _globalDta === void 0 ? (_reportPossibleCrUseOf_globalDta({
          error: Error()
        }), _globalDta) : _globalDta,
        socketInfo: _crd && _socketInfo === void 0 ? (_reportPossibleCrUseOf_socketInfo({
          error: Error()
        }), _socketInfo) : _socketInfo
      });

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=entry.js.map
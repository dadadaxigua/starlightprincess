System.register(["cc"], function (_export, _context) {
  "use strict";

  var _cclegacy, _decorator, Component, Node, assetManager, SpriteFrame, instantiate, Sprite, UITransform, tween, v3, Widget, Label, director, _dec, _dec2, _dec3, _dec4, _class, _class2, _descriptor, _descriptor2, _descriptor3, _temp, _crd, ccclass, property, UIImgView;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  return {
    setters: [function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      Component = _cc.Component;
      Node = _cc.Node;
      assetManager = _cc.assetManager;
      SpriteFrame = _cc.SpriteFrame;
      instantiate = _cc.instantiate;
      Sprite = _cc.Sprite;
      UITransform = _cc.UITransform;
      tween = _cc.tween;
      v3 = _cc.v3;
      Widget = _cc.Widget;
      Label = _cc.Label;
      director = _cc.director;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "bc179uoqhxHU7KRUN0WkaC4", "UIImgView", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("UIImgView", UIImgView = (_dec = ccclass('UIImgView'), _dec2 = property(Node), _dec3 = property(Node), _dec4 = property(Label), _dec(_class = (_class2 = (_temp = class UIImgView extends Component {
        constructor() {
          super(...arguments);

          _initializerDefineProperty(this, "imgCopyNode", _descriptor, this);

          _initializerDefineProperty(this, "closeBtn", _descriptor2, this);

          _initializerDefineProperty(this, "titleLabel", _descriptor3, this);

          _defineProperty(this, "touchEnable", true);
        }

        onLoad() {
          this.node.getComponent(Widget).updateAlignment();
        }

        start() {
          this.closeBtn.on('click', () => {
            globalThis.subGamePlayShotAudio('subGameClick');
            this.disappearViewAnima();
          }, this);
        }

        onView(path, title) {
          this.titleLabel.string = title;
          var bundle = assetManager.getBundle(globalThis.currentPlayingGameID);

          if (bundle) {
            bundle.loadDir(path, SpriteFrame, (err, assets) => {
              // console.log(assets);
              if (assets && Array.isArray(assets) && assets.length > 0) {
                var dic = assets.sort((a, b) => {
                  var indexs1 = a.name.split('_');
                  var indexs2 = b.name.split('_');

                  if (indexs1 && indexs2) {
                    var num1 = parseInt(indexs1[indexs1.length - 1]);
                    var num2 = parseInt(indexs2[indexs2.length - 1]);

                    if (!isNaN(num1) && !isNaN(num2)) {
                      return num1 - num2;
                    }
                  }

                  return 0;
                });

                for (var i = 0; i < dic.length; i++) {
                  var asset = dic[i];
                  var img = instantiate(this.imgCopyNode);
                  img.setParent(this.imgCopyNode.parent, false);
                  img.active = true;
                  img.getComponent(Sprite).spriteFrame = asset;
                }
              }
            });
          }
        }

        appearViewAnima() {
          var viewUITransform = this.node.getComponent(UITransform);
          var mainNode = this.node.getChildByName('main').getComponent(UITransform);
          mainNode.contentSize = viewUITransform.contentSize;
          mainNode.node.position = v3(0, -viewUITransform.contentSize.height / 2, 0);
          tween(mainNode.node).to(0.3, {
            position: v3(0, viewUITransform.contentSize.height / 2, 0)
          }, {
            easing: 'sineOut'
          }).call(() => {}).start();
        }

        disappearViewAnima() {
          if (this.touchEnable) {
            this.touchEnable = false;
            var viewUITransform = this.node.getComponent(UITransform);
            var mainNode = this.node.getChildByName('main').getComponent(UITransform);
            tween(mainNode.node).to(0.3, {
              position: v3(0, -viewUITransform.contentSize.height / 2, 0)
            }, {
              easing: 'sineOut'
            }).call(() => {
              this.touchEnable = true;
              this.node.active = false;

              for (var i = 1; i < this.imgCopyNode.parent.children.length; i++) {
                var element = this.imgCopyNode.parent.children[i];
                element.destroy();
              }

              director.emit('slotDisappearView', 'UIImgView');
            }).start();
          }
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "imgCopyNode", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "closeBtn", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "titleLabel", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=UIImgView.js.map
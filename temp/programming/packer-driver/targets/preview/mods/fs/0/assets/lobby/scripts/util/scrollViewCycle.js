System.register(["cc"], function (_export, _context) {
  "use strict";

  var _cclegacy, _decorator, Component, Node, Widget, UITransform, instantiate, v3, _dec, _dec2, _dec3, _class, _class2, _descriptor, _descriptor2, _descriptor3, _temp, _crd, ccclass, property, ScrollViewCycle;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  return {
    setters: [function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      Component = _cc.Component;
      Node = _cc.Node;
      Widget = _cc.Widget;
      UITransform = _cc.UITransform;
      instantiate = _cc.instantiate;
      v3 = _cc.v3;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "691d8WGBR9CWqheBmqupjUr", "scrollViewCycle", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("ScrollViewCycle", ScrollViewCycle = (_dec = ccclass('ScrollViewCycle'), _dec2 = property(Node), _dec3 = property(Node), _dec(_class = (_class2 = (_temp = class ScrollViewCycle extends Component {
        constructor() {
          super(...arguments);

          _initializerDefineProperty(this, "contentNode", _descriptor, this);

          _initializerDefineProperty(this, "itemForCopy", _descriptor2, this);

          _initializerDefineProperty(this, "deltaItemName", _descriptor3, this);

          _defineProperty(this, "scrollView", void 0);

          _defineProperty(this, "itemCallBack", void 0);

          _defineProperty(this, "viewItemNumCallBack", void 0);

          _defineProperty(this, "dataSum", void 0);

          _defineProperty(this, "itemHeight", void 0);

          _defineProperty(this, "contentStartY", void 0);

          _defineProperty(this, "viewItemNum", void 0);

          _defineProperty(this, "itemNum", void 0);

          _defineProperty(this, "contentStartHeight", void 0);
        }

        onLoad() {}

        start() {
          this.node.on('scrolling', () => {
            this.updateItem();
          }, this);
          this.node.on('scroll-ended', () => {
            if (this.contentNode.position.y >= this.contentStartY) {
              var contentY = this.contentNode.position.y - this.contentStartY;
              var startIndex = Math.floor(contentY / this.itemHeight);
              this.contentNode.getComponent(UITransform).height = this.contentStartHeight + startIndex * this.itemHeight;
            }
          }, this);
        }

        setConfig() {
          var _widget = this.node.getComponentsInChildren(Widget);

          if (_widget) {
            for (var i = 0; i < _widget.length; i++) {
              _widget[i].updateAlignment();

              _widget[i].enabled = false;
            }
          }

          this.scrollView = null;
          this.itemCallBack = null;
          this.viewItemNumCallBack = null;
          this.dataSum = 0;
          this.contentStartY = this.contentNode.position.y;
          var copyItem = null;

          if (this.itemForCopy) {
            copyItem = this.itemForCopy;
          } else {
            copyItem = this.contentNode.children[0];
          }

          this.itemHeight = copyItem.getComponent(UITransform).height;
          this.viewItemNum = this.node.getChildByName('view').getComponent(UITransform).height / this.itemHeight;
          var creatNum = Math.floor(this.node.getComponent(UITransform).height / this.itemHeight) + this.deltaItemName;

          for (var _i = 0; _i < creatNum; _i++) {
            var item = instantiate(copyItem);
            item.parent = this.contentNode;
            item.position = v3(0, -this.itemHeight / 2 + -this.itemHeight * (_i + 1), 0);
          }

          this.itemNum = this.contentNode.children.length;
          this.contentStartHeight = (creatNum + 1) * this.itemHeight;
        }

        init(num, callBack) {
          this.setConfig();
          this.dataSum = num;
          this.itemCallBack = callBack;

          for (var i = 0; i < this.contentNode.children.length; i++) {
            if (i < this.dataSum) {
              this.contentNode.children[i].active = true;

              if (this.itemCallBack) {
                this.itemCallBack(this.contentNode.children[i], i);
              }
            } else {
              this.contentNode.children[i].active = false;
            }
          }

          this.contentNode.getComponent(UITransform).height = this.dataSum * this.itemHeight;
          this.updateItem();
        }

        changeHeight(num) {
          this.dataSum = num;

          for (var i = 0; i < this.contentNode.children.length; i++) {
            if (i < this.dataSum) {
              this.contentNode.children[i].active = true;
            } else {
              this.contentNode.children[i].active = false;
            }
          }

          this.contentNode.getComponent(UITransform).height = this.dataSum * this.itemHeight;
          console.log('--------------- change height int', this.contentNode.getComponent(UITransform).height, this.dataSum);
          this.updateItem();
        }

        resetContentY() {
          this.contentNode.position = v3(0, this.contentStartY, 0);
          this.updateItem();
        }

        onViewItemNumCallBack(callback) {
          this.viewItemNumCallBack = callback;
        }

        updateItem() {
          // console.log('this.contentNode.position.y:'+this.contentNode.position.y);
          if (this.contentNode.position.y >= this.contentStartY) {
            var contentY = this.contentNode.position.y - this.contentStartY;
            var startIndex = Math.floor(contentY / this.itemHeight);

            if (startIndex <= this.dataSum - this.itemNum) {
              this.contentNode.getComponent(UITransform).height = this.contentStartHeight + startIndex * this.itemHeight;
            } else {
              startIndex = this.dataSum - this.itemNum;
              this.contentNode.getComponent(UITransform).height = this.contentStartHeight + (this.dataSum - this.itemNum) * this.itemHeight;
            }

            var startIndexClamp = this.clamp(startIndex, this.contentNode.children.length);

            if (this.viewItemNumCallBack) {
              this.viewItemNumCallBack(startIndex + this.viewItemNum);
            }

            var i = 0;
            var index = startIndexClamp;
            var modul = startIndex; //    console.log('startIndex:'+startIndex);

            while (i < this.contentNode.children.length) {
              var item = this.contentNode.children[index];

              if (startIndex <= this.dataSum - this.itemNum) {
                item.position = v3(0, -this.itemHeight / 2 + -this.itemHeight * modul, 0);
              }

              if (this.itemCallBack) {
                this.itemCallBack(item, startIndex + i);
              }

              index++;

              if (index >= this.contentNode.children.length) {
                index = 0; // item.active = false;
              }

              modul++;
              i++;
            }
          }
        }

        clamp(value, length) {
          var count = Math.floor(value / length);
          return value - count * length;
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "contentNode", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "itemForCopy", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "deltaItemName", [property], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return 5;
        }
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=scrollViewCycle.js.map
System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2", "__unresolved_3", "__unresolved_4", "__unresolved_5", "__unresolved_6"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, director, Enum, macro, sAudioMgr, sBoxMgr, sComponent, sConfigMgr, sGameMgrBase, sUtil, _dec, _dec2, _dec3, _dec4, _dec5, _dec6, _dec7, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _descriptor5, _descriptor6, _descriptor7, _temp, _crd, ccclass, property, RollModeType, sGameFlowBase;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfsAudioMgr(extras) {
    _reporterNs.report("sAudioMgr", "./sAudioMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsBoxMgr(extras) {
    _reporterNs.report("sBoxMgr", "./sBoxMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsComponent(extras) {
    _reporterNs.report("sComponent", "./sComponent", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsConfigMgr(extras) {
    _reporterNs.report("sConfigMgr", "./sConfigMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsGameMgrBase(extras) {
    _reporterNs.report("sGameMgrBase", "./sGameMgrBase", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsUtil(extras) {
    _reporterNs.report("sUtil", "./sUtil", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      director = _cc.director;
      Enum = _cc.Enum;
      macro = _cc.macro;
    }, function (_unresolved_2) {
      sAudioMgr = _unresolved_2.default;
    }, function (_unresolved_3) {
      sBoxMgr = _unresolved_3.sBoxMgr;
    }, function (_unresolved_4) {
      sComponent = _unresolved_4.sComponent;
    }, function (_unresolved_5) {
      sConfigMgr = _unresolved_5.sConfigMgr;
    }, function (_unresolved_6) {
      sGameMgrBase = _unresolved_6.sGameMgrBase;
    }, function (_unresolved_7) {
      sUtil = _unresolved_7.sUtil;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "10cff+I4xxDxbUOKV2eJ8vw", "sGameFlowBase", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      (function (RollModeType) {
        RollModeType[RollModeType["ByWay"] = 0] = "ByWay";
        RollModeType[RollModeType["ByLine"] = 1] = "ByLine";
        RollModeType[RollModeType["ByCount"] = 2] = "ByCount";
      })(RollModeType || (RollModeType = {}));

      Enum(RollModeType);

      _export("sGameFlowBase", sGameFlowBase = (_dec = ccclass('sGameFlowBase'), _dec2 = property({
        tooltip: '普通模式下停止流程的前置等待时间',
        min: 0.1,
        max: 5
      }), _dec3 = property({
        tooltip: 'speedAnima那一列的停止流程的前置等待时间',
        min: 0,
        max: 5
      }), _dec4 = property({
        tooltip: 'speedAnima那一列的开始停止的等待时间',
        min: 0.1,
        max: 5
      }), _dec5 = property({
        tooltip: 'speedAnima单列触发scatter缓慢下降的减速速率',
        min: -10,
        max: 0
      }), _dec6 = property({
        tooltip: 'speedAnima单列触发scatter缓慢下降的最低速度(0是停止,1相当于是100%,0.5等于是原来速度的50%)',
        min: 0,
        max: 1
      }), _dec7 = property({
        type: RollModeType
      }), _dec(_class = (_class2 = (_temp = class sGameFlowBase extends (_crd && sComponent === void 0 ? (_reportPossibleCrUseOfsComponent({
        error: Error()
      }), sComponent) : sComponent) {
        constructor() {
          super(...arguments);

          _initializerDefineProperty(this, "normalRollWaitTime", _descriptor, this);

          _initializerDefineProperty(this, "speedAnimaOneRoundWaitTime", _descriptor2, this);

          _initializerDefineProperty(this, "speedAnimaCoolDownWaitTime", _descriptor3, this);

          _initializerDefineProperty(this, "speedAnimaFriction", _descriptor4, this);

          _initializerDefineProperty(this, "speedAnimaSpeedMin", _descriptor5, this);

          _initializerDefineProperty(this, "gameFlowType", _descriptor6, this);

          _initializerDefineProperty(this, "rollModeType", _descriptor7, this);

          _defineProperty(this, "roundShowBoxLineInterval", null);

          _defineProperty(this, "roundShowBoxLineTimeout", null);

          _defineProperty(this, "clickMode", 'normal');
        }

        onLoad() {
          director.on('betBtnClick', () => {
            this.clearUI();
          }, this);
          director.on('backToHall', () => {
            this.clearUI();
          }, this);
        }

        start() {
          if (this.gameFlowType) {
            (_crd && sGameMgrBase === void 0 ? (_reportPossibleCrUseOfsGameMgrBase({
              error: Error()
            }), sGameMgrBase) : sGameMgrBase).instance.RegisterGameFlowBase(this.gameFlowType, this);
          }
        }

        clearUI() {
          if (this.roundShowBoxLineInterval) {
            this.unschedule(this.roundShowBoxLineInterval);
            this.roundShowBoxLineInterval = null;
          }

          if (this.roundShowBoxLineTimeout) {
            this.unschedule(this.roundShowBoxLineTimeout);
            this.roundShowBoxLineTimeout = null;
          }
        }
        /**
         * @zh
         * 格子旋转后的最初方法，用该方法控制所有格子的停下
         * @param rollSpeedMode - 滚动类型 normal,turbo
         * @param clickMode - 点击类型 normal,autoBet
         */


        flowBegin(rollSpeedMode, clickMode) {
          this.clickMode = clickMode;

          var _round = (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
            error: Error()
          }), sConfigMgr) : sConfigMgr).instance.GetOneRoundData(0);

          if (rollSpeedMode == 'normal') {
            this.rollStopAction(_round, rollSpeedMode, clickMode);
          } else if (rollSpeedMode == 'turbo') {
            this.rollStopTurboAction(_round, rollSpeedMode, clickMode);
          }
        } //user click bet button when rolling


        flowStopShutDown() {}
        /**
         * @zh
         * 普通旋转触发滚动停下方法
         * @param round - 当前round数据
         * @param rollSpeedMode - 滚动类型 normal,turbo
         * @param clickMode - 点击类型 normal,autoBet
         */


        rollStopAction(round, rollSpeedMode, clickMode) {
          if (round) {
            this.scheduleOnce(() => {
              (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
                error: Error()
              }), sConfigMgr) : sConfigMgr).instance.SetResData(round['round_symbol_map']);
              (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                error: Error()
              }), sUtil) : sUtil).once('rollStopOneRoundCall', () => {
                this.rollShowResAction(round);
              });
              (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                error: Error()
              }), sBoxMgr) : sBoxMgr).instance.rollStopAllColumn();
            }, 1.9);
          }
        }
        /**
         * @zh
         * 加速旋转触发滚动停下方法
         * @param round - 当前round数据
         * @param rollSpeedMode - 滚动类型 normal,turbo
         * @param clickMode - 点击类型 normal,autoBet
         */


        rollStopTurboAction(round, rollSpeedMode, clickMode) {
          if (round) {
            this.scheduleOnce(() => {
              (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
                error: Error()
              }), sConfigMgr) : sConfigMgr).instance.SetResData(round['round_symbol_map']);
              (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                error: Error()
              }), sUtil) : sUtil).once('rollStopOneRoundCall', () => {
                (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                  error: Error()
                }), sAudioMgr) : sAudioMgr).StopAudio();
                this.rollShowResAction(round);
              });
              (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                error: Error()
              }), sBoxMgr) : sBoxMgr).instance.rollStopAllColumn();
            }, 0.05);
          }
        }
        /**
         * @zh
         * 滚动停下后展示结果
         * @param round - 当前round数据
         */


        rollShowResAction(round) {
          if (round) {
            this.boxLightsOnAction(round);
          }
        }
        /**
         * @zh
         * 点亮所有满足条件的格子
         * @param round - 当前round数据
         */


        boxLightsOnAction(round) {
          // sBoxMgr.instance.clearEffectAllBoxArray('rest','settlement');
          if (this.rollModeType == RollModeType.ByLine) {
            this.byLineBoxLightCtr(round);
          } else if (this.rollModeType == RollModeType.ByWay) {
            this.byWayBoxLightCtr(round);
          }
        }
        /**
         * @zh
         * 用byLine的方式控制点亮所有满足条件的格子
         * @param round - 当前round数据
         */


        byLineBoxLightCtr(round) {
          var events = (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
            error: Error()
          }), sConfigMgr) : sConfigMgr).instance.GetAllEventConfigByHitList(round['hit_events_list']);

          if (events) {
            var resActObj = {};
            var eventList = [];
            var isWin = false;

            for (var i = 0; i < events.length; i++) {
              var _event = events[i];

              if (_event['event_type'] == 'boxAnima') {
                eventList.push(_event);

                var acts = _event['act_pos'].split(',');

                if (acts) {
                  isWin = true;

                  for (var a = 0; a < acts.length; a++) {
                    var act = acts[a];
                    resActObj[act] = true;
                  }
                }
              }
            }

            if (isWin) {
              (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                error: Error()
              }), sBoxMgr) : sBoxMgr).instance.blackCurtainAnima();
            } else {}

            this.byLineBoxLightOn(resActObj);
            var index = 0;

            this.roundShowBoxLineTimeout = () => {
              this.roundShowBoxLineInterval = () => {
                if (index >= eventList.length) {
                  index = 0;
                }

                var res = eventList[index++];

                if (res && res['act_pos']) {
                  var _acts = res['act_pos'].split(',');

                  if (_acts) {
                    var resSingleActObj = {};

                    for (var _a = 0; _a < _acts.length; _a++) {
                      var _act = _acts[_a];
                      resSingleActObj[_act] = true;
                    }

                    this.byLineBoxLightOn(resSingleActObj);
                  }
                }
              };

              this.schedule(this.roundShowBoxLineInterval, 2, macro.REPEAT_FOREVER);
            };

            this.scheduleOnce(this.roundShowBoxLineTimeout, 2);
          }

          (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
            error: Error()
          }), sUtil) : sUtil).once('titleWinSettleEnd', () => {
            this.titleWinSettleEnd(round);
          });
          director.emit('winBetRes', globalThis.GameBtnEntity.CurrentBetAmount * round.rate, round.rate);
        }
        /**
         * @zh
         * 用byWay的方式控制点亮所有满足条件的格子
         * @param round - 当前round数据
         */


        byWayBoxLightCtr(round, rollSpeedMode) {
          var _this = this;

          if (rollSpeedMode === void 0) {
            rollSpeedMode = '';
          }

          var events = (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
            error: Error()
          }), sConfigMgr) : sConfigMgr).instance.GetAllEventConfigByHitList(round['hit_events_list']);
          var isWin = false;

          if (events) {
            var resAct = {};

            for (var i = 0; i < events.length; i++) {
              var _event = events[i];

              if (_event['event_type'] == 'boxAnima') {
                isWin = true;
                var act = resAct[_event.bonus_symbol];

                if (!act) {
                  act = {};
                  act.bonus_symbol = _event.bonus_symbol;
                  act.act_pos = {};
                  act.rate = 0;
                }

                if (_event.rate_change) {
                  if (act.rate == 0) {
                    act.rate = _event.rate_change.num;
                  } else {
                    if (_event.rate_change.type == 0) {
                      act.rate += _event.rate_change.num;
                    } else if (_event.rate_change.type == 1) {
                      act.rate *= _event.rate_change.num;
                    }
                  }
                }

                if (_event.act_pos) {
                  var poss = _event['act_pos'].split(',');

                  if (poss) {
                    for (var a = 0; a < poss.length; a++) {
                      var value = poss[a];
                      act.act_pos[value] = true;
                    }
                  }
                }

                resAct[_event.bonus_symbol] = act;
              }
            }

            if (isWin) {
              (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                error: Error()
              }), sBoxMgr) : sBoxMgr).instance.blackCurtainAnima();
            } else {}

            var resActKeys = Object.keys(resAct);
            var allActPos = {};

            if (resActKeys) {
              for (var _i = 0; _i < resActKeys.length; _i++) {
                var element = resAct[resActKeys[_i]];
                var itemKeys = Object.keys(element.act_pos);

                for (var j = 0; j < itemKeys.length; j++) {
                  var key = itemKeys[j];
                  allActPos[key] = true;
                } // this.byWayboxResWinAction(element);

              }

              var boxActArr = [];
              var allKeys = Object.keys(allActPos);

              for (var _i2 = 0; _i2 < allKeys.length; _i2++) {
                var _element = allKeys[_i2];

                var _act2 = _element.split('_');

                if (_act2 && _act2.length == 2) {
                  var act1 = parseInt(_act2[0]);
                  var act2 = parseInt(_act2[1]);

                  if (act1 && act2 || act1 == 0 || act2 == 0) {
                    if (!boxActArr[act1]) {
                      boxActArr[act1] = [];
                    }

                    boxActArr[act1].push(act2);
                  }
                }
              }

              var _loop = function _loop(_i3) {
                var arr = boxActArr[_i3];

                _this.scheduleOnce(() => {
                  for (var _j = 0; _j < arr.length; _j++) {
                    var y = arr[_j];
                    (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                      error: Error()
                    }), sBoxMgr) : sBoxMgr).instance.updateBoxDataByXY(_i3, y, 'win', 'settlement');
                  }
                }, _i3 * 0.1 + 0.1);
              };

              for (var _i3 = 0; _i3 < boxActArr.length; _i3++) {
                _loop(_i3);
              }

              var index = 0;
              var resActkeys = Object.keys(resAct);

              if (resActkeys && resActkeys.length > 0) {
                this.roundShowBoxLineTimeout = () => {
                  this.roundShowBoxLineInterval = () => {
                    if (index >= resActKeys.length) {
                      index = 0;
                    }

                    var res = resAct[resActKeys[index++]];
                    this.byWayBoxLightOn(res);
                  };

                  this.schedule(this.roundShowBoxLineInterval, 2, macro.REPEAT_FOREVER);
                };

                this.scheduleOnce(this.roundShowBoxLineTimeout, 1);
              }

              director.emit('rollStop');
            }
          }
        }
        /**
         * @zh
         * 用byLine的方式点亮格子
         * @param resActObj - 格子集合
         */


        byLineBoxLightOn(resActObj) {
          // sBoxMgr.instance.clearEffectAllBoxArray('rest', 'settlement');
          // sBoxMgr.instance.setBoxArrayState('rest', 'settlement');
          (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
            error: Error()
          }), sBoxMgr) : sBoxMgr).instance.byLineboxResWinAction(resActObj);
        }
        /**
         * @zh
         * 用byWay的方式点亮格子
         * @param resActObj - 格子集合
         */


        byWayBoxLightOn(resActObj) {
          (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
            error: Error()
          }), sBoxMgr) : sBoxMgr).instance.clearEffectAllBoxArray('rest', 'settlement');
          (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
            error: Error()
          }), sBoxMgr) : sBoxMgr).instance.setBoxArrayState('rest', 'settlement');
          (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
            error: Error()
          }), sBoxMgr) : sBoxMgr).instance.byWayboxResWinAction(resActObj);
        }
        /**
         * @zh
         * 用byLine的方式点亮所有满足条件的格子
         * @param round - 当前round数据
         */


        titleWinSettleEnd(round) {
          director.emit('rollStop');
        }

        turboFlowDelayCall(call) {
          (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
            error: Error()
          }), sBoxMgr) : sBoxMgr).instance.wholeFlowDelayCall(0.02, call);
        }

        normalFlowDelayCall(call) {
          (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
            error: Error()
          }), sBoxMgr) : sBoxMgr).instance.wholeFlowDelayCall(this.normalRollWaitTime, call);
        }

        freeWinSpeedAnimaDelayCall(col, call) {
          (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
            error: Error()
          }), sBoxMgr) : sBoxMgr).instance.singleColumnDelayCall(col, this.speedAnimaOneRoundWaitTime, call);
        }

        setMotionTweenSpeedByIndex(col) {
          (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
            error: Error()
          }), sBoxMgr) : sBoxMgr).instance.setMotionTweenSpeedByIndex(col, this.speedAnimaFriction, this.speedAnimaSpeedMin, this.speedAnimaCoolDownWaitTime);
        }

        flowDelayCall(call, waitTime) {
          (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
            error: Error()
          }), sBoxMgr) : sBoxMgr).instance.flowDelayCall(waitTime, call);
        }

        countSymbolByData(arrs, call) {
          var count = 0;
          var itemSum = (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
            error: Error()
          }), sBoxMgr) : sBoxMgr).instance.boxSum;
          var viewItemSum = (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
            error: Error()
          }), sBoxMgr) : sBoxMgr).instance.boxViewSum;
          var viewY = itemSum.y - viewItemSum.y;

          if (arrs && Array.isArray(arrs) && call && viewItemSum) {
            for (var x = 0; x < arrs.length; x++) {
              var arrsCol = arrs[x];

              if (arrsCol && Array.isArray(arrsCol)) {
                for (var y = viewY / 2; y < viewItemSum.y + viewY / 2; y++) {
                  if (y >= 0 && y < arrsCol.length) {
                    var symbol = arrsCol[y];

                    if (call(symbol)) {
                      count++;
                    }
                  }
                }
              }
            }
          }

          return count;
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "normalRollWaitTime", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return 0.1;
        }
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "speedAnimaOneRoundWaitTime", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return 1.1;
        }
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "speedAnimaCoolDownWaitTime", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return 0.6;
        }
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "speedAnimaFriction", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return -0.5;
        }
      }), _descriptor5 = _applyDecoratedDescriptor(_class2.prototype, "speedAnimaSpeedMin", [_dec6], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return 0.15;
        }
      }), _descriptor6 = _applyDecoratedDescriptor(_class2.prototype, "gameFlowType", [property], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return '';
        }
      }), _descriptor7 = _applyDecoratedDescriptor(_class2.prototype, "rollModeType", [_dec7], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return RollModeType.ByLine;
        }
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=sGameFlowBase.js.map
System.register(["__unresolved_0", "cc", "__unresolved_1"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, Component, Node, UITransform, instantiate, NodePool, gameRecordImgItem, _dec, _dec2, _dec3, _class, _class2, _descriptor, _descriptor2, _temp, _crd, ccclass, property, gameRecordImgFollow;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfgameRecordImgItem(extras) {
    _reporterNs.report("gameRecordImgItem", "./gameRecordImgItem", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      Component = _cc.Component;
      Node = _cc.Node;
      UITransform = _cc.UITransform;
      instantiate = _cc.instantiate;
      NodePool = _cc.NodePool;
    }, function (_unresolved_2) {
      gameRecordImgItem = _unresolved_2.gameRecordImgItem;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "0a839CZv9JHtLvcZbmbfJ/R", "gameRecordImgFollow", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("gameRecordImgFollow", gameRecordImgFollow = (_dec = ccclass('gameRecordImgFollow'), _dec2 = property(Node), _dec3 = property(Node), _dec(_class = (_class2 = (_temp = class gameRecordImgFollow extends Component {
        constructor() {
          super(...arguments);

          _initializerDefineProperty(this, "followNode", _descriptor, this);

          _initializerDefineProperty(this, "item", _descriptor2, this);

          _defineProperty(this, "itemPool", null);

          _defineProperty(this, "itemMap", {});
        }

        init() {
          this.itemPool = new NodePool();
          this.node.getComponent(UITransform).contentSize = this.followNode.getComponent(UITransform).contentSize;
          this.node.getComponent(UITransform).anchorPoint = this.followNode.getComponent(UITransform).anchorPoint;
          this.node.position = this.followNode.position;
        }

        creatItem() {
          var item = null;

          if (this.itemPool.size() > 0) {
            item = this.itemPool.get();
          } else {
            item = instantiate(this.item);
          }

          item.active = true;
          item.parent = this.node;
          return item;
        }

        recycleItem(item) {
          item.active = false;
          this.itemPool.put(item);
        }

        clear() {
          for (var key in this.itemMap) {
            this.recycleItem(this.itemMap[key]);
          }

          this.itemMap = {};
        }

        updateItem(item, data, index, gameId, recordType, tableId) {
          if (recordType === void 0) {
            recordType = 'solt';
          }

          if (!this.itemPool) {
            this.init();
          }

          var copyItem = null;

          if (this.itemMap[item.uuid]) {
            copyItem = this.itemMap[item.uuid];
            this.itemMap[item.uuid].position = item.position;
          } else {
            copyItem = this.creatItem();
            this.itemMap[item.uuid] = copyItem;
            this.itemMap[item.uuid].position = item.position;
          }

          copyItem.active = true;
          var profit = data.profit;

          if (recordType == 'poker') {
            profit = data.coin_change;
          }

          copyItem.getComponent(_crd && gameRecordImgItem === void 0 ? (_reportPossibleCrUseOfgameRecordImgItem({
            error: Error()
          }), gameRecordImgItem) : gameRecordImgItem).init(index, gameId, data.id, recordType, profit, tableId, data.round_id);
          this.node.getComponent(UITransform).contentSize = this.followNode.getComponent(UITransform).contentSize;
          this.node.getComponent(UITransform).anchorPoint = this.followNode.getComponent(UITransform).anchorPoint;
          this.node.position = this.followNode.position;
        }

        update() {
          if (this.node.position.x != this.followNode.position.x || this.node.position.y != this.followNode.position.y) {
            this.node.position = this.followNode.position;
          }
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "followNode", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "item", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=gameRecordImgFollow.js.map
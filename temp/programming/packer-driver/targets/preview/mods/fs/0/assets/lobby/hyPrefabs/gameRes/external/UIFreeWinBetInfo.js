System.register(["cc"], function (_export, _context) {
  "use strict";

  var _cclegacy, _decorator, Component, Label, v3, tween, director, _dec, _dec2, _dec3, _dec4, _class, _class2, _descriptor, _descriptor2, _descriptor3, _temp, _crd, ccclass, property, UIFreeWinBetInfo;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  return {
    setters: [function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      Component = _cc.Component;
      Label = _cc.Label;
      v3 = _cc.v3;
      tween = _cc.tween;
      director = _cc.director;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "4bb07vX1etDBrqtXGM/4a9f", "UIFreeWinBetInfo", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("UIFreeWinBetInfo", UIFreeWinBetInfo = (_dec = ccclass('UIFreeWinBetInfo'), _dec2 = property(Label), _dec3 = property(Label), _dec4 = property(Label), _dec(_class = (_class2 = (_temp = class UIFreeWinBetInfo extends Component {
        constructor() {
          super(...arguments);

          _initializerDefineProperty(this, "ownLabel", _descriptor, this);

          _initializerDefineProperty(this, "betLabel", _descriptor2, this);

          _initializerDefineProperty(this, "winLabel", _descriptor3, this);

          _defineProperty(this, "firstRate", 0);

          _defineProperty(this, "betAmount", 0);

          _defineProperty(this, "lineAmount", 0);

          _defineProperty(this, "userCoin", 0);

          _defineProperty(this, "winTotal", 0);
        }

        start() {
          var country = '';

          if (globalThis.getCountry) {
            var con = globalThis.getCountry();

            if (con) {
              country = con;
            }
          }

          if (country == 'VNM') {
            if (this.betLabel) {
              var vnIcon = this.betLabel.node.parent.getChildByName('vnIcon');

              if (vnIcon) {
                vnIcon.active = true;
              }
            }
          } else {
            if (this.betLabel) {
              var icon = this.betLabel.node.parent.getChildByName('icon');

              if (icon) {
                icon.active = true;
              }
            }
          }

          director.on('freeWinBetShowCoin', rate => {
            this.winTotal += rate * this.betAmount;
            this.userCoin += rate * this.betAmount; // console.log('freeWinBetShowCoin:'+rate+',,,'+this.userCoin);

            if (rate > 0) {
              this.betUserInfoUpdateAnima(this.userCoin, null, this.winTotal);
            }
          }, this);
        }

        onEnable() {
          this.lineAmount = globalThis.GameBtnEntity.lineAmount;
          this.betAmount = globalThis.GameBtnEntity.CurrentBetAmount;
          this.userCoin = globalThis.GameBtnEntity.beforeUserCoin;
          console.log('this.firstRate:' + this.firstRate);
          this.winTotal = this.firstRate > 0 ? this.firstRate * this.betAmount : 0;
          this.userCoin += this.winTotal;
          var mUserInfo = globalThis.getUserInfo();

          if (mUserInfo) {
            this.betUserInfoUpdate(this.userCoin, this.betAmount * this.lineAmount, this.winTotal);
          }
        }

        betUserInfoUpdate(own, bet, win) {
          if (own || own == 0) {
            this.ownLabel.node['rollAnimaValue'] = own;
            this.ownLabel.string = this.AddCommas(own);
          }

          if (bet || bet == 0) {
            this.betLabel.node['rollAnimaValue'] = bet;
            this.betLabel.string = this.AddCommas(bet);
          }

          if (win || win == 0) {
            this.winLabel.node['rollAnimaValue'] = win;
            this.winLabel.string = this.AddCommas(win);
          }
        }

        updateRate(rate) {
          this.firstRate = rate;
        }

        betUserInfoUpdateAnima(own, bet, win, animaTime, callBack) {
          if (animaTime === void 0) {
            animaTime = 0.2;
          }

          if (callBack === void 0) {
            callBack = null;
          }

          if (own || own == 0) {
            this.CommasLabelAnima(this.ownLabel, own, animaTime);
          }

          if (bet || bet == 0) {
            this.CommasLabelAnima(this.betLabel, bet, animaTime);
          }

          if (win || win == 0) {
            this.CommasLabelAnima(this.winLabel, win, animaTime);
          }

          if (callBack != null) {
            this.scheduleOnce(callBack, animaTime);
          }
        }

        CommasLabelAnima(label, value, time) {
          if (label && (label.node['rollAnimaValue'] || label.node['rollAnimaValue'] == 0)) {
            var valueOri = label.node['rollAnimaValue'];
            var mTween = label.node['rollAnimaTween'];

            if (mTween) {
              mTween.stop();
            }

            var tweenTargetVec3 = v3(valueOri, valueOri, valueOri); // console.log('tweenTargetVec3:'+tweenTargetVec3);

            label.node['rollAnimaTween'] = tween(tweenTargetVec3).to(time, v3(value, value, value), {
              "onUpdate": target => {
                if (label) {
                  label.node['rollAnimaValue'] = Math.floor(target.x);
                  label.string = this.AddCommas(Math.floor(target.x));
                }
              },
              easing: 'quadOut'
            }).call(() => {
              if (label) {
                label.node['rollAnimaValue'] = Math.floor(value);
                label.string = this.AddCommas(Math.floor(value));
              }
            }).start();
          }
        }

        AddCommas(money) {
          if (!money) {
            return "0";
          } else {
            var mon;

            if (typeof money == "string") {
              mon = Number(money);
            } else {
              mon = money;
            }

            var coinRate = 1;

            if (globalThis.getCoinRate) {
              coinRate = globalThis.getCoinRate();
            }

            var byte = coinRate.toString().length - 1;
            ;
            var showMoney = (mon / coinRate).toFixed(byte);
            var tempmoney = String(showMoney);
            var left = tempmoney.split('.')[0],
                right = tempmoney.split('.')[1];
            right = right ? right.length >= byte ? '.' + right.substring(0, byte) : '.' + right : '';
            var temp = left.split('').reverse().join('').match(/(\d{1,3})/g);

            if (temp) {
              return (Number(tempmoney) < 0 ? "-" : "") + temp.join(',').split('').reverse().join('') + right;
            } else {
              return '';
            }
          }
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "ownLabel", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "betLabel", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "winLabel", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=UIFreeWinBetInfo.js.map
System.register(["cc"], function (_export, _context) {
  "use strict";

  var _cclegacy, sObjPool, _crd;

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  _export("sObjPool", void 0);

  return {
    setters: [function (_cc) {
      _cclegacy = _cc.cclegacy;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "8e74fnsjeROurx7g+YhKpqW", "sObjPool", undefined);

      _export("sObjPool", sObjPool = class sObjPool {
        /**
         * 注册一个可以用作对象池的对象创建回调
         * @param key 名字
         * @param copy 这是一个回调 自定义这个对象的创建方法，return一个node 或者别的结构的对象即可
         */
        static SetObj(key, copy) {
          sObjPool.objsCopy[key] = copy;
        }

        static GetObjCall(key) {
          return sObjPool.objsCopy[key];
        }

        static Dequeue(key) {
          var arr = sObjPool.objs[key];

          if (arr && Array.isArray(arr)) {
            if (arr.length > 0) {
              // if(key == 'effectItem')
              //     console.log('Dequeue count:'+arr.length);
              return arr.pop();
            }
          } else {
            sObjPool.objs[key] = [];
          }

          var obj = sObjPool.objsCopy[key];

          if (obj) {
            // console.log('instance:'+key);
            return obj();
          }

          return null;
        }

        static Enqueue(key, item) {
          var arr = sObjPool.objs[key];

          if (arr && Array.isArray(arr)) {
            arr.push(item); // if(key == 'effectItem')
            //     console.log('Enqueue count:'+arr.length);
          } else {
            sObjPool.objs[key] = [];
            sObjPool.objs[key].push(item);
          }
        }

        static Count(key) {
          var arr = sObjPool.objs[key];

          if (arr && Array.isArray(arr)) {
            return arr.length;
          }

          return -1;
        }

        static Clear() {
          sObjPool.objsCopy = {};
          sObjPool.objs = {};
        }

        static PrintObjInfo() {
          for (var item in sObjPool.objs) {
            console.log(item, ' : ', sObjPool.objs[item].length);
          }
        }

      });

      _defineProperty(sObjPool, "objsCopy", {});

      _defineProperty(sObjPool, "objs", {});

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=sObjPool.js.map
System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2", "__unresolved_3", "__unresolved_4", "__unresolved_5", "__unresolved_6", "__unresolved_7"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, Animation, AudioClip, Button, color, Component, director, Input, input, JsonAsset, KeyCode, Label, Node, Renderable2D, sys, tween, UIOpacity, v3, Vec3, Widget, _decorator, macro, view, Prefab, instantiate, UISelectBet, UIAutoSpin, UIImgView, UIFreeWinBuy, btnInternationalManager, sUtil, sFreeWinBuyView, _dec, _dec2, _dec3, _dec4, _dec5, _dec6, _dec7, _dec8, _dec9, _dec10, _dec11, _dec12, _dec13, _dec14, _dec15, _dec16, _dec17, _dec18, _dec19, _dec20, _dec21, _dec22, _dec23, _dec24, _dec25, _dec26, _dec27, _dec28, _dec29, _dec30, _dec31, _dec32, _dec33, _dec34, _dec35, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _descriptor5, _descriptor6, _descriptor7, _descriptor8, _descriptor9, _descriptor10, _descriptor11, _descriptor12, _descriptor13, _descriptor14, _descriptor15, _descriptor16, _descriptor17, _descriptor18, _descriptor19, _descriptor20, _descriptor21, _descriptor22, _descriptor23, _descriptor24, _descriptor25, _descriptor26, _descriptor27, _descriptor28, _descriptor29, _descriptor30, _descriptor31, _descriptor32, _descriptor33, _descriptor34, _class3, _temp, _crd, ccclass, property, sGameEntity;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfUISelectBet(extras) {
    _reporterNs.report("UISelectBet", "./UISelectBet", _context.meta, extras);
  }

  function _reportPossibleCrUseOfUIAutoSpin(extras) {
    _reporterNs.report("UIAutoSpin", "./UIAutoSpin", _context.meta, extras);
  }

  function _reportPossibleCrUseOfUIImgView(extras) {
    _reporterNs.report("UIImgView", "./UIImgView", _context.meta, extras);
  }

  function _reportPossibleCrUseOfUIFreeWinBuy(extras) {
    _reporterNs.report("UIFreeWinBuy", "./UIFreeWinBuy", _context.meta, extras);
  }

  function _reportPossibleCrUseOfbtnInternationalManager(extras) {
    _reporterNs.report("btnInternationalManager", "./btnInternationalManager", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsUtil(extras) {
    _reporterNs.report("sUtil", "../../../game/core/sUtil", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsFreeWinBuyView(extras) {
    _reporterNs.report("sFreeWinBuyView", "../../../game/core/sFreeWinBuyView", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      Animation = _cc.Animation;
      AudioClip = _cc.AudioClip;
      Button = _cc.Button;
      color = _cc.color;
      Component = _cc.Component;
      director = _cc.director;
      Input = _cc.Input;
      input = _cc.input;
      JsonAsset = _cc.JsonAsset;
      KeyCode = _cc.KeyCode;
      Label = _cc.Label;
      Node = _cc.Node;
      Renderable2D = _cc.Renderable2D;
      sys = _cc.sys;
      tween = _cc.tween;
      UIOpacity = _cc.UIOpacity;
      v3 = _cc.v3;
      Vec3 = _cc.Vec3;
      Widget = _cc.Widget;
      _decorator = _cc._decorator;
      macro = _cc.macro;
      view = _cc.view;
      Prefab = _cc.Prefab;
      instantiate = _cc.instantiate;
    }, function (_unresolved_2) {
      UISelectBet = _unresolved_2.UISelectBet;
    }, function (_unresolved_3) {
      UIAutoSpin = _unresolved_3.UIAutoSpin;
    }, function (_unresolved_4) {
      UIImgView = _unresolved_4.UIImgView;
    }, function (_unresolved_5) {
      UIFreeWinBuy = _unresolved_5.UIFreeWinBuy;
    }, function (_unresolved_6) {
      btnInternationalManager = _unresolved_6.btnInternationalManager;
    }, function (_unresolved_7) {
      sUtil = _unresolved_7.sUtil;
    }, function (_unresolved_8) {
      sFreeWinBuyView = _unresolved_8.sFreeWinBuyView;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "7bb61howVdN0K+/HXh2Bilr", "sGameEntity", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("sGameEntity", sGameEntity = (_dec = ccclass('sGameEntity'), _dec2 = property({
        tooltip: '多语言配置文件',
        type: [JsonAsset]
      }), _dec3 = property({
        tooltip: '中奖数字动画时间',
        type: Number
      }), _dec4 = property(Label), _dec5 = property(Label), _dec6 = property(Label), _dec7 = property(Label), _dec8 = property(Button), _dec9 = property(Button), _dec10 = property(Button), _dec11 = property(Button), _dec12 = property(Button), _dec13 = property(Button), _dec14 = property(Button), _dec15 = property(Button), _dec16 = property(Button), _dec17 = property(Button), _dec18 = property(Button), _dec19 = property(Button), _dec20 = property(Button), _dec21 = property(Button), _dec22 = property(Button), _dec23 = property(UIOpacity), _dec24 = property(UIOpacity), _dec25 = property(UIOpacity), _dec26 = property(UIOpacity), _dec27 = property(_crd && UISelectBet === void 0 ? (_reportPossibleCrUseOfUISelectBet({
        error: Error()
      }), UISelectBet) : UISelectBet), _dec28 = property(_crd && UIAutoSpin === void 0 ? (_reportPossibleCrUseOfUIAutoSpin({
        error: Error()
      }), UIAutoSpin) : UIAutoSpin), _dec29 = property(_crd && UIImgView === void 0 ? (_reportPossibleCrUseOfUIImgView({
        error: Error()
      }), UIImgView) : UIImgView), _dec30 = property(Node), _dec31 = property(Node), _dec32 = property(AudioClip), _dec33 = property([Node]), _dec34 = property([Node]), _dec35 = property([Button]), _dec(_class = (_class2 = (_temp = _class3 = class sGameEntity extends Component {
        constructor() {
          super(...arguments);

          _initializerDefineProperty(this, "languageConfigs", _descriptor, this);

          _initializerDefineProperty(this, "winLabelAnimaTime", _descriptor2, this);

          _initializerDefineProperty(this, "ownLabel", _descriptor3, this);

          _initializerDefineProperty(this, "betLabel", _descriptor4, this);

          _initializerDefineProperty(this, "winLabel", _descriptor5, this);

          _initializerDefineProperty(this, "verLabel", _descriptor6, this);

          _initializerDefineProperty(this, "selectBetBtn", _descriptor7, this);

          _initializerDefineProperty(this, "winAreaBtn", _descriptor8, this);

          _initializerDefineProperty(this, "turboBtnNode", _descriptor9, this);

          _initializerDefineProperty(this, "minusBtnNode", _descriptor10, this);

          _initializerDefineProperty(this, "addBtnNode", _descriptor11, this);

          _initializerDefineProperty(this, "autoPlayBtnNode", _descriptor12, this);

          _initializerDefineProperty(this, "betBtnNode", _descriptor13, this);

          _initializerDefineProperty(this, "backToHall", _descriptor14, this);

          _initializerDefineProperty(this, "menuBtn", _descriptor15, this);

          _initializerDefineProperty(this, "quitBtn", _descriptor16, this);

          _initializerDefineProperty(this, "soundBtn", _descriptor17, this);

          _initializerDefineProperty(this, "payTableBtn", _descriptor18, this);

          _initializerDefineProperty(this, "rulesBtn", _descriptor19, this);

          _initializerDefineProperty(this, "histroyBtn", _descriptor20, this);

          _initializerDefineProperty(this, "closeBtn", _descriptor21, this);

          _initializerDefineProperty(this, "betBtns", _descriptor22, this);

          _initializerDefineProperty(this, "betInfo", _descriptor23, this);

          _initializerDefineProperty(this, "menuBtns", _descriptor24, this);

          _initializerDefineProperty(this, "recordBtns", _descriptor25, this);

          _initializerDefineProperty(this, "selectBet", _descriptor26, this);

          _initializerDefineProperty(this, "autoSpin", _descriptor27, this);

          _initializerDefineProperty(this, "imgView", _descriptor28, this);

          _initializerDefineProperty(this, "freeWinBuy", _descriptor29, this);

          _initializerDefineProperty(this, "freeWinBtn", _descriptor30, this);

          _initializerDefineProperty(this, "subGameClick", _descriptor31, this);

          _initializerDefineProperty(this, "changeColorParent", _descriptor32, this);

          _initializerDefineProperty(this, "changeColorNode", _descriptor33, this);

          _initializerDefineProperty(this, "changeColorButton", _descriptor34, this);

          _defineProperty(this, "selfFreeWinBuyView", void 0);

          _defineProperty(this, "gameid", 0);

          _defineProperty(this, "roomid", -1);

          _defineProperty(this, "clientConfig", null);

          _defineProperty(this, "betAmount", 0);

          _defineProperty(this, "currentBetAmount", 0);

          _defineProperty(this, "betType", 1);

          _defineProperty(this, "lineAmount", 0);

          _defineProperty(this, "beforeUserCoin", 0);

          _defineProperty(this, "freeBuyBetAmount", 0);

          _defineProperty(this, "freeBuyLineMultiple", 0);

          _defineProperty(this, "betAmountBeforeRecord", 0);

          _defineProperty(this, "freewinBuyCoin", 0);

          _defineProperty(this, "maxAmount", 0);

          _defineProperty(this, "minAmount", 100000000);

          _defineProperty(this, "touchEnable", false);

          _defineProperty(this, "audioTouchEnable", true);

          _defineProperty(this, "coinHasChanged", false);

          _defineProperty(this, "changeCoin", 0);

          _defineProperty(this, "userOwnCoin", 0);

          _defineProperty(this, "userWinRate", 0);

          _defineProperty(this, "autoBetCount", -2);

          _defineProperty(this, "recordSaveCount", 50);

          _defineProperty(this, "recordSaveRes", null);

          _defineProperty(this, "recordSaveReq", null);

          _defineProperty(this, "currentRecordData", null);

          _defineProperty(this, "oneBetEndResCall", null);

          _defineProperty(this, "onAutoSpinCall", null);

          _defineProperty(this, "canBet", true);

          _defineProperty(this, "betBtnState", 'normal');

          _defineProperty(this, "betSpeedMode", 'normal');

          _defineProperty(this, "betSpeedModeTemp", 'normal');

          _defineProperty(this, "betClickMode", 'normal');

          _defineProperty(this, "gameMode", 'normal');

          _defineProperty(this, "viewMode", 'play');

          _defineProperty(this, "recordState", 'idle');

          _defineProperty(this, "recordBtnState", 'pause');

          _defineProperty(this, "menuState", 'bet');

          _defineProperty(this, "viewPos", 'main');

          _defineProperty(this, "recordBtnTouchEnable", true);

          _defineProperty(this, "oriPos", void 0);

          _defineProperty(this, "quickMode", false);

          _defineProperty(this, "lastBetTime", 0);

          _defineProperty(this, "lastRoundRate", 0);

          _defineProperty(this, "tweenList", []);

          _defineProperty(this, "tweenDic", {});

          _defineProperty(this, "scheduleDic", {});

          _defineProperty(this, "userInfo", null);

          _defineProperty(this, "coinChangeEndCoin", 0);

          _defineProperty(this, "subGameSocketReq", void 0);
        }

        //play,record
        get ViewMode() {
          return this.viewMode;
        }

        get CurrentBetAmount() {
          return this.currentBetAmount;
        }

        pushIDSchedule(call, time, id, loop, interval, delay, callByStart) {
          if (loop === void 0) {
            loop = 1;
          }

          if (interval === void 0) {
            interval = 0;
          }

          if (delay === void 0) {
            delay = 0;
          }

          if (callByStart === void 0) {
            callByStart = false;
          }

          if (call) {
            if (loop == 1) {
              this.scheduleOnce(call, time);
            } else if (loop > 1) {
              var repeat = loop;

              if (callByStart) {
                call();
                repeat -= 2;
              } else {
                repeat--;
              }

              this.schedule(call, interval, repeat, delay);
            } else if (loop == -1) {
              var _repeat = macro.REPEAT_FOREVER;

              if (callByStart) {
                call();
              }

              this.schedule(call, interval, _repeat, delay);
            }

            this.scheduleDic[id] = call;
          }
        }

        removeIDSchedule(id) {
          var t = this.scheduleDic[id];

          if (t) {
            this.unschedule(t);
            delete this.scheduleDic[id];
          }
        }

        pushOneTween(t) {
          this.tweenList.push(t);
        }

        pushIDTween(t, id) {
          this.tweenDic[id] = t;
        }

        cleanTweenList() {
          if (this.tweenList.length > 0) {
            for (var i = 0; i < this.tweenList.length; i++) {
              var t = this.tweenList[i];
              t.stop();
            }

            this.tweenList = [];
          }
        }

        cleanTweenDic() {
          var keys = Object.keys(this.tweenDic);

          if (keys && keys.length > 0) {
            for (var i = 0; i < keys.length; i++) {
              var t = this.tweenDic[keys[i]];

              if (t) {
                t.stop();
              }
            }

            this.tweenDic = {};
          }
        }

        removeIDTween(id) {
          var t = this.tweenDic[id];

          if (t) {
            t.stop();
            delete this.tweenDic[t];
          }
        }

        getUserInfo() {
          return this.userInfo;
        }

        onLoad() {
          var mUserInfo = globalThis.getUserInfo();

          if (mUserInfo) {
            this.userInfo = {
              coin: mUserInfo.coin,
              max_bet: mUserInfo.max_bet,
              uuid: mUserInfo.uuid
            };
          }

          var widgets = this.node.getComponentsInChildren(Widget);

          if (widgets) {
            for (var i = 0; i < widgets.length; i++) {
              var widget = widgets[i];
              widget.alignMode = Widget.AlignMode.ONCE;
            }
          }

          (_crd && btnInternationalManager === void 0 ? (_reportPossibleCrUseOfbtnInternationalManager({
            error: Error()
          }), btnInternationalManager) : btnInternationalManager).Clear();

          if (this.languageConfigs && this.languageConfigs.length > 0) {
            for (var _i = 0; _i < this.languageConfigs.length; _i++) {
              var lan = this.languageConfigs[_i];
              (_crd && btnInternationalManager === void 0 ? (_reportPossibleCrUseOfbtnInternationalManager({
                error: Error()
              }), btnInternationalManager) : btnInternationalManager).SetLanguegeData(lan.name, lan.json);
            }
          }

          sGameEntity.instance = this;
          globalThis.GameBtnEntity = this;
          globalThis.setSubGameAudioClip(this.subGameClick);
          director.on('slotSubGameRollBegin', () => {
            if (globalThis.slotJackpotDate) {
              globalThis.slotJackpotDate.rate = 0;
              globalThis.slotJackpotDate.jackpot_amount = 0;
              globalThis.slotJackpotDate.bingo = false;
              globalThis.slotJackpotDate.num = 0;
            }
          }, this);
          director.on('slotBetInfoViewAniam', (value, _time, clickValue) => {
            if (typeof clickValue == 'boolean') {
              if (this.selectBetBtn) {
                this.selectBetBtn.interactable = clickValue;
              }
            }

            if (this.betInfo) {
              tween(this.betInfo).to(_time, {
                opacity: value
              }).start();
            }
          }, this);
          director.on('freeWinBingo', () => {
            this.freeWinBingo();
          }, this);
          director.on('freeWinOver', () => {
            this.freeWinOver();
          }, this);
          director.on('freeWinBegain', () => {
            this.freeWinBegain();
          }, this);
          director.on('freeWinBtnInfo', rate => {
            this.freeWinBtnInfo(rate);
          }, this);
          director.on('sBoxQuickClick', () => {
            this.quickMode = true;
          }, this);
          director.on('gameRecordReplay', res => {
            if (res && res.gameId == globalThis.currentPlayingGameID) {
              director.emit('closeGameRecord', this.gameid);
              var data = this.GetGameRecordDetail(res.id);

              if (data) {
                if (!sys.isNative) {
                  console.log('gameRecordReplay data : ' + JSON.stringify(data));
                }

                this.currentRecordData = data;
                this.OpenRecordView(true);
              }
            }
          }, this);
          director.on('slotDisappearView', type => {
            this.viewPos = 'main';
          }, this); // setTimeout(()=>{
          //     director.emit('reviewBigPrize',{
          //         lastRate: 40,            //倍率
          //         lastGameMode: 1,    //模式 购买还是普通砖
          //         roundType: 'round_type0',      //普通或免费
          //         lastRoundSeq: '176',         //seq 
          //         betAmount:this.betAmount,
          //     });
          // },3000);

          director.on('reviewBigPrize', obj => {
            if (obj && this.betBtnState == 'normal' && this.gameMode == 'normal' && this.viewMode == 'play' && this.viewPos == 'main') {
              var data = {};
              data.betAmount = obj.betAmount;
              data.local = {};
              data.local.lastRate = obj.lastRate;
              data.local.lastGameMode = obj.lastGameMode;
              data.local.roundType = obj.roundType;
              data.local.lastRoundSeq = obj.lastRoundSeq;
              this.currentRecordData = data;
              this.OpenRecordView(true);
            }
          }, this);
          director.on('socketTimeout', () => {
            var tips = (_crd && btnInternationalManager === void 0 ? (_reportPossibleCrUseOfbtnInternationalManager({
              error: Error()
            }), btnInternationalManager) : btnInternationalManager).GetDataByKey('betTimeout');

            if (tips) {
              director.emit('uiTipsOpen', 'text', tips);
            }
          }, this);
          director.on('socketClosedToGame', type => {
            console.log('socketClosedToGame:' + type);
            (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
              error: Error()
            }), sUtil) : sUtil).GamePerforAnalysis('slotBetAction_socketClosedToGame', type.toString());
            this.errToOneRes();

            if (type == 1) {} else if (type == 2) {} else if (type == 3) {
              globalThis.Activerecon();
            }
          }, this);
          director.on('socketConnectToGame', type => {
            console.log('socketConnectToGame:' + type);
            (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
              error: Error()
            }), sUtil) : sUtil).GamePerforAnalysis('slotBetAction_socketConnectToGame', type.toString());
            this.errToOneRes();

            if (type == 1) {} else if (type == 2) {
              globalThis.Activerecon();
            }
          }, this);
          director.on('subGameBetMsgErr', code => {
            this.freewinBuyCoin = 0;
            console.log('subGameBetMsgErr:' + code);

            if (globalThis.GetDataByKey) {
              var tips = globalThis.GetDataByKey && globalThis.GetDataByKey("mulTiLN", "common", code);

              if (tips) {
                director.emit('uiTipsOpen', 'text', tips + 'code : ' + code);
              } else {
                director.emit('uiTipsOpen', 'text', 'bet error : ' + code);
              }
            }

            this.errToOneRes();
          }, this);
          director.on('subGameBetConfigErr', (str, res) => {
            console.log('subGameBetConfigErr:' + str);
            this.betBtnState = 'rolling';
            director.emit('uiTipsOpen', 'text', str);
            var errStr = '';

            if (this.subGameSocketReq) {
              errStr += JSON.stringify(this.subGameSocketReq) + ', ';
            }

            if (res) {
              errStr += JSON.stringify(res);
            }

            (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
              error: Error()
            }), sUtil) : sUtil).GamePerforAnalysis('slotConfigError_err', errStr);
            this.errToOneRes();
          }, this);
          director.on('slotWinBetShowCoin', (rate, userCoin) => {
            if (userCoin || userCoin == 0) {
              var winTotal = rate * this.betAmount;

              if (rate > 0) {
                this.betUserInfoUpdateAnima(userCoin, null, winTotal, this.winLabelAnimaTime);
              }
            }
          }, this);
          director.on('subGameFreeWinBuyBtnClick', () => {
            if (this.betBtnState == 'normal' && this.betClickMode == 'normal') {
              if (this.clientConfig) {
                var bet_types = this.clientConfig.bet_types;

                if (bet_types && bet_types.length > 0) {
                  for (var _i2 = 0; _i2 < bet_types.length; _i2++) {
                    var element = bet_types[_i2];

                    if (element.game_id == this.gameid) {
                      if (element.bet_amount == this.betAmount && element.game_mode == 2) {
                        var _mUserInfo = this.getUserInfo();

                        if (_mUserInfo) {
                          var tipStr = '';
                          var canBuy = true;
                          var maxBet = _mUserInfo.max_bet;
                          var userCoin = _mUserInfo.coin;
                          var sumAmount = element.bet_amount * element.bet_multiple;
                          var sum = element.bet_amount;

                          if (sumAmount > userCoin) {
                            console.log('no userCoin');
                            canBuy = false;
                            tipStr = globalThis.GetDataByKey && globalThis.GetDataByKey("mulTiLN", "common", '5001');
                          } else if (sum > maxBet) {
                            console.log('no maxBet');
                            canBuy = false;
                            tipStr = (_crd && btnInternationalManager === void 0 ? (_reportPossibleCrUseOfbtnInternationalManager({
                              error: Error()
                            }), btnInternationalManager) : btnInternationalManager).GetDataByKey('maxBetNotEnough');
                          }

                          this.freeBuyBetAmount = element.bet_amount;
                          this.freeBuyLineMultiple = element.bet_multiple;
                          console.log('subGameFreeWinBuyBtnClick');

                          if (this.selfFreeWinBuyView) {
                            this.selfFreeWinBuyView.viewInit(element.bet_amount * element.bet_multiple, element.bet_type, canBuy, tipStr);
                          } else if (this.freeWinBuy) {
                            var freeWinBuy = this.freeWinBuy.getComponent(_crd && UIFreeWinBuy === void 0 ? (_reportPossibleCrUseOfUIFreeWinBuy({
                              error: Error()
                            }), UIFreeWinBuy) : UIFreeWinBuy);

                            if (freeWinBuy) {
                              freeWinBuy.viewInit(element.bet_amount * element.bet_multiple, element.bet_type, canBuy, tipStr);
                            }
                          }
                        }

                        break;
                      }
                    }
                  }
                }
              }
            }
          }, this);
          director.on('slotBtnMoveTo', (pos, animaTime) => {
            if (pos && animaTime) {
              this.removeIDTween('slotBtnMoveTo');

              if (pos.z == -100) {
                if (this.oriPos) {
                  this.pushIDTween(tween(this.node).to(animaTime, {
                    position: this.oriPos
                  }, {
                    easing: 'sineOut'
                  }).start(), 'slotBtnMoveTo');
                }
              } else {
                this.pushIDTween(tween(this.node).to(animaTime, {
                  position: v3(pos)
                }, {
                  easing: 'sineOut'
                }).start(), 'slotBtnMoveTo');
              }
            }
          }, this);
          director.on('subGameBackToHallBtnClick', () => {
            this.touchEnable = false;
            this.unscheduleAllCallbacks();
            globalThis.subGameBackToHall();
          }, this);
          director.on('onGameCoinChange', (data, isChangeNow) => {
            if (data.reason == "award_relief") {
              this.syncUserCoinByLocal();
            }
          }, this);
          input.on(Input.EventType.KEY_DOWN, this.onKeyDown, this);
          this.gameid = this.node.parent['gameid'];

          if (globalThis.curSlotRoomId) {
            this.roomid = globalThis.curSlotRoomId;
            console.log('roomid:', this.roomid);
          }
        }

        onKeyDown(event) {
          if (sys.platform == sys.Platform.MOBILE_BROWSER || sys.platform == sys.Platform.DESKTOP_BROWSER) {
            switch (event.keyCode) {
              case KeyCode.SPACE:
                this.betBtnNode.node.emit('click');
                break;
            }
          }
        }

        errToOneRes() {
          this.freewinBuyCoin = 0;

          if (this.autoBetCount > 0 || this.autoBetCount == -1) {
            director.emit('viewChange', 'autoSpinCancel');
          }

          if (this.betBtnState == 'rolling' && this.gameMode == 'normal' && this.viewMode == 'play') {
            this.betBtnState = 'clearing';
            this.scheduleOnce(() => {
              var seq = Math.floor(Math.random() * 10);
              console.log('seq:' + seq);
              var res = {
                "route": "slot.msg.end",
                "code": 0,
                "data": {
                  "game_mode": 1,
                  "rate": 0,
                  "seq": -1,
                  "coin": 0,
                  "round_type": 0,
                  "bonus_type": "0"
                }
              };
              director.emit('oneBetEndRes', res, 'normal', 'normal');
            }, 3);
          }
        }

        start() {
          var lobbyNode = new Node('lobbyNode');
          lobbyNode.layer = this.node.layer;
          lobbyNode.parent = this.node.parent;
          lobbyNode.position = Vec3.ZERO;
          lobbyNode.setSiblingIndex(1);
          var viewSize = view.getVisibleSize(); //hongbao
          // this.scheduleOnce(()=>{
          //     let redYDelta = 100;
          //     if(viewSize.y > viewSize.x){
          //         redYDelta = 200;
          //     }
          //     globalThis.addRedPackgeCountdown && globalThis.addRedPackgeCountdown(1.5,lobbyNode,v3(viewSize.x - 100,viewSize.y - redYDelta,0));
          // },1);

          this.oriPos = v3(this.node.position);

          var _recordSaveCount = globalThis.gameRecordMgr && globalThis.gameRecordMgr.getRecordMaxSaveLength();

          if (!isNaN(_recordSaveCount) && _recordSaveCount > 0) {
            this.recordSaveCount = _recordSaveCount;
          }

          var aligmentType = this.node.parent['aligmentType'];
          var aligmentPixel = this.node.parent['aligmentPixel'];

          if (aligmentType == 'bottom') {
            var widget = this.node.getComponent(Widget);
            widget.isAlignBottom = true;

            if (aligmentPixel || aligmentPixel == 0) {
              widget.bottom = aligmentPixel;
            }

            widget.updateAlignment();
          } else if (aligmentType == 'none') {
            if (aligmentPixel) {
              this.node.position = v3(this.node.position.x, this.node.position.y + aligmentPixel, this.node.position.z);
            }
          }

          if (this.recordBtns) {
            this.recordBtns.node.worldPosition = this.betBtns.node.worldPosition;
          }

          var gameVersion = this.node.parent['gameVersion'];

          if (gameVersion && this.verLabel) {
            this.verLabel.string = gameVersion;
          }

          var winAreaClickEnable = true;
          var winAreaClickEnableValue = this.node.parent['winAreaClickEnable'];

          if (winAreaClickEnableValue == null) {} else {
            winAreaClickEnable = winAreaClickEnableValue;
          }

          var customColor = this.node.parent['customColor'];

          if (customColor) {
            if (this.changeColorButton && this.changeColorButton.length > 0) {
              for (var i = 0; i < this.changeColorButton.length; i++) {
                var btn = this.changeColorButton[i];
                btn.normalColor = color(customColor.r, customColor.g, customColor.b, btn.normalColor.a);
                btn.pressedColor = color(customColor.r, customColor.g, customColor.b, btn.pressedColor.a);
                btn.hoverColor = color(customColor.r, customColor.g, customColor.b, btn.hoverColor.a);
                btn.disabledColor = color(customColor.r, customColor.g, customColor.b, btn.disabledColor.a);
              }
            }

            if (this.changeColorNode && this.changeColorNode.length > 0) {
              for (var _i3 = 0; _i3 < this.changeColorNode.length; _i3++) {
                var element = this.changeColorNode[_i3];
                var re = element.getComponent(Renderable2D);
                re.color = customColor;
              }
            }

            if (this.changeColorParent && this.changeColorParent.length > 0) {
              for (var _i4 = 0; _i4 < this.changeColorParent.length; _i4++) {
                var _element = this.changeColorParent[_i4];

                var renders = _element.getComponentsInChildren(Renderable2D);

                if (renders && renders.length > 0) {
                  for (var j = 0; j < renders.length; j++) {
                    var _re = renders[j];
                    _re.color = customColor;
                  }
                }
              }
            }
          } // if(this.gameid == 122 || this.gameid == 115 || this.gameid == 120){
          //     this.menuBtns.node.getChildByPath('btns/histroy').active = false;
          //     this.menuBtns.node.getChildByPath('labels/histroy').active = false;
          // }


          director.on('viewChange', (type, value) => {
            if (type == 'UISelectBet') {
              if (this.selectBetBtn) {
                this.selectBetBtn.interactable = true;
              }

              this.betLabel.node.setScale(1.5, 1.5, 1.5);
              tween(this.betLabel.node).to(0.6, {
                scale: Vec3.ONE
              }, {
                easing: 'elasticOut'
              }).start();
            } else if (type == 'UIAutoSpin') {
              if ((value > 0 || value == -1) && !this.playerBetLimit()) {
                if (this.canBet) {
                  this.autoBetCount = value;

                  if (this.betBtnNode) {
                    this.betBtnNode.node.active = false;
                  }

                  this.betClickMode = 'autoBet';
                  director.emit('autoBetInfoUpdate', this.autoBetCount);

                  this.onAutoSpinCall = () => {
                    if (this.autoBetCount > 0 || this.autoBetCount == -1) {
                      if (this.autoPlayBtnNode) {
                        var animation = this.autoPlayBtnNode.getComponent(Animation);

                        if (animation) {
                          animation.play();
                        }
                      }

                      this.betAction(this.betType);
                    } else {// this.setGameBtnState('enable');
                    }
                  };

                  this.scheduleOnce(this.onAutoSpinCall, 1);
                } else {
                  globalThis.goldNotenough && globalThis.goldNotenough();
                  this.setGameBtnState('enable');
                }
              } else {
                this.setGameBtnState('enable');
              }
            } else if (type == 'autoSpinCancel') {
              if (this.onAutoSpinCall) {
                this.unschedule(this.onAutoSpinCall);
              }

              if (this.betBtnState == 'normal') {
                this.setGameBtnState('enable');
              }

              this.autoBetCount = -2;
              this.betClickMode = 'normal';

              if (this.betBtnNode) {
                this.betBtnNode.node.active = true;
              }

              director.emit('autoBetInfoUpdate', this.autoBetCount);
            }
          }, this);
          director.on('betAmountChange', (amount, line) => {
            console.log('amount:' + amount);
            console.log('line:' + line);

            if (this.clientConfig) {
              var bet_types = this.clientConfig.bet_types;

              if (bet_types && bet_types.length > 0) {
                for (var _i5 = 0; _i5 < bet_types.length; _i5++) {
                  var _element2 = bet_types[_i5];

                  if (_element2.game_id == this.gameid) {
                    if (_element2.bet_amount >= amount && _element2.bet_multiple == line) {
                      var mUserInfo = this.getUserInfo();

                      if (mUserInfo) {
                        this.betAmount = amount;
                        this.lineAmount = line;
                        this.updateBetInfo();
                        director.emit('betTypeChange', _element2.bet_type);
                        this.saveBetAmountValue();
                        director.emit('sSlotBetInfoUpdate', {
                          betAmount: this.betAmount,
                          multiple: this.lineAmount
                        });
                      }

                      break;
                    }
                  }
                }
              }
            }
          }, this);
          director.on('betUserInfoUpdate', (own, bet, win) => {
            this.betUserInfoUpdate(own, bet, win);
          }, this);
          director.on('betTypeChange', type => {
            console.log('betTypeChange:' + type);
            this.betType = type;
          }, this);
          director.on('onGameEnd', res => {
            this.userOwnCoin = res.data.coin;
            this.userWinRate = res.data.rate; // if(this.recordSaveReq && this.recordSaveRes && (this.recordSaveRes.saveIndex || this.recordSaveRes.saveIndex == 0)){
            //     this.SaveGameRecordDetail(this.recordSaveRes.saveIndex,res);
            //     if(this.freewinBuyCoin > 0){
            //         // this.recordSaveReq.profit = this.currentBetAmount * this.userWinRate - this.freewinBuyCoin;
            //         this.recordSaveReq.profit = this.currentBetAmount * this.userWinRate;
            //     }else{
            //         this.recordSaveReq.profit = this.currentBetAmount * this.userWinRate - this.currentBetAmount * this.lineAmount;
            //     }
            //     // console.log('this.recordSaveReq:'+JSON.stringify(this.recordSaveReq));
            //     globalThis.gameRecordMgr && globalThis.gameRecordMgr.pairRecordById(this.gameid,this.recordSaveRes.saveIndex,this.recordSaveReq);
            // }

            this.oneBetEndResCall = type => {
              // console.log('oneBetEndResCall type'+type);
              director.emit('oneBetEndRes', res, type, this.betClickMode);
              this.oneBetEndResCall = null;
            };

            if (this.betBtnState == 'rolling') {
              this.betBtnState = 'clearing';

              if (this.oneBetEndResCall) {
                this.oneBetEndResCall(this.betSpeedMode);
                this.oneBetEndResCall = null;
              }
            }

            if (this.recordSaveReq && this.recordSaveRes && (this.recordSaveRes.saveIndex || this.recordSaveRes.saveIndex == 0)) {
              var slotInfo = globalThis.slotInfo;

              if (slotInfo) {
                this.SaveGameRecordDetail(this.recordSaveRes.saveIndex, {
                  local: slotInfo
                });

                if (this.freewinBuyCoin > 0) {
                  this.recordSaveReq.profit = this.currentBetAmount * this.userWinRate;
                } else {
                  this.recordSaveReq.profit = this.currentBetAmount * this.userWinRate;
                } // console.log('this.recordSaveReq:'+JSON.stringify(this.recordSaveReq));


                globalThis.gameRecordMgr && globalThis.gameRecordMgr.pairRecordById(this.gameid, this.recordSaveRes.saveIndex, this.recordSaveReq);
              }
            }

            this.freewinBuyCoin = 0; // if(this.betSpeedMode == 'turbo'){
            //     this.scheduleOnce(()=>{
            //         this.betBtnState = 'clearing';
            //         if(this.oneBetEndResCall){
            //             this.oneBetEndResCall(0,'rightNow');
            //             this.oneBetEndResCall = null;
            //         }
            //     },0.5);
            // }else{
            //     this.scheduleOnce(this.oneBetEndResCall,1);
            // }
          }, this);
          director.on('onCoinChange', data => {
            if (data) {
              if (data.reason != 'bet') {
                if (this.viewMode == 'play') {
                  if (this.betBtnState != 'normal') {
                    this.coinHasChanged = true;
                  } else {
                    var mUserInfo = this.getUserInfo();

                    if (mUserInfo) {
                      mUserInfo.coin = data.coin;
                      this.betUserInfoUpdate(mUserInfo.coin, null, null);
                      this.updateBetInfo();
                    }
                  }
                }
              }
            }
          }, this);
          director.on('betBtnState', state => {
            if (state == 'disable') {
              this.canBet = false; // this.betBtnNode.interactable = false;
            } else if (state == 'enable') {
              this.canBet = true; // this.betBtnNode.interactable = true;
            }
          }, this);
          director.on('betUserInfoUpdateWinAnima', rate => {
            this.betUserInfoUpdateAnima(null, null, rate * this.currentBetAmount, 0.2);
          }, this);
          director.on('rollStopEndTrigger', () => {
            var mUserInfo = this.getUserInfo();

            if (mUserInfo) {
              var act = () => {
                if (this.userWinRate == 0) {
                  this.betUserInfoUpdate(mUserInfo.coin, null, this.currentBetAmount * this.userWinRate);
                  director.emit('readyToBet');
                } else {
                  if (this.userWinRate < 480) {
                    this.betUserInfoUpdateAnima(mUserInfo.coin, null, this.currentBetAmount * this.userWinRate, this.winLabelAnimaTime, () => {
                      director.emit('readyToBet');
                    });
                  } else {
                    this.betUserInfoUpdateAnima(mUserInfo.coin, null, this.currentBetAmount * this.userWinRate, this.winLabelAnimaTime, () => {
                      director.emit('readyToBet');
                    });
                  }
                }
              };

              if (this.coinHasChanged) {
                this.coinHasChanged = false;

                var _userInfo = globalThis.getUserInfo();

                var _mUserInfo2 = this.getUserInfo();

                if (_userInfo && _mUserInfo2) {
                  _mUserInfo2.coin = _userInfo.coin;
                }

                act(); // globalThis.subGameSocketReq('user.userHandler.getUserInfoLight', {query_uid : mUserInfo.uuid}, (err, res) => {
                //     if (!err && res && res.data) {
                //         if (res.code == 200) {
                //             mUserInfo.coin = res.data.coin;
                //         }
                //     }
                //     act();
                // });
              } else {
                act();
              } // if(this.userOwnCoin != mUserInfo.coin){
              //     // let change = this.changeCoin > 0 ? this.changeCoin : null;
              //     this.betUserInfoUpdateAnima(mUserInfo.coin,null,this.betAmount*this.userWinRate,1);
              // }else{
              //     this.betUserInfoUpdate(mUserInfo.coin,null,this.betAmount*this.userWinRate);
              // }

            }
          }, this);
          director.on('rollStop', () => {
            // sUtil.webInfoCollect('game_free_fps');
            director.emit('rollStopEndTrigger');
          }, this);
          director.on('readyToBet', () => {
            if (this.viewMode == 'record') {
              this.recordState = 'idle';

              if (this.recordBtns) {
                var recordCtrBtn = this.recordBtns.node.getChildByName('ctrBtn');

                if (recordCtrBtn) {
                  recordCtrBtn.getChildByName('Play').active = true;
                  recordCtrBtn.getChildByName('Pause').active = false;
                }
              }

              this.RecordQuitBtnActionCtr(true);
            }

            var mUserInfo = this.getUserInfo();

            if (mUserInfo) {
              this.updateBetInfo();
              var canContinue = true;
              var canBet = this.canBet && !this.playerBetLimit();
              var notifyTemp = globalThis.notifySecretary && globalThis.notifySecretary(this.gameid, "game_settle");

              if (notifyTemp && !notifyTemp.continue) {
                canBet = false;
                canContinue = false;
              }

              if (canBet) {
                if (this.autoBetCount >= 0) {
                  if (this.autoBetCount > 0) {
                    this.scheduleOnce(() => {
                      this.betAction(this.betType);
                    }, 0.5);
                  } else if (this.autoBetCount == 0) {
                    director.emit('autoBetInfoUpdate', this.autoBetCount);
                    this.setGameBtnState('enable');

                    if (this.betBtnNode) {
                      this.betBtnNode.node.active = true;
                    }

                    if (this.autoPlayBtnNode) {
                      var animation = this.autoPlayBtnNode.getComponent(Animation);

                      if (animation) {
                        animation.stop();
                      }
                    }

                    this.betBtnState = 'normal';
                    this.betClickMode = 'normal';
                    this.autoBetCount = -2;
                    this.quickMode = false;
                  }
                } else if (this.autoBetCount == -1) {
                  this.scheduleOnce(() => {
                    this.betAction(this.betType);
                  }, 0.5);
                } else {
                  if (this.viewMode == 'play') {
                    director.emit('autoBetInfoUpdate', this.autoBetCount);
                  }

                  this.setGameBtnState('enable');

                  if (this.autoPlayBtnNode) {
                    var _animation = this.autoPlayBtnNode.getComponent(Animation);

                    if (_animation) {
                      _animation.stop();
                    }
                  }

                  this.betBtnState = 'normal';
                  this.quickMode = false;
                }
              } else {
                if (this.autoBetCount > 0 || this.autoBetCount == -1) {
                  director.emit('viewChange', 'autoSpinCancel');
                  director.emit('autoBetInfoUpdate', this.autoBetCount);

                  if (!this.canBet && canContinue) {
                    globalThis.goldNotenough && globalThis.goldNotenough();
                  }

                  if (this.betBtnNode) {
                    this.betBtnNode.node.active = true;
                  }

                  if (this.autoPlayBtnNode) {
                    var _animation2 = this.autoPlayBtnNode.getComponent(Animation);

                    if (_animation2) {
                      _animation2.stop();
                    }
                  }
                }

                this.setGameBtnState('enable');
                this.betBtnState = 'normal';
                this.betClickMode = 'normal';
                this.quickMode = false;
              }
            }

            director.emit('soltAllReadyToBet');
          }, this);
          director.on('subGameBetActionBtnClick', (_betType, _coin) => {
            if (!this.playerBetLimit()) {
              this.freewinBuyCoin = _coin;
              this.betActionBtnClick(_betType, 2);
            }
          }, this);
          director.on('freeWinOneRoundEnd', () => {
            if (this.viewMode == 'record' && this.recordBtnState == 'pause') {
              this.recordState = 'idle';
              this.RecordQuitBtnActionCtr(true);
            }
          }, this);
          director.on('freeWinRoundBegin', () => {
            if (this.viewMode == 'record') {
              this.recordState = 'playing';
              this.RecordQuitBtnActionCtr(false);
            }
          }, this);

          if (this.backToHall) {
            this.backToHall.node.on('click', () => {
              director.removeAll(this);
              input.off(Input.EventType.KEY_DOWN, this.onKeyDown, this);
              (_crd && btnInternationalManager === void 0 ? (_reportPossibleCrUseOfbtnInternationalManager({
                error: Error()
              }), btnInternationalManager) : btnInternationalManager).Clear();
              this.unscheduleAllCallbacks();
              globalThis.subGamePlayShotAudio('subGameClick');
              globalThis.subGameBackToHall();
            }, this);
          }

          if (this.turboBtnNode) {
            this.turboBtnNode.node.on('click', () => {
              this.turboBtnAction();
            }, this);
          }

          if (this.minusBtnNode) {
            this.minusBtnNode.node.on('click', () => {
              this.minusBtnClick();
            }, this);
          }

          if (this.addBtnNode) {
            this.addBtnNode.node.on('click', () => {
              this.addBtnClick();
            }, this);
          }

          if (this.autoPlayBtnNode) {
            this.autoPlayBtnNode.node.on('click', () => {
              globalThis.subGamePlayShotAudio('subGameClick');
              this.autoPlayBtnClick();
            }, this);
          }

          if (this.betBtnNode) {
            this.betBtnNode.node.on(Node.EventType.MOUSE_ENTER, () => {// director.emit('betBtnEnter');
            }, this);
            this.betBtnNode.node.on(Node.EventType.MOUSE_LEAVE, () => {// director.emit('betBtnLeave');
            }, this);
            this.betBtnNode.node.on('click', () => {
              if (!this.playerBetLimit()) {
                globalThis.webFullScreen && globalThis.webFullScreen();
                this.betActionBtnClick(this.betType);
              }
            }, this);

            if (this.selectBetBtn) {
              this.selectBetBtn.node.on('click', () => {
                if (this.viewMode == 'play') {
                  globalThis.subGamePlayShotAudio('subGameClick');
                  this.selectBetBtn.interactable = false;
                  this.selectBet.node.active = true;
                  this.selectBet.appearViewAnima();
                  this.selectBet.initData(this.betAmount);
                  this.selectBet.onView();
                  var mUserInfo = this.getUserInfo();

                  if (mUserInfo) {
                    this.selectBet.betInfoSet(mUserInfo.coin, this.betAmount * this.lineAmount, 0);
                  }
                }
              }, this);
            }
          }

          if (this.winAreaBtn) {
            this.winAreaBtn.node.on('click', () => {// if(winAreaClickEnable){
              //     globalThis.subGamePlayShotAudio('subGameClick');
              //     globalThis.gameRecordMgr && globalThis.gameRecordMgr.createGameRecordNode(this.node.parent,globalThis.currentPlayingGameID,'solt');
              // }
            }, this);
          }

          if (this.menuBtn) {
            this.menuBtn.node.on('click', () => {
              globalThis.subGamePlayShotAudio('subGameClick');
              globalThis.webFullScreen && globalThis.webFullScreen();
              this.menuBtnClick();
            }, this);
          }

          if (this.closeBtn) {
            this.closeBtn.node.on('click', () => {
              globalThis.subGamePlayShotAudio('subGameClick');
              this.closeBtnClick();
            }, this);
          }

          if (this.quitBtn) {
            this.quitBtn.node.on('click', () => {
              if (this.touchEnable) {
                console.log('quit game');
                this.touchEnable = false;
                this.unscheduleAllCallbacks();
                globalThis.subGamePlayShotAudio('subGameClick');
                globalThis.subGameBackToHall();
              }
            }, this);
          }

          if (this.soundBtn) {
            this.soundBtn.node.on('click', () => {
              this.soundBtnClick();
            }, this);
          }

          if (this.payTableBtn) {
            this.payTableBtn.node.on('click', () => {
              this.viewPos = 'paytable';
              globalThis.subGamePlayShotAudio('subGameClick');
              this.imgView.node.active = true;
              this.imgView.appearViewAnima();
              var languageType = globalThis.GetLanguageType && globalThis.GetLanguageType();

              if (!languageType) {
                languageType = 'EN';
              }

              this.imgView.onView('intro/paytable/' + languageType, (_crd && btnInternationalManager === void 0 ? (_reportPossibleCrUseOfbtnInternationalManager({
                error: Error()
              }), btnInternationalManager) : btnInternationalManager).GetDataByKey('payTable'));
            }, this);
          }

          if (this.rulesBtn) {
            this.rulesBtn.node.on('click', () => {
              this.viewPos = 'rule';
              this.rulesBtnClick();
            }, this);
          }

          if (this.histroyBtn) {
            this.histroyBtn.node.on('click', () => {
              this.viewPos = 'his';
              globalThis.subGamePlayShotAudio('subGameClick'); // director.emit('gameRecordReplay',1,111);

              globalThis.gameRecordMgr && globalThis.gameRecordMgr.createGameRecordNode(this.node.parent, globalThis.currentPlayingGameID, 'solt');
            }, this);
          }

          if (this.recordBtns) {
            var recordQuitBtn = this.recordBtns.node.getChildByPath('btns/quit');

            if (recordQuitBtn) {
              recordQuitBtn.on('click', () => {
                if (this.recordBtnTouchEnable) {
                  this.OpenRecordView(false);
                }
              }, this);
            }

            var recordCtrBtn = this.recordBtns.node.getChildByName('ctrBtn');

            if (recordCtrBtn) {
              recordCtrBtn.on('click', () => {
                if (this.recordBtnTouchEnable) {
                  if (this.recordState == 'idle') {
                    this.PlayGameRecordDetail();
                  } else if (this.recordState == 'playing') {
                    this.recordBtnState = 'pause';
                    recordCtrBtn.getChildByName('Play').active = true;
                    recordCtrBtn.getChildByName('Pause').active = false;
                  }
                }
              }, this);
            }
          }

          globalThis.getClientBetConfig(this.gameid, data => {
            if (data) {
              // console.log('getClientBetConfig:',JSON.stringify(data));
              (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                error: Error()
              }), sUtil) : sUtil).webInfoCollect('load_web_game_done');
              this.clientConfig = data;
              var betTypes = this.clientConfig.bet_types;

              if (betTypes) {
                var mUserInfo = this.getUserInfo();

                if (mUserInfo) {
                  // mUserInfo.coin = 100000;
                  var betType = -1;
                  var roomid = '';

                  if (this.roomid >= 0) {
                    roomid = this.roomid.toString();
                  }

                  var betTypeStr = sys.localStorage.getItem('slotBetAmountValue_' + this.gameid + '_' + roomid);
                  console.log('betTypeStr:', betTypeStr);

                  if (betTypeStr || betTypeStr == '0') {
                    betType = parseInt(betTypeStr);
                  } // for (let i = betTypes.length - 1; i >= 0; i--) {
                  //     const element = betTypes[i];
                  //     if (this.gameid == element.game_id) {
                  //         if (element.game_mode == 1 && (element.bet_amount * element.bet_multiple * 30 < mUserInfo.coin || element.bet_type == betType)){
                  //             console.log(`betAmount: ${element.bet_amount} betMultiple: ${element.bet_multiple} bet_type: ${element.bet_type}`);
                  //             this.betAmount = element.bet_amount;
                  //             this.lineAmount = element.bet_multiple;
                  //             this.betType = element.bet_type;
                  //             betType = element.bet_type;
                  //             director.emit('sSlotBetInfoUpdate', { betAmount: this.betAmount, multiple: this.lineAmount });
                  //             break;
                  //         }
                  //     }
                  // }


                  if (betType == -1) {
                    for (var _i6 = 0; _i6 < betTypes.length; _i6++) {
                      var _element3 = betTypes[_i6];

                      if (this.gameid == _element3.game_id) {
                        if (_element3.game_mode == 1 && _element3.bet_amount * _element3.bet_multiple < mUserInfo.coin) {
                          this.betAmount = _element3.bet_amount;
                          this.lineAmount = _element3.bet_multiple;
                          this.betType = _element3.bet_type;
                          betType = _element3.bet_type;
                          director.emit('sSlotBetInfoUpdate', {
                            betAmount: this.betAmount,
                            multiple: this.lineAmount
                          });
                          break;
                        }
                      }
                    }
                  } else {
                    for (var _i7 = 0; _i7 < betTypes.length; _i7++) {
                      var _element4 = betTypes[_i7];

                      if (this.gameid == _element4.game_id) {
                        if (_element4.game_mode == 1 && betType == _element4.bet_type) {
                          this.betAmount = _element4.bet_amount;
                          this.lineAmount = _element4.bet_multiple;
                          this.betType = _element4.bet_type;
                          betType = _element4.bet_type;
                          director.emit('sSlotBetInfoUpdate', {
                            betAmount: this.betAmount,
                            multiple: this.lineAmount
                          });
                          break;
                        }
                      }
                    }
                  }

                  if (this.betAmount == 0) {
                    for (var _i8 = 0; _i8 < betTypes.length; _i8++) {
                      var _element5 = betTypes[_i8];

                      if (_element5.game_mode == 1 && this.gameid == _element5.game_id) {
                        this.betAmount = _element5.bet_amount;
                        this.lineAmount = _element5.bet_multiple;
                        this.betType = _element5.bet_type;
                        betType = _element5.bet_type;
                        director.emit('sSlotBetInfoUpdate', {
                          betAmount: this.betAmount,
                          multiple: this.lineAmount
                        });
                        break;
                      }
                    }
                  } // if(mUserInfo.coin < this.betAmount * this.lineAmount){
                  //     for (let i = 0; i < betTypes.length; i++) {
                  //         const element = betTypes[i];
                  //         if (this.gameid == element.game_id) {
                  //             this.betAmount = element.bet_amount;
                  //             this.lineAmount = element.bet_multiple;
                  //             this.betType = element.bet_type;
                  //             break;
                  //         }
                  //     }
                  // }


                  director.emit('betUserInfoUpdate', mUserInfo.coin, this.betAmount * this.lineAmount, 0);
                  director.emit('betTypeChange', this.betType);
                  this.updateBetInfo();
                  director.emit('slotBetAmountRange', this.minAmount, this.maxAmount);
                } // let selBetType = globalThis.getSubGameBetType();
                // let selBetType = 1;
                // if (!selBetType) {
                //     selBetType = 1;
                // }

              }
            } // console.log('clientConfig:'+JSON.stringify(data));

          });
          var audioData = globalThis.getSubGameAudioVolume();

          if (audioData) {
            if (audioData.audioVolume == 1) {
              this.soundTip(false);
            } else {
              this.soundTip(true);
            }
          }

          this.scheduleOnce(() => {
            this.touchEnable = true; //lunbo

            globalThis.openHorseNode && globalThis.openHorseNode(obj => {
              if (obj && obj.isValid) {
                obj.parent = this.node.parent;
                obj.setSiblingIndex(0);
              }
            });
          }, 1);
        }

        betActionBtnClick(betType, gameMode) {
          if (gameMode === void 0) {
            gameMode = 1;
          }

          if (this.canBet) {
            this.spinBtnClick();

            if (this.betBtnState == 'normal') {
              this.betAction(betType, gameMode);
            } else if (this.betBtnState == 'rolling') {
              director.emit('subGameSlotBetActionBtnClick'); // if(this.betSpeedMode != 'turbo'){
              //     if(this.oneBetEndResCall){
              //         this.betBtnState = 'clearing';
              //         if(this.oneBetEndResCall){
              //             this.unschedule(this.oneBetEndResCall);
              //             this.oneBetEndResCall('shutDown');
              //             this.oneBetEndResCall = null;
              //         }
              //     }
              // }
            } else if (this.betBtnState == 'clearing') {
              if (this.quickMode == false) {// this.quickMode = true;
                // director.emit('subGameSlotRollQuickStopAction');
                // director.emit('subGameSlotRollQuickStop');
              }
            }
          } else {
            this.betClickCoinNotEnough();
          }
        }

        betClickCoinNotEnough() {
          if (this.betBtnState == 'normal') {
            var tipStr = '';

            if (globalThis.GetDataByKey) {
              var mUserInfo = this.getUserInfo();

              if (mUserInfo && this.clientConfig) {
                var maxBet = mUserInfo.max_bet;
                var userCoin = mUserInfo.coin;
                var sumAmount = this.lineAmount * this.betAmount;
                var sum = this.betAmount;

                if (this.betLabel) {
                  this.betLabel.string = this.AddCommas(this.lineAmount * this.betAmount);
                }

                if (sumAmount > userCoin) {
                  console.log('coin not enough');
                  globalThis.goldNotenough && globalThis.goldNotenough();
                } else if (sum > maxBet) {
                  var tips = (_crd && btnInternationalManager === void 0 ? (_reportPossibleCrUseOfbtnInternationalManager({
                    error: Error()
                  }), btnInternationalManager) : btnInternationalManager).GetDataByKey('maxBetNotEnough');

                  if (tips) {
                    tipStr = tips;
                  }

                  if (!tipStr) {
                    tipStr = 'unknow error';
                  }

                  director.emit('uiTipsOpen', 'text', tipStr);
                }
              }
            }
          }
        }

        changeBtnInfoLabelColor(color) {
          this.ownLabel.color = color;
          this.betLabel.color = color;
          this.winLabel.color = color;
        }

        betUserInfoUpdate(own, bet, win) {
          if (this.ownLabel) {
            if (own || own == 0) {
              this.ownLabel.node['rollAnimaValue'] = own;
              this.ownLabel.string = this.AddCommas(Math.trunc(own));
            }
          }

          if (this.betLabel) {
            if (bet || bet == 0) {
              this.betLabel.node['rollAnimaValue'] = bet;
              this.betLabel.string = this.AddCommas(Math.trunc(bet));
            }
          }

          if (this.winLabel) {
            if (win || win == 0) {
              this.winLabel.node['rollAnimaValue'] = win;
              this.winLabel.string = this.AddCommas(Math.trunc(win));
            }
          }
        }

        betUserInfoUpdateAnima(own, bet, win, animaTime, callBack) {
          if (animaTime === void 0) {
            animaTime = 1;
          }

          if (callBack === void 0) {
            callBack = null;
          }

          if (own || own == 0) {
            this.CommasLabelAnima(this.ownLabel, Math.trunc(own), animaTime);
          }

          if (bet || bet == 0) {
            this.CommasLabelAnima(this.betLabel, Math.trunc(bet), animaTime);
          }

          if (win || win == 0) {
            this.CommasLabelAnima(this.winLabel, Math.trunc(win), animaTime);
          }

          if (callBack != null) {
            this.scheduleOnce(callBack, animaTime);
          }
        }

        setGameBtnState(state) {
          //this.betAmount <= this.minAmount && this.betAmount >= this.maxAmount
          if (state == 'enable') {
            if (this.betAmount < this.maxAmount) {
              this.addBtnNodeStateUpdate(true);
            }

            if (this.betAmount > this.minAmount) {
              this.minusBtnNodeStateUpdate(true);
            }

            this.autoPlayBtnNodeStateUpdate(true);

            if (this.selectBetBtn) {
              this.selectBetBtn.interactable = true;
            }

            if (this.menuBtn) {
              this.menuBtn.interactable = true;
            }
          } else if (state == 'disable') {
            this.addBtnNodeStateUpdate(false);
            this.minusBtnNodeStateUpdate(false);
            this.autoPlayBtnNodeStateUpdate(false);

            if (this.selectBetBtn) {
              this.selectBetBtn.interactable = false;
            }

            if (this.menuBtn) {
              this.menuBtn.interactable = false;
            }
          }
        }

        CommasLabelAnima(label, value, time) {
          if (label && (label.node['rollAnimaValue'] || label.node['rollAnimaValue'] == 0)) {
            var valueOri = label.node['rollAnimaValue'];
            var mTween = label.node['rollAnimaTween'];

            if (mTween) {
              mTween.stop();
            }

            var tweenTargetVec3 = v3(valueOri, valueOri, valueOri); // console.log('tweenTargetVec3:'+tweenTargetVec3);

            label.node['rollAnimaTween'] = tween(tweenTargetVec3).to(time, v3(value, value, value), {
              "onUpdate": target => {
                if (label) {
                  label.node['rollAnimaValue'] = Math.floor(target.x);
                  label.string = this.AddCommas(Math.floor(target.x));
                }
              },
              easing: 'quadOut'
            }).call(() => {
              if (label) {
                label.node['rollAnimaValue'] = Math.floor(value);
                label.string = this.AddCommas(Math.floor(value));
              }
            }).start();
          }
        }

        updateBetInfo() {
          var mUserInfo = this.getUserInfo();

          if (mUserInfo && this.clientConfig) {
            var maxBet = mUserInfo.max_bet;
            var userCoin = mUserInfo.coin;
            var sumAmount = this.lineAmount * this.betAmount;
            var sum = this.betAmount;

            if (this.betLabel) {
              this.betLabel.string = this.AddCommas(this.lineAmount * this.betAmount);
            }

            if (sumAmount > userCoin || sum > maxBet) {
              director.emit('betBtnState', 'disable');
            } else {
              director.emit('betBtnState', 'enable');
            }
          }

          var bet_types = this.clientConfig.bet_types;
          this.maxAmount = 0;
          this.minAmount = 100000000;

          if (bet_types && bet_types.length > 0) {
            for (var i = 0; i < bet_types.length; i++) {
              var element = bet_types[i];

              if (element.game_id == this.gameid) {
                if (element.bet_amount > this.maxAmount) {
                  this.maxAmount = element.bet_amount;
                }

                if (element.bet_amount < this.minAmount) {
                  this.minAmount = element.bet_amount;
                }
              }
            }

            if (this.minusBtnNode) {
              if (this.betAmount <= this.minAmount) {
                this.changeRenderAlpha(this.minusBtnNode.node, 125); // this.minusBtnNode.getComponent(UIOpacity).opacity = 125;
              } else {
                this.changeRenderAlpha(this.minusBtnNode.node, 255); // this.minusBtnNode.getComponent(UIOpacity).opacity = 255;
              }
            }

            if (this.addBtnNode) {
              if (this.betAmount >= this.maxAmount) {
                this.changeRenderAlpha(this.addBtnNode.node, 125); // this.addBtnNode.getComponent(UIOpacity).opacity = 125;
              } else {
                this.changeRenderAlpha(this.addBtnNode.node, 255); // this.addBtnNode.getComponent(UIOpacity).opacity = 255;
              }
            } // console.log(maxAmount);
            // console.log(minAmount);

          }
        }

        betAction(_betType, _gameMode) {
          if (_gameMode === void 0) {
            _gameMode = 1;
          }

          this.betClickMode = this.autoBetCount > 0 || this.autoBetCount == -1 ? 'autoBet' : 'normal';
          this.oneBetEndResCall = null;
          this.userWinRate = 0;
          this.betBtnState = 'rolling';
          this.setGameBtnState('disable');
          this.betSpeedMode = this.betSpeedModeTemp;
          this.lastBetTime = globalThis.getServerTime ? globalThis.getServerTime() : 0; // this.changeBtnInfoLabelColor(Color.WHITE);

          this.betUserInfoUpdate(null, null, 0);
          this.subGameSocketReq = {
            game_id: this.gameid,
            game_mode: _gameMode,
            bet_time: this.lastBetTime,
            bet_type: _betType,
            bet_amount: this.betAmount,
            input: this.freewinBuyCoin > 0 ? this.freewinBuyCoin : this.betAmount * this.lineAmount
          };
          globalThis.subGameSocketReq('slot.mainHandler.bet', this.subGameSocketReq, (err, res) => {
            // sUtil.webInfoCollect('game_roll_fps');
            if (!err && res && res.data) {
              if (res.code == 200) {
                var _mUserInfo3 = this.getUserInfo();

                if (_mUserInfo3) {
                  if (_gameMode == 1) {
                    var coin = _mUserInfo3.coin - this.betAmount * this.lineAmount;
                    this.beforeUserCoin = coin;

                    if (_mUserInfo3.coin >= this.betAmount * this.lineAmount) {
                      _mUserInfo3.coin = this.beforeUserCoin;
                      this.betUserInfoUpdate(this.beforeUserCoin, null, null);
                    }
                  } else if (_gameMode == 2) {
                    var _coin2 = _mUserInfo3.coin - this.freeBuyBetAmount * this.freeBuyLineMultiple;

                    this.beforeUserCoin = _coin2;

                    if (_mUserInfo3.coin >= this.freeBuyBetAmount * this.freeBuyLineMultiple) {
                      _mUserInfo3.coin = this.beforeUserCoin;
                      this.betUserInfoUpdate(this.beforeUserCoin, null, null);
                    }
                  }

                  _mUserInfo3.coin = res.data.coin;
                }

                this.lastRoundRate = res.data.rate;
                director.emit('onGameEnd', res);

                var _seq = -1;

                var _round_tag = [];
                var roundType = -1;

                if (globalThis.slotInfo) {
                  if (globalThis.slotInfo.lastRollType == 'normal') {
                    roundType = 0;
                  } else if (globalThis.slotInfo.lastRollType == 'freeWin') {
                    roundType = 1;
                  }
                }

                if (_gameMode == 2) {
                  _round_tag.push(2);
                } else {
                  if (globalThis.slotInfo) {
                    if (globalThis.slotInfo) {
                      if (res.data.jackpot_amount > 0) {
                        _round_tag.push(3);
                      } else {
                        if (globalThis.slotInfo.lastRollType == 'normal') {
                          _round_tag.push(1);
                        } else if (globalThis.slotInfo.lastRollType == 'freeWin') {
                          _round_tag.push(11);
                        }
                      }
                    }
                  }
                }

                if (globalThis.slotInfo && globalThis.slotInfo.lastRoundSeq >= 0) {
                  _seq = globalThis.slotInfo.lastRoundSeq;
                }

                globalThis.subGameSocketReq('slot.mainHandler.report', {
                  round_tag: _round_tag,
                  bet_time: this.lastBetTime,
                  game_id: this.gameid,
                  result_id: "game_id=" + this.gameid + "&game_mode=" + _gameMode + "&rate=" + this.lastRoundRate + "&seq=" + _seq + "&round_type=" + roundType
                }, (err, res) => {});
              }
            } else {
              var code = -1;

              if (err) {
                code = err.code;
              }

              (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                error: Error()
              }), sUtil) : sUtil).GamePerforAnalysis('slotBetAction_Error', code.toString());
              director.emit('subGameBetMsgErr', code);
            }
          });
          var recordBetCoin = 0;
          var mUserInfo = this.getUserInfo();

          if (mUserInfo) {
            if (_gameMode == 1) {
              recordBetCoin = this.betAmount * this.lineAmount;
              this.currentBetAmount = this.betAmount;
            } else if (_gameMode == 2) {
              recordBetCoin = this.freewinBuyCoin;
              this.currentBetAmount = this.freeBuyBetAmount;
            }

            this.recordSaveReq = {
              time: globalThis.getServerTime(),
              bet: recordBetCoin
            };
            this.recordSaveRes = globalThis.gameRecordMgr && globalThis.gameRecordMgr.saveRecordByGameId(this.gameid, this.recordSaveReq);

            if (this.recordSaveRes) {
              if (this.recordSaveRes.delIndex || this.recordSaveRes.delIndex == 0) {
                sys.localStorage.removeItem(this.gameid + 'subGameRecordDetail_' + this.recordSaveRes.delIndex);
              }
            }
          }

          if (this.autoBetCount > 0) {
            this.autoBetCount--;
            director.emit('autoBetInfoUpdate', this.autoBetCount);
          }

          director.emit('betBtnClick', this.betSpeedMode); // else if(this.autoBetCount == 0){
          //     director.emit('autoBetInfoUpdate',this.autoBetCount);
          // }
        }

        AddCommas(money) {
          if (!money) {
            return "0";
          } else {
            var mon;

            if (typeof money == "string") {
              mon = Number(money);
            } else {
              mon = money;
            }

            var coinRate = 1;

            if (globalThis.getCoinRate) {
              coinRate = globalThis.getCoinRate();
            }

            var byte = coinRate.toString().length - 1;
            ;
            var showMoney = (mon / coinRate).toFixed(byte);
            var tempmoney = String(showMoney);
            var left = tempmoney.split('.')[0],
                right = tempmoney.split('.')[1];
            right = right ? right.length >= byte ? '.' + right.substring(0, byte) : '.' + right : '';
            var temp = left.split('').reverse().join('').match(/(\d{1,3})/g);

            if (temp) {
              return (Number(tempmoney) < 0 ? "-" : "") + temp.join(',').split('').reverse().join('') + right;
            } else {
              return '';
            }
          }
        }

        OpenRecordView(value) {
          if (this.recordBtns) {
            if (value) {
              this.recordBtnTouchEnable = true;
              director.emit('subGameBoxRecordView', true, this.menuState);
              this.betAmountBeforeRecord = this.betAmount;

              if (this.currentRecordData) {
                if (this.currentRecordData.betAmount >= 0) {
                  this.betAmount = this.currentRecordData.betAmount;
                  this.betUserInfoUpdate(null, this.betAmount * this.lineAmount, 0);
                }
              }

              this.viewMode = 'record';
              this.recordBtns.node.active = true;
              this.betBtns.node.active = false;
              this.menuBtns.node.active = false;
              var walletIcon = this.betInfo.node.getChildByPath('wallet/icon');

              if (walletIcon) {
                walletIcon.active = false;
              }

              var walletLabel = this.betInfo.node.getChildByName('walletLabel');

              if (walletLabel) {
                walletLabel.active = false;
              }

              var replayingLabel = this.betInfo.node.getChildByName('replayingLabel');

              if (replayingLabel) {
                replayingLabel.active = true;
              }

              if (this.freeWinBtn) {
                var item = this.freeWinBtn.getChildByPath('betInfo/item');

                if (item) {
                  item.getChildByName('icon').active = false;
                  item.getChildByName('Label').active = false;
                  item.getChildByName('replayingLabel').active = true;
                }
              } // director.emit('subGameReplayingView',true);

            } else {
              if (this.gameMode == 'freeWin' && this.recordState == 'idle') {
                director.off('soltAllReadyToBet');
                director.once('soltAllReadyToBet', () => {
                  this.viewMode = 'play';
                  director.emit('subGameBoxRecordView', false, this.menuState);
                  this.syncUserCoinByLocal();
                }, this);
                director.emit('freeWinOver', 'record');
                director.emit('rollStop');
              } else {
                director.emit('subGameBoxRecordView', false, this.menuState);
                this.viewMode = 'play';
                this.syncUserCoinByLocal();
              }

              this.recordBtnState = 'pause';
              this.betAmount = this.betAmountBeforeRecord;
              this.betUserInfoUpdate(null, this.betAmountBeforeRecord * this.lineAmount, 0);
              this.recordBtns.node.active = false;
              this.MenuPanelViewByState();

              var _walletIcon = this.betInfo.node.getChildByPath('wallet/icon');

              if (_walletIcon) {
                _walletIcon.active = true;
              }

              var _walletLabel = this.betInfo.node.getChildByName('walletLabel');

              if (_walletLabel) {
                _walletLabel.active = true;
              }

              var _replayingLabel = this.betInfo.node.getChildByName('replayingLabel');

              if (_replayingLabel) {
                _replayingLabel.active = false;
              }

              if (this.freeWinBtn) {
                var _item = this.freeWinBtn.getChildByPath('betInfo/item');

                if (_item) {
                  _item.getChildByName('icon').active = true;
                  _item.getChildByName('Label').active = true;
                  _item.getChildByName('replayingLabel').active = false;
                }
              }
            }
          }
        }

        SaveGameRecordDetail(index, res) {
          if (res) {
            var gameid = globalThis.currentPlayingGameID;
            res.betAmount = this.currentBetAmount;
            sys.localStorage.setItem(gameid + 'subGameRecordDetail_' + index, JSON.stringify(res));
          }
        }

        GetGameRecordDetail(index) {
          var gameid = globalThis.currentPlayingGameID;
          var res = sys.localStorage.getItem(gameid + 'subGameRecordDetail_' + index);

          if (res) {
            var resData = JSON.parse(res);

            if (resData) {
              return resData;
            }
          }

          return null;
        }

        PlayGameRecordDetail() {
          // console.log('this.currentRecordData:'+this.currentRecordData);
          if (this.recordState == 'idle') {
            if (this.gameMode == 'normal') {
              if (this.currentRecordData) {
                this.RecordQuitBtnActionCtr(false);
                this.recordState = 'playing';
                this.recordBtnState = 'play';
                this.currentBetAmount = this.betAmount;
                this.userWinRate = 0;
                this.betBtnState = 'rolling';
                this.setGameBtnState('disable');
                this.betSpeedMode = 'normal';
                director.emit('betBtnClick', 'normal');
                var recordCtrBtn = this.recordBtns.node.getChildByName('ctrBtn');

                if (recordCtrBtn) {
                  recordCtrBtn.getChildByName('Play').active = false;
                  recordCtrBtn.getChildByName('Pause').active = true;
                }

                this.scheduleOnce(() => {
                  if (this.currentRecordData) {
                    this.userWinRate = this.currentRecordData.local.lastRate;
                    this.betBtnState = 'clearing';
                    director.emit('oneBetEndRes', this.currentRecordData, 'normal', 'normal'); // director.emit('subGameMenuView', true);
                  }
                }, 1);
              }
            } else if (this.gameMode == 'freeWin') {
              var _recordCtrBtn = this.recordBtns.node.getChildByName('ctrBtn');

              if (_recordCtrBtn) {
                _recordCtrBtn.getChildByName('Play').active = false;
                _recordCtrBtn.getChildByName('Pause').active = true;
              }

              this.recordBtnState = 'play';
              director.emit('recordFreeWinPlayClick');
            }
          }
        }

        RecordQuitBtnActionCtr(value) {
          var recordQuitNode = this.recordBtns.node.getChildByPath('btns/quit');

          if (recordQuitNode) {
            var recordQuitBtn = recordQuitNode.getComponent(Button);

            if (recordQuitBtn) {
              recordQuitBtn.interactable = value;
            }
          }
        }

        NeedFreeWinContinue() {
          if (this.viewMode == 'play') {
            return true;
          } else {
            if (this.recordBtnState == 'pause') {
              return false;
            }
          }

          return true;
        }

        MenuPanelViewByState() {
          if (this.menuState == 'bet') {
            this.betBtns.node.active = true;
            this.menuBtns.node.active = false;
          } else if (this.menuState == 'menu') {
            this.betBtns.node.active = false;
            this.menuBtns.node.active = true;
          }
        }

        turboBtnAction() {
          globalThis.subGamePlayShotAudio('subGameClick');

          if (this.betSpeedModeTemp == 'normal') {
            this.betSpeedModeTemp = 'turbo';
            this.turboBtnNodeClick(true);
          } else {
            this.betSpeedModeTemp = 'normal';
            this.turboBtnNodeClick(false);
          }

          director.emit('uiTipsOpen', this.betSpeedModeTemp, this.betSpeedModeTemp == 'normal' ? (_crd && btnInternationalManager === void 0 ? (_reportPossibleCrUseOfbtnInternationalManager({
            error: Error()
          }), btnInternationalManager) : btnInternationalManager).GetDataByKey('closeTurboSpinMode') : (_crd && btnInternationalManager === void 0 ? (_reportPossibleCrUseOfbtnInternationalManager({
            error: Error()
          }), btnInternationalManager) : btnInternationalManager).GetDataByKey('openTurboSpinMode'));
        }

        playerBetLimit() {
          if (globalThis.checkGameIsLimit) {
            var res = globalThis.checkGameIsLimit(this.gameid);

            if (res) {
              if (res.limit) {
                if (res.tip) {
                  director.emit('uiTipsOpen', 'text', res.tip);
                }

                return true;
              }
            }
          }

          return false;
        }

        saveBetAmountValue() {
          this.removeIDSchedule('saveBetAmountValue');
          var roomid = '';

          if (this.roomid >= 0) {
            roomid = this.roomid.toString();
          }

          this.pushIDSchedule(() => {
            sys.localStorage.setItem('slotBetAmountValue_' + this.gameid + '_' + roomid, this.betType.toString());
          }, 0.5, 'saveBetAmountValue');
        }

        syncUserCoinByLocal() {
          var _userInfo = globalThis.getUserInfo();

          var mUserInfo = this.getUserInfo();

          if (_userInfo && mUserInfo) {
            mUserInfo.coin = _userInfo.coin;
            this.betUserInfoUpdate(_userInfo.coin, null, null);
            this.updateBetInfo();
          }
        }

        loadFreeBuyView() {
          (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
            error: Error()
          }), sUtil) : sUtil).getAssetInSubGame(globalThis.currentPlayingGameID, 'prefabs/UIFreeWinBuy', Prefab, (err, asset) => {
            if (asset) {
              var freebuy = instantiate(asset);
              freebuy.parent = this.node.parent;
              freebuy.active = true;
              this.selfFreeWinBuyView = freebuy.getComponent(_crd && sFreeWinBuyView === void 0 ? (_reportPossibleCrUseOfsFreeWinBuyView({
                error: Error()
              }), sFreeWinBuyView) : sFreeWinBuyView);
            }
          });
        }

        changeRenderAlpha(target, value) {
          if (target) {
            var renders = target.getComponentsInChildren(Renderable2D);

            if (renders && renders.length > 0) {
              for (var i = 0; i < renders.length; i++) {
                var render = renders[i];
                var newColor = color(render.color);
                newColor.a = value;
                render.color = newColor;
              }
            }
          }
        } //implement method


        turboBtnNodeClick(value) {}

        addBtnNodeStateUpdate(visible) {}

        minusBtnNodeStateUpdate(visible) {}

        autoPlayBtnNodeStateUpdate(visible) {}

        autoPlayBtnClick() {}

        menuBtnClick() {}

        closeBtnClick() {}

        soundBtnClick() {}

        soundTip(value) {}

        addBtnClick() {
          if (this.clientConfig && this.betBtnState == 'normal') {
            var findMax = false;
            var bet_types = this.clientConfig.bet_types;

            if (bet_types && bet_types.length > 0) {
              for (var i = 0; i < bet_types.length; i++) {
                var element = bet_types[i]; // console.log('addBtnClick:',JSON.stringify(element),'this.betAmount:',this.betAmount,'this.lineAmount:',this.lineAmount);

                if (element.game_mode == 1 && element.game_id == this.gameid) {
                  if (parseInt(element.bet_amount) > this.betAmount) {
                    this.betAmount = parseInt(element.bet_amount);
                    this.betType = element.bet_type;
                    director.emit('sSlotBetInfoUpdate', {
                      betAmount: this.betAmount,
                      multiple: this.lineAmount
                    });

                    if (this.betLabel) {
                      this.betLabel.node.setScale(1.5, 1.5, 1.5);
                      tween(this.betLabel.node).to(0.6, {
                        scale: Vec3.ONE
                      }, {
                        easing: 'elasticOut'
                      }).start();
                    }

                    this.updateBetInfo();
                    findMax = true;
                    this.saveBetAmountValue();
                    break;
                  }
                }
              }
            }

            if (!findMax) {
              director.emit('uiTipsOpen', 'text', (_crd && btnInternationalManager === void 0 ? (_reportPossibleCrUseOfbtnInternationalManager({
                error: Error()
              }), btnInternationalManager) : btnInternationalManager).GetDataByKey('maximunBet'));
            }
          }
        }

        minusBtnClick() {
          if (this.clientConfig && this.betBtnState == 'normal') {
            var findMinus = false;
            var bet_types = this.clientConfig.bet_types;

            if (bet_types && bet_types.length > 0) {
              for (var i = bet_types.length - 1; i >= 0; i--) {
                var element = bet_types[i]; // console.log('addBtnClick:',JSON.stringify(element),'this.betAmount:',this.betAmount,'this.lineAmount:',this.lineAmount);

                if (element.game_mode == 1 && element.game_id == this.gameid && parseInt(element.bet_amount) < this.betAmount) {
                  this.betAmount = parseInt(element.bet_amount);
                  this.betType = element.bet_type;
                  director.emit('sSlotBetInfoUpdate', {
                    betAmount: this.betAmount,
                    multiple: this.lineAmount
                  });

                  if (this.betLabel) {
                    this.betLabel.node.setScale(1.5, 1.5, 1.5);
                    tween(this.betLabel.node).to(0.6, {
                      scale: Vec3.ONE
                    }, {
                      easing: 'elasticOut'
                    }).start();
                  }

                  this.updateBetInfo();
                  this.saveBetAmountValue();
                  findMinus = true;
                  break;
                }
              }
            }

            if (!findMinus) {
              director.emit('uiTipsOpen', 'text', (_crd && btnInternationalManager === void 0 ? (_reportPossibleCrUseOfbtnInternationalManager({
                error: Error()
              }), btnInternationalManager) : btnInternationalManager).GetDataByKey('minimumBet'));
            }
          }
        }

        spinBtnClick() {}

        freeWinBingo() {}

        freeWinOver() {}

        freeWinBegain() {}

        freeWinBtnInfo(rate) {}

        rulesBtnClick() {
          globalThis.subGamePlayShotAudio('subGameClick');
          this.imgView.node.active = true;
          this.imgView.appearViewAnima();
          var languageType = globalThis.GetLanguageType && globalThis.GetLanguageType();

          if (!languageType) {
            languageType = 'EN';
          }

          this.imgView.onView('intro/rules/' + languageType, (_crd && btnInternationalManager === void 0 ? (_reportPossibleCrUseOfbtnInternationalManager({
            error: Error()
          }), btnInternationalManager) : btnInternationalManager).GetDataByKey('rules'));
        } //implement end


      }, _defineProperty(_class3, "instance", void 0), _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "languageConfigs", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return [];
        }
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "winLabelAnimaTime", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return 0.2;
        }
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "ownLabel", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "betLabel", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor5 = _applyDecoratedDescriptor(_class2.prototype, "winLabel", [_dec6], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor6 = _applyDecoratedDescriptor(_class2.prototype, "verLabel", [_dec7], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor7 = _applyDecoratedDescriptor(_class2.prototype, "selectBetBtn", [_dec8], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor8 = _applyDecoratedDescriptor(_class2.prototype, "winAreaBtn", [_dec9], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor9 = _applyDecoratedDescriptor(_class2.prototype, "turboBtnNode", [_dec10], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor10 = _applyDecoratedDescriptor(_class2.prototype, "minusBtnNode", [_dec11], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor11 = _applyDecoratedDescriptor(_class2.prototype, "addBtnNode", [_dec12], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor12 = _applyDecoratedDescriptor(_class2.prototype, "autoPlayBtnNode", [_dec13], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor13 = _applyDecoratedDescriptor(_class2.prototype, "betBtnNode", [_dec14], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor14 = _applyDecoratedDescriptor(_class2.prototype, "backToHall", [_dec15], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor15 = _applyDecoratedDescriptor(_class2.prototype, "menuBtn", [_dec16], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor16 = _applyDecoratedDescriptor(_class2.prototype, "quitBtn", [_dec17], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor17 = _applyDecoratedDescriptor(_class2.prototype, "soundBtn", [_dec18], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor18 = _applyDecoratedDescriptor(_class2.prototype, "payTableBtn", [_dec19], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor19 = _applyDecoratedDescriptor(_class2.prototype, "rulesBtn", [_dec20], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor20 = _applyDecoratedDescriptor(_class2.prototype, "histroyBtn", [_dec21], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor21 = _applyDecoratedDescriptor(_class2.prototype, "closeBtn", [_dec22], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor22 = _applyDecoratedDescriptor(_class2.prototype, "betBtns", [_dec23], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor23 = _applyDecoratedDescriptor(_class2.prototype, "betInfo", [_dec24], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor24 = _applyDecoratedDescriptor(_class2.prototype, "menuBtns", [_dec25], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor25 = _applyDecoratedDescriptor(_class2.prototype, "recordBtns", [_dec26], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor26 = _applyDecoratedDescriptor(_class2.prototype, "selectBet", [_dec27], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor27 = _applyDecoratedDescriptor(_class2.prototype, "autoSpin", [_dec28], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor28 = _applyDecoratedDescriptor(_class2.prototype, "imgView", [_dec29], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor29 = _applyDecoratedDescriptor(_class2.prototype, "freeWinBuy", [_dec30], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor30 = _applyDecoratedDescriptor(_class2.prototype, "freeWinBtn", [_dec31], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor31 = _applyDecoratedDescriptor(_class2.prototype, "subGameClick", [_dec32], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor32 = _applyDecoratedDescriptor(_class2.prototype, "changeColorParent", [_dec33], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return [];
        }
      }), _descriptor33 = _applyDecoratedDescriptor(_class2.prototype, "changeColorNode", [_dec34], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return [];
        }
      }), _descriptor34 = _applyDecoratedDescriptor(_class2.prototype, "changeColorButton", [_dec35], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return [];
        }
      })), _class2)) || _class));

      globalThis.getClientBetConfig = function (gameid, cb) {
        if (globalThis.getSlotBetConfig) {
          globalThis.getSlotBetConfig(gameid, cb);
        } else {
          globalThis.getClientConfig(cb);
        }
      }; // globalThis.notifySecretary = function(){
      //     setTimeout(() => {
      //         const mUserInfo = globalThis.getUserInfo();
      //         if(mUserInfo){
      //             mUserInfo.coin = 10000;
      //         }
      //         director.emit('onGameCoinChange',{reason : 'award_relief'});
      //     }, 3000);
      //     return {continue : false};
      // }


      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=sGameEntity.js.map
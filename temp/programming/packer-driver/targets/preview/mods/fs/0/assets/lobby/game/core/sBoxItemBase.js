System.register(["__unresolved_0", "cc", "__unresolved_1"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, sComponent, _dec, _class, _temp, _crd, ccclass, property, sBoxItemBase;

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _reportPossibleCrUseOfsComponent(extras) {
    _reporterNs.report("sComponent", "./sComponent", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
    }, function (_unresolved_2) {
      sComponent = _unresolved_2.sComponent;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "c7bd1zh+x1PM5aXfLBz+VtB", "sBoxItemBase", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("sBoxItemBase", sBoxItemBase = (_dec = ccclass('sBoxItemBase'), _dec(_class = (_temp = class sBoxItemBase extends (_crd && sComponent === void 0 ? (_reportPossibleCrUseOfsComponent({
        error: Error()
      }), sComponent) : sComponent) {
        constructor() {
          super(...arguments);

          _defineProperty(this, "actionArr", {});
        }

        onLoad() {}

        start() {}

        boxItemUpdate(renderData, symbolValue, boxState, tableState, animiaFinishCallBack) {
          if (animiaFinishCallBack === void 0) {
            animiaFinishCallBack = null;
          }
        }

        clearItem(symbolValue, rollState, tableState) {}
        /**
         * @zh
         * 注册以名字为key的行为
         * @param _name - 行为名称
         * @param call - 行为回调
         */


        RegisterEnitityAction(_name, call) {
          if (_name && call) {
            this.actionArr[_name] = call;
          }
        }
        /**
         * @zh
         * 触发以名字为key的行为
         * @param _name - 行为名称
         * @param param - 参数
         */


        DoEnitityAction(_name, param) {
          if (param === void 0) {
            param = null;
          }

          var action = this.actionArr[_name];

          if (action) {
            action(param);
          }
        }

      }, _temp)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=sBoxItemBase.js.map
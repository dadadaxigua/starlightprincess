System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2", "__unresolved_3"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, Component, Node, sys, ScrollView, director, UITransform, tween, v3, Widget, Label, ScrollViewCycle, gameRecordImgFollow, gameRecordItem, GameRecordMgr, _dec, _dec2, _dec3, _dec4, _dec5, _dec6, _dec7, _dec8, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _descriptor5, _descriptor6, _descriptor7, _temp, _crd, ccclass, property, gameRecord;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfScrollViewCycle(extras) {
    _reporterNs.report("ScrollViewCycle", "../../../../scripts/util/scrollViewCycle", _context.meta, extras);
  }

  function _reportPossibleCrUseOfgameRecordImgFollow(extras) {
    _reporterNs.report("gameRecordImgFollow", "./gameRecordImgFollow", _context.meta, extras);
  }

  function _reportPossibleCrUseOfgameRecordItem(extras) {
    _reporterNs.report("gameRecordItem", "./gameRecordItem", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      Component = _cc.Component;
      Node = _cc.Node;
      sys = _cc.sys;
      ScrollView = _cc.ScrollView;
      director = _cc.director;
      UITransform = _cc.UITransform;
      tween = _cc.tween;
      v3 = _cc.v3;
      Widget = _cc.Widget;
      Label = _cc.Label;
    }, function (_unresolved_2) {
      ScrollViewCycle = _unresolved_2.ScrollViewCycle;
    }, function (_unresolved_3) {
      gameRecordImgFollow = _unresolved_3.gameRecordImgFollow;
    }, function (_unresolved_4) {
      gameRecordItem = _unresolved_4.gameRecordItem;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "0f3e58AQv5H3KQCr3o3dQwr", "gameRecord", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("gameRecord", gameRecord = (_dec = ccclass('gameRecord'), _dec2 = property(Node), _dec3 = property(ScrollView), _dec4 = property(Node), _dec5 = property(Node), _dec6 = property(_crd && ScrollViewCycle === void 0 ? (_reportPossibleCrUseOfScrollViewCycle({
        error: Error()
      }), ScrollViewCycle) : ScrollViewCycle), _dec7 = property(_crd && gameRecordImgFollow === void 0 ? (_reportPossibleCrUseOfgameRecordImgFollow({
        error: Error()
      }), gameRecordImgFollow) : gameRecordImgFollow), _dec8 = property(Label), _dec(_class = (_class2 = (_temp = class gameRecord extends Component {
        constructor() {
          super(...arguments);

          _initializerDefineProperty(this, "clostBtn", _descriptor, this);

          _initializerDefineProperty(this, "scrolView", _descriptor2, this);

          _initializerDefineProperty(this, "content", _descriptor3, this);

          _initializerDefineProperty(this, "noHistoryNode", _descriptor4, this);

          _initializerDefineProperty(this, "scrollViewCycle", _descriptor5, this);

          _initializerDefineProperty(this, "gameRecordImgFollow", _descriptor6, this);

          _initializerDefineProperty(this, "timezoneLab", _descriptor7, this);

          _defineProperty(this, "gameId", '');

          _defineProperty(this, "tableId", '');

          _defineProperty(this, "type", '');

          _defineProperty(this, "recordDatas", void 0);

          _defineProperty(this, "touchEnable", true);
        }

        onLoad() {
          this.addBtnListen();
          if (globalThis.getTimezone) this.timezoneLab.string = globalThis.getTimezone();
          director.on('closeGameRecord', gameId => {
            if (gameId = this.gameId) {
              if (this.type == 'solt') {
                this.disappearViewAnima();
              } else {
                this.node.destroy();
              }
            }
          }, this);
          director.on('updateGameRecord', gameId => {
            if (gameId == this.gameId) {
              this.recordDatas = globalThis.gameRecordMgr.getRecordByGameId(this.gameId);
              this.noHistoryNode.active = false;
              this.scrollViewCycle.getComponent(ScrollView).stopAutoScroll();
              this.scrollViewCycle.resetContentY();
              this.scrollViewCycle.changeHeight(this.recordDatas.length);
            }
          }, this);
        }

        init() {
          if (this.type == 'solt') {
            this.recordDatas = globalThis.gameRecordMgr.getRecordByGameId(this.gameId);

            if (this.recordDatas.length == 0) {
              this.noHistoryNode.active = true;
            } else {
              this.noHistoryNode.active = false;
            }

            this.scollItemInit();
          } else if (this.type == 'poker') {
            globalThis.gameRecordMgr.getRecordByServer(this.tableId, data => {
              this.recordDatas = data;

              if (this.recordDatas.length == 0) {
                this.noHistoryNode.active = true;
              } else {
                this.noHistoryNode.active = false;
              }

              this.scollItemInit();
            });
          }
        }

        scollItemInit() {
          this.scrollViewCycle.init(0, (item, index) => {
            this.updateItemInfo(item, index);
          });
          this.scrollViewCycle.getComponent(ScrollView).stopAutoScroll();
          this.scrollViewCycle.resetContentY();
          this.scrollViewCycle.changeHeight(this.recordDatas.length);
        }

        addBtnListen() {
          this.clostBtn.on('click', () => {
            director.emit('slotDisappearView', 'gameRecord');
            this.node.destroy();
          }, this);
        }

        updateItemInfo(item, index) {
          if (!this.recordDatas || this.recordDatas.length < 0) return;
          var data = this.recordDatas[index];
          if (!this.recordDatas[index]) return;
          item.getComponent(_crd && gameRecordItem === void 0 ? (_reportPossibleCrUseOfgameRecordItem({
            error: Error()
          }), gameRecordItem) : gameRecordItem).init(data, this.type);
          this.gameRecordImgFollow.updateItem(item, data, index, this.gameId, this.type, this.tableId);
        }

        appearViewAnima() {
          var viewUITransform = this.node.getComponent(UITransform);
          var mainNode = this.node.getChildByName('main').getComponent(UITransform);
          mainNode.contentSize = viewUITransform.contentSize;
          mainNode.node.position = v3(0, -viewUITransform.contentSize.height / 2, 0);
          tween(mainNode.node).to(0.3, {
            position: v3(0, viewUITransform.contentSize.height / 2, 0)
          }, {
            easing: 'sineOut'
          }).call(() => {}).start();
        }

        disappearViewAnima() {
          if (this.touchEnable) {
            this.touchEnable = false;
            var viewUITransform = this.node.getComponent(UITransform);
            var mainNode = this.node.getChildByName('main').getComponent(UITransform);
            tween(mainNode.node).to(0.3, {
              position: v3(0, -viewUITransform.contentSize.height / 2, 0)
            }, {
              easing: 'sineOut'
            }).call(() => {
              this.touchEnable = true;
              this.node.destroy();
            }).start();
          }
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "clostBtn", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "scrolView", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "content", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "noHistoryNode", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor5 = _applyDecoratedDescriptor(_class2.prototype, "scrollViewCycle", [_dec6], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor6 = _applyDecoratedDescriptor(_class2.prototype, "gameRecordImgFollow", [_dec7], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor7 = _applyDecoratedDescriptor(_class2.prototype, "timezoneLab", [_dec8], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class));

      GameRecordMgr = class GameRecordMgr {
        constructor() {
          _defineProperty(this, "recordMap", {});

          _defineProperty(this, "curSaveIndexMap", {});

          _defineProperty(this, "serverRecordMap", {});
        }

        saveRecordByGameId(gameId, record) {
          var records = this.getRecordByGameId(gameId);
          var curSaveIndexs = this.getCurSaveIndexs(gameId);
          var uid = globalThis.getUserInfo().uid;
          var maxLength = this.getRecordMaxSaveLength();
          var startIndex = curSaveIndexs[0];
          var endIndex = curSaveIndexs[1];
          record.id = endIndex;
          var key = "record-" + gameId + "-" + uid + "-" + endIndex++;
          sys.localStorage.setItem(key, JSON.stringify(record));
          records.unshift(record);
          var needDelIndex = null;

          if (endIndex - startIndex > maxLength) {
            needDelIndex = startIndex;
            var removeKey = "record-" + gameId + "-" + uid + "-" + startIndex;
            startIndex++;
            sys.localStorage.removeItem(removeKey);
            records.pop();
          }

          this.saveCurSaveIndexs(gameId, [startIndex, endIndex]);
          this.recordMap[gameId] = records;
          director.emit('updateGameRecord', gameId);
          return {
            delIndex: needDelIndex,
            saveIndex: curSaveIndexs[1]
          };
        }

        getCurSaveIndexs(gameId) {
          var curSaveIndexs = this.curSaveIndexMap[gameId];

          if (!curSaveIndexs) {
            var uid = globalThis.getUserInfo().uid;
            var recordSaveIndexJson = sys.localStorage.getItem(gameId + "-" + uid + "-recordSaveIndex");

            try {
              curSaveIndexs = JSON.parse(recordSaveIndexJson);
            } catch (error) {}
          }

          if (!curSaveIndexs) {
            curSaveIndexs = [0, 0];
            this.curSaveIndexMap[gameId] = curSaveIndexs;
          }

          return curSaveIndexs;
        }

        pairRecordById(gameId, id, record) {
          var records = this.getRecordByGameId(gameId);

          for (var i = 0; i < records.length; i++) {
            if (records[i].id == id) {
              records[i] = record;
            }
          }

          director.emit('updateGameRecord', gameId);
          var uid = globalThis.getUserInfo().uid;
          var key = "record-" + gameId + "-" + uid + "-" + id;
          record.id = id;
          sys.localStorage.setItem(key, JSON.stringify(record));
        }

        saveCurSaveIndexs(gameId, curSaveIndexs) {
          var uid = globalThis.getUserInfo().uid;
          this.curSaveIndexMap[gameId] = curSaveIndexs;
          sys.localStorage.setItem(gameId + "-" + uid + "-recordSaveIndex", JSON.stringify(curSaveIndexs));
        }

        getRecordByGameId(gameId) {
          var records = this.recordMap[gameId];
          var uid = globalThis.getUserInfo().uid;
          if (!records) records = [];

          if (!records || records.length == 0) {
            var curSaveIndexs = this.getCurSaveIndexs(gameId);
            var startIndex = +curSaveIndexs[0],
                endIndex = +curSaveIndexs[1];
            if (endIndex == 0) return [];

            for (var i = startIndex; i < endIndex; i++) {
              var key = "record-" + gameId + "-" + uid + "-" + i;
              var recordJson = sys.localStorage.getItem(key);

              if (recordJson) {
                try {
                  var record = JSON.parse(recordJson);
                  records.unshift(record);
                } catch (error) {}
              }
            }
          }

          this.recordMap[gameId] = records;
          return records;
        }

        oneSoltRecordCome(gameId, record) {
          this.saveRecordByGameId(gameId, record);
        }

        getRecordMaxSaveLength() {
          return 50;
        }

        createGameRecordNode(parent, gameId, type, cb, tableId) {
          if (type == 'solt') {
            globalThis.getSoltGameRecord(node => {
              node.parent = parent;
              node.getComponent(gameRecord).gameId = gameId;
              node.getComponent(gameRecord).type = type;
              node.getComponent(gameRecord).init();
              node.active = true;
              node.getComponent(Widget).updateAlignment();
              node.getComponent(gameRecord).appearViewAnima();
            });
          } else if (type == 'poker') {
            globalThis.getPokerGameRecord(node => {
              node.parent = parent;
              node.getComponent(gameRecord).gameId = gameId;
              node.getComponent(gameRecord).type = type;
              node.getComponent(gameRecord).tableId = tableId;
              node.getComponent(gameRecord).init();
              node.active = true;
              if (cb) cb(node);
            });
          }
        }

        getRecordByServer(tableId, callback) {
          if (!globalThis.miniTableGameGetRecord) return [];
          var uid = globalThis.getUserInfo().uid;

          if (!this.serverRecordMap[tableId + '-' + uid]) {
            this.serverRecordMap[tableId + '-' + uid] = this.getLocalServerRecord(tableId);
          }

          var time = null;

          if (this.serverRecordMap[tableId + '-' + uid] && this.serverRecordMap[tableId + '-' + uid].length) {
            time = this.serverRecordMap[tableId + '-' + uid][0].bet_time;
          }

          globalThis.subGameSocketReq('miniTable.mainHandler.gameRecord', {
            table_id: tableId,
            start_time: time
          }, (err, res) => {
            if (res && res.code == 200) {
              this.serverRecordMap[tableId + '-' + uid] = [...res.data, ...this.serverRecordMap[tableId + '-' + uid]];
              this.setLocalServerRecord(tableId);
              callback(this.serverRecordMap[tableId + '-' + uid]);
            } else {
              callback([]); // if (globalThis.toast) {
              //     globalThis.toast(ResLoader.loadText(501, 'getRecordFailed'), 1.5)
              // }
            }
          });
        }

        getRecordDetailsByRoundId(tableId, roundId) {
          var uid = globalThis.getUserInfo().uid;
          var arr = this.serverRecordMap[tableId + '-' + uid];
          if (!arr) return null;

          for (var i = 0; i < arr.length; i++) {
            if (arr[i].round_id == roundId) return arr[i];
          }

          return null;
        }

        getLocalServerRecord(tableId) {
          var uid = globalThis.getUserInfo().uid;
          var key = "gameLocalServerRecord-" + tableId + "-" + uid;
          var localData = localStorage.getItem(key);

          try {
            var data = JSON.parse(localData);
            if (!data) data = [];
            return data;
          } catch (error) {
            return [];
          }
        }

        setLocalServerRecord(tableId) {
          var uid = globalThis.getUserInfo().uid;
          var key = "gameLocalServerRecord-" + tableId + "-" + uid;
          var data = this.serverRecordMap[tableId + '-' + uid];
          localStorage.setItem(key, JSON.stringify(data));
        }

      };
      globalThis.gameRecordMgr = new GameRecordMgr();

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=gameRecord.js.map
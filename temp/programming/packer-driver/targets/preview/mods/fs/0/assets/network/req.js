System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2", "__unresolved_3", "__unresolved_4", "__unresolved_5"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, director, sys, koi, tools, baseUtil, Socket, userData, _crd;

  function _reportPossibleCrUseOfkoi(extras) {
    _reporterNs.report("koi", "./entry", _context.meta, extras);
  }

  function _reportPossibleCrUseOftools(extras) {
    _reporterNs.report("tools", "./tools", _context.meta, extras);
  }

  function _reportPossibleCrUseOfbaseUtil(extras) {
    _reporterNs.report("baseUtil", "./baseUtil", _context.meta, extras);
  }

  function _reportPossibleCrUseOfSocket(extras) {
    _reporterNs.report("Socket", "./koi/game/global/pomelo-socket", _context.meta, extras);
  }

  function _reportPossibleCrUseOfuserData(extras) {
    _reporterNs.report("userData", "./userData", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      director = _cc.director;
      sys = _cc.sys;
    }, function (_unresolved_2) {
      koi = _unresolved_2.default;
    }, function (_unresolved_3) {
      tools = _unresolved_3.default;
    }, function (_unresolved_4) {
      baseUtil = _unresolved_4.baseUtil;
    }, function (_unresolved_5) {
      Socket = _unresolved_5.default;
    }, function (_unresolved_6) {
      userData = _unresolved_6.default;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "cc78d1EvyNNv5UTgfNH1MWA", "req", undefined);

      globalThis.getUserInfo = () => {
        return (_crd && koi === void 0 ? (_reportPossibleCrUseOfkoi({
          error: Error()
        }), koi) : koi).globalDta.user.myInfo;
      };

      globalThis.getSubGameBetAmount = function () {
        return 10;
      };

      globalThis.getSubGameBetType = function () {
        return 1;
      };

      _export("default", {
        init() {
          var listenerCallback = {
            onCoinChange(err, res) {
              console.log('onCoinChange:' + JSON.stringify(res));

              if (res && res.data) {
                if (res.data.coin || res.data.coin == 0) {
                  var mUserInfo = (_crd && userData === void 0 ? (_reportPossibleCrUseOfuserData({
                    error: Error()
                  }), userData) : userData).GetUserInfo();

                  if (mUserInfo) {
                    mUserInfo.coin = res.data.coin;
                    director.emit('userInfoUpdate');
                    director.emit('onCoinChange', res.data);
                  }
                }
              }
            },

            onGameEnd(err, res) {
              console.log('onGameEnd:' + JSON.stringify(res));

              if (err) {
                console.log('err-----', err);
              } else {
                director.emit('onGameEnd', res);
              }
            },

            onBroadcastNotice(err, res) {
              // console.log('onBroadcastNotice:' + JSON.stringify(res));
              if (err) {
                console.log('err-----', err);
              } else {
                if (res && res.data) {
                  director.emit('onBroadcastNotice', res.data);
                }
              }
            }

          };
          var listener = [{
            path: 'user.msg.coinChange',
            cb: 'onCoinChange'
          }, {
            path: 'chat.msg.broadcastNotice',
            cb: 'onBroadcastNotice'
          } // {
          //     path: 'slot.msg.end',
          //     cb: 'onGameEnd'
          // },
          // {path : 'event.Notice' , cb : 'onNotice'},
          // {path : 'event.MAIL_MESSAGE',cb:'mailMessage'},
          ];
          (_crd && koi === void 0 ? (_reportPossibleCrUseOfkoi({
            error: Error()
          }), koi) : koi).socketInfo.Package.addPackListenerArr(listenerCallback, listener);

          globalThis.testNetWork = () => {
            globalThis.inSocket = new (_crd && Socket === void 0 ? (_reportPossibleCrUseOfSocket({
              error: Error()
            }), Socket) : Socket)();
          };

          globalThis.subGameBetReq = (gameID, betType, callback) => {
            new (_crd && koi === void 0 ? (_reportPossibleCrUseOfkoi({
              error: Error()
            }), koi) : koi).socketInfo.Package('slot.mainHandler.bet', {
              game_id: gameID,
              bet_type: betType,
              bet_time: globalThis.getServerTime && globalThis.getServerTime()
            }).send((err, res) => {
              if (callback) {
                if (res && res.code == 200) {
                  callback(true, res);
                } else {
                  callback(false, err);
                }
              }
            });
          };

          globalThis.subGameSocketReq = (path, data, callback) => {
            new (_crd && koi === void 0 ? (_reportPossibleCrUseOfkoi({
              error: Error()
            }), koi) : koi).socketInfo.Package(path, data).send(callback);
          };

          director.on('userInfoUpdate', () => {}, this);
        },

        bet() {
          new (_crd && koi === void 0 ? (_reportPossibleCrUseOfkoi({
            error: Error()
          }), koi) : koi).socketInfo.Package('slot.mainHandler.bet', {
            game_id: '146',
            bet_type: 1
          }).send((err, res) => {});
        },

        auth() {
          new (_crd && koi === void 0 ? (_reportPossibleCrUseOfkoi({
            error: Error()
          }), koi) : koi).socketInfo.Package('connector.authHandler.auth', {
            app_id: globalThis.appID,
            app_version: '20220324_1',
            device_id: 'ddddd1233',
            os_type: 'android',
            os_version: '1.0'
          }).send((err, res) => {
            console.log('authLog:' + res);

            if (res.code == '200') {
              this.GetUserInfoReq((suc, res) => {
                if (suc) {
                  (_crd && koi === void 0 ? (_reportPossibleCrUseOfkoi({
                    error: Error()
                  }), koi) : koi).globalDta.user.myInfo = res.data;
                  (_crd && userData === void 0 ? (_reportPossibleCrUseOfuserData({
                    error: Error()
                  }), userData) : userData).SetCurrentUserUid(res.data.uid);
                  director.emit('userInfoUpdate');
                  director.emit('authSuc');
                } else {
                  console.error(res);
                }
              });
            } else {}
          });
        },

        getTeamInfo(token, page, cb) {
          //...check
          this.post('query/get_team_info', {
            page: page
          }, (res, err) => {
            if (cb) {
              cb(res, err);
            }
          }, token);
        },

        getDailyData(token, page, sort, date, cb) {
          //...check
          this.post('query/get_daily_data', {
            page: page,
            sort: sort,
            date: date
          }, (res, err) => {
            if (cb) {
              cb(res, err);
            }
          }, token);
        },

        get_record(token, cb) {
          //...check
          this.post('query/get_record', {}, (res, err) => {
            if (cb) {
              cb(res, err);
            }
          }, token);
        },

        AutoLogin(_loginToken, _deviceId, cb) {
          //...check
          this.post2('user/auto_login', {
            token: _loginToken,
            device_id: _deviceId
          }, (res, err) => {
            if (cb) {
              cb(res, err);
            }
          });
        },

        DeviceLogin(_deviceId, _appId, _channel_code, _invite_uuid, _region, cb) {
          this.post2('user/device_account_login', {
            device_account: _deviceId,
            app_id: _appId,
            channel_code: _channel_code,
            invite_uuid: _invite_uuid,
            region: _region,
            client_info: JSON.stringify({
              device_id: _deviceId
            })
          }, (res, err) => {
            if (cb) {
              cb(res, err);
            }
          });
        },

        PhoneLogin(_phone, _phone_code, _channel_code, _appId, invite_uuid, _region, cb) {
          //...check
          this.post2('user/phone_num_login', {
            phone_num: _phone,
            verify_code: _phone_code,
            channel_code: _channel_code,
            app_id: _appId,
            invite_uuid: invite_uuid,
            region: _region
          }, (res, err) => {
            if (cb) {
              cb(res, err);
            }
          });
        },

        PWDLogin(_action, _user_name, _pwd, _confirm_passwd, _channle_code, _appid, _invite_uuid, _region, cb) {
          //...check
          this.post2('user/user_name_login', {
            action: _action,
            user_name: _user_name,
            passwd: _pwd,
            confirm_passwd: _confirm_passwd,
            channle_code: _channle_code,
            app_id: _appid,
            invite_uuid: _invite_uuid,
            region: _region
          }, (res, err) => {
            if (cb) {
              cb(res, err);
            }
          });
        },

        UserAccountBind(_uuid, _user_name, _passwd, _confirm_passwd, cb) {
          this.post2('user/user_name_bind', {
            uuid: _uuid,
            user_name: _user_name,
            passwd: _passwd,
            confirm_passwd: _confirm_passwd
          }, (res, err) => {
            if (cb) {
              cb(res, err);
            }
          }, globalThis.userToken);
        },

        LoginOut() {
          //...check
          this.post2('user/logout', {}, res => {});
        },

        GetPhoneCode(_phone, _type) {
          //...check
          this.post2('user/get_phone_code', {
            phone: _phone,
            type: _type
          }, res => {});
        },

        CheckUserName(_appid, _userame) {
          //...check
          this.post2('user/check_user_name', {
            user_name: _userame,
            app_id: _appid
          }, res => {});
        },

        ChangeUserName(_appid, _phone, _phonecode, _username) {
          //...check
          this.post2('user/change_user_name', {
            phone: _phone,
            phone_code: _phonecode,
            user_name: _username,
            app_id: _appid
          }, res => {});
        },

        ChangePhone(_appid, _phone, _phonecode, cb) {
          //...check
          this.post2('user/change_phone', {
            phone: _phone,
            phone_code: _phonecode,
            app_id: _appid
          }, res => {
            if (cb) {
              cb(res);
            }
          }, globalThis.userToken);
        },

        ChangePWD(_phone, _phonecode, _oldpwd, _pwd, _appId, cb) {
          //...check
          this.post2('user/change_pwd', {
            phone: _phone,
            phone_code: _phonecode,
            old_pwd: _oldpwd,
            pwd: _pwd,
            app_id: _appId
          }, (res, err) => {
            if (cb) {
              cb(res, err);
            }
          });
        },

        BindPWD(_pwd, cb) {
          //...check
          this.post2('user/bind_pwd', {
            pwd: _pwd
          }, (res, err) => {
            if (cb) {
              cb(res, err);
            }
          }, globalThis.userToken);
        },

        GetUserInfo(_appid, _uuid, _uid, cb) {
          //...check
          this.post2('user/get_user_info', {
            uuid: _uuid,
            user_id: _uid,
            app_id: _appid
          }, (res, err) => {
            if (cb) {
              cb(res, err);
            }
          }, globalThis.userToken);
        },

        GetLockInfo(cb) {
          this.post2('user/get_lock_info', {//...check
          }, (res, err) => {
            if (cb) {
              cb(res, err);
            }
          }, globalThis.userToken);
        },

        AddFriend(_uuid, cb) {
          this.post('relation/add_friend', {
            //...check
            uuid: _uuid
          }, (res, err) => {
            if (cb) {
              cb(res, err);
            }
          }, globalThis.userToken);
        },

        GetFriendApplyList(cb) {
          this.post('relation/get_apply_list', {//...check
          }, (res, err) => {
            if (cb) {
              cb(res, err);
            }
          }, globalThis.userToken);
        },

        ApplyFriendGreet(_uuid, _type, cb) {
          this.post('relation/apply_dispose', {
            //...check
            uuid: _uuid,
            type: _type
          }, (res, err) => {
            if (cb) {
              cb(res, err);
            }
          }, globalThis.userToken);
        },

        GetRelationList(_ticks, _page, _limit, cb) {
          //...check
          this.post('query/get_relation_list', {
            ticks: _ticks,
            page: _page,
            limit: _limit
          }, (res, err) => {
            if (cb) {
              cb(res, err);
            }
          }, globalThis.userToken);
        },

        SendTransferbalance(_uuid, _gold, cb) {
          //...check
          this.post('relation/transfer_balance', {
            uuid: _uuid,
            gold: _gold
          }, (res, err) => {
            if (cb) {
              cb(res, err);
            }
          }, globalThis.userToken);
        },

        ChangeUserInfo(cb, _appid, _nickname, _des, _avatar) {
          //...check
          var paras = {};
          paras['app_id'] = _appid;

          if (_nickname != null) {
            paras['nick_name'] = _nickname;
          }

          if (_des != null) {
            paras['desc'] = _des;
          }

          if (_avatar != null) {
            paras['avatar'] = _avatar;
          }

          this.post('user/change_user_info', paras, (res, err) => {
            if (cb) {
              cb(res, err);
            }
          }, globalThis.userToken);
        },

        IdentityQuery() {
          this.post('user/get_user_info', {}, res => {});
        },

        Identity(_realname, _identitycode) {
          this.post('user/identity', {
            real_name: _realname,
            identity_code: _identitycode
          }, res => {});
        },

        gameServerCon(_loginToken, _hosts, _deviceId) {
          //...check
          // return;
          var connParas = {
            access_token: _loginToken,
            hosts: _hosts,
            // hosts: ['192.168.0.121:59001'],
            device_id: _deviceId
          };
          console.log('gameServerCon');
          director.emit('uri_pomelo_done', connParas);
        },

        getAppConfig(_appid, cb) {
          //...check
          // this.post('/sys/app_config_info', {
          //     app_id : _appid,
          //     time_stamp : _timestamp,
          // }, (res,err) => {
          //     if(cb){
          //         cb(res,err);
          //     }
          // });
          // this.get('/hotUpdate/app_config_info', {
          //     app_id : _appid,
          //     time_stamp : _timestamp,
          // }, (res,err) => {
          //     if(cb){
          //         cb(res,err);
          //     }
          // });
          // this.get('hotUpdate/app_config_'+(_appid), {
          //     // app_id : _appid,
          //     // time_stamp : _timestamp,
          // }, (res,err) => {
          //     if(cb){
          //         cb(res,err);
          //     }
          // });
          this.get('hotUpdate/app_config_' + (_crd && baseUtil === void 0 ? (_reportPossibleCrUseOfbaseUtil({
            error: Error()
          }), baseUtil) : baseUtil).base64_encode(_appid), {// app_id : _appid,
            // time_stamp : _timestamp,
          }, (res, err) => {
            if (cb) {
              cb(res, err);
            }
          });
        },

        getNodeList(_appid, _timestamp, cb) {
          //...check
          this.post('sys/node_list', {
            app_id: _appid,
            time_stamp: _timestamp
          }, (res, err) => {
            if (cb) {
              cb(res, err);
            }
          }, globalThis.userToken);
        },

        getPayList(_appid, _timestamp, cb) {
          //...check
          this.post('sys/pay_list', {
            app_id: _appid,
            time_stamp: _timestamp
          }, (res, err) => {
            if (cb) {
              cb(res, err);
            }
          }, globalThis.userToken);
          console.log('globalThis.userToken:' + globalThis.userToken);
        },

        getGoodList(_appid, _timestamp, cb) {
          //...check
          this.post('shop/get_goods_info', {
            app_id: _appid,
            time_stamp: _timestamp
          }, (res, err) => {
            if (cb) {
              cb(res, err);
            }
          }, globalThis.userToken);
        },

        getUserMailList(limit, _page, cb) {
          this.post('mail/get_user_mail_list', {
            limit: limit,
            page: _page
          }, (res, err) => {
            if (cb) {
              cb(res, err);
            }
          }, globalThis.userToken);
        },

        getGlobalMailList(limit, _page, cb) {
          this.post('mail/get_global_mail_list', {
            limit: limit,
            page: _page
          }, (res, err) => {
            if (cb) {
              cb(res, err);
            }
          }, globalThis.userToken);
        },

        getMailItem(_mailID, cb) {
          this.post('mail/get_user_mail_item', {
            email_id: _mailID
          }, (res, err) => {
            if (cb) {
              cb(res, err);
            }
          }, globalThis.userToken);
        },

        deleteMail(_mailID, cb) {
          this.post('mail/del_user_mail', {
            email_id: _mailID
          }, (res, err) => {
            if (cb) {
              cb(res, err);
            }
          }, globalThis.userToken);
        },

        getTurntableConfig(token, cb) {
          this.post('activity/get_turntable_config', {
            app_id: globalThis.appID
          }, (res, err) => {
            if (cb) {
              cb(res, err);
            }
          }, globalThis.userToken);
        },

        startTurnTable(token, cb) {
          console.log('startTurnTable', globalThis.appID);
          this.post('activity/commit_turntable', {
            app_id: globalThis.appID
          }, (res, err) => {
            if (cb) {
              cb(res, err);
            }
          }, globalThis.userToken);
        },

        inviteCode(invite_code, cb) {
          //...check
          this.post('user/bind_invite_code', {
            invite_code: invite_code
          }, (res, err) => {
            if (cb) {
              cb(res, err);
            }
          }, globalThis.userToken);
        },

        saveBoxReq(value, cb) {
          //...check
          this.post('user/change_lock_balance', {
            num: value
          }, (res, err) => {
            if (cb) {
              cb(res, err);
            }
          }, globalThis.userToken);
        },

        getShopInfo(_app_id, _boss_id, _channel_code, cb) {
          //...check
          this.post2('shop/get_shop_list', {
            app_id: _app_id,
            boss_id: _boss_id,
            channel_code: _channel_code
          }, (res, err) => {
            if (cb) {
              cb(res, err);
            }
          }, globalThis.userToken);
        },

        payShopItem(_goods_id, _app_id, _boss_id, _channel_code, cb) {
          //...check
          this.post2('shop/pay', {
            goods_id: _goods_id,
            app_id: _app_id,
            boss_id: _boss_id,
            channel_code: _channel_code
          }, (res, err) => {
            if (cb) {
              cb(res, err);
            }
          }, globalThis.userToken);
        },

        getCommissionData(_app_id, _boss_id, _channel_code, cb) {
          //...check
          this.post2('agent/get_base_commission', {
            app_id: _app_id,
            boss_id: _boss_id,
            channel_code: _channel_code
          }, (res, err) => {
            if (cb) {
              cb(res, err);
            }
          }, globalThis.userToken);
        },

        getDirectTeamNum(_current_page, _app_id, _boss_id, _channel_code, cb) {
          //...check
          this.post2('agent/get_direct_team_num', {
            current_page: _current_page,
            app_id: _app_id,
            boss_id: _boss_id,
            channel_code: _channel_code
          }, (res, err) => {
            if (cb) {
              cb(res, err);
            }
          }, globalThis.userToken);
        },

        getServerTime(cb) {
          this.get2('agent/get_server_timestamp', {//...check
          }, (res, err) => {
            if (cb) {
              cb(res, err);
            }
          }, globalThis.userToken);
        },

        getPastCommission(_date, _app_id, _boss_id, _channel_code, cb) {
          //...check
          this.post2('agent/get_past_commission', {
            date: _date,
            app_id: _app_id,
            boss_id: _boss_id,
            channel_code: _channel_code
          }, (res, err) => {
            if (cb) {
              cb(res, err);
            }
          }, globalThis.userToken);
        },

        getDirectTeamPerformance(_current_page, _date, _app_id, _boss_id, _channel_code, cb) {
          //...check
          this.post2('agent/get_direct_team_performance', {
            current_page: _current_page,
            date: _date,
            app_id: _app_id,
            boss_id: _boss_id,
            channel_code: _channel_code
          }, (res, err) => {
            if (cb) {
              cb(res, err);
            }
          }, globalThis.userToken);
        },

        sendFeedback(_app_id, _boss_id, _channel_code, _category_one, _category_two, _mobile, _title, _desc, _img, cb) {
          //...check
          this.post2('notice/submit', {
            category_one: _category_one,
            category_two: _category_two,
            mobile: _mobile,
            title: _title,
            desc: _desc,
            img: _img,
            app_id: _app_id,
            boss_id: _boss_id,
            channel_code: _channel_code
          }, (res, err) => {
            if (cb) {
              cb(res, err);
            }
          }, globalThis.userToken);
        },

        getNotice(_app_id, _boss_id, _channel_code, cb) {
          //...check
          this.post2('notice/get_notice', {
            app_id: _app_id,
            boss_id: _boss_id,
            channel_code: _channel_code
          }, (res, err) => {
            if (cb) {
              cb(res, err);
            }
          }, globalThis.userToken);
        },

        getAnnouncement(_last_time, cb) {
          this.post('query/get_announcement', {
            last_time: _last_time
          }, (res, err) => {
            if (cb) {
              cb(res, err);
            }
          }, globalThis.userToken);
        },

        getRankData(_rank_type, cb) {
          this.post('query/get_rank', {
            rank_type: _rank_type
          }, (res, err) => {
            if (cb) {
              cb(res, err);
            }
          }, globalThis.userToken);
        },

        getUnreadMsg(cb) {
          this.post('query/get_all_unread_msg', {}, (res, err) => {
            if (cb) {
              cb(res, err);
            }
          }, globalThis.userToken);
        },

        //socket req
        GetBackpackItemListReq(callback) {
          new (_crd && koi === void 0 ? (_reportPossibleCrUseOfkoi({
            error: Error()
          }), koi) : koi).socketInfo.Package('user.itemHandler.getItemList', {}).send((err, res) => {
            if (callback) {
              if (res && res.code == 200) {
                callback(true, res);
              } else {
                callback(false, err);
              }
            }
          });
        },

        UseItemReq(_item_id, _num, callback) {
          new (_crd && koi === void 0 ? (_reportPossibleCrUseOfkoi({
            error: Error()
          }), koi) : koi).socketInfo.Package('item.itemHandler.useItem', {
            item_id: _item_id,
            num: _num
          }).send((err, res) => {
            if (callback) {
              if (res && res.code == 200) {
                callback(true, res);
              } else {
                callback(false, err);
              }
            }
          });
        },

        TransferItemReq(_uuid, _item_id, _num, _comment, callback) {
          new (_crd && koi === void 0 ? (_reportPossibleCrUseOfkoi({
            error: Error()
          }), koi) : koi).socketInfo.Package('user.userHandler.transferItem', {
            target_uuid: _uuid,
            item_id: _item_id,
            num: _num,
            comment: _comment
          }).send((err, res) => {
            if (callback) {
              if (res && res.code == 200) {
                callback(true, res);
              } else {
                callback(false, err, res);
              }
            }
          });
        },

        GainTaskRewardReq(_task_id, _draw_time, callback) {
          new (_crd && koi === void 0 ? (_reportPossibleCrUseOfkoi({
            error: Error()
          }), koi) : koi).socketInfo.Package('task.taskHandler.gainTaskReward', {
            task_id: _task_id,
            draw_time: _draw_time
          }).send((err, res) => {
            if (callback) {
              if (res && res.code == 200) {
                callback(true, res);
              } else {
                callback(false, err);
              }
            }
          });
        },

        GroupListReq(callback) {
          new (_crd && koi === void 0 ? (_reportPossibleCrUseOfkoi({
            error: Error()
          }), koi) : koi).socketInfo.Package('user.taskHandler.groupList', {}).send((err, res) => {
            if (callback) {
              if (res && res.code == 200) {
                callback(true, res);
              } else {
                callback(false, err);
              }
            }
          });
        },

        GroupInfoReq(_task_group_id, callback) {
          new (_crd && koi === void 0 ? (_reportPossibleCrUseOfkoi({
            error: Error()
          }), koi) : koi).socketInfo.Package('user.taskHandler.groupInfo', {
            task_group_id: _task_group_id
          }).send((err, res) => {
            if (callback) {
              if (res && res.code == 200) {
                callback(true, res);
              } else {
                callback(false, err);
              }
            }
          });
        },

        ExchangeReq(_type, _cost, callback) {
          new (_crd && koi === void 0 ? (_reportPossibleCrUseOfkoi({
            error: Error()
          }), koi) : koi).socketInfo.Package('award.awardHandler.exchange', {
            type: _type,
            cost: _cost
          }).send((err, res) => {
            if (callback) {
              if (res && res.code == 200) {
                callback(true, res);
              } else {
                callback(false, err);
              }
            }
          });
        },

        GetUserInfoReq(callback, _query_uid) {
          if (_query_uid === void 0) {
            _query_uid = '';
          }

          new (_crd && koi === void 0 ? (_reportPossibleCrUseOfkoi({
            error: Error()
          }), koi) : koi).socketInfo.Package('user.userHandler.getUserInfo', {
            query_uid: _query_uid
          }).send((err, res) => {
            if (callback) {
              if (res && res.code == 200) {
                callback(true, res);
              } else {
                callback(false, err);
              }
            }
          });
        },

        ModifyNickNameReq(_nickName, callback) {
          new (_crd && koi === void 0 ? (_reportPossibleCrUseOfkoi({
            error: Error()
          }), koi) : koi).socketInfo.Package('user.userHandler.modifyNickName', {
            nick_name: _nickName
          }).send((err, res) => {
            if (callback) {
              if (res && res.code == 200) {
                callback(true, res);
              } else {
                callback(false, err);
              }
            }
          });
        },

        AwardExchange(_type, _cost, callback) {
          new (_crd && koi === void 0 ? (_reportPossibleCrUseOfkoi({
            error: Error()
          }), koi) : koi).socketInfo.Package('user.awardHandler.exchange', {
            type: _type,
            cost: _cost
          }).send((err, res) => {
            if (callback) {
              if (res && res.code == 200) {
                callback(true, res);
              } else {
                callback(false, err);
              }
            }
          });
        },

        subGameBetReq(_game_id, _bet_type, callback) {
          new (_crd && koi === void 0 ? (_reportPossibleCrUseOfkoi({
            error: Error()
          }), koi) : koi).socketInfo.Package('slot.mainHandler.bet', {
            game_id: _game_id,
            bet_type: _bet_type
          }).send((err, res) => {
            if (callback) {
              if (res && res.code == 200) {
                callback(true, res);
              } else {
                callback(false, err);
              }
            }
          });
        },

        //socket req end
        mobileCode: function mobileCode(_ref, cb) {
          var {
            mobile,
            bossId,
            usage,
            appId
          } = _ref;
          var path = "/mobileCode";
          var paras = {
            mobile,
            bid: bossId,
            usage,
            appId
          };
          (_crd && tools === void 0 ? (_reportPossibleCrUseOftools({
            error: Error()
          }), tools) : tools).forBreak((_crd && koi === void 0 ? (_reportPossibleCrUseOfkoi({
            error: Error()
          }), koi) : koi).socketInfo.AuthUrls.map(u => ({
            url: u + path,
            paras: paras
          })), (_crd && tools === void 0 ? (_reportPossibleCrUseOftools({
            error: Error()
          }), tools) : tools).getJSON, function (err, res) {
            if (err || !res) {
              cb({
                err: err || "no res"
              });
            } else {
              cb(null, res);
            }
          });
        },
        WXLogin: function WXLogin(_ref2, cb) {
          var {
            wxid,
            wxcode,
            deviceid,
            bid
          } = _ref2;
          var path = "/wxLogin";
          var paras = {
            wxId: wxid,
            wxCode: wxcode,
            deviceId: deviceid,
            bid: bid
          };
          console.log("测试微信登陆，WXLogin发送的参数", JSON.stringify(paras));
          (_crd && tools === void 0 ? (_reportPossibleCrUseOftools({
            error: Error()
          }), tools) : tools).forBreak((_crd && koi === void 0 ? (_reportPossibleCrUseOfkoi({
            error: Error()
          }), koi) : koi).socketInfo.AuthUrls.map(u => ({
            url: u + path,
            paras: paras
          })), (_crd && tools === void 0 ? (_reportPossibleCrUseOftools({
            error: Error()
          }), tools) : tools).getJSON, function (err, res) {
            if (err || !res) {
              cb({
                err: err || "no res"
              });
            } else {
              cb(null, res);
            }
          }); //tools.getWXJSON({ url: koi.socketInfo.AuthUrls[0], paras: paras }, path, cb);
        },
        facebookLogin: function facebookLogin(device_id, access_token, client_id, app_id, cb) {
          this.post('/user/facebook_login', {
            access_token: access_token,
            client_id: client_id,
            device_id: device_id,
            app_id: app_id
          }, (res, err) => {
            if (cb) {
              cb(res, err);
            }
          }, globalThis.userToken);
        },
        bindPhone: function bindPhone(mobile, code, cb) {
          new (_crd && koi === void 0 ? (_reportPossibleCrUseOfkoi({
            error: Error()
          }), koi) : koi).socketInfo.Package('user.userHandler.bindMobileForNormalPlayer', {
            code: code,
            mobile: mobile
          }).send((err, res) => {
            console.log("测试绑定返回,err", JSON.stringify(err));
            console.log("测试绑定返回,res", JSON.stringify(res));

            if (err) {
              console.log("测试绑定返回,进入err", JSON.stringify(err));
              cb(err);
            } else {
              console.log("测试绑定返回,进入res", JSON.stringify(res));
              cb(null, res);
            }
          });
        },
        // testGet:function({},cb){
        //     const path = "/test";
        //     var paras = {};
        //     console.log("测试微信登陆，WXLogin发送的参数",JSON.stringify(paras));
        //     tools.getWXJSON({url:koi.socketInfo.AuthUrls[0],paras:paras},path,cb);
        // },

        /**
         * 登出
         */
        logOut: function logOut() {
          new (_crd && koi === void 0 ? (_reportPossibleCrUseOfkoi({
            error: Error()
          }), koi) : koi).socketInfo.Package('user.userHandler.logout', null).send(() => {});
          director.loadScene('game-login');
        },

        //校验渠道号
        checkChannel(channelCode) {
          this.post('/node/lobby/checkChannelCode', {
            channelCode: channelCode,
            bossId: globalThis.bossID
          }, res => {
            if (res.err || !res.data) {
              return;
            }

            if (res.data.status == 1) {
              if ((_crd && koi === void 0 ? (_reportPossibleCrUseOfkoi({
                error: Error()
              }), koi) : koi).globalDta.game.getChannelCode) {
                sys.localStorage.setItem('channel', channelCode);
              } // koi.globalDta.user.setStorage('channel', channelCode);

            } else {
              if (globalThis.appID) {
                (_crd && koi === void 0 ? (_reportPossibleCrUseOfkoi({
                  error: Error()
                }), koi) : koi).globalDta.game.channelCode = globalThis.appID;
                sys.localStorage.setItem('channel', globalThis.appID);
              }
            }
          });
        },

        cilckWatching(actName, gameName, sceneName) {
          return; //  var otherInfo = koi.globalDta.game.loginDate;
          //  if (cc.Net_CType) {
          //      otherInfo.netConnect = cc.Net_CType;
          //  }
          //  console.log(otherInfo);
          //  new koi.socketInfo.Package('user.noticeHandler.actionNotice', { act: actName, game: gameName, scene: sceneName, otherInfo: otherInfo }).send((err, res) => {
          //  });
        },

        versionUpdate(version, storageV) {
          if (!version || !storageV) {
            return true;
          }

          if (version > storageV) {
            return true;
          } else {
            return false;
          }
        },

        /**
         * post
         */
        // todo : 待服务端网址定下来
        post: function post(path, paras, cb, _token) {
          (_crd && tools === void 0 ? (_reportPossibleCrUseOftools({
            error: Error()
          }), tools) : tools).forBreak(globalThis.intHttpUrl.queryUrl.map(u => ({
            url: u + path,
            paras: paras,
            token: _token
          })), (_crd && tools === void 0 ? (_reportPossibleCrUseOftools({
            error: Error()
          }), tools) : tools).post, function (err, res) {
            console.log('>>post 2222:' + path, JSON.stringify(err), JSON.stringify(res));

            if (res && (res.success || res.data && res.success)) {
              cb(res, false);
            } else {
              cb(res, true);
            } // if (!res || (!res.data && !res.success)) {
            //     cb(res,true);
            // } else {
            //     cb(res,false);
            // }

          });
        },
        post2: function post2(path, paras, cb, _token) {
          (_crd && tools === void 0 ? (_reportPossibleCrUseOftools({
            error: Error()
          }), tools) : tools).forBreak(globalThis.intHttpUrl.loginUrl.map(u => ({
            url: u + path,
            paras: paras,
            token: _token
          })), (_crd && tools === void 0 ? (_reportPossibleCrUseOftools({
            error: Error()
          }), tools) : tools).post, function (err, res) {
            console.log('>>post 2222:' + path, JSON.stringify(err), JSON.stringify(res));

            if (res && (res.success || res.data && res.success)) {
              cb(res, false);
            } else {
              cb(res, true);
            }
          });
        },
        get: function get(path, paras, cb, _token) {
          (_crd && tools === void 0 ? (_reportPossibleCrUseOftools({
            error: Error()
          }), tools) : tools).forBreak(globalThis.intHttpUrl.CdnUrl.map(u => ({
            url: u + path,
            paras: paras,
            token: _token
          })), (_crd && tools === void 0 ? (_reportPossibleCrUseOftools({
            error: Error()
          }), tools) : tools).get, function (err, res) {
            console.log('>>get 2222:' + path, JSON.stringify(err), JSON.stringify(res));

            if (res && (res.success || res.data && res.success)) {
              cb(res, false);
            } else {
              cb(res, true);
            }
          });
        },
        get2: function get2(path, paras, cb, _token) {
          (_crd && tools === void 0 ? (_reportPossibleCrUseOftools({
            error: Error()
          }), tools) : tools).forBreak(globalThis.intHttpUrl.CdnUrl.map(u => ({
            url: u + path,
            paras: paras,
            token: _token
          })), (_crd && tools === void 0 ? (_reportPossibleCrUseOftools({
            error: Error()
          }), tools) : tools).getReq, function (err, res) {
            console.log('>>post 2222:' + path, JSON.stringify(err), JSON.stringify(res));

            if (res && (res.success || res.data && res.success)) {
              cb(res, false);
            } else {
              cb(res, true);
            }
          });
        },
        postJSON: function postJSON(path, paras, cb) {
          (_crd && tools === void 0 ? (_reportPossibleCrUseOftools({
            error: Error()
          }), tools) : tools).forBreak((_crd && koi === void 0 ? (_reportPossibleCrUseOfkoi({
            error: Error()
          }), koi) : koi).socketInfo.PayUrls.map(u => ({
            url: u + path,
            paras: paras
          })), (_crd && tools === void 0 ? (_reportPossibleCrUseOftools({
            error: Error()
          }), tools) : tools).postJSON, function (err, res) {
            console.log(new Date().toLocaleTimeString('en-US') + ' nettest>>post recv', JSON.stringify(err), JSON.stringify(res));
            console.info(new Date().toLocaleTimeString('en-US') + ' nettest>>post recv' + path, JSON.stringify(err));

            if (err || !res) {
              cb({
                sendCode: 1
              });
            } else {
              cb(res);
            }
          });
        },
        chatSendSucCode: 200
      });

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=req.js.map
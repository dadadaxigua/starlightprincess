System.register(["cc"], function (_export, _context) {
  "use strict";

  var _cclegacy, _decorator, Component, Node, UITransform, v3, view, _dec, _dec2, _dec3, _dec4, _class, _class2, _descriptor, _descriptor2, _descriptor3, _temp, _crd, ccclass, property, StarlightPrincess_weightNode;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  return {
    setters: [function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      Component = _cc.Component;
      Node = _cc.Node;
      UITransform = _cc.UITransform;
      v3 = _cc.v3;
      view = _cc.view;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "94a07E6hDVMlYiqQSEdmJhZ", "StarlightPrincess_weightNode", undefined);

      ({
        ccclass,
        property
      } = _decorator);
      /**
       * Predefined variables
       * Name = StarlightPrincess_weightNode
       * DateTime = Tue Oct 10 2023 17:23:55 GMT+0800 (中国标准时间)
       * Author = dadadaxigua
       * FileBasename = StarlightPrincess_weightNode.ts
       * FileBasenameNoExtension = StarlightPrincess_weightNode
       * URL = db://assets/game/StarlightPrincess_weightNode.ts
       * ManualUrl = https://docs.cocos.com/creator/3.4/manual/zh/
       *
       */

      _export("StarlightPrincess_weightNode", StarlightPrincess_weightNode = (_dec = ccclass('StarlightPrincess_weightNode'), _dec2 = property(Node), _dec3 = property(Node), _dec4 = property(Node), _dec(_class = (_class2 = (_temp = class StarlightPrincess_weightNode extends Component {
        constructor() {
          super(...arguments);

          _initializerDefineProperty(this, "leftNode", _descriptor, this);

          _initializerDefineProperty(this, "RightNode", _descriptor2, this);

          _initializerDefineProperty(this, "stpr_freegame_idle", _descriptor3, this);
        }

        onLoad() {
          var posX1 = view.getVisibleSize().x / 2 - 415;
          var posX2 = view.getVisibleSize().x / 2 - 415;
          this.leftNode.getComponent(UITransform).setContentSize(posX1, 600);
          this.RightNode.getComponent(UITransform).setContentSize(posX2, 600);
          var stpr_freeNodeX = this.stpr_freegame_idle.getComponent(UITransform).contentSize.x * 0.72;
          var scaleX = view.getVisibleSize().x / stpr_freeNodeX + 0.4;
          this.stpr_freegame_idle.scale = v3(scaleX, scaleX, scaleX); // if(scaleX>1){
          //     for(let i=1;i<=10;i++){
          //         if(stpr_freeNodeX*(scaleX+i*0.1)>view.getVisibleSize().x){
          //             scaleX=scaleX+i*0.1;
          //             this.stpr_freegame_idle.scale=v3(scaleX,scaleX,scaleX);
          //         }else{
          //         };
          //     };
          // };
        }

        start() {// [3]
        } // update (deltaTime: number) {
        //     // [4]
        // }


      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "leftNode", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "RightNode", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "stpr_freegame_idle", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class));
      /**
       * [1] Class member could be defined like this.
       * [2] Use `property` decorator if your want the member to be serializable.
       * [3] Your initialization goes here.
       * [4] Your update function goes here.
       *
       * Learn more about scripting: https://docs.cocos.com/creator/3.4/manual/zh/scripting/
       * Learn more about CCClass: https://docs.cocos.com/creator/3.4/manual/zh/scripting/decorator.html
       * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.4/manual/zh/scripting/life-cycle-callbacks.html
       */


      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=StarlightPrincess_weightNode.js.map
System.register(["__unresolved_0", "cc", "__unresolved_1"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, director, sUtil, _dec, _class, _class2, _temp, _crd, ccclass, property, sBonusCtr;

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _reportPossibleCrUseOfsUtil(extras) {
    _reporterNs.report("sUtil", "../../../../game/core/sUtil", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      director = _cc.director;
    }, function (_unresolved_2) {
      sUtil = _unresolved_2.sUtil;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "f46a7AHIVlGK7xOAT9WVMlL", "sBonusCtr", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("sBonusCtr", sBonusCtr = (_dec = ccclass('sBonusCtr'), _dec(_class = (_temp = _class2 = class sBonusCtr {
        constructor() {
          _defineProperty(this, "gameid", 0);

          _defineProperty(this, "demc", 2);

          _defineProperty(this, "bonusList", []);

          _defineProperty(this, "bonusMap", new Map());

          _defineProperty(this, "mInterval", null);
        }

        start(sec) {
          this.update(0);
          this.mInterval = setInterval(() => {
            this.update(0);
          }, sec * 1000);
        }

        clear() {
          clearInterval(this.mInterval);
        }

        static Init(finishCall, sec) {
          if (finishCall === void 0) {
            finishCall = null;
          }

          if (sec === void 0) {
            sec = 0.1;
          }

          // console.log('sBonusCtr Init');
          var bonusCtrinit = () => {
            // if (!sBonusCtr.bonusCtr || !sBonusCtr.bonusCtr.isValid || !sBonusCtr.bonusCtr.node || !sBonusCtr.bonusCtr.node.isValid) {
            //     globalThis.getSubGamePrefabByPath('bonus/bonusCtr', (obj: Node) => {
            //         if (obj) {
            //             const scene = director.getScene();
            //             if (scene && scene.children && scene.children.length > 0) {
            //                 scene.children[0].addChild(obj);
            //                 const com = obj.getComponent(sBonusCtr);
            //                 sBonusCtr.bonusCtr = com;
            //                 if (finishCall) {
            //                     finishCall();
            //                 }
            //             }
            //         }
            //     });
            // }
            if (sBonusCtr.bonusCtr) {
              sBonusCtr.bonusCtr.clear();
            }

            sBonusCtr.bonusCtr = new sBonusCtr();
            sBonusCtr.bonusCtr.start(sec);

            if (finishCall) {
              finishCall();
            }
          };

          if (!sBonusCtr.loaded) {
            // if(globalThis.getCoinRate && globalThis.getCoinRate() > 1){
            // }
            globalThis.subGameSocketReq('user.jackpotHandler.jackpot', {}, (err, res) => {
              if (!err && res && res.data) {
                sBonusCtr.loaded = true;

                if (res.data && Array.isArray(res.data)) {
                  for (var i = 0; i < res.data.length; i++) {
                    var data = res.data[i];
                    sBonusCtr.AddJackpotData(data.jackpot_id, data.game_id, data.last_jackpot, data.jackpot_speed, data.last_jackpot_time);
                  }
                }

                bonusCtrinit();
              } else {}
            });
          } else {
            bonusCtrinit();
          }
        }

        static ViewUpdate() {
          if (sBonusCtr.bonusCtr) {
            sBonusCtr.bonusCtr.update(0);
          }
        }

        static AddJackpotData(_jackpot_id, _game_id, _last_jackpot, _jackpot_speed, _last_jackpot_time) {
          var jackpot = {
            jackpot_id: _jackpot_id,
            game_id: _game_id,
            last_jackpot: _last_jackpot,
            jackpot_speed: _jackpot_speed,
            last_jackpot_time: _last_jackpot_time,
            serverTime: globalThis.getServerTime(),
            enable: true
          }; // console.log('sJackpotEntityViewCtr,true : ',_jackpot_id);

          director.emit('sJackpotEntityViewCtr', _jackpot_id, true);
          sBonusCtr.bonusDataMap.set(_jackpot_id, jackpot);
        }

        static AddOneJackpotEntity(jackpotId, labels) {
          // console.log('sBonusCtr Bind');
          if (sBonusCtr.bonusCtr && sBonusCtr.bonusCtr.bonusMap) {
            var bonusEntity = sBonusCtr.bonusCtr.bonusMap.get(jackpotId);

            if (bonusEntity) {
              bonusEntity.targetLabels = labels;
            } else {
              var bonus = {
                targetLabels: labels,
                jackpot_id: jackpotId
              };
              sBonusCtr.bonusCtr.bonusList.push(bonus);
              sBonusCtr.bonusCtr.bonusMap.set(jackpotId, bonus);
            }
          }
        }

        static JackPotBonusEnable(jackpotId) {
          if (sBonusCtr.bonusDataMap) {
            var jackpot = sBonusCtr.bonusDataMap.get(jackpotId);

            if (jackpot) {
              return jackpot.enable;
            }
          }

          return false;
        }

        update(dt) {
          if (this.bonusList) {
            var localTime = globalThis.getServerTime();

            for (var i = 0; i < this.bonusList.length; i++) {
              var bonus = this.bonusList[i];

              if (bonus) {
                var bonusData = sBonusCtr.bonusDataMap.get(bonus.jackpot_id);

                if (bonusData) {
                  if (bonusData.enable) {
                    // bonusData.serverTime += dt;
                    if (bonus.targetLabels) {
                      for (var l = 0; l < bonus.targetLabels.length; l++) {
                        var targetLabel = bonus.targetLabels[l];

                        if (targetLabel && targetLabel.node && targetLabel.node.activeInHierarchy && targetLabel.node.isValid) {
                          targetLabel.string = (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                            error: Error()
                          }), sUtil) : sUtil).AddCommas((localTime - bonusData.last_jackpot_time) * bonusData.jackpot_speed / 1000 + bonusData.last_jackpot, this.demc); // console.log('bonusData.serverTime:',bonusData.serverTime);
                          // console.log('targetLabel.string:',(bonusData.serverTime - bonusData.last_jackpot_time) + ', '+targetLabel.string);
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }

      }, _defineProperty(_class2, "bonusDataMap", new Map()), _defineProperty(_class2, "loaded", false), _defineProperty(_class2, "bonusCtr", void 0), _temp)) || _class));

      director.on('socket_backToFront_game', () => {
        var serverTime = globalThis.getServerTime();

        if (sBonusCtr.bonusDataMap) {
          sBonusCtr.bonusDataMap.forEach((bonus, key) => {
            bonus.serverTime = serverTime;
          });
        }
      });
      director.on('socketConnectToGame', type => {
        if (type == 1) {
          globalThis.subGameSocketReq('user.jackpotHandler.jackpot', {}, (err, res) => {
            if (!err && res && res.data) {
              sBonusCtr.loaded = true;

              if (sBonusCtr.bonusDataMap) {
                sBonusCtr.bonusDataMap.forEach((value, key) => {
                  value.enable = false;
                });
              }

              if (res.data && Array.isArray(res.data)) {
                for (var i = 0; i < res.data.length; i++) {
                  var data = res.data[i];
                  sBonusCtr.AddJackpotData(data.jackpot_id, data.game_id, data.last_jackpot, data.jackpot_speed, data.last_jackpot_time);
                }
              }

              var bonusDataList = [];
              sBonusCtr.bonusDataMap.forEach((value, key) => {
                if (!value.enable) {
                  bonusDataList.push(key); // console.log('sJackpotEntityViewCtr,false : ',key);

                  director.emit('sJackpotEntityViewCtr', key, false);
                }
              });

              if (bonusDataList.length > 0) {
                for (var _i = 0; _i < bonusDataList.length; _i++) {
                  var bonusKey = bonusDataList[_i];
                  sBonusCtr.bonusDataMap.delete(bonusKey);
                }
              }
            } else {}
          });
        } else if (type == 2) {}
      }, void 0);
      director.on('onBroadcastNotice', data => {
        if (data) {
          if (data.type == 'jackpot') {
            if (data.msg && data.msg.content) {
              var msg = JSON.parse(data.msg.content);

              if (msg && Array.isArray(msg)) {
                if (sBonusCtr.bonusDataMap) {
                  sBonusCtr.bonusDataMap.forEach((value, key) => {
                    value.enable = false;
                  });
                }

                for (var i = 0; i < msg.length; i++) {
                  var item = msg[i];

                  if (item) {
                    var _bonus = sBonusCtr.bonusDataMap.get(item.jackpot_id);

                    if (_bonus) {
                      _bonus.enable = true;
                      _bonus.serverTime = globalThis.getServerTime();
                      _bonus.last_jackpot_time = item.last_jackpot_time;
                      _bonus.last_jackpot = item.last_jackpot;
                      _bonus.jackpot_speed = item.jackpot_speed;
                    } else {
                      sBonusCtr.AddJackpotData(item.jackpot_id, item.game_id, item.last_jackpot, item.jackpot_speed, item.last_jackpot_time);
                    }
                  }
                }
              }

              var bonusDataList = [];
              sBonusCtr.bonusDataMap.forEach((value, key) => {
                if (!value.enable) {
                  bonusDataList.push(key); // console.log('sJackpotEntityViewCtr,false : ',key);

                  director.emit('sJackpotEntityViewCtr', key, false);
                }
              });

              if (bonusDataList.length > 0) {
                for (var _i2 = 0; _i2 < bonusDataList.length; _i2++) {
                  var bonusKey = bonusDataList[_i2];
                  sBonusCtr.bonusDataMap.delete(bonusKey);
                }
              }
            }
          }
        }
      });

      globalThis.jackPotBonusLabelInit = function (finishCall, sec) {
        if (finishCall === void 0) {
          finishCall = null;
        }

        if (sec === void 0) {
          sec = 0.1;
        }

        sBonusCtr.Init(finishCall, sec);
      };

      globalThis.jackPotBonusLabelInitBind = function (jackpotId, labels) {
        sBonusCtr.AddOneJackpotEntity(jackpotId, labels);
      };

      globalThis.jackPotBonusLabelUpdate = function () {
        sBonusCtr.ViewUpdate();
      };

      globalThis.jackPotBonusEnable = function (jackpotId) {
        return sBonusCtr.JackPotBonusEnable(jackpotId);
      };

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=sBonusCtr.js.map
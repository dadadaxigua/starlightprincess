System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, Input, v3, director, sComponent, sFitScreen, _dec, _class, _temp, _crd, ccclass, property, sTouchView;

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _reportPossibleCrUseOfsComponent(extras) {
    _reporterNs.report("sComponent", "./sComponent", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsFitScreen(extras) {
    _reporterNs.report("sFitScreen", "./sFitScreen", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      Input = _cc.Input;
      v3 = _cc.v3;
      director = _cc.director;
    }, function (_unresolved_2) {
      sComponent = _unresolved_2.sComponent;
    }, function (_unresolved_3) {
      sFitScreen = _unresolved_3.sFitScreen;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "0d62deS9UlCGJHfa7bqZ5g6", "sTouchView", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("sTouchView", sTouchView = (_dec = ccclass('sTouchView'), _dec(_class = (_temp = class sTouchView extends (_crd && sComponent === void 0 ? (_reportPossibleCrUseOfsComponent({
        error: Error()
      }), sComponent) : sComponent) {
        constructor() {
          super(...arguments);

          _defineProperty(this, "touchDown", false);

          _defineProperty(this, "m_Time", 0);

          _defineProperty(this, "m_Dir", null);
        }

        start() {
          this.node.on(Input.EventType.TOUCH_START, event => {
            // console.log('start'+event.getLocationX());
            // console.log('ui'+event.getUILocation());
            this.touchDown = true;
            var pos = event.getUILocation();
            var windowSize = (_crd && sFitScreen === void 0 ? (_reportPossibleCrUseOfsFitScreen({
              error: Error()
            }), sFitScreen) : sFitScreen).GetWindowSize();
            this.m_Dir = v3(pos.x - windowSize.x / 2, pos.y - windowSize.y / 2, 0);
            director.emit('fishTouchViewPos', this.m_Dir); // this.node.children[0].position = v3(pos.x - windowSize.x/2,pos.y - windowSize.y/2,0);
          }, this);
          this.node.on(Input.EventType.TOUCH_MOVE, event => {
            var pos = event.getUILocation();
            var windowSize = (_crd && sFitScreen === void 0 ? (_reportPossibleCrUseOfsFitScreen({
              error: Error()
            }), sFitScreen) : sFitScreen).GetWindowSize();
            this.m_Dir = v3(pos.x - windowSize.x / 2, pos.y - windowSize.y / 2, 0);
          }, this);
          this.node.on(Input.EventType.TOUCH_END, event => {
            this.touchDown = false;
          }, this);
          this.node.on(Input.EventType.TOUCH_CANCEL, event => {
            this.touchDown = false;
          }, this);
        }

        update(dt) {
          if (this.touchDown) {
            if (this.m_Time > 0.1) {
              this.m_Time -= 0.1;
              director.emit('fishTouchViewPos', this.m_Dir);
            }

            this.m_Time += dt;
          }
        }

      }, _temp)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=sTouchView.js.map
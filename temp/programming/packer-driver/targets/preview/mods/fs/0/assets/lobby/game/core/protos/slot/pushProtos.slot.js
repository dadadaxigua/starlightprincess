System.register(["cc"], function (_export, _context) {
  "use strict";

  var _cclegacy, _crd;

  return {
    setters: [function (_cc) {
      _cclegacy = _cc.cclegacy;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "182f8T1DnpFL6sIkpudPCer", "pushProtos.slot", undefined);

      _export("default", {
        "nested": {
          "slot": {
            "nested": {
              "msg": {
                "nested": {
                  "end": {
                    "fields": {
                      "game_mode": {
                        "type": "int32",
                        "id": 1
                      },
                      "rate": {
                        "type": "double",
                        "id": 2
                      },
                      "seq": {
                        "type": "int32",
                        "id": 3
                      },
                      "coin": {
                        "type": "double",
                        "id": 4
                      },
                      "bonus_type": {
                        "type": "string",
                        "id": 5
                      }
                    }
                  }
                }
              }
            }
          }
        }
      });

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=pushProtos.slot.js.map
System.register(["cc"], function (_export, _context) {
  "use strict";

  var _cclegacy, _crd;

  return {
    setters: [function (_cc) {
      _cclegacy = _cc.cclegacy;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "23078mYxgVET4Hu0Q9cBTM1", "jackpotClientProtos", undefined);

      _export("default", {
        "nested": {
          "user": {
            "nested": {
              "jackpotHandler": {
                "nested": {
                  "jackpot": {
                    "fields": {}
                  }
                }
              }
            }
          }
        }
      });

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=jackpotClientProtos.js.map
System.register(["cc"], function (_export, _context) {
  "use strict";

  var _cclegacy, _decorator, Component, Node, Sprite, director, Color, _dec, _dec2, _dec3, _class, _class2, _descriptor, _descriptor2, _temp, _crd, ccclass, property, gameRecordImgItem;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  return {
    setters: [function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      Component = _cc.Component;
      Node = _cc.Node;
      Sprite = _cc.Sprite;
      director = _cc.director;
      Color = _cc.Color;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "50b8f61wipLT59HtO/Vs2Y0", "gameRecordImgItem", undefined);

      ({
        ccclass,
        property
      } = _decorator);
      /**
       * Predefined variables
       * Name = gameRecordImgItem
       * DateTime = Thu Nov 03 2022 13:25:56 GMT+0800 (中国标准时间)
       */

      _export("gameRecordImgItem", gameRecordImgItem = (_dec = ccclass('gameRecordImgItem'), _dec2 = property(Sprite), _dec3 = property(Node), _dec(_class = (_class2 = (_temp = class gameRecordImgItem extends Component {
        constructor() {
          super(...arguments);

          _initializerDefineProperty(this, "sprite", _descriptor, this);

          _initializerDefineProperty(this, "replayBtn", _descriptor2, this);

          _defineProperty(this, "id", void 0);

          _defineProperty(this, "gameId", void 0);

          _defineProperty(this, "tableId", void 0);

          _defineProperty(this, "roundId", void 0);

          _defineProperty(this, "colors", [new Color("343440"), new Color("30303c")]);
        }

        onLoad() {
          this.replayBtn.on('click', () => {
            console.log('id:', this.id);
            director.emit('gameRecordReplay', {
              id: this.id,
              gameId: this.gameId,
              tableId: this.tableId,
              roundId: this.roundId
            });
          }, this);
        }

        init(index, gameId, id, recordType, profit, tableId, roundId) {
          if (recordType == 'solt') {
            this.sprite.color = this.colors[index % 2];
          } else if (recordType == 'poker') {
            if (profit == null) {
              this.replayBtn.active = false;
            } else {
              this.replayBtn.active = true;
            }

            this.sprite.node.active = index % 2 == 0 ? true : false;
          }

          this.tableId = tableId;
          this.gameId = gameId;
          this.id = id;
          this.roundId = roundId;
        } // update (deltaTime: number) {
        //     // [4]
        // }


      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "sprite", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "replayBtn", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class));
      /**
       * [1] Class member could be defined like this.
       * [2] Use `property` decorator if your want the member to be serializable.
       * [3] Your initialization goes here.
       * [4] Your update function goes here.
       *
       * Learn more about scripting: https://docs.cocos.com/creator/3.4/manual/zh/scripting/
       * Learn more about CCClass: https://docs.cocos.com/creator/3.4/manual/zh/scripting/decorator.html
       * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.4/manual/zh/scripting/life-cycle-callbacks.html
       */


      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=gameRecordImgItem.js.map
System.register(["__unresolved_0", "cc", "__unresolved_1"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, Vec3, sComponent, _dec, _class, _temp, _crd, ccclass, property, sPokerCardBase;

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _reportPossibleCrUseOfsComponent(extras) {
    _reporterNs.report("sComponent", "./sComponent", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      Vec3 = _cc.Vec3;
    }, function (_unresolved_2) {
      sComponent = _unresolved_2.sComponent;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "e9be0voAb1PEbuE7cAXIh6a", "sPokerCardBase", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("sPokerCardBase", sPokerCardBase = (_dec = ccclass('sPokerCardBase'), _dec(_class = (_temp = class sPokerCardBase extends (_crd && sComponent === void 0 ? (_reportPossibleCrUseOfsComponent({
        error: Error()
      }), sComponent) : sComponent) {
        constructor() {
          super(...arguments);

          _defineProperty(this, "cardPoint", void 0);

          _defineProperty(this, "state", 'open');

          _defineProperty(this, "actionArr", {});
        }

        get CardPoint() {
          return this.cardPoint;
        }

        //open inHand select out close
        get State() {
          return this.state;
        }

        set State(value) {
          this.state = value;
        }

        start() {}

        updatePoint(point) {
          this.cardPoint = point;
        }

        openForeCard(anima) {
          if (anima === void 0) {
            anima = true;
          }
        }

        openBackCard(anima) {
          if (anima === void 0) {
            anima = true;
          }
        }

        clearCard() {
          this.unscheduleAllCallbacks();
          this.cleanTweenList();
          this.node.parent = null;
          this.CtrAllChildActive(this.node, false);
          this.cardTouchEnableCtr(false);
          this.State = 'close';
          this.node.scale = Vec3.ONE;
          this.node.eulerAngles = Vec3.ZERO;
          this.cardPoint = '0';
        }

        setBackCardScale(value) {}

        setForeCardScale(value) {}

        getForeCard() {
          return null;
        }

        getBackCard() {
          return null;
        }

        cardTouchEnableCtr(value) {}

        cardPosReset() {}

        cardSelected() {
          var sel = this.State;
          this.State = sel == 'select' ? 'inHand' : 'select';
        }

        setRenderColor(_color) {}
        /**
         * @zh
         * 注册以名字为key的行为
         * @param _name - 行为名称
         * @param call - 行为回调
         */


        RegisterEnitityAction(_name, call) {
          if (_name && call) {
            this.actionArr[_name] = call;
          }
        }
        /**
         * @zh
         * 触发以名字为key的行为
         * @param _name - 行为名称
         * @param param - 参数
         */


        DoEnitityAction(_name, param) {
          if (param === void 0) {
            param = null;
          }

          var action = this.actionArr[_name];

          if (action) {
            action(param);
          }
        }

      }, _temp)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=sPokerCardBase.js.map
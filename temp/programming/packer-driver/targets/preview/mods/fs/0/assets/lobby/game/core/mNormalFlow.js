System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2", "__unresolved_3", "__unresolved_4", "__unresolved_5", "__unresolved_6"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, director, macro, sAudioMgr, sBoxMgr, sConfigMgr, sNormalFlow, sObjPool, sUtil, _dec, _class, _temp, _crd, ccclass, property, mNormalFlow;

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _reportPossibleCrUseOfsAudioMgr(extras) {
    _reporterNs.report("sAudioMgr", "./sAudioMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsBoxMgr(extras) {
    _reporterNs.report("sBoxMgr", "./sBoxMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsConfigMgr(extras) {
    _reporterNs.report("sConfigMgr", "./sConfigMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsNormalFlow(extras) {
    _reporterNs.report("sNormalFlow", "./sNormalFlow", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsObjPool(extras) {
    _reporterNs.report("sObjPool", "./sObjPool", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsUtil(extras) {
    _reporterNs.report("sUtil", "./sUtil", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      director = _cc.director;
      macro = _cc.macro;
    }, function (_unresolved_2) {
      sAudioMgr = _unresolved_2.default;
    }, function (_unresolved_3) {
      sBoxMgr = _unresolved_3.sBoxMgr;
    }, function (_unresolved_4) {
      sConfigMgr = _unresolved_4.sConfigMgr;
    }, function (_unresolved_5) {
      sNormalFlow = _unresolved_5.sNormalFlow;
    }, function (_unresolved_6) {
      sObjPool = _unresolved_6.sObjPool;
    }, function (_unresolved_7) {
      sUtil = _unresolved_7.sUtil;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "128dbGwF4xBdrIJcmIiguFT", "mNormalFlow", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("mNormalFlow", mNormalFlow = (_dec = ccclass('mNormalFlow'), _dec(_class = (_temp = class mNormalFlow extends (_crd && sNormalFlow === void 0 ? (_reportPossibleCrUseOfsNormalFlow({
        error: Error()
      }), sNormalFlow) : sNormalFlow) {
        constructor() {
          super(...arguments);

          _defineProperty(this, "overEffectArr", []);
        }

        start() {
          super.start();
        }

        rollStopTurboAction(round, rollSpeedMode, clickMode) {
          if (round) {
            this.turboFlowDelayCall(() => {
              (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
                error: Error()
              }), sConfigMgr) : sConfigMgr).instance.SetResData(round['round_symbol_map']);
              (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                error: Error()
              }), sUtil) : sUtil).once('rollStopOneRoundCall', () => {
                director.off('rollOneColumnStop');
                director.off('rollOneColumnBump');
                this.rollShowResAction(round);
              });
              director.on('rollOneColumnBump', col => {
                if (col == 0) {
                  (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                    error: Error()
                  }), sAudioMgr) : sAudioMgr).PlayShotAudio('booo');
                }

                (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                  error: Error()
                }), sAudioMgr) : sAudioMgr).StopAudio();

                var _boxViewSize = (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                  error: Error()
                }), sBoxMgr) : sBoxMgr).instance.getBoxViewSize();

                var _boxSize = (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                  error: Error()
                }), sBoxMgr) : sBoxMgr).instance.getBoxSize();

                var round_symbol_map = round['round_symbol_map'];

                if (round_symbol_map) {
                  var girlBingo = false;
                  var scatterBingo = false;
                  var column = round_symbol_map[col];

                  if (column && Array.isArray(column)) {
                    var yDelta = (_boxSize.y - _boxViewSize.y) / 2;

                    for (var i = yDelta; i < yDelta + _boxViewSize.y; i++) {
                      var data = column[i];

                      if (data >= 1000) {
                        girlBingo = true;
                      } else if (data == 1) {
                        scatterBingo = true;
                      }

                      if (i == yDelta) {
                        if (data == 1004) {
                          (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                            error: Error()
                          }), sAudioMgr) : sAudioMgr).PlayShotAudio('girlSound1_' + ((_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                            error: Error()
                          }), sUtil) : sUtil).RandomInt(0, 2) == 0 ? '0' : '1'));
                        } else if (data == 2004) {
                          (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                            error: Error()
                          }), sAudioMgr) : sAudioMgr).PlayShotAudio('girlSound2_' + ((_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                            error: Error()
                          }), sUtil) : sUtil).RandomInt(0, 2) == 0 ? '0' : '1'));
                        } else if (data == 3004) {
                          (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                            error: Error()
                          }), sAudioMgr) : sAudioMgr).PlayShotAudio('girlSound3_' + ((_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                            error: Error()
                          }), sUtil) : sUtil).RandomInt(0, 2) == 0 ? '0' : '1'));
                        } else if (data == 4004) {
                          (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                            error: Error()
                          }), sAudioMgr) : sAudioMgr).PlayShotAudio('girlSound4_' + ((_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                            error: Error()
                          }), sUtil) : sUtil).RandomInt(0, 2) == 0 ? '0' : '1'));
                        } else if (data == 5004) {
                          (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                            error: Error()
                          }), sAudioMgr) : sAudioMgr).PlayShotAudio('girlSound5_' + ((_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                            error: Error()
                          }), sUtil) : sUtil).RandomInt(0, 2) == 0 ? '0' : '1'));
                        }
                      }
                    }
                  }

                  if (girlBingo) {
                    (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                      error: Error()
                    }), sAudioMgr) : sAudioMgr).PlayShotAudio('girlBingo');
                  }

                  if (scatterBingo) {
                    (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                      error: Error()
                    }), sAudioMgr) : sAudioMgr).PlayShotAudio('scatterBingo');
                  }
                }
              }, this); // for (let i = 0; i < 5; i++) {
              //     sBoxMgr.instance.rollStopBoxBySelf(i,0,false,(_col)=>{
              //         if(_col == 4){
              //             director.emit('rollStopOneRoundCall');
              //         }
              //     });              
              // }

              (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                error: Error()
              }), sBoxMgr) : sBoxMgr).instance.rollStopAllColumn();
            });
          }
        }

        rollStopAction(round) {
          var _boxViewSize = (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
            error: Error()
          }), sBoxMgr) : sBoxMgr).instance.getBoxViewSize();

          var _boxSize = (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
            error: Error()
          }), sBoxMgr) : sBoxMgr).instance.getBoxSize();

          var round_symbol_map = round['round_symbol_map'];
          (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
            error: Error()
          }), sUtil) : sUtil).once('rollStopOneRoundCall', () => {
            director.off('rollOneColumnStop');
            director.off('rollOneColumnBump');
            this.rollShowResAction(round);
          });
          director.on('rollOneColumnBump', col => {
            if (col == _boxViewSize.x - 1) {
              (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                error: Error()
              }), sAudioMgr) : sAudioMgr).StopAudio();
            }

            (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
              error: Error()
            }), sAudioMgr) : sAudioMgr).PlayShotAudio('boxRowStop');

            if (round_symbol_map) {
              var girlBingo = false;
              var scatterBingo = false;
              var column = round_symbol_map[col];

              if (column && Array.isArray(column)) {
                var yDelta = (_boxSize.y - _boxViewSize.y) / 2;

                for (var i = yDelta; i < yDelta + _boxViewSize.y; i++) {
                  var data = column[i];

                  if (data >= 1000) {
                    girlBingo = true;
                  } else if (data == 1) {
                    scatterBingo = true;
                  }

                  if (i == yDelta) {
                    if (data == 1004) {
                      (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                        error: Error()
                      }), sAudioMgr) : sAudioMgr).PlayShotAudio('girlSound1_' + ((_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                        error: Error()
                      }), sUtil) : sUtil).RandomInt(0, 2) == 0 ? '0' : '1'));
                    } else if (data == 2004) {
                      (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                        error: Error()
                      }), sAudioMgr) : sAudioMgr).PlayShotAudio('girlSound2_' + ((_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                        error: Error()
                      }), sUtil) : sUtil).RandomInt(0, 2) == 0 ? '0' : '1'));
                    } else if (data == 3004) {
                      (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                        error: Error()
                      }), sAudioMgr) : sAudioMgr).PlayShotAudio('girlSound3_' + ((_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                        error: Error()
                      }), sUtil) : sUtil).RandomInt(0, 2) == 0 ? '0' : '1'));
                    } else if (data == 4004) {
                      (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                        error: Error()
                      }), sAudioMgr) : sAudioMgr).PlayShotAudio('girlSound4_' + ((_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                        error: Error()
                      }), sUtil) : sUtil).RandomInt(0, 2) == 0 ? '0' : '1'));
                    } else if (data == 5004) {
                      (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                        error: Error()
                      }), sAudioMgr) : sAudioMgr).PlayShotAudio('girlSound5_' + ((_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                        error: Error()
                      }), sUtil) : sUtil).RandomInt(0, 2) == 0 ? '0' : '1'));
                    }
                  }
                }
              }

              if (girlBingo) {
                (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                  error: Error()
                }), sAudioMgr) : sAudioMgr).PlayShotAudio('girlBingo');
              }

              if (scatterBingo) {
                (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                  error: Error()
                }), sAudioMgr) : sAudioMgr).PlayShotAudio('scatterBingo');
              }
            }
          }, this);

          if (round) {
            this.normalFlowDelayCall(() => {
              (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
                error: Error()
              }), sConfigMgr) : sConfigMgr).instance.SetResData(round['round_symbol_map']);
              var events = (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
                error: Error()
              }), sConfigMgr) : sConfigMgr).instance.GetEventConfigByHitListOnCheck(round);
              var boxSize = (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                error: Error()
              }), sBoxMgr) : sBoxMgr).instance.getBoxViewSize();
              var freeWinArr = [],
                  targetColumn = [];

              if (events) {
                for (var i = 0; i < events.length; i++) {
                  var _event = events[i];

                  if (_event.event_type == 'speedAnima') {
                    var actColumn = _event.act_column.split(',');

                    if (actColumn && actColumn.length > 0) {
                      for (var a = 0; a < actColumn.length; a++) {
                        var element = actColumn[a];
                        freeWinArr.push(parseInt(element));
                      }
                    }
                  }
                }
              }

              if (freeWinArr.length > 0) {
                for (var _i = 0; _i < boxSize.x; _i++) {
                  if (freeWinArr.indexOf(_i) < 0) {
                    targetColumn.push(_i);
                  }
                }

                (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                  error: Error()
                }), sBoxMgr) : sBoxMgr).instance.rollStopByColumnArr(targetColumn, col => {
                  (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                    error: Error()
                  }), sBoxMgr) : sBoxMgr).instance.setColumnAllTargetBoxArrayState(col, symbol => {
                    return symbol > 1000;
                  }, 'spawn', 'settlement');
                  (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                    error: Error()
                  }), sBoxMgr) : sBoxMgr).instance.setColumnTargetBoxArrayState(col, symbol => {
                    return symbol == 1;
                  }, 'win', 'settlement');
                  this.scheduleOnce(() => {
                    (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                      error: Error()
                    }), sBoxMgr) : sBoxMgr).instance.setColumnTargetBoxArrayState(col, symbol => {
                      return symbol == 1;
                    }, 'idle', 'settlement');
                  }, 1);

                  if (col == targetColumn.length - 1) {
                    this.clearColumnOverEffect();
                    (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                      error: Error()
                    }), sAudioMgr) : sAudioMgr).PlayShotAudio('freeWinOneColBingo');
                    this.showColumnOverEffect(targetColumn.length);
                    this.scheduleOnce(() => {
                      (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                        error: Error()
                      }), sBoxMgr) : sBoxMgr).instance.blackCurtainAnima(targetColumn.length);
                    }, 0.5);
                  }
                }, () => {
                  if (freeWinArr.length > 0) {
                    var index = 0;

                    var freeWinCall = () => {
                      if (index < freeWinArr.length) {
                        var col = freeWinArr[index++];
                        this.setMotionTweenSpeedByIndex(col);
                        this.freeWinSpeedAnimaDelayCall(col, () => {
                          (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                            error: Error()
                          }), sBoxMgr) : sBoxMgr).instance.rollStopBoxBySelf(col, 0, false, _col => {
                            (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                              error: Error()
                            }), sBoxMgr) : sBoxMgr).instance.setColumnAllTargetBoxArrayState(col, symbol => {
                              return symbol > 1000;
                            }, 'spawn', 'settlement');
                            (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                              error: Error()
                            }), sBoxMgr) : sBoxMgr).instance.setColumnTargetBoxArrayState(col, symbol => {
                              return symbol == 1;
                            }, 'win', 'settlement');
                            this.scheduleOnce(() => {
                              (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                                error: Error()
                              }), sBoxMgr) : sBoxMgr).instance.setColumnTargetBoxArrayState(col, symbol => {
                                return symbol == 1;
                              }, 'idle', 'settlement');
                            }, 1);

                            if (freeWinArr.indexOf(col + 1) >= 0) {
                              this.clearColumnOverEffect();
                              (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                                error: Error()
                              }), sAudioMgr) : sAudioMgr).PlayShotAudio('freeWinOneColBingo');
                              (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                                error: Error()
                              }), sBoxMgr) : sBoxMgr).instance.blackCurtain(col + 1);
                              this.showColumnOverEffect(_col + 1);
                            }

                            if (_col == boxSize.x - 1) {
                              this.clearColumnOverEffect();
                              (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                                error: Error()
                              }), sBoxMgr) : sBoxMgr).instance.blackCurtainAnima(-1, false);
                              director.emit('rollStopOneRoundCall');
                            }

                            freeWinCall();
                          });
                        });
                      }
                    };

                    freeWinCall();
                  }
                });
              } else {
                (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                  error: Error()
                }), sBoxMgr) : sBoxMgr).instance.rollStopAllColumn();
              }
            });
          }
        }

        byLineBoxLightCtr(round) {
          var events = (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
            error: Error()
          }), sConfigMgr) : sConfigMgr).instance.GetEventConfigByHitListOnCheck(round);
          var titleAnimaArr = [];
          var isWin = false;
          var eventList = [];

          if (events) {
            var resActObj = {};

            for (var i = 0; i < events.length; i++) {
              var _event = events[i];

              if (_event['event_type'] == 'boxAnima') {
                eventList.push(_event);

                var acts = _event['act_pos'].split(',');

                if (acts) {
                  isWin = true;

                  for (var a = 0; a < acts.length; a++) {
                    var act = acts[a];
                    resActObj[act] = true;
                  }
                }
              } else if (_event['event_type'] == 'titleAnima') {
                var _acts = _event['act_pos'].split(',');

                if (_acts) {
                  for (var _a = 0; _a < _acts.length; _a++) {
                    var _act = _acts[_a];
                    resActObj[_act] = true;
                  }
                }

                titleAnimaArr.push(_event['act_column']);
              }
            } // console.log('clickMode:',this.clickMode);


            if (isWin) {
              // console.error('normalWin');
              (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                error: Error()
              }), sAudioMgr) : sAudioMgr).PlayShotAudio('normalWin');
              (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                error: Error()
              }), sBoxMgr) : sBoxMgr).instance.blackCurtainAnima();
              director.emit('boxWinInform');
              this.byLineBoxLightOn(resActObj);
            } else {}
          }

          if (titleAnimaArr.length > 0) {
            director.emit('titleMultipleAnima', titleAnimaArr, isWin);
          }

          if (isWin) {
            (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
              error: Error()
            }), sUtil) : sUtil).once('titleWinSettleEnd', () => {
              var index = 0;

              this.roundShowBoxLineInterval = () => {
                if (globalThis.GameBtnEntity.betClickMode == 'normal') {
                  if (index >= eventList.length) {
                    index = 0;
                  }

                  var res = eventList[index++];

                  if (res && res['act_pos']) {
                    var _acts2 = res['act_pos'].split(',');

                    if (_acts2) {
                      var resSingleActObj = {};

                      for (var _a2 = 0; _a2 < _acts2.length; _a2++) {
                        var _act2 = _acts2[_a2];
                        resSingleActObj[_act2] = true;
                      }

                      (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                        error: Error()
                      }), sAudioMgr) : sAudioMgr).PlayShotAudio('oneLineShow');
                      this.byLineBoxLightOn(resSingleActObj);
                    }
                  }
                }
              };

              this.schedule(this.roundShowBoxLineInterval, 2, macro.REPEAT_FOREVER);
              this.titleWinSettleEnd(round);
            });

            if (titleAnimaArr.length > 0) {
              var multiSum = 1;

              for (var _i2 = 0; _i2 < titleAnimaArr.length; _i2++) {
                var value = titleAnimaArr[_i2];
                multiSum *= parseInt(value) + 1;
              }

              var realRate = round.rate;

              if (!(titleAnimaArr.length == 1 && titleAnimaArr[0] == 0)) {
                director.emit('lowWinTiltleAnima', Math.floor(globalThis.GameBtnEntity.CurrentBetAmount * round.rate / multiSum));
              }

              if (titleAnimaArr.length == 1 && titleAnimaArr[0] == 0) {
                director.emit('winBetRes', globalThis.GameBtnEntity.CurrentBetAmount * realRate, realRate);
              } else {
                (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                  error: Error()
                }), sUtil) : sUtil).once('titleMultipleAnimaEnd', () => {
                  director.emit('winBetRes', globalThis.GameBtnEntity.CurrentBetAmount * realRate, realRate);
                });
              }
            } else {
              director.emit('winBetRes', globalThis.GameBtnEntity.CurrentBetAmount * round.rate, round.rate);
            }
          } else {
            this.titleWinSettleEnd(round);
          }
        }

        showColumnOverEffect(col) {
          director.emit('freeGameStart', col);
          var res = (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
            error: Error()
          }), sBoxMgr) : sBoxMgr).instance.GetColAllBoxPos(col);

          if (res && res.length > 0) {
            for (var i = 0; i < res.length; i++) {
              var pos = res[i];
              var overEffect = (_crd && sObjPool === void 0 ? (_reportPossibleCrUseOfsObjPool({
                error: Error()
              }), sObjPool) : sObjPool).Dequeue('overLayerEffectItem');
              overEffect.node.position = pos;
              overEffect.node.active = true;
              overEffect.boxItemUpdate(null, null, 'win', null);
              this.overEffectArr.push(overEffect);
            }
          }
        }

        clearColumnOverEffect() {
          director.emit('freeGameEnd');

          if (this.overEffectArr && this.overEffectArr.length > 0) {
            for (var i = 0; i < this.overEffectArr.length; i++) {
              var over = this.overEffectArr[i];
              (_crd && sObjPool === void 0 ? (_reportPossibleCrUseOfsObjPool({
                error: Error()
              }), sObjPool) : sObjPool).Enqueue('overLayerEffectItem', over);
              over.clearItem(null, null, null);
              over.node.active = false;
            }

            this.overEffectArr = [];
          }
        }

      }, _temp)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=mNormalFlow.js.map
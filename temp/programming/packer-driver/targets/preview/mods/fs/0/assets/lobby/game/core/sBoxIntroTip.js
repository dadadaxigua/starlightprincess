System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2", "__unresolved_3", "__unresolved_4"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, director, v3, UITransform, Label, Widget, Vec3, Sprite, UIOpacity, tween, SpriteFrame, v2, sAudioMgr, sBoxMgr, sComponent, sConfigMgr, _dec, _dec2, _dec3, _dec4, _dec5, _dec6, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _descriptor5, _temp, _crd, ccclass, property, sBoxIntroTip;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfsAudioMgr(extras) {
    _reporterNs.report("sAudioMgr", "./sAudioMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsBoxEntity(extras) {
    _reporterNs.report("sBoxEntity", "./sBoxEntity", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsBoxItemBase(extras) {
    _reporterNs.report("sBoxItemBase", "./sBoxItemBase", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsBoxMgr(extras) {
    _reporterNs.report("sBoxMgr", "./sBoxMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsComponent(extras) {
    _reporterNs.report("sComponent", "./sComponent", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsConfigMgr(extras) {
    _reporterNs.report("sConfigMgr", "./sConfigMgr", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      director = _cc.director;
      v3 = _cc.v3;
      UITransform = _cc.UITransform;
      Label = _cc.Label;
      Widget = _cc.Widget;
      Vec3 = _cc.Vec3;
      Sprite = _cc.Sprite;
      UIOpacity = _cc.UIOpacity;
      tween = _cc.tween;
      SpriteFrame = _cc.SpriteFrame;
      v2 = _cc.v2;
    }, function (_unresolved_2) {
      sAudioMgr = _unresolved_2.default;
    }, function (_unresolved_3) {
      sBoxMgr = _unresolved_3.sBoxMgr;
    }, function (_unresolved_4) {
      sComponent = _unresolved_4.sComponent;
    }, function (_unresolved_5) {
      sConfigMgr = _unresolved_5.sConfigMgr;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "dc5e9UN/1VK+phEGVPf1t+o", "sBoxIntroTip", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("sBoxIntroTip", sBoxIntroTip = (_dec = ccclass('sBoxIntroTip'), _dec2 = property(SpriteFrame), _dec3 = property(SpriteFrame), _dec4 = property({
        tooltip: '格子底部图片的位置偏移量'
      }), _dec5 = property({
        tooltip: '格子底部图片的大小偏移量'
      }), _dec6 = property({
        tooltip: '格子内容对象的位置偏移量'
      }), _dec(_class = (_class2 = (_temp = class sBoxIntroTip extends (_crd && sComponent === void 0 ? (_reportPossibleCrUseOfsComponent({
        error: Error()
      }), sComponent) : sComponent) {
        constructor() {
          super(...arguments);

          _initializerDefineProperty(this, "spriteBG1", _descriptor, this);

          _initializerDefineProperty(this, "spriteBG2", _descriptor2, this);

          _initializerDefineProperty(this, "renderPosDelta", _descriptor3, this);

          _initializerDefineProperty(this, "renderSizeDelta", _descriptor4, this);

          _initializerDefineProperty(this, "contentPosDelta", _descriptor5, this);

          _defineProperty(this, "renderNode", void 0);

          _defineProperty(this, "contentTrans", void 0);

          _defineProperty(this, "ratesTrans", void 0);

          _defineProperty(this, "textLabel", void 0);

          _defineProperty(this, "backTrans", void 0);

          _defineProperty(this, "backSprite", void 0);

          _defineProperty(this, "downBox", void 0);

          _defineProperty(this, "upBox", void 0);

          _defineProperty(this, "targetBox", void 0);

          _defineProperty(this, "lastTargetBox", void 0);

          _defineProperty(this, "boxTarget", void 0);

          _defineProperty(this, "boxItem", void 0);
        }

        onLoad() {
          this.boxTarget = this.node.getChildByName('box');
          this.renderNode = this.boxTarget.getChildByName('render');

          if (this.renderNode) {
            var back = this.renderNode.getChildByName('back');

            if (back) {
              this.backSprite = back.getComponent(Sprite);
              this.backTrans = back.getComponent(UITransform);
            }
          }

          this.contentTrans = this.boxTarget.getChildByName('content').getComponent(UITransform);

          if (this.contentTrans) {
            var labelNode = this.contentTrans.node.getChildByName('Label');

            if (labelNode) {
              this.textLabel = labelNode.getComponent(Label);
            }

            var ratesNode = this.contentTrans.node.getChildByName('rates');

            if (ratesNode) {
              this.ratesTrans = ratesNode.getComponent(UITransform);
            }
          }
        }

        start() {
          this.boxItem = (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
            error: Error()
          }), sBoxMgr) : sBoxMgr).instance.CreateBoxItemBaseByType('box');

          if (this.boxItem) {
            this.boxItem.node.parent = this.boxTarget;
            this.boxItem.node.position = Vec3.ZERO;
            this.boxItem.node.active = false;
          }

          director.on('sBoxIntroTipClick', (type, clickBox, x, y) => {
            if (this.renderNode && this.backTrans) {
              // console.log('x,y:'+x +','+y);
              if (this.canShow(type, x, y)) {
                (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                  error: Error()
                }), sAudioMgr) : sAudioMgr).PlayShotAudio('boxIntroClick');
                var size = (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                  error: Error()
                }), sBoxMgr) : sBoxMgr).instance.boxSize;
                var box = clickBox;

                if (box && this.judgeBoxCanTip(box)) {
                  this.downBox = box;
                  this.upBox = box;
                  this.targetBox = box;

                  if (type == 'vertical') {
                    var symbalStr = box.SymbolValue.toString();

                    if (this.isSmallBox(symbalStr)) {// const pos = sBoxMgr.instance.getBoxContainerPosByXY(x, y);
                      // this.boxTarget.position = v3(pos.x + size.x / 2, pos.y + size.y / 2, 0);
                    } else if (this.isLongBox(symbalStr)) {
                      this.judgeLongBoxTarget(x, y); // console.log('downBox:'+ this.downBox.SymbolValue);
                      // console.log('upBox:'+ this.upBox.SymbolValue);
                      // console.log('targetBox:'+ this.targetBox.ViewItemIndex);
                      // const pos = sBoxMgr.instance.getBoxContainerPosByXY(this.downBox.ViewItemIndex.x, this.downBox.ViewItemIndex.y);
                      // this.boxTarget.position = v3(pos.x + size.x / 2, pos.y + size.y / 2, 0);
                    }
                  }

                  if (this.lastTargetBox && this.lastTargetBox == this.targetBox) {
                    this.closeBoxItem();
                    this.blackMaskAnima(128, 0);
                    this.lastTargetBox = null;
                  } else {
                    this.setViewSize(this.targetBox, size.x, size.y);
                    var boxWorldPos = this.downBox.GetBoxWorldPosition();
                    this.boxTarget.worldPosition = v3(boxWorldPos.x + size.x / 2, boxWorldPos.y + size.y / 2, 0);
                    this.boxTarget.active = true;
                    this.contentTrans.node.active = true;
                    this.renderNode.active = true;
                    this.setViewDirection(x, y);
                    this.openBoxItem(this.targetBox);
                    this.setViewData(this.targetBox);
                    this.lastTargetBox = this.targetBox;
                  }
                }
              }
            }
          }, this);
          director.on('slotSubGameRollBegin', () => {
            this.closeBoxItem();
            this.lastTargetBox = null;
          }, this);
        }

        canShow(type, x, y) {
          return true;
        }

        isSmallBox(symbolValue) {
          return symbolValue.length < 4 && symbolValue != 2;
        }

        isLongBox(symbolValue) {
          return symbolValue.length >= 4 || symbolValue == 2;
        }

        judgeLongBoxTarget(x, y) {
          var boxViewSum = (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
            error: Error()
          }), sBoxMgr) : sBoxMgr).instance.boxViewSum;

          var findTargetBox = () => {
            var _downBox = (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
              error: Error()
            }), sBoxMgr) : sBoxMgr).instance.getBoxEntityByXY(x, this.targetBox.ViewItemIndex.y - 1);

            if (_downBox && _downBox.SymbolValue) {
              if (_downBox.SymbolValue == 10000) {
                this.targetBox = _downBox;
                findTargetBox();
              } else {
                this.targetBox = _downBox;
              }
            }
          };

          var findDownBox = () => {
            var _downBox = (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
              error: Error()
            }), sBoxMgr) : sBoxMgr).instance.getBoxEntityByXY(x, this.downBox.ViewItemIndex.y - 1);

            if (_downBox && _downBox.SymbolValue) {
              if (_downBox.ViewItemIndex.y >= 0) {
                if (_downBox.SymbolValue == 10000) {
                  this.downBox = _downBox;
                  findDownBox();
                } else {
                  this.downBox = _downBox;
                }
              }
            }
          };

          var findUpBox = () => {
            var _upBox = (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
              error: Error()
            }), sBoxMgr) : sBoxMgr).instance.getBoxEntityByXY(x, this.upBox.ViewItemIndex.y + 1);

            if (_upBox && _upBox.SymbolValue) {
              if (_upBox.ViewItemIndex.y < boxViewSum.y) {
                if (_upBox.SymbolValue == 10000) {
                  this.upBox = _upBox;
                  findUpBox();
                }
              }
            }
          };

          if (this.downBox.SymbolValue == 10000) {
            findDownBox();
          }

          findUpBox();

          if (this.targetBox.SymbolValue == 10000) {
            findTargetBox();
          }
        }

        judgeBoxCanTip(box) {
          return true;
        }

        setViewSize(box, width, height) {
          var renderSize = this.getRenderBackSize(box, width, height);

          if (this.backTrans) {
            var multiple = 1;

            if (box && (box.SymbolValue / 1000 > 1 || box.SymbolValue == 2)) {
              if (this.targetBox && this.downBox && this.upBox) {
                multiple = Math.abs(this.upBox.ViewItemIndex.y - this.downBox.ViewItemIndex.y) + 1;
              }
            }

            this.backTrans.node.position = v3(-width / 2 + this.renderPosDelta.x, -height / 2 + this.renderPosDelta.y, 0);
            this.backTrans.setContentSize(renderSize.x + this.renderSizeDelta.x, height * multiple + this.renderSizeDelta.y);

            if (this.contentTrans) {
              var boxSize = (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                error: Error()
              }), sBoxMgr) : sBoxMgr).instance.boxSize;
              this.contentTrans.setContentSize(renderSize.x - boxSize.x, renderSize.y * multiple);

              if (this.ratesTrans) {
                this.ratesTrans.setContentSize(this.contentTrans.contentSize.x, this.contentTrans.contentSize.y);
              }

              this.contentTrans.node.position = v3(boxSize.x / 2 + this.contentTrans.contentSize.x / 2 + this.contentPosDelta.x, this.contentTrans.contentSize.y / 2 - boxSize.y / 2 + this.contentPosDelta.y);

              if (this.textLabel) {
                var widget = this.textLabel.getComponent(Widget);

                if (widget) {
                  widget.updateAlignment();
                }
              } // console.log('this.contentTrans:'+this.contentTrans.contentSize);
              // console.log('this.contentTrans position:'+this.contentTrans.node.position);

            }
          }
        }

        getRenderBackSize(box, width, height) {
          var boxSize = v2(width * 2, height);

          if (box) {
            if (box.SymbolValue > 1000) {
              boxSize.x = width * 3;
            }
          }

          return boxSize;
        }

        setViewDirection(x, y) {
          var boxViewSum = (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
            error: Error()
          }), sBoxMgr) : sBoxMgr).instance.boxViewSum;
          var middleX = boxViewSum.x / 2;
          var limitX = this.getViewDirXLimit();

          if (limitX >= 0) {
            middleX = limitX;
          }

          if (x > middleX) {
            if (this.renderNode) {
              this.renderNode.scale = v3(-1, 1, 1);
            }

            if (this.contentTrans) {
              this.contentTrans.node.position = v3(-Math.abs(this.contentTrans.node.position.x), this.contentTrans.node.position.y, 0);
            }
          } else {
            if (this.renderNode) {
              this.renderNode.scale = v3(1, 1, 1);
            }

            if (this.contentTrans) {
              this.contentTrans.node.position = v3(Math.abs(this.contentTrans.node.position.x), this.contentTrans.node.position.y, 0);
            }
          }
        }

        getViewDirXLimit() {
          return -1;
        }

        openBoxItem(target) {
          if (this.boxItem && target) {
            var symbal = this.getTargetBoxSymbolValue(target);

            if (symbal) {
              var size = (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                error: Error()
              }), sBoxMgr) : sBoxMgr).instance.boxSize;
              var boxContainerPos = (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                error: Error()
              }), sBoxMgr) : sBoxMgr).instance.node.worldPosition;

              if (this.boxItem) {
                this.boxItem.clearItem(null, 'idle', 'idle');
                this.boxItem.node.active = false;
              }

              var data = (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
                error: Error()
              }), sConfigMgr) : sConfigMgr).instance.GetSymbolBoxImg(symbal);

              if (data) {
                var state = this.getTargetBoxUpdateState(target);
                var stateData = data[state];

                if (stateData) {
                  if (!this.lastTargetBox) {
                    this.blackMaskAnima(0, 128);
                  }

                  this.boxItem.node.active = true; // this.boxItem.node.worldPosition = v3(boxContainerPos.x + target.ViewItemIndex.x * size.x, boxContainerPos.y + target.ViewItemIndex.y * size.y + sBoxMgr.instance.getSkewPos(target.ViewItemIndex.x), 0);

                  this.boxItem.node.worldPosition = target.GetBoxWorldPosition();
                  this.boxItem.boxItemUpdate(stateData, symbal, state, 'idle');
                }
              }
            }
          }
        }

        getTargetBoxSymbolValue(target) {
          if (target) {
            return target.SymbolValue;
          }

          return null;
        }

        getTargetBoxUpdateState(target) {
          return 'idle';
        }

        closeBoxItem() {
          this.CtrAllChildActive(this.node, false);

          if (this.boxItem) {
            this.boxItem.clearItem(null, 'idle', 'idle');
            this.boxItem.node.active = false;
          }
        }

        setViewData(box) {
          if (this.ratesTrans) {
            this.ratesTrans.node.active = false;
          }

          if (this.textLabel) {
            this.textLabel.node.active = false;
          }

          if (box) {
            var symbalData = (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
              error: Error()
            }), sConfigMgr) : sConfigMgr).instance.getSymbalConfig(this.getTargetBoxSymbolValue(box));

            if (symbalData) {
              var type = 'des';

              if (symbalData.rate) {
                var rateKeys = Object.keys(symbalData.rate);

                if (rateKeys && rateKeys.length > 0) {
                  if (symbalData.rate[rateKeys[0]] != 0) {
                    type = 'rate';
                  }
                }
              }

              if (type == 'des') {
                if (this.backSprite) {
                  this.backSprite.spriteFrame = this.spriteBG2;
                }

                if (this.textLabel) {
                  if (symbalData.des) {
                    var des = symbalData.des[globalThis.GetLanguageType()];

                    if (des) {
                      this.textLabel.node.active = true;
                      this.textLabel.string = des;
                    }
                  }
                }
              } else if (type == 'rate') {
                if (this.backSprite) {
                  this.backSprite.spriteFrame = this.spriteBG1;
                }

                if (symbalData.rate) {
                  var _rateKeys = Object.keys(symbalData.rate);

                  if (_rateKeys && _rateKeys.length > 0) {
                    if (symbalData.rate[_rateKeys[0]] != 0) {
                      if (this.ratesTrans) {
                        this.ratesTrans.node.active = true;
                      }

                      for (var i = _rateKeys.length - 1; i >= 0; i--) {
                        var key = _rateKeys[i];
                        var rateData = symbalData.rate[key];
                        var rateNodeItem = this.ratesTrans.node.children[i];

                        if (rateNodeItem) {
                          rateNodeItem.getChildByName('left').getComponent(Label).string = key;
                          rateNodeItem.getChildByName('right').getComponent(Label).string = rateData;
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }

        blackMaskAnima(initial, value) {
          var black = this.node.getChildByName('blackMask');

          if (black) {
            black.active = true;
            var opacity = black.getComponent(UIOpacity);

            if (opacity) {
              this.removeIDTween('blackMask');
              opacity.opacity = initial;
              this.pushIDTween(tween(opacity).to(0.4, {
                opacity: value
              }).start(), 'blackMask');
            }
          }
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "spriteBG1", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "spriteBG2", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "renderPosDelta", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return v2(0, 0);
        }
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "renderSizeDelta", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return v2(0, 0);
        }
      }), _descriptor5 = _applyDecoratedDescriptor(_class2.prototype, "contentPosDelta", [_dec6], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return v2(0, 0);
        }
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=sBoxIntroTip.js.map
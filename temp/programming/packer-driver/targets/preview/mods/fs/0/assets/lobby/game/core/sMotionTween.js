System.register(["cc"], function (_export, _context) {
  "use strict";

  var _cclegacy, _decorator, Vec3, math, v3, _dec, _class, _temp, _crd, ccclass, property, sMotionTween;

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  return {
    setters: [function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      Vec3 = _cc.Vec3;
      math = _cc.math;
      v3 = _cc.v3;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "1d3f53xMoRHuJT1IXMVL28i", "sMotionTween", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("sMotionTween", sMotionTween = (_dec = ccclass('sMotionTween'), _dec(_class = (_temp = class sMotionTween {
        constructor() {
          _defineProperty(this, "id", null);

          _defineProperty(this, "start", null);

          _defineProperty(this, "ori", null);

          _defineProperty(this, "end", null);

          _defineProperty(this, "speed", 1);

          _defineProperty(this, "rollSpeed", 1);

          _defineProperty(this, "speedIncrement", 0);

          _defineProperty(this, "rollSpeedMin", 0);

          _defineProperty(this, "rollSpeedMax", 4);

          _defineProperty(this, "rollPixelXLimit", 0);

          _defineProperty(this, "rollPixelYLimit", 0);

          _defineProperty(this, "loop", 1);

          _defineProperty(this, "frameCallBack", null);

          _defineProperty(this, "completeCallBack", null);

          _defineProperty(this, "rateCallBack", null);

          _defineProperty(this, "rateCallTime", 0);

          _defineProperty(this, "mTime", -1);

          _defineProperty(this, "costTime", 0);

          _defineProperty(this, "motionDistance", 0);
        }

        get Speed() {
          return this.speed * this.rollSpeed;
        }

        set RollSpeed(value) {
          this.rollSpeed = value;
        }

        get IsFinish() {
          return this.mTime == -1;
        }

        get PixelYDelta() {
          return Math.abs(this.start.y - this.ori.y);
        }

        get PixelXDelta() {
          return Math.abs(this.start.x - this.ori.x);
        }

        CreateTween(_id, _end, _start, _speed, _frameCallback, _completeCallBack) {
          if (_frameCallback === void 0) {
            _frameCallback = null;
          }

          if (_completeCallBack === void 0) {
            _completeCallBack = null;
          }

          this.id = _id;
          this.start = v3(_start);
          this.end = _end;
          this.ori = _start;
          this.speed = _speed;
          this.frameCallBack = _frameCallback;
          this.completeCallBack = _completeCallBack;
          this.mTime = 0;
          this.motionDistance = Vec3.distance(_start, _end);
          this.costTime = this.motionDistance / this.speed; // console.log('time1:'+game.totalTime);
          // console.log('this.costTime:'+this.costTime);

          if (this.costTime == 0) {
            this.mTime = -1;
          }
        }

        Update(dt) {
          if (this.loop == -1 || this.mTime < this.costTime && this.mTime >= 0) {
            this.mTime += dt * this.rollSpeed;

            var _nowRate = this.mTime / this.costTime; // console.log('_nowRate:'+_nowRate);


            if (this.loop == -1) {
              this.tweenCtr(_nowRate);
            } else {
              this.tweenCtr(_nowRate > 1 ? 1 : _nowRate);

              if (this.rateCallBack && this.rateCallTime != 0) {
                if (_nowRate >= this.rateCallTime) {
                  this.rateCallBack();
                  this.rateCallTime = 0;
                }
              }
            } // this.tweenCtr(_nowRate);


            if (this.speedIncrement != 0) {
              this.changeSpeed(this.speedIncrement * dt);
            }

            if (this.rollPixelYLimit > 0) {
              if (this.PixelYDelta >= this.rollPixelYLimit) {
                if (this.rateCallBack && this.rateCallTime != 0) {
                  this.rateCallBack();
                  this.rateCallTime = 0;
                }

                this.tweenCtr(1);
                this.Complete();
              }
            }

            if (this.rollPixelXLimit > 0) {
              if (this.PixelXDelta >= this.rollPixelXLimit) {
                if (this.rateCallBack && this.rateCallTime != 0) {
                  this.rateCallBack();
                  this.rateCallTime = 0;
                }

                this.tweenCtr(1);
                this.Complete();
              }
            }
          }

          if (this.loop != -1 && this.mTime >= this.costTime) {
            this.Complete();
          }
        }

        Complete() {
          this.mTime = -1;

          if (this.completeCallBack) {
            this.completeCallBack(this.id);
          }
        }

        GoEnd() {
          this.tweenCtr(1);
          this.mTime = -1;

          if (this.completeCallBack) {
            this.completeCallBack(this.id);
          }
        }

        Pause() {
          this.rollSpeed = 0;
        }

        SetRateCall(call, rate) {
          if (call && rate) {
            this.rateCallBack = call;
            this.rateCallTime = rate;
          }
        }

        changeSpeed(delta) {
          this.rollSpeed += delta;

          if (this.rollSpeed < this.rollSpeedMin) {
            this.rollSpeed = this.rollSpeedMin;
          } else if (this.rollSpeed > this.rollSpeedMax) {
            // console.log('this.rollSpeedMax:'+this.rollSpeedMax);
            this.rollSpeed = this.rollSpeedMax;
          } // if(this.rollSpeed > 1){
          //     this.rollSpeed = 1;
          // }
          // if(this.speed > this.speedMin){
          //     this.speed += delta;
          //     this.costTime = this.motionDistance / this.speed;
          // }
          // console.log('this.costTime:'+this.costTime +', ' +this.speed);
          // if(this.speed > 0){
          // }
          // console.log('this.costTime:'+this.costTime);

        }

        tweenCtr(ratio) {
          // console.log(this.id+'_ratio : '+ ratio);
          if (this.start && this.end) {
            this.ori.x = math.lerp(this.start.x, this.end.x, ratio);
            this.ori.y = math.lerp(this.start.y, this.end.y, ratio);

            if (this.frameCallBack) {
              this.frameCallBack(this.ori);
            }
          }
        }

      }, _temp)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=sMotionTween.js.map
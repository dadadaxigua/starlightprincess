System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2", "__unresolved_3"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, JsonAsset, sys, assetManager, director, sComponent, sUtil, sSlotSceneInfo, _dec, _dec2, _dec3, _dec4, _class, _class2, _descriptor, _descriptor2, _descriptor3, _class3, _temp, _crd, ccclass, property, sConfigMgr;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfsComponent(extras) {
    _reporterNs.report("sComponent", "./sComponent", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsUtil(extras) {
    _reporterNs.report("sUtil", "./sUtil", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsSlotSceneInfo(extras) {
    _reporterNs.report("sSlotSceneInfo", "./sSlotSceneInfo", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      JsonAsset = _cc.JsonAsset;
      sys = _cc.sys;
      assetManager = _cc.assetManager;
      director = _cc.director;
    }, function (_unresolved_2) {
      sComponent = _unresolved_2.sComponent;
    }, function (_unresolved_3) {
      sUtil = _unresolved_3.sUtil;
    }, function (_unresolved_4) {
      sSlotSceneInfo = _unresolved_4.sSlotSceneInfo;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "bda58sK2LRIaplqKXxwwHhY", "sConfigMgr", undefined);

      ({
        ccclass,
        property
      } = _decorator); //globalThis.GameBtnEntity
      //director.emit('winBetRes',sGameEntity.instance.CurrentBetAmount * round.rate,round.rate);

      _export("sConfigMgr", sConfigMgr = (_dec = ccclass('sConfigMgr'), _dec2 = property(JsonAsset), _dec3 = property(JsonAsset), _dec4 = property(JsonAsset), _dec(_class = (_class2 = (_temp = _class3 = class sConfigMgr extends (_crd && sComponent === void 0 ? (_reportPossibleCrUseOfsComponent({
        error: Error()
      }), sComponent) : sComponent) {
        constructor() {
          super(...arguments);

          _initializerDefineProperty(this, "configJson", _descriptor, this);

          _initializerDefineProperty(this, "boxDataJson", _descriptor2, this);

          _initializerDefineProperty(this, "gameConfigJson", _descriptor3, this);

          _defineProperty(this, "boxArrayLength", 1);

          _defineProperty(this, "symboxData", []);

          _defineProperty(this, "symboxImgData", null);

          _defineProperty(this, "configObj", null);

          _defineProperty(this, "boxResDataObj", null);

          _defineProperty(this, "resArr", []);

          _defineProperty(this, "multiResArrIndex", 0);

          _defineProperty(this, "resData", null);

          _defineProperty(this, "resArea", []);

          _defineProperty(this, "boxUpdateTrigger", []);

          _defineProperty(this, "bandLength", {});

          _defineProperty(this, "oneRoundData", null);

          _defineProperty(this, "rollBingFreeWinLimit", 100);

          _defineProperty(this, "rollTotalInfo", null);

          _defineProperty(this, "rollRateInfo", null);

          _defineProperty(this, "currentSeq", -1);
        }

        get BoxArrayLength() {
          return this.boxArrayLength;
        }

        get GameConfigJson() {
          return this.gameConfigJson;
        }

        get SymboxImgData() {
          return this.symboxImgData;
        }

        get CurrentSeq() {
          return this.currentSeq;
        }

        onLoad() {
          director.on('backToHall', () => {
            sConfigMgr.instance = null;
          }, this);
          this.symboxImgData = this.gameConfigJson.json['symbol_box_img']; // let sds = Object.keys(this.symboxImgData);
          // for (let i = 0; i < sds.length; i++) {
          //     const key = sds[i];
          //     let kkk = this.symboxImgData[key];
          //     let kkkks = Object.keys(kkk);
          //     if(kkkks){
          //         for (let x = 0; x < kkkks.length; x++) {
          //             const element = kkkks[x];
          //             let ooo = kkk[element];
          //                 // const spriteFrame : SpriteFrame = assetMgr.GetAssetByName(ooo.render_name);
          //                 // if(spriteFrame){
          //                 // }
          //                 if(ooo.position && ooo.position.y == 53){
          //                     ooo.frame_size = {x : 125 , y : 104};
          //                 }else if(ooo.position && ooo.position.y == 106){
          //                     ooo.frame_size = {x : 125 , y : 209};
          //                 }else if(ooo.position && ooo.position.y == 159){
          //                     ooo.frame_size = {x : 125 , y : 314};
          //                 }else if(ooo.position && ooo.position.y == 212){
          //                     ooo.frame_size = {x : 125 , y : 417};
          //                 }    
          //         }
          //     }
          // }
          // console.log(JSON.stringify(this.symboxImgData));

          sConfigMgr.instance = this;
        }

        getGameVersion() {
          if (sConfigMgr.instance && sConfigMgr.instance.gameConfigJson && sConfigMgr.instance.gameConfigJson.json && sConfigMgr.instance.gameConfigJson.json['ver']) {
            return sConfigMgr.instance.gameConfigJson.json['ver'];
          }

          return '';
        }

        start() {
          if (sys.isNative) {
            if (jsb.fileUtils.isFileExist(jsb.fileUtils.getWritablePath() + 'clientRollMaps.json')) {
              assetManager.loadRemote(jsb.fileUtils.getWritablePath() + 'clientRollMaps.json', (err, file) => {
                if (file && file.json) {
                  this.boxDataJson = file;
                }

                console.log('path:' + jsb.fileUtils.getWritablePath() + 'config.json');

                if (jsb.fileUtils.isFileExist(jsb.fileUtils.getWritablePath() + 'config.json')) {
                  assetManager.loadRemote(jsb.fileUtils.getWritablePath() + 'config.json', (err, file) => {
                    if (file && file.json) {
                      this.configJson = file;
                      this.boxArrayLength = this.symboxData[0][0].length;
                      director.emit('configLoadSuc');
                    }
                  });
                }
              });
            } else {
              this.loadConfig();
            }
          } else {
            this.loadConfig();
          }
        }

        loadConfig() {
          this.configObj = this.configJson.json;

          if (this.boxDataJson) {
            this.boxResDataObj = this.boxDataJson.json;
          }

          this.symbolBoxDataInit('0');
          this.boxArrayLength = this.GetSymbolBox(0)[0].length;
          director.emit('configLoadSuc'); // let bundle = assetMgr.GetBundle();
          // if(bundle){
          //     bundle.load('116_config',BufferAsset,(err,data)=>{
          //         // console.log('wdwdw :' + sConfigMgr.uncompresss(new Uint8Array(data.buffer())));
          //         const str = sConfigMgr.uncompresss(new Uint8Array(data.buffer()));
          //         this.configObj = JSON.parse(str);
          //         bundle.release('116_config',BufferAsset);
          //         this.boxResDataObj = this.boxDataJson.json;
          //         this.symbolBoxDataInit('0');
          //         this.boxArrayLength = this.GetSymbolBox(0)[0].length;
          //         director.emit('configLoadSuc');
          //     });
          // }
        }

        static compresss(data, dict) {
          if (dict === void 0) {
            dict = null;
          }

          // @ts-ignore
          var result = new globalThis.deflateSync(data, {
            dictionary: dict
          });
          return result;
        }

        static uncompresss(data, dict) {
          if (dict === void 0) {
            dict = null;
          }

          // @ts-ignore
          // var result = new globalThis.inflateSync(data,{dictionary : dict});    
          // return result;
          var inflate = new Zlib.Inflate(data);
          var plain = inflate.decompress();
          return plain;
        }

        changeSymbolBoxDataToNormal() {
          this.symbolBoxDataInit('0');
        }

        changeSymbolBoxDataToFreeWin() {
          this.symbolBoxDataInit('1');
        }

        symbolBoxDataInit(roundConfigIndex) {
          try {
            if (roundConfigIndex) {
              this.symboxData = [];
              var posBox = this.configObj['round_config'][roundConfigIndex]['pos_box'];

              if (posBox && Array.isArray(posBox)) {
                for (var i = 0; i < posBox.length; i++) {
                  var element = posBox[i];
                  this.symboxData.push(element.vice_symbol_box);
                }
              } // console.log('this.symboxData:'+this.symboxData);

            }
          } catch (e) {
            console.log("symbolBoxDataInit:" + e);
          }
        }

        setBoxTrigger(column) {
          for (var i = 0; i < column; i++) {
            this.boxUpdateTrigger[i] = false;
          }
        }

        setBoxsTrigger(value) {
          for (var i = 0; i < this.boxUpdateTrigger.length; i++) {
            this.boxUpdateTrigger[i] = value;
          }
        }

        setBoxTriggerValue(row, value) {
          this.boxUpdateTrigger[row] = value;
        }

        getBoxData() {
          return this.boxResDataObj;
        }

        getConfig() {
          return this.configObj;
        }

        getSymbalConfig(symbal) {
          if (this.configObj && this.configObj['symbol_config']) {
            return this.configObj['symbol_config'][symbal];
          }
        }

        GetSymbolBox(row) {
          return this.symboxData[row];
        }

        GetSymbolBoxData(x, y) {
          // console.log('GetSymbolBoxData:'+x+','+y);
          var res = this.GetBoxDataArea(x);

          if (this.boxUpdateTrigger[x] && res && y >= res.resMinY && y < res.resMaxY) {
            var data = this.resArr[x]; // if(globalThis.weaweawe && x == 0 && (y - res.resMinY) == 3)
            //     console.log('y - res.resMinY:',(y - res.resMinY));
            // if(globalThis.slotJackpotDate){
            //     return {pos : data[y - res.resMinY],viewX : x, viewY : (y - res.resMinY)};
            // }else{
            //     return data[y - res.resMinY];
            // }

            return data[y - res.resMinY];
          } else {
            var _y = y; // if(res && y >= res.resMinY){
            //     _y -= (res.resMaxY - res.resMinY);
            // }

            var symbox = this.symboxData[x];
            var bigbox = Math.floor(_y / sConfigMgr.instance.BoxArrayLength);
            var smallIndex = _y - bigbox * sConfigMgr.instance.BoxArrayLength;
            return symbox[bigbox % symbox.length][smallIndex];
          }
        }

        IsSymbolBoxData(x, y) {
          var res = this.GetBoxDataArea(x);

          if (this.boxUpdateTrigger[x] && res && y >= res.resMinY && y < res.resMaxY) {
            return true;
          }

          return false;
        }

        GetSymbolBoxImg(index) {
          return this.symboxImgData[index];
        }

        SetResData(arr) {
          this.resData = arr;

          if (globalThis.slotJackpotDate && globalThis.slotJackpotDate.num > 0) {
            this.resData = (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
              error: Error()
            }), sUtil) : sUtil).CopyArray(arr);

            if (this.resData.length != 1) {
              this.resData = [this.resData];
            }

            var jackpotData = globalThis.slotJackpotDate;
            var _replace = globalThis.slotJackpotDate.replace;
            var boxSum = (_crd && sSlotSceneInfo === void 0 ? (_reportPossibleCrUseOfsSlotSceneInfo({
              error: Error()
            }), sSlotSceneInfo) : sSlotSceneInfo).BoxSum;
            var boxViewSum = (_crd && sSlotSceneInfo === void 0 ? (_reportPossibleCrUseOfsSlotSceneInfo({
              error: Error()
            }), sSlotSceneInfo) : sSlotSceneInfo).BoxViewSum;
            var num = jackpotData.num;
            var indexArr = [];

            for (var x = 0; x < this.resData[0].length; x++) {
              var elng = this.resData[0][x];

              for (var y = (boxSum.y - boxViewSum.y) / 2; y < (boxSum.y - boxViewSum.y) / 2 + boxViewSum.y; y++) {
                var lng = elng[y];

                if (this.CanBeJackpotBox(lng, x, y)) {
                  indexArr.push({
                    posX: x,
                    posY: y
                  });
                }
              }
            }

            for (var i = 0; i < num; i++) {
              if (indexArr.length > 0) {
                var indexA = indexArr.splice((_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                  error: Error()
                }), sUtil) : sUtil).RandomInt(0, indexArr.length), 1);

                if (indexA && indexA.length > 0) {
                  var index = indexA[0];
                  var resBol = this.resData[0][index.posX][index.posY];
                  this.resData[0][index.posX][index.posY] = {
                    symbol: resBol,
                    type: 'jackpot',
                    replace: _replace
                  };
                }
              }
            }
          }

          this.multiResArrIndex = 0;

          if (this.resData && Array.isArray(this.resData) && this.resData.length > 0) {
            if (Array.isArray(this.resData[0]) && Array.isArray(this.resData[0][0])) {
              this.resArr = this.resData[0];

              for (var _i = 0; _i < this.resArr.length; _i++) {
                this.bandLength[_i] = this.resArr[_i].length;
              }
            } else {
              this.resArr = this.resData;

              for (var _i2 = 0; _i2 < this.resArr.length; _i2++) {
                this.bandLength[_i2] = this.resArr[_i2].length;
              }
            } // console.log('resMinY:'+this.resMinY + 'resMaxY:' +this.resMaxY);

          }
        }

        CanBeJackpotBox(symbol, x, y) {
          if (!isNaN(symbol)) {
            if (symbol == 10000 || symbol == 1 || symbol == 2 || symbol == 100) {
              return false;
            }

            var symbolStr = symbol.toString();

            if (symbolStr.length == 4) {
              return symbolStr[3] == '1';
            } else {
              return true;
            }
          } else {
            return false;
          }
        }

        SetDropResData(arr) {
          this.resData = arr;

          if (globalThis.slotJackpotDate && globalThis.slotJackpotDate.num > 0) {
            this.resData = (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
              error: Error()
            }), sUtil) : sUtil).CopyArray(arr);
            var jackpotData = globalThis.slotJackpotDate;
            var _replace = globalThis.slotJackpotDate.replace;
            var boxSum = (_crd && sSlotSceneInfo === void 0 ? (_reportPossibleCrUseOfsSlotSceneInfo({
              error: Error()
            }), sSlotSceneInfo) : sSlotSceneInfo).BoxSum;
            var boxViewSum = (_crd && sSlotSceneInfo === void 0 ? (_reportPossibleCrUseOfsSlotSceneInfo({
              error: Error()
            }), sSlotSceneInfo) : sSlotSceneInfo).BoxViewSum;
            var num = jackpotData.num;
            var indexArr = [];

            for (var x = 0; x < this.resData[0].length; x++) {
              var elng = this.resData[0][x];

              for (var y = (boxSum.y - boxViewSum.y) / 2; y < (boxSum.y - boxViewSum.y) / 2 + boxViewSum.y; y++) {
                var lng = elng[y];

                if (this.CanBeJackpotBox(lng, x, y)) {
                  indexArr.push({
                    posX: x,
                    posY: y
                  });
                }
              }
            }

            for (var i = 0; i < num; i++) {
              if (indexArr.length > 0) {
                var indexA = indexArr.splice((_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                  error: Error()
                }), sUtil) : sUtil).RandomInt(0, indexArr.length), 1);

                if (indexA && indexA.length > 0) {
                  var index = indexA[0];
                  var resBol = this.resData[0][index.posX][index.posY];
                  this.resData[0][index.posX][index.posY] = {
                    symbol: resBol,
                    type: 'jackpot',
                    replace: _replace
                  };
                }
              }
            }
          }

          this.multiResArrIndex = 0;

          if (this.resData && Array.isArray(this.resData) && this.resData.length > 0) {
            if (Array.isArray(this.resData[0])) {
              this.resArr = this.resData[0];

              for (var _i3 = 0; _i3 < this.resArr.length; _i3++) {
                this.bandLength[_i3] = this.resArr[_i3].length;
              }
            } // console.log('resMinY:'+this.resMinY + 'resMaxY:' +this.resMaxY);

          }
        }

        TurnNextResRoundData() {
          this.multiResArrIndex++;
          this.resArr = this.resData[this.multiResArrIndex];
        }

        GetCurrentRoundResData() {
          return this.resArr;
        }

        GetBoxDataArea(column) {
          return this.resArea[column];
        }

        SetBoxDataArea(column, index) {
          var obj = this.resArea[column];

          if (obj) {
            obj.resMinY = index;
            obj.resMaxY = index + this.bandLength[column];
          } else {
            this.resArea[column] = {};
            this.resArea[column].resMinY = index;
            this.resArea[column].resMaxY = index + this.bandLength[column];
          }
        }

        GetNextBandIndex(index) {
          var bigbox = Math.ceil((index + 1) / sConfigMgr.instance.BoxArrayLength);
          var endIndex = bigbox * sConfigMgr.instance.BoxArrayLength;
          return endIndex;
        }

        SetOneRoundData(res) {
          this.oneRoundData = this.GetTargetRoundData(res);
          return this.oneRoundData == null ? false : true;
        }

        SetTargetRoundData(gameMode, rollType, rate, seq) {
          try {
            var boxData = sConfigMgr.instance.getBoxData();
            this.oneRoundData = boxData[1]['round_type1'][rate][seq].rounds;
          } catch (error) {
            director.emit('subGameBetConfigErr', 'config error');
            console.error(error);
          }
        }

        GetTargetRoundData(res) {
          try {
            if (!globalThis.slotInfo) {
              globalThis.slotInfo = {};
            }

            if (res.data) {
              var roundTest = globalThis.getSubGameOneBetTestData && globalThis.getSubGameOneBetTestData();

              if (roundTest) {
                return roundTest.rounds;
              } else {
                if (res.data.seq >= 0) {
                  var test1 = res.data.seq >> 29 & 1;
                  var test2 = res.data.seq & 536870911;
                  console.log('test1:', test1, ' test2:', test2);
                  var test_roundType = test1 == 1 ? 'round_type1' : 'round_type0';
                  globalThis.slotInfo.roundType = test_roundType;
                  globalThis.slotInfo.lastRollType = test1 == 1 ? 'freeWin' : 'normal';
                  var test_seq = test2;
                  var boxData = sConfigMgr.instance.getBoxData();
                  return boxData[1][test_roundType][res.data.rate][test_seq].rounds;
                } else {
                  var round = this.getRandomRoundData(res);
                  return round.rounds;
                }
              }
            } else {
              var _boxData = sConfigMgr.instance.getBoxData();

              return _boxData[res.local.lastGameMode][res.local.roundType][res.local.lastRate][res.local.lastRoundSeq].rounds;
            } // console.log('oneRoundData:',this.oneRoundData);

          } catch (e) {
            director.emit('subGameBetConfigErr', 'config error', res);
            console.error(e);
            return null;
          }
        }

        getRandomRoundData(res) {
          globalThis.slotInfo = {};
          var lineAmount = 50;
          var gameID = globalThis.currentPlayingGameID;

          if (globalThis.GameBtnEntity && globalThis.GameBtnEntity.lineAmount) {
            lineAmount = globalThis.GameBtnEntity.lineAmount;
          }

          if (!this.rollTotalInfo) {
            var rollTotalInfoData = sys.localStorage.getItem(gameID + '_subGameSlotRollTotalInfo');

            if (rollTotalInfoData) {
              this.rollTotalInfo = JSON.parse(rollTotalInfoData);
            } else {
              this.rollTotalInfo = {
                normal: 0,
                freewin: 0
              };
            }
          }

          if (!this.rollRateInfo) {
            this.rollRateInfo = {};
          }

          var boxData = sConfigMgr.instance.getBoxData();
          globalThis.slotInfo.lastRate = res.data.rate;
          globalThis.slotInfo.lastGameMode = res.data.game_mode;
          var rate = res.data.rate;
          var gameMode = 'normal';

          if (res.data.game_mode == 2) {
            gameMode = 'buy';
          }

          var gameModes = boxData[res.data.game_mode];
          var rateKey = 'subGameSlotRollInfoRate_' + gameMode + '_';
          var targetRate = null;
          var rollType = 'none';
          var roundType = 'round_type' + res.data.round_type;
          targetRate = gameModes[roundType][rate];
          rateKey += roundType + '_';

          if (res.data.round_type == 0) {
            rollType = 'normal';
          } else if (res.data.round_type == 1) {
            rollType = 'freeWin';
          } else {
            rollType = 'jackpot';
          }

          globalThis.slotInfo.roundType = roundType;
          globalThis.slotInfo.lastRollType = rollType; // const round_type0RateList = gameModes['round_type0'];
          // const round_type1RateList = gameModes['round_type1'];
          // if(round_type0RateList && round_type1RateList){
          //     let freeWinBingo = false;
          // const freeWinBingo = (rate > lineAmount) && ((this.rollTotalInfo.freewin + 1) * this.rollBingFreeWinLimit < this.rollTotalInfo.normal);
          // if((this.rollTotalInfo.freewin + 1) * 100 < this.rollTotalInfo.normal && this.rollTotalInfo.normal < (this.rollTotalInfo.freewin + 1) * 200){
          //     if(rate > lineAmount * 20){
          //         freeWinBingo = true;
          //     }
          // }else if((this.rollTotalInfo.freewin + 1) * 200 <= this.rollTotalInfo.normal){
          //     if(rate > lineAmount * 20){
          //         freeWinBingo = true;
          //     }
          // }
          // freeWinBingo = res.data.round_type == 1;
          // const round_type0Rate = round_type0RateList[rate];
          // const round_type1Rate = round_type1RateList[rate];
          // if(round_type0Rate && round_type1Rate){
          //     console.log('rollTotalInfo : canFreeWin');
          //     if(freeWinBingo){
          //         targetRate = round_type1Rate;
          //         rateKey += 'round_type1_';
          //         rollType = 'freeWin';
          //         globalThis.slotInfo.roundType = 'round_type1';
          //     }else{
          //         targetRate = round_type0Rate;
          //         rateKey += 'round_type0_';
          //         rollType = 'normal';
          //         globalThis.slotInfo.roundType = 'round_type0';
          //     }
          // }else{
          //     if(round_type0Rate){
          //         console.log('only round_type0Rate');
          //         targetRate = round_type0Rate;
          //         rateKey += 'round_type0_';
          //         rollType = 'normal';
          //         globalThis.slotInfo.roundType = 'round_type0';
          //     }else if(round_type1Rate){
          //         console.log('only round_type1Rate');
          //         targetRate = round_type1Rate;
          //         rateKey += 'round_type1_';
          //         rollType = 'freeWin';
          //         globalThis.slotInfo.roundType = 'round_type1';
          //     }
          // }
          // }else{
          //     if(round_type0RateList){
          //         targetRate = round_type0RateList[rate];
          //         rateKey += 'round_type0_';
          //         rollType = 'normal';
          //         globalThis.slotInfo.roundType = 'round_type0';
          //     }else if(round_type1RateList){
          //         targetRate = round_type1RateList[rate];
          //         rateKey += 'round_type1_';
          //         rollType = 'freeWin';
          //         globalThis.slotInfo.roundType = 'round_type1';
          //     }
          // }

          if (targetRate) {
            if (Array.isArray(targetRate) && targetRate.length > 0) {
              rateKey += rate;
              var rateInfo = this.rollRateInfo[rateKey];

              if (!rateInfo) {
                rateInfo = {};

                for (var i = 0; i < targetRate.length; i++) {
                  rateInfo[i] = 0;
                }

                this.rollRateInfo[rateKey] = rateInfo;
              }

              var rateInfoKeys = Object.keys(rateInfo);

              if (rateInfoKeys && rateInfoKeys.length > 0) {
                var minRound = rateInfo[rateInfoKeys[0]];

                for (var _i4 = 0; _i4 < rateInfoKeys.length; _i4++) {
                  var key = rateInfoKeys[_i4];

                  if (rateInfo[key] < minRound) {
                    minRound = rateInfo[key];
                  }
                }

                var targetSeqList = [];

                for (var _i5 = 0; _i5 < rateInfoKeys.length; _i5++) {
                  var _key = rateInfoKeys[_i5];

                  if (rateInfo[_key] == minRound) {
                    targetSeqList.push(_key);
                  }
                }

                var seq = (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                  error: Error()
                }), sUtil) : sUtil).RandomInt(0, targetSeqList.length);
                var targetSeq = targetSeqList[seq];
                this.currentSeq = targetSeq;
                globalThis.slotInfo.lastRoundSeq = targetSeq;
                console.log('currentBet seq:' + targetSeq + ' rollType:' + rollType + ' rate:' + rate);
                rateInfo[targetSeq]++;

                if (gameMode == 'normal') {
                  if (rollType == 'normal') {
                    this.rollTotalInfo.normal++;
                  } else if (rollType == 'freeWin') {
                    this.rollTotalInfo.freewin++;
                  }
                } // console.log('rateInfo:'+JSON.stringify(rateInfo));
                // console.log('rollTotalInfo:'+JSON.stringify(this.rollTotalInfo));


                sys.localStorage.setItem(gameID + '_subGameSlotRollTotalInfo', JSON.stringify(this.rollTotalInfo)); // sys.localStorage.setItem(rateKey,JSON.stringify(rateInfo));

                return targetRate[targetSeq];
              }
            }
          } // const req = sUtil.RandomInt(0,rateList.length);
          // this.oneRoundData = rateList[req].rounds;
          // if(!globalThis.slotInfo){
          //     globalThis.slotInfo = {};
          // }
          // globalThis.slotInfo.lastRoundReq = req;


          return null;
        }

        GetRoundData() {
          return this.oneRoundData;
        }
        /**
         * @zh
         * 获取当前round数据
         * @param index - 索引
         * @return one round
         */


        GetOneRoundData(index) {
          if (this.oneRoundData) {
            var round = this.oneRoundData[index];

            if (round) {
              return round;
            }
          }
        } //arr : hit_events_list


        GetAllEventConfigByHitList(arr) {
          if (arr && Array.isArray(arr)) {
            if (arr.length > 0) {
              if (Array.isArray(arr[0])) {
                var resList = [];
                var config = sConfigMgr.instance.getConfig();
                var events = config['event_config'];

                for (var i = 0; i < arr.length; i++) {
                  var _event = arr[i];

                  if (_event && Array.isArray(_event)) {
                    resList[i] = [];

                    for (var j = 0; j < _event.length; j++) {
                      var eve = _event[j];
                      var hitEvent = events[eve];
                      resList[i].push(hitEvent);
                    }
                  }
                }

                return resList;
              } else {
                var _resList = [];

                var _config = sConfigMgr.instance.getConfig();

                var _events = _config['event_config'];

                for (var _i6 = 0; _i6 < arr.length; _i6++) {
                  var _event2 = arr[_i6];
                  var _hitEvent = _events[_event2];

                  if (_hitEvent) {
                    _resList.push(_hitEvent);
                  }
                }

                return _resList;
              }
            } else {
              return [];
            }
          }
        }

        GetAllEventArrConfigByHitList(arr) {
          if (arr && Array.isArray(arr)) {
            var resList = [];
            var config = sConfigMgr.instance.getConfig();
            var events = config['event_config'];

            for (var i = 0; i < arr.length; i++) {
              var _event = arr[i];

              if (_event && Array.isArray(_event)) {
                resList[i] = [];

                for (var j = 0; j < _event.length; j++) {
                  var eve = _event[j];
                  var hitEvent = events[eve]; // if(hitEvent.event_type != 'boxAnima'){

                  resList[i].push(hitEvent); // }
                }
              }
            }

            return resList;
          }
        }

        GetAllEventConfigByHitListOnCheck(round) {
          if (round && round.hit_events_list && round.round_symbol_map) {
            var resList = [];
            var hits = round.hit_events_list;
            var config = sConfigMgr.instance.getConfig();
            var events = config['event_config'];

            for (var i = 0; i < hits.length; i++) {
              resList[i] = [];
              var _event = hits[i];

              if (_event && Array.isArray(_event)) {
                for (var j = 0; j < _event.length; j++) {
                  var eve = _event[j];
                  var hitEvent = events[eve];

                  if (hitEvent && hitEvent.event_type != 'boxAnima') {
                    resList[i].push(hitEvent);
                  }
                }

                if (i < round.round_symbol_map.length) {
                  if (globalThis.soltGameCheckWon) {
                    var checkRes = globalThis.soltGameCheckWon.checkWon(round.round_symbol_map[i]);

                    if (checkRes && checkRes.symbolWins) {
                      for (var c = 0; c < checkRes.symbolWins.length; c++) {
                        var checkItem = checkRes.symbolWins[c];

                        if (checkItem.rate && checkItem.rate > 0 && checkItem.pos && Array.isArray(checkItem.pos) && checkItem.pos.length > 0) {
                          var itemAdapter = {};
                          itemAdapter.event_type = 'boxAnima';
                          itemAdapter.rate_change = {
                            num: checkItem.rate
                          };
                          itemAdapter.bonus_symbol = checkItem.bonus_symbol;
                          itemAdapter.act_pos = checkItem.pos[0];
                          itemAdapter.opts = checkItem.opts;

                          for (var p = 1; p < checkItem.pos.length; p++) {
                            var pStr = checkItem.pos[p];
                            itemAdapter.act_pos += ',' + pStr;
                          }

                          resList[i].push(itemAdapter);
                        } else {
                          console.error('rate is null or pos is null,bonus_symbol : ' + checkItem.bonus_symbol);
                        }
                      }
                    } else {
                      console.error('symbolWins is empty');
                    }
                  } else {
                    console.error('on soltGameCheckWon');
                  }
                }
              }
            }

            return resList;
          }
        }

        GetEventConfigByHitListOnCheck(round) {
          if (round && round.hit_events_list && round.round_symbol_map) {
            var resList = [];
            var hits = round.hit_events_list;
            var config = sConfigMgr.instance.getConfig();
            var events = config['event_config'];
            var _event = hits;

            if (_event && Array.isArray(_event)) {
              for (var j = 0; j < _event.length; j++) {
                var eve = _event[j];
                var hitEvent = events[eve];

                if (hitEvent.event_type != 'boxAnima') {
                  resList.push(hitEvent);
                }
              }

              if (globalThis.soltGameCheckWon) {
                var checkRes = globalThis.soltGameCheckWon.checkWon(round.round_symbol_map); // console.log('checkRes:'+JSON.stringify(checkRes));

                if (checkRes && checkRes.symbolWins) {
                  for (var c = 0; c < checkRes.symbolWins.length; c++) {
                    var checkItem = checkRes.symbolWins[c];

                    if (checkItem.rate && checkItem.rate > 0 && checkItem.pos && Array.isArray(checkItem.pos) && checkItem.pos.length > 0) {
                      var itemAdapter = {};
                      itemAdapter.event_type = 'boxAnima';
                      itemAdapter.rate_change = {
                        num: checkItem.rate
                      };
                      itemAdapter.bonus_symbol = checkItem.bonus_symbol;
                      itemAdapter.act_pos = checkItem.pos[0];
                      itemAdapter.opts = checkItem.opts;

                      for (var p = 1; p < checkItem.pos.length; p++) {
                        var pStr = checkItem.pos[p];
                        itemAdapter.act_pos += ',' + pStr;
                      }

                      resList.push(itemAdapter);
                    } else {
                      console.error('rate is null or pos is null,bonus_symbol : ' + checkItem.bonus_symbol);
                    }
                  }
                } else {
                  console.error('symbolWins is empty');
                }
              } else {
                console.error('on soltGameCheckWon');
              }
            }

            return resList;
          }
        }

        GetAllEventOptConfigByHitListOnCheck(round) {
          if (round && round.hit_events_list && round.round_symbol_map) {
            var resList = [];
            var hits = round.hit_events_list;
            var config = sConfigMgr.instance.getConfig();
            var events = config['event_config'];

            for (var i = 0; i < hits.length; i++) {
              var boxInfo = [];
              var _event = hits[i];

              if (_event && Array.isArray(_event)) {
                for (var j = 0; j < _event.length; j++) {
                  var eve = _event[j];
                  var hitEvent = events[eve];

                  if (hitEvent && hitEvent.event_type != 'boxAnima' && hitEvent.event_type != 'bonusGame') {
                    boxInfo.push(hitEvent);
                  }
                }

                if (i < round.round_symbol_map.length) {
                  if (globalThis.soltGameCheckWon) {
                    var checkRes = globalThis.soltGameCheckWon.checkWon(round.round_symbol_map[i]);

                    if (checkRes) {
                      if (checkRes.symbolWins) {
                        for (var c = 0; c < checkRes.symbolWins.length; c++) {
                          var checkItem = checkRes.symbolWins[c];
                          var itemAdapter = {};

                          if (checkItem.rate && checkItem.rate > 0 && checkItem.pos && Array.isArray(checkItem.pos) && checkItem.pos.length > 0) {
                            itemAdapter.event_type = 'boxAnima';
                            itemAdapter.rate_change = {
                              num: checkItem.rate
                            };
                            itemAdapter.bonus_symbol = checkItem.bonus_symbol;
                            itemAdapter.act_pos = checkItem.pos[0];
                            itemAdapter.opts = checkItem.opts;

                            for (var p = 1; p < checkItem.pos.length; p++) {
                              var pStr = checkItem.pos[p];
                              itemAdapter.act_pos += ',' + pStr;
                            }

                            boxInfo.push(itemAdapter);
                          } else {
                            console.error('rate is null or pos is null,bonus_symbol : ' + checkItem.bonus_symbol);
                          }
                        }
                      }

                      resList[i] = {
                        eventData: boxInfo,
                        opt: checkRes.opt
                      };
                    } else {
                      console.error('symbolWins is empty');
                    }
                  } else {
                    console.error('on soltGameCheckWon');
                  }
                }
              }
            }

            return resList;
          }
        }

        IsMultiArrConfigHitList(arr) {
          if (arr && Array.isArray(arr)) {
            if (arr.length > 0) {
              if (arr[0] && Array.isArray(arr[0])) {
                return true;
              }
            }
          }

          return false;
        }

        ClearResData() {
          this.resArr = null;
        }

      }, _defineProperty(_class3, "instance", void 0), _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "configJson", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "boxDataJson", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "gameConfigJson", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=sConfigMgr.js.map
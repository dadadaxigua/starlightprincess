System.register(["cc"], function (_export, _context) {
  "use strict";

  var _cclegacy, _crd, info;

  return {
    setters: [function (_cc) {
      _cclegacy = _cc.cclegacy;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "afd35lu/bNOu4jQih5XtIym", "tipRes", undefined);

      info = {};

      _export("default", info);

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=tipRes.js.map
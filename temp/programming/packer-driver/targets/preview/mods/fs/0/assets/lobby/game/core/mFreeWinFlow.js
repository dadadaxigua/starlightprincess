System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2", "__unresolved_3", "__unresolved_4", "__unresolved_5", "__unresolved_6"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, director, sAudioMgr, sBoxMgr, sConfigMgr, sFreeWinFlow, sObjPool, sUtil, _dec, _class, _temp, _crd, ccclass, property, mFreeWinFlow;

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _reportPossibleCrUseOfsAudioMgr(extras) {
    _reporterNs.report("sAudioMgr", "./sAudioMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsBoxEntity(extras) {
    _reporterNs.report("sBoxEntity", "./sBoxEntity", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsBoxMgr(extras) {
    _reporterNs.report("sBoxMgr", "./sBoxMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsConfigMgr(extras) {
    _reporterNs.report("sConfigMgr", "./sConfigMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsFreeWinFlow(extras) {
    _reporterNs.report("sFreeWinFlow", "./sFreeWinFlow", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsObjPool(extras) {
    _reporterNs.report("sObjPool", "./sObjPool", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsUtil(extras) {
    _reporterNs.report("sUtil", "./sUtil", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      director = _cc.director;
    }, function (_unresolved_2) {
      sAudioMgr = _unresolved_2.default;
    }, function (_unresolved_3) {
      sBoxMgr = _unresolved_3.sBoxMgr;
    }, function (_unresolved_4) {
      sConfigMgr = _unresolved_4.sConfigMgr;
    }, function (_unresolved_5) {
      sFreeWinFlow = _unresolved_5.sFreeWinFlow;
    }, function (_unresolved_6) {
      sObjPool = _unresolved_6.sObjPool;
    }, function (_unresolved_7) {
      sUtil = _unresolved_7.sUtil;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "86981QOll5M+4e9Tw2ft6fk", "mFreeWinFlow", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("mFreeWinFlow", mFreeWinFlow = (_dec = ccclass('mFreeWinFlow'), _dec(_class = (_temp = class mFreeWinFlow extends (_crd && sFreeWinFlow === void 0 ? (_reportPossibleCrUseOfsFreeWinFlow({
        error: Error()
      }), sFreeWinFlow) : sFreeWinFlow) {
        constructor() {
          super(...arguments);

          _defineProperty(this, "overEffectArr", []);
        }

        onLoad() {
          super.onLoad();
        }

        start() {
          super.start();
        }

        rollFirstRoundAction(round, speedMode, clickMode) {
          super.rollFirstRoundAction(round, speedMode, clickMode);

          if (round) {
            var freeWinArr = [],
                targetColumn = [];
            this.isFirstWin = false;
            (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
              error: Error()
            }), sConfigMgr) : sConfigMgr).instance.SetResData(round['round_symbol_map']);
            var events = (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
              error: Error()
            }), sConfigMgr) : sConfigMgr).instance.GetEventConfigByHitListOnCheck(round); // const events = sConfigMgr.instance.GetAllEventConfigByHitList(round['hit_events_list']);

            var boxSize = (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
              error: Error()
            }), sBoxMgr) : sBoxMgr).instance.getBoxSize();

            var _boxViewSize = (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
              error: Error()
            }), sBoxMgr) : sBoxMgr).instance.getBoxViewSize();

            var round_symbol_map = round['round_symbol_map'];
            director.on('rollOneColumnBump', col => {
              if (col == boxSize.x - 1) {
                (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                  error: Error()
                }), sAudioMgr) : sAudioMgr).StopAudio();
              }

              (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                error: Error()
              }), sAudioMgr) : sAudioMgr).PlayShotAudio('boxRowStop');

              if (round_symbol_map) {
                var girlBingo = false;
                var scatterBingo = false;
                var column = round_symbol_map[col];

                if (column && Array.isArray(column)) {
                  var yDelta = (boxSize.y - _boxViewSize.y) / 2;

                  for (var i = yDelta; i < yDelta + _boxViewSize.y; i++) {
                    var data = column[i];

                    if (data >= 1000) {
                      girlBingo = true;
                    } else if (data == 1) {
                      scatterBingo = true;
                    }

                    if (i == yDelta) {
                      if (data == 1004) {
                        (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                          error: Error()
                        }), sAudioMgr) : sAudioMgr).PlayShotAudio('girlSound1_' + ((_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                          error: Error()
                        }), sUtil) : sUtil).RandomInt(0, 2) == 0 ? '0' : '1'));
                      } else if (data == 2004) {
                        (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                          error: Error()
                        }), sAudioMgr) : sAudioMgr).PlayShotAudio('girlSound2_' + ((_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                          error: Error()
                        }), sUtil) : sUtil).RandomInt(0, 2) == 0 ? '0' : '1'));
                      } else if (data == 3004) {
                        (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                          error: Error()
                        }), sAudioMgr) : sAudioMgr).PlayShotAudio('girlSound3_' + ((_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                          error: Error()
                        }), sUtil) : sUtil).RandomInt(0, 2) == 0 ? '0' : '1'));
                      } else if (data == 4004) {
                        (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                          error: Error()
                        }), sAudioMgr) : sAudioMgr).PlayShotAudio('girlSound4_' + ((_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                          error: Error()
                        }), sUtil) : sUtil).RandomInt(0, 2) == 0 ? '0' : '1'));
                      } else if (data == 5004) {
                        (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                          error: Error()
                        }), sAudioMgr) : sAudioMgr).PlayShotAudio('girlSound5_' + ((_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                          error: Error()
                        }), sUtil) : sUtil).RandomInt(0, 2) == 0 ? '0' : '1'));
                      }
                    }
                  }
                }

                if (girlBingo) {
                  (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                    error: Error()
                  }), sAudioMgr) : sAudioMgr).PlayShotAudio('girlBingo');
                }

                if (scatterBingo) {
                  (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                    error: Error()
                  }), sAudioMgr) : sAudioMgr).PlayShotAudio('scatterBingo');
                }
              }

              if (targetColumn.length > 0) {
                if (col == targetColumn.length - 1) {
                  if (speedMode == 'normal') {
                    (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                      error: Error()
                    }), sAudioMgr) : sAudioMgr).PlayShotAudio('freeWinOneColBingo');
                  }

                  if (speedMode == 'normal') {
                    this.clearColumnOverEffect();
                    this.scheduleOnce(() => {
                      (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                        error: Error()
                      }), sBoxMgr) : sBoxMgr).instance.blackCurtainAnima(targetColumn.length);
                    }, 0.5);
                    this.showColumnOverEffect(targetColumn.length);
                  }
                } // else if(freeWinArr.indexOf(col + 1) >= 0){
                //     sAudioMgr.PlayShotAudio('freeWinOneColBingo');
                //     sBoxMgr.instance.blackCurtain(col + 1);
                // }

              }
            }, this);

            if (events) {
              for (var i = 0; i < events.length; i++) {
                var _event = events[i];

                if (_event.event_type == 'speedAnima') {
                  var actColumn = _event.act_column.split(',');

                  if (actColumn && actColumn.length > 0) {
                    for (var a = 0; a < actColumn.length; a++) {
                      var element = actColumn[a];
                      freeWinArr.push(parseInt(element));
                    }
                  }
                } else if (_event.event_type == 'changeRound') {
                  this.freeWinCount = _event['round_change'].num;
                  this.freeWinIndex = 0;
                } else if (_event['event_type'] == 'boxAnima') {
                  this.isFirstWin = true;
                }
              }

              if (freeWinArr) {
                for (var _i = 0; _i < boxSize.x; _i++) {
                  if (freeWinArr.indexOf(_i) < 0) {
                    targetColumn.push(_i);
                  }
                }

                if (speedMode == 'turbo') {
                  this.turboFlowDelayCall(() => {
                    (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                      error: Error()
                    }), sBoxMgr) : sBoxMgr).instance.rollStopAllColumn();

                    if (this.isFirstWin) {
                      this.scheduleOnce(() => {
                        this.rollShowResAction(round);
                      }, 3);
                    } else {
                      (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                        error: Error()
                      }), sUtil) : sUtil).once('rollStopOneRoundCall', () => {
                        (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                          error: Error()
                        }), sBoxMgr) : sBoxMgr).instance.setAllViewTargetBoxArrayState(symbol => {
                          return symbol == 1;
                        }, 'win', 'settlement');
                        this.scheduleOnce(() => {
                          (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                            error: Error()
                          }), sBoxMgr) : sBoxMgr).instance.setAllViewTargetBoxArrayState(symbol => {
                            return symbol == 1;
                          }, 'idle', 'settlement');
                        }, 1);
                        (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                          error: Error()
                        }), sBoxMgr) : sBoxMgr).instance.blackArrCurtainAnima([], false);
                        (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                          error: Error()
                        }), sAudioMgr) : sAudioMgr).PlayShotAudio('freeWinBingoCheer');
                      });
                      this.scheduleOnce(() => {
                        director.off('rollOneColumnStop');
                        director.emit('freeWinBingo', this.freeWinCount, round.rate);
                      }, 3);
                    }
                  });
                } else if (speedMode == 'normal') {
                  this.normalFlowDelayCall(() => {
                    (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                      error: Error()
                    }), sBoxMgr) : sBoxMgr).instance.rollStopByColumnArr(targetColumn, col => {
                      this.scheduleOnce(() => {
                        (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                          error: Error()
                        }), sBoxMgr) : sBoxMgr).instance.setColumnAllTargetBoxArrayState(col, symbol => {
                          return symbol > 1000;
                        }, 'spawn', 'settlement');
                        (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                          error: Error()
                        }), sBoxMgr) : sBoxMgr).instance.setColumnTargetBoxArrayState(col, symbol => {
                          return symbol == 1;
                        }, 'win', 'settlement');
                        this.scheduleOnce(() => {
                          (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                            error: Error()
                          }), sBoxMgr) : sBoxMgr).instance.setColumnTargetBoxArrayState(col, symbol => {
                            return symbol == 1;
                          }, 'idle', 'settlement');
                        }, 1);
                      }, 0.1);
                    }, () => {
                      var index = 0;

                      var freeWinCall = () => {
                        if (index < freeWinArr.length) {
                          var col = freeWinArr[index++];
                          this.setMotionTweenSpeedByIndex(col);
                          this.freeWinSpeedAnimaDelayCall(col, () => {
                            (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                              error: Error()
                            }), sBoxMgr) : sBoxMgr).instance.rollStopBoxBySelf(col, 0, false, _col => {
                              (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                                error: Error()
                              }), sBoxMgr) : sBoxMgr).instance.setColumnAllTargetBoxArrayState(_col, symbol => {
                                return symbol > 1000;
                              }, 'spawn', 'settlement');
                              (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                                error: Error()
                              }), sBoxMgr) : sBoxMgr).instance.setColumnTargetBoxArrayState(_col, symbol => {
                                return symbol == 1;
                              }, 'win', 'settlement');
                              this.scheduleOnce(() => {
                                (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                                  error: Error()
                                }), sBoxMgr) : sBoxMgr).instance.setColumnTargetBoxArrayState(_col, symbol => {
                                  return symbol == 1;
                                }, 'idle', 'settlement');
                              }, 1);

                              if (freeWinArr.indexOf(_col + 1) >= 0) {
                                this.clearColumnOverEffect();
                                (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                                  error: Error()
                                }), sAudioMgr) : sAudioMgr).PlayShotAudio('freeWinOneColBingo');
                                (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                                  error: Error()
                                }), sBoxMgr) : sBoxMgr).instance.blackCurtain(_col + 1);
                                this.showColumnOverEffect(_col + 1);
                              }

                              freeWinCall();
                            });
                          });
                        } else {
                          if (this.isFirstWin) {
                            this.clearColumnOverEffect();
                            (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                              error: Error()
                            }), sBoxMgr) : sBoxMgr).instance.blackCurtainAnima(-1, false);
                            this.scheduleOnce(() => {
                              this.rollShowResAction(round);
                            }, 0.5);
                          } else {
                            this.clearColumnOverEffect();
                            director.off('rollOneColumnStop');
                            director.off('rollOneColumnBump');
                            (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                              error: Error()
                            }), sBoxMgr) : sBoxMgr).instance.blackCurtainAnima(-1, false);
                            (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                              error: Error()
                            }), sBoxMgr) : sBoxMgr).instance.setAllViewTargetBoxArrayState(symbol => {
                              return symbol == 1;
                            }, 'win', 'settlement');
                            this.flowDelayCall(() => {
                              (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                                error: Error()
                              }), sBoxMgr) : sBoxMgr).instance.setAllViewTargetBoxArrayState(symbol => {
                                return symbol == 1;
                              }, 'idle', 'settlement');
                              this.scheduleOnce(() => {
                                director.emit('freeWinBingo', this.freeWinCount, 0);
                              }, 2);
                            }, 1);
                            (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                              error: Error()
                            }), sAudioMgr) : sAudioMgr).PlayShotAudio('freeWinBingoCheer');
                          }
                        }
                      };

                      freeWinCall();
                    });
                  });
                }
              }
            }
          }
        }

        rollStopAction(round, rollSpeedMode, clickMode) {
          var _this = this;

          if (round) {
            (function () {
              (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
                error: Error()
              }), sConfigMgr) : sConfigMgr).instance.SetResData(round['round_symbol_map']);
              var events = (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
                error: Error()
              }), sConfigMgr) : sConfigMgr).instance.GetEventConfigByHitListOnCheck(round); // const events = sConfigMgr.instance.GetAllEventConfigByHitList(round['hit_events_list']);

              var _boxViewSize = (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                error: Error()
              }), sBoxMgr) : sBoxMgr).instance.getBoxViewSize();

              var _boxSize = (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                error: Error()
              }), sBoxMgr) : sBoxMgr).instance.getBoxSize();

              var round_symbol_map = round['round_symbol_map'];
              var act_columnArr = [];
              var freeWinArr = [],
                  targetColumn = [];
              var freeCount = 0;
              director.on('rollFinishOneRoundCall', col => {
                if (round_symbol_map) {
                  var column = round_symbol_map[col];

                  if (column && Array.isArray(column)) {
                    var yDelta = (_boxSize.y - _boxViewSize.y) / 2;

                    for (var i = yDelta; i < yDelta + _boxViewSize.y; i++) {
                      var data = column[i];

                      if (i == yDelta) {
                        if (data == 1004) {
                          (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                            error: Error()
                          }), sAudioMgr) : sAudioMgr).PlayShotAudio('girlSound1_' + ((_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                            error: Error()
                          }), sUtil) : sUtil).RandomInt(0, 2) == 0 ? '0' : '1'));
                        } else if (data == 2004) {
                          (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                            error: Error()
                          }), sAudioMgr) : sAudioMgr).PlayShotAudio('girlSound2_' + ((_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                            error: Error()
                          }), sUtil) : sUtil).RandomInt(0, 2) == 0 ? '0' : '1'));
                        } else if (data == 3004) {
                          (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                            error: Error()
                          }), sAudioMgr) : sAudioMgr).PlayShotAudio('girlSound3_' + ((_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                            error: Error()
                          }), sUtil) : sUtil).RandomInt(0, 2) == 0 ? '0' : '1'));
                        } else if (data == 4004) {
                          (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                            error: Error()
                          }), sAudioMgr) : sAudioMgr).PlayShotAudio('girlSound4_' + ((_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                            error: Error()
                          }), sUtil) : sUtil).RandomInt(0, 2) == 0 ? '0' : '1'));
                        } else if (data == 5004) {
                          (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                            error: Error()
                          }), sAudioMgr) : sAudioMgr).PlayShotAudio('girlSound5_' + ((_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                            error: Error()
                          }), sUtil) : sUtil).RandomInt(0, 2) == 0 ? '0' : '1'));
                        }
                      }
                    }
                  }
                }
              }, _this);
              director.on('rollOneColumnBump', col => {
                if (col == _boxViewSize.x - 1) {
                  (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                    error: Error()
                  }), sAudioMgr) : sAudioMgr).StopAudio();
                }

                (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                  error: Error()
                }), sAudioMgr) : sAudioMgr).PlayShotAudio('boxRowStop');

                if (round_symbol_map) {
                  var girlBingo = false;
                  var scatterBingo = false;
                  var column = round_symbol_map[col];

                  if (column && Array.isArray(column)) {
                    var yDelta = (_boxSize.y - _boxViewSize.y) / 2;

                    for (var i = yDelta; i < yDelta + _boxViewSize.y; i++) {
                      var data = column[i];

                      if (data >= 1000) {
                        girlBingo = true;
                      } else if (data == 1) {
                        scatterBingo = true;
                      }
                    }
                  }

                  if (girlBingo) {
                    (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                      error: Error()
                    }), sAudioMgr) : sAudioMgr).PlayShotAudio('girlBingo');
                  }

                  if (scatterBingo) {
                    (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                      error: Error()
                    }), sAudioMgr) : sAudioMgr).PlayShotAudio('scatterBingo');
                  }
                }
              }, _this);

              if (events) {
                for (var i = 0; i < events.length; i++) {
                  var _event = events[i];

                  if (_event['event_type'] == 'rollbackAnima') {
                    var col = _event['act_column'];
                    act_columnArr.push(parseInt(col));
                  } else if (_event.event_type == 'speedAnima') {
                    var actColumn = _event.act_column.split(',');

                    if (actColumn && actColumn.length > 0) {
                      for (var a = 0; a < actColumn.length; a++) {
                        var element = actColumn[a];
                        freeWinArr.push(parseInt(element));
                      }
                    }
                  } else if (_event.event_type == 'changeRound') {
                    freeCount = _event['round_change'].num;
                  }
                }
              }

              for (var _i2 = 0; _i2 < _boxViewSize.x; _i2++) {
                if (freeWinArr.indexOf(_i2) < 0) {
                  targetColumn.push(_i2);
                }
              } //speedAnima


              if (freeWinArr.length > 0 && targetColumn.length > 0) {
                var tPos = (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                  error: Error()
                }), sBoxMgr) : sBoxMgr).instance.getRollStopPosByColumn(0);

                for (var x = 0; x < targetColumn.length; x++) {
                  var column = targetColumn[x];

                  if (act_columnArr.indexOf(column) < 0) {
                    (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                      error: Error()
                    }), sBoxMgr) : sBoxMgr).instance.rollStopBoxBySelf(column, 0, false, col => {
                      (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                        error: Error()
                      }), sBoxMgr) : sBoxMgr).instance.setColumnAllTargetBoxArrayState(col, symbol => {
                        return symbol > 1000;
                      }, 'spawn', 'settlement');
                      (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                        error: Error()
                      }), sBoxMgr) : sBoxMgr).instance.setColumnTargetBoxArrayState(col, symbol => {
                        return symbol == 1;
                      }, 'win', 'settlement');

                      _this.scheduleOnce(() => {
                        (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                          error: Error()
                        }), sBoxMgr) : sBoxMgr).instance.setColumnTargetBoxArrayState(col, symbol => {
                          return symbol == 1;
                        }, 'idle', 'settlement');
                      }, 1);

                      if (col == targetColumn.length - 1) {
                        _this.clearColumnOverEffect();

                        (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                          error: Error()
                        }), sAudioMgr) : sAudioMgr).PlayShotAudio('freeWinOneColBingo');

                        _this.scheduleOnce(() => {
                          (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                            error: Error()
                          }), sBoxMgr) : sBoxMgr).instance.blackCurtainAnima(col + 1);
                        }, 0.5);

                        _this.showColumnOverEffect(col + 1);
                      }
                    });
                  } else {
                    (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                      error: Error()
                    }), sBoxMgr) : sBoxMgr).instance.rollStopBoxBySelf(column, (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                      error: Error()
                    }), sUtil) : sUtil).RandomInt(1, _boxViewSize.y), true, col => {
                      (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                        error: Error()
                      }), sBoxMgr) : sBoxMgr).instance.setColumnAllTargetBoxArrayState(col, symbol => {
                        return symbol > 1000;
                      }, 'spawn', 'settlement');
                      (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                        error: Error()
                      }), sBoxMgr) : sBoxMgr).instance.setColumnTargetBoxArrayState(col, symbol => {
                        return symbol == 1;
                      }, 'win', 'settlement');

                      _this.scheduleOnce(() => {
                        (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                          error: Error()
                        }), sBoxMgr) : sBoxMgr).instance.setColumnTargetBoxArrayState(col, symbol => {
                          return symbol == 1;
                        }, 'idle', 'settlement');
                      }, 1);

                      if (col == targetColumn.length - 1) {
                        _this.clearColumnOverEffect();

                        (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                          error: Error()
                        }), sAudioMgr) : sAudioMgr).PlayShotAudio('freeWinOneColBingo');

                        _this.scheduleOnce(() => {
                          (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                            error: Error()
                          }), sBoxMgr) : sBoxMgr).instance.blackCurtainAnima(col + 1);
                        }, 0.5);

                        _this.showColumnOverEffect(col + 1);
                      }
                    });
                  }
                }

                var act_columnArrCall = () => {
                  if (act_columnArr.length > 0) {
                    (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                      error: Error()
                    }), sAudioMgr) : sAudioMgr).PlayShotAudio('waterRollBack');
                    (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                      error: Error()
                    }), sBoxMgr) : sBoxMgr).instance.blackArrCurtainAnima(act_columnArr, true);
                    (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                      error: Error()
                    }), sBoxMgr) : sBoxMgr).instance.clearEffectAllBoxArray('rest', 'settlement');
                    (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                      error: Error()
                    }), sBoxMgr) : sBoxMgr).instance.setBoxArrayState('rest', 'settlement');

                    for (var c = 0; c < act_columnArr.length; c++) {
                      var waterCol = act_columnArr[c];

                      _this.waterAnima(waterCol);
                    }

                    _this.scheduleOnce(() => {
                      (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                        error: Error()
                      }), sUtil) : sUtil).once('rollFinishOneRoundCall', rollBackCol => {
                        _this.rollShowResAction(round);
                      });
                      director.emit('rollStopBoxBySelfRollBack');
                    }, 0.6);
                  } else {
                    _this.rollShowResAction(round);
                  }
                };

                var index = 0;

                var freeWinCall = () => {
                  if (index < freeWinArr.length) {
                    var _col2 = freeWinArr[index++];

                    if (act_columnArr.indexOf(_col2) < 0) {
                      _this.setMotionTweenSpeedByIndex(_col2);

                      _this.freeWinSpeedAnimaDelayCall(_col2, () => {
                        (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                          error: Error()
                        }), sBoxMgr) : sBoxMgr).instance.rollStopBoxBySelf(_col2, 0, false, col => {
                          (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                            error: Error()
                          }), sBoxMgr) : sBoxMgr).instance.setColumnAllTargetBoxArrayState(col, symbol => {
                            return symbol > 1000;
                          }, 'spawn', 'settlement');
                          (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                            error: Error()
                          }), sBoxMgr) : sBoxMgr).instance.setColumnTargetBoxArrayState(col, symbol => {
                            return symbol == 1;
                          }, 'win', 'settlement');

                          _this.scheduleOnce(() => {
                            (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                              error: Error()
                            }), sBoxMgr) : sBoxMgr).instance.setColumnTargetBoxArrayState(col, symbol => {
                              return symbol == 1;
                            }, 'idle', 'settlement');
                          }, 1);

                          if (freeWinArr.indexOf(col + 1) >= 0) {
                            _this.clearColumnOverEffect();

                            (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                              error: Error()
                            }), sAudioMgr) : sAudioMgr).PlayShotAudio('freeWinOneColBingo');
                            (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                              error: Error()
                            }), sBoxMgr) : sBoxMgr).instance.blackCurtain(col + 1);

                            _this.showColumnOverEffect(col + 1);
                          }

                          if (col == _boxViewSize.x - 1) {
                            _this.clearColumnOverEffect();

                            (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                              error: Error()
                            }), sBoxMgr) : sBoxMgr).instance.blackArrCurtainAnima([], false);

                            if (freeCount > 0) {
                              var nowIndex = _this.freeWinCount - _this.freeWinIndex;
                              _this.freeWinCount += freeCount;
                              (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                                error: Error()
                              }), sUtil) : sUtil).once('freeWinUpFinish', () => {
                                act_columnArrCall();
                              });
                              director.emit('freeWinUp', nowIndex, freeCount);
                            } else {
                              _this.clearColumnOverEffect();

                              (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                                error: Error()
                              }), sBoxMgr) : sBoxMgr).instance.blackArrCurtainAnima([], false);

                              if (act_columnArr.length > 0) {
                                act_columnArrCall();
                              } else {
                                _this.rollShowResAction(round);
                              }
                            }
                          }
                        });
                        freeWinCall();
                      });
                    } else {
                      _this.setMotionTweenSpeedByIndex(_col2);

                      _this.freeWinSpeedAnimaDelayCall(_col2, () => {
                        (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                          error: Error()
                        }), sBoxMgr) : sBoxMgr).instance.rollStopBoxBySelf(_col2, (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                          error: Error()
                        }), sUtil) : sUtil).RandomInt(1, _boxViewSize.y), true, col => {
                          (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                            error: Error()
                          }), sBoxMgr) : sBoxMgr).instance.setColumnAllTargetBoxArrayState(col, symbol => {
                            return symbol > 1000;
                          }, 'spawn', 'settlement');
                          (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                            error: Error()
                          }), sBoxMgr) : sBoxMgr).instance.setColumnTargetBoxArrayState(col, symbol => {
                            return symbol == 1;
                          }, 'win', 'settlement');

                          _this.scheduleOnce(() => {
                            (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                              error: Error()
                            }), sBoxMgr) : sBoxMgr).instance.setColumnTargetBoxArrayState(col, symbol => {
                              return symbol == 1;
                            }, 'idle', 'settlement');
                          }, 1);

                          if (freeWinArr.indexOf(col + 1) >= 0) {
                            _this.clearColumnOverEffect();

                            (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                              error: Error()
                            }), sAudioMgr) : sAudioMgr).PlayShotAudio('freeWinOneColBingo');
                            (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                              error: Error()
                            }), sBoxMgr) : sBoxMgr).instance.blackCurtain(col + 1);

                            _this.showColumnOverEffect(col + 1);
                          }

                          if (col == _boxViewSize.x - 1) {
                            (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                              error: Error()
                            }), sBoxMgr) : sBoxMgr).instance.blackArrCurtainAnima(act_columnArr);

                            _this.clearColumnOverEffect();

                            (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                              error: Error()
                            }), sBoxMgr) : sBoxMgr).instance.blackArrCurtainAnima([], false);

                            if (freeCount > 0) {
                              var nowIndex = _this.freeWinCount - _this.freeWinIndex;
                              _this.freeWinCount += freeCount;
                              (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                                error: Error()
                              }), sUtil) : sUtil).once('freeWinUpFinish', () => {
                                if (act_columnArr.length > 0) {
                                  act_columnArrCall();
                                } else {
                                  _this.rollShowResAction(round);
                                }
                              });
                              director.emit('freeWinUp', nowIndex, freeCount);
                            } else {
                              if (act_columnArr.length > 0) {
                                act_columnArrCall();
                              } else {
                                _this.rollShowResAction(round);
                              }
                            }
                          }

                          freeWinCall();
                        });
                      });
                    }
                  }
                };

                freeWinCall();
              } //normal
              else {
                  _this.normalFlowDelayCall(() => {
                    if (act_columnArr.length > 0) {
                      (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                        error: Error()
                      }), sBoxMgr) : sBoxMgr).instance.rollStopFloatBack(act_columnArr, true, () => {
                        // sBoxMgr.instance.blackArrCurtainAnima(act_columnArr);
                        // sBoxMgr.instance.clearEffectAllBoxArray('rest', 'settlement');
                        // sBoxMgr.instance.setBoxArrayState('rest', 'settlement');
                        if (act_columnArr.length > 0) {
                          (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                            error: Error()
                          }), sAudioMgr) : sAudioMgr).PlayShotAudio('waterRollBack');

                          for (var c = 0; c < act_columnArr.length; c++) {
                            var waterCol = act_columnArr[c];

                            _this.waterAnima(waterCol);
                          }
                        }

                        _this.scheduleOnce(() => {
                          (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                            error: Error()
                          }), sBoxMgr) : sBoxMgr).instance.blackArrCurtainAnima(act_columnArr);
                          (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                            error: Error()
                          }), sBoxMgr) : sBoxMgr).instance.clearEffectAllBoxArray('rest', 'settlement');
                          (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                            error: Error()
                          }), sBoxMgr) : sBoxMgr).instance.setBoxArrayState('rest', 'settlement');
                          director.emit('rollStopBoxBySelfRollBack');
                        }, 0.6);
                      });
                    } else {
                      (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                        error: Error()
                      }), sBoxMgr) : sBoxMgr).instance.rollStopAllColumn();
                    }

                    (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                      error: Error()
                    }), sUtil) : sUtil).once('rollStopOneRoundCall', () => {
                      // console.log('rollStopOneRoundCall');
                      _this.rollShowResAction(round);
                    });
                  });
                }
            })();
          }
        }

        rollShowResAction(round) {
          director.off('rollOneColumnStop');
          director.off('rollOneColumnBump');
          director.off('rollFinishOneRoundCall');
          this.boxLightsOnAction(round);
        }

        byLineBoxLightCtr(round) {
          var events = (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
            error: Error()
          }), sConfigMgr) : sConfigMgr).instance.GetEventConfigByHitListOnCheck(round); // const events = sConfigMgr.instance.GetAllEventConfigByHitList(round['hit_events_list']);

          var tileAnimaArr = [];
          var isWin = false;

          if (events) {
            var resActObj = {};

            for (var i = 0; i < events.length; i++) {
              var _event = events[i];

              if (_event['event_type'] == 'boxAnima') {
                var acts = _event['act_pos'].split(',');

                if (acts) {
                  isWin = true;

                  for (var a = 0; a < acts.length; a++) {
                    var act = acts[a];
                    resActObj[act] = true;
                  }
                }
              } else if (_event['event_type'] == 'titleAnima') {
                var _acts = _event['act_pos'].split(',');

                if (_acts) {
                  for (var _a = 0; _a < _acts.length; _a++) {
                    var _act = _acts[_a];
                    resActObj[_act] = true;
                  }
                }

                tileAnimaArr.push(_event['act_column']);
              }
            }

            if (isWin) {
              (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                error: Error()
              }), sAudioMgr) : sAudioMgr).PlayShotAudio('normalWin');
              (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                error: Error()
              }), sBoxMgr) : sBoxMgr).instance.blackCurtainAnima();
              this.byLineBoxLightOn(resActObj);
            }
          }

          if (tileAnimaArr.length > 0) {
            director.emit('titleMultipleAnima', tileAnimaArr, isWin);
          }

          if (isWin) {
            (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
              error: Error()
            }), sUtil) : sUtil).once('titleWinSettleEnd', () => {
              this.titleWinSettleEnd(round);
            });

            if (tileAnimaArr.length > 0) {
              var multiSum = 1;

              for (var _i3 = 0; _i3 < tileAnimaArr.length; _i3++) {
                var value = tileAnimaArr[_i3];
                multiSum *= parseInt(value) + 1;
              }

              var realRate = round.rate;

              if (!(tileAnimaArr.length == 1 && tileAnimaArr[0] == 0)) {
                director.emit('lowWinTiltleAnima', Math.floor(globalThis.GameBtnEntity.CurrentBetAmount * round.rate / multiSum));
              }

              if (tileAnimaArr.length == 1 && tileAnimaArr[0] == 0) {
                director.emit('winBetRes', globalThis.GameBtnEntity.CurrentBetAmount * realRate, realRate);
              } else {
                (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                  error: Error()
                }), sUtil) : sUtil).once('titleMultipleAnimaEnd', () => {
                  director.emit('winBetRes', globalThis.GameBtnEntity.CurrentBetAmount * realRate, realRate);
                });
              }
            } else {
              director.emit('winBetRes', globalThis.GameBtnEntity.CurrentBetAmount * round.rate, round.rate);
            }

            director.emit('betUserInfoUpdateWinAnima', round.rate);
          } else {
            this.titleWinSettleEnd(round);
          }
        }

        titleWinSettleEnd(round) {
          if (this.isFirstWin) {
            this.isFirstWin = false;
            this.scheduleOnce(() => {
              (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                error: Error()
              }), sBoxMgr) : sBoxMgr).instance.clearEffectAllBoxArray('rest', 'settlement');
              (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                error: Error()
              }), sBoxMgr) : sBoxMgr).instance.setBoxArrayState('rest', 'settlement');
              (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                error: Error()
              }), sBoxMgr) : sBoxMgr).instance.setAllViewTargetBoxArrayState(symbol => {
                return symbol == 1;
              }, 'win', 'settlement');
              this.scheduleOnce(() => {
                (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                  error: Error()
                }), sBoxMgr) : sBoxMgr).instance.setAllViewTargetBoxArrayState(symbol => {
                  return symbol == 1;
                }, 'idle', 'settlement');
                this.scheduleOnce(() => {
                  director.emit('freeWinBingo', this.freeWinCount, round.rate);
                }, 2);
              }, 1);
              (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                error: Error()
              }), sBoxMgr) : sBoxMgr).instance.blackArrCurtainAnima([], false);
              (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                error: Error()
              }), sAudioMgr) : sAudioMgr).PlayShotAudio('freeWinBingoCheer');
            }, 2);
          } else {
            director.emit('freeWinBetShowCoin', round.rate);
            this.scheduleOnce(() => {
              director.emit('freeWinOneRoundEnd');
            }, 0.8);
          }
        }

        showColumnOverEffect(col) {
          // sBoxMgr.instance.blackCurtainAnima(col);
          director.emit('freeGameStart', col);
          var res = (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
            error: Error()
          }), sBoxMgr) : sBoxMgr).instance.GetColAllBoxPos(col);

          if (res && res.length > 0) {
            for (var i = 0; i < res.length; i++) {
              var pos = res[i];
              var overEffect = (_crd && sObjPool === void 0 ? (_reportPossibleCrUseOfsObjPool({
                error: Error()
              }), sObjPool) : sObjPool).Dequeue('overLayerEffectItem');
              overEffect.node.position = pos;
              overEffect.node.active = true;
              overEffect.boxItemUpdate(null, null, 'win', null);
              this.overEffectArr.push(overEffect);
            }
          }
        }

        clearColumnOverEffect() {
          director.emit('freeGameEnd');

          if (this.overEffectArr && this.overEffectArr.length > 0) {
            for (var i = 0; i < this.overEffectArr.length; i++) {
              var over = this.overEffectArr[i];
              (_crd && sObjPool === void 0 ? (_reportPossibleCrUseOfsObjPool({
                error: Error()
              }), sObjPool) : sObjPool).Enqueue('overLayerEffectItem', over);
              over.clearItem(null, null, null);
              over.node.active = false;
            }

            this.overEffectArr = [];
          }
        }

        waterAnima(col) {
          (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
            error: Error()
          }), sBoxMgr) : sBoxMgr).instance.setColumnAllTargetBoxArrayState(col, symbol => {
            return symbol > 1000;
          }, 'idle', 'settlement');
          var arr = (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
            error: Error()
          }), sBoxMgr) : sBoxMgr).instance.GetColAllBox(col);
          var isDownward = false;
          var targetBox = null;

          for (var i = 0; i < arr.length; i++) {
            var box = arr[i];

            if (i == 0) {
              if (box.SymbolValue < 1000) {
                isDownward = true;
              } else {
                isDownward = false;
              }
            } else {
              if (isDownward) {
                if (box.SymbolValue > 1000) {
                  targetBox = box;
                  break;
                }
              } else {
                if (box.SymbolValue < 1000) {
                  targetBox = arr[i - 1];
                  break;
                }
              }
            }
          }

          if (targetBox) {
            targetBox.DoEnitityAction('girlShake');
            targetBox.DoEnitityAction('waterAnima', isDownward);
          }
        }

      }, _temp)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=mFreeWinFlow.js.map
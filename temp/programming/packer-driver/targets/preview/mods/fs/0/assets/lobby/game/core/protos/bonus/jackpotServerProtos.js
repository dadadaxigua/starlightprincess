System.register(["cc"], function (_export, _context) {
  "use strict";

  var _cclegacy, _crd;

  return {
    setters: [function (_cc) {
      _cclegacy = _cc.cclegacy;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "234f3h8nNBNtIYR/SZ8Fffl", "jackpotServerProtos", undefined);

      _export("default", {
        "nested": {
          "user": {
            "nested": {
              "jackpotHandler": {
                "nested": {
                  "jackpot": {
                    "fields": {
                      "code": {
                        "type": "int32",
                        "id": 1
                      },
                      "data": {
                        "rule": "repeated",
                        "type": "Data",
                        "id": 2
                      },
                      "message": {
                        "type": "string",
                        "id": 3
                      }
                    },
                    "nested": {
                      "Data": {
                        "fields": {
                          "jackpot_id": {
                            "type": "int32",
                            "id": 1
                          },
                          "game_id": {
                            "type": "string",
                            "id": 2
                          },
                          "bet_min": {
                            "type": "int32",
                            "id": 3
                          },
                          "bet_max": {
                            "type": "int32",
                            "id": 4
                          },
                          "last_jackpot": {
                            "type": "double",
                            "id": 5
                          },
                          "jackpot_speed": {
                            "type": "double",
                            "id": 6
                          },
                          "last_jackpot_time": {
                            "type": "double",
                            "id": 7
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      });

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=jackpotServerProtos.js.map
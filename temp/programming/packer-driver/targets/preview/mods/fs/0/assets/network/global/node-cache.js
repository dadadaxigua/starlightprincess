/**
 * 注意：已把原脚本注释，由于脚本变动过大，转换的时候可能有遗落，需要自行手动转换
 */
// var nodes = {};
// var instantiateNodes = {};
// var NodesCache = {
//     Key : {
//         'CoinSingle' : 'CoinSingle',
//         'CoinJump' : 'CoinJump',
//         'GetGoldLable' : {
//             '1':'mySelfGetGodeLable',
//             '2':'otherGetGodeLable'
//         },
//         // 鱼的死亡特效   小鱼      中鱼         大鱼       炸弹
//         'DeadEff':{
//             '1':'SmallDead',
//             '2':'MiddleDead',
//             '3':'BigDead',
//             '4':'BombDead',
//             '5':'ChainEveryNode'
//         },
//         'RotaryTable':{
//             '1':'RotaryTableBit100',
//             '2':'RotaryTableBit200_byMySelf',
//             '3':'RotaryTableBit200_byOther',
//         },
//         'Bullets' : {
//             '1' : 'Bullet1',
//             '2' : 'Bullet2',
//             '3' : 'Bullet3',
//             '4' : 'Bullet4',
//             '5' : 'Bullet5',
//             '6' : 'Bullet6',
//             '7' : 'Bullet7',
//         },
//         'Nets' : {
//             '1' : 'Net1',
//             '2' : 'Net2',
//             '3' : 'Net3',
//             '4' : 'Net4',
//             '5' : 'Net5',
//             '6' : 'Net6',
//             '7' : 'Net7',
//         },
//         'Fishs' : {
//         },
//         'Groups' : {
// 
//         },
//         'LockLine' : 'LockLine',
//         'LockCenter' : 'LockCenter'
//     },
// 
//     add : function(key , srcNode){
//         if(!this.inited){
//             nodes = {};
//             this.inited = true;
//         }
//         nodes[key] = nodes[key] || [];
//         if(srcNode instanceof cc.Node){
//             var node = cc.instantiate(srcNode);
//             node.active = false;
//             if(nodes[key].length == 0){
//                 instantiateNodes[key] = srcNode;
//             }
//             nodes[key].push(node);
//         }
//         
//     },
// 
//     pop : function(key){
//         if(!nodes[key]){
//             throw key;
//         }
//         // console.log(key,'取之前',nodes[key])
//         var node = nodes[key].shift();
//         // console.log(key,'取之后',nodes[key])
//         if(!node){
//             node = cc.instantiate(instantiateNodes[key]);
//         }
//         
//         node.__v_cache_key = key;
//         return node;
//     },
// 
//     recycle : function(node){
//         switch(node.__v_cache_key){
//             case this.Key.Bullet:
//                 node.anchorY = 0;
//                 break;
//         }
//         node.removeFromParent(false);
//         node.active = false;
//         var _key = node.__v_cache_key;
//         if(_key && nodes[_key]){
//             if(node.uuid==null){
//             }
//             
//             nodes[_key].push(node);
//         }
//     },
// 
//     clear : function(){
//         nodes = {};
//         this.Key.Fishs = {};
//         this.Key.Groups = {};
//         this.inited = false;
// 
//         
//     }
// };
// 
// module.exports = NodesCache;
System.register(["cc"], function (_export, _context) {
  "use strict";

  var _cclegacy, _crd;

  return {
    setters: [function (_cc) {
      _cclegacy = _cc.cclegacy;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "f7f47QTJ1FL96WIaluzPBJ7", "node-cache", undefined);

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=node-cache.js.map
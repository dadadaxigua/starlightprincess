System.register(["cc"], function (_export, _context) {
  "use strict";

  var _cclegacy, _crd;

  return {
    setters: [function (_cc) {
      _cclegacy = _cc.cclegacy;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "2894fxutC9C4atVDRbUFDYI", "clientProtos", undefined);

      _export("default", {
        "nested": {
          "chat": {
            "nested": {
              "chatHandler": {
                "nested": {
                  "chatOneByOne": {
                    "fields": {
                      "uid": {
                        "type": "string",
                        "id": 1
                      },
                      "type": {
                        "type": "int32",
                        "id": 2
                      },
                      "msg": {
                        "type": "string",
                        "id": 3
                      }
                    }
                  },
                  "chatDesk": {
                    "fields": {
                      "type": {
                        "type": "int32",
                        "id": 1
                      },
                      "id": {
                        "type": "int32",
                        "id": 2
                      }
                    }
                  },
                  "get1by1Msg": {
                    "fields": {
                      "type": {
                        "type": "int32",
                        "id": 1
                      },
                      "id": {
                        "type": "int32",
                        "id": 2
                      }
                    }
                  },
                  "sendChatMsg": {
                    "fields": {
                      "target_uid": {
                        "type": "string",
                        "id": 1
                      },
                      "msg": {
                        "type": "string",
                        "id": 2
                      },
                      "type": {
                        "type": "string",
                        "id": 3
                      }
                    }
                  },
                  "getAllUnreadMsg": {
                    "fields": {}
                  }
                }
              }
            }
          }
        }
      });

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=clientProtos.js.map
System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2", "__unresolved_3", "__unresolved_4"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, Node, v2, Vec3, director, v3, instantiate, tween, UITransform, Button, UIOpacity, sComponent, sConfigMgr, sObjPool, sMotionMgr, _dec, _dec2, _class, _class2, _descriptor, _descriptor2, _temp, _dec3, _dec4, _dec5, _dec6, _dec7, _dec8, _dec9, _dec10, _dec11, _dec12, _dec13, _dec14, _dec15, _class4, _class5, _descriptor3, _descriptor4, _descriptor5, _descriptor6, _descriptor7, _descriptor8, _descriptor9, _descriptor10, _descriptor11, _descriptor12, _descriptor13, _descriptor14, _class6, _temp2, _crd, ccclass, property, sBoxEntityAsset, sHorizontalBoxMgr;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfsBoxEntity(extras) {
    _reporterNs.report("sBoxEntity", "./sBoxEntity", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsComponent(extras) {
    _reporterNs.report("sComponent", "./sComponent", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsConfigMgr(extras) {
    _reporterNs.report("sConfigMgr", "./sConfigMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsObjPool(extras) {
    _reporterNs.report("sObjPool", "./sObjPool", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsMotionMgr(extras) {
    _reporterNs.report("sMotionMgr", "./sMotionMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsMotionTween(extras) {
    _reporterNs.report("sMotionTween", "./sMotionTween", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      Node = _cc.Node;
      v2 = _cc.v2;
      Vec3 = _cc.Vec3;
      director = _cc.director;
      v3 = _cc.v3;
      instantiate = _cc.instantiate;
      tween = _cc.tween;
      UITransform = _cc.UITransform;
      Button = _cc.Button;
      UIOpacity = _cc.UIOpacity;
    }, function (_unresolved_2) {
      sComponent = _unresolved_2.sComponent;
    }, function (_unresolved_3) {
      sConfigMgr = _unresolved_3.sConfigMgr;
    }, function (_unresolved_4) {
      sObjPool = _unresolved_4.sObjPool;
    }, function (_unresolved_5) {
      sMotionMgr = _unresolved_5.sMotionMgr;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "e8ab8IzBvVOb4PNrCAPsmz8", "sHorizontalBoxMgr", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("sBoxEntityAsset", sBoxEntityAsset = (_dec = ccclass('sHorizontalBoxEntityAsset'), _dec2 = property(Node), _dec(_class = (_class2 = (_temp = class sBoxEntityAsset {
        constructor() {
          _initializerDefineProperty(this, "targetBoxName", _descriptor, this);

          _initializerDefineProperty(this, "targetBoxNode", _descriptor2, this);
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "targetBoxName", [property], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return '';
        }
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "targetBoxNode", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class));

      _export("sHorizontalBoxMgr", sHorizontalBoxMgr = (_dec3 = ccclass('sHorizontalBoxMgr'), _dec4 = property({
        group: {
          name: 'BarderBoxConfig'
        }
      }), _dec5 = property({
        group: {
          name: 'BarderBoxConfig'
        }
      }), _dec6 = property({
        group: {
          name: 'BarderBoxConfig'
        }
      }), _dec7 = property({
        group: {
          name: 'BarderBoxConfig'
        },
        tooltip: '每一行的偏移量',
        type: [Vec3]
      }), _dec8 = property({
        group: {
          name: 'BarderBoxConfig'
        },
        tooltip: '结果表中的起始数据索引'
      }), _dec9 = property(sBoxEntityAsset), _dec10 = property(Node), _dec11 = property(Node), _dec12 = property(Node), _dec13 = property(Node), _dec14 = property({
        tooltip: '普通滚动的速度,越大越快',
        min: 8
      }), _dec15 = property({
        tooltip: '加速滚动的速度,越大越快',
        min: 8
      }), _dec3(_class4 = (_class5 = (_temp2 = _class6 = class sHorizontalBoxMgr extends (_crd && sComponent === void 0 ? (_reportPossibleCrUseOfsComponent({
        error: Error()
      }), sComponent) : sComponent) {
        constructor() {
          super(...arguments);

          _initializerDefineProperty(this, "boxSum", _descriptor3, this);

          _initializerDefineProperty(this, "boxViewSum", _descriptor4, this);

          _initializerDefineProperty(this, "boxSize", _descriptor5, this);

          _initializerDefineProperty(this, "skewVec3Arr", _descriptor6, this);

          _initializerDefineProperty(this, "configDataXDelta", _descriptor7, this);

          _defineProperty(this, "disappearAreaSize", void 0);

          _initializerDefineProperty(this, "boxEntitityAsset", _descriptor8, this);

          _initializerDefineProperty(this, "boxContainer", _descriptor9, this);

          _initializerDefineProperty(this, "floatBackLayer", _descriptor10, this);

          _initializerDefineProperty(this, "overEffectLayer", _descriptor11, this);

          _initializerDefineProperty(this, "overLayerEffectForCopy", _descriptor12, this);

          _initializerDefineProperty(this, "normalSpeed", _descriptor13, this);

          _initializerDefineProperty(this, "turboSpeed", _descriptor14, this);

          _defineProperty(this, "boxArray", []);

          _defineProperty(this, "boxTargetPosY", []);

          _defineProperty(this, "tweenRollRowArray", {});

          _defineProperty(this, "disappearBottomNum", 0);

          _defineProperty(this, "startScheduleAnima", null);

          _defineProperty(this, "rollState", 'idle');

          _defineProperty(this, "betRollType", 'normal');

          _defineProperty(this, "rollSpeedMode", 'normal');

          _defineProperty(this, "rollCount", 0);

          _defineProperty(this, "boxRollState", {});

          _defineProperty(this, "wholeFlowCallList", []);

          _defineProperty(this, "rollMotionType", 'stop');

          _defineProperty(this, "colBoxState", []);

          _defineProperty(this, "rollQuickMode", 'normal');

          _defineProperty(this, "rollMotions", new Map());
        }

        onLoad() {
          var _this = this;

          sHorizontalBoxMgr.instance = this;
          this.disappearAreaSize = v2(-((this.boxSum.x - this.boxViewSum.x) / 2 + 1) * this.boxSize.x, 0);
          this.disappearBottomNum = Math.floor(Math.abs(this.disappearAreaSize.x) / this.boxSize.x);

          for (var i = 0; i < this.boxSum.y; i++) {
            this.boxRollState[i] = 0;
          }

          director.on('configLoadSuc', () => {
            if (this.enabled) {
              for (var y = 0; y < this.boxSum.y; y++) {
                this.boxTargetPosY[y] = v3(0, y * this.boxSize.y, 0);
              }

              if (this.boxContainer) {
                var boxSize = this.boxSize;
                var startX = (this.boxSum.x - this.boxViewSum.x) / 2;

                for (var _y = 0; _y < this.boxSum.y; _y++) {
                  var skewDeltaX = this.getSkewPos(_y);

                  for (var x = 0; x < this.boxSum.x; x++) {
                    var pos = v3(x * boxSize.x - startX * boxSize.x + skewDeltaX, _y * boxSize.y, 0);
                    var boxEntity = this.CreateBoxEntity(pos);
                    boxEntity.ItemIndex = v2(_y + this.configDataXDelta, x);
                    boxEntity.ViewItemIndex = v2(_y, x - startX);
                    boxEntity.EntityInit('hor');
                    boxEntity.UpdateData('idle', 'idle');
                    this.setBoxArray(_y, x, boxEntity);
                  }

                  this.boxTargetPosY[_y].add3f(-startX * boxSize.x + skewDeltaX, 0, 0);
                }
              }

              (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
                error: Error()
              }), sConfigMgr) : sConfigMgr).instance.setBoxTrigger(this.boxSum.y + this.configDataXDelta);
            }
          }, this);
          director.on('soltRollBeagin', function (type) {
            if (type === void 0) {
              type = 'normal';
            }

            _this.rollBegain(type);
          }, this);
          director.on('rollStop', () => {
            this.rollState = 'idle';
          }, this);
          director.on('backToHall', () => {
            sHorizontalBoxMgr.instance = null;
          }, this);
          director.on('sBoxQuickClick', () => {
            if (this.rollMotionType == 'go' && this.rollSpeedMode == 'normal') {
              this.setColBoxState(-1, 'idle');
              this.setAllBoxArrayState('rest', 'settlement');

              for (var item of this.rollMotions.values()) {
                if (item) {
                  item.GoEnd();
                }
              }

              this.rollMotions.clear();
            }
          }, this);
          director.on('subGameSlotRollQuickStopAction', () => {
            this.rollQuickMode = 'quick';
          }, this); // director.on('subGameSlotRollQuickStop',()=>{
          //     if(this.rollMotionType == 'go' && this.rollSpeedMode == 'normal'){
          //         for(let item of this.rollMotions.values()){
          //             if(item){
          //                 item.GoEnd();
          //             }
          //         }
          //         this.rollMotions.clear();
          //     }
          // },this);
        }

        start() {
          this.scheduleOnce(() => {
            this.createBoxTipButton();
          }, 1);
        }
        /**
         * @zh
         * 创建格子实体
         * @param pos - 初始坐标位置postision
         */


        CreateBoxEntity(pos) {
          // console.log('CreateBoxEntity:'+globalThis.currentPlayingGameID);
          var boxEntity = new globalThis.boxEntityCtr[globalThis.currentPlayingGameID]();
          boxEntity.BoxPos = pos;

          for (var x = 0; x < this.boxEntitityAsset.length; x++) {
            var entityAsset = this.boxEntitityAsset[x];
            var obj = instantiate(entityAsset.targetBoxNode);
            obj.setParent(entityAsset.targetBoxNode.parent);
            obj.position = pos;
            obj.setSiblingIndex(0);
            boxEntity.SetAssetToObj(entityAsset.targetBoxName, obj);
          }

          return boxEntity;
        }

        rollStopAllRow() {
          this.removeIDSchedule('startScheduleAnima');
          var indexX = Math.floor(Math.abs(this.boxTargetPosY[0].x) / this.boxSize.x);
          var rollBackType = true;

          if (this.rollSpeedMode == 'turbo' || this.rollQuickMode == 'quick') {// rollBackType = false;
          }

          var motions = [];

          for (var i = 0; i < this.boxArray.length; i++) {
            var topIndex = indexX + this.boxViewSum.x + (this.boxSum.x - this.boxViewSum.x) / 2;
            var nextIndex = (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
              error: Error()
            }), sConfigMgr) : sConfigMgr).instance.GetNextBandIndex(topIndex);
            var nowPosX = this.boxTargetPosY[0].x;
            var deltaX = nowPosX % this.boxSize.x;
            var targetPos = nowPosX - (this.boxSum.x + nextIndex - topIndex) * this.boxSize.x - deltaX;
            var row = i;

            if (row >= 0 && row < this.boxArray.length) {
              (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
                error: Error()
              }), sConfigMgr) : sConfigMgr).instance.setBoxTriggerValue(row + this.configDataXDelta, true);
              (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
                error: Error()
              }), sConfigMgr) : sConfigMgr).instance.SetBoxDataArea(row + this.configDataXDelta, nextIndex);
              var motion = this.stopToPos(row, targetPos + this.getSkewPos(i), _row => {
                if (this.isAllBoxRollStop()) {
                  if (!rollBackType) {
                    this.boxSetTargetPosUpdate();
                  }

                  director.emit('horRollStopOneRoundCall');
                }
              }, rollBackType);

              if (this.rollSpeedMode == 'turbo') {
                motion.rollPixelXLimit = this.boxSize.x * this.boxViewSum.x - this.boxSize.x / 2;
              }

              motions.push(motion);
              this.rollMotionPush(row, motion);
            }
          } // if(this.rollQuickMode == 'quick'){
          //     for (let m = 0; m < motions.length; m++) {
          //         const _motion = motions[m];
          //         if(_motion){
          //             _motion.GoEnd();
          //         }
          //     }
          // }

        }

        stopToPos(row, posX, finishCallBack, rollBack) {
          if (finishCallBack === void 0) {
            finishCallBack = null;
          }

          if (rollBack === void 0) {
            rollBack = true;
          }

          var speed = null,
              speedIncrement = 0,
              rollSpeedMin = 0;
          var tweenRoll = this.tweenRollRowArray[row];

          if (tweenRoll) {
            var rollMotion = tweenRoll.tween;

            if (rollMotion) {
              speed = rollMotion.Speed;
              speedIncrement = rollMotion.speedIncrement;
              rollSpeedMin = rollMotion.rollSpeedMin;
              (_crd && sMotionMgr === void 0 ? (_reportPossibleCrUseOfsMotionMgr({
                error: Error()
              }), sMotionMgr) : sMotionMgr).RemoveTweenByID(tweenRoll.tween.id);
            }
          }

          var nowPosX = this.boxTargetPosY[row].x;
          var yDelta = nowPosX > posX ? -40 : 40;
          var motion = null;

          if (rollBack) {
            motion = this.rollBoxToPosArrays(row, posX + yDelta, speed, () => {
              this.rollMotionRemove(row);
              director.emit('horRollOneColumnStop', row);
              this.rollBoxToPosArrays(row, posX, rollBack ? speed / 10 : speed, () => {
                this.setColumnAllBoxArrayState(row, 'idle', 'settlement');
                this.setBoxRollState(row, 0);

                if (finishCallBack) {
                  finishCallBack(row);
                }
              });
            });
            motion.speedIncrement = speedIncrement;
            motion.rollSpeedMin = rollSpeedMin;
            motion.SetRateCall(() => {
              this.setColumnAllBoxArrayState(row, 'rest', 'rolling');
              this.setColBoxState(row, 'idle');
            }, 0.7);
          } else {
            motion = this.rollBoxToPosArrays(row, posX, speed, () => {
              this.setColBoxState(row, 'idle');
              this.rollMotionRemove(row);
              this.setBoxRollState(row, 0);
              director.emit('horRollOneColumnStop', row);

              if (finishCallBack) {
                finishCallBack(row);
              }
            });
            motion.SetRateCall(() => {
              this.setColumnAllBoxArrayState(row, 'rest', 'rolling');
              this.setColBoxState(row, 'idle');
            }, 0.7);
          }

          return motion;
        }

        rollBoxToPosArraysRightNow(index, posX) {
          var pos = this.boxTargetPosY[index];
          pos.x = posX;

          for (var y = 0; y < this.boxArray[index].length; y++) {
            var obj = this.boxArray[index][y];

            var _pos = obj.GetBoxPosition();

            obj.UpdateBoxPosition(v3(posX, _pos.y, _pos.z)); // obj.node.position = v3(obj.node.position.x,posY,obj.node.position.z);
          }
        }
        /**
        * @zh
        * 改变指定列的所有格子的状态
        * @param index - 指定列
        * @param rollState - 格子状态
        * @param tableState - 桌子状态
        */


        setColumnAllBoxArrayState(index, type, tableState) {
          for (var y = 0; y < this.boxArray[index].length; y++) {
            var element = this.boxArray[index][y];
            element.UpdateData(type, tableState);
          }
        }

        boxSetTargetPosUpdate() {
          for (var x = 0; x < this.boxArray.length; x++) {
            var theX = this.boxTargetPosY[x];
            var skewX = this.getSkewPos(x);
            var yadd = Math.floor(Math.abs(theX.x - skewX) / this.boxSize.x);
            var mor = theX.x + yadd * this.boxSize.x;

            for (var y = 0; y < this.boxArray[x].length; y++) {
              var obj = this.boxArray[x][y];

              if (obj) {
                obj.SetBoxActive(true);
                var indexColumn = yadd + y - this.disappearBottomNum + 1;
                obj.ItemIndex = v2(x + this.configDataXDelta, indexColumn);
                obj.ViewItemIndex = v2(x + this.configDataXDelta, y - (this.boxSum.x - this.boxViewSum.x) / 2);
                obj.UpdateBoxPosition(v3((y - this.disappearBottomNum + 1) * this.boxSize.x + mor, theX.y, 0));
                obj.SetBoxSiblingIndex(0);
                obj.ClearItem('rolling', 'rolling');
                obj.UpdateData('idle', 'settlement');
              }
            }
          }
        }

        getSkewPos(column) {
          var skew = this.skewVec3Arr[column];
          return skew ? skew.y : 0;
        }

        setBoxArray(x, y, item) {
          if (!this.boxArray[x]) {
            this.boxArray[x] = [];
          }

          this.boxArray[x][y] = item;
        }

        rollBegain(type) {
          var x = this.boxTargetPosY[0].x - this.getSkewPos(0);

          for (var i = 0; i < this.boxTargetPosY.length; i++) {
            var element = this.boxTargetPosY[i];
            element.x = x + this.getSkewPos(i);
          }

          this.betRollType = 'normal';
          this.rollSpeedMode = type; // this.betBtnClickAction();

          this.clearEffectAllBoxArray('rest', 'idle');
          this.setBoxArrayState('rest', 'idle');
          (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
            error: Error()
          }), sConfigMgr) : sConfigMgr).instance.setBoxsTrigger(false);
          this.rrrolll();
        }

        rollMotionPush(col, motion) {
          this.rollMotions.set(col, motion);
        }

        rollMotionRemove(col) {
          this.rollMotions.delete(col);
        }

        rrrolll() {
          this.rollMotions.clear();
          this.removeAllSchedule();

          for (var i = 0; i < this.boxSum.y; i++) {
            this.boxRollState[i] = 1;
          }

          if (this.rollCount > 0) {
            for (var _i = 0; _i < this.boxTargetPosY.length; _i++) {
              var boxPos = this.boxTargetPosY[_i];
              boxPos.x += this.boxSum.x * this.boxSize.x;
            }
          }

          for (var _i2 = 0; _i2 < this.boxArray.length; _i2++) {
            for (var y = 0; y < this.boxArray[_i2].length; y++) {
              var element = this.boxArray[_i2][y];
              element.IsDirty = true;
            }
          }

          this.rollQuickMode = 'normal';
          this.setColBoxState(-1, 'rolling');
          var iii = 0;

          this.startScheduleAnima = () => {
            if (iii == 0) {// this.setBoxArrayState('roll','rolling');
            } else if (iii == this.boxSum.y - 1) {}

            var index = iii++;
            var rollBoxArrayMotion = this.rollBoxArray(index, this.rollSpeedMode == 'turbo' ? 20 : 40, this.rollSpeedMode == 'turbo' ? 200 : 1000, () => {
              var rollBoxArrayForeverMotion = this.rollBoxArrayForever(index, -42040, this.rollSpeedMode == 'turbo' ? this.turboSpeed : this.normalSpeed);

              if (rollBoxArrayForeverMotion) {
                if (this.rollSpeedMode == 'turbo') {
                  rollBoxArrayForeverMotion.RollSpeed = 0;
                  rollBoxArrayForeverMotion.speedIncrement = 30;
                  rollBoxArrayForeverMotion.rollSpeedMax = 2;
                }
              }

              if (index == this.boxSum.y - 1) {
                this.rollMotionType = 'go';
                this.wholeFlowDelayCallTrigger();

                if (this.betRollType == 'rightNow') {}
              }
            });

            if (rollBoxArrayMotion) {
              rollBoxArrayMotion.speedIncrement = -0.2;
              rollBoxArrayMotion.rollSpeedMin = 0.2;
            }
          };

          if (this.rollSpeedMode == 'turbo') {
            this.rollMotionType = 'float';

            for (var _y2 = 0; _y2 < this.boxSum.y; _y2++) {
              this.startScheduleAnima();
            }
          } else {
            this.rollMotionType = 'float';
            this.pushIDSchedule(this.startScheduleAnima, 0, 'startScheduleAnima', this.boxViewSum.y, 0.06, 0, true);
          }

          this.rollCount++;
        }

        setColBoxState(index, state) {
          if (index === void 0) {
            index = -1;
          }

          if (this.colBoxState) {
            if (index == -1) {
              for (var i = 0; i < this.boxSum.x; i++) {
                this.colBoxState[i] = state;
              }
            } else {
              if (index >= 0 && index < this.colBoxState.length) {
                this.colBoxState[index] = state;
              }
            }
          }
        }

        rollBoxToPosArrays(index, posX, rollSpeed, call) {
          var pos = this.boxTargetPosY[index];
          var valueX = pos.x;
          var motion = (_crd && sMotionMgr === void 0 ? (_reportPossibleCrUseOfsMotionMgr({
            error: Error()
          }), sMotionMgr) : sMotionMgr).Tween('hRoll_' + index, v3(posX, pos.y, 0), pos, rollSpeed, target => {
            var deltaX = target.x - valueX;
            valueX = target.x;

            for (var y = 0; y < this.boxArray[index].length; y++) {
              var obj = this.boxArray[index][y];

              var _pos2 = obj.GetBoxPosition();

              obj.UpdateBoxPosition(v3(_pos2.x + deltaX, _pos2.y, _pos2.z));
            }

            this.boxUpdateByIndex(index);
          }, call);
          return motion;
        }

        rollBoxArray(index, posX, rollSpeed, call) {
          var pos = this.boxTargetPosY[index];
          var valueX = pos.x;
          var tweenObj = {};
          var targetPos = v3(0, 0, 0);
          this.tweenRollRowArray[index] = tweenObj;
          return (_crd && sMotionMgr === void 0 ? (_reportPossibleCrUseOfsMotionMgr({
            error: Error()
          }), sMotionMgr) : sMotionMgr).Tween('hRoll_' + index, v3(pos.x + posX, pos.y, 0), pos, rollSpeed, target => {
            var deltaY = target.x - valueX;
            valueX = target.x;

            for (var y = 0; y < this.boxArray[index].length; y++) {
              var obj = this.boxArray[index][y];

              var _pos3 = obj.GetBoxPosition();

              targetPos.x = _pos3.x + deltaY;
              targetPos.y = _pos3.y;
              targetPos.z = _pos3.z;
              obj.UpdateBoxPosition(v3(_pos3.x + deltaY, _pos3.y, _pos3.z));
            }

            this.boxUpdateByIndex(index);
          }, call);
        }

        rollBoxArrayForever(index, posX, rollSpeed) {
          var pos = this.boxTargetPosY[index];
          var valueX = pos.x;
          var tweenObj = {};
          var targetPos = v3(0, 0, 0);
          this.tweenRollRowArray[index] = tweenObj;
          var motion = (_crd && sMotionMgr === void 0 ? (_reportPossibleCrUseOfsMotionMgr({
            error: Error()
          }), sMotionMgr) : sMotionMgr).Tween('hRoll_' + index, v3(pos.x + posX, pos.y, 0), pos, rollSpeed, target => {
            var deltaY = target.x - valueX;
            valueX = target.x;

            for (var y = 0; y < this.boxArray[index].length; y++) {
              var obj = this.boxArray[index][y];

              var _pos4 = obj.GetBoxPosition();

              targetPos.x = _pos4.x + deltaY;
              targetPos.y = _pos4.y;
              targetPos.z = _pos4.z;
              obj.UpdateBoxPosition(targetPos);
            }

            this.boxUpdateByIndex(index);
          });
          motion.loop = -1;
          tweenObj['tween'] = motion;
          return motion;
        }
        /**
         * @zh
         * 消除所有格子的特效
         * @param rollState - 格子状态
         * @param tableState - 桌子状态
         */


        clearEffectAllBoxArray(rollState, tableState) {
          for (var x = 0; x < this.boxArray.length; x++) {
            for (var y = 0; y < this.boxArray[x].length; y++) {
              var element = this.boxArray[x][y];
              element.ClearEffect(rollState, tableState);
            }
          }
        }
        /**
         * @zh
         * 改变所有格子的状态
         * @param rollState - 格子状态
         * @param tableState - 桌子状态
         */


        setBoxArrayState(type, tableState) {
          for (var x = 0; x < this.boxArray.length; x++) {
            for (var y = 0; y < this.boxArray[x].length; y++) {
              var element = this.boxArray[x][y];
              element.UpdateData(type, tableState);
            }
          }
        }

        boxUpdate() {
          for (var x = 0; x < this.boxArray.length; x++) {
            this.boxUpdateByIndex(x);
          }
        }

        boxUpdateByIndex(index) {
          var x = index;
          var viewArray = [];
          var unViewArray = [];

          for (var y = 0; y < this.boxArray[x].length; y++) {
            var element = this.boxArray[x][y];

            if (element.GetBoxPosition().x < this.disappearAreaSize.x + this.getSkewPos(x)) {
              unViewArray.push(element);
            } else {
              viewArray.push(element);
            }
          }

          for (var i = 0; i < unViewArray.length; i++) {
            var _element = unViewArray[i];
            (_crd && sObjPool === void 0 ? (_reportPossibleCrUseOfsObjPool({
              error: Error()
            }), sObjPool) : sObjPool).Enqueue('horBoxItem', _element);

            _element.ClearItem('rolling', 'rolling');
          }

          var viewYSum = viewArray.length;

          if (viewYSum < this.boxSum.x) {
            for (var _i3 = 0; _i3 < this.boxSum.x - viewYSum; _i3++) {
              var obj = (_crd && sObjPool === void 0 ? (_reportPossibleCrUseOfsObjPool({
                error: Error()
              }), sObjPool) : sObjPool).Dequeue('horBoxItem');

              if (obj) {
                obj.SetBoxActive(true);
                viewArray.push(obj);
              }
            }
          }

          var theX = this.boxTargetPosY[x];
          var skewX = this.getSkewPos(x);
          var yadd = Math.floor(Math.abs(theX.x - skewX) / this.boxSize.x);
          var mor = theX.x + yadd * this.boxSize.x;

          for (var _i4 = 0; _i4 < viewArray.length; _i4++) {
            var box = viewArray[_i4];
            var indexColumn = yadd + _i4 - this.disappearBottomNum + 1;

            if (!box.IsDirty && indexColumn != box.ItemIndex.x) {
              box.ClearItem('rolling', 'rolling');
              box.ItemIndex = v2(x + this.configDataXDelta, indexColumn < 0 ? 0 : indexColumn);
              box.UpdateBoxPosition(v3((_i4 - this.disappearBottomNum + 1) * this.boxSize.x + mor, theX.y, 0));
              box.SetBoxSiblingIndex(0);

              if (this.colBoxState[x] == 'idle') {
                box.UpdateData('rest', 'rolling');
              } else {
                box.UpdateData('rolling', 'rolling');
              }
            }

            if (box.ViewItemIndex) {
              box.ViewItemIndex.x = x + this.configDataXDelta;
              box.ViewItemIndex.y = _i4 - (this.boxSum.x - this.boxViewSum.x) / 2;
            } // box.ViewItemIndex = v2(x + this.configDataXDelta,viewYSum + i - (this.boxSum.x - this.boxViewSum.x) / 2);

          }

          this.boxArray[x] = viewArray;
        }
        /**
        * @zh
        * 在最上方补充新的格子
        */


        ReplenishBoxEntity() {
          for (var yy = 0; yy < this.boxArray.length; yy++) {
            var colBoxes = this.boxArray[yy];
            var bottomItemIndex = this.boxArray[yy][0].ItemIndex;
            var bottomViewItemIndex = this.boxArray[yy][0].ViewItemIndex;
            var disAppearBoxNum = 0;

            for (var xx = 0; xx < colBoxes.length; xx++) {
              var box = colBoxes[xx];

              if (box.IsDisappearBox) {
                disAppearBoxNum++;
              }
            }

            if (disAppearBoxNum > 0) {
              var topItemIndex = this.boxArray[yy][this.boxArray[yy].length - 1].ItemIndex;
              var waitRemoveBoxes = [];

              for (var _xx = 0; _xx < colBoxes.length; _xx++) {
                var _box = colBoxes[_xx];

                if (_box.IsDisappearBox) {
                  waitRemoveBoxes.push(_box);
                  (_crd && sObjPool === void 0 ? (_reportPossibleCrUseOfsObjPool({
                    error: Error()
                  }), sObjPool) : sObjPool).Enqueue('boxItem', _box);

                  _box.ClearItem('rolling', 'rolling');
                }
              }

              for (var i = 0; i < waitRemoveBoxes.length; i++) {
                var element = waitRemoveBoxes[i];
                this.RemoveEntityBoxByObj(yy, element);
              }

              var startX = (this.boxSum.x - this.boxViewSum.x) / 2;
              var nowLastBoxNum = colBoxes.length;

              for (var _i5 = 0; _i5 < disAppearBoxNum; _i5++) {
                var obj = (_crd && sObjPool === void 0 ? (_reportPossibleCrUseOfsObjPool({
                  error: Error()
                }), sObjPool) : sObjPool).Dequeue('boxItem');

                if (obj) {
                  // obj.IsDropBox = true;
                  obj.SetBoxActive(true);
                  obj.SetBoxSiblingIndex(0);
                  obj.UpdateBoxPosition(v3((this.boxSum.x - startX + _i5) * this.boxSize.x + this.getSkewPos(yy), this.boxSize.y * yy, 0));
                  obj.ItemIndex = v2(yy + this.configDataXDelta, bottomItemIndex.y + nowLastBoxNum + _i5);
                  obj.ViewItemIndex = v2(yy + this.configDataXDelta, bottomViewItemIndex.y + nowLastBoxNum + _i5);
                  obj.UpdateData('rest', 'settlement');
                  colBoxes.push(obj);
                }
              }

              for (var x = 0; x < colBoxes.length; x++) {
                var _box2 = colBoxes[x];
                _box2.ItemIndex = v2(yy + this.configDataXDelta, bottomItemIndex.y + x);
                _box2.ViewItemIndex = v2(yy + this.configDataXDelta, bottomViewItemIndex.y + x);
              }
            }
          } // console.log(this.boxArray);

        }
        /**
         * @zh
         * 让移除格子后的队列掉落补充的格子
         */


        DropAllEntityBoxAfterRemove(strikeCallBack) {
          if (strikeCallBack === void 0) {
            strikeCallBack = null;
          }

          var delayTime = 0.05;
          var startX = (this.boxSum.x - this.boxViewSum.x) / 2;
          var dropList = [];
          var isDrop = false;

          for (var yy = 0; yy < this.boxArray.length; yy++) {
            dropList[yy] = [];
            var colBoxes = this.boxArray[yy];

            for (var xx = 0; xx < colBoxes.length; xx++) {
              var box = colBoxes[xx];
              var targetPos = v3((xx - startX) * this.boxSize.x + this.getSkewPos(yy), this.boxSize.x * yy, 0);
              var boxNowPos = box.GetBoxPosition();

              if (Math.abs(boxNowPos.x - targetPos.x) > 0.1 || Math.abs(boxNowPos.y - targetPos.y) > 0.1) {
                // console.log('drop the beat:'+x+','+ y);
                dropList[yy].push({
                  _box: box,
                  _targetPos: targetPos
                }); // box.DropEntityBoxToPosAnima(targetPos,0.4,yIndex++ * 0.1 + (xIndex) * 0.1);

                isDrop = true;
              }
            }
          }

          if (isDrop) {
            var yIndex = 0;

            var _loop = function _loop(_yy) {
              var yList = dropList[_yy];

              if (yList && yList.length > 0) {
                var _loop2 = function _loop2(_xx2) {
                  var boxObj = yList[_xx2];

                  if (boxObj) {
                    var tBox = boxObj._box;
                    var tTargetPos = boxObj._targetPos;
                    tBox.DropHorEntityBoxToPosAnima(tTargetPos, 0.24, _xx2 * delayTime + yIndex * delayTime, () => {
                      if (_yy == 0 && _xx2 >= dropList[0].length - 1) {
                        // console.log(x+','+y);
                        director.emit('dropAllHorizontalBoxFinish');
                      }
                    }, () => {
                      if (strikeCallBack) {
                        strikeCallBack(tBox);
                      }
                    });
                  }
                };

                for (var _xx2 = 0; _xx2 < yList.length; _xx2++) {
                  _loop2(_xx2);
                }

                if (yList && yList.length > 0) {
                  yIndex++;
                }
              }
            };

            for (var _yy = this.boxArray.length - 1; _yy >= 0; _yy--) {
              _loop(_yy);
            }
          }
        }
        /**
         * @zh
         * 让移除格子后的队列掉落补充的格子
         * strikeCallBack : 格子第一次触底的回调
         * delayTime : 落下来的格子的间隔时间
         * colDelayTime : 每列开始落下的间隔时间
         */


        DropAllEntityBoxAfterRemoveByConfig(_config) {
          if (_config === void 0) {
            _config = null;
          }

          var strikeCallBack = null;
          var delayTime = 0.07;
          var colDelayTime = 0.07;

          if (_config) {
            strikeCallBack = _config.strikeCallBack ? _config.strikeCallBack : strikeCallBack;
            delayTime = _config.delayTime >= 0 ? _config.delayTime : delayTime;
            colDelayTime = _config.colDelayTime >= 0 ? _config.colDelayTime : colDelayTime;
          }

          var startX = (this.boxSum.x - this.boxViewSum.x) / 2;
          var dropList = [];
          var isDrop = false;

          for (var yy = 0; yy < this.boxArray.length; yy++) {
            dropList[yy] = [];
            var colBoxes = this.boxArray[yy];

            for (var xx = 0; xx < colBoxes.length; xx++) {
              var box = colBoxes[xx];
              var targetPos = v3((xx - startX) * this.boxSize.x + this.getSkewPos(yy), this.boxSize.x * yy, 0);
              var boxNowPos = box.GetBoxPosition();

              if (Math.abs(boxNowPos.x - targetPos.x) > 0.1 || Math.abs(boxNowPos.y - targetPos.y) > 0.1) {
                // console.log('drop the beat:'+x+','+ y);
                dropList[yy].push({
                  _box: box,
                  _targetPos: targetPos
                }); // box.DropEntityBoxToPosAnima(targetPos,0.4,yIndex++ * 0.1 + (xIndex) * 0.1);

                isDrop = true;
              }
            }
          }

          if (isDrop) {
            var yIndex = 0;

            var _loop3 = function _loop3(_yy2) {
              var yList = dropList[_yy2];

              if (yList && yList.length > 0) {
                var _loop4 = function _loop4(_xx3) {
                  var boxObj = yList[_xx3];

                  if (boxObj) {
                    var tBox = boxObj._box;
                    var tTargetPos = boxObj._targetPos;
                    tBox.DropHorEntityBoxToPosAnima(tTargetPos, 0.24, _xx3 * delayTime + yIndex * colDelayTime, () => {
                      if (_yy2 == 0 && _xx3 >= dropList[0].length - 1) {
                        // console.log(x+','+y);
                        director.emit('dropAllHorizontalBoxFinish');
                      }
                    }, () => {
                      if (strikeCallBack) {
                        strikeCallBack(tBox);
                      }
                    });
                  }
                };

                for (var _xx3 = 0; _xx3 < yList.length; _xx3++) {
                  _loop4(_xx3);
                }

                if (yList && yList.length > 0) {
                  yIndex++;
                }
              }
            };

            for (var _yy2 = this.boxArray.length - 1; _yy2 >= 0; _yy2--) {
              _loop3(_yy2);
            }
          }
        }
        /**
         * @zh
         * 移除队列里的格子
         * @param col - 指定列
         * @param box - 格子引用
         */


        RemoveEntityBoxByObj(col, box) {
          for (var i = 0; i < this.boxArray[col].length; i++) {
            var element = this.boxArray[col][i];

            if (element == box) {
              this.boxArray[col].splice(i, 1);
              break;
            }
          }
        }
        /**
         * @zh
         * 触发指定格子的特定行为
         * @param x - 指定行
         * @param x - 指定列
         * @param _name - 动作名称
         */


        DoBoxEntityActonByXY(x, y, _name) {
          var startX = Math.floor((this.boxSum.x - this.boxViewSum.x) / 2);
          var box = this.boxArray[y][x + startX];

          if (box) {
            box.DoEnitityAction(_name);
          }
        }
        /**
         * @zh
         * 触发所有格子特定行为
         * @param index - 指定列
         * @param _name - 动作名称
         */


        DoAllBoxEntityActon(_name) {
          for (var x = 0; x < this.boxArray.length; x++) {
            for (var y = 0; y < this.boxArray[x].length; y++) {
              var element = this.boxArray[x][y];
              element.DoEnitityAction(_name);
            }
          }
        }
        /**
         * @zh
         * 根据视窗坐标改变格子的状态
         * @param x - 指定列
         * @param y - 指定行
         * @param rollState - 格子状态
         * @param tableState - 桌子状态
         */


        updateBoxDataByXY(x, y, boxType, tableState) {
          var startX = Math.floor((this.boxSum.x - this.boxViewSum.x) / 2);

          if (y >= 0 && y < this.boxArray.length) {
            var xIndex = x + startX;

            if (xIndex >= 0 && xIndex < this.boxArray[y].length) {
              var box = this.boxArray[y][xIndex];

              if (box) {
                box.UpdateData(boxType, tableState);
              }
            }
          }
        }
        /**
         * @zh
         * 获取视窗索引的格子
         * @param x - 指定列
         * @param y - 指定行
         * @return - The boxEntity
         */


        getBoxEntityByXY(x, y) {
          if (y < this.boxArray.length) {
            var index = (this.boxSum.x - this.boxViewSum.x) / 2 + x;

            if (index < this.boxArray[y].length) {
              return this.boxArray[y][index];
            }
          }

          return null;
        }
        /**
         * @zh
         * 改变所有满足条件的格子的状态
         * @param symbol - 条件回调
         * @param rollState - 格子状态
         * @param tableState - 桌子状态
         * 
         * @example
         * sBoxMgr.instance.setAllViewTargetBoxArrayState(symbol => {return symbol == 1;},'win','settlement');
         * 让所有symbol为1的格子变成rollState为win且tableState为settlement的状态
         */


        setAllViewTargetBoxArrayState(symbol, rollState, tableState) {
          for (var yy = 0; yy < this.boxArray.length; yy++) {
            for (var xx = (this.boxSum.x - this.boxViewSum.x) / 2; xx < this.boxArray[yy].length - (this.boxSum.x - this.boxViewSum.x) / 2; xx++) {
              var element = this.boxArray[yy][xx];

              if (symbol && symbol(element)) {
                element.UpdateData(rollState, tableState);
              }
            }
          }
        }

        setAllBoxArrayState(rollState, tableState) {
          for (var yy = 0; yy < this.boxArray.length; yy++) {
            for (var xx = 0; xx < this.boxArray[yy].length; xx++) {
              var element = this.boxArray[yy][xx];
              element.UpdateData(rollState, tableState);
            }
          }
        }

        setRowTargetBoxArrayState(index, symbol, rollState, tableState) {
          if (index >= 0 && index < this.boxArray.length) {
            for (var xx = (this.boxSum.x - this.boxViewSum.x) / 2; xx < this.boxArray[index].length - (this.boxSum.x - this.boxViewSum.x) / 2; xx++) {
              var element = this.boxArray[index][xx];

              if (symbol && symbol(element)) {
                element.UpdateData(rollState, tableState);
              }
            }
          }
        }
        /**
         * @zh
         * 触发所有满足条件的所有可视格子特殊方法
         * @param index - 指定列
         * @param symbol - 条件回调
         * @param actionName - 自定义方法名
         */


        DoAllTargetViewBoxArrayAction(symbol, actionName) {
          if (this.boxArray) {
            var boxViewX = Math.trunc((this.boxSum.x - this.boxViewSum.x) / 2);

            for (var y = 0; y < this.boxSum.y; y++) {
              if (y < this.boxArray.length) {
                for (var x = boxViewX; x < this.boxViewSum.x + boxViewX; x++) {
                  if (x < this.boxArray[y].length) {
                    var element = this.boxArray[y][x];

                    if (element && symbol && symbol(element)) {
                      element.DoEnitityAction(actionName);
                    }
                  }
                }
              }
            }
          }
        }
        /**
         * @zh
         * 让第一层黑幕以动画效果的方式变亮或者变暗
         * @param index - 这一列不变化
         * @param beComeBlack - 是否变黑
         * 
         * @example
         * sBoxMgr.instance.blackCurtainAnima(0,true);
         * 让除了第1列的黑幕变黑
         */


        blackCurtainAnima(index, endOpa, startOpa, isAnima) {
          if (index === void 0) {
            index = -1;
          }

          if (startOpa === void 0) {
            startOpa = -1;
          }

          if (isAnima === void 0) {
            isAnima = true;
          }

          var black = this.node.getChildByName('blackMask');

          if (black && black.children.length) {
            black.active = true;

            for (var i = 0; i < black.children.length; i++) {
              if (index != i) {
                var element = black.children[i];
                var opaScript = element.getComponent(UIOpacity);

                if (startOpa >= 0) {
                  opaScript.opacity = startOpa;
                }

                if (isAnima) {
                  tween(opaScript).to(0.4, {
                    opacity: endOpa
                  }).start();
                } else {
                  opaScript.opacity = endOpa;
                }
              }
            }
          }
        }

        createBoxTipButton() {
          var _this2 = this;

          var boxTrans = this.node.getComponent(UITransform);

          if (boxTrans && this.boxViewSum && this.boxViewSum.x > 0 && this.boxViewSum.y > 0 && this.boxSize) {
            var boxTipBtnsNode = new Node('boxTipBtns');
            boxTipBtnsNode.layer = boxTrans.node.layer;
            boxTipBtnsNode.parent = this.node;
            var boxTipBtnsTrans = boxTipBtnsNode.addComponent(UITransform);
            boxTipBtnsTrans.anchorX = 0;
            boxTipBtnsTrans.anchorY = 0;
            boxTipBtnsNode.position = Vec3.ZERO;
            boxTipBtnsTrans.setContentSize(boxTrans.contentSize.width, boxTrans.contentSize.height);

            for (var x = 0; x < this.boxViewSum.x; x++) {
              var _loop5 = function _loop5(y) {
                var indexX = x;
                var indexY = y;
                var btnNode = new Node(indexX + '_' + indexY);
                btnNode.layer = boxTipBtnsNode.layer;
                btnNode.parent = boxTipBtnsNode;
                var btnTrans = btnNode.addComponent(UITransform);
                btnTrans.anchorX = 0;
                btnTrans.anchorY = 0;
                btnTrans.setContentSize(_this2.boxSize.x, _this2.boxSize.y);
                btnNode.position = v3(x * _this2.boxSize.x, y * _this2.boxSize.y + _this2.getSkewPos(x), 0);
                var btn = btnTrans.addComponent(Button);
                btnNode.on('click', () => {
                  // console.log('createBoxTipButton:'+indexX +','+ indexY);
                  if (globalThis.GameBtnEntity) {
                    if (globalThis.GameBtnEntity.betBtnState == 'normal' && globalThis.GameBtnEntity.gameMode == 'normal') {
                      director.emit('sBoxIntroTipClick', 'horizontal', _this2.getBoxEntityByXY(indexX, indexY), indexX + 1, indexY);
                    }
                  }
                }, _this2);
              };

              for (var y = 0; y < this.boxViewSum.y; y++) {
                _loop5(y);
              }
            }
          }
        }

        wholeFlowDelayCall(waitTime, call) {
          if (call) {
            if (this.rollMotionType == 'go') {
              if (this.rollQuickMode == 'quick') {
                call();
              } else {
                this.removeIDSchedule('wholeDelayCall');
                this.pushIDSchedule(() => {
                  if (call) {
                    call();
                  }

                  this.removeIDSchedule('wholeDelayCall');
                }, waitTime, 'wholeDelayCall');
              }
            } else {
              this.wholeFlowCallList.push(() => {
                this.removeIDSchedule('wholeDelayCall');
                this.pushIDSchedule(() => {
                  if (call) {
                    call();
                  }

                  this.removeIDSchedule('wholeDelayCall');
                }, waitTime, 'wholeDelayCall');
              });
            }
          }
        }

        wholeFlowDelayCallTrigger() {
          if (this.wholeFlowCallList && this.wholeFlowCallList.length > 0) {
            for (var i = 0; i < this.wholeFlowCallList.length; i++) {
              var call = this.wholeFlowCallList[i];

              if (call) {
                call();
              }
            }

            this.wholeFlowCallList = [];
          }
        }

        setBoxRollState(index, state) {
          this.boxRollState[index] = state;
        }

        isAllBoxRollStop() {
          if (this.boxRollState) {
            for (var i = 0; i < this.boxSum.x; i++) {
              if (this.boxRollState[i] == 1) {
                return false;
              }
            }
          }

          return true;
        }

      }, _defineProperty(_class6, "instance", void 0), _temp2), (_descriptor3 = _applyDecoratedDescriptor(_class5.prototype, "boxSum", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return v2(0, 0);
        }
      }), _descriptor4 = _applyDecoratedDescriptor(_class5.prototype, "boxViewSum", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return v2(0, 0);
        }
      }), _descriptor5 = _applyDecoratedDescriptor(_class5.prototype, "boxSize", [_dec6], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return v2(0, 0);
        }
      }), _descriptor6 = _applyDecoratedDescriptor(_class5.prototype, "skewVec3Arr", [_dec7], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return [];
        }
      }), _descriptor7 = _applyDecoratedDescriptor(_class5.prototype, "configDataXDelta", [_dec8], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return 0;
        }
      }), _descriptor8 = _applyDecoratedDescriptor(_class5.prototype, "boxEntitityAsset", [_dec9], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return [];
        }
      }), _descriptor9 = _applyDecoratedDescriptor(_class5.prototype, "boxContainer", [_dec10], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor10 = _applyDecoratedDescriptor(_class5.prototype, "floatBackLayer", [_dec11], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor11 = _applyDecoratedDescriptor(_class5.prototype, "overEffectLayer", [_dec12], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor12 = _applyDecoratedDescriptor(_class5.prototype, "overLayerEffectForCopy", [_dec13], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor13 = _applyDecoratedDescriptor(_class5.prototype, "normalSpeed", [_dec14], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return 3000;
        }
      }), _descriptor14 = _applyDecoratedDescriptor(_class5.prototype, "turboSpeed", [_dec15], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return 2000;
        }
      })), _class5)) || _class4));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=sHorizontalBoxMgr.js.map
System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, director, Label, sComponent, sAudioMgr, _dec, _class, _temp, _crd, ccclass, property, sFreeWinBuyView;

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _reportPossibleCrUseOfsComponent(extras) {
    _reporterNs.report("sComponent", "./sComponent", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsAudioMgr(extras) {
    _reporterNs.report("sAudioMgr", "./sAudioMgr", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      director = _cc.director;
      Label = _cc.Label;
    }, function (_unresolved_2) {
      sComponent = _unresolved_2.sComponent;
    }, function (_unresolved_3) {
      sAudioMgr = _unresolved_3.default;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "a7bd3okvmRI16bZ2cf1mMX5", "sFreeWinBuyView", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("sFreeWinBuyView", sFreeWinBuyView = (_dec = ccclass('sFreeWinBuyView'), _dec(_class = (_temp = class sFreeWinBuyView extends (_crd && sComponent === void 0 ? (_reportPossibleCrUseOfsComponent({
        error: Error()
      }), sComponent) : sComponent) {
        constructor() {
          super(...arguments);

          _defineProperty(this, "startBtn", void 0);

          _defineProperty(this, "cancelBtn", void 0);

          _defineProperty(this, "coinLabel", void 0);

          _defineProperty(this, "mainNode", void 0);

          _defineProperty(this, "mBetType", -1);

          _defineProperty(this, "mCoin", 0);

          _defineProperty(this, "canBuy", false);
        }

        start() {
          this.mainNode = this.node.getChildByName('main');
          this.startBtn = this.node.getChildByPath('main/start');
          this.cancelBtn = this.node.getChildByPath('main/cancel');

          var _label = this.node.getChildByPath('main/coin');

          if (_label) {
            this.coinLabel = _label.getComponent(Label);
          }

          if (this.startBtn) {
            this.startBtn.on('click', () => {
              (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                error: Error()
              }), sAudioMgr) : sAudioMgr).PlayShotAudio('featureBuy_audio_sure');

              if (this.canBuy) {
                this.mainNode.active = false;

                if (this.mBetType != -1) {
                  (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                    error: Error()
                  }), sAudioMgr) : sAudioMgr).PlayShotAudio('featureBuy_audio');
                  director.emit('subGameBetActionBtnClick', this.mBetType, this.mCoin);
                }
              } else {
                globalThis.goldNotenough && globalThis.goldNotenough();
              }
            }, this);
          }

          if (this.cancelBtn) {
            this.cancelBtn.on('click', () => {
              (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                error: Error()
              }), sAudioMgr) : sAudioMgr).PlayShotAudio('featureBuy_audio_cancel');
              this.mBetType = -1;
              this.mainNode.active = false;
              director.emit('subGameBetActionBtnCancel');
            }, this);
          }
        }

        viewInit(coin, betType, _canBuy, _tipStr) {
          this.coinLabel.string = this.AddCommas(coin);
          this.mCoin = coin;
          this.mBetType = betType;
          this.canBuy = _canBuy;
          this.mainNode.active = true;
        }

      }, _temp)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=sFreeWinBuyView.js.map
System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2", "__unresolved_3"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, Animation, director, Label, sComponent, sUtil, sAudioMgr, _dec, _dec2, _dec3, _class, _class2, _descriptor, _descriptor2, _temp, _crd, ccclass, property, ristBigWinCtr;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfsComponent(extras) {
    _reporterNs.report("sComponent", "../../../../../game/core/sComponent", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsUtil(extras) {
    _reporterNs.report("sUtil", "../../../../../game/core/sUtil", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsAudioMgr(extras) {
    _reporterNs.report("sAudioMgr", "../../../../../game/core/sAudioMgr", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      Animation = _cc.Animation;
      director = _cc.director;
      Label = _cc.Label;
    }, function (_unresolved_2) {
      sComponent = _unresolved_2.sComponent;
    }, function (_unresolved_3) {
      sUtil = _unresolved_3.sUtil;
    }, function (_unresolved_4) {
      sAudioMgr = _unresolved_4.default;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "ac648liDO1OL5OuvtHNtsck", "ristBigWinCtr", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("ristBigWinCtr", ristBigWinCtr = (_dec = ccclass('ristBigWinCtr'), _dec2 = property(Animation), _dec3 = property(Label), _dec(_class = (_class2 = (_temp = class ristBigWinCtr extends (_crd && sComponent === void 0 ? (_reportPossibleCrUseOfsComponent({
        error: Error()
      }), sComponent) : sComponent) {
        constructor() {
          super(...arguments);

          _initializerDefineProperty(this, "mAnimation", _descriptor, this);

          _initializerDefineProperty(this, "coinLabel", _descriptor2, this);

          _defineProperty(this, "targetModel", '');
        }

        start() {
          this.node.on('bigWinAnima', this.bigWinAnima, this);
        }

        bigWinAnima(coin, rate) {
          // console.log('coin:'+coin);
          // console.log('rate:'+rate);
          var multiple = globalThis.GameBtnEntity.lineAmount;

          if (coin > 0 && rate > 0 && multiple > 0) {
            var mainNode = this.node.getChildByName('main');

            if (mainNode) {
              mainNode.active = true;
              this.mAnimation.off(Animation.EventType.FINISHED);
              this.cleanTweenList();
              this.cleanTweenDic();
              this.removeScheduleDic();
              this.SetUIFront();
              var readRate = rate / multiple;
              var bigWin = 10;
              var megaWin = 20;
              var superWin = 50;
              this.targetModel = 'bigwin';
              var targetCoin = coin;
              var line = coin / rate;
              var megaCoin = 0;
              var superCoin = 0;

              if (readRate >= bigWin && readRate < megaWin) {} else if (readRate >= megaWin && readRate < superWin) {
                this.targetModel = 'megawin';
                targetCoin = line * megaWin * multiple;
                megaCoin = coin;
              } else if (readRate >= superWin) {
                this.targetModel = 'superwin';
                targetCoin = line * megaWin * multiple;
                megaCoin = line * superWin * multiple;
                superCoin = coin;
              }

              this.mAnimation.play('Bigwin');

              if (this.targetModel == 'bigwin') {
                (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                  error: Error()
                }), sAudioMgr) : sAudioMgr).PlayShotAudio('ristBtnBigWin_1');
                (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                  error: Error()
                }), sUtil) : sUtil).TweenLabel(0, targetCoin, this.coinLabel, 2.5, 0, 'quadOut', 0, () => {
                  this.coinLabelAnima();
                  this.closeAnima();
                });
              } else if (this.targetModel == 'megawin') {
                (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                  error: Error()
                }), sAudioMgr) : sAudioMgr).PlayShotAudio('ristBtnBigWin_1');
                (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                  error: Error()
                }), sUtil) : sUtil).TweenLabel(0, targetCoin, this.coinLabel, 2.5, 0, 'quadOut', 0, () => {
                  this.coinLabelAnima();
                });
                this.pushIDSchedule(() => {
                  (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                    error: Error()
                  }), sAudioMgr) : sAudioMgr).PlayShotAudio('ristBtnBigWin_2');
                  this.mAnimation.play('Hugewin');
                  (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                    error: Error()
                  }), sUtil) : sUtil).TweenLabel(targetCoin, megaCoin, this.coinLabel, 3, 0, 'quadOut', 0, () => {
                    this.coinLabelAnima();
                    this.closeAnima();
                  });
                }, 4, 'megawin');
              } else if (this.targetModel == 'superwin') {
                (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                  error: Error()
                }), sAudioMgr) : sAudioMgr).PlayShotAudio('ristBtnBigWin_1');
                (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                  error: Error()
                }), sUtil) : sUtil).TweenLabel(0, targetCoin, this.coinLabel, 2.5, 0, 'quadOut', 0, () => {
                  this.coinLabelAnima();
                });
                this.pushIDSchedule(() => {
                  (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                    error: Error()
                  }), sAudioMgr) : sAudioMgr).PlayShotAudio('ristBtnBigWin_2');
                  this.mAnimation.play('Hugewin');
                  (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                    error: Error()
                  }), sUtil) : sUtil).TweenLabel(targetCoin, megaCoin, this.coinLabel, 3, 0, 'quadOut', 0, () => {
                    this.coinLabelAnima();
                  });
                  this.pushIDSchedule(() => {
                    (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                      error: Error()
                    }), sAudioMgr) : sAudioMgr).PlayShotAudio('ristBtnBigWin_3');
                    this.mAnimation.stop();
                    this.mAnimation.play('Superwin');
                    this.scheduleOnce(() => {
                      console.log(this.node);
                    }, 1);
                    (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                      error: Error()
                    }), sUtil) : sUtil).TweenLabel(megaCoin, superCoin, this.coinLabel, 3, 0, 'quadOut', 0, () => {
                      this.coinLabelAnima();
                      this.closeAnima();
                    });
                  }, 5, 'superwin');
                }, 5, 'megawin');
              }
            }
          }
        }

        coinLabelAnima() {
          (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
            error: Error()
          }), sAudioMgr) : sAudioMgr).PlayShotAudio('ristBtnBigWin_ding');
          this.mAnimation.play('End'); // if(this.coinLabel){
          //     this.pushOneTween(tween(this.coinLabel.node).to(0.18,{scale : v3(1.1,1.1,1.1)},{easing : 'sineOut'}).to(0.14,{scale : Vec3.ONE},{easing : 'sineIn'}).start());
          // }
        }

        closeAnima() {
          this.pushIDSchedule(() => {
            // this.node.children[0].active = false;
            this.mAnimation.on(Animation.EventType.FINISHED, () => {
              // this.node.active = false;
              this.node.children[0].active = false;
              director.emit('ristBigWinFinish');
            }, this);
            this.mAnimation.play('Close');
          }, 1.5, 'close');
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "mAnimation", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "coinLabel", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=ristBigWinCtr.js.map
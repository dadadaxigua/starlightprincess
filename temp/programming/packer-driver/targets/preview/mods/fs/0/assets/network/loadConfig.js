System.register(["cc"], function (_export, _context) {
  "use strict";

  var _cclegacy, sys, _crd;

  return {
    setters: [function (_cc) {
      _cclegacy = _cc.cclegacy;
      sys = _cc.sys;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "aa376j8AA9COpi/q1kgcC/2", "loadConfig", undefined);

      (() => {
        //请更改
        var AllPorts = {
          CDN: [],
          // LocalGame: null|| ['http://192.168.0.11:17001/'],
          // LocalApi: null|| ['http://192.168.0.11:17006/'],
          CdnUrl: null || ['http://192.168.0.12:18400/'],
          // CdnUrl : null || ['https://b9-lg.oss-cn-hongkong.aliyuncs.com/	'],
          // LocalGame: null|| ['https://b.aabbcc22.xyz:7001/'],
          // LocalApi: null|| ['https://b.aabbcc22.xyz:7006/'],
          // CdnUrl : null || ['https://oss.aabbcc22.xyz/'],
          AliOssAds: '192.168.0.11:13001'
        };
        globalThis.runMode = 'debug'; // sys.localStorage.clear();

        if (!globalThis.intHttpUrl) {
          globalThis.intHttpUrl = {};
        }

        if (!globalThis.intHttpUrl.CdnUrl) {
          globalThis.intHttpUrl.CdnUrl = [];
        }

        if (!globalThis.intHttpUrl.loginUrl) {
          globalThis.intHttpUrl.loginUrl = [];
        }

        if (!globalThis.intHttpUrl.queryUrl) {
          globalThis.intHttpUrl.queryUrl = [];
        }

        globalThis.cdnIndex = 0; // const cdnLocal = sys.localStorage.getItem('CdnUrl');
        // if(cdnLocal){
        //     const cdn = JSON.parse(cdnLocal);
        //     if(cdn){
        //         globalThis.intHttpUrl.CdnUrl = globalThis.intHttpUrl.CdnUrl.concat(cdn);
        //     }
        // }

        globalThis.intHttpUrl.CdnUrl = globalThis.intHttpUrl.CdnUrl.concat(AllPorts.CdnUrl); // console.log('globalThis.intHttpUrl.CdnUrl:'+globalThis.intHttpUrl.CdnUrl);
        // tools.resetXdnPortsAlias(AllPorts.XDNSource);

        if (globalThis.runMode == 'debug') {
          if (!globalThis.appID) {
            if (sys.platform == sys.Platform.ANDROID) {
              globalThis.appID = '2966';
            } else if (sys.platform == sys.Platform.IOS) {
              globalThis.appID = '1001';
            } else {
              globalThis.appID = '2966'; // globalThis.appID = '2466';
            }
          }
        } else {
          if (!globalThis.appID) {
            if (sys.platform == sys.Platform.ANDROID) {
              globalThis.appID = '2001';
            } else if (sys.platform == sys.Platform.IOS) {
              globalThis.appID = '1001';
            } else {
              globalThis.appID = '2001';
            }
          }
        }
      })();

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=loadConfig.js.map
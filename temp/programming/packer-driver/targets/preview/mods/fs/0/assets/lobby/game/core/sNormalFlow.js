System.register(["__unresolved_0", "cc", "__unresolved_1"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, sGameFlowBase, _dec, _class, _crd, ccclass, property, sNormalFlow;

  function _reportPossibleCrUseOfsGameFlowBase(extras) {
    _reporterNs.report("sGameFlowBase", "./sGameFlowBase", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
    }, function (_unresolved_2) {
      sGameFlowBase = _unresolved_2.sGameFlowBase;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "cb7373+n+FCR5IBMhChE9IV", "sNormalFlow", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("sNormalFlow", sNormalFlow = (_dec = ccclass('sNormalFlow'), _dec(_class = class sNormalFlow extends (_crd && sGameFlowBase === void 0 ? (_reportPossibleCrUseOfsGameFlowBase({
        error: Error()
      }), sGameFlowBase) : sGameFlowBase) {
        start() {
          super.start();
        }

        rollStopAction(round, rollSpeedMode, clickMode) {
          if (round) {}
        }

      }) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=sNormalFlow.js.map
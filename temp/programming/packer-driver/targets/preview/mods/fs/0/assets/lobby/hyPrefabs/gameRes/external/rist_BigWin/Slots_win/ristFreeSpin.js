System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2", "__unresolved_3"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, Animation, Button, director, Label, sComponent, sAudioMgr, sUtil, _dec, _dec2, _dec3, _dec4, _class, _class2, _descriptor, _descriptor2, _descriptor3, _temp, _crd, ccclass, property, ristFreeSpin;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfsComponent(extras) {
    _reporterNs.report("sComponent", "../../../../../game/core/sComponent", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsAudioMgr(extras) {
    _reporterNs.report("sAudioMgr", "../../../../../game/core/sAudioMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsUtil(extras) {
    _reporterNs.report("sUtil", "../../../../../game/core/sUtil", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      Animation = _cc.Animation;
      Button = _cc.Button;
      director = _cc.director;
      Label = _cc.Label;
    }, function (_unresolved_2) {
      sComponent = _unresolved_2.sComponent;
    }, function (_unresolved_3) {
      sAudioMgr = _unresolved_3.default;
    }, function (_unresolved_4) {
      sUtil = _unresolved_4.sUtil;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "646fbW0md9NGIkGLM/dFiYR", "ristFreeSpin", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("ristFreeSpin", ristFreeSpin = (_dec = ccclass('ristFreeSpin'), _dec2 = property(Button), _dec3 = property(Animation), _dec4 = property(Label), _dec(_class = (_class2 = (_temp = class ristFreeSpin extends (_crd && sComponent === void 0 ? (_reportPossibleCrUseOfsComponent({
        error: Error()
      }), sComponent) : sComponent) {
        constructor() {
          super(...arguments);

          _initializerDefineProperty(this, "clickBtn", _descriptor, this);

          _initializerDefineProperty(this, "mAnimation", _descriptor2, this);

          _initializerDefineProperty(this, "spinLabel", _descriptor3, this);

          _defineProperty(this, "touchEnable", false);

          _defineProperty(this, "targetNum", 0);
        }

        start() {
          this.node.on('freeSpinAnima', this.freeSpinAnima, this);
          this.clickBtn.node.on('click', () => {
            if (this.touchEnable) {
              this.touchEnable = false;
              (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                error: Error()
              }), sAudioMgr) : sAudioMgr).PlayShotAudio('btnClick');
              var mainNode = this.node.getChildByName('main');

              if (mainNode) {
                this.mAnimation.on(Animation.EventType.FINISHED, () => {
                  mainNode.active = false;
                  director.emit('ristFreeSpinClose');
                }, this);
                this.mAnimation.play('Opa_Close');
              }
            }
          }, this);
          director.on('ristFreeSpinNum', () => {
            console.log('ristFreeSpinNum');
            (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
              error: Error()
            }), sUtil) : sUtil).TweenLabel(3, this.targetNum, this.spinLabel, 1, 0, 'quadOut', 0, null, true);
          }, this);
        }

        freeSpinAnima(num) {
          var mainNode = this.node.getChildByName('main');

          if (mainNode) {
            this.targetNum = num;
            mainNode.active = true;
            this.SetUIFront();
            this.mAnimation.off(Animation.EventType.FINISHED);
            this.removeScheduleDic();
            this.spinLabel.string = '3';
            this.mAnimation.play('FreeSpins');
            this.scheduleOnce(() => {
              (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                error: Error()
              }), sAudioMgr) : sAudioMgr).PlayShotAudio('slots_spins_bonus');
            }, 0.6);
            this.scheduleOnce(() => {
              this.touchEnable = true;
            }, 2);
            this.pushIDSchedule(() => {
              if (this.touchEnable) {
                this.touchEnable = false;
                this.mAnimation.on(Animation.EventType.FINISHED, () => {
                  mainNode.active = false;
                  director.emit('ristFreeSpinClose');
                }, this);
                this.mAnimation.play('Opa_Close');
              }
            }, 5, 'close');
          }
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "clickBtn", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "mAnimation", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "spinLabel", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=ristFreeSpin.js.map
System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, Label, tween, UIOpacity, v3, Vec3, sComponent, sObjPool, _dec, _dec2, _dec3, _class, _class2, _descriptor, _descriptor2, _temp, _crd, ccclass, property, StarlightPrincess_WinTipLabel;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfsComponent(extras) {
    _reporterNs.report("sComponent", "../lobby/game/core/sComponent", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsObjPool(extras) {
    _reporterNs.report("sObjPool", "../lobby/game/core/sObjPool", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      Label = _cc.Label;
      tween = _cc.tween;
      UIOpacity = _cc.UIOpacity;
      v3 = _cc.v3;
      Vec3 = _cc.Vec3;
    }, function (_unresolved_2) {
      sComponent = _unresolved_2.sComponent;
    }, function (_unresolved_3) {
      sObjPool = _unresolved_3.sObjPool;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "c07b0tn0WtFJrWicNNwJ/QN", "StarlightPrincess_WinTipLabel", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("StarlightPrincess_WinTipLabel", StarlightPrincess_WinTipLabel = (_dec = ccclass('StarlightPrincess_WinTipLabel'), _dec2 = property(UIOpacity), _dec3 = property(Label), _dec(_class = (_class2 = (_temp = class StarlightPrincess_WinTipLabel extends (_crd && sComponent === void 0 ? (_reportPossibleCrUseOfsComponent({
        error: Error()
      }), sComponent) : sComponent) {
        constructor() {
          super(...arguments);

          _initializerDefineProperty(this, "mainOpa", _descriptor, this);

          _initializerDefineProperty(this, "coinLabel", _descriptor2, this);
        }

        start() {// this.scheduleOnce(()=>{
          //     this.play('1.2');
          // },2);
        }

        play(str) {
          this.cleanTweenList();
          this.coinLabel.string = str;
          this.mainOpa.opacity = 0;
          this.node.scale = Vec3.ZERO;
          this.pushOneTween(tween(this.mainOpa).to(0.3, {
            opacity: 255
          }).start());
          this.pushOneTween(tween(this.node).by(0.3, {
            scale: v3(1, 1, 1),
            position: v3(0, 15, 0)
          }).call(() => {
            this.pushOneTween(tween(this.mainOpa).to(1, {
              opacity: 0
            }, {
              easing: 'cubicIn'
            }).start());
            this.pushOneTween(tween(this.node).by(1, {
              scale: v3(-0.6, -0.6, -0.6),
              position: v3(0, 80, 0)
            }).call(() => {
              this.node.active = false;
              (_crd && sObjPool === void 0 ? (_reportPossibleCrUseOfsObjPool({
                error: Error()
              }), sObjPool) : sObjPool).Enqueue(this.node.name, this.node);
            }).start());
          }).start());
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "mainOpa", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "coinLabel", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=StarlightPrincess_WinTipLabel.js.map
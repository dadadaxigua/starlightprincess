System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2", "__unresolved_3", "__unresolved_4", "__unresolved_5", "__unresolved_6", "__unresolved_7", "__unresolved_8", "__unresolved_9"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, Node, view, ResolutionPolicy, v2, director, Label, tween, v3, Vec3, UITransform, sAudioMgr, sComponent, sDeskReq, sInternationalManager, sNewPokerPlayerEntity, sObjPool, sPokerChip, subGameReq, sUtil, _dec, _dec2, _dec3, _dec4, _dec5, _dec6, _dec7, _dec8, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _descriptor5, _descriptor6, _descriptor7, _descriptor8, _descriptor9, _temp, _crd, ccclass, property, sNewPokerGameMgr;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfsAudioMgr(extras) {
    _reporterNs.report("sAudioMgr", "./sAudioMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsComponent(extras) {
    _reporterNs.report("sComponent", "./sComponent", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsDeskReq(extras) {
    _reporterNs.report("sDeskReq", "./sDeskReq", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsInternationalManager(extras) {
    _reporterNs.report("sInternationalManager", "./sInternationalManager", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsNewPokerPlayerEntity(extras) {
    _reporterNs.report("sNewPokerPlayerEntity", "./sNewPokerPlayerEntity", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsObjPool(extras) {
    _reporterNs.report("sObjPool", "./sObjPool", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsPokerChip(extras) {
    _reporterNs.report("sPokerChip", "./sPokerChip", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsubGameReq(extras) {
    _reporterNs.report("subGameReq", "./subGameReq", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsUtil(extras) {
    _reporterNs.report("sUtil", "./sUtil", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      Node = _cc.Node;
      view = _cc.view;
      ResolutionPolicy = _cc.ResolutionPolicy;
      v2 = _cc.v2;
      director = _cc.director;
      Label = _cc.Label;
      tween = _cc.tween;
      v3 = _cc.v3;
      Vec3 = _cc.Vec3;
      UITransform = _cc.UITransform;
    }, function (_unresolved_2) {
      sAudioMgr = _unresolved_2.default;
    }, function (_unresolved_3) {
      sComponent = _unresolved_3.sComponent;
    }, function (_unresolved_4) {
      sDeskReq = _unresolved_4.sDeskReq;
    }, function (_unresolved_5) {
      sInternationalManager = _unresolved_5.sInternationalManager;
    }, function (_unresolved_6) {
      sNewPokerPlayerEntity = _unresolved_6.sNewPokerPlayerEntity;
    }, function (_unresolved_7) {
      sObjPool = _unresolved_7.sObjPool;
    }, function (_unresolved_8) {
      sPokerChip = _unresolved_8.sPokerChip;
    }, function (_unresolved_9) {
      subGameReq = _unresolved_9.subGameReq;
    }, function (_unresolved_10) {
      sUtil = _unresolved_10.sUtil;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "34201JUsQpBFayME8ETGpzl", "sNewPokerGameMgr", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("sNewPokerGameMgr", sNewPokerGameMgr = (_dec = ccclass('sNewPokerGameMgr'), _dec2 = property({
        tooltip: '游戏分辨率'
      }), _dec3 = property({
        type: Node,
        tooltip: '玩家节点对象的根结点'
      }), _dec4 = property({
        type: Label,
        tooltip: '游戏开始的倒计时字体对象'
      }), _dec5 = property({
        type: Label,
        tooltip: '桌面上的总下注字体对象'
      }), _dec6 = property({
        type: Node,
        tooltip: '桌面上的总下注根节点'
      }), _dec7 = property({
        type: Node,
        tooltip: '玩家下出筹码的父节点'
      }), _dec8 = property({
        type: Node,
        tooltip: '房间信息父节点'
      }), _dec(_class = (_class2 = (_temp = class sNewPokerGameMgr extends (_crd && sComponent === void 0 ? (_reportPossibleCrUseOfsComponent({
        error: Error()
      }), sComponent) : sComponent) {
        constructor() {
          super(...arguments);

          _initializerDefineProperty(this, "gameID", _descriptor, this);

          _initializerDefineProperty(this, "viewResolution", _descriptor2, this);

          _initializerDefineProperty(this, "usersNode", _descriptor3, this);

          _initializerDefineProperty(this, "startCDLabel", _descriptor4, this);

          _initializerDefineProperty(this, "tableTotalBetLabel", _descriptor5, this);

          _initializerDefineProperty(this, "tableTotalBetNode", _descriptor6, this);

          _initializerDefineProperty(this, "chipsContainer", _descriptor7, this);

          _initializerDefineProperty(this, "tableInfoContainer", _descriptor8, this);

          _initializerDefineProperty(this, "autoReady", _descriptor9, this);

          _defineProperty(this, "pokerPlayerEntitys", []);

          _defineProperty(this, "selfPokerPlayerEntity", void 0);

          _defineProperty(this, "selfUserSeatPos", 0);

          _defineProperty(this, "pokerTableChips", []);

          _defineProperty(this, "tableTotalBetValue", 0);

          _defineProperty(this, "baseBetCoin", 0);

          _defineProperty(this, "lastRoundBetCoin", 0);

          _defineProperty(this, "deskClientInfo", null);

          _defineProperty(this, "stage", 0);
        }

        onLoad() {
          globalThis.currentPlayingGameID = this.gameID;
          (_crd && subGameReq === void 0 ? (_reportPossibleCrUseOfsubGameReq({
            error: Error()
          }), subGameReq) : subGameReq).Init();
          (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
            error: Error()
          }), sAudioMgr) : sAudioMgr).AudioInit();

          if (this.viewResolution) {
            view.setDesignResolutionSize(this.viewResolution.x, this.viewResolution.y, ResolutionPolicy.FIXED_HEIGHT);
            director.on('CorrectResolution', () => {
              view.setDesignResolutionSize(this.viewResolution.x, this.viewResolution.y, ResolutionPolicy.FIXED_HEIGHT);
            }, this);
          }
        }

        start() {
          if (this.usersNode && this.usersNode.children.length > 0) {
            for (var i = 0; i < this.usersNode.children.length; i++) {
              var userNode = this.usersNode.children[i];
              var playerEntity = userNode.getComponent(_crd && sNewPokerPlayerEntity === void 0 ? (_reportPossibleCrUseOfsNewPokerPlayerEntity({
                error: Error()
              }), sNewPokerPlayerEntity) : sNewPokerPlayerEntity);

              if (playerEntity) {
                if (i == 0) {
                  this.selfPokerPlayerEntity = playerEntity;
                }

                this.pokerPlayerEntitys.push(playerEntity);
              } else {
                console.error('pokerPlayerEntities bind error!!!');
              }
            }
          }

          director.on('socketClosedToGame', type => {
            console.log('socketClosedToGame:' + type);

            if (type == 1) {
              globalThis.Activerecon();
            } else if (type == 2) {} else if (type == 3) {
              globalThis.Activerecon();
            }
          }, this);
          director.on('socketConnectToGame', type => {
            console.log('socketConnectToGame:' + type);

            if (type == 1) {
              this.cleanTable();
              this.reloadTable();
            } else if (type == 2) {
              globalThis.Activerecon();
            }
          }, this);
          director.on('uiTipsOpen', (type, value) => {
            if (globalThis.toast) {
              globalThis.toast(value, 1);
            }
          }, this);
          director.on('pokerBetChip', (chipNum, startPos) => {
            console.log('pokerBetChip');
            var chips = this.createChips(chipNum);

            if (chips && chips.length > 0) {
              this.putChipToTable(chips, startPos, true);
            }
          }, this);
          director.on('pokerEnterDeskSuc', data => {
            (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
              error: Error()
            }), sAudioMgr) : sAudioMgr).PlayBG('bgm_mg');
            this.deskClientInfo = data;
            this.pokerEnterDeskSuc(); // if(this.autoReady){
            //     this.playerReadyAction();
            // }
          }, this);
          director.on('pokerEnterDesk', res => {
            this.playerEnterDesk(res);
          }, this);
          director.on('pokerStartCd', res => {
            if (res && res.data) {
              this.gameStartCd(res.data.dua / 1000 - 1);
            }
          }, this);
          director.on('pokerOnGameStart', res => {
            if (res && res.data) {
              this.gameStart(res.data.join_pos_arr);
            }
          }, this);
          director.on('pokerGameEnd', res => {
            this.gameEnd(res);
          }, this);
          director.on('pokerCardsDeal', res => {
            if (res && res.data) {
              if (this.selfPokerPlayerEntity) {
                this.selfPokerPlayerEntity.GameStart(res.data.pokers);
                this.scheduleOnce(() => {
                  if (res.data.suitType) {
                    if (res.data.suitType == 1) {
                      this.selfPokerPlayerEntity.cardTypeTipTip('sanpai', 3);
                    } else if (res.data.suitType == 2) {
                      this.selfPokerPlayerEntity.cardTypeTipTip('huangjia', 3);
                    } else if (res.data.suitType == 3) {
                      this.selfPokerPlayerEntity.cardTypeTipTip('shunzi', 3);
                    } else if (res.data.suitType == 4) {
                      this.selfPokerPlayerEntity.cardTypeTipTip('tonghua', 3);
                    } else if (res.data.suitType == 5) {
                      this.selfPokerPlayerEntity.cardTypeTipTip('baozi', 3);
                    }
                  }
                }, 1);
              }
            }
          }, this);
          director.on('pokerTurnStart', res => {
            if (res.data.pos) {
              var player = this.getViewPlayer(res.data.pos);

              if (player) {
                player.UserGetTurn();
                this.turnStartPlayer(player);
              }
            }
          }, this);
          director.on('pokerTurnAction', res => {
            if (res.data && res.data.pos) {
              var player = this.getViewPlayer(res.data.pos);

              if (player) {
                this.turnActionPlayer(player, res);
                player.UserTurnAction(res.data.action, res.data.coin, this.baseBetCoin, this.tableTotalBetValue);

                if (res.data.action == 'flow' || res.data.action == 'add') {
                  this.lastRoundBetCoin = player.BetCoin;

                  if (res.data.coin) {
                    this.updateTableTotalBet(Math.abs(res.data.coin), true);
                  }
                }
              }
            }
          }, this);
          director.on('pokerLeaveDesk', res => {
            this.playerLeaveDesk(res);
          }, this);
          director.on('pokerLeaveSuc', () => {
            this.cleanTable();

            if (this.pokerPlayerEntitys) {
              for (var _i = 0; _i < this.pokerPlayerEntitys.length; _i++) {
                var player = this.pokerPlayerEntitys[_i];

                if (player) {
                  player.LeaveDesk(); // player.userInfoViewUpdate();
                }
              }
            }
          }, this); // if(this.leaveBtn){
          //     this.leaveBtn.on('click',()=>{
          //         this.leaveTable();
          //     },this);
          // }else{
          //     console.error('please bind leave button');
          // }
          // this.scheduleOnce(()=>{
          //     this.gameStartCd(5);
          // },2);
          // this.scheduleOnce(()=>{
          //     this.gameStartCd(5);
          // },5);
        }
        /**
         * @zh
         * 桌子信息初始化
         * @param datas [data] : {uid : 用户编号,pos : 座位号,avatar : 头像编号,nickName : 昵称,coin : 拥有的钱...}
         * @param stage 桌子状态 0: 空闲状态（桌子里没有人） 1: 等待状态 2：游戏中 3：结算状态 4：结算结束（等待开局）
         */


        tableInfoInit(datas, stage, nowPos) {
          this.stage = stage;

          if (datas && datas.length > 0) {
            for (var i = 0; i < datas.length; i++) {
              var data = datas[i];

              if (data && this.isSelfUser(data.uid)) {
                this.selfUserSeatPos = data.pos;
                var selfPlayer = this.pokerPlayerEntitys[0];

                if (selfPlayer) {
                  selfPlayer.userInfoDataSetByModel(data);
                  selfPlayer.EnterDesk(data.uid, stage);
                  selfPlayer.userInfoViewUpdate();
                  selfPlayer.IsSelf = true;
                } else {
                  console.error('the userPlayer node is null');
                }

                break;
              }
            }

            for (var _i2 = 0; _i2 < datas.length; _i2++) {
              var _data = datas[_i2];

              if (_data && !this.isSelfUser(_data.uid)) {
                var seat = this.getViewSeatPos(_data.pos);

                if (seat >= 0 && seat < this.pokerPlayerEntitys.length) {
                  var othPlayer = this.pokerPlayerEntitys[seat];

                  if (othPlayer) {
                    othPlayer.userInfoDataSetByModel(_data);
                    othPlayer.EnterDesk(_data.uid, stage);
                    othPlayer.userInfoViewUpdate();
                  } else {
                    console.error('no other userPlayer node');
                  }
                }
              }
            }

            if (this.pokerPlayerEntitys && this.pokerPlayerEntitys.length > 0) {
              var totalBetValue = 0;

              for (var _i3 = 0; _i3 < this.pokerPlayerEntitys.length; _i3++) {
                var player = this.pokerPlayerEntitys[_i3];

                if (player) {
                  if (player.IsPlaying) {
                    player.GameStart(player.UserData.handPokers, false);
                    player.ShowBetInfo(player.UserData.input, this.baseBetCoin, false);
                    totalBetValue += Math.abs(player.UserData.input);

                    if (Math.abs(this.lastRoundBetCoin) < Math.abs(player.UserData.input)) {
                      this.lastRoundBetCoin = -Math.abs(player.UserData.input);
                    }
                  }
                }
              }

              if (stage == 2) {
                this.updateTableTotalBet(totalBetValue, false);

                if (totalBetValue && this.baseBetCoin) {
                  var chips = this.createChips(totalBetValue / this.baseBetCoin);

                  if (chips && chips.length > 0) {
                    this.putChipToTable(chips, null, false);
                  }
                }

                var nowActionPlayer = this.getViewPlayer(nowPos);

                if (nowActionPlayer) {
                  nowActionPlayer.UserGetTurn();
                }
              } else {
                if (this.selfPokerPlayerEntity && this.selfPokerPlayerEntity.UserData.userState == 0) {
                  this.playerReadyAction();
                }
              }
            }
          } else {
            console.error('playersInfoInit error');
          }
        }
        /**
         * @zh
         * 单个玩家信息初始化
         * data : {uid : 用户编号,pos : 座位号,avatar : 头像编号,nickName : 昵称,coin : 拥有的钱...}
         */


        playerInfoInit(data) {
          var seat = this.getViewSeatPos(data.pos);

          if (seat >= 0 && seat < this.pokerPlayerEntitys.length) {
            var player = this.pokerPlayerEntitys[seat];

            if (player) {
              player.userInfoDataSetByModel(data);
              player.EnterDesk(data.uid, this.stage);
              player.userInfoViewUpdate();
            } else {
              console.error('single userPlayer node no found');
            }
          }
        }
        /**
         * @zh
         * 根据uid判断是否是自己
         */


        isSelfUser(uid) {
          var user = globalThis.getUserInfo();

          if (user) {
            if (user.uid == uid) {
              return true;
            }
          }

          return false;
        }
        /**
         * @zh
         * 根据服务端座位号获取场景对应玩家索引
         */


        getViewSeatPos(othSeatPos) {
          var seat = othSeatPos - this.selfUserSeatPos;

          if (seat < 0) {
            seat += this.pokerPlayerEntitys.length;
          }

          return seat;
        }
        /**
         * @zh
         * 根据服务端座位号获取玩家对象
         */


        getViewPlayer(seatPos) {
          var seat = this.getViewSeatPos(seatPos);

          if (seat >= 0 && seat < this.pokerPlayerEntitys.length) {
            return this.pokerPlayerEntitys[seat];
          }

          return null;
        }
        /**
         * @zh
         * 自己离开桌子
         */


        leaveTable() {
          (_crd && sDeskReq === void 0 ? (_reportPossibleCrUseOfsDeskReq({
            error: Error()
          }), sDeskReq) : sDeskReq).leaveTable(null, (err, res) => {
            globalThis.closeLoadView && globalThis.closeLoadView();

            if (res) {
              if (res.code == 200 || res.code != 1002) {
                director.emit('pokerLeaveSuc');
              } else if (res.code == 1) {
                this.scheduleOnce(() => {
                  this.leaveTable();
                }, (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                  error: Error()
                }), sUtil) : sUtil).Random(0.8, 2));
              }
            }
          });
        }

        playerLeaveDesk(res) {
          if (res.data.uid) {
            for (var i = 0; i < this.pokerPlayerEntitys.length; i++) {
              var player = this.pokerPlayerEntitys[i];

              if (player) {
                var userdata = player.UserData;

                if (userdata && userdata.uid == res.data.uid) {
                  player.LeaveDesk(); // player.userInfoViewUpdate();
                }
              }
            }
          }
        } //------need implementation

        /**
         * @zh
         * 自己进桌成功
         */


        pokerEnterDeskSuc() {}
        /**
        * @zh
        * 自己准备操作
        */


        playerReadyAction() {}
        /**
         * @zh
         * 玩家进入桌子
         */


        playerEnterDesk(res) {}
        /**
         * @zh
         * 自己进桌后初始化所有对象
         */


        enterDesk(res) {
          if (res && res.data) {
            this.tableTipInfoInit(res.data.desk_id);
          } else {
            director.emit('pokerLeaveSuc');
          }
        }
        /**
         * @zh
         * 重新载入桌子信息
         */


        reloadTable() {} //-------end implementation
        //-------optional implementation

        /**
         * @zh
         * 桌子进入开始前的倒计时
         * @param _time 倒计时时间
         */


        gameStartCd(_time) {
          this.cleanTable();
          (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
            error: Error()
          }), sAudioMgr) : sAudioMgr).PlayShotAudio('gameStartCd', 0.5);

          if (this.startCDLabel && _time > 0) {
            // console.log('gamestartcd : '+ _time);
            var timeValue = _time;
            this.startCDLabel.node.active = true;

            var startCDLabelCall = () => {
              (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                error: Error()
              }), sAudioMgr) : sAudioMgr).PlayShotAudio('countdown');
              this.removeIDTween('startCDLabelTween');
              this.startCDLabel.node.scale = Vec3.ONE;
              this.startCDLabel.string = (timeValue--).toString();
              this.pushIDTween(tween(this.startCDLabel.node).to(1, {
                scale: v3(0.7, 0.7, 0.7)
              }).call(() => {
                if (timeValue <= 0) {
                  this.startCDLabel.node.active = false;
                }
              }).start(), 'startCDLabelTween');
            };

            this.removeIDSchedule('gameStartCd');
            this.pushIDSchedule(startCDLabelCall, 0, 'gameStartCd', _time, 1, 0, true);
          }
        }
        /**
         * @zh
         * 游戏开始
         * @param joinPlayerPos 参与本局游戏的玩家
         */


        gameStart(joinPlayerPos) {
          this.stage = 2;

          if (joinPlayerPos && joinPlayerPos.length > 0) {
            for (var i = 0; i < joinPlayerPos.length; i++) {
              var playerPos = joinPlayerPos[i];
              var player = this.getViewPlayer(playerPos);

              if (player) {
                player.GameStart();
              }
            }
          }
        }
        /**
         * @zh
         * 游戏结束
         */


        gameEnd(res) {
          this.stage = 1;

          if (res && res.data) {
            var playerListData = res.data.playerList;

            if (playerListData && Array.isArray(playerListData)) {
              for (var i = 0; i < playerListData.length; i++) {
                var playerData = playerListData[i];

                if (playerData) {
                  var player = this.getViewPlayer(playerData.pos);

                  if (player) {
                    player.GameEnd(playerData.handPokers, playerData.win, res.data.type);
                  }
                }
              }
            }
          }
        }
        /**
         * @zh
         * 清除玩家信息之外所有的对象
         */


        cleanTable() {
          this.unscheduleAllCallbacks();

          for (var i = 0; i < this.pokerPlayerEntitys.length; i++) {
            var player = this.pokerPlayerEntitys[i];
            player.cleanTable();
            player.userInfoViewUpdate();
          }

          for (var _i4 = 0; _i4 < this.pokerTableChips.length; _i4++) {
            var chip = this.pokerTableChips[_i4];
            chip.RecycleObj();
          }

          this.tableTotalBetNode.active = false;
          this.pokerTableChips = [];
          this.tableTotalBetValue = 0;
          this.lastRoundBetCoin = 0;
          this.startCDLabel.node.active = false;
        }
        /**
         * @zh
         * 发给玩家的手牌
         * @param pos 玩家位置
         * @param cardData 手牌信息
         */


        gameCardsDeal(pos, cardData) {
          var player = this.getViewPlayer(pos);

          if (player && player.IsSelf) {
            if (cardData && cardData.length > 0) {
              player.GameStart(cardData);
            }
          }
        }
        /**
         * @zh
         * 把下注筹码放到公共区域
         * @param chipNodes 筹码对象
         * @param startPos 动画起始坐标
         * @param anima 是否播放动画
         */


        putChipToTable(chipNodes, startPos, anima) {
          var _this = this;

          if (chipNodes === void 0) {
            chipNodes = null;
          }

          if (startPos === void 0) {
            startPos = null;
          }

          if (anima === void 0) {
            anima = false;
          }

          if (chipNodes && chipNodes.length > 0) {
            for (var i = 0; i < chipNodes.length; i++) {
              var chipNode = chipNodes[i];

              if (chipNode) {
                (function () {
                  var chip = chipNode.getComponent(_crd && sPokerChip === void 0 ? (_reportPossibleCrUseOfsPokerChip({
                    error: Error()
                  }), sPokerChip) : sPokerChip);

                  if (chip) {
                    if (_this.chipsContainer) {
                      var chipTotalNum = _this.pokerTableChips.length;

                      if (_this.pokerTableChips.length < 60) {
                        var index = Math.trunc(chipTotalNum / 10);
                        var colContainer = _this.chipsContainer.children[0];

                        if (colContainer.children.length >= 6) {
                          var targetCol = colContainer.children[colContainer.children.length - 1 - index];
                          chip.node.parent = targetCol;
                          var endPos = v3(0, 10 * targetCol.children.length, 0).add(targetCol.worldPosition);

                          if (anima) {
                            chip.node.worldPosition = startPos;
                            chip.MoveChip(0.5, endPos, i * 0.08, () => {
                              chip.node.active = true;
                            });
                          } else {
                            chip.node.worldPosition = endPos;
                            chip.node.active = true;
                          }

                          _this.pokerTableChips.push(chip);
                        }
                      } else {
                        if (anima) {
                          var _colContainer = _this.chipsContainer.children[1];
                          chip.node.parent = _colContainer;
                          chip.node.worldPosition = startPos;
                          var _endPos = _colContainer.worldPosition;
                          chip.MoveChip(0.5, _endPos, i * 0.08, () => {
                            chip.node.active = true;
                          }, () => {
                            chip.FlipChip(0.5, 0, 0, 255, () => {
                              chip.RecycleObj();
                            });
                          });
                        }
                      }
                    }
                  }
                })();
              }
            }
          }
        }
        /**
         * @zh
         * 把桌子上的筹码发给获胜的玩家
         * @param players 获胜的玩家数组
         */


        putChipToPlayers(players) {
          var _this2 = this;

          if (players && players.length > 0 && this.pokerTableChips && this.pokerTableChips.length > 0) {
            var index = 0;

            var _loop = function _loop(i) {
              var chip = _this2.pokerTableChips[i];

              if (chip) {
                var targetPlayer = players[(_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                  error: Error()
                }), sUtil) : sUtil).RandomInt(0, players.length)];
                chip.MoveChip(0.5, targetPlayer.node.worldPosition, index++ * 0.08, null, () => {
                  chip.node.active = false;
                });
              }
            };

            for (var i = this.pokerTableChips.length - 1; i >= 0; i--) {
              _loop(i);
            }
          }

          this.updateTableTotalBet(-this.tableTotalBetValue, true);
        }
        /**
         * @zh
         * 创建筹码
         * @param sum 筹码的数量
         */


        createChips(sum) {
          var chips = [];

          if (sum > 0) {
            for (var i = 0; i < sum; i++) {
              var chip = (_crd && sObjPool === void 0 ? (_reportPossibleCrUseOfsObjPool({
                error: Error()
              }), sObjPool) : sObjPool).Dequeue('pokerChip');
              chips.push(chip);
            }
          }

          return chips;
        }
        /**
         * @zh
         * 刷新桌子筹码的总数字体显示
         * @param betValue 下注金额
         * @param anima 是否播放动画
         * @param anima 动画时间
         */


        updateTableTotalBet(betValue, anima, animaTime) {
          if (animaTime === void 0) {
            animaTime = 0.4;
          }

          if (this.tableTotalBetNode && this.tableTotalBetLabel) {
            this.removeIDTween('updateTableTotalBet');
            this.tableTotalBetNode.active = true;
            var nowBetValue = this.tableTotalBetValue;
            this.tableTotalBetValue += betValue;

            if (anima) {
              this.pushIDTween((_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                error: Error()
              }), sUtil) : sUtil).TweenLabel(nowBetValue, this.tableTotalBetValue, this.tableTotalBetLabel, animaTime, 0), 'updateTableTotalBet');
            } else {
              this.tableTotalBetLabel.string = (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                error: Error()
              }), sUtil) : sUtil).AddCommas(this.tableTotalBetValue);
            }
          }
        }
        /**
         * @zh
         * 轮到玩家操作
         * @param player 玩家对象
         */


        turnStartPlayer(player) {}
        /**
         * @zh
         * 玩家操作
         * @param player 玩家对象
         */


        turnActionPlayer(player, res) {}
        /**
         * @zh
         * 桌子右上角文本信息显示
         * @param deskId 桌号
         */


        tableTipInfoInit(deskId) {
          if (this.deskClientInfo) {
            if (this.tableInfoContainer) {
              var baseLabel = this.tableInfoContainer.getChildByName('baseLabel');
              var baseTitle = this.tableInfoContainer.getChildByName('baseTitle');
              var coinIcon = this.tableInfoContainer.getChildByName('coin');

              if (baseLabel && baseTitle && coinIcon) {
                var _baseLabel = baseLabel.getComponent(Label);

                baseTitle.getComponent(Label).string = (_crd && sInternationalManager === void 0 ? (_reportPossibleCrUseOfsInternationalManager({
                  error: Error()
                }), sInternationalManager) : sInternationalManager).GetDataByKey('base') + ':        ';
                _baseLabel.string = (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                  error: Error()
                }), sUtil) : sUtil).changeNumberToKW(this.deskClientInfo.base_coin);

                _baseLabel.updateRenderData(true);

                var wid = _baseLabel.getComponent(UITransform).width;

                baseTitle.position = v3(-wid, baseTitle.position.y, 0);
                coinIcon.position = v3(-wid - 15, baseTitle.position.y, 0);
              }

              var roomName = this.tableInfoContainer.getChildByName('roomName');

              if (roomName) {
                roomName.getComponent(Label).string = 'Lieng' + ' - ' + this.deskClientInfo.name;
              }

              var roomNumber = this.tableInfoContainer.getChildByName('roomNumber');

              if (roomNumber && deskId) {
                roomNumber.getComponent(Label).string = (_crd && sInternationalManager === void 0 ? (_reportPossibleCrUseOfsInternationalManager({
                  error: Error()
                }), sInternationalManager) : sInternationalManager).GetDataByKey('tableNo') + ' : ' + deskId;
              }
            }
          }
        } //-------end implementation


      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "gameID", [property], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return '';
        }
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "viewResolution", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return v2(0, 0);
        }
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "usersNode", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "startCDLabel", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor5 = _applyDecoratedDescriptor(_class2.prototype, "tableTotalBetLabel", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor6 = _applyDecoratedDescriptor(_class2.prototype, "tableTotalBetNode", [_dec6], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor7 = _applyDecoratedDescriptor(_class2.prototype, "chipsContainer", [_dec7], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor8 = _applyDecoratedDescriptor(_class2.prototype, "tableInfoContainer", [_dec8], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor9 = _applyDecoratedDescriptor(_class2.prototype, "autoReady", [property], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return true;
        }
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=sNewPokerGameMgr.js.map
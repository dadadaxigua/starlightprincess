System.register(["cc"], function (_export, _context) {
  "use strict";

  var _cclegacy, _crd, info;

  return {
    setters: [function (_cc) {
      _cclegacy = _cc.cclegacy;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "5e9324jfXBPZKor9Yc7IRGl", "game", undefined);

      info = {
        mainScript: null,
        currGameId: 0,
        mySeatPos: 0,
        socketClosed: false,
        roomList: {
          fish: [],
          bigAwardCompetition: [],
          atOnceCompetition: []
        },
        vipHeadList: [],
        detailChannelSet: {
          name: '',
          url: ""
        },
        loginDate: null,
        inviteCode: '0',
        count_weakNet: 0,
        myAgentLevel: 1,
        access_token: null,
        wxAppId: "",
        isHaveWXLogin: 1,
        qrCodeCount: 0,
        reloadqrcode: false,
        shareUrlCode: null,
        kefu_Appkey: null,
        serverUrl: null,
        copyUrl: null,
        agentCopyUrl: null,
        shareqrCodeUrl: null,
        guanwangUrl: null,
        sharePngUrl: null,
        slide_switch: [],
        saveUrl: null,
        noticeSwitch: true,
        timeDiff: 0,
        isOpenChat: false,
        //是否打开了聊天
        childGamePicture: null,
        webType: 1,
        connectClosed: false,
        //链接是否关闭
        isReconnection: false,
        //是否重连
        version: '20220324_7',
        //版本号
        purchaseVer: [],
        //需苹果审核的版本号字段---向后台请求。
        IOSDownloadUrl: '',
        //苹果App更新地址
        AndroidDownloadUrl: '',
        //安卓App更新地址
        curStatus: 1,
        //1:正常状态，2：充值中状态：
        maintain: false,
        //是否即将维护
        updateUrl: '',
        localUnReadChatNum: {},
        onLineUnRedChatNum: {},
        attribute: 1,
        //包属性，1直推，2代理包
        trial: false,
        //首次登陆弹出注册
        e_info: null,
        //收益列表信息
        isOnFishingLobby: true,
        //现在的场景
        firstScene: 'game-lobby',
        //第一个场景
        is_open_agent: 0,
        halfOrderStatue: 0,
        //半自动订单状态;
        haveHalfOrder: 0,
        currentScene: null,
        //当前场景
        isReturnLogin: false,
        //是否返回登录
        gamesInfo: {},
        //游戏信息
        gamesList: {},
        //游戏列表
        roomsList: {},
        //房间列表信息
        __loads: {},
        rankList: [],
        userList: null,
        lobbyPrefabs: {},
        dlurl: '',
        //代理网页域名
        floatPlaces: 0,
        beMove: true,
        headSpriteAtlas: {},
        is_open_notification: 1,
        is_open_exchange: 1,
        roomIdList: [],
        netWorkStatus: 'netWork',
        gamesReadName: {
          101: 'Medusa',
          102: 'Fu Xing Gao Zhao',
          103: 'Easter',
          104: 'Fire Spin',
          105: 'Samba',
          106: 'Money Mage',
          107: 'Luckycat',
          108: 'Wu Fu Lin Men',
          109: 'Hercules',
          110: 'Homerun Baseball'
        },
        canGoGameTourist: [131, 120, 104, 133, 132],
        agencyList: [],
        loginErr: {
          code: 0,
          msg: ''
        },
        //登录错误
        isMoveing: false,
        //防止玩家两点两个btn
        ticksDiff: 0,
        //时差，毫秒 local time - server time
        serverTime: 0,
        blessingRequiredAmount: [100000, 1000000],
        backInfo: null,
        //需要返回的信息
        channelCode: '',
        //渠道号
        accChannelCode: '',
        //帐号渠道号
        lastGetTureTable: null,
        turntableData: null,
        tureTableInterval: null,
        getChannelCode: true,
        bid: null,
        bundleid: '',
        XdnKey: '',
        defaultNotice: '欢迎来到合集大厅游戏',
        defaultUrl: '',
        currentGameVersion: {
          lobby: ''
        },
        openChargePrefabs: false,
        certificationMessage: {},
        login_ID: '',
        login_Src: 2,
        curPanelPos: 0,
        infoArr: [],
        firstInfo: {},
        activityQueue: [],
        startQueue: true,
        activityUrl: '',
        firstThreeGame: [],
        notice: '欢迎来到游戏!',
        activity_count: 0,
        Orientation: true,
        peoArg: {},
        welfareSwitch: true,
        welfareSwitchlocal: true,
        welfareNum: 0,
        welfareType: 2,
        firstLoadLobby: true,
        order_id: '',
        //充值订单号
        visitor_not_enter_room_list: {},
        loginLogo: null,
        isFirstTipNotice: true,
        PictureFrameSprite: null,
        //头像框
        allversionInfo: {
          data: null
        },
        gameDownloadQueue: [],
        //游戏下载队列
        gameDownloading: false,
        //是否有游戏正在下载
        lobbyEnterRoomName: '',
        lobbyEnterRoomID: '',
        lobbyEnterformal: false,
        lobbyEnterCallBack: null,
        gameMaintain: false,
        needInviteBtn: null,
        lobbyProVersion: 1,
        strategy: '0',
        strategyEnable: true,
        gonggaoView: false,
        logEnable: '0',
        roadRoomId: 0,
        roadRoomType: 0,
        musicPath: {
          brnn: 'bairen_nn_enter',
          //百人
          safeBox: 'baoxianxiang_enter',
          //保险箱
          csTask: 'caishenrenwu_enter',
          //任务
          shop: 'chongzhi_enter',
          //充值
          ddz: 'ddz_second_enter',
          //d斗地主
          exchange: 'duihuan_enter',
          //收益
          update: 'game_update_succ',
          //更新
          servers: 'kefu_enter',
          //客服
          mail: 'mail_enter',
          //邮件
          rank: 'paihangbang_enter',
          //排行榜
          lobby: 'qi_kai_de_sheng',
          //老板好 祝老板旗开得胜
          qznn: 'qznn_second_enter',
          //抢庄
          finshTask: 'task',
          //完成任务
          vip: 'vip_enter',
          //vip
          agency: 'yuerubaiwan_enter',
          //代理
          zjh: 'zjh_second_enter' //扎金花

        },
        gameIsLoad: {
          'FightLandlord': false,
          'fiveNiuNiu': false,
          'TexasPoker': false,
          'Fish': false,
          'Fqzs': false,
          'boomNiu': false,
          'Csd': false,
          'Zjh': false,
          'LkFish': false,
          'Benz': false,
          'Brnn': false,
          'redBlack': false,
          'LHFlight': false
        },
        gameIsUpdate: {
          'FightLandlord': false,
          'fiveNiuNiu': false,
          'TexasPoker': false,
          'Fish': false,
          'Fqzs': false,
          'boomNiu': false,
          'Csd': false,
          'Zjh': false,
          'LkFish': false,
          'Benz': false,
          'Brnn': false,
          'redBlack': false,
          'LHFlight': false
        },
        downloadGame: [],
        needBackGame: function needBackGame() {
          //调用，以在getDesk/比赛ready返回err.code = 12时进入游戏（重置global参数）
          if (!this.backInfo || this.backInfo.endAt * 1000 + this.ticksDiff < new Date().getTime()) return null;else return {
            roomId: this.backInfo.roomId,
            deskId: this.backInfo.deskId,
            position: this.backInfo.position,
            artilleryId: this.backInfo.artilleryId,
            shellValue: this.backInfo.shellValue,
            isCompetition: this.backInfo.isCompetition,
            countdown: this.backInfo.endAt * 1000 + this.ticksDiff - new Date().getTime() //倒计时，以毫秒计

          };
        }
      }; // info.headSpriteAtlas.getSpriteFrame = function(value,cb){
      //     if(Object.keys(info.headSpriteAtlas).length <= 1){
      //         ResLoader.loadDir("nRes/head", SpriteFrame,  (err, assets)=> {
      //             if(assets){
      //                 for(let i = 0;i < assets.length;i++){
      //                     info.headSpriteAtlas[assets[i].name] = assets[i];
      //                 }
      //                 if(cb){
      //                     cb(info.headSpriteAtlas[value]);
      //                 }
      //             }
      //         });
      //     }else{
      //         if(cb){
      //             cb(info.headSpriteAtlas[value]);
      //         }
      //     }
      // },
      // info.avatarSpriteAtlas = function(value,cb){
      //     if(!info.avatarHeadAtlas || info.avatarHeadAtlas == null){
      //         // info.avatarHeadAtlas = cc.loader.loadRes('')
      //     }
      //     let _sprite = info.avatarHeadAtlas.getSpriteFrame(value);
      //     if(cb){
      //         if(_sprite){
      //             cb(_sprite);
      //         } 
      //     }
      // }
      // info.avatarSpriteAtlas = cc.loader.loadRes

      _export("default", info);

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=game.js.map
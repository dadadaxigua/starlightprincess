System.register(["cc"], function (_export, _context) {
  "use strict";

  var _cclegacy, director, _crd;

  return {
    setters: [function (_cc) {
      _cclegacy = _cc.cclegacy;
      director = _cc.director;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "62d70Gj2vdLPIjHqNw852aM", "pomelo", undefined);

      (function () {
        /**
         * Expose `Emitter`.
         */
        // module.exports = Emitter;

        /**
         * Initialize a new `Emitter`.
         *
         * @api public
         */
        function Emitter(obj) {
          if (obj) return mixin(obj);
        }

        ;
        /**
         * Mixin the emitter properties.
         *
         * @param {Object} obj
         * @return {Object}
         * @api private
         */

        function mixin(obj) {
          for (var key in Emitter.prototype) {
            obj[key] = Emitter.prototype[key];
          }

          return obj;
        }
        /**
         * Listen on the given `event` with `fn`.
         *
         * @param {String} event
         * @param {Function} fn
         * @return {Emitter}
         * @api public
         */


        Emitter.prototype.on = Emitter.prototype.addEventListener = function (event, fn) {
          this._callbacks = this._callbacks || {};
          (this._callbacks[event] = this._callbacks[event] || []).push(fn);
          return this;
        };
        /**
         * Adds an `event` listener that will be invoked a single
         * time then automatically removed.
         *
         * @param {String} event
         * @param {Function} fn
         * @return {Emitter}
         * @api public
         */


        Emitter.prototype.once = function (event, fn) {
          var self = this;
          this._callbacks = this._callbacks || {};

          function on() {
            self.off(event, on);
            fn.apply(this, arguments);
          }

          on.fn = fn;
          this.on(event, on);
          return this;
        };
        /**
         * Remove the given callback for `event` or all
         * registered callbacks.
         *
         * @param {String} event
         * @param {Function} fn
         * @return {Emitter}
         * @api public
         */


        Emitter.prototype.off = Emitter.prototype.removeListener = Emitter.prototype.removeAllListeners = Emitter.prototype.removeEventListener = function (event, fn) {
          this._callbacks = this._callbacks || {}; // all

          if (0 == arguments.length) {
            this._callbacks = {};
            return this;
          } // specific event


          var callbacks = this._callbacks[event];
          if (!callbacks) return this; // remove all handlers

          if (1 == arguments.length) {
            delete this._callbacks[event];
            return this;
          } // remove specific handler


          var cb;

          for (var i = 0; i < callbacks.length; i++) {
            cb = callbacks[i];

            if (cb === fn || cb.fn === fn) {
              callbacks.splice(i, 1);
              break;
            }
          }

          return this;
        };
        /**
         * Emit `event` with the given args.
         *
         * @param {String} event
         * @param {Mixed} ...
         * @return {Emitter}
         */


        Emitter.prototype.emit = function (event) {
          this._callbacks = this._callbacks || {};
          var args = [].slice.call(arguments, 1),
              callbacks = this._callbacks[event];

          if (callbacks) {
            callbacks = callbacks.slice(0);

            for (var i = 0, len = callbacks.length; i < len; ++i) {
              callbacks[i].apply(this, args);
            }
          }

          return this;
        };
        /**
         * Return array of callbacks for `event`.
         *
         * @param {String} event
         * @return {Array}
         * @api public
         */


        Emitter.prototype.listeners = function (event) {
          this._callbacks = this._callbacks || {};
          return this._callbacks[event] || [];
        };
        /**
         * Check if this emitter has `event` handlers.
         *
         * @param {String} event
         * @return {Boolean}
         * @api public
         */


        Emitter.prototype.hasListeners = function (event) {
          return !!this.listeners(event).length;
        }; // if (typeof (window) != "undefined") {


        globalThis.EventEmitter = Emitter; // }
      })();

      (function (exports, ByteArray, global) {
        var Protocol = exports;
        var PKG_HEAD_BYTES = 4;
        var MSG_FLAG_BYTES = 1;
        var MSG_ROUTE_CODE_BYTES = 2;
        var MSG_ID_MAX_BYTES = 5;
        var MSG_ROUTE_LEN_BYTES = 1;
        var MSG_ROUTE_CODE_MAX = 0xffff;
        var MSG_COMPRESS_ROUTE_MASK = 0x1;
        var MSG_TYPE_MASK = 0x7;
        var Package = Protocol.Package = {};
        var Message = Protocol.Message = {};
        Package.TYPE_HANDSHAKE = 1;
        Package.TYPE_HANDSHAKE_ACK = 2;
        Package.TYPE_HEARTBEAT = 3;
        Package.TYPE_DATA = 4;
        Package.TYPE_KICK = 5;
        Message.TYPE_REQUEST = 0;
        Message.TYPE_NOTIFY = 1;
        Message.TYPE_RESPONSE = 2;
        Message.TYPE_PUSH = 3;
        /**
         * pomele client encode
         * id message id;
         * route message route
         * msg message body
         * socketio current support string
         */

        Protocol.strencode = function (str) {
          var byteArray = new ByteArray(str.length * 3);
          var offset = 0;

          for (var i = 0; i < str.length; i++) {
            var charCode = str.charCodeAt(i);
            var codes = null;

            if (charCode <= 0x7f) {
              codes = [charCode];
            } else if (charCode <= 0x7ff) {
              codes = [0xc0 | charCode >> 6, 0x80 | charCode & 0x3f];
            } else {
              codes = [0xe0 | charCode >> 12, 0x80 | (charCode & 0xfc0) >> 6, 0x80 | charCode & 0x3f];
            }

            for (var j = 0; j < codes.length; j++) {
              byteArray[offset] = codes[j];
              ++offset;
            }
          }

          var _buffer = new ByteArray(offset);

          copyArray(_buffer, 0, byteArray, 0, offset);
          return _buffer;
        };
        /**
         * client decode
         * msg String data
         * return Message Object
         */


        Protocol.strdecode = function (buffer) {
          var bytes = new ByteArray(buffer);
          var array = [];
          var offset = 0;
          var charCode = 0;
          var end = bytes.length;

          while (offset < end) {
            if (bytes[offset] < 128) {
              charCode = bytes[offset];
              offset += 1;
            } else if (bytes[offset] < 224) {
              charCode = ((bytes[offset] & 0x3f) << 6) + (bytes[offset + 1] & 0x3f);
              offset += 2;
            } else {
              charCode = ((bytes[offset] & 0x0f) << 12) + ((bytes[offset + 1] & 0x3f) << 6) + (bytes[offset + 2] & 0x3f);
              offset += 3;
            }

            array.push(charCode);
          }

          return String.fromCharCode.apply(null, array);
        };
        /**
         * Package protocol encode.
         *
         * Pomelo package format:
         * +------+-------------+------------------+
         * | type | body length |       body       |
         * +------+-------------+------------------+
         *
         * Head: 4bytes
         *   0: package type,
         *      1 - handshake,
         *      2 - handshake ack,
         *      3 - heartbeat,
         *      4 - data
         *      5 - kick
         *   1 - 3: big-endian body length
         * Body: body length bytes
         *
         * @param  {Number}    type   package type
         * @param  {ByteArray} body   body content in bytes
         * @return {ByteArray}        new byte array that contains encode result
         */


        Package.encode = function (type, body) {
          var length = body ? body.length : 0;
          var buffer = new ByteArray(PKG_HEAD_BYTES + length);
          var index = 0;
          buffer[index++] = type & 0xff;
          buffer[index++] = length >> 16 & 0xff;
          buffer[index++] = length >> 8 & 0xff;
          buffer[index++] = length & 0xff;

          if (body) {
            copyArray(buffer, index, body, 0, length);
          }

          return buffer;
        };
        /**
         * Package protocol decode.
         * See encode for package format.
         *
         * @param  {ByteArray} buffer byte array containing package content
         * @return {Object}           {type: package type, buffer: body byte array}
         */


        Package.decode = function (buffer) {
          // console.log(buffer);
          var offset = 0;
          var bytes = new ByteArray(buffer);
          var length = 0;
          var rs = [];

          while (offset < bytes.length) {
            var type = bytes[offset++];
            length = (bytes[offset++] << 16 | bytes[offset++] << 8 | bytes[offset++]) >>> 0;
            var body = length ? new ByteArray(length) : null;
            copyArray(body, 0, bytes, offset, length);
            offset += length;
            rs.push({
              'type': type,
              'body': body
            });
          }

          return rs.length === 1 ? rs[0] : rs;
        };
        /**
         * Message protocol encode.
         *
         * @param  {Number} id            message id
         * @param  {Number} type          message type
         * @param  {Number} compressRoute whether compress route
         * @param  {Number|String} route  route code or route string
         * @param  {Buffer} msg           message body bytes
         * @return {Buffer}               encode result
         */


        Message.encode = function (id, type, compressRoute, route, msg) {
          // caculate message max length
          var idBytes = msgHasId(type) ? caculateMsgIdBytes(id) : 0;
          var msgLen = MSG_FLAG_BYTES + idBytes;

          if (msgHasRoute(type)) {
            if (compressRoute) {
              if (typeof route !== 'number') {
                throw new Error('error flag for number route!');
              }

              msgLen += MSG_ROUTE_CODE_BYTES;
            } else {
              msgLen += MSG_ROUTE_LEN_BYTES;

              if (route) {
                route = Protocol.strencode(route);

                if (route.length > 255) {
                  throw new Error('route maxlength is overflow');
                }

                msgLen += route.length;
              }
            }
          }

          if (msg) {
            msgLen += msg.length;
          }

          var buffer = new ByteArray(msgLen);
          var offset = 0; // add flag

          offset = encodeMsgFlag(type, compressRoute, buffer, offset); // add message id

          if (msgHasId(type)) {
            offset = encodeMsgId(id, buffer, offset);
          } // add route


          if (msgHasRoute(type)) {
            offset = encodeMsgRoute(compressRoute, route, buffer, offset);
          } // add body


          if (msg) {
            offset = encodeMsgBody(msg, buffer, offset);
          }

          return buffer;
        };
        /**
         * Message protocol decode.
         *
         * @param  {Buffer|Uint8Array} buffer message bytes
         * @return {Object}            message object
         */


        Message.decode = function (buffer) {
          var bytes = new ByteArray(buffer);
          var bytesLen = bytes.length || bytes.byteLength;
          var offset = 0;
          var id = 0;
          var route = null; // parse flag

          var flag = bytes[offset++];
          var compressRoute = flag & MSG_COMPRESS_ROUTE_MASK;
          var type = flag >> 1 & MSG_TYPE_MASK; // parse id

          if (msgHasId(type)) {
            var m = parseInt(bytes[offset]);
            var i = 0;

            do {
              var m = parseInt(bytes[offset]);
              id = id + (m & 0x7f) * Math.pow(2, 7 * i);
              offset++;
              i++;
            } while (m >= 128);
          } // parse route


          if (msgHasRoute(type)) {
            if (compressRoute) {
              route = bytes[offset++] << 8 | bytes[offset++];
            } else {
              var routeLen = bytes[offset++];

              if (routeLen) {
                route = new ByteArray(routeLen);
                copyArray(route, 0, bytes, offset, routeLen);
                route = Protocol.strdecode(route);
              } else {
                route = '';
              }

              offset += routeLen;
            }
          } // parse body


          var bodyLen = bytesLen - offset;
          var body = new ByteArray(bodyLen);
          copyArray(body, 0, bytes, offset, bodyLen);
          return {
            'id': id,
            'type': type,
            'compressRoute': compressRoute,
            'route': route,
            'body': body
          };
        };

        var copyArray = function copyArray(dest, doffset, src, soffset, length) {
          if ('function' === typeof src.copy) {
            // Buffer
            src.copy(dest, doffset, soffset, soffset + length);
          } else {
            // Uint8Array
            for (var index = 0; index < length; index++) {
              dest[doffset++] = src[soffset++];
            }
          }
        };

        var msgHasId = function msgHasId(type) {
          return type === Message.TYPE_REQUEST || type === Message.TYPE_RESPONSE;
        };

        var msgHasRoute = function msgHasRoute(type) {
          return type === Message.TYPE_REQUEST || type === Message.TYPE_NOTIFY || type === Message.TYPE_PUSH;
        };

        var caculateMsgIdBytes = function caculateMsgIdBytes(id) {
          var len = 0;

          do {
            len += 1;
            id >>= 7;
          } while (id > 0);

          return len;
        };

        var encodeMsgFlag = function encodeMsgFlag(type, compressRoute, buffer, offset) {
          if (type !== Message.TYPE_REQUEST && type !== Message.TYPE_NOTIFY && type !== Message.TYPE_RESPONSE && type !== Message.TYPE_PUSH) {
            throw new Error('unkonw message type: ' + type);
          }

          buffer[offset] = type << 1 | (compressRoute ? 1 : 0);
          return offset + MSG_FLAG_BYTES;
        };

        var encodeMsgId = function encodeMsgId(id, buffer, offset) {
          do {
            var tmp = id % 128;
            var next = Math.floor(id / 128);

            if (next !== 0) {
              tmp = tmp + 128;
            }

            buffer[offset++] = tmp;
            id = next;
          } while (id !== 0);

          return offset;
        };

        var encodeMsgRoute = function encodeMsgRoute(compressRoute, route, buffer, offset) {
          if (compressRoute) {
            if (route > MSG_ROUTE_CODE_MAX) {
              throw new Error('route number is overflow');
            }

            buffer[offset++] = route >> 8 & 0xff;
            buffer[offset++] = route & 0xff;
          } else {
            if (route) {
              buffer[offset++] = route.length & 0xff;
              copyArray(buffer, offset, route, 0, route.length);
              offset += route.length;
            } else {
              buffer[offset++] = 0;
            }
          }

          return offset;
        };

        var encodeMsgBody = function encodeMsgBody(msg, buffer, offset) {
          copyArray(buffer, offset, msg, 0, msg.length);
          return offset + msg.length;
        }; // module.exports = Protocol;


        if (typeof globalThis != "undefined") {
          globalThis.Protocol = Protocol;
        }
      })({}, Uint8Array, void 0);

      (function () {
        var JS_WS_CLIENT_TYPE = 'js-websocket';
        var JS_WS_CLIENT_VERSION = '0.0.1';
        var Protocol = globalThis.Protocol;
        var protobufjs = globalThis.m_Protobuf;

        if (!protobufjs) {
          protobufjs = globalThis.protobuf;
          globalThis.m_Protobuf = globalThis.protobuf;
        }

        if (globalThis.__v_pomelo) {
          globalThis.decodeIO_encoder = protobufjs.Root.fromJSON(globalThis._clientProtos); // console.log('cc._clientProtos:'+cc._clientProtos);

          globalThis.decodeIO_decoder = protobufjs.Root.fromJSON(globalThis._serverProtos); // console.log('cc._serverProtos:'+cc._serverProtos);

          globalThis.push_decoder = protobufjs.Root.fromJSON(globalThis._pushProtos);
          globalThis.pomelo = globalThis.__v_pomelo; // console.log("globalThis.push_decoder", globalThis.push_decoder);

          return;
        }

        var Package = Protocol.Package;
        var Message = Protocol.Message;
        var EventEmitter = globalThis.EventEmitter;
        var rsa = globalThis.rsa;
        var disconnectCb = null;
        var RES_OK = 200;
        var RES_FAIL = 500;
        var RES_OLD_CLIENT = 501;

        if (typeof Object.create !== 'function') {
          Object.create = function (o) {
            function F() {}

            F.prototype = o;
            return new F();
          };
        } // var root = window;


        var pomelo = Object.create(EventEmitter.prototype); // object extend from object
        // console.log('cc._clientProtos11:'+globalThis._clientProtos);
        // console.log('cc._serverProtos11:'+globalThis._serverProtos); 
        // console.log('cc._pushProtos11:'+globalThis._pushProtos); 

        globalThis.decodeIO_encoder = protobufjs.Root.fromJSON(globalThis._clientProtos);
        globalThis.decodeIO_decoder = protobufjs.Root.fromJSON(globalThis._serverProtos);
        globalThis.push_decoder = protobufjs.Root.fromJSON(globalThis._pushProtos); // console.log("globalThis.push_decoder11111",globalThis.push_decoder);
        // console.log("globalThis.push_decoder11111",JSON.stringify(globalThis.push_decoder));

        globalThis.__v_pomelo = pomelo;
        globalThis.pomelo = pomelo;
        var socket = null;
        var socketUrl = null;
        var socketSid = 100;
        var reqId = 0;
        var callbacks = {};
        var handlers = {}; //Map from request id to route

        var routeMap = {};
        var dict = {}; // route string to code

        var abbrs = {}; // code to route string

        var serverProtos = {};
        var clientProtos = {};
        var pushProtos = {};
        var protoVersion = 0;
        var heartbeatInterval = 0;
        var heartbeatTimeout = 0;
        var nextHeartbeatTimeout = 0;
        var gapThreshold = 100;
        var heartbeatId = null;
        var heartbeatTimeoutId = null;
        var handshakeCallback = null;
        var decode = null;
        var encode = null;
        var reconnect = false;
        var reconncetTimer = null;
        var hardCloseTimer = null;
        var msgTimeoutTimer = [];
        var reconnectUrl = null;
        var reconnectAttempts = 0;
        var reconnectionDelay = 5000;
        var DEFAULT_MAX_RECONNECT_ATTEMPTS = 10;
        var useCrypto;
        var handshakeBuffer = {
          'sys': {
            type: JS_WS_CLIENT_TYPE,
            version: JS_WS_CLIENT_VERSION,
            rsa: {}
          },
          'user': {}
        };
        var initCallback = null;

        pomelo.init = function (_ref, cb) {
          var {
            host,
            port,
            access_token,
            device_id
          } = _ref;
          initCallback = cb;
          globalThis.clientSid++;
          socketSid = globalThis.clientSid;
          encode = defaultEncode;
          decode = defaultDecode;
          var url; //url = 'ws:' + host;

          if (host.includes("192.168.")) {
            url = 'ws:' + host;
          } else {
            url = 'wss:' + host;
          }

          console.log("web websocket url:", url); // var url = 'ws:' + host;

          if (port) {
            url += ':' + port;
          }

          console.log("web websocket url + port:", url); // url += '/sid=' + ++socketSid;

          connect({
            access_token,
            device_id
          }, url, cb);
        };

        var defaultDecode = pomelo.decode = function (data) {
          //probuff decode
          var msg = Message.decode(data);

          if (msg.id > 0) {
            msg.route = routeMap[msg.id];
            delete routeMap[msg.id];

            if (!msg.route) {
              return;
            }
          }

          msg.body = deCompose(msg);
          return msg;
        };

        var defaultEncode = pomelo.encode = function (reqId, route, msg) {
          var type = reqId ? Message.TYPE_REQUEST : Message.TYPE_NOTIFY; //console.log('testkeys', Object.keys(globalThis.decodeIO_encoder.nested));

          if (globalThis.decodeIO_encoder && globalThis.decodeIO_encoder.lookup(route)) {
            var um = globalThis.decodeIO_encoder.lookupType(route);
            var buf = um.encode(um.create(msg)).finish(); //console.log('send', route, '-encode bufio-', buf.toString());

            msg = buf;
          } else {
            //console.log('send', route, '-encode, json');
            msg = Protocol.strencode(JSON.stringify(msg));
          }

          var compressRoute = 0;

          if (dict && dict[route]) {
            route = dict[route];
            compressRoute = 1;
          }

          return Message.encode(reqId, type, compressRoute, route, msg);
        };

        var connect = function connect(_ref2, url, cb) {
          var {
            access_token,
            device_id
          } = _ref2;
          console.log('connect to ' + url);

          if (socket && socket.readyState == 0) {
            console.log('connect is not success readyState: ' + socket.readyState);
            pomelo.disconnect();
          }

          reconnectUrl = url;
          handshakeBuffer.sys.protoVersion = -1;

          var onopen = function onopen(event) {
            console.log(new Date().toLocaleTimeString('en-US', {
              hour12: false
            }), "---------nettestpp  socket onopen");

            if (!!reconnect) {
              pomelo.emit('reconnect');
            }

            reset();
            var obj = Package.encode(Package.TYPE_HANDSHAKE, Protocol.strencode(JSON.stringify(handshakeBuffer))); // console.log("---------nettestpp  socket send HANDSHAKE");

            send(obj);
          };

          var onmessage = function onmessage(event) {
            processPackage(Package.decode(event.data));

            if (heartbeatTimeout) {
              //心跳
              if (event.data.byteLength == 4) {
                globalThis.heartBeatEnd = Date.now(); //console.log("globalThis.heartBeatEnd:", globalThis.heartBeatEnd);

                var heartTciks = globalThis.heartBeatEnd - globalThis.heartBeatStart; //console.log("-----------heartTciks-----------", heartTciks);

                director.emit("heartbeatChange", heartTciks);
              }

              nextHeartbeatTimeout = Date.now() + heartbeatTimeout;
            }

            if (msgTimeoutTimer) {
              for (var i = 0; i < msgTimeoutTimer.length; i++) {
                clearTimeout(msgTimeoutTimer[i]);
              }
            }
          };

          var onerror = function onerror(event) {
            console.error(new Date().toLocaleTimeString('en-US', {
              hour12: false
            }), event.target.url != socketUrl, "---------nettestpp  socket onerror", event.target.url, event.type, event.detail); // if (event.target.url != socketUrl) {
            //   return;
            // }

            event.sid = socketSid;
            pomelo.emit('error', event); // pomelo.emit('io-error', event);
            // pomelo.emit('io-error1', event);
          };

          var onclose = function onclose(event) {
            console.log(new Date().toLocaleTimeString('en-US', {
              hour12: false
            }), event.target.url != socketUrl && 'will not emit!', "---------nettestpp  socket onclose", event.target.url, socketUrl, event && event.reason); // if(event.target.url != socketUrl){
            //   return;
            // }

            event.sid = socketSid;
            socket = null;
            disconnectCb && disconnectCb();
            reset();
            pomelo.emit('close', event);
            pomelo.emit('disconnect', event);
          };

          if (url.includes("192.168.")) {
            socket = new WebSocket(url, ["access_token", access_token, "device_id", device_id, "XVersion", globalThis.__xdnVer]);
          } else {
            socket = new WebSocket(url, ["access_token", access_token, "device_id", device_id, "XVersion", globalThis.__xdnVer], globalThis.certification); //socket = new WebSocket(url, ["access_token", access_token, "device_id", device_id, "XVersion", globalThis.__xdnVer]);
          }

          socketUrl = url;
          console.log(new Date().toLocaleTimeString('en-US', {
            hour12: false
          }), 'nettestpp socket new ,', url, 'hardCloseTimer');
          hardCloseTimer = setTimeout(() => {
            console.log(new Date().toLocaleTimeString('en-US', {
              hour12: false
            }), '============================nettestpp hardClose1', socket && socket.readyState);

            if (socket && socket.readyState == 0) {
              console.log(new Date().toLocaleTimeString('en-US', {
                hour12: false
              }), '============================nettestpp hardClose2 hardCloseTimer', socket && socket.readyState, socket && socket.url);
              var _url = socket.url;
              pomelo.disconnect();
              onclose({
                reason: 'hardclose',
                target: {
                  url: _url
                }
              });
              pomelo.emit("socketInitTimeout", socketSid);
            }
          }, 10000);
          socket.binaryType = 'arraybuffer';
          socket.onopen = onopen;
          socket.onmessage = onmessage;
          socket.onerror = onerror;
          socket.onclose = onclose;
        };

        pomelo.disconnect = function (cb) {
          if (!socket) {
            return -1;
          }

          console.log(new Date().toLocaleTimeString('en-US', {
            hour12: false
          }), "---------nettestpp : pomelo.disconnect  ", socket.url, ' socket.readyState = ', socket.readyState); // console.log("----------- pomelo socket status ", socket.readyState)

          disconnectCb = cb;

          if (heartbeatId) {
            clearTimeout(heartbeatId);
            heartbeatId = null;
          }

          if (heartbeatTimeoutId) {
            clearTimeout(heartbeatTimeoutId);
            heartbeatTimeoutId = null;
          }

          if (socket) {
            var _s = socket;
            var _url = _s.url;
            if (socket.disconnect) socket.disconnect();

            if (socket.close) {
              socket.close(); // setTimeout(() => {

              console.log(new Date().toLocaleTimeString('en-US', {
                hour12: false
              }), "---------nettestpp disconnect timeout !socket && _s.readyState  ", !socket && _s.readyState);
              !socket && _s.readyState != 3 && _s.onclose({
                reason: 'hardclose',
                target: {
                  url: _url
                }
              }); // }, 1000);
            } // console.log('disconnect');


            socket = null;
          }
        };

        var reset = function reset() {
          reconnect = false;
          reconnectionDelay = 1000 * 5;
          reconnectAttempts = 0;
          disconnectCb = null;
          clearTimeout(reconncetTimer);
          console.log(new Date().toLocaleTimeString('en-US', {
            hour12: false
          }), 'clear hardCloseTimer');
          clearTimeout(hardCloseTimer);
        };

        pomelo.request = function (route, msg, cb) {
          // console.log('req=>>', route);
          if (arguments.length === 2 && typeof msg === 'function') {
            cb = msg;
            msg = {};
          } else {
            msg = msg || {};
          }

          route = route || msg.route;

          if (!route) {
            return;
          }

          if (cb) {
            msgTimeoutTimer.push(setTimeout(() => {
              console.log("-------------timeout route:", route);
              pomelo.emit('timeout', socketSid);
            }, 5000));
            reqId++;
            callbacks[reqId] = cb;
            routeMap[reqId] = route;
            sendMessage(reqId, route, msg);
          } else {
            sendMessage(0, route, msg);
          }
        };

        pomelo.notify = function (route, msg) {
          msg = msg || {};
          sendMessage(0, route, msg);
        };

        var sendMessage = function sendMessage(reqId, route, msg) {
          console.log("---------nettestpp  sendMessage1", JSON.stringify(route), JSON.stringify(msg));

          if (useCrypto) {
            msg = JSON.stringify(msg);
            var sig = rsa.signString(msg, "sha256");
            msg = JSON.parse(msg);
            msg['__crypto__'] = sig;
          } // console.log("---------nettestpp  sendMessage2",route ,msg);


          if (encode) {
            msg = encode(reqId, route, msg);
          } // console.log("---------nettestpp  sendMessage3",route ,msg);


          var packet = Package.encode(Package.TYPE_DATA, msg);

          if (socket == null || socket.readyState != 1) {
            console.log('warn! nettestpp message would not send ', route, ', because socket == null || socket.readyState != 1');
            return;
          } // console.log("---------nettestpp  sendMessage4",route ,msg);


          send(packet);
        };

        var intervalErrReadyState;

        var send = function send(packet, type) {
          if (type === void 0) {
            type = null;
          }

          //console.log("---------nettestpp  socket send", socket && socket.readyState);
          if (socket == null || socket.readyState != 1) {
            return;
          }

          globalThis.heartBeatStart = Date.now(); //console.log("globalThis.heartBeatStart:", globalThis.heartBeatStart);

          socket.send(packet.buffer);
        };

        var handler = {};

        var heartbeat = function heartbeat(data) {
          if (!heartbeatInterval) {
            return;
          }

          var obj = Package.encode(Package.TYPE_HEARTBEAT);

          if (heartbeatTimeoutId) {
            clearTimeout(heartbeatTimeoutId);
            heartbeatTimeoutId = null;
          }

          if (heartbeatId) {
            return;
          }

          heartbeatId = setTimeout(function () {
            heartbeatId = null;
            send(obj, "heartBeat");
            nextHeartbeatTimeout = Date.now() + heartbeatTimeout;
            heartbeatTimeoutId = setTimeout(heartbeatTimeoutCb, heartbeatTimeout); //修改heartbeat
          }, heartbeatTimeout);
        };

        var heartbeatTimeoutCb = function heartbeatTimeoutCb() {
          var gap = nextHeartbeatTimeout - Date.now();

          if (gap > gapThreshold) {
            heartbeatTimeoutId = setTimeout(heartbeatTimeoutCb, gap);
          } else {
            pomelo.emit('heartbeat timeout');
            var heartObj = {
              lastHeartBeat: globalThis.heartBeatEnd,
              nowTicks: Date.now()
            };
            globalThis.GamePerforAnalysis('netHeartBreak', JSON.stringify(heartObj));
            pomelo.disconnect();
          }
        };

        var handshake = function handshake(data) {
          data = JSON.parse(Protocol.strdecode(data));

          if (data.code === RES_OLD_CLIENT) {
            pomelo.emit('error', 'client version not fullfill');
            return;
          }

          if (data.code !== RES_OK) {
            pomelo.emit('error', 'handshake fail');
            return;
          }

          handshakeInit(data);
          var obj = Package.encode(Package.TYPE_HANDSHAKE_ACK);
          send(obj);

          if (initCallback) {
            initCallback(socket);
          }
        };

        var onData = function onData(data) {
          var msg = data;

          if (decode) {
            msg = decode(msg);
          }

          processMessage(pomelo, msg);
        };

        var onKick = function onKick(data) {
          data = JSON.parse(Protocol.strdecode(data));
          console.log('---------- nettestpp pomelo onkick', data.reason, ',sid = ', data.sid, data.sid == socketSid && 'will kick'); //if (data.sid == socketSid) {

          pomelo.emit('kick', data); //}
        };

        handlers[Package.TYPE_HANDSHAKE] = handshake;
        handlers[Package.TYPE_HEARTBEAT] = heartbeat;
        handlers[Package.TYPE_DATA] = onData;
        handlers[Package.TYPE_KICK] = onKick;

        var processPackage = function processPackage(msgs) {
          if (Array.isArray(msgs)) {
            for (var i = 0; i < msgs.length; i++) {
              var msg = msgs[i];
              handlers[msg.type](msg.body);
            }
          } else {
            handlers[msgs.type](msgs.body);
          }
        };

        var processMessage = function processMessage(pomelo, msg) {
          //console.log('请求详细包', msg.route, msg.body, JSON.stringify(msg));
          if (!msg.id) {
            return pomelo.emit("message", {
              route: msg.route,
              code: 0,
              data: msg.body
            });
          } //if have a id then find the callback function with the request


          var cb = callbacks[msg.id];
          delete callbacks[msg.id];

          if (typeof cb !== 'function') {
            pomelo.emit('message', Object.assign({
              route: msg.route
            }, msg.body));
            return;
          }

          var err = msg.body && msg.body.code && msg.body.code != 200 ? {
            code: msg.body.code,
            message: msg.body.message
          } : null;
          cb(err, msg.body);
          return;
        };

        var processMessageBatch = function processMessageBatch(pomelo, msgs) {
          for (var i = 0, l = msgs.length; i < l; i++) {
            processMessage(pomelo, msgs[i]);
          }
        };

        var deCompose = function deCompose(msg) {
          var isPush = !(msg.id > 0) && msg.route.startsWith('_.');

          if (isPush) {
            console.log('-1-----', msg.route);
            msg.route = msg.route.slice(2);
            console.log('-2-----', msg.route);
          }

          console.log('decompose', isPush, msg.route);
          var route = msg.route; //Decompose route from dict

          try {
            if (msg.compressRoute) {
              if (!abbrs[route]) {
                return {};
              }

              route = msg.route = abbrs[route];
            }

            console.log("globalThis.push_decoder2222", globalThis.push_decoder);

            if (isPush && globalThis.push_decoder && globalThis.push_decoder.lookup(route)) {
              console.log("route", route);
              var um = globalThis.push_decoder.lookupType(route);
              console.log("um", um);
              var d = um.decode(msg.body);
              console.log("d", d);
              console.log('==?==', 'push', route, '-bufio-', JSON.stringify({
                d
              }));
              return um.toObject(d, {
                longs: String,
                enums: String,
                bytes: String,
                defaults: true,
                arrays: true,
                objects: true,
                oneof: true
              });
            } else if (!isPush && globalThis.decodeIO_decoder && globalThis.decodeIO_decoder.lookup(route)) {
              var um = globalThis.decodeIO_decoder.lookupType(route);
              var d = um.decode(msg.body);
              console.log('==?==', 'resp', route, '-bufio-', JSON.stringify({
                d
              }), d);
              return um.toObject(d, {
                longs: String,
                enums: String,
                bytes: String,
                defaults: true,
                arrays: true,
                objects: true,
                oneof: true
              });
            } else {
              console.log('resp', route, '-json-');
              return JSON.parse(Protocol.strdecode(msg.body));
            }
          } catch (ex) {
            console.error('route, body = ' + route + ", " + msg.body, ex);
            console.error('route, body = ' + route + ", " + msg.body, ex, {
              route,
              isPush,
              decodeIO_decoder: typeof globalThis.decodeIO_decoder
            });
          }

          return msg;
        };

        var handshakeInit = function handshakeInit(data) {
          if (data.sys && data.sys.heartbeat) {
            heartbeatInterval = (data.sys.heartbeat - 5) * 1000;
            heartbeatTimeout = data.sys.heartbeat * 1000;
          } else {
            heartbeatInterval = 0;
            heartbeatTimeout = 0;
          }

          initData(data);

          if (typeof handshakeCallback === 'function') {
            handshakeCallback(data.user);
          }
        }; //Initilize data used in pomelo client


        var initData = function initData(data) {
          if (!data || !data.sys) {
            return;
          }

          dict = data.sys.dict;
          var protos = data.sys.protos; //Init compress dict

          if (dict) {
            dict = dict;
            abbrs = {};

            for (var route in dict) {
              abbrs[dict[route]] = route;
            }
          } //Init probufjs protos
          // if (protos) {
          //   protoVersion = protos.version || 0;
          //   serverProtos = protos.server || {};
          //   clientProtos = protos.client || {};
          //   pushProtos = protos.push || {};
          //   //Save probufjs protos to localStorage
          //   cc.localStorage && cc.localStorage.setItem('protos', JSON.stringify(protos));
          //   if (!!protobufjs) {
          //     cc.decodeIO_encoder = protobufjs.Root.fromJSON(clientProtos);
          //     cc.decodeIO_decoder = protobufjs.Root.fromJSON(serverProtos);
          //     cc.push_decoder = protobufjs.Root.fromJSON(pushProtos);
          //   }
          // }

        };
      })();

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=pomelo.js.map
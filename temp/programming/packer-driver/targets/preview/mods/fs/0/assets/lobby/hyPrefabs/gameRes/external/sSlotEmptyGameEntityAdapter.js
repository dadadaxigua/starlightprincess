System.register(["__unresolved_0", "cc", "__unresolved_1"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, director, sGameEntity, _dec, _class, _crd, ccclass, property, sSlotEmptyGameEntityAdapter;

  function _reportPossibleCrUseOfsGameEntity(extras) {
    _reporterNs.report("sGameEntity", "./sGameEntity", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      director = _cc.director;
    }, function (_unresolved_2) {
      sGameEntity = _unresolved_2.sGameEntity;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "40f78Wi8FRI35FvH1weZHOL", "sSlotEmptyGameEntityAdapter", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("sSlotEmptyGameEntityAdapter", sSlotEmptyGameEntityAdapter = (_dec = ccclass('sSlotEmptyGameEntityAdapter'), _dec(_class = class sSlotEmptyGameEntityAdapter extends (_crd && sGameEntity === void 0 ? (_reportPossibleCrUseOfsGameEntity({
        error: Error()
      }), sGameEntity) : sGameEntity) {
        onLoad() {
          super.onLoad();
          director.on('slotEmptySpinClick', () => {
            if (!this.playerBetLimit()) {
              globalThis.webFullScreen && globalThis.webFullScreen();
              this.betActionBtnClick(this.betType);
            }
          }, this);
          director.on('slotEmptyTurboBtnClick', () => {
            this.turboBtnAction();
          }, this);
          director.on('slotEmptyAutoSpinBtnClick', () => {
            this.autoPlayBtnClick();
          }, this);
          director.on('slotEmptyMinusBtnClick', () => {
            this.minusBtnClick();
          }, this);
          director.on('slotEmptyAddBtnClick', () => {
            this.addBtnClick();
          }, this);
          director.on('uiTipsOpen', (mode, tex) => {
            if (mode == 'text') {
              if (globalThis.toast) {
                globalThis.toast(tex, 1);
              }
            }
          }, this);
        }

        autoPlayBtnClick() {
          if (this.betBtnState == 'normal') {
            if (this.betClickMode == 'normal') {
              this.setGameBtnState('disable');
              director.emit('viewChange', 'UIAutoSpin', -1);
            } else if (this.betClickMode == 'autoBet') {
              director.emit('viewChange', 'autoSpinCancel');
            }
          } else {
            if (this.betClickMode == 'autoBet') {
              director.emit('viewChange', 'autoSpinCancel');
            }
          }
        }

        updateBetInfo() {
          super.updateBetInfo();
          director.emit('slotUpdateBetInfo', this.betAmount);
        }

      }) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=sSlotEmptyGameEntityAdapter.js.map
System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2", "__unresolved_3", "__unresolved_4", "__unresolved_5", "__unresolved_6", "__unresolved_7", "__unresolved_8", "__unresolved_9"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, assetManager, Color, director, instantiate, Node, Prefab, ResolutionPolicy, sp, v3, view, sComponent, sAudioMgr, sUtil, sBoxMgr, sObjPool, sBoxAssetInit, soltGameCheckUtil_149, StarlightPrincess_WinTipLabel, StarlightPrincess_Entity, _dec, _dec2, _dec3, _dec4, _dec5, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _temp, _crd, ccclass, property, StarlightPrincess_SceneAdater;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfsComponent(extras) {
    _reporterNs.report("sComponent", "../lobby/game/core/sComponent", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsAudioMgr(extras) {
    _reporterNs.report("sAudioMgr", "../lobby/game/core/sAudioMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsUtil(extras) {
    _reporterNs.report("sUtil", "../lobby/game/core/sUtil", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsBoxMgr(extras) {
    _reporterNs.report("sBoxMgr", "../lobby/game/core/sBoxMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsObjPool(extras) {
    _reporterNs.report("sObjPool", "../lobby/game/core/sObjPool", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsBoxAssetInit(extras) {
    _reporterNs.report("sBoxAssetInit", "../lobby/game/core/sBoxAssetInit", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsoltGameCheckUtil_(extras) {
    _reporterNs.report("soltGameCheckUtil_149", "./soltGameCheckLine_149", _context.meta, extras);
  }

  function _reportPossibleCrUseOfStarlightPrincess_WinTipLabel(extras) {
    _reporterNs.report("StarlightPrincess_WinTipLabel", "./StarlightPrincess_WinTipLabel", _context.meta, extras);
  }

  function _reportPossibleCrUseOfStarlightPrincess_Entity(extras) {
    _reporterNs.report("StarlightPrincess_Entity", "./StarlightPrincess_Entity", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      assetManager = _cc.assetManager;
      Color = _cc.Color;
      director = _cc.director;
      instantiate = _cc.instantiate;
      Node = _cc.Node;
      Prefab = _cc.Prefab;
      ResolutionPolicy = _cc.ResolutionPolicy;
      sp = _cc.sp;
      v3 = _cc.v3;
      view = _cc.view;
    }, function (_unresolved_2) {
      sComponent = _unresolved_2.sComponent;
    }, function (_unresolved_3) {
      sAudioMgr = _unresolved_3.default;
    }, function (_unresolved_4) {
      sUtil = _unresolved_4.sUtil;
    }, function (_unresolved_5) {
      sBoxMgr = _unresolved_5.sBoxMgr;
    }, function (_unresolved_6) {
      sObjPool = _unresolved_6.sObjPool;
    }, function (_unresolved_7) {
      sBoxAssetInit = _unresolved_7.sBoxAssetInit;
    }, function (_unresolved_8) {
      soltGameCheckUtil_149 = _unresolved_8.soltGameCheckUtil_149;
    }, function (_unresolved_9) {
      StarlightPrincess_WinTipLabel = _unresolved_9.StarlightPrincess_WinTipLabel;
    }, function (_unresolved_10) {
      StarlightPrincess_Entity = _unresolved_10.StarlightPrincess_Entity;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "2c880eHBwpCe4TNJRL+L4E0", "StarlightPrincess_SceneAdater", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("StarlightPrincess_SceneAdater", StarlightPrincess_SceneAdater = (_dec = ccclass('StarlightPrincess_SceneAdater'), _dec2 = property(Prefab), _dec3 = property([Node]), _dec4 = property([Node]), _dec5 = property(Node), _dec(_class = (_class2 = (_temp = class StarlightPrincess_SceneAdater extends (_crd && sComponent === void 0 ? (_reportPossibleCrUseOfsComponent({
        error: Error()
      }), sComponent) : sComponent) {
        constructor() {
          super(...arguments);

          _initializerDefineProperty(this, "freebuyNode", _descriptor, this);

          _initializerDefineProperty(this, "normalNodes", _descriptor2, this);

          _initializerDefineProperty(this, "freeWinNodes", _descriptor3, this);

          _initializerDefineProperty(this, "freebuybutton", _descriptor4, this);
        }

        // @property(Label)
        // jackpotLabel : Label;
        onLoad() {
          globalThis.currentPlayingGameID = 149;

          if (!globalThis.boxEntityCtr) {
            globalThis.boxEntityCtr = {};
          }

          globalThis.boxEntityCtr[globalThis.currentPlayingGameID] = _crd && StarlightPrincess_Entity === void 0 ? (_reportPossibleCrUseOfStarlightPrincess_Entity({
            error: Error()
          }), StarlightPrincess_Entity) : StarlightPrincess_Entity;
        }

        start() {
          globalThis.soltGameCheckWon = _crd && soltGameCheckUtil_149 === void 0 ? (_reportPossibleCrUseOfsoltGameCheckUtil_({
            error: Error()
          }), soltGameCheckUtil_149) : soltGameCheckUtil_149;
          (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
            error: Error()
          }), sAudioMgr) : sAudioMgr).AudioInit();
          (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
            error: Error()
          }), sAudioMgr) : sAudioMgr).SetBGVolume(0.5);
          (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
            error: Error()
          }), sAudioMgr) : sAudioMgr).PlayBG('bgm_mg', true);
          view.setDesignResolutionSize(1280, 720, ResolutionPolicy.FIXED_HEIGHT);
          director.on('CorrectResolution', () => {
            view.setDesignResolutionSize(1280, 720, ResolutionPolicy.FIXED_HEIGHT);
          }, this); // director.emit('bonusLabelInit',globalThis.currentPlayingGameID,[this.jackpotLabel]);
          // console.log(this.node.getChildByName('Camera').getComponent(Camera).orthoHeight);
          // sUtil.getSubGameBtnByName(this.bottomPrefab,globalThis.currentPlayingGameID,'subGameBtn_Rist','none',0,'20231011_7',color(251,224,76,255),(subGameBtn : Node)=>{
          //     if(subGameBtn){
          //         subGameBtn.setParent(this.node);
          //         director.emit('getSubGameBtnByPathEnd');
          //     };
          // });
          // sUtil.getSubGameBtnByPath('subGameBtn_StarlightPrincess',globalThis.currentPlayingGameID,'none',0,'20230726_4',Color.WHITE,subGameBtn=>{
          //     if(subGameBtn){
          //         subGameBtn.setParent(this.node);
          //         this.SetNodeFront(subGameBtn);
          //         director.emit('getSubGameBtnByPathEnd');
          //     };
          // });

          var bundle = assetManager.getBundle('149');

          if (bundle) {
            console.log('bundle');
            bundle.load('subGameBtn_Starlight', Prefab, (err, _prefab) => {
              console.log('err:', JSON.stringify(err));
              console.log('_prefab', _prefab);

              if (_prefab) {
                console.log('_prefab');
                var obj = instantiate(_prefab);

                if (obj) {
                  obj['gameid'] = 149;
                  obj['aligmentType'] = 'none';
                  obj['aligmentPixel'] = 0;
                  obj['gameVersion'] = '20231019_5';
                  obj['customColor'] = Color.WHITE;

                  if (_prefab) {
                    console.log('加载btn成功');
                    obj.setParent(this.node);
                    this.SetNodeFront(obj);
                    director.emit('getSubGameBtnByPathEnd');
                  } else {
                    console.log('加载btn失败');
                  }

                  ;
                }
              }
            });
          }

          ; //    console.log('StarlightPrincess_SceneAdater');
          //     try { 
          //         sUtil.getSubGameBtnByPath('subGameBtn_toa', globalThis.currentPlayingGameID, 'none', 0, '20231018_7', Color.WHITE, subGameBtn => {
          //                     if (subGameBtn) {
          //                         console.log('加载btn1成功');
          //                         subGameBtn.setParent(this.node);
          //                         this.SetNodeFront(subGameBtn);
          //                         director.emit('getSubGameBtnByPathEnd');
          //                     } else {
          //                         console.log('加载btn1失败');
          //                     };
          //         });
          //     } catch (error) {
          //         console.log('加载错误1' + error);
          //     };

          director.on('freeWinBegain', () => {
            this.scheduleOnce(() => {
              for (var i = 0; i < this.normalNodes.length; i++) {
                var element = this.normalNodes[i];
                element.active = false;
              }

              ;

              for (var _i = 0; _i < this.freeWinNodes.length; _i++) {
                var _element = this.freeWinNodes[_i];
                _element.active = true;

                if (_element.name == 'stpr_freegame_idle') {
                  console.log('免费模式地图', _element);

                  _element.getComponent(sp.Skeleton).setAnimation(0, 'stpr_freegame_idle', true);
                }

                ;
              }

              ;
            }, 0.6);
          }, this);
          director.on('freeWinOver', () => {
            for (var i = 0; i < this.normalNodes.length; i++) {
              var element = this.normalNodes[i];
              element.active = true;
            }

            ;

            for (var _i2 = 0; _i2 < this.freeWinNodes.length; _i2++) {
              var _element2 = this.freeWinNodes[_i2];
              _element2.active = false;
            }

            ;
          }, this);
          director.on('uiTipsOpen', (mode, tex) => {
            (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
              error: Error()
            }), sUtil) : sUtil).toast(tex);
          }, this);
          director.on('slotWinBoxItemInfo', obj => {
            if (obj) {
              //console.log('boxPos:',obj.boxPos);
              if (obj.boxPos) {
                var posStr = obj.boxPos.split('_');

                if (posStr && posStr.length == 2) {
                  var boxPos = (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                    error: Error()
                  }), sBoxMgr) : sBoxMgr).instance.getBoxWorldPosByXY(parseInt(posStr[0]), parseInt(posStr[1]));

                  if (boxPos) {
                    var boxWinTip = (_crd && sObjPool === void 0 ? (_reportPossibleCrUseOfsObjPool({
                      error: Error()
                    }), sObjPool) : sObjPool).Dequeue('boxWinTipLabel');

                    if (boxWinTip && boxWinTip.isValid) {
                      boxWinTip.parent = (_crd && sBoxAssetInit === void 0 ? (_reportPossibleCrUseOfsBoxAssetInit({
                        error: Error()
                      }), sBoxAssetInit) : sBoxAssetInit).instance.getTargetNodeByName('effectLayer');
                      boxWinTip.worldPosition = v3(boxPos.x, boxPos.y - 48, 0);
                      boxWinTip.active = true;
                      boxWinTip.getComponent(_crd && StarlightPrincess_WinTipLabel === void 0 ? (_reportPossibleCrUseOfStarlightPrincess_WinTipLabel({
                        error: Error()
                      }), StarlightPrincess_WinTipLabel) : StarlightPrincess_WinTipLabel).play(this.AddCommas(obj.rate * globalThis.GameBtnEntity.CurrentBetAmount));
                    }
                  }
                }
              }
            }
          }, this);
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "freebuyNode", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "normalNodes", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return [];
        }
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "freeWinNodes", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return [];
        }
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "freebuybutton", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=StarlightPrincess_SceneAdater.js.map
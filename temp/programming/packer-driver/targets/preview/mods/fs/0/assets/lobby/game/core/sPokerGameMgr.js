System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, Node, director, sComponent, sPokerPlayerEntity, _dec, _dec2, _class, _class2, _descriptor, _temp, _crd, ccclass, property, sPokerGameMgr;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfsComponent(extras) {
    _reporterNs.report("sComponent", "./sComponent", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsPokerCardBase(extras) {
    _reporterNs.report("sPokerCardBase", "./sPokerCardBase", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsPokerPlayerEntity(extras) {
    _reporterNs.report("sPokerPlayerEntity", "./sPokerPlayerEntity", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      Node = _cc.Node;
      director = _cc.director;
    }, function (_unresolved_2) {
      sComponent = _unresolved_2.sComponent;
    }, function (_unresolved_3) {
      sPokerPlayerEntity = _unresolved_3.sPokerPlayerEntity;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "a6c0e4vpBFAt7L1VJfXhG2p", "sPokerGameMgr", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("sPokerGameMgr", sPokerGameMgr = (_dec = ccclass('sPokerGameMgr'), _dec2 = property([Node]), _dec(_class = (_class2 = (_temp = class sPokerGameMgr extends (_crd && sComponent === void 0 ? (_reportPossibleCrUseOfsComponent({
        error: Error()
      }), sComponent) : sComponent) {
        constructor() {
          super(...arguments);

          _initializerDefineProperty(this, "pokerPlayerNodes", _descriptor, this);

          _defineProperty(this, "pokerPlayerEntities", []);

          _defineProperty(this, "joinGamePokerPlayerEntities", []);

          _defineProperty(this, "publicPokerCards", []);

          _defineProperty(this, "selfPokerPlayerEntity", void 0);

          _defineProperty(this, "lastDiscardUser", void 0);

          _defineProperty(this, "selfUserSeatPos", 0);

          _defineProperty(this, "lastCardPoints", void 0);

          _defineProperty(this, "deskClientInfo", null);
        }

        start() {
          if (this.pokerPlayerNodes) {
            for (var i = 0; i < this.pokerPlayerNodes.length; i++) {
              var element = this.pokerPlayerNodes[i];
              var pokerEntity = element.getComponent(_crd && sPokerPlayerEntity === void 0 ? (_reportPossibleCrUseOfsPokerPlayerEntity({
                error: Error()
              }), sPokerPlayerEntity) : sPokerPlayerEntity);

              if (pokerEntity) {
                if (i == 0) {
                  this.selfPokerPlayerEntity = pokerEntity;
                }

                this.pokerPlayerEntities.push(element.getComponent(_crd && sPokerPlayerEntity === void 0 ? (_reportPossibleCrUseOfsPokerPlayerEntity({
                  error: Error()
                }), sPokerPlayerEntity) : sPokerPlayerEntity));
              } else {
                console.error('pokerPlayerEntities bind error!!!');
              }
            }
          }

          director.on('pokerLeaveSuc', () => {
            director.emit('GameLeaveDesk');
          }, this);
          director.on('socketClosedToGame', type => {
            console.log('socketClosedToGame:' + type);

            if (type == 1) {
              globalThis.Activerecon();
            } else if (type == 2) {} else if (type == 3) {
              globalThis.Activerecon();
            }
          }, this);
          director.on('socketConnectToGame', type => {
            console.log('socketConnectToGame:' + type);

            if (type == 1) {
              this.cleanTable();
              this.reloadTable();
            } else if (type == 2) {
              globalThis.Activerecon();
            }
          }, this);
          director.on('pokerOnGameStart', res => {
            if (res && res.data) {
              this.gameStart(res);
            }
          }, this);
          director.on('pokerStartCd', res => {
            if (res && res.data) {
              this.gameStartCd();
            }
          }, this);
          director.on('pokerOnReady', res => {
            if (res.data.pos) {
              var player = this.getVierPlayer(res.data.pos);

              if (player) {
                player.ReadyAction(1);
              }
            }
          }, this);
          director.on('pokerTurnStart', res => {
            if (res.data.pos) {
              var player = this.getVierPlayer(res.data.pos);

              if (player == this.selfPokerPlayerEntity) {
                this.turnStartSelfPlayer();
              }

              if (player) {
                player.UserGetTurn();
              }
            }
          }, this);
          director.on('pokerTurnAction', res => {
            if (res.data.pos) {
              res.data.rate = parseInt(this.deskClientInfo.base_coin);
              this.lastCardPoints = res.data.pokers;
              var player = this.getVierPlayer(res.data.pos);

              if (player) {
                player.UserTurnAction(res, this.lastDiscardUser);
              }

              if (player == this.selfPokerPlayerEntity) {
                this.turnActionSelfPlayer(res);
              } else {
                this.turnActionOtherPlayer(res);
              }
            }
          }, this);
          director.on('pokerDraw', res => {
            var player = this.selfPokerPlayerEntity;

            if (player) {
              player.UserDrawCard(res);
            }
          }, this);
          director.on('pokerEnterDesk', res => {
            this.playerEnterDesk(res);
          }, this);
          director.on('pokerLeaveDesk', res => {
            this.playerLeaveDesk(res);
          }, this);
          director.on('pokerGameEnd', res => {
            this.gameEnd(res);
          }, this);
          director.on('pokerCleanTable', () => {
            this.cleanTable();
          }, this);
          director.on('pokerSetAutoMode', res => {
            var player = this.getVierPlayer(res.data.pos);

            if (player) {
              player.SetAutoPlayMode(res.data.state == 1 ? true : false);
            }

            this.setAutoPlayMode(res);
          }, this);
        }

        playerEnterDesk(res) {
          if (res && res.data && res.data.user) {
            if (!this.isSelfUser(res.data.user.uid)) {
              var seat = this.getViewSeatPos(res.data.user.pos);

              if (seat >= 0 && seat < this.pokerPlayerEntities.length) {
                this.pokerPlayerEntities[seat].UserData = res.data.user;
                this.pokerPlayerEntities[seat].UserInfoView(true);
              } else {
                console.error('pokerPlayerEntities error:' + seat);
              }
            }
          }
        }

        playerLeaveDesk(res) {
          if (res.data.uid) {
            for (var i = 0; i < this.pokerPlayerEntities.length; i++) {
              var player = this.pokerPlayerEntities[i];

              if (player) {
                var userdata = player.UserData;

                if (userdata && userdata.uid == res.data.uid) {
                  player.LeaveDesk(res.data.reason);
                }
              }
            }
          }
        }

        isSelfUser(uid) {
          var user = globalThis.getUserInfo();

          if (user) {
            if (user.uid == uid) {
              return true;
            }
          }

          return false;
        }

        getViewSeatPos(othSeatPos) {
          var seat = othSeatPos - this.selfUserSeatPos;

          if (seat < 0) {
            seat += this.pokerPlayerNodes.length;
          }

          return seat;
        }

        getVierPlayer(seatPos) {
          var seat = this.getViewSeatPos(seatPos);

          if (seat >= 0 && seat < this.pokerPlayerEntities.length) {
            return this.pokerPlayerEntities[seat];
          }

          return null;
        }

        gameStart(res) {
          for (var i = 0; i < this.pokerPlayerEntities.length; i++) {
            var player = this.pokerPlayerEntities[i];
            player.GameStart();
          }
        }

        gameStartCd() {}

        reloadTable() {}

        cleanTable() {
          this.unscheduleAllCallbacks();

          for (var i = 0; i < this.pokerPlayerEntities.length; i++) {
            var player = this.pokerPlayerEntities[i];
            player.CleanTable();
          }
        }

        game(res) {
          for (var i = 0; i < this.pokerPlayerEntities.length; i++) {
            var player = this.pokerPlayerEntities[i];
            player.GameStart();
          }
        }

        turnStartSelfPlayer() {}

        turnActionSelfPlayer(res) {}

        turnActionOtherPlayer(res) {}

        setAutoPlayMode(res) {}

        gameEnd(res) {
          this.lastCardPoints = null;
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "pokerPlayerNodes", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return [];
        }
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=sPokerGameMgr.js.map
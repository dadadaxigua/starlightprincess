System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2", "__unresolved_3"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, AudioClip, director, Label, Node, sp, tween, UIOpacity, sComponent, sAudioMgr, sUtil, _dec, _dec2, _dec3, _dec4, _dec5, _dec6, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _descriptor5, _temp, _crd, ccclass, property, sJackPotEffect;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfsComponent(extras) {
    _reporterNs.report("sComponent", "../../../../game/core/sComponent", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsAudioMgr(extras) {
    _reporterNs.report("sAudioMgr", "../../../../game/core/sAudioMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsUtil(extras) {
    _reporterNs.report("sUtil", "../../../../game/core/sUtil", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      AudioClip = _cc.AudioClip;
      director = _cc.director;
      Label = _cc.Label;
      Node = _cc.Node;
      sp = _cc.sp;
      tween = _cc.tween;
      UIOpacity = _cc.UIOpacity;
    }, function (_unresolved_2) {
      sComponent = _unresolved_2.sComponent;
    }, function (_unresolved_3) {
      sAudioMgr = _unresolved_3.default;
    }, function (_unresolved_4) {
      sUtil = _unresolved_4.sUtil;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "bbeaeDK4FRN0bC9fB8s/jKp", "sJackPotEffect", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("sJackPotEffect", sJackPotEffect = (_dec = ccclass('sJackPotEffect'), _dec2 = property(Node), _dec3 = property(sp.Skeleton), _dec4 = property(UIOpacity), _dec5 = property(Label), _dec6 = property({
        tooltip: '音频的资源引用',
        type: [AudioClip]
      }), _dec(_class = (_class2 = (_temp = class sJackPotEffect extends (_crd && sComponent === void 0 ? (_reportPossibleCrUseOfsComponent({
        error: Error()
      }), sComponent) : sComponent) {
        constructor() {
          super(...arguments);

          _initializerDefineProperty(this, "mainNode", _descriptor, this);

          _initializerDefineProperty(this, "spineAnima", _descriptor2, this);

          _initializerDefineProperty(this, "maskOpa", _descriptor3, this);

          _initializerDefineProperty(this, "coinLabel", _descriptor4, this);

          _initializerDefineProperty(this, "gameAudioClip", _descriptor5, this);

          _defineProperty(this, "touchEnable", false);

          _defineProperty(this, "coinValueEnd", 0);
        }

        start() {
          // this.scheduleOnce(()=>{
          //     director.emit('slotJackPotBingo',1004223);
          // },2);
          if (this.gameAudioClip && this.gameAudioClip.length) {
            for (var i = 0; i < this.gameAudioClip.length; i++) {
              var clip = this.gameAudioClip[i];
              (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                error: Error()
              }), sAudioMgr) : sAudioMgr).SetAudioClip(clip);
            }
          }

          this.maskOpa.node.on('click', () => {
            if (this.touchEnable) {
              this.touchEnable = false;
              this.coinLabel.string = this.AddCommas(this.coinValueEnd);
              this.cleanTweenList();
              this.spineAnima.setCompleteListener(null);
              this.scheduleOnce(() => {
                this.coinLabel.string = '';
                this.viewClose();
              }, 3);
            }
          }, this);
          director.on('slotJackPotBingo', coinValue => {
            if (coinValue > 0) {
              (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                error: Error()
              }), sAudioMgr) : sAudioMgr).PlayShotAudio('jackpot');
              this.SetUIFront();
              this.coinValueEnd = coinValue;
              this.cleanTweenList();
              this.mainNode.active = true;
              this.maskOpa.opacity = 0;
              this.pushOneTween(tween(this.maskOpa).to(0.3, {
                opacity: 255
              }).start());
              this.spineAnima.node.active = true;
              this.spineAnima.setAnimation(0, 'a1', false);
              this.spineAnima.setCompleteListener(() => {
                this.spineAnima.setAnimation(0, 'a2', true);
                this.touchEnable = true;
              });
              this.coinLabel.string = '0';
              this.pushOneTween((_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                error: Error()
              }), sUtil) : sUtil).TweenLabel(0, coinValue, this.coinLabel, 5, 0, 'quadOut', 0, () => {
                this.touchEnable = false;
                this.scheduleOnce(() => {
                  this.coinLabel.string = '';
                  this.viewClose();
                }, 3);
              }));
            } else {
              director.emit('slotJackPotEtClose');
            }
          }, this);
        }

        viewClose() {
          this.cleanTweenList();
          this.pushOneTween(tween(this.maskOpa).to(0.3, {
            opacity: 0
          }).call(() => {
            this.mainNode.active = false;
          }).start());
          this.spineAnima.setCompleteListener(null);
          this.spineAnima.node.active = false;
          director.emit('slotJackPotEtClose');
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "mainNode", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "spineAnima", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "maskOpa", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "coinLabel", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor5 = _applyDecoratedDescriptor(_class2.prototype, "gameAudioClip", [_dec6], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return [];
        }
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=sJackPotEffect.js.map
System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, UIOpacity, tween, sComponent, sObjPool, _dec, _dec2, _class, _class2, _descriptor, _temp, _crd, ccclass, property, sPokerChip;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfsComponent(extras) {
    _reporterNs.report("sComponent", "./sComponent", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsObjPool(extras) {
    _reporterNs.report("sObjPool", "./sObjPool", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      UIOpacity = _cc.UIOpacity;
      tween = _cc.tween;
    }, function (_unresolved_2) {
      sComponent = _unresolved_2.sComponent;
    }, function (_unresolved_3) {
      sObjPool = _unresolved_3.sObjPool;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "96035vvU8VHa4i9Kvot4XH9", "sPokerChip", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("sPokerChip", sPokerChip = (_dec = ccclass('sPokerChip'), _dec2 = property(UIOpacity), _dec(_class = (_class2 = (_temp = class sPokerChip extends (_crd && sComponent === void 0 ? (_reportPossibleCrUseOfsComponent({
        error: Error()
      }), sComponent) : sComponent) {
        constructor() {
          super(...arguments);

          _initializerDefineProperty(this, "chipOpa", _descriptor, this);
        }

        start() {}

        MoveChip(animaTime, targetPos, delayTime, startCall, onComplete) {
          if (startCall === void 0) {
            startCall = null;
          }

          if (onComplete === void 0) {
            onComplete = null;
          }

          this.removeIDTween('move');
          this.pushIDTween(tween(this.node).delay(delayTime).call(() => {
            if (startCall) {
              startCall();
            }
          }).to(animaTime, {
            worldPosition: targetPos
          }, {
            easing: 'cubicOut'
          }).call(onComplete).start(), 'move');
        }

        FlipChip(animaTime, opa, delayTime, startOpa, _onComplete) {
          if (startOpa === void 0) {
            startOpa = -1;
          }

          if (_onComplete === void 0) {
            _onComplete = null;
          }

          this.removeIDTween('flip');

          if (startOpa != -1) {
            this.chipOpa.opacity = startOpa;
          }

          this.pushIDTween(tween(this.chipOpa).delay(delayTime).to(animaTime, {
            opacity: opa
          }, {
            easing: 'cubicOut',
            'onComplete': () => {
              if (_onComplete) {
                _onComplete();
              }
            }
          }).start(), 'flip');
        }

        RecycleObj() {
          this.removeIDTween('flip');
          this.removeIDTween('move');

          if (this.chipOpa) {
            this.chipOpa.opacity = 255;
          }

          this.node.active = false;
          this.node.parent = null;
          (_crd && sObjPool === void 0 ? (_reportPossibleCrUseOfsObjPool({
            error: Error()
          }), sObjPool) : sObjPool).Enqueue(this.node.name, this.node);
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "chipOpa", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=sPokerChip.js.map
System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2", "__unresolved_3", "__unresolved_4", "__unresolved_5"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, color, Color, director, Label, Node, sp, Sprite, UITransform, v3, sBoxItemBase, assetMgr, sObjPool, sConfigMgr, sUtil, _dec, _dec2, _dec3, _dec4, _dec5, _dec6, _dec7, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _descriptor5, _descriptor6, _temp, _crd, ccclass, property, StarlightPrincess_BoxItem;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfsBoxItemBase(extras) {
    _reporterNs.report("sBoxItemBase", "../lobby/game/core/sBoxItemBase", _context.meta, extras);
  }

  function _reportPossibleCrUseOfassetMgr(extras) {
    _reporterNs.report("assetMgr", "../lobby/game/core/sAssetMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsObjPool(extras) {
    _reporterNs.report("sObjPool", "../lobby/game/core/sObjPool", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsConfigMgr(extras) {
    _reporterNs.report("sConfigMgr", "../lobby/game/core/sConfigMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsUtil(extras) {
    _reporterNs.report("sUtil", "../lobby/game/core/sUtil", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      color = _cc.color;
      Color = _cc.Color;
      director = _cc.director;
      Label = _cc.Label;
      Node = _cc.Node;
      sp = _cc.sp;
      Sprite = _cc.Sprite;
      UITransform = _cc.UITransform;
      v3 = _cc.v3;
    }, function (_unresolved_2) {
      sBoxItemBase = _unresolved_2.sBoxItemBase;
    }, function (_unresolved_3) {
      assetMgr = _unresolved_3.assetMgr;
    }, function (_unresolved_4) {
      sObjPool = _unresolved_4.sObjPool;
    }, function (_unresolved_5) {
      sConfigMgr = _unresolved_5.sConfigMgr;
    }, function (_unresolved_6) {
      sUtil = _unresolved_6.sUtil;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "d98c4K2k8NEjZuFK+++V/Yk", "StarlightPrincess_BoxItem", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("StarlightPrincess_BoxItem", StarlightPrincess_BoxItem = (_dec = ccclass('StarlightPrincess_BoxItem'), _dec2 = property(Sprite), _dec3 = property(UITransform), _dec4 = property(Label), _dec5 = property(Node), _dec6 = property(Node), _dec7 = property(Node), _dec(_class = (_class2 = (_temp = class StarlightPrincess_BoxItem extends (_crd && sBoxItemBase === void 0 ? (_reportPossibleCrUseOfsBoxItemBase({
        error: Error()
      }), sBoxItemBase) : sBoxItemBase) {
        constructor() {
          super(...arguments);

          _initializerDefineProperty(this, "renderSprite", _descriptor, this);

          _initializerDefineProperty(this, "renderTransform", _descriptor2, this);

          _initializerDefineProperty(this, "multiLabel", _descriptor3, this);

          _initializerDefineProperty(this, "SpineLayer", _descriptor4, this);

          _initializerDefineProperty(this, "winFrame", _descriptor5, this);

          _initializerDefineProperty(this, "disappearAnimation", _descriptor6, this);

          _defineProperty(this, "skeleton", void 0);

          _defineProperty(this, "isThundered", false);

          _defineProperty(this, "isMultied", false);
        }

        boxItemUpdate(renderData, symbolValue, boxState, tableState) {
          if (renderData) {
            if (renderData.type == 'img') {
              this.imgRenderSet(boxState, symbolValue, renderData, tableState);
            } else if (renderData.type == 'spine') {
              this.spineRenderSet(boxState, symbolValue, renderData, tableState);
            }

            ;

            if (symbolValue > 2000) {
              if (renderData.multi && !this.isMultied) {
                this.multiLabel.node.active = true;
                this.SetNodeFront(this.multiLabel.node);
                this.multiLabel.string = renderData.multi + 'x'; //let pos=this.multiLabel.node.position;
                //this.multiLabel.node.setPosition(pos.x,pos.y+2,pos.z);
              }

              ;
            } else {
              this.isMultied = false;
            }

            ;

            if (boxState == 'win') {
              if (symbolValue > 2000) {
                this.isMultied = true;
                director.emit('winMultiBox', {
                  actionType: tableState,
                  pos: this.multiLabel.node.worldPosition,
                  multi: renderData.multi,
                  fontSize: this.multiLabel.fontSize
                });
                this.multiLabel.node.active = false;
              } else {
                this.isMultied = false;

                if (symbolValue != 1) {
                  this.winFrame.active = true;
                  this.winFrame.getComponent(Sprite).color = color(255, 255, 255, 0);
                  (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                    error: Error()
                  }), sUtil) : sUtil).TweenColor(color(255, 255, 255, 255), this.winFrame.getComponent(Sprite), 0.2, 0);
                  this.scheduleOnce(() => {
                    this.winFrame.active = false;
                    this.scheduleOnce(() => {
                      this.disappearAnimation.active = true;
                      this.disappearAnimation;
                      this.disappearAnimation.getComponent(sp.Skeleton).setAnimation(0, "stpr_symbol_all_fx_disappearance", false);
                      this.disappearAnimation.getComponent(sp.Skeleton).setCompleteListener(() => {
                        this.disappearAnimation.active = false;
                      });
                      var boxDatab = this.getSymbolData(symbolValue);
                      renderData = boxDatab['out'];
                      this.skeleton.timeScale = 2.5;
                      this.skeleton.setAnimation(0, renderData.animation_name, false);
                      this.skeleton.setCompleteListener(() => {
                        this.skeleton.node.active = false;
                      });
                    }, 0.2);
                  }, 1.4); // this.scheduleOnce(()=>{
                  // },0.2);
                }

                ;
              }

              ;
            } else {
              this.winFrame.active = false;
            }

            ;
          }

          ; // if(this.isMultied){f
          //     if(boxState == 'rolling'){
          //         this.isMultied = false;
          //     }
          // }
        }

        imgRenderSet(boxState, symbolValue, renderData, tableState) {
          var renderSize = null;
          var spriteFrame = (_crd && assetMgr === void 0 ? (_reportPossibleCrUseOfassetMgr({
            error: Error()
          }), assetMgr) : assetMgr).GetAssetByName(renderData.render_name);
          this.renderSprite.node.active = true;
          this.renderSprite.spriteFrame = spriteFrame; // renderSize = size(spriteFrame.rect.width,spriteFrame.rect.height);
          // console.log('spriteFrame:'+spriteFrame.name +','+renderSize.x+','+renderSize.y);

          if (renderData.position) {
            this.renderSprite.node.position = v3(renderData.position.x, renderData.position.y, 0);
          }

          ;

          if (renderData.contentSize) {// this.renderTransform.contentSize = size(renderData.contentSize.x,renderData.contentSize.y);
          } else {
            if (renderSize) {// this.renderTransform.contentSize = renderSize;
            }

            ;
          }

          ;

          if (renderData.scale) {
            this.renderSprite.node.scale = v3(renderData.scale, renderData.scale, renderData.scale);
          }

          ; // let pos=this.multiLabel.node.position;
          // this.multiLabel.node.setPosition(pos.x,pos.y+2,pos.z);

          if (boxState == 'win') {// this.renderSprite.node.scale = v3(2,2,2);
          }

          ;
        }

        spineRenderSet(rollState, symbolValue, renderData, tableState) {
          if (this.skeleton) {
            this.skeleton.node.active = false;
            (_crd && sObjPool === void 0 ? (_reportPossibleCrUseOfsObjPool({
              error: Error()
            }), sObjPool) : sObjPool).Enqueue(renderData.render_name, this.skeleton);
            this.skeleton = null;
          }

          ;
          this.skeleton = (_crd && sObjPool === void 0 ? (_reportPossibleCrUseOfsObjPool({
            error: Error()
          }), sObjPool) : sObjPool).Dequeue(renderData.render_name);

          if (this.skeleton) {
            this.skeleton.node.setParent(this.SpineLayer, false);

            if (renderData.loop && renderData.loop == -1) {
              this.skeleton.loop = true;
            } else {
              this.skeleton.loop = false;
            }

            ;

            if (symbolValue > 2000 && rollState == 'win') {
              this.skeleton.getComponent(sp.Skeleton).timeScale = 0.8;
            } else {
              this.skeleton.getComponent(sp.Skeleton).timeScale = 1;
            }

            ;

            if (this.skeleton.animation == 'out') {//this.skeleton.getComponent(sp.Skeleton).timeScale=0.1;
            }

            ;

            if (renderData.position) {
              this.skeleton.node.position = v3(renderData.position.x, renderData.position.y, 0);
            }

            ; // if(renderData.scale){
            //     if(this.skeleton.node.scale.x != renderData.scale){
            //         this.skeleton.node.scale = v3(renderData.scale,renderData.scale,1);
            //     }
            // }else{
            //     if(this.skeleton.node.scale.x != 1){
            //         this.skeleton.node.scale = v3(1,1,1);
            //     }
            // }

            this.skeleton.node.active = true; // if(renderData.timeScale){
            //     this.skeleton.timeScale = renderData.timeScale;
            // };

            this.skeleton.animation = renderData.animation_name;

            if (symbolValue <= 2000 && symbolValue != 1) {
              if (this.skeleton.animation == 'out') {
                this.skeleton.setCompleteListener(() => {
                  this.skeleton.node.active = false;
                });
              }

              ;
            }

            ;
          }

          this.renderSprite.node.active = false;
        }

        clearItem(symbolValue, rollState, tableState) {
          this.cleanTweenList();
          this.multiLabel.node.active = false;
          this.renderSprite.color = Color.WHITE;

          if (this.skeleton) {
            this.skeleton.setCompleteListener(null);
            this.skeleton.node.active = false;
            (_crd && sObjPool === void 0 ? (_reportPossibleCrUseOfsObjPool({
              error: Error()
            }), sObjPool) : sObjPool).Enqueue(this.skeleton.node.name, this.skeleton);
            this.skeleton = null;
          }

          ;
        }

        getSymbolData(symbolValue) {
          try {
            if (isNaN(symbolValue)) {
              return (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
                error: Error()
              }), sConfigMgr) : sConfigMgr).instance.GetSymbolBoxImg(symbolValue.OLD);
            } else {
              return (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
                error: Error()
              }), sConfigMgr) : sConfigMgr).instance.GetSymbolBoxImg(symbolValue);
            }

            ;
          } catch (e) {
            console.error('getSymbolData error' + e);
          }

          ;
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "renderSprite", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "renderTransform", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "multiLabel", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "SpineLayer", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor5 = _applyDecoratedDescriptor(_class2.prototype, "winFrame", [_dec6], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor6 = _applyDecoratedDescriptor(_class2.prototype, "disappearAnimation", [_dec7], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=StarlightPrincess_BoxItem.js.map
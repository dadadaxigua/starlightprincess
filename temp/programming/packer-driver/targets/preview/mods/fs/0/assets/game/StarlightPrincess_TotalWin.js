System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2", "__unresolved_3"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, director, Label, Node, sp, tween, UIOpacity, v3, sAudioMgr, UIBase, sUtil, _dec, _dec2, _dec3, _dec4, _dec5, _dec6, _dec7, _dec8, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _descriptor5, _descriptor6, _descriptor7, _temp, _crd, ccclass, property, StarlightPrincess_TotalWin;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfsAudioMgr(extras) {
    _reporterNs.report("sAudioMgr", "../lobby/game/core/sAudioMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfUIBase(extras) {
    _reporterNs.report("UIBase", "../lobby/game/core/UIBase", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsUtil(extras) {
    _reporterNs.report("sUtil", "../lobby/game/core/sUtil", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      director = _cc.director;
      Label = _cc.Label;
      Node = _cc.Node;
      sp = _cc.sp;
      tween = _cc.tween;
      UIOpacity = _cc.UIOpacity;
      v3 = _cc.v3;
    }, function (_unresolved_2) {
      sAudioMgr = _unresolved_2.default;
    }, function (_unresolved_3) {
      UIBase = _unresolved_3.UIBase;
    }, function (_unresolved_4) {
      sUtil = _unresolved_4.sUtil;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "be9ae2E+HpIFIfONvyWtCdy", "StarlightPrincess_TotalWin", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("StarlightPrincess_TotalWin", StarlightPrincess_TotalWin = (_dec = ccclass('StarlightPrincess_TotalWin'), _dec2 = property(Node), _dec3 = property(Label), _dec4 = property(Label), _dec5 = property(Node), _dec6 = property(sp.Skeleton), _dec7 = property(sp.Skeleton), _dec8 = property({
        type: [Node]
      }), _dec(_class = (_class2 = (_temp = class StarlightPrincess_TotalWin extends (_crd && UIBase === void 0 ? (_reportPossibleCrUseOfUIBase({
        error: Error()
      }), UIBase) : UIBase) {
        constructor() {
          super(...arguments);

          _initializerDefineProperty(this, "freeBtn", _descriptor, this);

          _initializerDefineProperty(this, "numLabel", _descriptor2, this);

          _initializerDefineProperty(this, "freenum", _descriptor3, this);

          _initializerDefineProperty(this, "contentNode", _descriptor4, this);

          _initializerDefineProperty(this, "popSpine", _descriptor5, this);

          _initializerDefineProperty(this, "thunderSpine", _descriptor6, this);

          _initializerDefineProperty(this, "LabelNode", _descriptor7, this);

          _defineProperty(this, "touchEnable", true);
        }

        start() {
          this.freeBtn.on('click', () => {
            if (this.touchEnable) {
              (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                error: Error()
              }), sAudioMgr) : sAudioMgr).PlayShotAudio('thunder');
              this.cleanTweenList();
              this.touchEnable = false;
              (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                error: Error()
              }), sAudioMgr) : sAudioMgr).PlayShotAudio('freeWinTransitBtn');
              this.popSpine.setAnimation(0, 'stpr_banner_out', false);
              this.scheduleOnce(() => {
                this.playThunderAnima();
              }, 0.4);

              for (var jindex = 0; jindex < this.LabelNode.length; jindex++) {
                this.LabelNode[jindex].active = true;
                this.pushIDTween(tween(this.LabelNode[jindex].getComponent(UIOpacity)).to(0.4, {
                  opacity: 0
                }).start(), 'LabelNode2');
              }

              ;
              this.popSpine.setCompleteListener(() => {
                this.popSpine.node.active = false;
                this.contentNode.active = false;
                this.scheduleOnce(() => {
                  director.emit('freeWinOver');
                  director.emit('rollStop');
                  (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                    error: Error()
                  }), sAudioMgr) : sAudioMgr).StopBGAudio();
                  (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                    error: Error()
                  }), sAudioMgr) : sAudioMgr).PlayBG('bgm_mg'); // this.scheduleOnce(()=>{
                  //     this.touchEnable=true;
                  //     director.emit('StarlightTotleViewOpen',800000,13);
                  // },2)
                }, 1);
              });
            }
          }, this);
          director.on('StarlightTotleViewOpen', (coin, num) => {
            var demc = -1;

            if (globalThis.getCoinRate && globalThis.getCoinRate() > 1) {
              demc = 2;
            }

            ;
            (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
              error: Error()
            }), sAudioMgr) : sAudioMgr).bgMusicOut(0.4, 0);
            this.contentNode.active = true;
            this.freenum.string = num; //this.popSpine.timeScale = 2.5;

            this.popSpine.node.active = true;
            this.popSpine.setAnimation(0, 'stpr_banner_in', false);
            (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
              error: Error()
            }), sAudioMgr) : sAudioMgr).PlayShotAudio('freegame_end');
            (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
              error: Error()
            }), sAudioMgr) : sAudioMgr).PlayShotAudio('freegame_voice_congratulations'); //this.scheduleOnce(()=>{

            var fs_Node = this.LabelNode[3];
            var fslabel = this.LabelNode[4];

            if (globalThis.GetLanguageType() == 'EN') {
              fslabel.position = v3(-103, -151, 0);
            } else if (globalThis.GetLanguageType() == 'VN') {
              fslabel.position = v3(-164, -154, 0);
            } else if (globalThis.GetLanguageType() == 'PT') {
              fslabel.position = v3(-215, -152, 0);
            } else if (globalThis.GetLanguageType() == 'TH') {
              fslabel.position = v3(-55, -152, 0);
            }

            ;
            fslabel.active = true;
            fslabel.getComponent(UIOpacity).opacity = 0; // this.scheduleOnce(()=>{
            // },0.3);

            for (var jindexx = 0; jindexx < this.LabelNode.length; jindexx++) {
              //this.scheduleOnce(()=>{
              //this.pushIDSchedule(()=>{
              this.LabelNode[jindexx].active = true;
              this.LabelNode[jindexx].getComponent(UIOpacity).opacity = 0;

              if (jindexx == 3) {
                this.pushIDTween(tween(this.LabelNode[jindexx]).delay(0.4).call(() => {
                  (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                    error: Error()
                  }), sAudioMgr) : sAudioMgr).PlayBG('bgm_interlude');
                  (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                    error: Error()
                  }), sAudioMgr) : sAudioMgr).PlayShotAudio('freegame_frame_open');
                }).to(0.2, {
                  scale: v3(1.2, 1.2, 1.2)
                }).to(0.1, {
                  scale: v3(0.8, 0.8, 0.8)
                }).start(), 'LabelNode3'); //console.log('this.LabelNode',this.LabelNode);

                this.pushIDTween(tween(this.LabelNode[jindexx].getComponent(UIOpacity)).delay(0.6).to(0.2 * jindexx + 0.1, {
                  opacity: 255
                }).start(), 'LabelNode1');
              } else {
                this.pushIDTween(tween(this.LabelNode[jindexx]).delay(0.4).to(0.2, {
                  scale: v3(1.4, 1.4, 1.4)
                }).to(0.1, {
                  scale: v3(1, 1, 1)
                }).start(), 'LabelNode3'); //console.log('this.LabelNode',this.LabelNode);

                this.pushIDTween(tween(this.LabelNode[jindexx].getComponent(UIOpacity)).delay(0.6).to(0.2 * jindexx + 0.1, {
                  opacity: 255
                }).start(), 'LabelNode1');
              } //},0.03*jindexx+0.3,'xx');
              //},0.03*jindexx+0.3);

            }

            ;
            this.popSpine.setCompleteListener(() => {
              this.popSpine.timeScale = 1;
              this.popSpine.setAnimation(0, 'stpr_banner_loop', true);
            });
            this.numLabel.string = (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
              error: Error()
            }), sUtil) : sUtil).AddCommas(coin, demc);
            this.node.setSiblingIndex(this.node.parent.children.length - 1);
            this.unscheduleAllCallbacks();
            this.cleanTweenList();
            this.scheduleOnce(() => {
              this.freeBtn.emit('click');
            }, 5);
          }, this);
          director.on('freeWinBegain', () => {
            this.touchEnable = true;
          }, this); // this.scheduleOnce(()=>{
          //     director.emit('StarlightTotleViewOpen',800000,13);
          // },2)
        }

        playThunderAnima() {
          if (this.thunderSpine) {
            this.thunderSpine.node.active = true;
            this.thunderSpine.setAnimation(0, 'stpr_transition', false);
            (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
              error: Error()
            }), sAudioMgr) : sAudioMgr).PlayShotAudio('freegame_transitions', 1);
            this.thunderSpine.setCompleteListener(() => {
              this.thunderSpine.node.active = false;
            });
          }
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "freeBtn", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "numLabel", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "freenum", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "contentNode", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor5 = _applyDecoratedDescriptor(_class2.prototype, "popSpine", [_dec6], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor6 = _applyDecoratedDescriptor(_class2.prototype, "thunderSpine", [_dec7], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor7 = _applyDecoratedDescriptor(_class2.prototype, "LabelNode", [_dec8], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return [];
        }
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=StarlightPrincess_TotalWin.js.map
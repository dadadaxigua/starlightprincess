System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2", "__unresolved_3", "__unresolved_4", "__unresolved_5", "__unresolved_6", "__unresolved_7", "__unresolved_8", "__unresolved_9"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, userClient, userServer, userPush, lobbyClient, lobbyServer, chatClient, chatServer, PackageM, chatPushProtos, gameEntry, _crd;

  function _reportPossibleCrUseOfuserClient(extras) {
    _reporterNs.report("userClient", "../game/protos/user/clientProtos", _context.meta, extras);
  }

  function _reportPossibleCrUseOfuserServer(extras) {
    _reporterNs.report("userServer", "../game/protos/user/serverProtos", _context.meta, extras);
  }

  function _reportPossibleCrUseOfuserPush(extras) {
    _reporterNs.report("userPush", "../game/protos/user/pushProtos", _context.meta, extras);
  }

  function _reportPossibleCrUseOflobbyClient(extras) {
    _reporterNs.report("lobbyClient", "../game/protos/lobby/clientProtos.lobby", _context.meta, extras);
  }

  function _reportPossibleCrUseOflobbyServer(extras) {
    _reporterNs.report("lobbyServer", "../game/protos/lobby/serverProtos.lobby", _context.meta, extras);
  }

  function _reportPossibleCrUseOfchatClient(extras) {
    _reporterNs.report("chatClient", "../game/protos/chat/clientProtos", _context.meta, extras);
  }

  function _reportPossibleCrUseOfchatServer(extras) {
    _reporterNs.report("chatServer", "../game/protos/chat/serverProtos", _context.meta, extras);
  }

  function _reportPossibleCrUseOfPackageM(extras) {
    _reporterNs.report("PackageM", "./global/pomelo-package", _context.meta, extras);
  }

  function _reportPossibleCrUseOfchatPushProtos(extras) {
    _reporterNs.report("chatPushProtos", "./protos/chat/chatPushProtos", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
    }, function (_unresolved_2) {
      userClient = _unresolved_2.default;
    }, function (_unresolved_3) {
      userServer = _unresolved_3.default;
    }, function (_unresolved_4) {
      userPush = _unresolved_4.default;
    }, function (_unresolved_5) {
      lobbyClient = _unresolved_5.default;
    }, function (_unresolved_6) {
      lobbyServer = _unresolved_6.default;
    }, function (_unresolved_7) {
      chatClient = _unresolved_7.default;
    }, function (_unresolved_8) {
      chatServer = _unresolved_8.default;
    }, function (_unresolved_9) {
      PackageM = _unresolved_9.default;
    }, function (_unresolved_10) {
      chatPushProtos = _unresolved_10.default;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "8b477/CXdxIupdh2+8STV1H", "game-entry", undefined);

      gameEntry = class gameEntry {
        static isObject(item) {
          return item && typeof item === 'object' && !Array.isArray(item);
        }

        static mergeDeep(target) {
          for (var _len = arguments.length, sources = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
            sources[_key - 1] = arguments[_key];
          }

          if (!sources.length) return target;
          var source = sources.shift();

          if (gameEntry.isObject(target) && gameEntry.isObject(source)) {
            for (var key in source) {
              if (gameEntry.isObject(source[key])) {
                if (!target[key]) Object.assign(target, {
                  [key]: {}
                });
                gameEntry.mergeDeep(target[key], source[key]);
              } else {
                Object.assign(target, {
                  [key]: source[key]
                });
              }
            }
          }

          return gameEntry.mergeDeep(target, ...sources);
        }

        static init() {
          // console.log('initinitinitinit');
          // globalThis._clientProtos = gameEntry.mergeDeep(globalThis._clientProtos || {}, authClient);
          // globalThis._serverProtos = gameEntry.mergeDeep(globalThis._serverProtos || {}, authServer);
          // globalThis._pushProtos = gameEntry.mergeDeep(globalThis._pushProtos || {}, authPush);
          globalThis._clientProtos = gameEntry.mergeDeep(globalThis._clientProtos || {}, _crd && userClient === void 0 ? (_reportPossibleCrUseOfuserClient({
            error: Error()
          }), userClient) : userClient);
          globalThis._serverProtos = gameEntry.mergeDeep(globalThis._serverProtos || {}, _crd && userServer === void 0 ? (_reportPossibleCrUseOfuserServer({
            error: Error()
          }), userServer) : userServer);
          globalThis._pushProtos = gameEntry.mergeDeep(globalThis._pushProtos || {}, _crd && userPush === void 0 ? (_reportPossibleCrUseOfuserPush({
            error: Error()
          }), userPush) : userPush);
          globalThis._clientProtos = gameEntry.mergeDeep(globalThis._clientProtos || {}, _crd && lobbyClient === void 0 ? (_reportPossibleCrUseOflobbyClient({
            error: Error()
          }), lobbyClient) : lobbyClient);
          globalThis._serverProtos = gameEntry.mergeDeep(globalThis._serverProtos || {}, _crd && lobbyServer === void 0 ? (_reportPossibleCrUseOflobbyServer({
            error: Error()
          }), lobbyServer) : lobbyServer); // cc._pushProtos = mergeDeep(cc._pushProtos || {}, require('./protos/lobby/pushProtos.lobby.js'));

          globalThis._clientProtos = gameEntry.mergeDeep(globalThis._clientProtos || {}, _crd && chatClient === void 0 ? (_reportPossibleCrUseOfchatClient({
            error: Error()
          }), chatClient) : chatClient);
          globalThis._serverProtos = gameEntry.mergeDeep(globalThis._serverProtos || {}, _crd && chatServer === void 0 ? (_reportPossibleCrUseOfchatServer({
            error: Error()
          }), chatServer) : chatServer);
          globalThis._pushProtos = gameEntry.mergeDeep(globalThis._pushProtos || {}, _crd && chatPushProtos === void 0 ? (_reportPossibleCrUseOfchatPushProtos({
            error: Error()
          }), chatPushProtos) : chatPushProtos); // globalThis.decodeIO_encoder = globalThis.protobufjs.Root.fromJSON(globalThis._clientProtos);
          // globalThis.decodeIO_decoder = globalThis.protobufjs.Root.fromJSON(globalThis._serverProtos);
          // globalThis.push_decoder = globalThis.protobufjs.Root.fromJSON(globalThis._pushProtos);
        }

      };
      gameEntry.init();
      globalThis.package = _crd && PackageM === void 0 ? (_reportPossibleCrUseOfPackageM({
        error: Error()
      }), PackageM) : PackageM;

      _export("default", {
        Package: _crd && PackageM === void 0 ? (_reportPossibleCrUseOfPackageM({
          error: Error()
        }), PackageM) : PackageM,
        GameUrls: [],
        //connector urls
        PayUrls: [],
        //php api
        AuthUrls: [],
        //user sign in, sign up
        SerUrls: [],
        //chat upload
        PhpUrils: []
      });

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=game-entry.js.map
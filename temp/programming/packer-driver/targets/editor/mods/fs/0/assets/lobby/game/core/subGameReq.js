System.register(["cc"], function (_export, _context) {
  "use strict";

  var _cclegacy, _decorator, director, subGameReq, _crd, ccclass, property;

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  _export("subGameReq", void 0);

  return {
    setters: [function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      director = _cc.director;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "47b86RWa85GvqOfnx3IFOnV", "subGameReq", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("subGameReq", subGameReq = class subGameReq {
        static Init() {
          subGameReq.isReadyRec = false;
        }

        static Clear() {
          subGameReq.isReadyRec = false;
        }

        static ReadyForPush() {
          subGameReq.isReadyRec = true;
        }

        static isObject(item) {
          return item && typeof item === 'object' && !Array.isArray(item);
        }

        static mergeDeep(target, ...sources) {
          if (!sources.length) return target;
          const source = sources.shift();

          if (subGameReq.isObject(target) && subGameReq.isObject(source)) {
            for (const key in source) {
              if (subGameReq.isObject(source[key])) {
                if (!target[key]) Object.assign(target, {
                  [key]: {}
                });
                subGameReq.mergeDeep(target[key], source[key]);
              } else {
                Object.assign(target, {
                  [key]: source[key]
                });
              }
            }
          }

          return subGameReq.mergeDeep(target, ...sources);
        }

        static PushEmit(msgName, msgObj) {
          if (subGameReq.isReadyRec) {
            director.emit(msgName, msgObj);
          }
        }

      });

      _defineProperty(subGameReq, "isReadyRec", false);

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=subGameReq.js.map
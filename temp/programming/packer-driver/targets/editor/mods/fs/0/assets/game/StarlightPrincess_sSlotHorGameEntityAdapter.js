System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2", "__unresolved_3"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, director, instantiate, Label, Node, Prefab, tween, v3, sSlotHorGameEntityAdapter, sUtil, UIFreeWinBuy, _dec, _dec2, _dec3, _dec4, _class, _class2, _descriptor, _descriptor2, _descriptor3, _temp, _crd, ccclass, property, Starlight_sSlotHorGameEntityAdapter;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfsSlotHorGameEntityAdapter(extras) {
    _reporterNs.report("sSlotHorGameEntityAdapter", "../lobby/hyPrefabs/gameRes/external/sSlotHorGameEntityAdapter", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsUtil(extras) {
    _reporterNs.report("sUtil", "../lobby/game/core/sUtil", _context.meta, extras);
  }

  function _reportPossibleCrUseOfUIFreeWinBuy(extras) {
    _reporterNs.report("UIFreeWinBuy", "../lobby/hyPrefabs/gameRes/external/UIFreeWinBuy", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      director = _cc.director;
      instantiate = _cc.instantiate;
      Label = _cc.Label;
      Node = _cc.Node;
      Prefab = _cc.Prefab;
      tween = _cc.tween;
      v3 = _cc.v3;
    }, function (_unresolved_2) {
      sSlotHorGameEntityAdapter = _unresolved_2.sSlotHorGameEntityAdapter;
    }, function (_unresolved_3) {
      sUtil = _unresolved_3.sUtil;
    }, function (_unresolved_4) {
      UIFreeWinBuy = _unresolved_4.UIFreeWinBuy;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "414a5EiJXxDRaAml7YxJpEh", "StarlightPrincess_sSlotHorGameEntityAdapter", undefined);

      ({
        ccclass,
        property
      } = _decorator);
      /**
       * Predefined variables
       * Name = Starlight_sSlotHorGameEntityAdapter
       * DateTime = Thu Oct 12 2023 14:54:31 GMT+0800 (中国标准时间)
       * Author = dadadaxigua
       * FileBasename = Starlight_sSlotHorGameEntityAdapter.ts
       * FileBasenameNoExtension = Starlight_sSlotHorGameEntityAdapter
       * URL = db://assets/game/Starlight_sSlotHorGameEntityAdapter.ts
       * ManualUrl = https://docs.cocos.com/creator/3.4/manual/zh/
       *
       */

      _export("Starlight_sSlotHorGameEntityAdapter", Starlight_sSlotHorGameEntityAdapter = (_dec = ccclass('Starlight_sSlotHorGameEntityAdapter'), _dec2 = property(Label), _dec3 = property(Node), _dec4 = property(Prefab), _dec(_class = (_class2 = (_temp = class Starlight_sSlotHorGameEntityAdapter extends (_crd && sSlotHorGameEntityAdapter === void 0 ? (_reportPossibleCrUseOfsSlotHorGameEntityAdapter({
        error: Error()
      }), sSlotHorGameEntityAdapter) : sSlotHorGameEntityAdapter) {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "winStringLabel", _descriptor, this);

          _initializerDefineProperty(this, "winStringLabel1", _descriptor2, this);

          _initializerDefineProperty(this, "freebuyNode", _descriptor3, this);

          _defineProperty(this, "callBack", null);

          _defineProperty(this, "updateLabTween", void 0);
        }

        onLoad() {
          super.onLoad(); //this.currentBetAmount=this.betAmount;
          // const freebuy = instantiate(this.node.parent.getChildByPath('UIFreeWinBuy1'));
          // freebuy.parent = this.node.parent;
          // freebuy.active = true;
          // this.selfFreeWinBuyView = freebuy.getComponent(sFreeWinBuyView);
          // this.winLabel = this.node.getChildByPath('betInfo/Node/winLabel').getComponent(Label);
          //this.ruleViewSize = 1200;
        }

        start() {
          super.start(); // Promise.all([new Promise((resolve,reject)=>{
          //     globalThis.getSubGamePrefabByPath('prefabs/subGameUserCoin',(newObj : Node)=>{
          //         if(newObj){
          //             newObj.parent = this.node.parent;
          //             this.ownLabel = newObj.getChildByName('Label').getComponent(Label);
          //             this.shopNode = newObj.getChildByName('shop').getComponent(Button);
          //         }
          //         resolve(1);
          //     });
          // }),new Promise((resolve,reject)=>{
          //     globalThis.getSubGamePrefabByPath('prefabs/subGameSettingDetail',(newObj : Node)=>{
          //         if(newObj){
          //             newObj.parent = this.node.parent.parent;
          //             this.menuBtns = newObj.getComponent(UIOpacity);
          //             this.soundBtn = this.menuBtns.node.getChildByPath('main/items/sound').getComponent(Button);
          //             this.closeBtn = this.menuBtns.node.getChildByPath('close').getComponent(Button);
          //             this.quitBtn = this.menuBtns.node.getChildByPath('main/items/exit').getComponent(Button);
          //             newObj.active = false;
          //         }
          //         resolve(2);
          //     });
          // }),new Promise((resolve,reject)=>{
          //     const lua = globalThis.GetLanguageType();
          //     if(lua){
          //         sUtil.getLobbyAssetInSubGame('config/'+lua,JsonAsset,(err,asset)=>{
          //             if(asset){
          //                 btnInternationalManager.SetLanguegeData(asset.name, asset.json);
          //             }
          //             resolve(5);
          //         });
          //     }else{
          //         resolve(5);
          //     }
          // })]).then((res)=>{
          //     super.super.start();
          //     if(this.shopNode){
          //         this.shopNode.node.on('click',()=>{
          //             globalThis.openStore && globalThis.openStore();
          //         },this);
          //     }
          // });
          // [3]
          // director.off('rollStop',null,sGameEntity);
          // director.off('rollStopEndTrigger',null,sGameEntity);
          //director.targetOff()
          ///this.freeWinBuy=this.node.parent.getChildByPath('UIFreeWinBuy1');
          // director.off('betUserInfoUpdateWinAnima');
          // director.on('betUserInfoUpdateWinAnima', (rate) => {
          //     this.currentBetAmount=this.betAmount;
          //     this.betUserInfoUpdateAnima(null, null, rate * this.currentBetAmount, 0.2);
          // }, this);

          this.freeWinBuy = instantiate(this.freebuyNode); //this.ruleNode=this.node.parent.getChildByName('UIIntro');
          //this.loadFreeBuyView();

          const freebuy = instantiate(this.freebuyNode);
          freebuy.parent = this.node.parent;
          freebuy.active = true;
          this.selfFreeWinBuyView = freebuy.getComponent(_crd && UIFreeWinBuy === void 0 ? (_reportPossibleCrUseOfUIFreeWinBuy({
            error: Error()
          }), UIFreeWinBuy) : UIFreeWinBuy); // director.off('subGameFreeWinBuyBtnClick');
          // director.on('subGameFreeWinBuyBtnClick', () => {
          //     if (this.betBtnState == 'normal' && this.betClickMode == 'normal') {
          //         if (this.clientConfig) {
          //             let bet_types = this.clientConfig.bet_types;
          //             if (bet_types && bet_types.length > 0) {
          //                 for (let i = 0; i < bet_types.length; i++) {
          //                     const element = bet_types[i];
          //                     if (element.game_id == this.gameid) {
          //                         if (element.bet_amount == this.betAmount && element.game_mode == 2) {
          //                             let mUserInfo = this.getUserInfo();
          //                             if (mUserInfo) {
          //                                 let tipStr = '';
          //                                 let canBuy = true;
          //                                 let maxBet = mUserInfo.max_bet;
          //                                 let userCoin = mUserInfo.coin;
          //                                 let sumAmount = element.bet_amount * element.bet_multiple;
          //                                 let sum = element.bet_amount;
          //                                 if (sumAmount > userCoin) {
          //                                     console.log('no userCoin');
          //                                     canBuy = false;
          //                                     tipStr = globalThis.GetDataByKey && globalThis.GetDataByKey("mulTiLN", "common", '5001');
          //                                 } else if (sum > maxBet) {
          //                                     console.log('no maxBet');
          //                                     canBuy = false;
          //                                     tipStr = btnInternationalManager.GetDataByKey('maxBetNotEnough');
          //                                 }
          //                                 this.freeBuyBetAmount1 = element.bet_amount;
          //                                 this.freeBuyLineMultiple1 = element.bet_multiple;
          //                                 this.currentBetAmount=element.bet_amount;
          //                                 console.log('subGameFreeWinBuyBtnClick');
          //                                 if(this.selfFreeWinBuyView){
          //                                     this.selfFreeWinBuyView.viewInit(element.bet_amount * element.bet_multiple, element.bet_type, canBuy, tipStr);
          //                                 }else if(this.freeWinBuy){
          //                                     this.freeWinBuy=this.node.parent.getChildByPath('UIFreeWinBuy');
          //                                     const freeWinBuy = this.freeWinBuy.getComponent(UIFreeWinBuy);
          //                                     if(freeWinBuy){
          //                                        // this.currentBetAmount=this.freeBuyBetAmount;
          //                                         freeWinBuy.viewInit(element.bet_amount * element.bet_multiple, element.bet_type, canBuy, tipStr);
          //                                     };
          //                                 };
          //                             };
          //                             break;
          //                         };
          //                     };
          //                 };
          //             };
          //         };
          //     };
          // }, this); 
        }

        // rulesBtnClick(){
        //     this.ruleNode.active = true;
        // };
        rulesBtnClick() {
          //this.ruleNode.scale=v3(1.28,1.28,0.78125);
          // let pNodeimg=this.ruleNode.getChildByPath('main/ScrollView/view/content/img')
          // let contNode=this.ruleNode.getChildByPath('main/ScrollView/view/content');
          // let pviewNod=this.ruleNode.getChildByPath('main/ScrollView/view');
          // let pcloseNode=this.ruleNode.getChildByPath('main/close');
          // let pbgNode=this.ruleNode.getChildByPath('main/bg');
          // let ScrollViewNode=this.ruleNode.getChildByPath('main/ScrollView');
          // pcloseNode.addComponent(Widget);
          // pcloseNode.getComponent(Widget).isAlignRight=true;
          // pcloseNode.getComponent(Widget).isAlignTop=true;
          // pcloseNode.getComponent(Widget)!.isAbsoluteRight= false;
          // pcloseNode.getComponent(Widget)!.isAbsoluteTop = false;
          // pcloseNode.getComponent(Widget)!.right=0.11;
          // pcloseNode.getComponent(Widget)!.top=0.1;
          // this.setNodeWidget(pbgNode,0.11,0.11,0.08,0.08);
          // //this.setNodeWidget(ScrollViewNode,0.11,0.11,0.2,0.1);
          // pviewNod.active=false;
          // pviewNod.active=true;
          // let pviewHeight=pviewNod.getComponent(UITransform).contentSize.x;
          // contNode.getComponent(Layout).affectedByScale=true;
          // console.log(pbgNode);
          // pNodeimg.scale=v3((pviewHeight/1280),(pviewHeight/1280),0.78125);
          this.ruleNode.active = true;
        }

        betUserInfoUpdate(own, bet, win) {
          if (this.ownLabel) {
            if (own || own == 0) {
              this.ownLabel.node['rollAnimaValue'] = own;
              this.ownLabel.string = this.AddCommas(Math.trunc(own));
            }
          }

          if (this.betLabel) {
            if (bet || bet == 0) {
              this.betLabel.node['rollAnimaValue'] = bet;
              this.betLabel.string = this.AddCommas(Math.trunc(bet));
            }
          }

          ; //if(own==null){

          if (win == null) {
            this.winLabel.node['rollAnimaValue'] = 0;
            console.log('重置winLabel1');
            this.winLabel.node.active = false;
            this.winStringLabel.node.active = true;
            this.winStringLabel1.active = false;
            this.winLabel.string = 0 + '';
          } else {
            if (win && win > 0) {
              //console.log('win1:',win);
              this.winLabel.node['rollAnimaValue'] = win;
              console.log('重置winLabel2');
              this.winLabel.string = this.AddCommas(Math.trunc(win));
              this.winStringLabel1.active = true;
            } else {
              this.winLabel.node['rollAnimaValue'] = 0;
              console.log('重置winLabel-1'); //console.log('win2:',win);
              // this.winLabel.string=0+'';
            }

            ;
          }

          ; //};
        }

        betUserInfoUpdateAnima(own, bet, win, animaTime = 1, callBack = null) {
          if (own || own == 0) {
            this.CommasLabelAnima(this.ownLabel, Math.trunc(own), animaTime);
          }

          ;

          if (bet || bet == 0) {
            this.CommasLabelAnima(this.betLabel, Math.trunc(bet), animaTime);
          }

          ;

          if (own == null) {
            if (win == null) {
              this.winLabel.node['rollAnimaValue'] = 0;
              console.log('重置winLabel3');
              this.winLabel.string = 0 + '';
            } else {
              if (win && win > 0) {
                this.winLabel.node.active = true;
                this.winStringLabel.node.active = false;
                this.winStringLabel1.active = true; //console.log('win3:',win);

                let time = 0.6;

                if (win >= this.currentBetAmount * 200) {
                  time = 12;
                }

                ;

                if (!this.winLabel.node['rollAnimaValue']) {
                  console.log('重置winLabel4');
                  this.winLabel.node['rollAnimaValue'] = 0;
                }

                ; //this.winLabel.string = this.AddCommas(Math.floor(win));

                let num = parseInt(this.winLabel.string);
                console.log('win:', win);
                console.log('num:', num);
                console.log('this.winLabel.string:', this.winLabel.string);
                console.log('rollAnimaValue', this.winLabel.node['rollAnimaValue']); //sUtil.TweenLabel(this.winLabel.node['rollAnimaValue'],Math.trunc(win+this.winLabel.node['rollAnimaValue']),this.winLabel,time);

                this.updateWinLab(this.winLabel.node['rollAnimaValue'], this.winLabel, Math.trunc(win + this.winLabel.node['rollAnimaValue']), time, this.callBack); //this.winLabel.node['rollAnimaValue'] += win;

                this.winLabel.node['rollAnimaValue'] += Math.trunc(win); //this.CommasLabelAnima(this.winLabel, Math.trunc(bet), animaTime);

                director.off('StopLabelanima');
                (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                  error: Error()
                }), sUtil) : sUtil).once('StopLabelanima', () => {
                  //this.winLabel.node['rollAnimaValue'] += win;
                  this.updateLabTween.stop();
                  this.winLabel.string = this.AddCommas(Math.floor(this.winLabel.node['rollAnimaValue']));
                });
              } else {
                if (!this.winLabel.node['rollAnimaValue']) {
                  console.log('重置winLabel5');
                  this.winLabel.node['rollAnimaValue'] = 0;
                  this.winLabel.string = 0 + '';
                } else {
                  this.winLabel.node['rollAnimaValue'] = 0;
                  console.log('重置winLabel6');
                }

                ; //console.log('win4:',win);
                // this.winLabel.node.active=false;
                // this.winStringLabel.node.active=true;
                // this.winStringLabel1.node.active=false
              }

              ;
            }

            ;
          }

          ;

          if (callBack != null) {
            this.scheduleOnce(callBack, animaTime);
          }

          ;
        }

        updateWinLab(lastCoin, winLabel, coin, time, cb) {
          let tweenTargetVec3 = v3(lastCoin, lastCoin, lastCoin);
          let demc = -1;

          if (globalThis.getCoinRate && globalThis.getCoinRate() > 1) {
            demc = 2;
          }

          this.updateLabTween && this.updateLabTween.stop();
          this.updateLabTween = tween(tweenTargetVec3).to(time, v3(coin, coin, coin), {
            "onUpdate": target => {
              if (winLabel) {
                //winLabel.string = sUtil.AddCommas(Math.floor(target.x));
                winLabel.string = false ? Math.trunc(target.x).toString() : 0 > 0 ? (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                  error: Error()
                }), sUtil) : sUtil).changeNumberToKW(target.x, 0) : (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                  error: Error()
                }), sUtil) : sUtil).AddCommas(target.x, demc).toString(); //winLabel.string = sUtil.AddCommas(Math.trunc(target.x));
              }

              ;
            },
            easing: undefined
          }).call(() => {
            if (winLabel) {
              winLabel.string = false ? Math.trunc(coin).toString() : 0 > 0 ? (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                error: Error()
              }), sUtil) : sUtil).changeNumberToKW(coin, 0) : (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                error: Error()
              }), sUtil) : sUtil).AddCommas(coin, demc).toString();
            }

            ; //this.winLabel.node['rollAnimaValue'] += coin;

            if (cb) cb();
          }).start();
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "winStringLabel", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "winStringLabel1", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "freebuyNode", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class)); //     tween(tweenTargetVec3).to(_time, v3(value, value, value), {
      //         "onUpdate": (target: Vec3) => {
      //             if (targetLabel) {
      //                 targetLabel.string = isInt ? Math.trunc(target.x).toString() : (needKWNum > 0 ? sUtil.changeNumberToKW((target.x),needKWNum) : (sUtil.AddCommas(target.x,demc)).toString());
      //             }
      //         }, easing: ease
      //     }).call(() => {
      //         if (targetLabel) {
      //             targetLabel.string = isInt ? Math.trunc(value).toString() : (needKWNum > 0 ? sUtil.changeNumberToKW((value),needKWNum) : (sUtil.AddCommas(value,demc)).toString());
      //         }
      //         if(finishCall){
      //             finishCall();
      //         }
      //     }).start();
      // }

      /**
       * [1] Class member could be defined like this.
       * [2] Use `property` decorator if your want the member to be serializable.
       * [3] Your initialization goes here.
       * [4] Your update function goes here.
       *
       * Learn more about scripting: https://docs.cocos.com/creator/3.4/manual/zh/scripting/
       * Learn more about CCClass: https://docs.cocos.com/creator/3.4/manual/zh/scripting/decorator.html
       * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.4/manual/zh/scripting/life-cycle-callbacks.html
       */


      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=StarlightPrincess_sSlotHorGameEntityAdapter.js.map
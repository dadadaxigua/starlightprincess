System.register(["cc"], function (_export, _context) {
  "use strict";

  var _cclegacy, _decorator, Component, _dec, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _descriptor5, _temp, _crd, ccclass, property, InternationalizationScript1;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  return {
    setters: [function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      Component = _cc.Component;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "e76c9SAjeRMR797oZjL2Mkj", "internationalizationScript1", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("InternationalizationScript1", InternationalizationScript1 = (_dec = ccclass('InternationalizationScript1'), _dec(_class = (_class2 = (_temp = class InternationalizationScript1 extends Component {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "componentName", _descriptor, this);

          _initializerDefineProperty(this, "propertyName", _descriptor2, this);

          _initializerDefineProperty(this, "jsonName", _descriptor3, this);

          _initializerDefineProperty(this, "module", _descriptor4, this);

          _initializerDefineProperty(this, "key", _descriptor5, this);
        }

        onLoad() {}

        init() {
          let _label = this.node.getComponent(this.componentName);

          if (_label) {
            // _label[this.propertyName] = inMgr.GetDataByKey(this.module,this.key);
            _label[this.propertyName] = globalThis.GetDataByKey(this.jsonName, this.module, this.key);
          }
        }

        start() {
          this.init();
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "componentName", [property], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return 'cc.Label';
        }
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "propertyName", [property], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return 'string';
        }
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "jsonName", [property], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return 'mulTiLN';
        }
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "module", [property], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return '';
        }
      }), _descriptor5 = _applyDecoratedDescriptor(_class2.prototype, "key", [property], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return '';
        }
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=internationalizationScript1.js.map
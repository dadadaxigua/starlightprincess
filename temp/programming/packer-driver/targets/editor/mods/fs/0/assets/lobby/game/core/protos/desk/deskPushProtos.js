System.register(["cc"], function (_export, _context) {
  "use strict";

  var _cclegacy, _crd;

  return {
    setters: [function (_cc) {
      _cclegacy = _cc.cclegacy;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "df4ca6oOr1I+qBLEBbqSyrY", "deskPushProtos", undefined);

      _export("default", {
        "nested": {
          "desk": {
            "nested": {
              "msg": {}
            }
          }
        }
      });

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=deskPushProtos.js.map
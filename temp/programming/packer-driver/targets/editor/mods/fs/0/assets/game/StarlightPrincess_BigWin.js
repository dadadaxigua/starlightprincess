System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2", "__unresolved_3"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, Button, director, Label, Node, ParticleSystem, sp, tween, UIOpacity, v3, sComponent, sUtil, sAudioMgr, _dec, _dec2, _dec3, _dec4, _dec5, _dec6, _dec7, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _descriptor5, _descriptor6, _temp, _crd, ccclass, property, StarlightPrincess_BigWin;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfsComponent(extras) {
    _reporterNs.report("sComponent", "../lobby/game/core/sComponent", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsUtil(extras) {
    _reporterNs.report("sUtil", "../lobby/game/core/sUtil", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsAudioMgr(extras) {
    _reporterNs.report("sAudioMgr", "../lobby/game/core/sAudioMgr", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      Button = _cc.Button;
      director = _cc.director;
      Label = _cc.Label;
      Node = _cc.Node;
      ParticleSystem = _cc.ParticleSystem;
      sp = _cc.sp;
      tween = _cc.tween;
      UIOpacity = _cc.UIOpacity;
      v3 = _cc.v3;
    }, function (_unresolved_2) {
      sComponent = _unresolved_2.sComponent;
    }, function (_unresolved_3) {
      sUtil = _unresolved_3.sUtil;
    }, function (_unresolved_4) {
      sAudioMgr = _unresolved_4.default;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "5ff1erVvRhEGadjpmXApWD3", "StarlightPrincess_BigWin", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("StarlightPrincess_BigWin", StarlightPrincess_BigWin = (_dec = ccclass('StarlightPrincess_BigWin'), _dec2 = property(Node), _dec3 = property(Node), _dec4 = property(sp.Skeleton), _dec5 = property(UIOpacity), _dec6 = property(UIOpacity), _dec7 = property(Button), _dec(_class = (_class2 = (_temp = class StarlightPrincess_BigWin extends (_crd && sComponent === void 0 ? (_reportPossibleCrUseOfsComponent({
        error: Error()
      }), sComponent) : sComponent) {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "mainNode", _descriptor, this);

          _initializerDefineProperty(this, "CoinNode", _descriptor2, this);

          _initializerDefineProperty(this, "targetSpine", _descriptor3, this);

          _initializerDefineProperty(this, "borderNode", _descriptor4, this);

          _initializerDefineProperty(this, "maskOpa", _descriptor5, this);

          _initializerDefineProperty(this, "closeBtn", _descriptor6, this);

          _defineProperty(this, "touchEnable", false);

          _defineProperty(this, "targetCoinValue", 0);

          _defineProperty(this, "animaType", 'nice');

          _defineProperty(this, "bgmp", 'bgm_mg');
        }

        start() {
          director.on('freeWinBegain', () => {
            this.bgmp = 'bgm_fs';
          }, this);
          director.on('freeWinOver', () => {
            this.bgmp = 'bgm_mg';
          }, this);
          director.on('StarlightPrincessBigWin', rate => {
            let spineAnimanName = '_nice_';
            (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
              error: Error()
            }), sAudioMgr) : sAudioMgr).StopBGAudio();

            if (rate >= 200 && rate < 400) {
              (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                error: Error()
              }), sAudioMgr) : sAudioMgr).PlayBG('bgm_nicewin');
              spineAnimanName = '_nice_';
              this.animaType = 'nice';
            } else if (rate >= 400 && rate < 600) {
              (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                error: Error()
              }), sAudioMgr) : sAudioMgr).PlayBG('bgm_megawin');
              spineAnimanName = '_mega_';
              this.animaType = 'mega';
            } else if (rate >= 600 && rate < 1000) {
              (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                error: Error()
              }), sAudioMgr) : sAudioMgr).PlayBG('bgm_superwin');
              (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                error: Error()
              }), sAudioMgr) : sAudioMgr).PlayShotAudio('bgm_superwin_voice');
              spineAnimanName = '_superb_';
              this.animaType = 'super';
            } else if (rate >= 1000) {
              (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                error: Error()
              }), sAudioMgr) : sAudioMgr).PlayBG('bgm_sensationalwin');
              (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                error: Error()
              }), sAudioMgr) : sAudioMgr).PlayShotAudio('bgm_sensationalwin_voice');
              spineAnimanName = '_sensational_';
              this.animaType = 'sensational';
            } else {
              director.emit('StarlightPrincessBigWinEnd');
            }

            ; // this.viewOpen(spineAnimanName,rate * 12); 

            this.viewOpen(spineAnimanName, rate * globalThis.GameBtnEntity.CurrentBetAmount); //this.viewOpen(spineAnimanName,rate* 20);
          }, this); // this.scheduleOnce(()=>{
          //    director.emit('StarlightPrincessBigWin',200);
          // },2);

          this.closeBtn.node.on('click', () => {
            if (this.touchEnable) {
              this.touchEnable = false;
              this.cleanTweenList();

              for (let i = 0; i < this.CoinNode.children.length; i++) {
                this.CoinNode.children[i].getComponent(ParticleSystem).loop = false;
              }

              ;
              this.removeIDSchedule('coinLable');
              const coinLabelNode = this.borderNode.node.getChildByName('label');

              if (coinLabelNode) {
                (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                  error: Error()
                }), sAudioMgr) : sAudioMgr).StopBGAudio();

                if (this.animaType == 'nice') {
                  (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                    error: Error()
                  }), sAudioMgr) : sAudioMgr).PlayBG('bgm_nicewin_end', false);
                } else if (this.animaType == 'mega') {
                  (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                    error: Error()
                  }), sAudioMgr) : sAudioMgr).PlayBG('bgm_megawin_end', false);
                } else if (this.animaType == 'super') {
                  (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                    error: Error()
                  }), sAudioMgr) : sAudioMgr).PlayBG('bgm_superwin_end', false);
                } else if (this.animaType == 'sensational') {
                  (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                    error: Error()
                  }), sAudioMgr) : sAudioMgr).PlayBG('bgm_sensationalwin_end', false);
                }

                ;
                const coinLable = coinLabelNode.getComponent(Label);
                coinLable.string = this.AddCommas(this.targetCoinValue); //this.pushOneTween(tween(coinLable.node).to(0.3,{scale : v3(1.3,1.3,1.3)},{easing : 'sineOut'}).to(0.3,{scale : Vec3.ONE},{easing : 'sineIn'}).union().repeatForever().start());
                //this.pushOneTween(tween(this.borderNode.node).to(0.6,{scale : v3(1.2,1.2,1.2)},{easing : 'sineOut'}).to(0.6,{scale : Vec3.ONE},{easing : 'sineIn'}).union().repeatForever().start());
              }

              ;
              this.scheduleOnce(() => {
                director.emit('StopLabelanima');
                this.viewClose();
              }, 3);
            }
          }, this);
        }

        viewOpen(spineAnima, coin) {
          this.targetCoinValue = coin;

          if (this.targetSpine && this.borderNode) {
            this.SetNodeFront(this.node);
            this.removeIDSchedule('touch');
            this.touchEnable = false;
            this.cleanTweenList();
            this.SetUIFront();
            this.mainNode.active = true;
            this.targetSpine.node.active = true;
            this.targetSpine.setAnimation(0, 'stpr_bigwin' + spineAnima + 'in', false);

            for (let i = 0; i < this.CoinNode.children.length; i++) {
              this.CoinNode.children[i].getComponent(ParticleSystem).loop = true;
            }

            ;
            this.maskOpa.opacity = 0;
            this.pushOneTween(tween(this.maskOpa).to(0.3, {
              opacity: 255
            }).start());
            let IsCom = 0;
            IsCom += 1;
            const coinLabelNode = this.borderNode.node.getChildByName('label');

            if (coinLabelNode && IsCom == 1) {
              const coinLable = coinLabelNode.getComponent(Label);
              let bigWinTime = 12;

              if (this.animaType == 'nice') {
                this.CoinNode.active = true;
                this.CoinNode.children[0].active = false;
                this.CoinNode.children[1].active = false;
                this.CoinNode.children[2].active = true;
              } else if (this.animaType == 'mega') {
                bigWinTime = 12;
                this.CoinNode.active = true;
                this.CoinNode.children[0].active = false;
                this.CoinNode.children[1].active = false;
                this.CoinNode.children[2].active = true;
              } else if (this.animaType == 'super') {
                bigWinTime = 12;
                this.CoinNode.active = true;
                this.CoinNode.children[0].active = true;
                this.CoinNode.children[1].active = true;
                this.CoinNode.children[2].active = true;
              } else if (this.animaType == 'sensational') {
                bigWinTime = 12;
                this.CoinNode.active = true;
                this.CoinNode.children[0].active = true;
                this.CoinNode.children[1].active = true;
                this.CoinNode.children[2].active = true;
              }

              ;
              coinLable.string = '0';
              this.scheduleOnce(() => {
                this.pushOneTween((_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                  error: Error()
                }), sUtil) : sUtil).TweenLabel(0, coin, coinLable, bigWinTime, 0, 'quadOut', 0, () => {
                  (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                    error: Error()
                  }), sAudioMgr) : sAudioMgr).StopBGAudio();

                  if (this.animaType == 'nice') {
                    (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                      error: Error()
                    }), sAudioMgr) : sAudioMgr).PlayBG('bgm_nicewin_end', false);
                  } else if (this.animaType == 'mega') {
                    (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                      error: Error()
                    }), sAudioMgr) : sAudioMgr).PlayBG('bgm_megawin_end', false);
                  } else if (this.animaType == 'super') {
                    (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                      error: Error()
                    }), sAudioMgr) : sAudioMgr).PlayBG('bgm_superwin_end', false);
                  } else if (this.animaType == 'sensational') {
                    (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                      error: Error()
                    }), sAudioMgr) : sAudioMgr).PlayBG('bgm_sensationalwin_end', false);
                  }

                  ;

                  for (let i = 0; i < this.CoinNode.children.length; i++) {
                    this.CoinNode.children[i].getComponent(ParticleSystem).loop = false; //this.CoinNode.children[i].getComponent(ParticleSystem).stop();
                  }

                  ; //this.pushOneTween(tween(coinLable.node).to(0.3,{scale : v3(1.3,1.3,1.3)},{easing : 'sineOut'}).to(0.3,{scale : Vec3.ONE},{easing : 'sineIn'}).union().repeatForever().start());

                  this.pushIDSchedule(() => {
                    this.viewClose();
                  }, 1, 'coinLable');
                }));
              }, 0.7);
            }

            ;
            this.targetSpine.setCompleteListener(() => {
              this.targetSpine.setAnimation(0, 'stpr_bigwin' + spineAnima + 'loop', true);
            });
            this.borderNode.opacity = 0;
            this.borderNode.node.position = v3(0, -120, 0);
            this.borderNode.node.scale = v3(1.4, 1.4, 1.4);
            this.pushOneTween(tween(this.borderNode).delay(0.6).to(0.2, {
              opacity: 255
            }).start());
            this.pushOneTween(tween(this.borderNode.node).delay(0.6).to(0.2, {
              position: v3(0, -240, 0)
            }).call(() => {// this.pushOneTween(tween(this.borderNode.node).to(0.6,{scale : v3(1.2,1.2,1.2)},{easing : 'sineOut'}).to(0.6,{scale : Vec3.ONE},{easing : 'sineIn'}).union().repeatForever().start());
            }).start());
            this.pushIDSchedule(() => {
              this.touchEnable = true;
            }, 0.5, 'touch');
          }
        }

        viewClose() {
          this.CoinNode.active = false;

          if (this.targetSpine && this.borderNode) {
            const animaName = this.targetSpine.animation;

            if (animaName.length > 1) {
              //const rName = animaName.slice(0,2);
              //if(rName){
              if (this.animaType == 'nice') {
                this.targetSpine.setAnimation(0, 'stpr_bigwin_nice_out', false);
              } else if (this.animaType == 'mega') {
                this.targetSpine.setAnimation(0, 'stpr_bigwin_mega_out', false);
              } else if (this.animaType == 'super') {
                this.targetSpine.setAnimation(0, 'stpr_bigwin_superb_out', false);
              } else if (this.animaType == 'sensational') {
                this.targetSpine.setAnimation(0, 'stpr_bigwin_sensational_out', false);
              }

              ;
              this.targetSpine.setCompleteListener(() => {
                this.scheduleOnce(() => {
                  for (let i = 0; i < this.CoinNode.children.length; i++) {
                    //this.CoinNode.children[i].getComponent(ParticleSystem).loop=false;
                    this.CoinNode.children[i].getComponent(ParticleSystem).clear();
                    this.CoinNode.children[i].getComponent(ParticleSystem).stop();
                  }

                  ;
                  (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                    error: Error()
                  }), sAudioMgr) : sAudioMgr).StopAudio();
                  (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                    error: Error()
                  }), sAudioMgr) : sAudioMgr).StopBGAudio();
                  (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                    error: Error()
                  }), sAudioMgr) : sAudioMgr).PlayBG(this.bgmp);
                  this.scheduleOnce(() => {
                    this.mainNode.active = false;
                    director.emit('StarlightPrincessBigWinEnd');
                  }, 0.5);
                }, 0.5);
              }); //};
            }

            ;
            this.cleanTweenList();
            this.removeIDSchedule('coinLable');
            this.pushOneTween(tween(this.maskOpa).to(0.3, {
              opacity: 0
            }).start());
            this.pushOneTween(tween(this.borderNode).to(0.3, {
              opacity: 0
            }).call(() => {
              this.borderNode.node.getChildByName('label').getComponent(Label).string = '0';
            }).start()); // this.pushOneTween(tween(this.borderNode.node).to(0.2,{position : v3(0,-120,0),scale : v3(1.6,1.6,1.6)}).call(()=>{
            // }).start());
            //},0.5);
          }

          ;
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "mainNode", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "CoinNode", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "targetSpine", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "borderNode", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor5 = _applyDecoratedDescriptor(_class2.prototype, "maskOpa", [_dec6], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor6 = _applyDecoratedDescriptor(_class2.prototype, "closeBtn", [_dec7], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=StarlightPrincess_BigWin.js.map
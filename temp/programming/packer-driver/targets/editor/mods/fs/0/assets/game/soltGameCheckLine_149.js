System.register(["cc"], function (_export, _context) {
  "use strict";

  var _cclegacy, _crd, odds, topSymbols, bottom_symbol_length, top_symbol_length, col, row, wildSymbols, unableWinSymbol, conversIDToMulti, soltGameCheckUtil_149;

  return {
    setters: [function (_cc) {
      _cclegacy = _cc.cclegacy;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "ae5f0M1rxpIr7VtHVzHD2do", "soltGameCheckLine_149", undefined);

      odds = {
        "1": {},
        "1101": {
          "8": 200,
          "9": 200,
          "10": 500,
          "11": 500,
          "12": 1000,
          "13": 1000,
          "14": 1000,
          "15": 1000,
          "16": 1000,
          "17": 1000,
          "18": 1000,
          "19": 1000,
          "20": 1000,
          "21": 1000,
          "22": 1000,
          "23": 1000,
          "24": 1000,
          "25": 1000,
          "26": 1000,
          "27": 1000,
          "28": 1000,
          "29": 1000,
          "30": 1000
        },
        "1201": {
          "8": 50,
          "9": 50,
          "10": 200,
          "11": 200,
          "12": 500,
          "13": 500,
          "14": 500,
          "15": 500,
          "16": 500,
          "17": 500,
          "18": 500,
          "19": 500,
          "20": 500,
          "21": 500,
          "22": 500,
          "23": 500,
          "24": 500,
          "25": 500,
          "26": 500,
          "27": 500,
          "28": 500,
          "29": 500,
          "30": 500
        },
        "1301": {
          "8": 40,
          "9": 40,
          "10": 100,
          "11": 100,
          "12": 300,
          "13": 300,
          "14": 300,
          "15": 300,
          "16": 300,
          "17": 300,
          "18": 300,
          "19": 300,
          "20": 300,
          "21": 300,
          "22": 300,
          "23": 300,
          "24": 300,
          "25": 300,
          "26": 300,
          "27": 300,
          "28": 300,
          "29": 300,
          "30": 300
        },
        "1401": {
          "8": 30,
          "9": 30,
          "10": 40,
          "11": 40,
          "12": 240,
          "13": 240,
          "14": 240,
          "15": 240,
          "16": 240,
          "17": 240,
          "18": 240,
          "19": 240,
          "20": 240,
          "21": 240,
          "22": 240,
          "23": 240,
          "24": 240,
          "25": 240,
          "26": 240,
          "27": 240,
          "28": 240,
          "29": 240,
          "30": 240
        },
        "1501": {
          "8": 20,
          "9": 20,
          "10": 30,
          "11": 30,
          "12": 200,
          "13": 200,
          "14": 200,
          "15": 200,
          "16": 200,
          "17": 200,
          "18": 200,
          "19": 200,
          "20": 200,
          "21": 200,
          "22": 200,
          "23": 200,
          "24": 200,
          "25": 200,
          "26": 200,
          "27": 200,
          "28": 200,
          "29": 200,
          "30": 200
        },
        "1601": {
          "8": 16,
          "9": 16,
          "10": 24,
          "11": 24,
          "12": 160,
          "13": 160,
          "14": 160,
          "15": 160,
          "16": 160,
          "17": 160,
          "18": 160,
          "19": 160,
          "20": 160,
          "21": 160,
          "22": 160,
          "23": 160,
          "24": 160,
          "25": 160,
          "26": 160,
          "27": 160,
          "28": 160,
          "29": 160,
          "30": 160
        },
        "1701": {
          "8": 10,
          "9": 10,
          "10": 20,
          "11": 20,
          "12": 100,
          "13": 100,
          "14": 100,
          "15": 100,
          "16": 100,
          "17": 100,
          "18": 100,
          "19": 100,
          "20": 100,
          "21": 100,
          "22": 100,
          "23": 100,
          "24": 100,
          "25": 100,
          "26": 100,
          "27": 100,
          "28": 100,
          "29": 100,
          "30": 100
        },
        "1801": {
          "8": 8,
          "9": 8,
          "10": 18,
          "11": 18,
          "12": 80,
          "13": 80,
          "14": 80,
          "15": 80,
          "16": 80,
          "17": 80,
          "18": 80,
          "19": 80,
          "20": 80,
          "21": 80,
          "22": 80,
          "23": 80,
          "24": 80,
          "25": 80,
          "26": 80,
          "27": 80,
          "28": 80,
          "29": 80,
          "30": 80
        },
        "1901": {
          "8": 5,
          "9": 5,
          "10": 15,
          "11": 15,
          "12": 40,
          "13": 40,
          "14": 40,
          "15": 40,
          "16": 40,
          "17": 40,
          "18": 40,
          "19": 40,
          "20": 40,
          "21": 40,
          "22": 40,
          "23": 40,
          "24": 40,
          "25": 40,
          "26": 40,
          "27": 40,
          "28": 40,
          "29": 40,
          "30": 40
        },
        "2001": {},
        "2002": {},
        "2003": {},
        "2004": {},
        "2005": {},
        "2006": {},
        "2007": {},
        "2008": {},
        "2009": {},
        "2010": {},
        "2011": {},
        "2012": {},
        "2013": {},
        "2014": {},
        "2015": {}
      };
      topSymbols = [];
      bottom_symbol_length = 1;
      top_symbol_length = 1;
      col = 6; //列

      row = 5;
      wildSymbols = [];
      unableWinSymbol = [];
      conversIDToMulti = {
        2001: 2,
        2002: 3,
        2003: 4,
        2004: 5,
        2005: 6,
        2006: 8,
        2007: 10,
        2008: 12,
        2009: 15,
        2010: 20,
        2011: 25,
        2012: 50,
        2013: 100,
        2014: 250,
        2015: 500
      };

      _export("soltGameCheckUtil_149", soltGameCheckUtil_149 = {
        checkWon(mapArr) {
          // mapArr = _.cloneDeep(mapArr)
          let originalArr = [];

          for (let i = 0; i < mapArr.length; i++) {
            originalArr[i] = [];

            for (let j = bottom_symbol_length; j < row + bottom_symbol_length; j++) {
              originalArr[i].push(mapArr[i][j]);
            }
          } //特殊处理
          // let lastMapArr = mapArr[mapArr.length - 1]
          // lastMapArr = lastMapArr.slice(bottom_symbol_length, lastMapArr.length - top_symbol_length - 1)
          // for (let i = 0; i < 4; i++) {
          //     originalArr[i + 1].push(lastMapArr[i])
          // }


          let arr = this.conversSymbolId(originalArr);
          let colMap = {};
          let curCol = 0;
          let wonItemMap = {};
          let indexArr = [];

          for (let i = 0; i < row; i++) {
            indexArr.push(i * arr.length);
          } // arr[0][0] = 16;
          // arr[1][1] = 16;
          // arr[2][2] = 11;


          while (curCol < arr.length) {
            let curColArr = arr[curCol]; // colMap[curCol] = {};

            let lastColMap = colMap[curCol - 1];

            for (let i = 0; i < curColArr.length; i++) {
              let item = curColArr[i];

              if (unableWinSymbol.includes(+item)) {
                //不计入连线的ID
                continue;
              }

              if (!colMap[item]) {
                colMap[item] = [];
              }

              colMap[item].push(indexArr[i] + curCol);
            }

            curCol++;
          }

          let wonPosMap = {};
          let multiPosMap = {};
          const colMapKeys = Object.keys(colMap);

          if (colMapKeys && colMapKeys.length > 0) {
            for (let k = 0; k < colMapKeys.length; k++) {
              const wonValues = colMap[colMapKeys[k]];

              if (wonValues && wonValues.length >= 8) {
                wonItemMap[colMapKeys[k]] = wonValues;
                wonPosMap[colMapKeys[k]] = [];
              }

              ;

              if (parseInt(colMapKeys[k]) >= 2000) {
                //console.log('大于2000',colMap[item]);
                multiPosMap[colMapKeys[k]] = wonValues;
              }
            }
          } //找出赢的图标 图标出现在第三列说明至少3连
          // for (let iconKey in colMap[2]) {
          //     wonPosMap[iconKey] = [];
          // }


          for (let key in colMap) {
            if (wonPosMap[key]) {
              wonPosMap[key] = [...wonPosMap[key], ...colMap[key]];
            }
          }

          let wonPosArr = Object.values(wonPosMap);
          let categoryData = this.symbolCategory(arr, wonPosArr);
          categoryData.bonus_line_num = 0;
          categoryData.bonus_line_length = 0;
          let symbolWins = [];
          let allRate = 0;
          let clear = false;

          for (let item in wonItemMap) {
            if (+item <= 2000) {
              let itemArr = wonItemMap[item];
              let linkCount = 1;
              let winItem = {};
              let odd = odds[item][itemArr.length];
              let won = linkCount * odd;
              winItem['rate'] = won;
              winItem['pos'] = this.conversActPos(wonPosMap[item]); //console.log('中奖格子',item,itemArr.length,odd)

              let row = Math.floor(wonPosMap[item][0] / 6);
              let col = wonPosMap[item][0] % 6;
              winItem['bonus_symbol'] = originalArr[col][row];
              symbolWins.push(winItem);
              clear = true;
              categoryData.bonus_line_num += linkCount;
              categoryData.bonus_line_length = Math.max(itemArr.length, categoryData.bonus_line_length); // console.log(`连线${item}------${linkCount}条--------${itemArr.length}列--------赢得${won}`)

              allRate += won;
            }

            ;
          }

          ;
          let opt = {
            multiData: []
          }; //opt.multiData = [];

          for (let items in multiPosMap) {
            const itemArr = multiPosMap[items];
            const multi = conversIDToMulti[items];

            if (multi > 0) {
              let winItem = {};
              winItem['symbol'] = items;
              winItem['pos'] = this.conversActPos(itemArr);
              winItem['multi'] = itemArr.length * multi;
              opt.multiData.push(winItem);
            }
          }

          return {
            symbolWins: symbolWins,
            allRate: allRate,
            wonPosArr: wonPosArr,
            clear: clear,
            categoryData: categoryData,
            opt: opt
          };
        },

        conversSymbolId(mapArr) {
          let arr = [];

          for (let i = 0; i < mapArr.length; i++) {
            let rowArr = mapArr[i];
            arr[i] = [];

            for (let j = 0; j < rowArr.length; j++) {
              let index = rowArr[j];

              if (wildSymbols.includes(index)) {
                //百搭符号不处理
                arr[i].push(index);
                continue;
              } // let tempIndex = j;
              // while (index == 10000) {
              //     tempIndex--;
              //     if (tempIndex < 0) {
              //         throw console.error('error conversSymbolId:', JSON.stringify(mapArr));
              //     }
              //     index = rowArr[tempIndex]
              // }


              arr[i].push(index);
            }
          }

          return arr;
        },

        conversActPos(winPos) {
          let arr = [];

          for (let i = 0; i < winPos.length; i++) {
            let row = Math.floor(winPos[i] / 6);
            let col = winPos[i] % 6;
            arr.push(`${col}_${row}`);
          }

          return arr;
        },

        symbolCategory(arr, wonPosArr) {
          let winArr = [];

          for (let i = 0; i < wonPosArr.length; i++) {
            winArr = [...winArr, ...this.conversActPos(wonPosArr[i])];
          }

          let bonusSymbolCategoryMap = {};
          let symbolCategoryMap = {};
          let topSymbolNum = 0;

          for (let i = 0; i < arr.length; i++) {
            let colArr = arr[i];

            for (let j = 0; j < colArr.length; j++) {
              let pos = `${i}_${j}`;
              let symbolId = arr[i][j];
              if (topSymbols.includes(symbolId)) topSymbolNum++;

              if (winArr.includes(pos)) {
                if (!bonusSymbolCategoryMap[symbolId]) {
                  bonusSymbolCategoryMap[symbolId] = 0;
                }

                bonusSymbolCategoryMap[symbolId]++;
              }

              if (!symbolCategoryMap[symbolId]) {
                symbolCategoryMap[symbolId] = 0;
              }

              symbolCategoryMap[symbolId]++;
            }
          }

          return {
            symbolCategoryArr: Object.values(symbolCategoryMap),
            bonusSymbolCategoryArr: Object.values(bonusSymbolCategoryMap),
            bonus_symbol_toprate_num: topSymbolNum
          };
        }

      });

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=soltGameCheckLine_149.js.map
System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, Sprite, Button, director, sComponent, sAudioMgr, _dec, _dec2, _dec3, _class, _class2, _descriptor, _descriptor2, _temp, _crd, ccclass, property, sSoltFreeWinBuyCtr;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfsComponent(extras) {
    _reporterNs.report("sComponent", "./sComponent", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsAudioMgr(extras) {
    _reporterNs.report("sAudioMgr", "./sAudioMgr", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      Sprite = _cc.Sprite;
      Button = _cc.Button;
      director = _cc.director;
    }, function (_unresolved_2) {
      sComponent = _unresolved_2.sComponent;
    }, function (_unresolved_3) {
      sAudioMgr = _unresolved_3.default;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "a12619KyrRFy4G6mcMFqIZ4", "sSoltFreeWinBuyCtr", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("sSoltFreeWinBuyCtr", sSoltFreeWinBuyCtr = (_dec = ccclass('sSoltFreeWinBuyCtr'), _dec2 = property(Sprite), _dec3 = property(Button), _dec(_class = (_class2 = (_temp = class sSoltFreeWinBuyCtr extends (_crd && sComponent === void 0 ? (_reportPossibleCrUseOfsComponent({
        error: Error()
      }), sComponent) : sComponent) {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "freeWinBtnSprite", _descriptor, this);

          _initializerDefineProperty(this, "freeWinButton", _descriptor2, this);

          _defineProperty(this, "isRecord", false);

          _defineProperty(this, "menuState", 'normal');
        }

        //narmal menu ..
        start() {
          const freewin = this.node.children[0];

          if (freewin) {
            if (!this.freeWinBtnSprite) {
              this.freeWinBtnSprite = freewin.getComponent(Sprite);
            }

            if (!this.freeWinButton) {
              this.freeWinButton = freewin.getComponent(Button);
            }
          }

          director.on('freeWinBegain', () => {
            this.node.active = false;
          }, this);
          director.on('freeWinOver', () => {
            if (this.canActiveBtn()) {
              this.node.active = true;
            }
          }, this);
          director.on('rollStop', () => {
            if (this.canActiveBtn()) {
              this.renderChange(false);
              this.freeWinButton.interactable = true;
            }
          }, this);
          director.on('betBtnClick', () => {
            this.renderChange(true);
            this.freeWinButton.interactable = false;
          }, this);
          director.on('subGameMenuView', value => {
            if (value) {
              this.menuState = 'menu';
              this.renderChange(true);
              this.freeWinButton.interactable = false;
            } else {
              this.menuState = 'normal';
              this.renderChange(false);
              this.freeWinButton.interactable = true;
            }
          }, this);
          director.on('subGameBoxRecordView', (value, state) => {
            this.isRecord = value;

            if (value) {
              this.node.active = false;
            } else {
              this.node.active = true;

              if (state == 'bet' && this.canActiveBtn()) {
                this.renderChange(false);
                this.freeWinButton.interactable = true;
              }
            }
          }, this);
          this.freeWinButton.node.on('click', () => {
            (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
              error: Error()
            }), sAudioMgr) : sAudioMgr).PlayShotAudio('featureBuy_audio_click');
            director.emit('subGameFreeWinBuyBtnClick');
          }, this);
        }

        renderChange(value) {
          // const sprites = this.node.getComponentsInChildren(Sprite);
          // if(sprites && sprites.length > 0){
          //     for (let i = 0; i < sprites.length; i++) {
          //         sprites[i].grayscale = value;
          //     }
          // }
          this.freeWinBtnSprite.grayscale = value;
        }

        canActiveBtn() {
          if (this.menuState == 'normal' && !this.isRecord) {
            return true;
          }

          return false;
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "freeWinBtnSprite", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "freeWinButton", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=sSoltFreeWinBuyCtr.js.map
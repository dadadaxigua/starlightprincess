System.register(["cc"], function (_export, _context) {
  "use strict";

  var _cclegacy, v2, v3, sSlotSceneInfo, _crd;

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  _export("sSlotSceneInfo", void 0);

  return {
    setters: [function (_cc) {
      _cclegacy = _cc.cclegacy;
      v2 = _cc.v2;
      v3 = _cc.v3;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "248f2e4WiJMrbc4bCdaCdkj", "sSlotSceneInfo", undefined);

      _export("sSlotSceneInfo", sSlotSceneInfo = class sSlotSceneInfo {
        static SetBoxSize(x, y) {
          sSlotSceneInfo.BoxSize = v3(x, y, 0);
        }

        static SetBoxSum(x, y) {
          sSlotSceneInfo.BoxSum = v2(x, y);
        }

        static SetBoxViewSum(x, y) {
          sSlotSceneInfo.BoxViewSum = v2(x, y);
        }

      });

      _defineProperty(sSlotSceneInfo, "BoxSize", v3(0, 0));

      _defineProperty(sSlotSceneInfo, "BoxSum", v2(0, 0));

      _defineProperty(sSlotSceneInfo, "BoxViewSum", v2(0, 0));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=sSlotSceneInfo.js.map
System.register(["cc"], function (_export, _context) {
  "use strict";

  var _cclegacy, _crd;

  return {
    setters: [function (_cc) {
      _cclegacy = _cc.cclegacy;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "d0124RmhXZPppId3IkJHvdB", "chatPushProtos", undefined);

      _export("default", {
        "nested": {
          "chat": {
            "nested": {
              "msg": {
                "nested": {
                  "one_by_one_chat": {
                    "fields": {
                      "chan": {
                        "type": "string",
                        "id": 1
                      },
                      "type": {
                        "type": "int32",
                        "id": 2
                      },
                      "uid": {
                        "type": "string",
                        "id": 3
                      },
                      "id": {
                        "type": "int32",
                        "id": 4
                      },
                      "msg": {
                        "type": "string",
                        "id": 5
                      },
                      "time": {
                        "type": "int32",
                        "id": 6
                      }
                    }
                  },
                  "desk_chat": {
                    "fields": {
                      "chan": {
                        "type": "string",
                        "id": 1
                      },
                      "type": {
                        "type": "int32",
                        "id": 2
                      },
                      "uid": {
                        "type": "string",
                        "id": 3
                      },
                      "id": {
                        "type": "int32",
                        "id": 4
                      },
                      "msg": {
                        "type": "string",
                        "id": 5
                      },
                      "time": {
                        "type": "int32",
                        "id": 6
                      }
                    }
                  },
                  "world_chat": {
                    "fields": {
                      "chan": {
                        "type": "string",
                        "id": 1
                      },
                      "type": {
                        "type": "int32",
                        "id": 2
                      },
                      "uid": {
                        "type": "string",
                        "id": 3
                      },
                      "id": {
                        "type": "int32",
                        "id": 4
                      },
                      "msg": {
                        "type": "string",
                        "id": 5
                      },
                      "time": {
                        "type": "int32",
                        "id": 6
                      }
                    }
                  },
                  "chatNotice": {
                    "fields": {
                      "time": {
                        "type": "int64",
                        "id": 1
                      },
                      "type": {
                        "type": "string",
                        "id": 2
                      },
                      "msg": {
                        "type": "string",
                        "id": 3
                      },
                      "sender_id": {
                        "type": "string",
                        "id": 4
                      }
                    }
                  },
                  "broadcastNotice": {
                    "fields": {
                      "type": {
                        "type": "string",
                        "id": 1
                      },
                      "msg": {
                        "type": "Msg",
                        "id": 2
                      }
                    },
                    "nested": {
                      "Msg": {
                        "fields": {
                          "content": {
                            "type": "string",
                            "id": 1
                          }
                        }
                      }
                    }
                  },
                  "message": {
                    "fields": {
                      "room_id": {
                        "type": "int32",
                        "id": 1
                      },
                      "user": {
                        "type": "User",
                        "id": 2
                      },
                      "content": {
                        "type": "string",
                        "id": 3
                      },
                      "send_time": {
                        "type": "int64",
                        "id": 4
                      }
                    },
                    "nested": {
                      "User": {
                        "fields": {
                          "uid": {
                            "type": "string",
                            "id": 1
                          },
                          "nickname": {
                            "type": "string",
                            "id": 2
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      });

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=chatPushProtos.js.map
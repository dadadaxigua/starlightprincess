System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, AudioClip, Component, SpriteAtlas, sAudioMgr, assetMgr, _dec, _dec2, _class, _class2, _descriptor, _descriptor2, _temp, _crd, ccclass, property, sSlotBtnAssetInit;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfsAudioMgr(extras) {
    _reporterNs.report("sAudioMgr", "../../../game/core/sAudioMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfassetMgr(extras) {
    _reporterNs.report("assetMgr", "../../../game/core/sAssetMgr", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      AudioClip = _cc.AudioClip;
      Component = _cc.Component;
      SpriteAtlas = _cc.SpriteAtlas;
    }, function (_unresolved_2) {
      sAudioMgr = _unresolved_2.default;
    }, function (_unresolved_3) {
      assetMgr = _unresolved_3.assetMgr;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "17a466m4kJKTbFBmo35i3hq", "sSlotBtnAssetInit", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("sSlotBtnAssetInit", sSlotBtnAssetInit = (_dec = ccclass('sSlotBtnAssetInit'), _dec2 = property({
        tooltip: '音频的资源引用',
        type: [AudioClip]
      }), _dec(_class = (_class2 = (_temp = class sSlotBtnAssetInit extends Component {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "gameAudioClip", _descriptor, this);

          _initializerDefineProperty(this, "isLoadLanguageImg", _descriptor2, this);
        }

        onLoad() {
          if (this.gameAudioClip && this.gameAudioClip.length) {
            for (let i = 0; i < this.gameAudioClip.length; i++) {
              const clip = this.gameAudioClip[i];
              (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                error: Error()
              }), sAudioMgr) : sAudioMgr).SetAudioClip(clip);
            }
          }
        }

        start() {
          let language = 'EN';

          if (globalThis.GetLanguageType) {
            language = globalThis.GetLanguageType();
          }

          const bundle = globalThis.lobbyBundle;

          if (bundle) {
            bundle.load('hyPrefabs/gameRes/external/config/img/' + language, SpriteAtlas, (err, spriteAtlas) => {
              if (spriteAtlas) {
                (_crd && assetMgr === void 0 ? (_reportPossibleCrUseOfassetMgr({
                  error: Error()
                }), assetMgr) : assetMgr).SetSpriteFramesToDic(spriteAtlas);
              } else {// console.warn('subGameLanguageImg:'+err);
              }
            });
          }
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "gameAudioClip", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return [];
        }
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "isLoadLanguageImg", [property], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return true;
        }
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=sSlotBtnAssetInit.js.map
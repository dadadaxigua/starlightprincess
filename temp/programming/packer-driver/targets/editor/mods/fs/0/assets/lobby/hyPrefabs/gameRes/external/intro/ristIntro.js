System.register(["__unresolved_0", "cc", "__unresolved_1"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, assetManager, Input, instantiate, Node, size, Sprite, SpriteFrame, tween, UITransform, v3, Vec3, sComponent, _dec, _dec2, _dec3, _dec4, _dec5, _dec6, _dec7, _dec8, _dec9, _dec10, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _descriptor5, _descriptor6, _descriptor7, _descriptor8, _descriptor9, _temp, _crd, ccclass, property, ristIntro;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfsComponent(extras) {
    _reporterNs.report("sComponent", "../../../../game/core/sComponent", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      assetManager = _cc.assetManager;
      Input = _cc.Input;
      instantiate = _cc.instantiate;
      Node = _cc.Node;
      size = _cc.size;
      Sprite = _cc.Sprite;
      SpriteFrame = _cc.SpriteFrame;
      tween = _cc.tween;
      UITransform = _cc.UITransform;
      v3 = _cc.v3;
      Vec3 = _cc.Vec3;
    }, function (_unresolved_2) {
      sComponent = _unresolved_2.sComponent;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "645b4Mpn0VLWIFMj2jRHa8c", "ristIntro", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("ristIntro", ristIntro = (_dec = ccclass('ristIntro'), _dec2 = property(Node), _dec3 = property(UITransform), _dec4 = property(UITransform), _dec5 = property(Node), _dec6 = property(Node), _dec7 = property(Node), _dec8 = property(Node), _dec9 = property(Node), _dec10 = property(Node), _dec(_class = (_class2 = (_temp = class ristIntro extends (_crd && sComponent === void 0 ? (_reportPossibleCrUseOfsComponent({
        error: Error()
      }), sComponent) : sComponent) {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "mainNode", _descriptor, this);

          _initializerDefineProperty(this, "scrollView", _descriptor2, this);

          _initializerDefineProperty(this, "content", _descriptor3, this);

          _initializerDefineProperty(this, "itemCopy", _descriptor4, this);

          _initializerDefineProperty(this, "toggleCopy", _descriptor5, this);

          _initializerDefineProperty(this, "toggles", _descriptor6, this);

          _initializerDefineProperty(this, "leftBtn", _descriptor7, this);

          _initializerDefineProperty(this, "rightBtn", _descriptor8, this);

          _initializerDefineProperty(this, "closeArea", _descriptor9, this);

          _defineProperty(this, "itemSize", void 0);

          _defineProperty(this, "scrollXStart", 0);

          _defineProperty(this, "isScolling", false);

          _defineProperty(this, "hasScroll", false);

          _defineProperty(this, "currentIndex", 0);

          _defineProperty(this, "imgTotal", 0);

          _defineProperty(this, "touchEnable", false);
        }

        get CurrentIndex() {
          return this.currentIndex;
        }

        set CurrentIndex(value) {
          this.currentIndex = value < 0 ? 0 : value < this.imgTotal ? value : this.imgTotal - 1;
        }

        start() {
          if (this.closeArea) {
            this.closeArea.on('click', () => {
              this.node.destroy();
            }, this);
          }

          console.log('this.scrollView:' + this.scrollView.contentSize.x);
          this.scheduleOnce(() => {
            console.log('this.scrollView:' + this.scrollView.contentSize.x);
          }, 1);
          this.scrollView.node.on(Input.EventType.TOUCH_START, event => {
            this.scrollXStart = event.getLocationX();
          }, this);
          this.scrollView.node.on(Input.EventType.TOUCH_MOVE, event => {
            if (this.touchEnable && Math.abs(event.getLocationX() - this.scrollXStart) > 30) {
              if (!this.hasScroll && !this.isScolling) {
                this.hasScroll = true;
                this.isScolling = true;
                this.rightScroll(event.getLocationX() - this.scrollXStart > 0 ? -1 : 1);
              }
            }
          }, this);

          let endCall = event => {
            this.hasScroll = false;
          };

          this.scrollView.node.on(Input.EventType.TOUCH_END, endCall, this);
          this.scrollView.node.on(Input.EventType.TOUCH_CANCEL, endCall, this);
          this.leftBtn.on('click', () => {
            if (this.touchEnable) {
              this.rightScroll(-1);
            }
          }, this);
          this.rightBtn.on('click', () => {
            if (this.touchEnable) {
              this.rightScroll(1);
            }
          }, this);
          this.onView('intro/rule/' + (globalThis.GetLanguageType ? globalThis.GetLanguageType() : 'EN'));
        }

        setSize() {
          if (this.scrollView) {
            this.itemSize = size(this.scrollView.contentSize);
          }
        }

        rightScroll(dir) {
          if (dir == 1) {
            this.CurrentIndex++;
          } else {
            this.CurrentIndex--;
          }

          tween(this.content.node).to(0.2, {
            position: v3(-(this.CurrentIndex * this.itemSize.x), 0, 0)
          }, {
            easing: 'sineOut'
          }).call(() => {
            this.isScolling = false;
          }).start();
          this.setToggleInde(this.CurrentIndex);
          this.arrowBtnUpdate(this.CurrentIndex);
        }

        arrowBtnUpdate(index) {
          if (index == 0) {
            this.leftBtn.children[0].active = false;
            this.rightBtn.children[0].active = true;
          } else if (index == this.imgTotal - 1) {
            this.leftBtn.children[0].active = true;
            this.rightBtn.children[0].active = false;
          } else {
            this.leftBtn.children[0].active = true;
            this.rightBtn.children[0].active = true;
          }
        }

        setToggleInde(index) {
          this.cleanTweenList();

          for (let i = 0; i < this.toggles.children.length; i++) {
            let element = this.toggles.children[i];
            let fore = element.children[1];

            if (index == i) {
              this.pushOneTween(tween(fore).to(0.2, {
                scale: Vec3.ONE
              }, {
                easing: 'sineOut'
              }).start());
            } else {
              this.pushOneTween(tween(fore).to(0.2, {
                scale: Vec3.ZERO
              }, {
                easing: 'sineOut'
              }).start());
            }
          }
        }

        onView(path) {
          if (this.itemCopy && this.content) {
            const bundle = assetManager.getBundle(globalThis.currentPlayingGameID);

            if (bundle) {
              bundle.loadDir(path, SpriteFrame, (err, assets) => {
                if (assets && Array.isArray(assets) && assets.length > 0) {
                  if (assets.length > 1) {
                    this.leftBtn.active = true;
                    this.rightBtn.active = true;
                    this.toggles.active = true;
                  }

                  this.setSize();
                  let dic = assets.sort((a, b) => {
                    let indexs1 = a.name.split('_');
                    let indexs2 = b.name.split('_');

                    if (indexs1 && indexs2) {
                      let num1 = parseInt(indexs1[indexs1.length - 1]);
                      let num2 = parseInt(indexs2[indexs2.length - 1]);

                      if (!isNaN(num1) && !isNaN(num2)) {
                        return num1 - num2;
                      }
                    }

                    return 0;
                  });

                  for (let i = 0; i < dic.length; i++) {
                    const asset = dic[i];
                    let obj = instantiate(this.itemCopy);
                    obj.setParent(this.itemCopy.parent, false);
                    obj.active = true;
                    obj.position = v3(i * this.itemSize.x + this.itemSize.x / 2, 0, 0);
                    obj.getComponent(UITransform).setContentSize(this.itemSize.x, this.itemSize.y);
                    const img = obj.getChildByName('img');

                    if (img) {
                      img.getComponent(Sprite).spriteFrame = asset;
                    }

                    if (this.toggleCopy && this.toggles) {
                      let tol = instantiate(this.toggleCopy);
                      tol.setParent(this.toggles, false);
                      tol.active = true;
                    }
                  }

                  this.imgTotal = dic.length;
                  this.content.setContentSize(dic.length * this.itemSize.x, this.itemSize.y);
                  this.setToggleInde(this.CurrentIndex);
                  this.touchEnable = true;
                }
              });
            }
          }
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "mainNode", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "scrollView", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "content", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "itemCopy", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor5 = _applyDecoratedDescriptor(_class2.prototype, "toggleCopy", [_dec6], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor6 = _applyDecoratedDescriptor(_class2.prototype, "toggles", [_dec7], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor7 = _applyDecoratedDescriptor(_class2.prototype, "leftBtn", [_dec8], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor8 = _applyDecoratedDescriptor(_class2.prototype, "rightBtn", [_dec9], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor9 = _applyDecoratedDescriptor(_class2.prototype, "closeArea", [_dec10], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=ristIntro.js.map
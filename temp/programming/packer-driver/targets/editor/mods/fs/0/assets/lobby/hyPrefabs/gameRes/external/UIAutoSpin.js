System.register(["cc"], function (_export, _context) {
  "use strict";

  var _cclegacy, _decorator, Component, Node, Label, color, Widget, UITransform, tween, v3, director, Sprite, Renderable2D, Button, _dec, _dec2, _dec3, _dec4, _dec5, _dec6, _dec7, _dec8, _dec9, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _descriptor5, _descriptor6, _descriptor7, _descriptor8, _temp, _crd, ccclass, property, UIAutoSpin;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  return {
    setters: [function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      Component = _cc.Component;
      Node = _cc.Node;
      Label = _cc.Label;
      color = _cc.color;
      Widget = _cc.Widget;
      UITransform = _cc.UITransform;
      tween = _cc.tween;
      v3 = _cc.v3;
      director = _cc.director;
      Sprite = _cc.Sprite;
      Renderable2D = _cc.Renderable2D;
      Button = _cc.Button;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "d1e3fV5kBFDLqWYqZNgOwp+", "UIAutoSpin", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("UIAutoSpin", UIAutoSpin = (_dec = ccclass('UIAutoSpin'), _dec2 = property(Node), _dec3 = property(Node), _dec4 = property(Node), _dec5 = property(Node), _dec6 = property(Node), _dec7 = property(Label), _dec8 = property(Label), _dec9 = property(Label), _dec(_class = (_class2 = (_temp = class UIAutoSpin extends Component {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "btnNode", _descriptor, this);

          _initializerDefineProperty(this, "mainNode", _descriptor2, this);

          _initializerDefineProperty(this, "contentNode", _descriptor3, this);

          _initializerDefineProperty(this, "closeBtn", _descriptor4, this);

          _initializerDefineProperty(this, "comfirmBtn", _descriptor5, this);

          _initializerDefineProperty(this, "ownLabel", _descriptor6, this);

          _initializerDefineProperty(this, "betLabel", _descriptor7, this);

          _initializerDefineProperty(this, "winLabel", _descriptor8, this);

          _defineProperty(this, "btnColor", void 0);

          _defineProperty(this, "autoSpinCount", 0);

          _defineProperty(this, "touchEnable", true);
        }

        onLoad() {
          this.node.getComponent(Widget).updateAlignment();
        }

        start() {
          let country = '';

          if (globalThis.getCountry) {
            const con = globalThis.getCountry();

            if (con) {
              country = con;
            }
          }

          if (country == 'VNM') {
            if (this.betLabel) {
              const vnIcon = this.betLabel.node.parent.getChildByName('vnIcon');

              if (vnIcon) {
                vnIcon.active = true;
              }
            }
          } else {
            if (this.betLabel) {
              const icon = this.betLabel.node.parent.getChildByName('icon');

              if (icon) {
                icon.active = true;
              }
            }
          }

          this.closeBtn.on('click', () => {
            globalThis.subGamePlayShotAudio('subGameClick');
            this.autoSpinCount = 0;
            this.disappearViewAnima();
          }, this);
          this.comfirmBtn.on('click', () => {
            globalThis.subGamePlayShotAudio('subGameClick');
            this.disappearViewAnima();
          }, this);
        }

        onEnable() {
          this.comfirmBtn.getComponent(Button).interactable = false;
          const col = this.comfirmBtn.getComponent(Sprite).color;
          this.btnColor = color(col.r, col.g, col.b, 255);
          this.comfirmBtn.getComponent(Sprite).color = color(this.btnColor.r, this.btnColor.g, this.btnColor.b, 117);

          if (this.btnNode) {
            for (let i = 0; i < this.btnNode.children.length; i++) {
              const element = this.btnNode.children[i];
              const label = element.children[1].getComponent(Renderable2D);

              if (label) {
                label.color = color(134, 134, 134, 255);
              }
            }
          }
        }

        btnClick(event, customEventData) {
          globalThis.subGamePlayShotAudio('subGameClick');
          this.autoSpinCount = customEventData;
          this.comfirmBtn.getComponent(Button).interactable = true;
          this.comfirmBtn.getComponent(Sprite).color = color(this.btnColor.r, this.btnColor.g, this.btnColor.b, 255);

          if (this.btnNode) {
            for (let i = 0; i < this.btnNode.children.length; i++) {
              const element = this.btnNode.children[i];
              const label = element.children[1].getComponent(Renderable2D);

              if (label) {
                label.color = color(134, 134, 134, 255);
              }
            }
          }

          let target = event.target;

          if (target) {
            target.children[1].getComponent(Renderable2D).color = color(this.btnColor.r, this.btnColor.g, this.btnColor.b, 255);
          }
        }

        betInfoSet(coin, bet, win) {
          this.ownLabel.string = this.AddCommas(coin);
          this.betLabel.string = this.AddCommas(bet);
          this.winLabel.string = this.AddCommas(win);
        }

        appearViewAnima() {
          const viewHeight = this.node.getComponent(UITransform).contentSize.height;
          const mainNode = this.mainNode;
          const panelHeight = mainNode.getComponent(UITransform).height; // const opa = this.contentNode.getComponent(UIOpacity);
          // opa.opacity = 0;

          tween(mainNode).to(0.3, {
            position: v3(0, panelHeight - viewHeight / 2, 0)
          }, {
            easing: 'sineOut'
          }).call(() => {// tween(opa).to(0.2,{opacity:255}).call(()=>{
            // }).start();
          }).start();
        }

        disappearViewAnima() {
          if (this.touchEnable) {
            this.touchEnable = false;
            const viewHeight = this.node.getComponent(UITransform).contentSize.height;
            const mainNode = this.mainNode;
            tween(mainNode).to(0.3, {
              position: v3(0, -viewHeight / 2, 0)
            }, {
              easing: 'sineOut'
            }).call(() => {
              this.touchEnable = true;
              this.node.active = false;
              director.emit('viewChange', 'UIAutoSpin', this.autoSpinCount);
            }).start();
          }
        }

        AddCommas(money) {
          if (!money) {
            return "0";
          } else {
            let mon;

            if (typeof money == "string") {
              mon = Number(money);
            } else {
              mon = money;
            }

            let coinRate = 1;

            if (globalThis.getCoinRate) {
              coinRate = globalThis.getCoinRate();
            }

            let byte = coinRate.toString().length - 1;
            ;
            let showMoney = (mon / coinRate).toFixed(byte);
            let tempmoney = String(showMoney);
            var left = tempmoney.split('.')[0],
                right = tempmoney.split('.')[1];
            right = right ? right.length >= byte ? '.' + right.substring(0, byte) : '.' + right : '';
            var temp = left.split('').reverse().join('').match(/(\d{1,3})/g);

            if (temp) {
              return (Number(tempmoney) < 0 ? "-" : "") + temp.join(',').split('').reverse().join('') + right;
            } else {
              return '';
            }
          }
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "btnNode", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "mainNode", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "contentNode", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "closeBtn", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor5 = _applyDecoratedDescriptor(_class2.prototype, "comfirmBtn", [_dec6], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor6 = _applyDecoratedDescriptor(_class2.prototype, "ownLabel", [_dec7], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor7 = _applyDecoratedDescriptor(_class2.prototype, "betLabel", [_dec8], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor8 = _applyDecoratedDescriptor(_class2.prototype, "winLabel", [_dec9], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=UIAutoSpin.js.map
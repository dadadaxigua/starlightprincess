System.register(["cc"], function (_export, _context) {
  "use strict";

  var _cclegacy, _crd, InLobbySettings;

  return {
    setters: [function (_cc) {
      _cclegacy = _cc.cclegacy;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "fa9bfqL3AJKE47BozT5CTAp", "setting", undefined);

      InLobbySettings = {
        audioBgVolume: 1,
        //背景音乐
        audioSoundEffectVolume: 1,
        //音效
        fullView: 1 //全面屏全屏

      };

      _export("default", InLobbySettings);

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=setting.js.map
System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2", "__unresolved_3"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, color, director, instantiate, Material, MeshRenderer, Node, utils, v2, v3, sComponent, sObjPool, sUtil, _dec, _dec2, _dec3, _dec4, _class, _class2, _descriptor, _descriptor2, _descriptor3, _temp, _crd, ccclass, property, slotLinesMgr;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfsComponent(extras) {
    _reporterNs.report("sComponent", "../../../../game/core/sComponent", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsObjPool(extras) {
    _reporterNs.report("sObjPool", "../../../../game/core/sObjPool", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsUtil(extras) {
    _reporterNs.report("sUtil", "../../../../game/core/sUtil", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      color = _cc.color;
      director = _cc.director;
      instantiate = _cc.instantiate;
      Material = _cc.Material;
      MeshRenderer = _cc.MeshRenderer;
      Node = _cc.Node;
      utils = _cc.utils;
      v2 = _cc.v2;
      v3 = _cc.v3;
    }, function (_unresolved_2) {
      sComponent = _unresolved_2.sComponent;
    }, function (_unresolved_3) {
      sObjPool = _unresolved_3.sObjPool;
    }, function (_unresolved_4) {
      sUtil = _unresolved_4.sUtil;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "bf0f4jekdpJC5rrO97Wj/Ce", "slotLinesMgr", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("slotLinesMgr", slotLinesMgr = (_dec = ccclass('slotLinesMgr'), _dec2 = property(Node), _dec3 = property(Node), _dec4 = property(Material), _dec(_class = (_class2 = (_temp = class slotLinesMgr extends (_crd && sComponent === void 0 ? (_reportPossibleCrUseOfsComponent({
        error: Error()
      }), sComponent) : sComponent) {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "lineCopy", _descriptor, this);

          _initializerDefineProperty(this, "linesContainer", _descriptor2, this);

          _initializerDefineProperty(this, "renderMat", _descriptor3, this);

          _defineProperty(this, "borderSize", void 0);

          _defineProperty(this, "initialIndexPos", void 0);

          _defineProperty(this, "lineIndexSize", void 0);

          _defineProperty(this, "lineSize", 0.02);

          _defineProperty(this, "pixelSize", void 0);

          _defineProperty(this, "pixelRealXSize", void 0);

          _defineProperty(this, "pos", void 0);

          _defineProperty(this, "lineList", []);
        }

        // protected update(dt: number): void {
        //     let iii = 255;
        //     this.renderMat.setProperty('mainColor',color(255,255,255,iii--));
        // }
        onLoad() {
          (_crd && sObjPool === void 0 ? (_reportPossibleCrUseOfsObjPool({
            error: Error()
          }), sObjPool) : sObjPool).SetObj('slotLine', () => {
            if (this.lineCopy) {
              const line = instantiate(this.lineCopy);

              if (line) {
                return line.getComponent(MeshRenderer);
              }
            }

            return null;
          });
          director.on('slotLineOpen', (dataArr, _color) => {
            if (dataArr && Array.isArray(dataArr)) {
              for (let i = 0; i < dataArr.length; i++) {
                const datas = dataArr[i];

                if (datas && Array.isArray(datas)) {
                  this.setOneLineByIndexPath(datas, _color);
                }
              }
            } // let iii = 255;
            // sUtil.TweenColorFrame(color(255,255,255,255),color(255,255,255,0),3,0,()=>{},(_color : Color)=>{
            //     console.log('_color:'+_color);
            //     this.renderMat.setProperty('mainColor',color(255,255,255,iii--));
            // });
            // this.renderMat.setProperty('mainColor',color(255,255,255,120));
            // this.scheduleOnce(()=>{
            //     this.renderMat.setProperty('mainColor',color(255,255,255,244));
            // },2);

          }, this);
          director.on('slotLineClose', () => {
            if (this.lineList && this.lineList.length > 0) {
              for (let i = 0; i < this.lineList.length; i++) {
                const line = this.lineList[i];
                const h = line.getRenderMaterial(0).passes[0].getHandle('mainColor');
                this.removeIDTween(line.node.uuid);
                this.pushIDTween((_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                  error: Error()
                }), sUtil) : sUtil).TweenColorFrame(color(255, 255, 255, 255), color(255, 255, 255, 0), 0.3, 0, () => {
                  (_crd && sObjPool === void 0 ? (_reportPossibleCrUseOfsObjPool({
                    error: Error()
                  }), sObjPool) : sObjPool).Enqueue('slotLine', line);
                  line.node.active = false;
                }, _color => {
                  line.getRenderMaterial(0).passes[0].setUniform(h, _color);
                }), line.node.uuid);
              }

              this.lineList = [];
            }
          }, this);
        }

        start() {}

        Init(boxSumSize, pixelSize, pos) {
          if (boxSumSize && boxSumSize.x > 0 && boxSumSize.y > 0) {
            this.borderSize = v2(boxSumSize.x, boxSumSize.y);
            this.initialIndexPos = v2(1 / (this.borderSize.x * 2) - 0.5, 1 / (this.borderSize.y * 2) - 0.5);
            this.lineIndexSize = v2(1 / this.borderSize.x, 1 / this.borderSize.y); // this.initialIndexPos = v2(this.lineIndexSize.x - 0.5,this.lineIndexSize.y - 0.5);

            this.pixelSize = v3(pixelSize);
            this.pixelRealXSize = pixelSize.x / pixelSize.y;
            this.pos = v3(pos);
            console.log('this.borderSize:' + this.borderSize + ' this.initialPos:' + this.initialIndexPos + ' ');
          } else {
            console.warn('slotLinesMgr init error');
          }
        }

        setOneLineByIndexPath(indexs, _color) {
          // console.log('setOneLineByIndexPath:'+_color);
          if (indexs && Array.isArray(indexs) && indexs.length > 1) {
            const lineRender = (_crd && sObjPool === void 0 ? (_reportPossibleCrUseOfsObjPool({
              error: Error()
            }), sObjPool) : sObjPool).Dequeue('slotLine');

            if (lineRender) {
              this.lineList.push(lineRender);
              lineRender.node.parent = this.linesContainer;
              lineRender.node.active = true;
              lineRender.node.position = this.pos;
              lineRender.node.scale = v3(this.pixelSize.y, this.pixelSize.y, 1);
              const h = lineRender.getRenderMaterial(0).passes[0].getHandle('mainColor');
              lineRender.getRenderMaterial(0).passes[0].setUniform(h, color(255, 255, 255, 0));
              this.removeIDTween(lineRender.node.uuid);
              this.pushIDTween((_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                error: Error()
              }), sUtil) : sUtil).TweenColorFrame(color(255, 255, 255, 0), color(255, 255, 255, 255), 0.3, 0, () => {}, _color => {
                lineRender.getRenderMaterial(0).passes[0].setUniform(h, _color);
              }), lineRender.node.uuid);

              if (indexs[0] && Array.isArray(indexs[0]) && indexs[0].length > 1 && indexs[1] && Array.isArray(indexs[1]) && indexs[1].length > 1) {
                const positions = [-this.pixelRealXSize / 2, this.initialIndexPos.y + indexs[0][1] * this.lineIndexSize.y - this.lineSize * 0.5, 0, -this.pixelRealXSize / 2, this.initialIndexPos.y + indexs[0][1] * this.lineIndexSize.y + this.lineSize * 0.5, 0];
                const uvs = [0, 1, 0, 0];

                for (let i = 0; i < indexs.length; i++) {
                  const index = indexs[i];

                  if (index && Array.isArray(index) && index.length == 2) {
                    // positions.push(
                    //     this.initialIndexPos.x + index[0] * this.lineIndexSize.x,this.initialIndexPos.y + index[1] * this.lineIndexSize.y,0,
                    //     this.initialIndexPos.x + index[0] * this.lineIndexSize.x - (i == 0 ? this.lineSize * 0.3 : 0),this.initialIndexPos.y + index[1] * this.lineIndexSize.y + (i == 0 ? this.lineSize * 0.85 : this.lineSize),0
                    // );
                    positions.push((this.initialIndexPos.x + index[0] * this.lineIndexSize.x) * this.pixelRealXSize, this.initialIndexPos.y + index[1] * this.lineIndexSize.y - this.lineSize * 0.5, 0, (this.initialIndexPos.x + index[0] * this.lineIndexSize.x) * this.pixelRealXSize, this.initialIndexPos.y + index[1] * this.lineIndexSize.y + this.lineSize * 0.5, 0);
                    uvs.push(this.initialIndexPos.x + index[0] * this.lineIndexSize.x + 0.5, 1, this.initialIndexPos.x + index[0] * this.lineIndexSize.x + 0.5, 0);
                  }
                }

                positions.push(this.pixelRealXSize / 2, this.initialIndexPos.y + indexs[indexs.length - 1][1] * this.lineIndexSize.y - this.lineSize * 0.5, 0, this.pixelRealXSize / 2, this.initialIndexPos.y + indexs[indexs.length - 1][1] * this.lineIndexSize.y + this.lineSize * 0.5, 0);
                uvs.push(1, 1, 1, 0);
                const indicesLength = (positions.length / 3 - 2) / 2;
                const indices = [];

                for (let i = 0; i < indicesLength; i++) {
                  indices.push(0 + i * 2, 2 + i * 2, 1 + i * 2, 2 + i * 2, 3 + i * 2, 1 + i * 2);
                }

                const colors = [];

                if (_color) {
                  const colorsLength = positions.length / 3;

                  for (let i = 0; i < colorsLength; i++) {
                    // console.log('color:'+_color);
                    colors.push(_color.r / 255, _color.g / 255, _color.b / 255, _color.a / 255); // colors.push(1,0.9,0,1);
                  }
                }

                for (let i = 0; i < indexs.length; i++) {
                  const index = indexs[i];

                  if (index.length == 2) {// console.log(index[1]);
                  }
                }

                if (positions && positions.length / 6 >= 3) {
                  const len = positions.length / 6 - 1;
                  const rrr = 0.3;

                  for (let i = 1; i < len; i++) {
                    const lastDownYPos = positions[(i - 1) * 6 + 1];
                    const currentDownYPos = positions[i * 6 + 1];
                    const nextDownYPos = positions[(i + 1) * 6 + 1];
                    const leftSlope = (currentDownYPos - lastDownYPos) / this.lineIndexSize.y;
                    const rightSlope = (nextDownYPos - currentDownYPos) / this.lineIndexSize.y;
                    let upDeltaX = 0,
                        upDeltaY = 0;

                    if (leftSlope == 0 || rightSlope == 0) {
                      if (leftSlope != 0) {
                        upDeltaX = this.lineSize * rrr * -leftSlope;
                      }

                      if (rightSlope != 0) {
                        upDeltaX = this.lineSize * rrr * -rightSlope;
                      }
                    } else {
                      // console.log('yyyyy:'+(Math.abs(leftSlope) + Math.abs(rightSlope)) / 2);
                      const mLeftSlope = Math.abs(leftSlope),
                            mRightSlope = Math.abs(rightSlope);

                      if (mLeftSlope != mRightSlope) {
                        const sss = Math.abs(leftSlope - rightSlope);

                        if (leftSlope > 0) {
                          if (mLeftSlope > mRightSlope) {
                            upDeltaX = -this.lineSize * rrr * Math.pow(sss, 0.1);
                          } else {
                            upDeltaX = this.lineSize * rrr * Math.pow(sss, 0.1);
                          }
                        } else if (leftSlope < 0) {
                          if (mLeftSlope > mRightSlope) {
                            upDeltaX = this.lineSize * rrr * Math.pow(sss, 0.1);
                          } else {
                            upDeltaX = -this.lineSize * rrr * Math.pow(sss, 0.1);
                          }
                        }

                        upDeltaY = Math.pow(sss, 0.5) * this.lineSize * rrr;
                      } else {
                        upDeltaY = (Math.abs(leftSlope) + Math.abs(rightSlope)) / 2 * this.lineSize * rrr * 2;
                      }
                    }

                    positions[i * 6 + 3] += upDeltaX;
                    positions[i * 6 + 4] += upDeltaY; // uvs[i * 4] += downDeltaX;

                    uvs[i * 4 + 2] += upDeltaX;
                    uvs[i * 4 + 3] -= upDeltaY; // console.log('currentDownYPos:'+(currentDownYPos / this.lineIndexSize.y),'leftSlope:'+(leftSlope),'rightSlope:'+rightSlope);
                    // console.log('upDeltaX:'+upDeltaX+',upDeltaY:'+upDeltaY);
                  }
                }

                let model = {
                  "positions": positions,
                  "uvs": uvs,
                  "indices": indices,
                  "colors": colors
                }; // const ppp = [
                //     -1,-0.5,0,
                //     -1,0.5,0,
                //     -0.25,-0.5,0,
                //     -0.25,0.5,0,
                //     0,-0.5,0,
                //     0,0.5,0,
                //     0.25,-0.5,0,
                //     0.25,0.5,0,
                //     0.5,-0.5,0,
                //     0.5,0.5,0,
                // ];
                // const indicesppLength = (ppp.length / 3 - 2) / 2;
                // const indicespp = [];
                // for (let i = 0; i < indicesppLength; i++)
                // {
                //     indicespp.push(
                //         0 + i * 2 , 2 + i * 2 , 1 + i * 2,
                //         2 + i * 2 , 3 + i * 2 , 1 + i * 2
                //     );
                // }
                // let ss : primitives.IGeometry = {
                //     "positions":ppp
                //     ,
                //     "uvs":
                //     [
                //         0,1,
                //         0,0,
                //         0.25,1,
                //         0.25,0,
                //         0.5,1,
                //         0.5,0,
                //         0.75,1,
                //         0.75,0,
                //         1,1,
                //         1,0
                //     ],
                //     "indices":
                //     indicespp,
                // };
                // lineRender.mesh = utils.createMesh(ss,null,{calculateBounds : false});

                lineRender.mesh = utils.createMesh(model, null, {
                  calculateBounds: false
                });
              }
            }
          }
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "lineCopy", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "linesContainer", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "renderMat", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=slotLinesMgr.js.map
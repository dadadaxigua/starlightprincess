System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2", "__unresolved_3"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, gameM, userM, settingM, _crd;

  function _reportPossibleCrUseOfgameM(extras) {
    _reporterNs.report("gameM", "../global/game", _context.meta, extras);
  }

  function _reportPossibleCrUseOfuserM(extras) {
    _reporterNs.report("userM", "../global/user", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsettingM(extras) {
    _reporterNs.report("settingM", "../global/setting", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
    }, function (_unresolved_2) {
      gameM = _unresolved_2.default;
    }, function (_unresolved_3) {
      userM = _unresolved_3.default;
    }, function (_unresolved_4) {
      settingM = _unresolved_4.default;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "a9031Ia1oRCpaKlIK7tpU19", "global-entry", undefined);

      // /**
      //  * 全局数据的引用
      //  */
      _export("default", {
        game: _crd && gameM === void 0 ? (_reportPossibleCrUseOfgameM({
          error: Error()
        }), gameM) : gameM,
        user: _crd && userM === void 0 ? (_reportPossibleCrUseOfuserM({
          error: Error()
        }), userM) : userM,
        // nodes_cache : require('./node-cache'),
        setting: _crd && settingM === void 0 ? (_reportPossibleCrUseOfsettingM({
          error: Error()
        }), settingM) : settingM
      });

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=global-entry.js.map
System.register(["cc"], function (_export, _context) {
  "use strict";

  var _cclegacy, _crd;

  return {
    setters: [function (_cc) {
      _cclegacy = _cc.cclegacy;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "f33b4GlbARBmIheKPyzLe58", "slotServerProtos", undefined);

      _export("default", {
        "nested": {
          "slot": {
            "nested": {
              "mainHandler": {
                "nested": {
                  "bet": {
                    "fields": {
                      "code": {
                        "type": "int32",
                        "id": 1
                      },
                      "data": {
                        "type": "Data",
                        "id": 2
                      },
                      "message": {
                        "type": "string",
                        "id": 3
                      }
                    },
                    "nested": {
                      "Data": {
                        "fields": {
                          "game_mode": {
                            "type": "int32",
                            "id": 1
                          },
                          "rate": {
                            "type": "double",
                            "id": 2
                          },
                          "seq": {
                            "type": "int32",
                            "id": 3
                          },
                          "coin": {
                            "type": "double",
                            "id": 4
                          },
                          "bonus_type": {
                            "type": "string",
                            "id": 5
                          },
                          "bet_time": {
                            "type": "int64",
                            "id": 6
                          },
                          "jackpot_amount": {
                            "type": "double",
                            "id": 7
                          },
                          "round_type": {
                            "type": "int32",
                            "id": 8
                          },
                          "reward_id": {
                            "type": "int32",
                            "id": 9
                          }
                        }
                      }
                    }
                  },
                  "report": {
                    "fields": {
                      "code": {
                        "type": "int32",
                        "id": 1
                      },
                      "data": {
                        "type": "Data",
                        "id": 2
                      },
                      "message": {
                        "type": "string",
                        "id": 3
                      }
                    },
                    "nested": {
                      "Data": {
                        "fields": {}
                      }
                    }
                  }
                }
              }
            }
          }
        }
      });

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=slotServerProtos.js.map
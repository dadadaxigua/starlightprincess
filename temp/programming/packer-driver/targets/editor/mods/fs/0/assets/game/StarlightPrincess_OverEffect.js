System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, Color, color, Sprite, sBoxItemBase, sUtil, _dec, _dec2, _dec3, _class, _class2, _descriptor, _descriptor2, _temp, _crd, ccclass, property, StarlightPrincess_OverEffect;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfsBoxItemBase(extras) {
    _reporterNs.report("sBoxItemBase", "../lobby/game/core/sBoxItemBase", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsUtil(extras) {
    _reporterNs.report("sUtil", "../lobby/game/core/sUtil", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      Color = _cc.Color;
      color = _cc.color;
      Sprite = _cc.Sprite;
    }, function (_unresolved_2) {
      sBoxItemBase = _unresolved_2.sBoxItemBase;
    }, function (_unresolved_3) {
      sUtil = _unresolved_3.sUtil;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "5592cDESr1CDIHOQ0yzsZid", "StarlightPrincess_OverEffect", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("StarlightPrincess_OverEffect", StarlightPrincess_OverEffect = (_dec = ccclass('StarlightPrincess_OverEffect'), _dec2 = property(Sprite), _dec3 = property(Sprite), _dec(_class = (_class2 = (_temp = class StarlightPrincess_OverEffect extends (_crd && sBoxItemBase === void 0 ? (_reportPossibleCrUseOfsBoxItemBase({
        error: Error()
      }), sBoxItemBase) : sBoxItemBase) {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "lightNode", _descriptor, this);

          _initializerDefineProperty(this, "boomNode", _descriptor2, this);
        }

        start() {
          super.start();
        }

        boxItemUpdate(renderData, symbolValue, boxState, tableState) {
          if (boxState == 'win') {
            this.lightNode.color = color(255, 255, 255, 0);
            this.lightNode.node.active = true;
            (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
              error: Error()
            }), sUtil) : sUtil).TweenColor(Color.WHITE, this.lightNode, 0.2, 0);
            this.pushOneSchedule(() => {
              (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                error: Error()
              }), sUtil) : sUtil).TweenColor(color(255, 255, 255, 0), this.lightNode, 0.2, 0);
            }, 1);
            this.pushOneSchedule(() => {
              this.lightNode.node.active = false;
              this.boomNode.node.active = true;
            }, 1.2);
          }
        }

        clearItem(symbolValue, rollState, tableState) {
          this.removeAllSchedule();
          this.lightNode.node.active = false;
          this.boomNode.node.active = false;
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "lightNode", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "boomNode", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=StarlightPrincess_OverEffect.js.map
System.register(["cc"], function (_export, _context) {
  "use strict";

  var _cclegacy, _decorator, Component, Node, director, Animation, UIOpacity, tween, Tween, Label, v3, UITransform, size, SpriteFrame, Sprite, _dec, _dec2, _dec3, _dec4, _dec5, _dec6, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _descriptor5, _temp, _crd, ccclass, property, UIPopup;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  return {
    setters: [function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      Component = _cc.Component;
      Node = _cc.Node;
      director = _cc.director;
      Animation = _cc.Animation;
      UIOpacity = _cc.UIOpacity;
      tween = _cc.tween;
      Tween = _cc.Tween;
      Label = _cc.Label;
      v3 = _cc.v3;
      UITransform = _cc.UITransform;
      size = _cc.size;
      SpriteFrame = _cc.SpriteFrame;
      Sprite = _cc.Sprite;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "ceeebf6McpJKZNfzCm5NxtY", "UIPopup", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("UIPopup", UIPopup = (_dec = ccclass('UIPopup'), _dec2 = property(Label), _dec3 = property(Node), _dec4 = property(UITransform), _dec5 = property(SpriteFrame), _dec6 = property(SpriteFrame), _dec(_class = (_class2 = (_temp = class UIPopup extends Component {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "tipLabel", _descriptor, this);

          _initializerDefineProperty(this, "iconNode", _descriptor2, this);

          _initializerDefineProperty(this, "bgTrans", _descriptor3, this);

          _initializerDefineProperty(this, "turboOnSpriteFrame", _descriptor4, this);

          _initializerDefineProperty(this, "turboOffSpriteFrame", _descriptor5, this);
        }

        onLoad() {
          this.node.active = false;
          director.on('uiTipsOpen', (mode, tex) => {
            if (mode == 'text') {
              this.node.active = true;
              this.tipLabel.string = tex;
              this.tipLabel.updateRenderData(true);
              this.tipLabel.node.position = v3(0, 0, 0);
              this.bgTrans.contentSize = size(this.tipLabel.node.getComponent(UITransform).contentSize.x + 50, this.bgTrans.contentSize.height);
              this.iconNode.active = false;
              this.node.getComponent(Animation).play();
              let opa = this.node.getComponent(UIOpacity);
              opa.opacity = 255;
              Tween.stopAllByTarget(opa);
              tween(opa).delay(3).to(0.5, {
                opacity: 0
              }).call(() => {
                this.node.active = false;
              }).start().target(opa);
            } else if (mode == 'turbo') {
              this.node.active = true;
              this.tipLabel.string = tex;
              this.tipLabel.updateRenderData(true);
              this.tipLabel.node.position = v3(20, 0, 0);
              this.bgTrans.contentSize = size(this.tipLabel.node.getComponent(UITransform).contentSize.x + 100, this.bgTrans.contentSize.height);
              this.iconNode.active = true;
              this.iconNode.position = v3(-this.bgTrans.contentSize.x / 2 + 40, 0, 0);
              this.iconNode.getComponent(Sprite).spriteFrame = this.turboOnSpriteFrame;
              this.node.getComponent(Animation).play();
              let opa = this.node.getComponent(UIOpacity);
              opa.opacity = 255;
              Tween.stopAllByTarget(opa);
              tween(opa).delay(3).to(0.5, {
                opacity: 0
              }).call(() => {
                this.node.active = false;
              }).start().target(opa);
            } else if (mode == 'normal') {
              this.node.active = true;
              this.tipLabel.string = tex;
              this.tipLabel.updateRenderData(true);
              this.tipLabel.node.position = v3(20, 0, 0);
              this.bgTrans.contentSize = size(this.tipLabel.node.getComponent(UITransform).contentSize.x + 100, this.bgTrans.contentSize.height);
              this.iconNode.active = true;
              this.iconNode.position = v3(-this.bgTrans.contentSize.x / 2 + 40, 0, 0);
              this.iconNode.getComponent(Sprite).spriteFrame = this.turboOffSpriteFrame;
              this.node.getComponent(Animation).play();
              let opa = this.node.getComponent(UIOpacity);
              opa.opacity = 255;
              Tween.stopAllByTarget(opa);
              tween(opa).delay(3).to(0.5, {
                opacity: 0
              }).call(() => {
                this.node.active = false;
              }).start().target(opa);
            }
          }, this);
        }

        start() {}

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "tipLabel", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "iconNode", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "bgTrans", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "turboOnSpriteFrame", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor5 = _applyDecoratedDescriptor(_class2.prototype, "turboOffSpriteFrame", [_dec6], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=UIPopup.js.map
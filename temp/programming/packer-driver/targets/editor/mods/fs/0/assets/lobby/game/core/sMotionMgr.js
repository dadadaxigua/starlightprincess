System.register(["__unresolved_0", "cc", "__unresolved_1"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, Component, Node, director, sMotionTween, _dec, _class, _class2, _temp, _crd, ccclass, property, sMotionMgr;

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _reportPossibleCrUseOfsMotionTween(extras) {
    _reporterNs.report("sMotionTween", "./sMotionTween", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      Component = _cc.Component;
      Node = _cc.Node;
      director = _cc.director;
    }, function (_unresolved_2) {
      sMotionTween = _unresolved_2.sMotionTween;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "75458AwEXJFe658VVtYlj4f", "sMotionMgr", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("sMotionMgr", sMotionMgr = (_dec = ccclass('sMotionMgr'), _dec(_class = (_temp = _class2 = class sMotionMgr extends Component {
        constructor(...args) {
          super(...args);

          _defineProperty(this, "motionTweenList", []);

          _defineProperty(this, "finishTweenList", []);
        }

        static get Instance() {
          if (!sMotionMgr.instance || !sMotionMgr.selfNode || !sMotionMgr.selfNode.isValid) {
            let motionNode = new Node();
            motionNode.name = 'motionNode';
            let parentNode = director.getScene().children[0];

            if (parentNode) {
              parentNode.addChild(motionNode);
              sMotionMgr.selfNode = motionNode;
            }

            sMotionMgr.instance = motionNode.addComponent(sMotionMgr);
          }

          return sMotionMgr.instance;
        }

        start() {}

        update(dt) {
          // game.deltaTime
          if (sMotionMgr.instance && sMotionMgr.instance.motionTweenList && sMotionMgr.instance.motionTweenList.length > 0) {
            for (let i = 0; i < sMotionMgr.instance.motionTweenList.length; i++) {
              const motion = sMotionMgr.instance.motionTweenList[i];

              if (!motion.IsFinish) {
                motion.Update(dt);
              }

              if (motion.IsFinish) {
                sMotionMgr.instance.finishTweenList.push(motion);
              }
            }

            if (sMotionMgr.instance.finishTweenList.length > 0) {
              for (let i = 0; i < sMotionMgr.instance.finishTweenList.length; i++) {
                const fMotion = sMotionMgr.instance.finishTweenList[i];
                sMotionMgr.RemoveTweenByID(fMotion.id);
              }

              sMotionMgr.instance.finishTweenList = [];
            } // console.log('---------------');

          }
        }

        static Tween(_id, _end, _start, _speed, _frameCallback = null, _completeCallBack = null) {
          const motion = new (_crd && sMotionTween === void 0 ? (_reportPossibleCrUseOfsMotionTween({
            error: Error()
          }), sMotionTween) : sMotionTween)();
          motion.CreateTween(_id, _end, _start, _speed, _frameCallback, cid => {
            if (_completeCallBack) {
              _completeCallBack();
            } // sMotionMgr.RemoveTweenByID(cid);

          }); // console.log('sMotionMgr.Instance.motionTweenList:'+sMotionMgr.Instance.motionTweenList.length);

          sMotionMgr.Instance.motionTweenList.push(motion);
          return motion;
        }

        static RemoveTweenByID(_id) {
          if (sMotionMgr.instance && sMotionMgr.instance.motionTweenList && sMotionMgr.instance.motionTweenList.length > 0) {
            for (let i = 0; i < sMotionMgr.instance.motionTweenList.length; i++) {
              const motion = sMotionMgr.instance.motionTweenList[i];

              if (motion && motion.id == _id) {
                sMotionMgr.instance.motionTweenList.splice(i, 1);
                break;
              }
            }
          }
        }

        static GetTweenByID(_id) {
          if (sMotionMgr.instance && sMotionMgr.instance.motionTweenList && sMotionMgr.instance.motionTweenList.length > 0) {
            for (let i = 0; i < sMotionMgr.instance.motionTweenList.length; i++) {
              const motion = sMotionMgr.instance.motionTweenList[i];

              if (motion && motion.id == _id) {
                return motion;
              }
            }
          }
        }

        static Clear() {
          if (sMotionMgr.instance) {
            // sMotionMgr.instance.motionTweens.clear();
            sMotionMgr.instance.motionTweenList = [];
            sMotionMgr.instance = null;
          }
        }

      }, _defineProperty(_class2, "instance", null), _defineProperty(_class2, "selfNode", null), _temp)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=sMotionMgr.js.map
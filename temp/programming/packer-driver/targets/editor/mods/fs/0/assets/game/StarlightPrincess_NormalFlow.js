System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2", "__unresolved_3", "__unresolved_4", "__unresolved_5"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, director, Node, sNormalFlow, sBoxMgr, sAudioMgr, sUtil, sConfigMgr, _dec, _dec2, _class, _class2, _descriptor, _temp, _crd, ccclass, property, StarlightPrincess_NormalFlow;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfsNormalFlow(extras) {
    _reporterNs.report("sNormalFlow", "../lobby/game/core/sNormalFlow", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsBoxMgr(extras) {
    _reporterNs.report("sBoxMgr", "../lobby/game/core/sBoxMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsAudioMgr(extras) {
    _reporterNs.report("sAudioMgr", "../lobby/game/core/sAudioMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsUtil(extras) {
    _reporterNs.report("sUtil", "../lobby/game/core/sUtil", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsConfigMgr(extras) {
    _reporterNs.report("sConfigMgr", "../lobby/game/core/sConfigMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsBoxEntity(extras) {
    _reporterNs.report("sBoxEntity", "../lobby/game/core/sBoxEntity", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      director = _cc.director;
      Node = _cc.Node;
    }, function (_unresolved_2) {
      sNormalFlow = _unresolved_2.sNormalFlow;
    }, function (_unresolved_3) {
      sBoxMgr = _unresolved_3.sBoxMgr;
    }, function (_unresolved_4) {
      sAudioMgr = _unresolved_4.default;
    }, function (_unresolved_5) {
      sUtil = _unresolved_5.sUtil;
    }, function (_unresolved_6) {
      sConfigMgr = _unresolved_6.sConfigMgr;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "bdad29Hn5lHQaNLb0iHVpSm", "StarlightPrincess_NormalFlow", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("StarlightPrincess_NormalFlow", StarlightPrincess_NormalFlow = (_dec = ccclass('StarlightPrincess_NormalFlow'), _dec2 = property(Node), _dec(_class = (_class2 = (_temp = class StarlightPrincess_NormalFlow extends (_crd && sNormalFlow === void 0 ? (_reportPossibleCrUseOfsNormalFlow({
        error: Error()
      }), sNormalFlow) : sNormalFlow) {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "effectLayer", _descriptor, this);

          _defineProperty(this, "tatalWin", 0);
        }

        start() {
          super.start(); // this.scheduleOnce(()=>{
          //     sBoxMgr.instance.updateBoxDataByXY(1, 0, 'win', 'settlement');
          //     sBoxMgr.instance.updateBoxDataByXY(1, 1, 'win', 'settlement');
          //     sBoxMgr.instance.updateBoxDataByXY(1, 2, 'win', 'settlement');
          //     sBoxMgr.instance.updateBoxDataByXY(1, 3, 'win', 'settlement');
          //     sBoxMgr.instance.updateBoxDataByXY(1, 4, 'win', 'settlement');
          // },2);

          director.on('betBtnClick', betSpeedMode => {
            if (betSpeedMode != 'turbo') {
              (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                error: Error()
              }), sAudioMgr) : sAudioMgr).PlayAudio('normal_spinning', true);
            }

            ;
          }, this);
        }

        clearUI() {
          super.clearUI();
          this.removeScheduleDic();
        }

        rollStopTurboAction(round, rollSpeedMode, clickMode) {
          director.off('rollOneColumnBump');
          director.off('byWayWinLightEnd');
          director.off('titleWinSettleEnd');
          director.off('rollStopOneRoundCall');
          director.off('StarlightPrincessBigWinEnd'); // const isJackpot = this.jackpotAmount > 0;
          // const isJackpot = true;

          this.tatalWin = 0;

          const _boxViewSize = (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
            error: Error()
          }), sBoxMgr) : sBoxMgr).instance.getBoxViewSize();

          const _boxSize = (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
            error: Error()
          }), sBoxMgr) : sBoxMgr).instance.getBoxSize();

          const round_symbol_map = round['round_symbol_map'];
          director.on('rollOneColumnStop', col => {
            if (col == 5) {
              (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                error: Error()
              }), sAudioMgr) : sAudioMgr).PlayShotAudio('normal_spin_end', 1);
              (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                error: Error()
              }), sAudioMgr) : sAudioMgr).StopAudio();
            }

            ;
          }, this);

          if (round) {
            let timestop = 0;
            const round_symbol_map = round['round_symbol_map'];

            for (let xx of round_symbol_map[0]) {
              for (let yy = 0; yy < xx.length; yy++) {
                //console.log('symbel:',xx[yy]);
                if (yy >= 1 && yy <= 5 && xx[yy] > 2000) {
                  timestop = 0;
                  director.emit('controlHuaXianZi', 'NormalIdle');
                  break;
                }

                ;
              }

              ;
              if (timestop > 0) break;
            }

            ;
            this.normalFlowDelayCall(() => {
              this.scheduleOnce(() => {
                let needWinLight = round.rate > 0 ? true : false;
                const round_symbol_map = round['round_symbol_map'];
                (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
                  error: Error()
                }), sConfigMgr) : sConfigMgr).instance.SetResData(round_symbol_map);
                const hits = round.hit_events_list;
                const config = (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
                  error: Error()
                }), sConfigMgr) : sConfigMgr).instance.getConfig();
                const events = config['event_config'];

                for (let i = 0; i < hits.length; i++) {
                  const _event = hits[i];

                  if (_event && Array.isArray(_event)) {
                    for (let j = 0; j < _event.length; j++) {
                      const eve = _event[j];
                      const hitEvent = events[eve];

                      if (hitEvent && hitEvent.event_type == 'bonusGame') {// isJackpot = true;
                      }

                      ;
                    }

                    ;
                  }

                  ;
                }

                ;
                director.on('rollOneColumnBump', col => {
                  (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                    error: Error()
                  }), sAudioMgr) : sAudioMgr).PlayShotAudio('luodi', 0.4);
                  const colBoxs = (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                    error: Error()
                  }), sBoxMgr) : sBoxMgr).instance.GetColAllBox(col);
                  let shunxu = 0;
                  let shunxu1 = 0;

                  if (colBoxs) {
                    for (let rY = 0; rY < colBoxs.length; rY++) {
                      const rSymbol = colBoxs[rY];

                      if (rSymbol.SymbolValue > 2000) {
                        if (rSymbol.ViewItemIndex.y >= 0 && rSymbol.ViewItemIndex.y <= 4) {
                          rSymbol.DoEnitityAction('thunder');
                          console.log('召唤一道落雷4');

                          if (rSymbol.SymbolValue == 1) {
                            shunxu1++;

                            if (shunxu1 == 1) {
                              this.playmultMp();
                            }

                            ;
                          }

                          ;
                        }

                        ;
                      } else if (rSymbol.SymbolValue == 1) {
                        shunxu++;
                        (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                          error: Error()
                        }), sAudioMgr) : sAudioMgr).PlayShotAudio('scatter_appear' + shunxu);

                        if (shunxu == 1) {
                          this.playscatterMp();
                        }

                        ;
                      }

                      ;
                      ;
                    }

                    ;
                  }

                  ;
                }, this);
                (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                  error: Error()
                }), sUtil) : sUtil).once('byWayWinLightEnd', () => {
                  (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                    error: Error()
                  }), sUtil) : sUtil).once('titleWinSettleEnd', () => {
                    let sBoxMax = false; // sBoxMgr.instance.setAllViewTargetBoxActionArrayState((sbox : sBoxEntity)=>{
                    //     if(sbox.SymbolValue>2000){
                    //         sBoxMax=true;
                    //     };
                    //     return (sbox.SymbolValue>2000);
                    // },'win','settlement');

                    let times = sBoxMax ? 2.5 : 0.5;
                    this.scheduleOnce(() => {
                      const rate = round.rate;

                      if (rate >= 200) {
                        director.emit('betUserInfoUpdateWinAnima', rate - this.tatalWin);
                        director.emit('StarlightPrincessBigWin', rate);
                        (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                          error: Error()
                        }), sUtil) : sUtil).once('StarlightPrincessBigWinEnd', () => {
                          this.scheduleOnce(() => {
                            console.log('rollstop1');
                            director.emit('rollStop');
                          }, 0.5);
                        });
                      } else {
                        this.scheduleOnce(() => {
                          console.log('rollstop2');
                          director.emit('rollStop');
                        }, 0.5);
                      }

                      ;
                    }, times);
                  });
                  director.emit('winBetRes', -1);
                });
                (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                  error: Error()
                }), sUtil) : sUtil).once('rollStopOneRoundCall', () => {
                  director.off('rollOneColumnStop');
                  director.off('rollOneColumnBump');

                  if (needWinLight) {
                    // this.scheduleOnce(()=>{
                    //     this.byWayBoxLightCtr(round,'normal');
                    // },1);
                    this.byWayBoxLightCtr(round, 'normal');
                  } else {
                    console.log('rollstop3');
                    director.emit('rollStop');
                  }
                });
                (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                  error: Error()
                }), sBoxMgr) : sBoxMgr).instance.rollStopAllColumn();
              }, timestop);
            });
          }

          ;
        }

        rollStopAction(round, rollSpeedMode, clickMode) {
          director.off('rollOneColumnBump');
          director.off('byWayWinLightEnd');
          director.off('titleWinSettleEnd');
          director.off('rollStopOneRoundCall');
          director.off('StarlightPrincessBigWinEnd'); // const isJackpot = this.jackpotAmount > 0;
          // const isJackpot = true;

          this.tatalWin = 0;

          const _boxViewSize = (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
            error: Error()
          }), sBoxMgr) : sBoxMgr).instance.getBoxViewSize();

          const _boxSize = (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
            error: Error()
          }), sBoxMgr) : sBoxMgr).instance.getBoxSize();

          const round_symbol_map = round['round_symbol_map'];
          director.on('rollOneColumnStop', col => {
            if (col == 0) {
              (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                error: Error()
              }), sAudioMgr) : sAudioMgr).StopAudio();
            }

            ;
          }, this);

          if (round) {
            this.normalFlowDelayCall(() => {
              let timestop = 0;
              const round_symbol_map = round['round_symbol_map'];

              for (let xx of round_symbol_map[0]) {
                for (let yy = 0; yy < xx.length; yy++) {
                  //console.log('symbel:',xx[yy]);
                  if (yy >= 1 && yy <= 5 && xx[yy] > 2000) {
                    timestop = 0;
                    director.emit('controlHuaXianZi', 'NormalIdle');
                    break;
                  }

                  ;
                }

                ;
                if (timestop > 0) break;
              }

              ;
              this.scheduleOnce(() => {
                let needWinLight = round.rate > 0 ? true : false;
                (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
                  error: Error()
                }), sConfigMgr) : sConfigMgr).instance.SetResData(round_symbol_map);
                const hits = round.hit_events_list;
                const config = (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
                  error: Error()
                }), sConfigMgr) : sConfigMgr).instance.getConfig();
                const events = config['event_config'];

                for (let i = 0; i < hits.length; i++) {
                  const _event = hits[i];

                  if (_event && Array.isArray(_event)) {
                    for (let j = 0; j < _event.length; j++) {
                      const eve = _event[j];
                      const hitEvent = events[eve];

                      if (hitEvent && hitEvent.event_type == 'bonusGame') {// isJackpot = true;
                      }

                      ;
                    }

                    ;
                  }

                  ;
                }

                ;
                director.on('rollOneColumnBump', col => {
                  (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                    error: Error()
                  }), sAudioMgr) : sAudioMgr).PlayShotAudio('normal_spin_col_end', 1);
                  const colBoxs = (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                    error: Error()
                  }), sBoxMgr) : sBoxMgr).instance.GetColAllBox(col);
                  let shunxu = 0;
                  let shunxu1 = 0;

                  if (colBoxs) {
                    for (let rY = 0; rY < colBoxs.length; rY++) {
                      const rSymbol = colBoxs[rY];

                      if (rSymbol.SymbolValue > 2000) {
                        if (rSymbol.ViewItemIndex.y >= 0 && rSymbol.ViewItemIndex.y <= 4) {
                          rSymbol.DoEnitityAction('thunder');
                          console.log('召唤一道落雷5');

                          if (rSymbol.SymbolValue == 1) {
                            shunxu1++;

                            if (shunxu1 == 1) {
                              this.playmultMp();
                            }

                            ;
                          }

                          ;
                        }

                        ;
                      } else if (rSymbol.SymbolValue == 1) {
                        shunxu++;
                        (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                          error: Error()
                        }), sAudioMgr) : sAudioMgr).PlayShotAudio('scatter_appear' + shunxu);

                        if (shunxu == 1) {
                          this.playscatterMp();
                        }

                        ;
                      }

                      ;
                    }

                    ;
                  }

                  ;
                }, this);
                (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                  error: Error()
                }), sUtil) : sUtil).once('byWayWinLightEnd', () => {
                  (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                    error: Error()
                  }), sUtil) : sUtil).once('titleWinSettleEnd', () => {
                    let sBoxMax = false; // sBoxMgr.instance.setAllViewTargetBoxActionArrayState((sbox : sBoxEntity)=>{
                    //     if(sbox.SymbolValue>2000){
                    //         sBoxMax=true;
                    //     };
                    //     return (sbox.SymbolValue>2000);
                    // },'win','settlement');

                    let times = sBoxMax ? 3 : 0.5;
                    this.scheduleOnce(() => {
                      const rate = round.rate;

                      if (rate >= 200) {
                        director.emit('betUserInfoUpdateWinAnima', rate - this.tatalWin);
                        director.emit('StarlightPrincessBigWin', rate);
                        (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                          error: Error()
                        }), sUtil) : sUtil).once('StarlightPrincessBigWinEnd', () => {
                          director.emit('rollStop');
                        });
                      } else {
                        this.scheduleOnce(() => {
                          console.log('rollstop2');
                          director.emit('rollStop');
                        }, 0.5);
                      }

                      ;
                    }, times);
                  });
                  director.emit('winBetRes', -1);
                });
                (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                  error: Error()
                }), sUtil) : sUtil).once('rollStopOneRoundCall', () => {
                  director.off('rollOneColumnStop');
                  director.off('rollOneColumnBump');

                  if (needWinLight) {
                    // this.scheduleOnce(()=>{
                    //     this.byWayBoxLightCtr(round,'normal');
                    // },1);
                    this.byWayBoxLightCtr(round, 'normal');
                  } else {
                    //director.emit('betUserInfoUpdateWinAnima',round.rate);
                    director.emit('rollStop');
                  }
                });
                (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                  error: Error()
                }), sBoxMgr) : sBoxMgr).instance.rollStopAllColumn();
              }, timestop);
            });
          }

          ;
        }

        byWayBoxLightCtr(round, rollSpeedMode) {
          director.off('dropAllBoxFinish');
          const eventArr = (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
            error: Error()
          }), sConfigMgr) : sConfigMgr).instance.GetAllEventOptConfigByHitListOnCheck(round); // console.log('eventArr:'+JSON.stringify(eventArr));

          const boxSum = (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
            error: Error()
          }), sBoxMgr) : sBoxMgr).instance.boxSum;
          let isEnd = false;
          let isTotalWin = false;
          let IsMulit = false;
          let winRate = 0;

          if (eventArr && Array.isArray(eventArr)) {
            let eventIndex = 0;

            const boxAnimaCall = () => {
              let isWin = false;

              if (eventIndex < eventArr.length) {
                isEnd = eventIndex == eventArr.length - 1;
                const nowData = eventArr[eventIndex++];

                if (nowData && nowData.eventData && nowData.eventData.length > 0) {
                  const events = nowData.eventData;
                  let resAct = {};
                  let winSymbol = {};
                  winRate = 0;

                  for (let i = 0; i < events.length; i++) {
                    const _event = events[i];

                    if (_event.event_type == 'boxAnima') {
                      if (_event.rate_change && _event.rate_change.num) {
                        winRate += _event.rate_change.num;
                      }

                      if (_event.bonus_symbol) {
                        winSymbol[_event.bonus_symbol] = true;
                      }

                      isWin = true;
                      isTotalWin = true;
                      let act = resAct[_event.bonus_symbol];

                      if (!act) {
                        act = {};
                        act.bonus_symbol = _event.bonus_symbol;
                        act.act_pos = {};
                        act.rate = 0;
                      }

                      if (_event.rate_change) {
                        if (act.rate == 0) {
                          act.rate = _event.rate_change.num;
                        } else {
                          if (_event.rate_change.type == 0) {
                            act.rate += _event.rate_change.num;
                          } else if (_event.rate_change.type == 1) {
                            act.rate *= _event.rate_change.num;
                          }
                        }
                      }

                      if (_event.act_pos) {
                        const poss = _event['act_pos'].split(',');

                        if (poss) {
                          for (let a = 0; a < poss.length; a++) {
                            const value = poss[a];
                            act.act_pos[value] = true;
                          }

                          ;
                        }

                        ;
                      }

                      ;
                      resAct[_event.bonus_symbol] = act;
                    }

                    ;
                  }

                  ;
                  const resActKeys = Object.keys(resAct);
                  let allActPos = {};

                  if (resActKeys) {
                    for (let i = 0; i < resActKeys.length; i++) {
                      const element = resAct[resActKeys[i]];
                      const itemKeys = Object.keys(element.act_pos);

                      for (let j = 0; j < itemKeys.length; j++) {
                        const key = itemKeys[j];
                        allActPos[key] = true;
                      }

                      ; //console.log('element',element);

                      director.emit('slotWinBoxItemInfo', {
                        symbol: element.bonus_symbol,
                        rate: element.rate,
                        num: itemKeys.length,
                        boxPos: itemKeys[Math.trunc(itemKeys.length / 2)]
                      }); // this.byWayboxResWinAction(element);
                    } // let boxActArr: any[][] = [];


                    const allKeys = Object.keys(allActPos);
                    let xx = 0;

                    for (let i = 0; i < allKeys.length; i++) {
                      const element = allKeys[i];
                      const act = element.split('_');

                      if (act && act.length == 2) {
                        const act1 = parseInt(act[0]);
                        const act2 = parseInt(act[1]); //console.log('消除的格子',act1,act2);

                        if (act1 && act2 || act1 == 0 || act2 == 0) {
                          if ((_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                            error: Error()
                          }), sBoxMgr) : sBoxMgr).instance.getBoxEntityByXY(act1, act2).SymbolValue > 2000) {
                            xx++;
                            this.scheduleOnce(() => {
                              (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                                error: Error()
                              }), sBoxMgr) : sBoxMgr).instance.updateBoxDataByXY(act1, act2, 'win', 'settlement');
                            }, 0.1 * xx);
                          } else {
                            (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                              error: Error()
                            }), sBoxMgr) : sBoxMgr).instance.updateBoxDataByXY(act1, act2, 'win', 'settlement');
                          }

                          ;
                        }

                        ;
                      }

                      ;
                    }

                    ;

                    for (let i = 0; i < (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                      error: Error()
                    }), sBoxMgr) : sBoxMgr).instance.boxArray.length; i++) {
                      const arr = (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                        error: Error()
                      }), sBoxMgr) : sBoxMgr).instance.boxArray[i];

                      for (let m = 0; m < arr.length; m++) {
                        if (arr[m].ViewItemIndex.y >= 0 && arr[m].ViewItemIndex.y <= 4) {
                          //console.log('symbolvalue:',sBoxMgr.instance.boxArray,i,m,sBoxMgr.instance.getBoxEntityByXY(i,m),sBoxMgr.instance.getBoxEntityByXY(i,m).SymbolValue);
                          if (arr[m].SymbolValue > 2000) {
                            IsMulit = true;
                            console.log('加倍存在：', IsMulit);
                            break;
                          }

                          ;
                        }

                        ;
                      }

                      ;

                      if (IsMulit) {
                        break;
                      }

                      ;
                    }

                    ;
                    director.emit('winBetRes', winRate); //director.emit('betUserInfoUpdateWinAnima',winRate);
                    //this.tatalWin+=winRate;

                    if (round.rate < 200 && !IsMulit) {
                      console.log('小于200：无加倍倍数显示3', winRate);
                      director.emit('betUserInfoUpdateWinAnima', winRate);
                    } else if (!IsMulit) {
                      if (eventIndex != eventArr.length - 1) {
                        this.tatalWin += winRate;
                        director.emit('betUserInfoUpdateWinAnima', winRate);
                      }

                      ;
                    } else if (IsMulit) {
                      this.tatalWin += winRate;
                      director.emit('betUserInfoUpdateWinAnima', winRate);
                    }

                    ;

                    if (isWin) {
                      const winSymbolKeys = Object.keys(winSymbol);

                      if (winSymbolKeys && winSymbolKeys.length > 0) {
                        for (let i = 0; i < winSymbolKeys.length; i++) {
                          const winSymbolKey = winSymbolKeys[i];
                        }

                        ;
                      }

                      ;
                      (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                        error: Error()
                      }), sAudioMgr) : sAudioMgr).PlayAudio('small_win');
                      this.scheduleOnce(() => {
                        (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                          error: Error()
                        }), sAudioMgr) : sAudioMgr).StopAudio();
                      }, 0.6);
                      (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                        error: Error()
                      }), sAudioMgr) : sAudioMgr).PlayShotAudio('small_win_effect');

                      if (rollSpeedMode == 'trubo') {
                        (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                          error: Error()
                        }), sAudioMgr) : sAudioMgr).StopAudio();
                        (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                          error: Error()
                        }), sAudioMgr) : sAudioMgr).PlayShotAudio('small_win_end');
                        this.scheduleOnce(() => {
                          (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                            error: Error()
                          }), sAudioMgr) : sAudioMgr).PlayShotAudio('symbol_clear' + eventIndex);
                        }, 1);
                      } else {
                        this.scheduleOnce(() => {
                          (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                            error: Error()
                          }), sAudioMgr) : sAudioMgr).PlayShotAudio('small_win_end');
                          this.scheduleOnce(() => {
                            (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                              error: Error()
                            }), sAudioMgr) : sAudioMgr).PlayShotAudio('symbol_clear' + eventIndex);
                          }, 1);
                        }, 0.6);
                      }

                      ; // director.emit('winBetRateRes',winRate * this.getMultiple(eventIndex - 1));

                      (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                        error: Error()
                      }), sBoxMgr) : sBoxMgr).instance.blackCurtainAnima();
                      (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                        error: Error()
                      }), sBoxMgr) : sBoxMgr).instance.blackCurtainAnima(-1, false);
                      this.scheduleOnce(() => {
                        // director.emit('titleSumLightMsg','normal',eventIndex);
                        (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
                          error: Error()
                        }), sConfigMgr) : sConfigMgr).instance.TurnNextResRoundData();
                        (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                          error: Error()
                        }), sBoxMgr) : sBoxMgr).instance.ReplenishBoxEntity();
                        let conturhua = 0;
                        (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                          error: Error()
                        }), sBoxMgr) : sBoxMgr).instance.DropAllEntityBoxAfterRemove(strikeBox => {
                          (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                            error: Error()
                          }), sAudioMgr) : sAudioMgr).PlayShotAudio('normal_dropgrid');

                          if (strikeBox) {
                            if (strikeBox.SymbolValue > 2000) {
                              if (strikeBox.ViewItemIndex.y > 4) {
                                conturhua++;
                                console.log('召唤一道落雷6');
                                strikeBox.DoEnitityAction('thunder');

                                if (conturhua == 1) {
                                  director.emit('controlHuaXianZi', 'NormalIdle');
                                }

                                ;
                              }

                              ;
                            }

                            ;
                          }

                          ;
                        }, 0.02);
                      }, 2.5);
                    }

                    ;
                  }

                  ;
                } else {
                  if (isEnd) {
                    let multiTotal = 0;

                    if (isTotalWin && nowData.opt && nowData.opt.multiData && nowData.opt.multiData.length > 0) {
                      const multiboxes = [];

                      for (let o = 0; o < nowData.opt.multiData.length; o++) {
                        const multiData = nowData.opt.multiData[o];

                        if (multiData && multiData.pos && multiData.pos.length > 0) {
                          multiTotal += multiData.multi;

                          for (let l = 0; l < multiData.pos.length; l++) {
                            const mPos = multiData.pos[l];

                            if (mPos) {
                              const mPosArr = mPos.split('_');

                              if (mPosArr && mPosArr.length == 2) {
                                multiboxes.push((_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                                  error: Error()
                                }), sBoxMgr) : sBoxMgr).instance.getBoxEntityByXY(parseInt(mPosArr[0]), parseInt(mPosArr[1])));
                              }
                            }
                          }
                        }
                      }

                      if (multiboxes.length > 0) {
                        let mulIndex = 0;
                        this.pushOneSchedule(() => {
                          const mBox = multiboxes[mulIndex++];

                          if (mBox) {
                            mBox.UpdateData('win', mulIndex == multiboxes.length ? 'close' : 'open');
                          }
                        }, 0, multiboxes.length, 1.5, 0, true);
                        this.scheduleOnce(() => {
                          if (round.rate > 200) {} else {
                            console.log('小于200：有加倍倍数显示4', round.rate);
                            director.emit('betUserInfoUpdateWinAnima', round.rate - this.tatalWin);
                          }

                          ;
                          director.emit('byWayWinLightEnd');
                        }, multiboxes.length * 1.5);
                      } else {
                        director.emit('byWayWinLightEnd');
                      }

                      ;
                    } else {
                      if (eventArr.length > 1) {
                        director.emit('byWayWinLightEnd');
                      }

                      ;
                    }

                    ;
                  }

                  ;
                }

                ;
              }

              ;
            };

            boxAnimaCall();
            director.on('dropAllBoxFinish', () => {
              // console.log('dropAllBoxFinish');
              boxAnimaCall(); // console.log('isEnd:',isEnd);
            }, this);
          }

          ;
        }

        playscatterMp() {
          let num = (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
            error: Error()
          }), sUtil) : sUtil).RandomInt(1, 11);

          if (num >= 1 && num < 9) {
            (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
              error: Error()
            }), sAudioMgr) : sAudioMgr).PlayShotAudio('scatter_appear_voice' + num);
          }
        }

        playmultMp() {
          let num = (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
            error: Error()
          }), sUtil) : sUtil).RandomInt(1, 11);

          if (num >= 0 && num < 10) {
            (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
              error: Error()
            }), sAudioMgr) : sAudioMgr).PlayShotAudio('mult_appear_voice' + num);
          }
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "effectLayer", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=StarlightPrincess_NormalFlow.js.map
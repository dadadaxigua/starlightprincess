System.register(["cc"], function (_export, _context) {
  "use strict";

  var _cclegacy, _decorator, Component, Node, instantiate, v3, UITransform, Label, ScrollView, tween, UIOpacity, Button, Widget, director, _dec, _dec2, _dec3, _dec4, _dec5, _dec6, _dec7, _dec8, _dec9, _dec10, _dec11, _dec12, _dec13, _dec14, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _descriptor5, _descriptor6, _descriptor7, _descriptor8, _descriptor9, _descriptor10, _descriptor11, _descriptor12, _descriptor13, _temp, _crd, ccclass, property, UISelectBet;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  return {
    setters: [function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      Component = _cc.Component;
      Node = _cc.Node;
      instantiate = _cc.instantiate;
      v3 = _cc.v3;
      UITransform = _cc.UITransform;
      Label = _cc.Label;
      ScrollView = _cc.ScrollView;
      tween = _cc.tween;
      UIOpacity = _cc.UIOpacity;
      Button = _cc.Button;
      Widget = _cc.Widget;
      director = _cc.director;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "6cb03XN8JlE8qEEg4hsqYUM", "UISelectBet", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("UISelectBet", UISelectBet = (_dec = ccclass('UISelectBet'), _dec2 = property(Node), _dec3 = property(Node), _dec4 = property(Node), _dec5 = property(Label), _dec6 = property(Label), _dec7 = property(Label), _dec8 = property(ScrollView), _dec9 = property(ScrollView), _dec10 = property(ScrollView), _dec11 = property(Node), _dec12 = property(Node), _dec13 = property(Node), _dec14 = property(Node), _dec(_class = (_class2 = (_temp = class UISelectBet extends Component {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "comfirmBtn", _descriptor, this);

          _initializerDefineProperty(this, "maxBet", _descriptor2, this);

          _initializerDefineProperty(this, "closeBtn", _descriptor3, this);

          _initializerDefineProperty(this, "ownLabel", _descriptor4, this);

          _initializerDefineProperty(this, "betLabel", _descriptor5, this);

          _initializerDefineProperty(this, "winLabel", _descriptor6, this);

          _initializerDefineProperty(this, "betSizeScrollView", _descriptor7, this);

          _initializerDefineProperty(this, "betLineScrollView", _descriptor8, this);

          _initializerDefineProperty(this, "betAmountScrollView", _descriptor9, this);

          _initializerDefineProperty(this, "betSizeItemCopy", _descriptor10, this);

          _initializerDefineProperty(this, "betLineItemCopy", _descriptor11, this);

          _initializerDefineProperty(this, "betAmountItemCopy", _descriptor12, this);

          _initializerDefineProperty(this, "numNode", _descriptor13, this);

          _defineProperty(this, "itemHeight", 60);

          _defineProperty(this, "betSizeCount", 0);

          _defineProperty(this, "betAmountCount", 0);

          _defineProperty(this, "betLine", 0);

          _defineProperty(this, "lineAmount", 0);

          _defineProperty(this, "betAmount", 20);

          _defineProperty(this, "betAmountOri", 0);

          _defineProperty(this, "betSizeArray", []);

          _defineProperty(this, "betAmountArray", []);

          _defineProperty(this, "betLineArray", []);

          _defineProperty(this, "touchEnable", true);

          _defineProperty(this, "loaded", false);
        }

        onLoad() {
          this.node.getComponent(Widget).updateAlignment();
        }

        start() {
          let country = '';

          if (globalThis.getCountry) {
            const con = globalThis.getCountry();

            if (con) {
              country = con;
            }
          }

          if (country == 'VNM') {
            if (this.betLabel) {
              const vnIcon = this.betLabel.node.parent.getChildByName('vnIcon');

              if (vnIcon) {
                vnIcon.active = true;
              }
            }
          } else {
            if (this.betLabel) {
              const icon = this.betLabel.node.parent.getChildByName('icon');

              if (icon) {
                icon.active = true;
              }
            }
          }

          globalThis.getClientBetConfig(globalThis.currentPlayingGameID, config => {
            if (config) {
              // let games = config.games;
              // if(games){
              //     let index = 0;
              //     for (let i = 0; i < games.length; i++) {
              //         const element = games[i];
              //         if(this.gameid == element.game_id){
              //             this.lineAmount = element.bet_multiple;
              //             let obj = instantiate(this.betLineItemCopy);
              //             obj.parent = this.betLineItemCopy.parent;
              //             obj.active = true;
              //             obj.position = v3(0,index++ * -this.itemHeight - this.itemHeight * 2,0);
              //             obj.children[0].getComponent(Label).string = this.lineAmount.toString();
              //             break;
              //         }
              //     }
              // }
              let bet_types = config.bet_types;

              if (bet_types && bet_types.length > 0) {
                const height = this.betSizeItemCopy.getComponent(UITransform).height;
                let betSizeIndex = 0,
                    betAmountIndex = 0;
                let betSizeArr = [],
                    betAmountArr = [],
                    lineArr = [];

                for (let i = 0; i < bet_types.length; i++) {
                  const bet = bet_types[i];

                  if (bet.game_mode == 1 && bet.game_id == globalThis.GameBtnEntity.gameid) {
                    if (this.lineAmount == 0) {
                      this.lineAmount = bet.bet_multiple;
                    }

                    if (lineArr.indexOf(bet.bet_multiple) < 0) {
                      lineArr.push(bet.bet_multiple);
                    }
                  }
                }

                for (let i = 0; i < bet_types.length; i++) {
                  const bet = bet_types[i];

                  if (bet.game_mode == 1 && bet.game_id == globalThis.GameBtnEntity.gameid) {
                    // for (let l = 0; l < lineArr.length; l++) {
                    //     const _line = lineArr[l];
                    // }
                    if (betSizeArr.indexOf(bet.bet_amount) < 0) {
                      let obj = instantiate(this.betSizeItemCopy);
                      obj.parent = this.betSizeItemCopy.parent;
                      obj.active = true;
                      obj.position = v3(0, betSizeIndex * -height - height * 2, 0);
                      obj.name = bet.bet_amount.toString();
                      obj.children[0].getComponent(Label).string = this.AddCommas(bet.bet_amount);
                      obj['mLine'] = bet.bet_multiple;
                      this.betSizeArray.push(obj);
                      betSizeArr.push(bet.bet_amount);
                      betSizeIndex++;
                    }

                    if (betAmountArr.indexOf(bet.bet_amount * bet.bet_multiple) < 0) {
                      let amountObj = instantiate(this.betAmountItemCopy);
                      amountObj.parent = this.betAmountItemCopy.parent;
                      amountObj.active = true;
                      amountObj.position = v3(0, betAmountIndex * -height - height * 2, 0);
                      amountObj.name = (bet.bet_amount * bet.bet_multiple).toString();
                      amountObj.children[0].getComponent(Label).string = this.AddCommas(bet.bet_amount * bet.bet_multiple);
                      amountObj['mLine'] = bet.bet_multiple;
                      this.betAmountArray.push(amountObj);
                      betAmountArr.push(bet.bet_amount * bet.bet_multiple);
                      betAmountIndex++;
                    }
                  }
                }

                this.betSizeCount = betSizeIndex;
                let betSizeTransform = this.betSizeItemCopy.parent.getComponent(UITransform);
                betSizeTransform.setContentSize(betSizeTransform.width, betSizeIndex * height + 4 * height);
                this.betSizeItemCopy.parent.getChildByName('line-end').position = v3(0, betSizeIndex * -height - 2 * height, 0);
                this.betAmountCount = betAmountIndex;
                let betAmountTransform = this.betAmountItemCopy.parent.getComponent(UITransform);
                betAmountTransform.setContentSize(betAmountTransform.width, betAmountIndex * height + 4 * height);
                this.betAmountItemCopy.parent.getChildByName('line-end').position = v3(0, betAmountIndex * -height - 2 * height, 0);

                if (lineArr.length > 0) {
                  for (let i = 0; i < lineArr.length; i++) {
                    const line = lineArr[i];
                    let obj = instantiate(this.betLineItemCopy);
                    obj.parent = this.betLineItemCopy.parent;
                    obj.active = true;
                    obj.position = v3(0, i * -this.itemHeight - this.itemHeight * 2, 0);
                    obj.name = line.toString();
                    obj.children[0].getComponent(Label).string = line.toString();
                    this.betLineArray.push(obj);
                  }

                  let betLineTransform = this.betLineItemCopy.parent.getComponent(UITransform);
                  betLineTransform.setContentSize(betLineTransform.width, lineArr.length * height + 4 * height);
                  betLineTransform.node.getChildByName('line-end').position = v3(0, lineArr.length * -height - 2 * height, 0);
                }
              }
            }
          });
          this.betSizeScrollView.node.on('scroll-began', () => {
            this.touchEnable = false;
            this.betAmountScrollView.enabled = false;
            this.betLineScrollView.enabled = false;
            this.touchForbid();
          }, this);
          this.betAmountScrollView.node.on('scroll-began', () => {
            this.touchEnable = false;
            this.betSizeScrollView.enabled = false;
            this.betLineScrollView.enabled = false;
            this.touchForbid();
          }, this);
          this.betLineScrollView.node.on('scroll-began', () => {
            this.touchEnable = false;
            this.betAmountScrollView.enabled = false;
            this.betSizeScrollView.enabled = false;
            this.touchForbid();
          }, this);
          this.betSizeScrollView.node.on('scroll-ended', () => {
            const pos = this.betSizeScrollView.getScrollOffset();
            const index = Math.floor((pos.y + this.itemHeight / 2) / this.itemHeight);
            tween(this.betSizeScrollView.content).to(0.2, {
              position: v3(0, index * this.itemHeight, 0)
            }, {
              easing: 'sineOut'
            }).call(() => {
              const target = this.betSizeArray[index];

              if (target) {
                const _line = target['mLine'];
                this.betAmount = parseInt(target.name);

                if (this.lineAmount != _line) {
                  for (let i = this.betLineArray.length - 1; i >= 0; i--) {
                    if (this.betLineArray[i].name == _line) {
                      this.lineAmount = _line;
                      tween(this.betLineScrollView.content).to(0.2, {
                        position: v3(0, (this.betLineArray[i].getSiblingIndex() - 3) * this.itemHeight, 0)
                      }, {
                        easing: 'sineOut'
                      }).call(() => {}).start();
                      break;
                    }
                  }
                }

                const amountName = parseInt(target.name) * this.lineAmount;

                for (let i = 0; i < this.betAmountArray.length; i++) {
                  const element = this.betAmountArray[i];

                  if (element.name == amountName) {
                    tween(this.betAmountScrollView.content).to(0.2, {
                      position: v3(0, (element.getSiblingIndex() - 3) * this.itemHeight, 0)
                    }, {
                      easing: 'sineOut'
                    }).call(() => {
                      this.touchResume();
                    }).start();
                    break;
                  }
                }
              }
            }).start();
          }, this);
          this.betAmountScrollView.node.on('scroll-ended', () => {
            const pos = this.betAmountScrollView.getScrollOffset();
            const index = Math.floor((pos.y + this.itemHeight / 2) / this.itemHeight);
            tween(this.betAmountScrollView.content).to(0.2, {
              position: v3(0, index * this.itemHeight, 0)
            }, {
              easing: 'sineOut'
            }).call(() => {
              const target = this.betAmountArray[index];

              if (target) {
                const _line = target['mLine'];

                if (this.lineAmount != _line) {
                  for (let i = this.betLineArray.length - 1; i >= 0; i--) {
                    if (this.betLineArray[i].name == _line) {
                      this.lineAmount = _line;
                      tween(this.betLineScrollView.content).to(0.2, {
                        position: v3(0, (this.betLineArray[i].getSiblingIndex() - 3) * this.itemHeight, 0)
                      }, {
                        easing: 'sineOut'
                      }).call(() => {}).start();
                      break;
                    }
                  }
                }

                const betSizeName = parseInt(target.name) / this.lineAmount;

                for (let i = 0; i < this.betSizeArray.length; i++) {
                  const element = this.betSizeArray[i];

                  if (element.name == betSizeName) {
                    this.betAmount = betSizeName;
                    tween(this.betSizeScrollView.content).to(0.2, {
                      position: v3(0, (element.getSiblingIndex() - 3) * this.itemHeight, 0)
                    }, {
                      easing: 'sineOut'
                    }).call(() => {
                      this.touchResume();
                    }).start();
                    break;
                  }
                }
              }
            }).start();
          }, this);
          this.betLineScrollView.node.on('scroll-ended', () => {
            const pos = this.betLineScrollView.getScrollOffset();
            const index = Math.floor((pos.y + this.itemHeight / 2) / this.itemHeight); // console.log('index:'+index);

            tween(this.betLineScrollView.content).to(0.2, {
              position: v3(0, index * this.itemHeight, 0)
            }, {
              easing: 'sineOut'
            }).call(() => {
              const target = this.betLineArray[index];

              if (target) {
                console.log('target:' + target.name);
                this.lineAmount = parseInt(target.name);
                const amountName = this.lineAmount * this.betAmount;

                for (let i = 0; i < this.betAmountArray.length; i++) {
                  const element = this.betAmountArray[i];

                  if (element.name == amountName) {
                    tween(this.betAmountScrollView.content).to(0.2, {
                      position: v3(0, (element.getSiblingIndex() - 3) * this.itemHeight, 0)
                    }, {
                      easing: 'sineOut'
                    }).call(() => {
                      this.touchResume();
                    }).start();
                    break;
                  }
                }
              }
            }).start();
          }, this);
          this.maxBet.on('click', () => {
            globalThis.subGamePlayShotAudio('subGameClick');

            if (this.touchEnable) {
              this.touchEnable = false;
              this.betAmountScrollView.enabled = false;
              this.touchForbid();
              const index = this.betAmountArray.length - 1;
              this.betAmount = parseInt(this.betAmountArray[index].name) / this.lineAmount;
              tween(this.betSizeScrollView.content).to(0.2, {
                position: v3(0, index * this.itemHeight, 0)
              }, {
                easing: 'sineOut'
              }).call(() => {
                const target = this.betSizeArray[index];

                if (target) {
                  const amountName = parseInt(target.name) * this.lineAmount;

                  for (let i = 0; i < this.betAmountArray.length; i++) {
                    const element = this.betAmountArray[i];

                    if (element.name == amountName) {
                      tween(this.betAmountScrollView.content).to(0.2, {
                        position: v3(0, (element.getSiblingIndex() - 3) * this.itemHeight, 0)
                      }, {
                        easing: 'sineOut'
                      }).call(() => {
                        this.touchResume();
                      }).start();
                      break;
                    }
                  }
                }
              }).start();
            }
          }, this);
          this.comfirmBtn.on('click', () => {
            globalThis.subGamePlayShotAudio('subGameClick');

            if (this.touchEnable) {
              console.log('this.betAmount:' + this.betAmount);
              director.emit('betAmountChange', this.betAmount, this.lineAmount);
              this.disappearViewAnima();
              this.touchEnable = false;
            }
          }, this);
          this.closeBtn.on('click', () => {
            globalThis.subGamePlayShotAudio('subGameClick');
            this.disappearViewAnima();
          }, this);
          this.loaded = true;
          this.updateView();
        }

        onView() {
          if (this.loaded) {
            this.updateView();
          }
        }

        initData(betAmount) {
          this.betAmountOri = betAmount;
          this.betAmount = betAmount;
        }

        updateView() {
          for (let i = 0; i < this.betSizeArray.length; i++) {
            const element = this.betSizeArray[i];

            if (element.name == this.betAmount) {
              this.betSizeScrollView.content.position = v3(0, -element.position.y - 2 * this.itemHeight, 0);

              for (let x = 0; x < this.betAmountArray.length; x++) {
                const amount = this.betAmountArray[x];

                if (amount.name == this.betAmount * this.lineAmount) {
                  this.betAmountScrollView.content.position = v3(0, (amount.getSiblingIndex() - 3) * this.itemHeight, 0);
                  break;
                }
              }
            }
          } // const index = this.betAmountArray.length - 1;
          // tween(this.betSizeScrollView.content).to(0.2,{position:v3(0,index * this.itemHeight,0)},{easing:'sineOut'}).call(()=>{
          //     const target = this.betSizeArray[index];
          //     if(target){
          //         const amountName = parseInt(target.name) * this.lineAmount;
          //         for (let i = 0; i < this.betAmountArray.length; i++) {
          //             const element = this.betAmountArray[i];
          //             if(element.name == amountName){
          //                 tween(this.betAmountScrollView.content).to(0.2,{position:v3(0,(element.getSiblingIndex()-3) * this.itemHeight,0)},{easing:'sineOut'}).call(()=>{
          //                     this.touchResume();
          //                 }).start();
          //                 break;
          //             }
          //         }
          //     }
          // }).start();

        }

        touchForbid() {
          this.touchEnable = false;
          this.maxBet.getComponent(UIOpacity).opacity = 100;
          this.comfirmBtn.getComponent(UIOpacity).opacity = 100;
          this.maxBet.getComponent(Button).interactable = false;
          this.comfirmBtn.getComponent(Button).interactable = false;
        }

        touchResume() {
          this.touchEnable = true;
          this.betSizeScrollView.enabled = true;
          this.betLineScrollView.enabled = true;
          this.betAmountScrollView.enabled = true;
          this.maxBet.getComponent(UIOpacity).opacity = 255;
          this.comfirmBtn.getComponent(UIOpacity).opacity = 255;
          this.maxBet.getComponent(Button).interactable = true;
          this.comfirmBtn.getComponent(Button).interactable = true;
        }

        appearViewAnima() {
          this.touchEnable = true;
          const viewHeight = this.node.getComponent(UITransform).contentSize.height;
          const mainNode = this.node.getChildByName('main');
          const panelHeight = mainNode.getComponent(UITransform).height;
          const opa = mainNode.getChildByName('content').getComponent(UIOpacity);
          const num = mainNode.getChildByPath('content/num').getComponent(UIOpacity); // opa.opacity = 0;
          // num.opacity = 0;

          tween(mainNode).to(0.3, {
            position: v3(0, panelHeight - viewHeight / 2, 0)
          }, {
            easing: 'sineOut'
          }).call(() => {// this.numNode.active = true;
            // tween(opa).to(0.2,{opacity:255}).call(()=>{
            //     tween(num).to(0.2,{opacity:255}).start();
            // }).start();
          }).start();
        }

        disappearViewAnima() {
          if (this.touchEnable) {
            this.touchEnable = false;
            const viewHeight = this.node.getComponent(UITransform).contentSize.height;
            const mainNode = this.node.getChildByName('main');
            tween(mainNode).to(0.3, {
              position: v3(0, -viewHeight / 2, 0)
            }, {
              easing: 'sineOut'
            }).call(() => {
              this.touchEnable = true;
              this.node.active = false; // this.numNode.active = false;

              director.emit('viewChange', 'UISelectBet');
            }).start();
          }
        }

        betInfoSet(coin, bet, win) {
          this.ownLabel.string = this.AddCommas(coin);
          this.betLabel.string = this.AddCommas(bet);
          this.winLabel.string = this.AddCommas(win);
        }

        AddCommas(money) {
          if (!money) {
            return "0";
          } else {
            let mon;

            if (typeof money == "string") {
              mon = Number(money);
            } else {
              mon = money;
            }

            let coinRate = 1;

            if (globalThis.getCoinRate) {
              coinRate = globalThis.getCoinRate();
            }

            let byte = coinRate.toString().length - 1;
            ;
            let showMoney = (mon / coinRate).toFixed(byte);
            let tempmoney = String(showMoney);
            var left = tempmoney.split('.')[0],
                right = tempmoney.split('.')[1];
            right = right ? right.length >= byte ? '.' + right.substring(0, byte) : '.' + right : '';
            var temp = left.split('').reverse().join('').match(/(\d{1,3})/g);

            if (temp) {
              return (Number(tempmoney) < 0 ? "-" : "") + temp.join(',').split('').reverse().join('') + right;
            } else {
              return '';
            }
          }
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "comfirmBtn", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "maxBet", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "closeBtn", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "ownLabel", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor5 = _applyDecoratedDescriptor(_class2.prototype, "betLabel", [_dec6], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor6 = _applyDecoratedDescriptor(_class2.prototype, "winLabel", [_dec7], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor7 = _applyDecoratedDescriptor(_class2.prototype, "betSizeScrollView", [_dec8], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor8 = _applyDecoratedDescriptor(_class2.prototype, "betLineScrollView", [_dec9], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor9 = _applyDecoratedDescriptor(_class2.prototype, "betAmountScrollView", [_dec10], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor10 = _applyDecoratedDescriptor(_class2.prototype, "betSizeItemCopy", [_dec11], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor11 = _applyDecoratedDescriptor(_class2.prototype, "betLineItemCopy", [_dec12], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor12 = _applyDecoratedDescriptor(_class2.prototype, "betAmountItemCopy", [_dec13], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor13 = _applyDecoratedDescriptor(_class2.prototype, "numNode", [_dec14], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=UISelectBet.js.map
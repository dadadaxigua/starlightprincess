System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2", "__unresolved_3", "__unresolved_4", "__unresolved_5", "__unresolved_6"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, director, macro, sAudioMgr, sBoxMgr, sConfigMgr, sNormalFlow, sObjPool, sUtil, _dec, _class, _temp, _crd, ccclass, property, mNormalFlow;

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _reportPossibleCrUseOfsAudioMgr(extras) {
    _reporterNs.report("sAudioMgr", "./sAudioMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsBoxMgr(extras) {
    _reporterNs.report("sBoxMgr", "./sBoxMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsConfigMgr(extras) {
    _reporterNs.report("sConfigMgr", "./sConfigMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsNormalFlow(extras) {
    _reporterNs.report("sNormalFlow", "./sNormalFlow", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsObjPool(extras) {
    _reporterNs.report("sObjPool", "./sObjPool", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsUtil(extras) {
    _reporterNs.report("sUtil", "./sUtil", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      director = _cc.director;
      macro = _cc.macro;
    }, function (_unresolved_2) {
      sAudioMgr = _unresolved_2.default;
    }, function (_unresolved_3) {
      sBoxMgr = _unresolved_3.sBoxMgr;
    }, function (_unresolved_4) {
      sConfigMgr = _unresolved_4.sConfigMgr;
    }, function (_unresolved_5) {
      sNormalFlow = _unresolved_5.sNormalFlow;
    }, function (_unresolved_6) {
      sObjPool = _unresolved_6.sObjPool;
    }, function (_unresolved_7) {
      sUtil = _unresolved_7.sUtil;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "128dbGwF4xBdrIJcmIiguFT", "mNormalFlow", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("mNormalFlow", mNormalFlow = (_dec = ccclass('mNormalFlow'), _dec(_class = (_temp = class mNormalFlow extends (_crd && sNormalFlow === void 0 ? (_reportPossibleCrUseOfsNormalFlow({
        error: Error()
      }), sNormalFlow) : sNormalFlow) {
        constructor(...args) {
          super(...args);

          _defineProperty(this, "overEffectArr", []);
        }

        start() {
          super.start();
        }

        rollStopTurboAction(round, rollSpeedMode, clickMode) {
          if (round) {
            this.turboFlowDelayCall(() => {
              (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
                error: Error()
              }), sConfigMgr) : sConfigMgr).instance.SetResData(round['round_symbol_map']);
              (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                error: Error()
              }), sUtil) : sUtil).once('rollStopOneRoundCall', () => {
                director.off('rollOneColumnStop');
                director.off('rollOneColumnBump');
                this.rollShowResAction(round);
              });
              director.on('rollOneColumnBump', col => {
                if (col == 0) {
                  (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                    error: Error()
                  }), sAudioMgr) : sAudioMgr).PlayShotAudio('booo');
                }

                (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                  error: Error()
                }), sAudioMgr) : sAudioMgr).StopAudio();

                const _boxViewSize = (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                  error: Error()
                }), sBoxMgr) : sBoxMgr).instance.getBoxViewSize();

                const _boxSize = (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                  error: Error()
                }), sBoxMgr) : sBoxMgr).instance.getBoxSize();

                const round_symbol_map = round['round_symbol_map'];

                if (round_symbol_map) {
                  let girlBingo = false;
                  let scatterBingo = false;
                  const column = round_symbol_map[col];

                  if (column && Array.isArray(column)) {
                    const yDelta = (_boxSize.y - _boxViewSize.y) / 2;

                    for (let i = yDelta; i < yDelta + _boxViewSize.y; i++) {
                      const data = column[i];

                      if (data >= 1000) {
                        girlBingo = true;
                      } else if (data == 1) {
                        scatterBingo = true;
                      }

                      if (i == yDelta) {
                        if (data == 1004) {
                          (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                            error: Error()
                          }), sAudioMgr) : sAudioMgr).PlayShotAudio('girlSound1_' + ((_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                            error: Error()
                          }), sUtil) : sUtil).RandomInt(0, 2) == 0 ? '0' : '1'));
                        } else if (data == 2004) {
                          (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                            error: Error()
                          }), sAudioMgr) : sAudioMgr).PlayShotAudio('girlSound2_' + ((_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                            error: Error()
                          }), sUtil) : sUtil).RandomInt(0, 2) == 0 ? '0' : '1'));
                        } else if (data == 3004) {
                          (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                            error: Error()
                          }), sAudioMgr) : sAudioMgr).PlayShotAudio('girlSound3_' + ((_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                            error: Error()
                          }), sUtil) : sUtil).RandomInt(0, 2) == 0 ? '0' : '1'));
                        } else if (data == 4004) {
                          (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                            error: Error()
                          }), sAudioMgr) : sAudioMgr).PlayShotAudio('girlSound4_' + ((_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                            error: Error()
                          }), sUtil) : sUtil).RandomInt(0, 2) == 0 ? '0' : '1'));
                        } else if (data == 5004) {
                          (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                            error: Error()
                          }), sAudioMgr) : sAudioMgr).PlayShotAudio('girlSound5_' + ((_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                            error: Error()
                          }), sUtil) : sUtil).RandomInt(0, 2) == 0 ? '0' : '1'));
                        }
                      }
                    }
                  }

                  if (girlBingo) {
                    (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                      error: Error()
                    }), sAudioMgr) : sAudioMgr).PlayShotAudio('girlBingo');
                  }

                  if (scatterBingo) {
                    (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                      error: Error()
                    }), sAudioMgr) : sAudioMgr).PlayShotAudio('scatterBingo');
                  }
                }
              }, this); // for (let i = 0; i < 5; i++) {
              //     sBoxMgr.instance.rollStopBoxBySelf(i,0,false,(_col)=>{
              //         if(_col == 4){
              //             director.emit('rollStopOneRoundCall');
              //         }
              //     });              
              // }

              (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                error: Error()
              }), sBoxMgr) : sBoxMgr).instance.rollStopAllColumn();
            });
          }
        }

        rollStopAction(round) {
          const _boxViewSize = (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
            error: Error()
          }), sBoxMgr) : sBoxMgr).instance.getBoxViewSize();

          const _boxSize = (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
            error: Error()
          }), sBoxMgr) : sBoxMgr).instance.getBoxSize();

          const round_symbol_map = round['round_symbol_map'];
          (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
            error: Error()
          }), sUtil) : sUtil).once('rollStopOneRoundCall', () => {
            director.off('rollOneColumnStop');
            director.off('rollOneColumnBump');
            this.rollShowResAction(round);
          });
          director.on('rollOneColumnBump', col => {
            if (col == _boxViewSize.x - 1) {
              (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                error: Error()
              }), sAudioMgr) : sAudioMgr).StopAudio();
            }

            (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
              error: Error()
            }), sAudioMgr) : sAudioMgr).PlayShotAudio('boxRowStop');

            if (round_symbol_map) {
              let girlBingo = false;
              let scatterBingo = false;
              const column = round_symbol_map[col];

              if (column && Array.isArray(column)) {
                const yDelta = (_boxSize.y - _boxViewSize.y) / 2;

                for (let i = yDelta; i < yDelta + _boxViewSize.y; i++) {
                  const data = column[i];

                  if (data >= 1000) {
                    girlBingo = true;
                  } else if (data == 1) {
                    scatterBingo = true;
                  }

                  if (i == yDelta) {
                    if (data == 1004) {
                      (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                        error: Error()
                      }), sAudioMgr) : sAudioMgr).PlayShotAudio('girlSound1_' + ((_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                        error: Error()
                      }), sUtil) : sUtil).RandomInt(0, 2) == 0 ? '0' : '1'));
                    } else if (data == 2004) {
                      (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                        error: Error()
                      }), sAudioMgr) : sAudioMgr).PlayShotAudio('girlSound2_' + ((_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                        error: Error()
                      }), sUtil) : sUtil).RandomInt(0, 2) == 0 ? '0' : '1'));
                    } else if (data == 3004) {
                      (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                        error: Error()
                      }), sAudioMgr) : sAudioMgr).PlayShotAudio('girlSound3_' + ((_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                        error: Error()
                      }), sUtil) : sUtil).RandomInt(0, 2) == 0 ? '0' : '1'));
                    } else if (data == 4004) {
                      (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                        error: Error()
                      }), sAudioMgr) : sAudioMgr).PlayShotAudio('girlSound4_' + ((_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                        error: Error()
                      }), sUtil) : sUtil).RandomInt(0, 2) == 0 ? '0' : '1'));
                    } else if (data == 5004) {
                      (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                        error: Error()
                      }), sAudioMgr) : sAudioMgr).PlayShotAudio('girlSound5_' + ((_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                        error: Error()
                      }), sUtil) : sUtil).RandomInt(0, 2) == 0 ? '0' : '1'));
                    }
                  }
                }
              }

              if (girlBingo) {
                (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                  error: Error()
                }), sAudioMgr) : sAudioMgr).PlayShotAudio('girlBingo');
              }

              if (scatterBingo) {
                (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                  error: Error()
                }), sAudioMgr) : sAudioMgr).PlayShotAudio('scatterBingo');
              }
            }
          }, this);

          if (round) {
            this.normalFlowDelayCall(() => {
              (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
                error: Error()
              }), sConfigMgr) : sConfigMgr).instance.SetResData(round['round_symbol_map']);
              const events = (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
                error: Error()
              }), sConfigMgr) : sConfigMgr).instance.GetEventConfigByHitListOnCheck(round);
              const boxSize = (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                error: Error()
              }), sBoxMgr) : sBoxMgr).instance.getBoxViewSize();
              let freeWinArr = [],
                  targetColumn = [];

              if (events) {
                for (let i = 0; i < events.length; i++) {
                  const _event = events[i];

                  if (_event.event_type == 'speedAnima') {
                    let actColumn = _event.act_column.split(',');

                    if (actColumn && actColumn.length > 0) {
                      for (let a = 0; a < actColumn.length; a++) {
                        const element = actColumn[a];
                        freeWinArr.push(parseInt(element));
                      }
                    }
                  }
                }
              }

              if (freeWinArr.length > 0) {
                for (let i = 0; i < boxSize.x; i++) {
                  if (freeWinArr.indexOf(i) < 0) {
                    targetColumn.push(i);
                  }
                }

                (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                  error: Error()
                }), sBoxMgr) : sBoxMgr).instance.rollStopByColumnArr(targetColumn, col => {
                  (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                    error: Error()
                  }), sBoxMgr) : sBoxMgr).instance.setColumnAllTargetBoxArrayState(col, symbol => {
                    return symbol > 1000;
                  }, 'spawn', 'settlement');
                  (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                    error: Error()
                  }), sBoxMgr) : sBoxMgr).instance.setColumnTargetBoxArrayState(col, symbol => {
                    return symbol == 1;
                  }, 'win', 'settlement');
                  this.scheduleOnce(() => {
                    (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                      error: Error()
                    }), sBoxMgr) : sBoxMgr).instance.setColumnTargetBoxArrayState(col, symbol => {
                      return symbol == 1;
                    }, 'idle', 'settlement');
                  }, 1);

                  if (col == targetColumn.length - 1) {
                    this.clearColumnOverEffect();
                    (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                      error: Error()
                    }), sAudioMgr) : sAudioMgr).PlayShotAudio('freeWinOneColBingo');
                    this.showColumnOverEffect(targetColumn.length);
                    this.scheduleOnce(() => {
                      (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                        error: Error()
                      }), sBoxMgr) : sBoxMgr).instance.blackCurtainAnima(targetColumn.length);
                    }, 0.5);
                  }
                }, () => {
                  if (freeWinArr.length > 0) {
                    let index = 0;

                    const freeWinCall = () => {
                      if (index < freeWinArr.length) {
                        const col = freeWinArr[index++];
                        this.setMotionTweenSpeedByIndex(col);
                        this.freeWinSpeedAnimaDelayCall(col, () => {
                          (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                            error: Error()
                          }), sBoxMgr) : sBoxMgr).instance.rollStopBoxBySelf(col, 0, false, _col => {
                            (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                              error: Error()
                            }), sBoxMgr) : sBoxMgr).instance.setColumnAllTargetBoxArrayState(col, symbol => {
                              return symbol > 1000;
                            }, 'spawn', 'settlement');
                            (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                              error: Error()
                            }), sBoxMgr) : sBoxMgr).instance.setColumnTargetBoxArrayState(col, symbol => {
                              return symbol == 1;
                            }, 'win', 'settlement');
                            this.scheduleOnce(() => {
                              (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                                error: Error()
                              }), sBoxMgr) : sBoxMgr).instance.setColumnTargetBoxArrayState(col, symbol => {
                                return symbol == 1;
                              }, 'idle', 'settlement');
                            }, 1);

                            if (freeWinArr.indexOf(col + 1) >= 0) {
                              this.clearColumnOverEffect();
                              (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                                error: Error()
                              }), sAudioMgr) : sAudioMgr).PlayShotAudio('freeWinOneColBingo');
                              (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                                error: Error()
                              }), sBoxMgr) : sBoxMgr).instance.blackCurtain(col + 1);
                              this.showColumnOverEffect(_col + 1);
                            }

                            if (_col == boxSize.x - 1) {
                              this.clearColumnOverEffect();
                              (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                                error: Error()
                              }), sBoxMgr) : sBoxMgr).instance.blackCurtainAnima(-1, false);
                              director.emit('rollStopOneRoundCall');
                            }

                            freeWinCall();
                          });
                        });
                      }
                    };

                    freeWinCall();
                  }
                });
              } else {
                (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                  error: Error()
                }), sBoxMgr) : sBoxMgr).instance.rollStopAllColumn();
              }
            });
          }
        }

        byLineBoxLightCtr(round) {
          const events = (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
            error: Error()
          }), sConfigMgr) : sConfigMgr).instance.GetEventConfigByHitListOnCheck(round);
          let titleAnimaArr = [];
          let isWin = false;
          let eventList = [];

          if (events) {
            let resActObj = {};

            for (let i = 0; i < events.length; i++) {
              const _event = events[i];

              if (_event['event_type'] == 'boxAnima') {
                eventList.push(_event);

                const acts = _event['act_pos'].split(',');

                if (acts) {
                  isWin = true;

                  for (let a = 0; a < acts.length; a++) {
                    const act = acts[a];
                    resActObj[act] = true;
                  }
                }
              } else if (_event['event_type'] == 'titleAnima') {
                const acts = _event['act_pos'].split(',');

                if (acts) {
                  for (let a = 0; a < acts.length; a++) {
                    const act = acts[a];
                    resActObj[act] = true;
                  }
                }

                titleAnimaArr.push(_event['act_column']);
              }
            } // console.log('clickMode:',this.clickMode);


            if (isWin) {
              // console.error('normalWin');
              (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                error: Error()
              }), sAudioMgr) : sAudioMgr).PlayShotAudio('normalWin');
              (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                error: Error()
              }), sBoxMgr) : sBoxMgr).instance.blackCurtainAnima();
              director.emit('boxWinInform');
              this.byLineBoxLightOn(resActObj);
            } else {}
          }

          if (titleAnimaArr.length > 0) {
            director.emit('titleMultipleAnima', titleAnimaArr, isWin);
          }

          if (isWin) {
            (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
              error: Error()
            }), sUtil) : sUtil).once('titleWinSettleEnd', () => {
              let index = 0;

              this.roundShowBoxLineInterval = () => {
                if (globalThis.GameBtnEntity.betClickMode == 'normal') {
                  if (index >= eventList.length) {
                    index = 0;
                  }

                  const res = eventList[index++];

                  if (res && res['act_pos']) {
                    const acts = res['act_pos'].split(',');

                    if (acts) {
                      let resSingleActObj = {};

                      for (let a = 0; a < acts.length; a++) {
                        const act = acts[a];
                        resSingleActObj[act] = true;
                      }

                      (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                        error: Error()
                      }), sAudioMgr) : sAudioMgr).PlayShotAudio('oneLineShow');
                      this.byLineBoxLightOn(resSingleActObj);
                    }
                  }
                }
              };

              this.schedule(this.roundShowBoxLineInterval, 2, macro.REPEAT_FOREVER);
              this.titleWinSettleEnd(round);
            });

            if (titleAnimaArr.length > 0) {
              let multiSum = 1;

              for (let i = 0; i < titleAnimaArr.length; i++) {
                const value = titleAnimaArr[i];
                multiSum *= parseInt(value) + 1;
              }

              const realRate = round.rate;

              if (!(titleAnimaArr.length == 1 && titleAnimaArr[0] == 0)) {
                director.emit('lowWinTiltleAnima', Math.floor(globalThis.GameBtnEntity.CurrentBetAmount * round.rate / multiSum));
              }

              if (titleAnimaArr.length == 1 && titleAnimaArr[0] == 0) {
                director.emit('winBetRes', globalThis.GameBtnEntity.CurrentBetAmount * realRate, realRate);
              } else {
                (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                  error: Error()
                }), sUtil) : sUtil).once('titleMultipleAnimaEnd', () => {
                  director.emit('winBetRes', globalThis.GameBtnEntity.CurrentBetAmount * realRate, realRate);
                });
              }
            } else {
              director.emit('winBetRes', globalThis.GameBtnEntity.CurrentBetAmount * round.rate, round.rate);
            }
          } else {
            this.titleWinSettleEnd(round);
          }
        }

        showColumnOverEffect(col) {
          director.emit('freeGameStart', col);
          let res = (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
            error: Error()
          }), sBoxMgr) : sBoxMgr).instance.GetColAllBoxPos(col);

          if (res && res.length > 0) {
            for (let i = 0; i < res.length; i++) {
              const pos = res[i];
              const overEffect = (_crd && sObjPool === void 0 ? (_reportPossibleCrUseOfsObjPool({
                error: Error()
              }), sObjPool) : sObjPool).Dequeue('overLayerEffectItem');
              overEffect.node.position = pos;
              overEffect.node.active = true;
              overEffect.boxItemUpdate(null, null, 'win', null);
              this.overEffectArr.push(overEffect);
            }
          }
        }

        clearColumnOverEffect() {
          director.emit('freeGameEnd');

          if (this.overEffectArr && this.overEffectArr.length > 0) {
            for (let i = 0; i < this.overEffectArr.length; i++) {
              const over = this.overEffectArr[i];
              (_crd && sObjPool === void 0 ? (_reportPossibleCrUseOfsObjPool({
                error: Error()
              }), sObjPool) : sObjPool).Enqueue('overLayerEffectItem', over);
              over.clearItem(null, null, null);
              over.node.active = false;
            }

            this.overEffectArr = [];
          }
        }

      }, _temp)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=mNormalFlow.js.map
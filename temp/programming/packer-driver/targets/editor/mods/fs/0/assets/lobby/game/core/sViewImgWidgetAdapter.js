System.register(["cc"], function (_export, _context) {
  "use strict";

  var _cclegacy, _decorator, Component, v3, view, _dec, _class, _class2, _descriptor, _temp, _crd, ccclass, property, sViewImgWidgetAdapter;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  return {
    setters: [function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      Component = _cc.Component;
      v3 = _cc.v3;
      view = _cc.view;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "045abDeCQtFEY7H+EMJ8Ws8", "sViewImgWidgetAdapter", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("sViewImgWidgetAdapter", sViewImgWidgetAdapter = (_dec = ccclass('sViewImgWidgetAdapter'), _dec(_class = (_class2 = (_temp = class sViewImgWidgetAdapter extends Component {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "designWidth", _descriptor, this);
        }

        onLoad() {
          const viewSize = view.getVisibleSize();

          if (this.designWidth > 0 && viewSize && viewSize.width > 0 && viewSize.width > this.designWidth) {
            const delta = viewSize.width / this.designWidth;
            this.node.scale = v3(this.node.scale.x * delta, this.node.scale.y * delta, this.node.scale.z * delta);
          }
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "designWidth", [property], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return 0;
        }
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=sViewImgWidgetAdapter.js.map
System.register(["cc"], function (_export, _context) {
  "use strict";

  var _cclegacy, AudioSource, director, Node, sys, tween, sAudioMgr, _crd, bgAudioClipID;

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  return {
    setters: [function (_cc) {
      _cclegacy = _cc.cclegacy;
      AudioSource = _cc.AudioSource;
      director = _cc.director;
      Node = _cc.Node;
      sys = _cc.sys;
      tween = _cc.tween;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "0e818tl7AdFY7dMG45DFk1I", "sAudioMgr", undefined);

      bgAudioClipID = null;
      sAudioMgr = class sAudioMgr {
        static PlayShotAudio(path, volume = 1) {
          if (sAudioMgr.audioBgVolume == '1') {
            if (!sAudioMgr.m_AudioShotSource || !sAudioMgr.m_AudioShotSource.isValid || !sAudioMgr.m_AudioShotSource.node || !sAudioMgr.m_AudioShotSource.node.isValid) {
              if (sAudioMgr.m_AudioShotSource && sAudioMgr.m_AudioShotSource.node) {
                // console.log('m_AudioShotSource destroy : ' + path);
                sAudioMgr.m_AudioShotSource.node.destroy();
              }

              let audioNode = new Node();
              audioNode.name = 'sAudioShotMgr';
              let parentNode = director.getScene().children[0];

              if (parentNode) {
                // console.log('m_AudioShotSource create : ' + path);
                parentNode.addChild(audioNode);
              }

              sAudioMgr.m_AudioShotSource = audioNode.addComponent(AudioSource);
              sAudioMgr.m_AudioShotSource.playOnAwake = false;
              sAudioMgr.m_AudioShotSource.volume = 1;
            }

            if (sAudioMgr.m_AudioShotSource) {
              const clip = sAudioMgr.GetAudioClipByName(path);

              if (clip) {
                // console.log('PlayShotAudio : ' + path +','+ clip.name);
                sAudioMgr.m_AudioShotSource.playOneShot(clip, volume);
              }
            }
          }
        }

        static PlayAudio(path, isLoop = false, vol = 1) {
          if (sAudioMgr.audioBgVolume == '1') {
            if (!sAudioMgr.m_AudioSource || !sAudioMgr.m_AudioSource.isValid || !sAudioMgr.m_AudioSource.node || !sAudioMgr.m_AudioSource.node.isValid) {
              if (sAudioMgr.m_AudioSource && sAudioMgr.m_AudioSource.node) {
                sAudioMgr.m_AudioSource.node.destroy();
              }

              let audioNode = new Node();
              audioNode.name = 'sAudioMgr';
              let parentNode = director.getScene().children[0];

              if (parentNode) {
                parentNode.addChild(audioNode);
              }

              sAudioMgr.m_AudioSource = audioNode.addComponent(AudioSource);
              sAudioMgr.m_AudioSource.playOnAwake = false;
              sAudioMgr.m_AudioSource.volume = 1;
            }

            if (sAudioMgr.m_AudioSource) {
              const clip = sAudioMgr.GetAudioClipByName(path);

              if (clip) {
                sAudioMgr.m_AudioSource.stop();
                sAudioMgr.m_AudioSource.loop = isLoop;
                sAudioMgr.m_AudioSource.clip = clip;
                sAudioMgr.m_AudioSource.volume = vol;
                sAudioMgr.m_AudioSource.play();
              }
            }
          }
        }

        static PlayAudioByID(id, path, isLoop = false, vol = 1) {
          if (sAudioMgr.audioBgVolume == '1' && id) {
            let _audioSource = sAudioMgr.m_AudioSourceIds.get(id);

            if (!_audioSource) {
              let audioNode = new Node();
              audioNode.name = 'sAudioMgr_' + id;
              let parentNode = director.getScene().children[0];

              if (parentNode) {
                parentNode.addChild(audioNode);
              }

              _audioSource = audioNode.addComponent(AudioSource);
              _audioSource.playOnAwake = false;
              _audioSource.volume = 1;
              sAudioMgr.m_AudioSourceIds.set(id, _audioSource);
            }

            const clip = sAudioMgr.GetAudioClipByName(path);

            if (clip) {
              if (_audioSource.playing) {
                _audioSource.stop();
              }

              _audioSource.loop = isLoop;
              _audioSource.clip = clip;
              _audioSource.volume = vol;

              _audioSource.play();
            }
          }
        }

        static StopAudioByID(id) {
          let _audioSource = sAudioMgr.m_AudioSourceIds.get(id);

          if (_audioSource && _audioSource.isValid) {
            _audioSource.stop();
          }
        }

        static PauseAudioByID(id) {
          let _audioSource = sAudioMgr.m_AudioSourceIds.get(id);

          if (_audioSource && _audioSource.isValid) {
            _audioSource.pause();
          }
        }

        static ResumeAudioByID(id) {
          let _audioSource = sAudioMgr.m_AudioSourceIds.get(id);

          if (_audioSource && _audioSource.isValid) {
            _audioSource.play();
          }
        }

        static bgMusicOut(time, targetVolume) {
          if (sAudioMgr.m_BGAudioSource && sAudioMgr.m_BGAudioSource.isValid && sAudioMgr.audioBgVolume == '1') {
            sAudioMgr.clearAudioTween();
            sAudioMgr.bgtween = tween(sAudioMgr.m_BGAudioSource).to(time, {
              volume: targetVolume
            }).call(() => {
              sAudioMgr.bgtween = null;
            }).start();
          }
        }

        static clearAudioTween() {
          sAudioMgr.bgtween && sAudioMgr.bgtween.stop();
          sAudioMgr.bgtween = null;
        }

        static SetBGMusicVolume(vol) {
          if (sAudioMgr.m_BGAudioSource && sAudioMgr.m_BGAudioSource.isValid && sAudioMgr.audioBgVolume == '1') {
            sAudioMgr.m_BGAudioSource.volume = vol;
          }
        }

        static StopAudio(time = 0) {
          if (sAudioMgr.m_AudioSource && sAudioMgr.m_AudioSource.playing) {
            if (time == 0) {
              sAudioMgr.m_AudioSource.stop();
            } else {
              tween(sAudioMgr.m_AudioSource).to(time, {
                volume: 0
              }).call(() => {
                sAudioMgr.m_AudioSource.stop();
              }).start();
            }

            sAudioMgr.m_AudioSource.loop = false;
          }
        }

        static StopBGAudio() {
          if (sAudioMgr.m_BGAudioSource) {
            sAudioMgr.m_BGAudioSource.stop();
          }
        }

        static PlayBG(path, isLoop = true) {
          sAudioMgr.bgPath = path;

          if (!sAudioMgr.m_BGAudioSource || !sAudioMgr.m_BGAudioSource.isValid || !sAudioMgr.m_BGAudioSource.node || !sAudioMgr.m_BGAudioSource.node.isValid) {
            if (sAudioMgr.m_BGAudioSource && sAudioMgr.m_BGAudioSource.node) {
              sAudioMgr.m_BGAudioSource.node.destroy();
            }

            let audioNode = new Node();
            audioNode.name = 'sAudioBGMgr';
            let parentNode = director.getScene().children[0];

            if (parentNode) {
              parentNode.addChild(audioNode);
            }

            sAudioMgr.m_BGAudioSource = audioNode.addComponent(AudioSource);
            sAudioMgr.m_BGAudioSource.playOnAwake = false;
            sAudioMgr.m_BGAudioSource.volume = 1;
          }

          if (sAudioMgr.audioBgVolume == '1') {
            if (sAudioMgr.m_BGAudioSource) {
              sAudioMgr.m_BGAudioSource.stop();
              sAudioMgr.m_BGAudioSource.loop = isLoop;
              const clip = sAudioMgr.GetAudioClipByName(path);
              sAudioMgr.m_BGAudioSource.clip = clip;
              sAudioMgr.m_BGAudioSource.volume = sAudioMgr.bgVolume;
              sAudioMgr.m_BGAudioSource.play();
            }
          } else {
            if (sAudioMgr.m_BGAudioSource) {
              sAudioMgr.m_BGAudioSource.loop = isLoop;
              sAudioMgr.m_BGAudioSource.stop();
            }
          }
        }

        static SetAudioClip(clip) {
          if (clip) {
            sAudioMgr.clipList[clip.name] = clip;
          }
        }

        static GetAudioClipByName(path) {
          let res = sAudioMgr.clipList[path];

          if (res) {
            return res;
          } else {
            return null;
          }
        }

        static AudioInit() {
          sAudioMgr.bgVolume = 1;
          let audioBg = sys.localStorage.getItem('audioBgVolume');

          if (audioBg || audioBg == '0') {
            sAudioMgr.audioBgVolume = audioBg;
          } // sAudioMgr.audioBgVolume = '0';

        }

        static SetBGVolume(value) {
          sAudioMgr.bgVolume = value;
        }

        static AudioDataInit() {
          let audioBg = sys.localStorage.getItem('audioBgVolume');

          if (audioBg || audioBg == '0') {
            sAudioMgr.audioBgVolume = audioBg;
          } else {
            sAudioMgr.audioBgVolume = '0';
          }
        }

        static GetAudioData() {
          return {
            audioVolume: sAudioMgr.audioBgVolume
          };
        }

        static CloseAudioVolume() {
          sAudioMgr.audioBgVolume = '0';

          if (sAudioMgr.m_AudioSource) {
            sAudioMgr.m_AudioSource.stop();
          }

          if (sAudioMgr.m_BGAudioSource) {
            sAudioMgr.m_BGAudioSource.stop();
          }

          sys.localStorage.setItem('audioBgVolume', '0');
        }

        static OpenAudioVolume() {
          sAudioMgr.audioBgVolume = '1';
          sys.localStorage.setItem('audioBgVolume', '1');

          if (sAudioMgr.m_BGAudioSource) {
            if (!sAudioMgr.m_BGAudioSource.clip) {
              const clip = sAudioMgr.GetAudioClipByName(sAudioMgr.bgPath ? sAudioMgr.bgPath : 'bgm_mg');
              sAudioMgr.m_BGAudioSource.clip = clip;
              sAudioMgr.m_BGAudioSource.volume = sAudioMgr.bgVolume;
            }

            sAudioMgr.m_BGAudioSource.play();
          }
        }

        static Clear() {
          sAudioMgr.m_AudioShotSource = null;
          sAudioMgr.m_AudioSource = null;
          sAudioMgr.m_AudioSourceIds.clear();
          sAudioMgr.m_BGAudioSource = null;
          sAudioMgr.clipList = {};
          sAudioMgr.audioBgVolume = null;
          sAudioMgr.clearAudioTween();
          sAudioMgr.bgPath = null;
        }

      };

      _defineProperty(sAudioMgr, "m_AudioShotSource", null);

      _defineProperty(sAudioMgr, "m_AudioSource", null);

      _defineProperty(sAudioMgr, "m_BGAudioSource", null);

      _defineProperty(sAudioMgr, "m_AudioSourceIds", new Map());

      _defineProperty(sAudioMgr, "clipList", {});

      _defineProperty(sAudioMgr, "audioBgVolume", null);

      _defineProperty(sAudioMgr, "bgPath", null);

      _defineProperty(sAudioMgr, "bgVolume", 1);

      _defineProperty(sAudioMgr, "bgtween", null);

      globalThis.setSubGameAudioClip = function (clip) {
        sAudioMgr.SetAudioClip(clip);
      };

      globalThis.subGamePlayShotAudio = function (path) {
        sAudioMgr.PlayShotAudio(path);
      };

      globalThis.subGameAudioInit = function () {
        sAudioMgr.AudioDataInit();
      };

      globalThis.subGameAudioVolumeCtr = function (value) {
        if (value == true) {
          sAudioMgr.OpenAudioVolume();
        } else if (value == false) {
          sAudioMgr.CloseAudioVolume();
        }
      };

      globalThis.getSubGameAudioVolume = function () {
        return sAudioMgr.GetAudioData();
      };

      _export("default", sAudioMgr);

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=sAudioMgr.js.map
System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2", "__unresolved_3"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, instantiate, Node, Prefab, sp, sBoxItemBase, sObjPool, sUtil, _dec, _dec2, _dec3, _dec4, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _temp, _crd, ccclass, property, sJackpotBoxItem;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfsBoxItemBase(extras) {
    _reporterNs.report("sBoxItemBase", "./sBoxItemBase", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsObjPool(extras) {
    _reporterNs.report("sObjPool", "./sObjPool", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsUtil(extras) {
    _reporterNs.report("sUtil", "./sUtil", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      instantiate = _cc.instantiate;
      Node = _cc.Node;
      Prefab = _cc.Prefab;
      sp = _cc.sp;
    }, function (_unresolved_2) {
      sBoxItemBase = _unresolved_2.sBoxItemBase;
    }, function (_unresolved_3) {
      sObjPool = _unresolved_3.sObjPool;
    }, function (_unresolved_4) {
      sUtil = _unresolved_4.sUtil;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "9e7d20iHgdGhoSHZARQyTOH", "sJackpotBoxItem", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("sJackpotBoxItem", sJackpotBoxItem = (_dec = ccclass('sJackpotBoxItem'), _dec2 = property(Node), _dec3 = property(Node), _dec4 = property(sp.Skeleton), _dec(_class = (_class2 = (_temp = class sJackpotBoxItem extends (_crd && sBoxItemBase === void 0 ? (_reportPossibleCrUseOfsBoxItemBase({
        error: Error()
      }), sBoxItemBase) : sBoxItemBase) {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "iconNode", _descriptor, this);

          _initializerDefineProperty(this, "animaNode", _descriptor2, this);

          _initializerDefineProperty(this, "spineNode", _descriptor3, this);

          _initializerDefineProperty(this, "spineAnimaName", _descriptor4, this);
        }

        start() {
          super.start(); // this.scheduleOnce(()=>{
          //     this.boxItemUpdate(null,null,'win',null);
          // },2);
        }

        boxItemUpdate(renderData, symbolValue, boxState, tableState, animiaFinishCallBack = null) {
          if (boxState == 'rest') {
            if (this.iconNode && this.animaNode) {
              this.iconNode.active = true;
              this.animaNode.active = false;
            }
          } else if (boxState == 'win') {
            if (this.node.active) {
              if (this.iconNode && this.animaNode) {
                this.animaNode.active = true;
                this.iconNode.active = false;
              }

              if (this.spineNode && this.spineAnimaName) {
                this.spineNode.setAnimation(0, this.spineAnimaName, false);
              }
            }
          }
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "iconNode", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "animaNode", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "spineNode", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "spineAnimaName", [property], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return '';
        }
      })), _class2)) || _class));

      globalThis.soltCreateJackpotNode = function (call = null) {
        if ((_crd && sObjPool === void 0 ? (_reportPossibleCrUseOfsObjPool({
          error: Error()
        }), sObjPool) : sObjPool).GetObjCall('slotJackpot')) {
          if (call != null) {
            call((_crd && sObjPool === void 0 ? (_reportPossibleCrUseOfsObjPool({
              error: Error()
            }), sObjPool) : sObjPool).Dequeue('slotJackpot'));
          }
        } else {
          (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
            error: Error()
          }), sUtil) : sUtil).getAssetInSubGame(globalThis.currentPlayingGameID, 'prefabs/jackpot', Prefab, (err, asset) => {
            if (asset) {
              (_crd && sObjPool === void 0 ? (_reportPossibleCrUseOfsObjPool({
                error: Error()
              }), sObjPool) : sObjPool).SetObj('slotJackpot', () => {
                const obj = instantiate(asset);
                return obj;
              });

              if (call != null) {
                call((_crd && sObjPool === void 0 ? (_reportPossibleCrUseOfsObjPool({
                  error: Error()
                }), sObjPool) : sObjPool).Dequeue('slotJackpot'));
              }
            }
          });
        }
      };

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=sJackpotBoxItem.js.map
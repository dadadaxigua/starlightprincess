System.register(["__unresolved_0", "cc", "__unresolved_1"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, Component, Label, Color, sUtil, _dec, _dec2, _dec3, _dec4, _class, _class2, _descriptor, _descriptor2, _descriptor3, _temp, _crd, ccclass, property, gameRecordItem;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfsUtil(extras) {
    _reporterNs.report("sUtil", "../../../../game/core/sUtil", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      Component = _cc.Component;
      Label = _cc.Label;
      Color = _cc.Color;
    }, function (_unresolved_2) {
      sUtil = _unresolved_2.sUtil;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "78d5c7JQJtPW6viYo/szCCV", "gameRecordItem", undefined);

      ({
        ccclass,
        property
      } = _decorator);
      /**
       * Predefined variables
       * Name = gameRecordItem
       * DateTime = Thu Nov 03 2022 11:51:03 GMT+0800 (中国标准时间)
       */

      _export("gameRecordItem", gameRecordItem = (_dec = ccclass('gameRecordItem'), _dec2 = property(Label), _dec3 = property(Label), _dec4 = property(Label), _dec(_class = (_class2 = (_temp = class gameRecordItem extends Component {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "timeLab", _descriptor, this);

          _initializerDefineProperty(this, "betLab", _descriptor2, this);

          _initializerDefineProperty(this, "profitLab", _descriptor3, this);

          _defineProperty(this, "colors", [new Color('36f06a'), new Color('f03636'), new Color('ffffff')]);
        }

        onLoad() {}

        init(data, type) {
          if (type == 'poker') {
            this.initPokerItem(data);
          } else if (type == 'solt') {
            this.initSoltItem(data);
          }
        }

        initPokerItem(data) {
          let date = new Date(+data.bet_time);
          let m = +date.getMinutes();
          let s = +date.getSeconds();
          let mTime = m < 10 ? "0" + m : m;
          let sTime = s < 10 ? "0" + s : s;
          let timeStr1 = `${date.getHours()}:${mTime}:${sTime}`;
          let timeStr2 = `${date.getMonth() + 1}/${date.getDate()}`;
          this.timeLab.string = `${timeStr2} ${timeStr1}`;

          if (data.bet_amount || data.bet_amount == 0) {
            this.betLab.string = (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
              error: Error()
            }), sUtil) : sUtil).AddCommas(data.bet_amount);
          } else {
            this.betLab.string = '-';
          }

          if (data.coin_change >= 0) {
            this.profitLab.color = this.colors[0];
          } else {
            this.profitLab.color = this.colors[1];
          }

          let sub = data.coin_change >= 0 ? '+' : '-';
          sub = data.coin_change == 0 ? '' : sub;

          if (data.coin_change || data.coin_change == 0) {
            this.profitLab.string = sub + (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
              error: Error()
            }), sUtil) : sUtil).AddCommas(Math.abs(data.coin_change));
          } else {
            this.profitLab.color = this.colors[2];
            this.profitLab.string = '-';
          }
        }

        initSoltItem(data) {
          let date = new Date(data.time);
          let m = +date.getMinutes();
          let s = +date.getSeconds();
          let mTime = m < 10 ? "0" + m : m;
          let sTime = s < 10 ? "0" + s : s;
          let timeStr1 = `${date.getHours()}:${mTime}:${sTime}`;
          let timeStr2 = `${date.getMonth() + 1}/${date.getDate()}`;
          this.timeLab.string = `${timeStr2}\n${timeStr1}`;

          if (data.bet || data.bet == 0) {
            this.betLab.string = (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
              error: Error()
            }), sUtil) : sUtil).AddCommas(data.bet);
          } else {
            this.betLab.string = '-';
          }

          if (data.profit >= 0) {
            this.profitLab.color = this.colors[0];
          } else {
            this.profitLab.color = this.colors[1];
          }

          let sub = data.profit >= 0 ? '+' : '-';
          sub = data.profit == 0 ? '' : sub;

          if (data.profit || data.profit == 0) {
            this.profitLab.string = sub + (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
              error: Error()
            }), sUtil) : sUtil).AddCommas(Math.abs(data.profit));
          } else {
            this.profitLab.color = this.colors[2];
            this.profitLab.string = '-';
          }
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "timeLab", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "betLab", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "profitLab", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=gameRecordItem.js.map
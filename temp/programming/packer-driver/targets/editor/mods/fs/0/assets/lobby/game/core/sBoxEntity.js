System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2", "__unresolved_3"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, Vec3, v3, tween, sBoxItemBase, sConfigMgr, sSlotSceneInfo, _dec, _class, _temp, _crd, ccclass, property, sBoxEntity;

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _reportPossibleCrUseOfsBoxItemBase(extras) {
    _reporterNs.report("sBoxItemBase", "./sBoxItemBase", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsConfigMgr(extras) {
    _reporterNs.report("sConfigMgr", "./sConfigMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsSlotSceneInfo(extras) {
    _reporterNs.report("sSlotSceneInfo", "./sSlotSceneInfo", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      Vec3 = _cc.Vec3;
      v3 = _cc.v3;
      tween = _cc.tween;
    }, function (_unresolved_2) {
      sBoxItemBase = _unresolved_2.sBoxItemBase;
    }, function (_unresolved_3) {
      sConfigMgr = _unresolved_3.sConfigMgr;
    }, function (_unresolved_4) {
      sSlotSceneInfo = _unresolved_4.sSlotSceneInfo;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "37583r9Uy5DFpCMewENUaSA", "sBoxEntity", undefined);

      ({
        ccclass,
        property
      } = _decorator); //tableState : ['idle','rolling','settlement']  桌子状态 ['闲置','转动中','结算中']
      //rollState : ['idle','roll'，'rolling','win','spawn','rest']  格子状态 ['闲置','刚开始转动','快速转动(图变模糊)','中奖','有机会抽中免费游戏','转动停止/静态图状态']
      //SymbolValue : 格子对应的索引 必须用get方法获取
      //sBoxItem : 每个格子的组件类 sEffectItem : 每个格子之上层的格子组件类 mOverLayerEffect : 最上层的特效组件类
      //clearItem 清除格子所有渲染对象
      //boxTypeAction 当设置为floating 时 每个状态会触发的方法
      //singleBoxAction 单个格子floating任何状态触发方法
      //multiBoxAction 长条格子floating任何状态触发方法
      //各个行为触发的状态: 
      //### 刚进入游戏 : [rollState : 'idle',tableState : 'idle'] 
      //### 点击下注 : clearItem : {[rollState : 'rest',tableState : 'idle']}, updateData : {[rollState : 'rest',tableState : 'idle']}
      //### 格子向上稍微移动 : updateData : {[rollState : 'roll',tableState : 'rolling']} 
      //### 格子向下快速移动 : updateData : {[rollState : 'rolling',tableState : 'rolling']} 
      //### 格子快停止 : updateData : {[rollState : 'rest',tableState : 'rolling']} 
      //### 停止 : updateData : {[rollState : 'idle',tableState : 'settlement']}
      //### 中奖格子显示 : clearItem : {[rollState : 'rest',tableState : 'settlement']}, {未中奖格子 : updateData : {[rollState : 'rest',tableState : 'settlement']} ,中奖格子 : updateData : {[rollState : 'win',tableState : 'settlement']}}

      _export("sBoxEntity", sBoxEntity = (_dec = ccclass('sBoxEntity'), _dec(_class = (_temp = class sBoxEntity {
        constructor() {
          _defineProperty(this, "boxObj", null);

          _defineProperty(this, "itemIndex", void 0);

          _defineProperty(this, "viewItemIndex", void 0);

          _defineProperty(this, "symbolValue", -1);

          _defineProperty(this, "boxData", void 0);

          _defineProperty(this, "boxPos", void 0);

          _defineProperty(this, "moveBoxNode", void 0);

          _defineProperty(this, "willDisappear", false);

          _defineProperty(this, "boxStateNow", '');

          _defineProperty(this, "locked", false);

          _defineProperty(this, "isDropBox", false);

          _defineProperty(this, "isCreateBox", false);

          _defineProperty(this, "isDirty", false);

          _defineProperty(this, "isJackpotBox", false);

          _defineProperty(this, "actionArr", {});

          _defineProperty(this, "jackpotNode", void 0);

          _defineProperty(this, "jackpotNodess", 0);
        }

        /**
         * 当前box的相对坐标
         */
        set BoxPos(value) {
          this.boxPos = value;
        }

        set ItemIndex(value) {
          this.itemIndex = value; // this.boxObj.box.node['kkk'] = value;
          // this.boxObj.floatBox.node['kkk'] = value;

          this.symbolValue = this.getSymbolValue();
        }
        /**
         * 获取当前box的索引值
         */


        get ItemIndex() {
          return this.itemIndex;
        }
        /**
         * 获取当前box的内容编号
         */


        get SymbolValue() {
          return this.symbolValue;
        }

        set SymbolValue(value) {
          this.symbolValue = value;
        }

        set ViewItemIndex(value) {
          this.viewItemIndex = value; // if(globalThis.weaweawe && this.viewItemIndex && this.viewItemIndex.x == 1 && this.viewItemIndex.y == 3){
          //     console.log('!!!::',this.symbolValue);
          //     this.boxObj.box.node.scale = v3(2,2,2);
          // }else{
          //     this.boxObj.box.node.scale = v3(1,1,1);
          // }
        }
        /**
         * 获取当前box的可视索引
         */


        get ViewItemIndex() {
          return this.viewItemIndex;
        }

        get BoxObj() {
          return this.boxObj;
        }
        /**
         * 获取当前box的状态
         */


        get BoxStateNow() {
          return this.boxStateNow;
        }
        /**
         *  设置当前box的状态
         */


        set BoxStateNow(value) {
          this.boxStateNow = value;
        }
        /**
         * 是否是需要消除的格子
         */


        get IsDisappearBox() {
          if (this.boxStateNow == 'win' && !this.IsLocked) {
            return true;
          }

          return false;
        }
        /**
         * 是否是需要变换的格子
         */


        get IsConversionBox() {
          const boxData = this.getSymbolData(this.SymbolValue);

          if (boxData) {
            const renderData = boxData[this.boxStateNow];

            if (renderData && renderData.needConvert) {
              return true;
            }
          }

          return false;
        }
        /**
         * 设置即将消除的格子
         */


        get WillDisappearBox() {
          return this.willDisappear;
        }
        /**
         * 是否是即将消除的格子
         */


        set WillDisappearBox(value) {
          this.willDisappear = value;
        }
        /**
         * 锁定格子
         */


        set IsLocked(value) {
          this.locked = value;
        }
        /**
         * 格子是否锁定中
         */


        get IsLocked() {
          return this.locked;
        }
        /**
         * 设置消除后生成的新格子
         */


        set IsDropBox(value) {
          this.isDropBox = value;
        }
        /**
         * 是否是消除后生成的新格子
         */


        get IsDropBox() {
          return this.isDropBox;
        }

        get IsJackpotBox() {
          return this.isJackpotBox;
        }
        /**
         * 格子实体的初始化
         */


        EntityInit(type = null) {
          if (this.boxObj) {
            if (this.boxObj.floatEffect) {
              this.boxObj.floatEffect.node.active = false;
            }
          }

          this.RegisterEnitityAction('jackpotBingo', () => {
            this.JackpotBingo();
          });
        }
        /**
         * 格子实体的销毁回调
         */


        EntityDestroy() {}
        /**
         * 是否是待消除的长格子
         */


        get IsLongWild() {
          if (this.SymbolValue) {
            let symbol = this.SymbolValue.toString();

            if (symbol.length == 4) {
              if (symbol[2] == 0) {
                if (symbol[3] == 2 || symbol[3] == 3 || symbol[3] == 4) {
                  return true;
                }
              }

              return false;
            }
          }

          return false;
        }
        /**
         * 是否是空格子
         */


        get IsEmptyBox() {
          if (this.SymbolValue == 10000) {
            return true;
          }

          return false;
        }
        /**
         * @zh
         * 清除格子的渲染
         * @param boxState - 格子状态
         * @param tableState - 桌子状态
         */


        ClearItem(boxState, tableState) {
          this.boxStateNow = boxState;
          this.isDirty = false;
          this.isJackpotBox = false;
          this.IsLocked = false;
          this.IsDropBox = false;
          this.isCreateBox = false;
          this.willDisappear = false;

          if (this.itemIndex) {
            this.itemIndex.x = 0;
            this.itemIndex.y = 0;
          }

          if (this.viewItemIndex) {
            this.viewItemIndex.x = 0;
            this.viewItemIndex.y = 0;
          }

          this.boxObj.box.clearItem(this.SymbolValue, boxState, tableState);

          if (this.boxObj.floatBox) {
            this.boxObj.floatBox.clearItem(this.SymbolValue, boxState, tableState);
          }

          if (this.jackpotNode && this.jackpotNode.node.active == true) {
            this.jackpotNode.node.active = false;
          }
        }
        /**
         * @zh
         * 关闭格子的所有对象
         * @param boxState - 格子状态
         * @param tableState - 桌子状态
         */


        CloseAllItem(boxState, tableState) {
          if (this.boxObj) {
            var keys = Object.keys(this.boxObj);

            for (let i = 0; i < keys.length; i++) {
              const element = this.boxObj[keys[i]];

              if (element && element.node) {
                element.node.active = false;
              }
            }
          }
        }
        /**
         * @zh
         * 打开格子的所有对象
         * @param boxState - 格子状态
         * @param tableState - 桌子状态
         */


        OpenAllItem(boxState, tableState) {
          if (this.boxObj) {
            var keys = Object.keys(this.boxObj);

            for (let i = 0; i < keys.length; i++) {
              const element = this.boxObj[keys[i]];

              if (element && element.node) {
                element.node.active = true;
              }
            }
          }
        }
        /**
         * @zh
         * 注册以名字为key的行为
         * @param _name - 行为名称
         * @param call - 行为回调
         */


        RegisterEnitityAction(_name, call) {
          if (_name && call) {
            this.actionArr[_name] = call;
          }
        }
        /**
         * @zh
         * 触发以名字为key的行为
         * @param _name - 行为名称
         * @param param - 参数
         */


        DoEnitityAction(_name, param = null) {
          const action = this.actionArr[_name];

          if (action) {
            action(param);
          }
        }
        /**
         * @zh
         * 获取格子的相对坐标
         * @return - position : Vec3
         */


        GetBoxPosition() {
          return this.boxObj.box.node.position;
        }
        /**
         * @zh
         * 获取格子的世界坐标
         * @return - position : Vec3
         */


        GetBoxWorldPosition() {
          return this.boxObj.box.node.worldPosition;
        }

        set IsDirty(value) {
          this.isDirty = value;
        }

        get IsDirty() {
          return this.isDirty;
        }
        /**
         * @zh
         * 更新指定格子的状态
         * @param boxState - 格子状态
         * @param tableState - 桌子状态
         */


        UpdateData(boxState, tableState) {
          // this.symbolValue = this.getSymbolValue();
          // console.log('this.symbolValue:'+ this.symbolValue);
          // if(this.symbolValue == 1){
          //     console.log('111.zip:'+ boxState);
          // }
          this.boxStateNow = boxState;
          this.boxData = this.getSymbolData(this.SymbolValue);

          if (this.boxData) {
            const renderData = this.boxData[boxState];

            if (renderData) {
              if (renderData.floating) {
                // this.boxObj.floatBox.node.position = this.boxPos;
                if (this.boxObj.floatBox) {
                  this.boxObj.floatBox.node.active = true;
                  this.boxObj.floatBox.boxItemUpdate(renderData, this.SymbolValue, boxState, tableState);
                }

                if (this.boxObj.box.node.active) {
                  this.boxObj.box.clearItem(this.SymbolValue, boxState, tableState);
                  this.boxObj.box.node.active = false;
                  this.boxObj.box.clearItem(this.SymbolValue, boxState, tableState);
                  this.boxObj.box.node.active = false;
                }
              } else {
                if (this.boxObj.floatBox) {
                  if (this.boxObj.floatBox.node.active) {
                    this.boxObj.floatBox.clearItem(this.SymbolValue, boxState, tableState);
                    this.boxObj.floatBox.node.active = false;
                  }
                }

                this.boxObj.box.node.active = true;
                this.boxObj.box.boxItemUpdate(renderData, this.SymbolValue, boxState, tableState);
              }
            }
          } else {// console.log('SymbolData is error'+this.symbolValue);
          }
        }
        /**
         * @zh
         * 清除格子的特效动画
         * @param boxState - 格子状态
         * @param tableState - 桌子状态
         */


        ClearEffect(boxState, tableState) {
          if (this.boxObj.floatEffect) {
            if (this.boxObj.floatEffect.node.active) {
              this.boxObj.floatEffect.clearItem(this.SymbolValue, boxState, tableState);
              this.boxObj.floatEffect.node.active = false;
            }
          }
        }

        SetAssetToObj(objName, objNode) {
          if (objName && objNode) {
            // this.boxObj.box = objNode.getComponent(sBoxItemBase)
            this.boxObj[objName] = objNode.getComponent(_crd && sBoxItemBase === void 0 ? (_reportPossibleCrUseOfsBoxItemBase({
              error: Error()
            }), sBoxItemBase) : sBoxItemBase);
          } else {
            console.log('setAssetToObj error');
          }
        }

        UpdateBoxPosition(pos) {
          this.boxPos = pos;
        }

        SetBoxActive(value) {
          this.boxObj.box.node.active = value;
        }

        SetBoxSiblingIndex(value) {
          this.boxObj.box.node.setSiblingIndex(value);
        }

        ChangeEffectAction(boxState, tableState) {}

        getSymbolData(symbolValue) {
          try {
            return (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
              error: Error()
            }), sConfigMgr) : sConfigMgr).instance.GetSymbolBoxImg(symbolValue);
          } catch (e) {
            console.error('getSymbolData error' + e);
          }
        }

        getSymbolValue() {
          try {
            const data = (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
              error: Error()
            }), sConfigMgr) : sConfigMgr).instance.GetSymbolBoxData(this.ItemIndex.x, this.ItemIndex.y);

            if (data && data.symbol) {
              let symbol = data.symbol;

              if (data.type == 'jackpot') {
                this.isJackpotBox = true;

                if (data.replace > 0) {
                  symbol = data.replace;
                } else {
                  this.CreateJackposNode();
                }
              } // console.log('symbolValue:',symbol);


              return symbol;
            } else {
              if (this.jackpotNode && this.jackpotNode.node.active == true) {
                this.jackpotNode.node.active = false;
              } // console.log('symbolValue data :',data);


              return data;
            }
          } catch (e) {
            console.error('getSymbolValue error' + e);
          }
        }

        isSymbolBoxData() {
          try {
            return (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
              error: Error()
            }), sConfigMgr) : sConfigMgr).instance.IsSymbolBoxData(this.ItemIndex.x, this.ItemIndex.y);
          } catch (e) {
            console.error('isSymbolBoxData error' + e);
            return false;
          }
        }

        getViewBoxItemBase() {
          return null;
        } // public DropEntityBoxToPosAnima(pos : Vec3,animaTime : number,delay : number,finishCallBack : Function|null = null,strikeCallBack : Function|null = null){
        //     const box = this.getViewBoxItemBase();
        //     if(box && box.node && box.node.active){
        //         tween(box.node).delay(delay).to(animaTime,{position : pos},{easing : 'cubicIn'}).call(()=>{
        //             if(strikeCallBack){
        //                 strikeCallBack();
        //             }
        //         }).by(0.2,{position : v3(0,20,0)},{easing:'cubicOut'}).by(0.2,{position : v3(0,-20,0)},{easing:'cubicIn'}).call(()=>{
        //             this.UpdateBoxPosition(pos);
        //             if(finishCallBack){
        //                 finishCallBack();
        //             }
        //         }).start();
        //     }
        // }


        DropEntityBoxToPosAnima(pos, animaTime, delay, finishCallBack = null, strikeCallBack = null) {
          const boxPos = v3(this.boxPos);
          tween(boxPos).delay(delay).to(animaTime, pos, {
            easing: 'cubicIn',
            onUpdate: (target, ratio) => {
              this.UpdateBoxPosition(boxPos);
            }
          }).call(() => {
            if (strikeCallBack) {
              strikeCallBack();
            }
          }).by(0.2, v3(0, 7, 0), {
            easing: 'cubicOut',
            onUpdate: (target, ratio) => {
              this.UpdateBoxPosition(boxPos);
            }
          }).by(0.2, v3(0, -7, 0), {
            easing: 'cubicIn',
            onUpdate: (target, ratio) => {
              this.UpdateBoxPosition(boxPos);
            }
          }).call(() => {
            this.UpdateBoxPosition(v3(pos));

            if (finishCallBack) {
              finishCallBack();
            }
          }).start();
        } // public DropHorEntityBoxToPosAnima(pos : Vec3,animaTime : number,delay : number,finishCallBack : Function|null = null){
        //     const box = this.getViewBoxItemBase();
        //     if(box && box.node && box.node.active){
        //         tween(box.node).delay(delay).to(animaTime,{position : pos},{easing : 'cubicIn'}).call(()=>{
        //             director.emit('boxDropStrikeMsg');
        //         }).by(0.2,{position : v3(20,0,0)},{easing:'cubicOut'}).by(0.2,{position : v3(-20,0,0)},{easing:'cubicIn'}).call(()=>{
        //             this.UpdateBoxPosition(pos);
        //             if(finishCallBack){
        //                 finishCallBack();
        //             }
        //         }).start();
        //     }
        // }


        DropHorEntityBoxToPosAnima(pos, animaTime, delay, finishCallBack = null, strikeCallBack = null) {
          const boxPos = v3(this.boxPos);
          tween(boxPos).delay(delay).to(animaTime, pos, {
            easing: 'cubicIn',
            onUpdate: (target, ratio) => {
              this.UpdateBoxPosition(boxPos);
            }
          }).call(() => {
            if (strikeCallBack) {
              strikeCallBack();
            }
          }).by(0.2, v3(20, 0, 0), {
            easing: 'cubicOut',
            onUpdate: (target, ratio) => {
              this.UpdateBoxPosition(boxPos);
            }
          }).by(0.2, v3(-20, 0, 0), {
            easing: 'cubicIn',
            onUpdate: (target, ratio) => {
              this.UpdateBoxPosition(boxPos);
            }
          }).call(() => {
            this.UpdateBoxPosition(v3(pos));

            if (finishCallBack) {
              finishCallBack();
            }
          }).start();
        }

        CreateJackposNode() {
          // console.log('CreateJackposNode');
          if (this.jackpotNode) {
            this.jackpotNode.node.active = true;
          } else {
            globalThis.soltCreateJackpotNode(obj => {
              if (obj) {
                this.jackpotNode = obj.getComponent(_crd && sBoxItemBase === void 0 ? (_reportPossibleCrUseOfsBoxItemBase({
                  error: Error()
                }), sBoxItemBase) : sBoxItemBase);

                if (this.jackpotNode) {
                  this.jackpotNode.node.parent = this.boxObj.box.node;
                  const size = (_crd && sSlotSceneInfo === void 0 ? (_reportPossibleCrUseOfsSlotSceneInfo({
                    error: Error()
                  }), sSlotSceneInfo) : sSlotSceneInfo).BoxSize;
                  this.jackpotNode.node.position = v3(size.x / 2, size.y / 2, 0); // const scaleValue = size.x / 200;

                  this.jackpotNode.node.scale = Vec3.ONE;
                  this.jackpotNode.boxItemUpdate(null, null, 'rest', null); // this.jackpotNode['setTargetNode'](this.boxObj.box.node);
                }
              }
            });
          }
        }

        JackpotBingo() {
          if (this.jackpotNode) {
            this.jackpotNode.boxItemUpdate(null, null, 'win', null);
          }
        }

      }, _temp)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=sBoxEntity.js.map
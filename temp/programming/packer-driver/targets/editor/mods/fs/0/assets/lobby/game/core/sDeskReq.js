System.register(["cc"], function (_export, _context) {
  "use strict";

  var _cclegacy, director, sDeskReq, _crd;

  _export("sDeskReq", void 0);

  return {
    setters: [function (_cc) {
      _cclegacy = _cc.cclegacy;
      director = _cc.director;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "8de7a/cUcNIi7uQXUiyPtk3", "sDeskReq", undefined);

      _export("sDeskReq", sDeskReq = class sDeskReq {
        static enterTable(deskid, _transfer_safebox, callback) {
          globalThis.subGameSocketReq('desk.mainHandler.enterDeskByDeskId', {
            desk_id: deskid,
            transfer_safebox: _transfer_safebox
          }, (err, res) => {
            if (!err && res && res.data) {
              if (callback) {
                callback(res);
              }
            } else {
              director.emit('pokerMsgErr', res);
            }
          });
        }

        static enterGroupTable(groupid, _transfer_safebox, callback) {
          globalThis.subGameSocketReq('desk.mainHandler.enterDeskByDeskGroup', {
            desk_group: groupid,
            transfer_safebox: _transfer_safebox
          }, (err, res) => {
            if (callback) {
              callback(err, res);
            }

            if (err && err.code != 200) {
              const tips = globalThis.GetDataByKey && globalThis.GetDataByKey("mulTiLN", "common", err.code);

              if (tips) {
                director.emit('uiTipsOpen', 'text', tips + ' [' + err.code + ']');
              }
            }
          });
        }

        static leaveTable(deskid, callback) {
          globalThis.subGameSocketReq('desk.mainHandler.leaveDesk', {}, (err, res) => {
            if (callback) {
              callback(err, res);
            }
          });
        }

      });

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=sDeskReq.js.map
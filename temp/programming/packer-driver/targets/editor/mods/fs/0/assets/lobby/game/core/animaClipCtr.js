System.register(["__unresolved_0", "cc", "__unresolved_1"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, Animation, director, ParticleSystem, sComponent, _dec, _class, _crd, ccclass, property, animaClipCtr;

  function _reportPossibleCrUseOfsComponent(extras) {
    _reporterNs.report("sComponent", "./sComponent", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      Animation = _cc.Animation;
      director = _cc.director;
      ParticleSystem = _cc.ParticleSystem;
    }, function (_unresolved_2) {
      sComponent = _unresolved_2.sComponent;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "0bcef2H3+tEkY6IxCUVc40X", "animaClipCtr", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("animaClipCtr", animaClipCtr = (_dec = ccclass('animaClipCtr'), _dec(_class = class animaClipCtr extends (_crd && sComponent === void 0 ? (_reportPossibleCrUseOfsComponent({
        error: Error()
      }), sComponent) : sComponent) {
        start() {}

        animaPlayStart(path) {
          if (path) {
            const obj = this.node.getChildByPath(path);

            if (obj) {
              const anima = obj.getComponent(Animation);

              if (anima) {
                anima.enabled = true;
              } else {
                console.log('path no anima : ' + path);
              }
            } else {
              console.log('path error : ' + path);
            }
          } else {
            console.log('path must input');
          }
        }

        animaPlayOver() {
          director.emit('animaPlayOver', this.node);
        }

        particlePlay(path) {
          if (path) {
            const obj = this.node.getChildByPath(path);

            if (obj) {
              const par = obj.getComponent(ParticleSystem);

              if (par) {
                par.stop();
                par.play();
              } else {
                console.log('path no par : ' + path);
              }
            } else {
              console.log('path error : ' + path);
            }
          } else {
            console.log('path must input');
          }
        }

        particleStop(path) {
          if (path) {
            const obj = this.node.getChildByPath(path);

            if (obj) {
              const par = obj.getComponent(ParticleSystem);

              if (par) {
                par.stop();
              } else {
                console.log('path no par : ' + path);
              }
            } else {
              console.log('path error : ' + path);
            }
          } else {
            console.log('path must input');
          }
        }

        activeCtrTrue(path) {
          if (path) {
            const obj = this.node.getChildByPath(path);

            if (obj) {
              obj.active = true;
            } else {
              console.log('path error : ' + path);
            }
          } else {
            console.log('path must input');
          }
        }

        activeCtrFalse(path) {
          if (path) {
            const obj = this.node.getChildByPath(path);

            if (obj) {
              obj.active = false;
            } else {
              console.log('path error : ' + path);
            }
          } else {
            console.log('path must input');
          }
        }

        animationMsg(msg) {
          if (msg) {
            director.emit(msg);
          }
        }

      }) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=animaClipCtr.js.map
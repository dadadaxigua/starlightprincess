System.register(["__unresolved_0", "cc", "__unresolved_1"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, director, Label, sSoltFreeWinBuyCtr, _dec, _dec2, _class, _class2, _descriptor, _temp, _crd, ccclass, property, StarlightPrincess_FreeWinBuyCtr;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfsSoltFreeWinBuyCtr(extras) {
    _reporterNs.report("sSoltFreeWinBuyCtr", "../lobby/game/core/sSoltFreeWinBuyCtr", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      director = _cc.director;
      Label = _cc.Label;
    }, function (_unresolved_2) {
      sSoltFreeWinBuyCtr = _unresolved_2.sSoltFreeWinBuyCtr;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "80112nCce5DEa5GO25ihG9M", "StarlightPrincess_FreeWinBuyCtr", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("StarlightPrincess_FreeWinBuyCtr", StarlightPrincess_FreeWinBuyCtr = (_dec = ccclass('StarlightPrincess_FreeWinBuyCtr'), _dec2 = property(Label), _dec(_class = (_class2 = (_temp = class StarlightPrincess_FreeWinBuyCtr extends (_crd && sSoltFreeWinBuyCtr === void 0 ? (_reportPossibleCrUseOfsSoltFreeWinBuyCtr({
        error: Error()
      }), sSoltFreeWinBuyCtr) : sSoltFreeWinBuyCtr) {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "coinLabel", _descriptor, this);

          _defineProperty(this, "touchEnabel", true);
        }

        onLoad() {
          director.on('sSlotBetInfoUpdate', info => {
            console.log('sSlotBetInfoUpdate1', info);

            if (info) {
              if (info.betAmount) {
                this.coinLabel.string = this.AddCommas(info.betAmount * 50 * 20);
              }

              ;
            }

            ;
          }, this);
        }

        start() {
          super.start();
          this.freeWinButton.node.on('click', () => {
            //director.emit('slotJackPotBingo',1004223);
            if (this.touchEnabel) {
              this.touchEnabel = false;
              this.cleanTweenList(); // this.node.scale = Vec3.ONE;
              // this.pushOneTween(tween(this.node).to(0.15,{scale : Vec3.ZERO}).start());
            }
          }, this);
          director.on('subGameBetActionBtnCancel', () => {
            this.touchEnabel = true;
            this.cleanTweenList(); // this.pushOneTween(tween(this.node).to(0.15,{scale : Vec3.ONE}).start());
          }, this);
          director.on('subGameBetActionBtnClick', () => {
            this.touchEnabel = true;
            this.cleanTweenList(); // this.pushOneTween(tween(this.node).to(0.15,{scale : Vec3.ONE}).start());
          }, this);
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "coinLabel", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=StarlightPrincess_FreeWinBuyCtr.js.map
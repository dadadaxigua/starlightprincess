System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2", "__unresolved_3", "__unresolved_4", "__unresolved_5", "__unresolved_6", "__unresolved_7", "__unresolved_8", "__unresolved_9"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, Node, v2, instantiate, v3, Vec3, Tween, tween, director, UIOpacity, sys, UITransform, Button, sObjPool, sConfigMgr, sUtil, sBoxItemBase, sAudioMgr, assetMgr, sComponent, sMotionMgr, sSlotSceneInfo, _dec, _dec2, _class, _class2, _descriptor, _descriptor2, _temp, _dec3, _dec4, _dec5, _dec6, _dec7, _dec8, _dec9, _dec10, _dec11, _dec12, _dec13, _class4, _class5, _descriptor3, _descriptor4, _descriptor5, _descriptor6, _descriptor7, _descriptor8, _descriptor9, _descriptor10, _descriptor11, _descriptor12, _class6, _temp2, _crd, ccclass, property, sBoxEntityAsset, sBoxMgr;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfsObjPool(extras) {
    _reporterNs.report("sObjPool", "./sObjPool", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsConfigMgr(extras) {
    _reporterNs.report("sConfigMgr", "./sConfigMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsUtil(extras) {
    _reporterNs.report("sUtil", "./sUtil", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsBoxEntity(extras) {
    _reporterNs.report("sBoxEntity", "./sBoxEntity", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsBoxItemBase(extras) {
    _reporterNs.report("sBoxItemBase", "./sBoxItemBase", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsAudioMgr(extras) {
    _reporterNs.report("sAudioMgr", "./sAudioMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfassetMgr(extras) {
    _reporterNs.report("assetMgr", "./sAssetMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsComponent(extras) {
    _reporterNs.report("sComponent", "./sComponent", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsMotionMgr(extras) {
    _reporterNs.report("sMotionMgr", "./sMotionMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsMotionTween(extras) {
    _reporterNs.report("sMotionTween", "./sMotionTween", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsSlotSceneInfo(extras) {
    _reporterNs.report("sSlotSceneInfo", "./sSlotSceneInfo", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      Node = _cc.Node;
      v2 = _cc.v2;
      instantiate = _cc.instantiate;
      v3 = _cc.v3;
      Vec3 = _cc.Vec3;
      Tween = _cc.Tween;
      tween = _cc.tween;
      director = _cc.director;
      UIOpacity = _cc.UIOpacity;
      sys = _cc.sys;
      UITransform = _cc.UITransform;
      Button = _cc.Button;
    }, function (_unresolved_2) {
      sObjPool = _unresolved_2.sObjPool;
    }, function (_unresolved_3) {
      sConfigMgr = _unresolved_3.sConfigMgr;
    }, function (_unresolved_4) {
      sUtil = _unresolved_4.sUtil;
    }, function (_unresolved_5) {
      sBoxItemBase = _unresolved_5.sBoxItemBase;
    }, function (_unresolved_6) {
      sAudioMgr = _unresolved_6.default;
    }, function (_unresolved_7) {
      assetMgr = _unresolved_7.assetMgr;
    }, function (_unresolved_8) {
      sComponent = _unresolved_8.sComponent;
    }, function (_unresolved_9) {
      sMotionMgr = _unresolved_9.sMotionMgr;
    }, function (_unresolved_10) {
      sSlotSceneInfo = _unresolved_10.sSlotSceneInfo;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "fa975gNNgJF77w4qQr8ro3a", "sBoxMgr", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("sBoxEntityAsset", sBoxEntityAsset = (_dec = ccclass('sBoxEntityAsset'), _dec2 = property(Node), _dec(_class = (_class2 = (_temp = class sBoxEntityAsset {
        constructor() {
          _initializerDefineProperty(this, "targetBoxName", _descriptor, this);

          _initializerDefineProperty(this, "targetBoxNode", _descriptor2, this);
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "targetBoxName", [property], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return '';
        }
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "targetBoxNode", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class));

      _export("sBoxMgr", sBoxMgr = (_dec3 = ccclass('sBoxMgr'), _dec4 = property({
        group: {
          name: 'BarderBoxConfig'
        },
        tooltip: '格子集合的真实长宽数量'
      }), _dec5 = property({
        group: {
          name: 'BarderBoxConfig'
        },
        tooltip: '格子集合的视窗长宽数量'
      }), _dec6 = property({
        group: {
          name: 'BarderBoxConfig'
        },
        tooltip: '单个格子的长宽尺寸'
      }), _dec7 = property({
        group: {
          name: 'BarderBoxConfig'
        },
        tooltip: '每一列的偏移量',
        type: [Vec3]
      }), _dec8 = property(sBoxEntityAsset), _dec9 = property({
        tooltip: '普通滚动的速度,越大越快',
        min: 1
      }), _dec10 = property({
        tooltip: '加速滚动的速度,越大越快',
        min: 1
      }), _dec11 = property({
        tooltip: '转到speedAnima列等待停下的时间',
        min: 0.5
      }), _dec12 = property({
        tooltip: '自动保存最后一次抽奖结果并在每次登陆展示'
      }), _dec13 = property({
        tooltip: '是否打开自动发送格子信息提示位置'
      }), _dec3(_class4 = (_class5 = (_temp2 = _class6 = class sBoxMgr extends (_crd && sComponent === void 0 ? (_reportPossibleCrUseOfsComponent({
        error: Error()
      }), sComponent) : sComponent) {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "boxSum", _descriptor3, this);

          _initializerDefineProperty(this, "boxViewSum", _descriptor4, this);

          _initializerDefineProperty(this, "boxSize", _descriptor5, this);

          _initializerDefineProperty(this, "skewVec3Arr", _descriptor6, this);

          _defineProperty(this, "disappearAreaSize", void 0);

          _initializerDefineProperty(this, "boxEntitityAsset", _descriptor7, this);

          _initializerDefineProperty(this, "normalSpeed", _descriptor8, this);

          _initializerDefineProperty(this, "turboSpeed", _descriptor9, this);

          _initializerDefineProperty(this, "speedAnimaDelayTime", _descriptor10, this);

          _initializerDefineProperty(this, "autoSaveRollResult", _descriptor11, this);

          _initializerDefineProperty(this, "canSendBoxTipPos", _descriptor12, this);

          _defineProperty(this, "boxArray", []);

          _defineProperty(this, "boxTargetPosX", []);

          _defineProperty(this, "tweenRollColumnArray", {});

          _defineProperty(this, "boxRollState", {});

          _defineProperty(this, "disappearBottomNum", 0);

          _defineProperty(this, "startScheduleAnima", null);

          _defineProperty(this, "blackNode", void 0);

          _defineProperty(this, "blackNodeSibling", 3);

          _defineProperty(this, "rollState", 'idle');

          _defineProperty(this, "betRollType", 'normal');

          _defineProperty(this, "rollSpeedMode", 'normal');

          _defineProperty(this, "rollFixedSpeedMode", 'none');

          _defineProperty(this, "rollCount", 0);

          _defineProperty(this, "rollMotionType", 'stop');

          _defineProperty(this, "rollQuickMode", 'normal');

          _defineProperty(this, "wholeFlowCallList", []);

          _defineProperty(this, "colBoxState", []);

          _defineProperty(this, "rollMotions", new Map());
        }

        get BetRollType() {
          return this.betRollType;
        }

        //none,normal
        get RollSpeedMode() {
          if (this.rollFixedSpeedMode == 'none') {
            return this.rollSpeedMode;
          } else if (this.rollFixedSpeedMode == 'normal') {
            return this.rollFixedSpeedMode;
          } else {
            return this.rollSpeedMode;
          }
        }

        onLoad() {
          sBoxMgr.instance = this;
          (_crd && sSlotSceneInfo === void 0 ? (_reportPossibleCrUseOfsSlotSceneInfo({
            error: Error()
          }), sSlotSceneInfo) : sSlotSceneInfo).SetBoxSize(this.boxSize.x, this.boxSize.y);
          (_crd && sSlotSceneInfo === void 0 ? (_reportPossibleCrUseOfsSlotSceneInfo({
            error: Error()
          }), sSlotSceneInfo) : sSlotSceneInfo).SetBoxSum(this.boxSum.x, this.boxSum.y);
          (_crd && sSlotSceneInfo === void 0 ? (_reportPossibleCrUseOfsSlotSceneInfo({
            error: Error()
          }), sSlotSceneInfo) : sSlotSceneInfo).SetBoxViewSum(this.boxViewSum.x, this.boxViewSum.y);
          this.setColBoxState(-1, 'idle');
          this.blackNode = this.node.getChildByName('blackMask');

          if (this.blackNode) {
            this.blackNodeSibling = this.blackNode.getSiblingIndex();
          }

          this.disappearAreaSize = v2(0, -((this.boxSum.y - this.boxViewSum.y) / 2 + 1) * this.boxSize.y);
          this.disappearBottomNum = Math.floor(Math.abs(this.disappearAreaSize.y) / this.boxSize.y); // console.log('this.disappearAreaSize:',this.disappearAreaSize);
          // console.log('this.disappearBottomNum:',this.disappearBottomNum);

          for (let i = 0; i < this.boxSum.x; i++) {
            this.boxRollState[i] = 0;
          }

          director.on('configLoadSuc', () => {
            if (this.enabled) {
              console.log('box configLoadSuc');
              const boxSize = this.boxSize;
              const startY = (this.boxSum.y - this.boxViewSum.y) / 2;
              const boxArrayLength = (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
                error: Error()
              }), sConfigMgr) : sConfigMgr).instance.BoxArrayLength;
              let boxRoundEndData = null;
              let boxRoundEndDataStr = null;

              if (this.autoSaveRollResult) {
                boxRoundEndDataStr = sys.localStorage.getItem('subGameBoxRoundEndData_' + globalThis.currentPlayingGameID);
              }

              if (boxRoundEndDataStr) {
                boxRoundEndData = JSON.parse(boxRoundEndDataStr); // console.log(boxRoundEndData);

                for (let x = 0; x < this.boxSum.x; x++) {
                  this.boxTargetPosX[x] = v3(x * this.boxSize.x, -(boxArrayLength - this.boxSum.y % boxArrayLength) * boxSize.y, 0);
                }
              } else {
                for (let x = 0; x < this.boxSum.x; x++) {
                  this.boxTargetPosX[x] = v3(x * this.boxSize.x, 0, 0);
                }
              }

              for (let x = 0; x < this.boxSum.x; x++) {
                let skewDeltaY = this.getSkewPos(x);

                for (let y = 0; y < this.boxSum.y; y++) {
                  const pos = v3(x * boxSize.x, y * boxSize.y - startY * boxSize.y + skewDeltaY, 0);
                  let boxEntity = this.CreateBoxEntity(pos);
                  boxEntity.ItemIndex = v2(x, y);

                  if (boxRoundEndData) {
                    let boxData = boxRoundEndData.data[x][y];

                    if (boxData) {
                      boxEntity.SymbolValue = boxData;
                    }
                  }

                  boxEntity.ViewItemIndex = v2(x, y - startY);
                  boxEntity.EntityInit('ver');
                  boxEntity.UpdateData('idle', 'idle');
                  this.setBoxArray(x, y, boxEntity);
                }

                this.boxTargetPosX[x].add3f(0, -startY * boxSize.y + skewDeltaY, 0); // if(boxRoundEndData){
                //     this.boxTargetPosX[x].add3f(0,(this.boxSum.y % boxArrayLength)*boxSize.y,0);
                // }
              }

              (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
                error: Error()
              }), sConfigMgr) : sConfigMgr).instance.setBoxTrigger(this.boxSum.x);
            }
          }, this);
          director.on('setRollFixedSpeedMode', value => {
            if (value) {
              this.rollFixedSpeedMode = 'normal';
            } else {
              this.rollFixedSpeedMode = 'none';
            }
          }, this);
          director.on('soltRollBeagin', (type = 'normal') => {
            this.rollBegain(type);
          }, this);
          director.on('rollStop', () => {
            this.saveBoxData();
          }, this);
          director.on('backToHall', () => {
            this.quit();
          }, this);
          director.on('subGameSlotRollQuickStop', () => {
            console.log('subGameSlotRollQuickStop');
            this.quickClick();
          }, this); // director.on('subGameSlotBetActionBtnClick',()=>{
          //     if(this.canQuickStop){
          //         this.allStopTime = 0.1;
          //     }
          // },this);
        }

        start() {
          if (this.canSendBoxTipPos) {
            this.scheduleOnce(() => {
              this.createBoxTipButton();
            }, 1);
          }
        }

        setBoxRollState(index, state) {
          this.boxRollState[index] = state;
        }

        isAllBoxRollStop() {
          if (this.boxRollState) {
            for (let i = 0; i < this.boxSum.x; i++) {
              if (this.boxRollState[i] == 1) {
                return false;
              }
            }
          }

          return true;
        }

        rollBoxToPosArrays(index, posY, rollSpeed, call) {
          let pos = this.boxTargetPosX[index];
          let valueY = pos.y;
          let targetPos = v3(0, 0, 0);
          const motion = (_crd && sMotionMgr === void 0 ? (_reportPossibleCrUseOfsMotionMgr({
            error: Error()
          }), sMotionMgr) : sMotionMgr).Tween('roll_' + index, v3(pos.x, posY, 0), pos, rollSpeed, target => {
            const deltaY = target.y - valueY;
            valueY = target.y;

            for (let y = 0; y < this.boxArray[index].length; y++) {
              let obj = this.boxArray[index][y];
              const pos = obj.GetBoxPosition();
              targetPos.x = pos.x;
              targetPos.y = pos.y + deltaY;
              targetPos.z = pos.z;
              obj.UpdateBoxPosition(v3(targetPos));
            }

            this.boxUpdateByIndex(index);
          }, call);
          return motion;
        }

        rollBoxArray(index, posY, rollSpeed, call) {
          let pos = this.boxTargetPosX[index];
          let valueY = pos.y;
          let tweenObj = {};
          let targetPos = v3(0, 0, 0);
          this.tweenRollColumnArray[index] = tweenObj;
          return (_crd && sMotionMgr === void 0 ? (_reportPossibleCrUseOfsMotionMgr({
            error: Error()
          }), sMotionMgr) : sMotionMgr).Tween('roll_' + index, v3(pos.x, pos.y + posY, 0), pos, rollSpeed, target => {
            const deltaY = target.y - valueY;
            valueY = target.y;

            for (let y = 0; y < this.boxArray[index].length; y++) {
              let obj = this.boxArray[index][y];
              const pos = obj.GetBoxPosition();
              targetPos.x = pos.x;
              targetPos.y = pos.y + deltaY;
              targetPos.z = pos.z;
              obj.UpdateBoxPosition(v3(targetPos));
            }

            this.boxUpdateByIndex(index);
          }, call);
        }

        rollBoxArrayForever(index, posY, rollSpeed) {
          let pos = this.boxTargetPosX[index];
          let valueY = pos.y;
          let tweenObj = {};
          let targetPos = v3(0, 0, 0);
          this.tweenRollColumnArray[index] = tweenObj;
          const motion = (_crd && sMotionMgr === void 0 ? (_reportPossibleCrUseOfsMotionMgr({
            error: Error()
          }), sMotionMgr) : sMotionMgr).Tween('roll_' + index, v3(pos.x, pos.y + posY, 0), pos, rollSpeed, target => {
            const deltaY = target.y - valueY; // pos = target;

            valueY = target.y;

            for (let y = 0; y < this.boxArray[index].length; y++) {
              let obj = this.boxArray[index][y];
              const bPos = obj.GetBoxPosition();
              targetPos.x = bPos.x;
              targetPos.y = bPos.y + deltaY;
              targetPos.z = bPos.z;
              obj.UpdateBoxPosition(v3(targetPos));
            }

            this.boxUpdateByIndex(index);
          });
          motion.loop = -1;
          tweenObj['tween'] = motion;
          return motion;
        }

        rrrolll() {
          // this.unscheduleAllCallbacks();
          this.rollMotions.clear();
          this.removeAllSchedule();

          for (let i = 0; i < this.boxSum.x; i++) {
            this.boxRollState[i] = 1;
          }

          if (this.rollCount > 0) {
            for (let i = 0; i < this.boxTargetPosX.length; i++) {
              let boxPos = this.boxTargetPosX[i]; // console.log('boxPosY : ', boxPos.y);

              boxPos.y += this.boxSum.y * this.boxSize.y;
            }
          }

          for (let i = 0; i < this.boxArray.length; i++) {
            for (let y = 0; y < this.boxArray[i].length; y++) {
              const element = this.boxArray[i][y];
              element.IsDirty = true;
            }
          }

          this.rollQuickMode = 'normal';
          this.setColBoxState(-1, 'rolling');
          let iii = 0;

          this.startScheduleAnima = () => {
            if (iii == 0) {// this.setBoxArrayState('roll','rolling');
            } else if (iii == this.boxSum.x - 1) {}

            const index = iii++;
            const rollBoxArrayMotion = this.rollBoxArray(index, this.RollSpeedMode == 'turbo' ? 20 : 40, this.RollSpeedMode == 'turbo' ? 200 : 1000, () => {
              const rollBoxArrayForeverMotion = this.rollBoxArrayForever(index, -42040, this.RollSpeedMode == 'turbo' ? this.turboSpeed : this.normalSpeed);

              if (rollBoxArrayForeverMotion) {
                if (this.RollSpeedMode == 'turbo') {
                  rollBoxArrayForeverMotion.RollSpeed = 0;
                  rollBoxArrayForeverMotion.speedIncrement = 30;
                  rollBoxArrayForeverMotion.rollSpeedMax = 2;
                }
              }

              if (index == this.boxSum.x - 1) {
                this.rollMotionType = 'go';
                this.wholeFlowDelayCallTrigger(); // director.emit('');

                if (this.betRollType == 'rightNow') {}
              }
            });

            if (rollBoxArrayMotion) {
              rollBoxArrayMotion.speedIncrement = -0.2;
              rollBoxArrayMotion.rollSpeedMin = 0.2;
            }
          };

          if (this.RollSpeedMode == 'turbo') {
            this.rollMotionType = 'float';

            for (let x = 0; x < this.boxSum.x; x++) {
              this.startScheduleAnima();
            }
          } else {
            this.rollMotionType = 'float';
            this.pushIDSchedule(this.startScheduleAnima, 0, 'startScheduleAnima', this.boxViewSum.x, 0.06, 0, true);
          }

          this.rollCount++;
        }

        rollBoxArrays(index, posY, rollTime, ease = 'linear') {
          let pos = this.boxTargetPosX[index];
          let valueY = pos.y;
          let targetPos = v3(0, 0, 0);
          tween(pos).by(rollTime, v3(0, posY, 0), {
            'onUpdate': (target, ratio) => {
              const deltaY = target.y - valueY;
              valueY = target.y;

              for (let y = 0; y < this.boxArray[index].length; y++) {
                let obj = this.boxArray[index][y];
                let pos = obj.GetBoxPosition();
                targetPos.x = pos.x;
                targetPos.y = pos.y + deltaY;
                targetPos.z = pos.z;
                obj.UpdateBoxPosition(v3(targetPos)); // obj.node.position = v3(obj.node.position.x,obj.node.position.y + deltaY,obj.node.position.z);
              }
            },
            easing: ease
          }).start();
        }

        setBoxArray(x, y, item) {
          if (!this.boxArray[x]) {
            this.boxArray[x] = [];
          }

          this.boxArray[x][y] = item;
        }

        boxSetTargetPosUpdate() {
          for (let x = 0; x < this.boxArray.length; x++) {
            let theY = this.boxTargetPosX[x];
            const skewY = this.getSkewPos(x);
            const yadd = Math.floor(Math.abs(theY.y - skewY) / this.boxSize.y);
            const mor = theY.y + yadd * this.boxSize.y;

            for (let y = 0; y < this.boxArray[x].length; y++) {
              let obj = this.boxArray[x][y];

              if (obj) {
                obj.SetBoxActive(true);
                const indexColumn = yadd + y - this.disappearBottomNum + 1;
                obj.ItemIndex = v2(x, indexColumn);
                obj.ViewItemIndex = v2(x, y - (this.boxSum.y - this.boxViewSum.y) / 2);
                obj.UpdateBoxPosition(v3(theY.x, (y - this.disappearBottomNum + 1) * this.boxSize.y + mor, 0));
                obj.SetBoxSiblingIndex(0);
                obj.ClearItem('rolling', 'rolling');
                obj.UpdateData('idle', 'settlement');
              }
            }
          }
        }

        boxUpdateByIndex(index) {
          const x = index;
          let viewArray = [];
          let unViewArray = [];

          for (let y = 0; y < this.boxArray[x].length; y++) {
            const element = this.boxArray[x][y];

            if (element.GetBoxPosition().y < this.disappearAreaSize.y + this.getSkewPos(x)) {
              unViewArray.push(element);
            } else {
              viewArray.push(element);
            }
          }

          for (let i = 0; i < unViewArray.length; i++) {
            const element = unViewArray[i];
            (_crd && sObjPool === void 0 ? (_reportPossibleCrUseOfsObjPool({
              error: Error()
            }), sObjPool) : sObjPool).Enqueue('boxItem', element);
            element.ClearItem('rolling', 'rolling');
          }

          const viewYSum = viewArray.length;

          if (viewYSum < this.boxSum.y) {
            for (let vY = 0; vY < this.boxSum.y - viewYSum; vY++) {
              let obj = (_crd && sObjPool === void 0 ? (_reportPossibleCrUseOfsObjPool({
                error: Error()
              }), sObjPool) : sObjPool).Dequeue('boxItem');

              if (obj) {
                obj.SetBoxActive(true);
                viewArray.push(obj);
              }
            }
          }

          let theY = this.boxTargetPosX[x];
          const skewY = this.getSkewPos(x);
          const yadd = Math.trunc(Math.abs(theY.y - skewY) / this.boxSize.y);
          const mor = theY.y + yadd * this.boxSize.y;

          for (let i = 0; i < viewArray.length; i++) {
            const box = viewArray[i];
            const indexColumn = yadd + i - this.disappearBottomNum + 1;

            if (!box.IsDirty && indexColumn != box.ItemIndex.y) {
              box.ClearItem('rolling', 'rolling');
              box.ItemIndex = v2(x, indexColumn < 0 ? 0 : indexColumn); // box.UpdateBoxPosition(v3(theY.x,theY.y + i * this.boxSize.y,0));

              box.UpdateBoxPosition(v3(theY.x, (i - this.disappearBottomNum + 1) * this.boxSize.y + mor, 0));
              box.SetBoxSiblingIndex(0);

              if (this.colBoxState[x] == 'idle') {
                box.UpdateData('rest', 'rolling');
              } else {
                box.UpdateData('rolling', 'rolling');
              } // console.log(x,',,','symbolvalue:',box.ViewItemIndex);

            }

            if (box.ViewItemIndex) {
              box.ViewItemIndex.x = x;
              box.ViewItemIndex.y = i - (this.boxSum.y - this.boxViewSum.y) / 2;
            } // console.log(x,',,','boxUpdateByIndex:',box.ItemIndex.y);

          }

          this.boxArray[x] = viewArray;
        }

        boxUpdate() {
          for (let x = 0; x < this.boxArray.length; x++) {
            this.boxUpdateByIndex(x);
          }
        }

        betBtnClickAction() {
          let black = this.node.getChildByName('blackMask');

          if (black && black.children.length) {
            black.active = true;

            for (let i = 0; i < black.children.length; i++) {
              const element = black.children[i];
              let opacity = element.getComponent(UIOpacity);
              opacity.opacity = 0;
              tween(opacity).to(0.4, {
                opacity: 0
              }).start();
            }
          }
        } //-------- ****** -------


        rollStopByColumnArr(arr, colFinishCall = null, allFinishCall = null) {
          if (arr && Array.isArray(arr) && arr.length > 0) {
            this.removeIDSchedule('startScheduleAnima');
            let rollBackType = true; // if(this.rollSpeedMode == 'turbo' || this.rollQuickMode == 'quick'){
            //     rollBackType = false;
            // }

            let motions = [];
            let indexY = Math.floor(Math.abs(this.boxTargetPosX[0].y) / this.boxSize.y);
            const topIndex = indexY + this.boxViewSum.y + (this.boxSum.y - this.boxViewSum.y) / 2;
            const nextIndex = (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
              error: Error()
            }), sConfigMgr) : sConfigMgr).instance.GetNextBandIndex(topIndex);
            const nowPosY = this.boxTargetPosX[0].y;
            const deltaY = nowPosY % this.boxSize.y;
            let targetPos = nowPosY - (this.boxSum.y + nextIndex - topIndex) * this.boxSize.y - deltaY;

            for (let i = 0; i < arr.length; i++) {
              const column = arr[i];

              if (column >= 0 && column < this.boxArray.length) {
                (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
                  error: Error()
                }), sConfigMgr) : sConfigMgr).instance.setBoxTriggerValue(column, true);
                (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
                  error: Error()
                }), sConfigMgr) : sConfigMgr).instance.SetBoxDataArea(column, nextIndex);
                const motion = this.stopToPos(column, targetPos + this.getSkewPos(i), _col => {
                  if (colFinishCall) {
                    colFinishCall(_col);
                  }

                  if (column == arr.length - 1) {
                    this.boxUpdate(); // this.setBoxArrayState('rest','rolling');

                    if (allFinishCall) {
                      allFinishCall();
                    }
                  }
                }, rollBackType);
                motions.push(motion);
                this.rollMotionPush(column, motion);
              }
            }

            if (this.rollQuickMode == 'quick') {
              for (let m = 0; m < motions.length; m++) {
                const _motion = motions[m];

                if (_motion) {
                  _motion.GoEnd();
                }
              }
            }
          }
        }

        rollStopAllColumn() {
          this.removeIDSchedule('startScheduleAnima');
          const headBoxPos = this.boxTargetPosX[0].y;
          let indexY = Math.abs(Math.trunc(headBoxPos / this.boxSize.y)); // console.log('indexY:'+indexY);

          let rollBackType = true; // if(this.RollSpeedMode == 'turbo' || this.rollQuickMode == 'quick'){
          //     rollBackType = false;
          // }

          let motions = [];

          for (let i = 0; i < this.boxArray.length; i++) {
            let topIndex = indexY + this.boxViewSum.y + (this.boxSum.y - this.boxViewSum.y) / 2;
            let nextIndex = (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
              error: Error()
            }), sConfigMgr) : sConfigMgr).instance.GetNextBandIndex(topIndex); // console.log('nextIndex:'+nextIndex);

            let nowPosY = headBoxPos;
            let deltaY = nowPosY % this.boxSize.y;
            let targetPos = nowPosY - (this.boxSum.y + nextIndex - topIndex) * this.boxSize.y - deltaY;
            const column = i;

            if (column >= 0 && column < this.boxArray.length) {
              (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
                error: Error()
              }), sConfigMgr) : sConfigMgr).instance.setBoxTriggerValue(column, true);
              (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
                error: Error()
              }), sConfigMgr) : sConfigMgr).instance.SetBoxDataArea(column, nextIndex);
              const motion = this.stopToPos(column, targetPos + this.getSkewPos(i), _col => {
                if (this.isAllBoxRollStop()) {
                  if (!rollBackType) {
                    this.boxSetTargetPosUpdate();
                  }

                  if (this.RollSpeedMode == 'turbo') {
                    this.boxUpdate();
                  }

                  this.rollMotionType = 'stop';
                  director.emit('rollStopOneRoundCall');
                }
              }, rollBackType);

              if (this.RollSpeedMode == 'turbo') {
                motion.rollPixelYLimit = this.boxSize.y * this.boxViewSum.y - this.boxSize.y / 2;
              }

              motions.push(motion);
              this.rollMotionPush(column, motion);
            }
          }

          if (this.rollQuickMode == 'quick') {
            for (let m = 0; m < motions.length; m++) {
              const _motion = motions[m];

              if (_motion) {
                _motion.GoEnd();
              }
            }
          }
        }

        rollStopFloatBack(arr, waitFromMsg = false, finishCallBack = null) {
          if (arr) {
            this.removeIDSchedule('startScheduleAnima');
            let indexY = Math.floor(Math.abs(this.boxTargetPosX[0].y) / this.boxSize.y);
            const topIndex = indexY + this.boxViewSum.y + (this.boxSum.y - this.boxViewSum.y) / 2;
            const nextIndex = (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
              error: Error()
            }), sConfigMgr) : sConfigMgr).instance.GetNextBandIndex(topIndex);
            const nowPosY = this.boxTargetPosX[0].y;
            const deltaY = nowPosY % this.boxSize.y;
            let targetPos = nowPosY - (this.boxSum.y + nextIndex - topIndex) * this.boxSize.y - deltaY;
            const yLen = this.boxViewSum.y;
            let ccc = 0;

            for (let i = 0; i < this.boxArray.length; i++) {
              const column = i;

              if (arr.indexOf(column) < 0) {
                (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
                  error: Error()
                }), sConfigMgr) : sConfigMgr).instance.setBoxTriggerValue(column, true);
                (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
                  error: Error()
                }), sConfigMgr) : sConfigMgr).instance.SetBoxDataArea(column, nextIndex);
                this.stopToPos(column, targetPos + this.getSkewPos(i), _col => {
                  if (_col == this.boxArray.length - 1) {
                    if (finishCallBack) {
                      finishCallBack();
                    }
                  }
                });
              } else {
                ccc++;
                let rollBackValue = 0;
                rollBackValue = this.boxSize.y * (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                  error: Error()
                }), sUtil) : sUtil).RandomInt(1, yLen); // rollBackValue = this.boxSize.y * sUtil.RandomInt(1,yLen) * (sUtil.RandomInt(0,2) == 0 ? 1 : -1);

                (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
                  error: Error()
                }), sConfigMgr) : sConfigMgr).instance.setBoxTriggerValue(column, true);
                (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
                  error: Error()
                }), sConfigMgr) : sConfigMgr).instance.SetBoxDataArea(column, nextIndex);
                this.stopToPos(column, targetPos + rollBackValue + this.getSkewPos(i), _col => {
                  if (_col == this.boxArray.length - 1) {
                    if (finishCallBack) {
                      finishCallBack();
                    }
                  }

                  if (waitFromMsg) {
                    (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                      error: Error()
                    }), sUtil) : sUtil).once('rollStopBoxBySelfRollBack', () => {
                      this.stopToPosPure(column, targetPos + this.getSkewPos(i), col => {
                        director.emit('rollFinishOneRoundCall', col);
                        ccc--;

                        if (ccc == 0) {
                          this.boxUpdate();
                          this.setBoxArrayState('rest', 'rolling');
                          this.rollMotionType = 'stop';
                          director.emit('rollStopOneRoundCall');
                        }
                      }, 0.2);
                    });
                  } else {
                    this.scheduleOnce(() => {
                      this.stopToPosPure(column, targetPos, col => {
                        director.emit('rollFinishOneRoundCall', col);
                        ccc--;

                        if (ccc == 0) {
                          this.boxUpdate();
                          this.setBoxArrayState('rest', 'rolling');
                          this.rollMotionType = 'stop';
                          director.emit('rollStopOneRoundCall');
                        }
                      }, 0.2);
                    }, 0.5);
                  }
                });
              }
            }
          }
        }

        getRollStopPosByColumn(column) {
          let indexY = Math.floor(Math.abs(this.boxTargetPosX[column].y) / this.boxSize.y);
          const topIndex = indexY + this.boxViewSum.y + (this.boxSum.y - this.boxViewSum.y) / 2;
          const nextIndex = (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
            error: Error()
          }), sConfigMgr) : sConfigMgr).instance.GetNextBandIndex(topIndex);
          const nowPosY = this.boxTargetPosX[column].y;
          const deltaY = nowPosY % this.boxSize.y;
          const targetPos = nowPosY - (this.boxSum.y + nextIndex - topIndex) * this.boxSize.y - deltaY;
          return targetPos;
        }

        rollStopBoxBySelf(column, rollBackValue = 0, waitFromMsg = false, finishCallBack = null, tPos = -1, rollBack = false) {
          this.removeIDSchedule('startScheduleAnima');
          const nCol = tPos == -1 ? column : tPos;
          let indexY = Math.floor(Math.abs(this.boxTargetPosX[nCol].y) / this.boxSize.y);
          const topIndex = indexY + this.boxViewSum.y + (this.boxSum.y - this.boxViewSum.y) / 2;
          const nextIndex = (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
            error: Error()
          }), sConfigMgr) : sConfigMgr).instance.GetNextBandIndex(topIndex);
          const nowPosY = this.boxTargetPosX[nCol].y;
          const deltaY = nowPosY % this.boxSize.y;
          let targetPos = nowPosY - (this.boxSum.y + nextIndex - topIndex) * this.boxSize.y - deltaY;
          (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
            error: Error()
          }), sConfigMgr) : sConfigMgr).instance.setBoxTriggerValue(column, true);
          (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
            error: Error()
          }), sConfigMgr) : sConfigMgr).instance.SetBoxDataArea(column, nextIndex);
          let rollBackType = true; // if(this.rollSpeedMode == 'turbo' || this.rollQuickMode == 'quick'){
          //     rollBackType = false;
          //     if(this.rollSpeedMode == 'turbo' && rollBack){
          //         rollBackType = true;
          //     }
          // }

          let motion = null;

          if (rollBackValue == 0) {
            motion = this.stopToPos(column, targetPos + this.getSkewPos(column), _col => {
              if (!rollBackType) {
                this.boxUpdateByIndex(_col);
              }

              if (finishCallBack) {
                finishCallBack(_col);
              }
            }, rollBackType);
          } else {
            rollBackValue = this.boxSize.y * rollBackValue;
            motion = this.stopToPos(column, targetPos + rollBackValue + this.getSkewPos(column), _col => {
              if (waitFromMsg) {
                (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                  error: Error()
                }), sUtil) : sUtil).once('rollStopBoxBySelfRollBack', () => {
                  this.stopToPosPure(column, targetPos, col => {
                    director.emit('rollFinishOneRoundCall', col);
                  }, 0.2);
                });

                if (finishCallBack) {
                  finishCallBack(_col);
                }
              } else {
                this.scheduleOnce(() => {
                  this.stopToPosPure(column, targetPos, col => {
                    director.emit('rollFinishOneRoundCall', col);
                  }, 0.2);
                }, 0.5);
              }
            });
          }
        }

        setMotionTweenSpeedByIndex(index, add, min, waitTime = 0) {
          // console.log('add:',add,'min : ', min, 'waitTime : ', waitTime);
          const rollCol = this.tweenRollColumnArray[index];

          if (rollCol) {
            this.removeIDSchedule('MotionTweenSpeed_' + index);

            if (waitTime == 0) {
              const motion = this.tweenRollColumnArray[index].tween;

              if (motion) {
                motion.speedIncrement = add;
                motion.rollSpeedMin = min;
              }
            } else {
              this.pushIDSchedule(() => {
                const motion = this.tweenRollColumnArray[index].tween;

                if (motion) {
                  motion.speedIncrement = add;
                  motion.rollSpeedMin = min;
                }
              }, waitTime, 'MotionTweenSpeed_' + index);
            }
          }
        }

        rollBegain(type = 'normal') {
          director.emit('slotSubGameRollBegin');
          this.setBlackNodeSibling(this.blackNodeSibling);
          const y = this.boxTargetPosX[0].y - this.getSkewPos(0);

          for (let i = 0; i < this.boxTargetPosX.length; i++) {
            const element = this.boxTargetPosX[i];
            element.y = y + this.getSkewPos(i);
          }

          this.betRollType = 'normal';
          this.rollSpeedMode = type;
          this.betBtnClickAction();
          this.clearEffectAllBoxArray('rest', 'idle');
          this.setBoxArrayState('rest', 'idle');
          (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
            error: Error()
          }), sConfigMgr) : sConfigMgr).instance.setBoxsTrigger(false);
          this.rrrolll();
        }

        stopToPos(column, posY, finishCallBack = null, rollBack = true) {
          let speed = null,
              speedIncrement = 0,
              rollSpeedMin = 0;
          const tweenRoll = this.tweenRollColumnArray[column];

          if (tweenRoll) {
            const rollMotion = tweenRoll.tween;

            if (rollMotion) {
              speed = rollMotion.Speed;
              speedIncrement = rollMotion.speedIncrement;
              rollSpeedMin = rollMotion.rollSpeedMin;
              (_crd && sMotionMgr === void 0 ? (_reportPossibleCrUseOfsMotionMgr({
                error: Error()
              }), sMotionMgr) : sMotionMgr).RemoveTweenByID(tweenRoll.tween.id);
            }
          }

          const nowPosY = this.boxTargetPosX[column].y;
          let yDelta = nowPosY > posY ? -40 : 40;
          let motion = null;

          if (rollBack) {
            motion = this.rollBoxToPosArrays(column, posY + yDelta, speed, () => {
              this.rollMotionRemove(column);
              director.emit('rollOneColumnBump', column);
              this.rollBoxToPosArrays(column, posY, rollBack ? speed / 10 : speed, () => {
                this.setColumnAllRealBoxArrayState(column, 'idle', 'settlement');
                this.setBoxRollState(column, 0);
                director.emit('rollOneColumnStop', column);

                if (finishCallBack) {
                  finishCallBack(column);
                }
              });
            });
            motion.speedIncrement = speedIncrement;
            motion.rollSpeedMin = rollSpeedMin;
            motion.SetRateCall(() => {
              this.setColumnAllRealBoxArrayState(column, 'rest', 'rolling');
              this.setColBoxState(column, 'idle');
            }, 0.7); // console.log('motion:'+motion.costTime);
          } else {
            motion = this.rollBoxToPosArrays(column, posY, speed, () => {
              this.rollMotionRemove(column);
              this.setColumnAllRealBoxArrayState(column, 'idle', 'settlement');
              this.setBoxRollState(column, 0);
              director.emit('rollOneColumnBump', column);
              director.emit('rollOneColumnStop', column);

              if (finishCallBack) {
                finishCallBack(column);
              }
            });
            motion.speedIncrement = speedIncrement;
            motion.rollSpeedMin = rollSpeedMin;
            motion.SetRateCall(() => {
              this.setColumnAllRealBoxArrayState(column, 'rest', 'rolling');
              this.setColBoxState(column, 'idle');
            }, 0.7); // motion.rollSpeedMin = 100000;
          }

          return motion;
        }

        stopToPosPure(column, posY, finishCallBack = null, time = -1) {
          let speed = this.normalSpeed / 2;
          const tweenRoll = this.tweenRollColumnArray[column];

          if (tweenRoll) {
            const rollMotion = tweenRoll.tween;

            if (rollMotion) {
              (_crd && sMotionMgr === void 0 ? (_reportPossibleCrUseOfsMotionMgr({
                error: Error()
              }), sMotionMgr) : sMotionMgr).RemoveTweenByID(tweenRoll.tween.id);
            }
          }

          const nowPosY = this.boxTargetPosX[column].y;
          const yDelta = nowPosY > posY ? -40 : 40;
          this.rollBoxToPosArrays(column, posY + yDelta, speed, () => {
            director.emit('rollOneColumnBump', column);
            this.rollBoxToPosArrays(column, posY, this.normalSpeed / 20, () => {
              this.setBoxRollState(column, 0);
              director.emit('rollOneColumnStop', column);

              if (finishCallBack) {
                finishCallBack(column);
              }
            });
          });
        }
        /**
         * @zh
         * 点亮ByLine类型的所有中奖格子
         * @param resActObj - 所有中奖的格子集合
         * @param boxState - 格子状态
         * @param tableState - 桌子状态
         */


        byLineboxResWinAction(resActObj, boxState = 'win', tableState = 'settlement') {
          const startY = Math.floor((this.boxSum.y - this.boxViewSum.y) / 2);
          const keys = Object.keys(resActObj);

          if (keys) {
            let longBox = {};

            for (let i = 0; i < keys.length; i++) {
              const element = keys[i]; // console.log('act:'+element);

              const act = element.split('_');

              if (act && act.length == 2) {
                const act1 = parseInt(act[0]);
                const act2 = parseInt(act[1]);

                if (act1 && act2 || act1 == 0 || act2 == 0) {
                  const box = this.boxArray[act1][act2 + startY];

                  if (box) {
                    if (box.SymbolValue >= 1000) {
                      longBox[act1.toString() + (act2 + startY).toString()] = box;

                      for (let m = 1; m < 4; m++) {
                        const y = act2 + startY + m;
                        const upBox = this.boxArray[act1][y];

                        if (upBox && upBox.SymbolValue >= 1000 && y <= this.boxViewSum.y + (this.boxSum.y - this.boxViewSum.y) / 2) {
                          const bKey = act1.toString() + (act2 + startY + m).toString();
                          longBox[bKey] = upBox; // console.log(bKey);
                          // longBox[upBox] = true;
                        } else {
                          break;
                        }
                      }

                      for (let n = 1; n < 4; n++) {
                        const y = act2 + startY - n;
                        const downBox = this.boxArray[act1][act2 + startY - n];

                        if (downBox && downBox.SymbolValue >= 1000 && y <= this.boxViewSum.y + (this.boxSum.y - this.boxViewSum.y) / 2) {
                          const dKey = act1.toString() + (act2 + startY - n).toString();
                          longBox[dKey] = downBox;
                        } else {
                          break;
                        }
                      }
                    } else {
                      box.UpdateData(boxState, tableState);
                    }
                  }
                }
              }
            }

            const longKeys = Object.keys(longBox);

            if (longKeys && longKeys.length > 0) {
              for (let i = 0; i < longKeys.length; i++) {
                const key = longKeys[i];
                const box = longBox[key]; // console.log('key:'+box.SymbolValue);

                box.UpdateData(boxState, tableState);
              }
            } // console.log('......');

          }
        }
        /**
         * @zh
         * 点亮ByWay类型的所有中奖格子
         * @param resActObj - 所有中奖的格子集合
         * @param boxState - 格子状态
         * @param tableState - 桌子状态
         */


        byWayboxResWinAction(resActObj, boxState = 'win', tableState = 'settlement') {
          const startY = Math.floor((this.boxSum.y - this.boxViewSum.y) / 2);
          const posKey = Object.keys(resActObj.act_pos);

          if (posKey && posKey.length > 0) {
            let boxActArr = [];

            for (let i = 0; i < posKey.length; i++) {
              const key = posKey[i];
              const act = key.split('_');

              if (act && act.length == 2) {
                const act1 = parseInt(act[0]);
                const act2 = parseInt(act[1]);

                if (act1 && act2 || act1 == 0 || act2 == 0) {
                  if (!boxActArr[act1]) {
                    boxActArr[act1] = [];
                  }

                  const box = this.boxArray[act1][act2 + startY];
                  boxActArr[act1].push(box);
                }
              } // for (let j = startY; j < this.boxArray[column].length - startY; j++) {
              //     const box : sBoxItem = this.boxArray[column][j];
              //     const symbolValue = box.SymbolValue;
              //     if(symbolValue == resActObj.bonus_symbol || symbolValue == 100){
              //         box.updateData('win');
              //     }
              // }

            }

            for (let i = 0; i < boxActArr.length; i++) {
              const arr = boxActArr[i];
              this.scheduleOnce(() => {
                for (let j = 0; j < arr.length; j++) {
                  const box = arr[j];

                  if (box) {
                    box.UpdateData(boxState, tableState);
                  }
                }
              }, i * 0.1);
            }
          }
        }
        /**
         * @zh
         * 改变resActObj集合结构中所有格子状态
         * @param resActObj - 所有中奖的格子集合
         * @param boxState - 格子状态
         * @param tableState - 桌子状态
         */


        boxStateAction(resActObj, boxState = 'win', tableState = 'settlement') {
          if (resActObj) {
            const startY = Math.floor((this.boxSum.y - this.boxViewSum.y) / 2);

            for (let x = 0; x < this.boxArray.length; x++) {
              for (let y = (this.boxSum.y - this.boxViewSum.y) / 2; y < this.boxArray[x].length - (this.boxSum.y - this.boxViewSum.y) / 2; y++) {
                const element = this.boxArray[x][y];
                const res = resActObj[x + '_' + (y - startY)];

                if (!res) {
                  element.UpdateData(boxState, tableState);
                }
              }
            }
          }
        }
        /**
         * @zh
         * 让第一层黑幕以动画效果的方式变亮或者变暗
         * @param index - 这一列不变化
         * @param beComeBlack - 是否变黑
         * 
         * @example
         * sBoxMgr.instance.blackCurtainAnima(0,true);
         * 让除了第1列的黑幕变黑
         */


        blackCurtainAnima(index = -1, beComeBlack = true) {
          let opacityValue = 0,
              targetValue = 125;

          if (!beComeBlack) {
            opacityValue = 125;
            targetValue = 0;
          }

          let black = this.node.getChildByName('blackMask');

          if (black && black.children.length) {
            black.active = true;

            for (let i = 0; i < black.children.length; i++) {
              if (index != i) {
                const element = black.children[i];
                let opacity = element.getComponent(UIOpacity);

                if (beComeBlack) {
                  opacity.opacity = opacityValue;
                }

                tween(opacity).to(0.4, {
                  opacity: targetValue
                }).start();
              }
            }
          }
        }

        blackCurtainAnimaNoStart(index = -1, beComeBlack = true) {
          let opacityValue = 0,
              targetValue = 125;

          if (!beComeBlack) {
            opacityValue = 125;
            targetValue = 0;
          }

          let black = this.node.getChildByName('blackMask');

          if (black && black.children.length) {
            black.active = true;

            for (let i = 0; i < black.children.length; i++) {
              if (index != i) {
                const element = black.children[i];
                let opacity = element.getComponent(UIOpacity);
                tween(opacity).to(0.4, {
                  opacity: targetValue
                }).start();
              }
            }
          }
        }
        /**
         * @zh
         * 让第一层黑幕以动画效果的方式变亮或者变暗
         * @param arrIndex - 这一列不变化
         * @param beComeBlack - 是否变黑
         * 
         * @example
         * sBoxMgr.instance.blackCurtainAnima([1,2],true);
         * 让除了第2、3列的其他黑幕变黑
         */


        blackArrCurtainAnima(arrIndex, beComeBlack = true) {
          let opacityValue = 0,
              targetValue = 125;

          if (!beComeBlack) {
            opacityValue = 125;
            targetValue = 0;
          }

          let black = this.node.getChildByName('blackMask');

          if (black && black.children.length) {
            black.active = true;

            for (let i = 0; i < black.children.length; i++) {
              if (arrIndex.indexOf(i) < 0) {
                const element = black.children[i];
                let opacity = element.getComponent(UIOpacity);

                if (beComeBlack) {
                  opacity.opacity = opacityValue;
                }

                tween(opacity).to(0.4, {
                  opacity: targetValue
                }).start();
              }
            }
          }
        }
        /**
         * @zh
         * 让第一层黑幕变亮或者变暗
         * @param index - 这一列变亮
         * 
         * @example
         * sBoxMgr.instance.blackCurtain(1,true);
         * 让除了第2列的其他黑幕变黑
         */


        blackCurtain(index = -1) {
          let black = this.node.getChildByName('blackMask');

          if (black && black.children.length) {
            black.active = true;

            for (let i = 0; i < black.children.length; i++) {
              const element = black.children[i];
              let opacity = element.getComponent(UIOpacity);

              if (index != i) {
                opacity.opacity = 125;
              } else {
                opacity.opacity = 0;
              }
            }
          }
        }

        setBlackNodeSibling(index) {
          if (this.blackNode) {
            if (index >= 0) {
              this.blackNode.setSiblingIndex(index);
            } else {
              this.blackNode.setSiblingIndex(this.blackNode.parent.children.length + index);
            }
          }
        }
        /**
         * @zh
         * 消除所有格子的特效
         * @param rollState - 格子状态
         * @param tableState - 桌子状态
         */


        clearEffectAllBoxArray(rollState, tableState) {
          for (let x = 0; x < this.boxArray.length; x++) {
            for (let y = 0; y < this.boxArray[x].length; y++) {
              const element = this.boxArray[x][y];
              element.ClearEffect(rollState, tableState);
            }
          }
        }
        /**
         * @zh
         * 改变所有可视格子的状态
         * @param rollState - 格子状态
         * @param tableState - 桌子状态
         */


        setAllViewBoxArrayState(rollState, tableState) {
          for (let x = 0; x < this.boxArray.length; x++) {
            for (let y = (this.boxSum.y - this.boxViewSum.y) / 2; y < this.boxArray[x].length - (this.boxSum.y - this.boxViewSum.y) / 2; y++) {
              const element = this.boxArray[x][y];
              element.UpdateData(rollState, tableState);
            }
          }
        }
        /**
         * @zh
         * 改变所有可视格子的状态
         * @param rollState - 格子状态
         * @param tableState - 桌子状态
         */


        setAllBoxArrayState(rollState, tableState) {
          for (let x = 0; x < this.boxArray.length; x++) {
            for (let y = 0; y < this.boxArray[x].length; y++) {
              const element = this.boxArray[x][y];
              element.UpdateData(rollState, tableState);
            }
          }
        }
        /**
         * @zh
         * 改变所有格子的状态
         * @param rollState - 格子状态
         * @param tableState - 桌子状态
         */


        setBoxArrayState(type, tableState) {
          for (let x = 0; x < this.boxArray.length; x++) {
            // const boxViewY = this.boxArray[x].length - (this.boxSum.y - this.boxViewSum.y)/2;
            for (let y = 0; y < this.boxArray[x].length; y++) {
              const element = this.boxArray[x][y];
              element.UpdateData(type, tableState);
            }
          }
        }
        /**
         * @zh
         * 改变所有满足条件的可视格子的状态
         * @param symbol - 条件回调
         * @param rollState - 格子状态
         * @param tableState - 桌子状态
         * 
         * @example
         * sBoxMgr.instance.setAllViewTargetBoxArrayState(symbol => {return symbol == 1;},'win','settlement');
         * 让所有symbol为1的格子变成rollState为win且tableState为settlement的状态
         */


        setAllViewTargetBoxArrayState(symbol, rollState, tableState) {
          for (let x = 0; x < this.boxArray.length; x++) {
            for (let y = (this.boxSum.y - this.boxViewSum.y) / 2; y < this.boxArray[x].length - (this.boxSum.y - this.boxViewSum.y) / 2; y++) {
              const element = this.boxArray[x][y];

              if (symbol && symbol(element.SymbolValue)) {
                element.UpdateData(rollState, tableState);
              }
            }
          }
        }

        setAllViewTargetBoxActionArrayState(symbol, rollState, tableState) {
          for (let x = 0; x < this.boxArray.length; x++) {
            for (let y = (this.boxSum.y - this.boxViewSum.y) / 2; y < this.boxArray[x].length - (this.boxSum.y - this.boxViewSum.y) / 2; y++) {
              const element = this.boxArray[x][y];

              if (symbol && symbol(element)) {
                element.UpdateData(rollState, tableState);
              }
            }
          }
        }
        /**
         * @zh
         * 改变指定列的可视格子的状态
         * @param index - 指定列
         * @param rollState - 格子状态
         * @param tableState - 桌子状态
         */


        setColumnBoxArrayState(index, type, tableState) {
          for (let y = (this.boxSum.y - this.boxViewSum.y) / 2; y < this.boxArray[index].length - (this.boxSum.y - this.boxViewSum.y) / 2; y++) {
            const element = this.boxArray[index][y];
            element.UpdateData(type, tableState);
          }
        }
        /**
         * @zh
         * 改变所有满足条件的指定列的格子的状态
         * @param index - 指定列
         * @param symbol - 条件回调
         * @param rollState - 格子状态
         * @param tableState - 桌子状态
         * 
         * @example
         * sBoxMgr.instance.setColumnTargetBoxArrayState(1,symbol => {return symbol == 1;},'win','settlement');
         * 让所有第二列symbol为1的格子变成rollState为win且tableState为settlement的状态
         */


        setColumnTargetBoxArrayState(index, symbol, type, tableState) {
          for (let y = (this.boxSum.y - this.boxViewSum.y) / 2; y < this.boxArray[index].length - (this.boxSum.y - this.boxViewSum.y) / 2; y++) {
            const element = this.boxArray[index][y];

            if (symbol && symbol(element.SymbolValue)) {
              element.UpdateData(type, tableState);
            }
          }
        }

        setColumnTargetBoxActionArrayState(index, symbol, type, tableState) {
          for (let y = (this.boxSum.y - this.boxViewSum.y) / 2; y < this.boxArray[index].length - (this.boxSum.y - this.boxViewSum.y) / 2; y++) {
            const element = this.boxArray[index][y];

            if (symbol && symbol(element)) {
              element.UpdateData(type, tableState);
            }
          }
        }
        /**
         * @zh
         * 改变指定列的所有格子的状态
         * @param index - 指定列
         * @param rollState - 格子状态
         * @param tableState - 桌子状态
         */


        setColumnAllBoxArrayState(index, type, tableState) {
          if (index >= 0 && index < this.boxArray.length) {
            const boxViewY = this.boxArray[index].length - (this.boxSum.y - this.boxViewSum.y) / 2;

            for (let y = 0; y < boxViewY; y++) {
              const element = this.boxArray[index][y];
              element.UpdateData(type, tableState);
            }
          }
        }
        /**
        * @zh
        * 改变指定列的所有真实格子的状态
        * @param index - 指定列
        * @param rollState - 格子状态
        * @param tableState - 桌子状态
        */


        setColumnAllRealBoxArrayState(index, type, tableState) {
          if (index >= 0 && index < this.boxArray.length) {
            const boxViewY = this.boxArray[index].length - (this.boxSum.y - this.boxViewSum.y) / 2;

            for (let y = 0; y < this.boxArray[index].length; y++) {
              const element = this.boxArray[index][y];
              element.UpdateData(y < boxViewY ? type : 'rest', tableState);
            }
          }
        }
        /**
         * @zh
         * 改变所有满足条件的所有格子的状态
         * @param index - 指定列
         * @param symbol - 条件回调
         * @param rollState - 格子状态
         * @param tableState - 桌子状态
         */


        setColumnAllTargetBoxArrayState(index, symbol, type, tableState) {
          if (index >= 0 && index < this.boxArray.length) {
            const boxViewY = this.boxArray[index].length - (this.boxSum.y - this.boxViewSum.y) / 2;

            for (let y = 0; y < boxViewY; y++) {
              const element = this.boxArray[index][y];

              if (symbol && symbol(element.SymbolValue)) {
                element.UpdateData(type, tableState);
              }
            }
          }
        }

        changeEffectAllBoxArray(rollState, tableState) {
          for (let x = 0; x < this.boxArray.length; x++) {
            for (let y = 0; y < this.boxArray[x].length; y++) {
              const element = this.boxArray[x][y];
              element.ChangeEffectAction(rollState, tableState);
            }
          }
        }

        changeEffectBoxArray(x, y, rollState, tableState) {
          if (x >= 0 && x < this.boxArray.length && y >= 0 && y < this.boxViewSum.y) {
            const element = this.boxArray[x][y + (this.boxSum.y - this.boxViewSum.y) / 2];

            if (element) {
              element.ChangeEffectAction(rollState, tableState);
            }
          }
        }
        /**
         * @zh
         * 获取视窗索引的格子
         * @param x - 指定列
         * @param y - 指定行
         * @return - The boxEntity
         */


        getBoxByPos(x, y) {
          return this.boxArray[x][(this.boxSum.y - this.boxViewSum.y) / 2 + y];
        }
        /**
         * @zh
         * 获取视窗大小
         * @return - The number of vec2
         */


        getBoxViewSize() {
          return this.boxViewSum;
        }
        /**
         * @zh
         * 获取实际格子数量
         * @return - [x,y]
         */


        getBoxSize() {
          return this.boxSum;
        }
        /**
         * @zh
         * 根据视窗坐标改变格子的状态
         * @param x - 指定列
         * @param y - 指定行
         * @param rollState - 格子状态
         * @param tableState - 桌子状态
         */


        updateBoxDataByXY(x, y, boxType, tableState) {
          const startY = Math.floor((this.boxSum.y - this.boxViewSum.y) / 2);

          if (x >= 0 && x < this.boxArray.length) {
            const yIndex = y + startY;

            if (yIndex >= 0 && yIndex < this.boxArray[x].length) {
              const box = this.boxArray[x][y + startY];

              if (box) {
                box.UpdateData(boxType, tableState);
              }
            }
          }
        }
        /**
         * @zh
         * 根据视窗坐标获取格子实体
         * @param x - 指定列
         * @param y - 指定行
         */


        getBoxEntityByXY(x, y) {
          const startY = Math.floor((this.boxSum.y - this.boxViewSum.y) / 2);
          const box = this.boxArray[x][y + startY];
          return box;
        }
        /**
         * @zh
         * 判断该坐标是否在视窗坐标系之内
         * @param index - 指定行列 itemIndex
         * @return - 是或否
         */


        isViewBoxItem(index) {
          if (index.x >= 0 && index.x < this.boxViewSum.x && index.y >= 0 && index.y < this.boxViewSum.y) {
            return true;
          }

          return false;
        }
        /**
         * @zh
         * 判断该全局坐标是否在视窗坐标系之内
         * @return - 是或否
         */


        isViewBoxPos(x, y) {
          if (x >= 0 && x < this.boxViewSum.x && y >= (this.boxSum.y - this.boxViewSum.y) / 2 && y < (this.boxSum.y - this.boxViewSum.y) / 2 + this.boxViewSum.y) {
            return true;
          }

          return false;
        }
        /**
         * @zh
         * 通过可视坐标的xy获取可视窗口的格子的世界坐标
         * @param x - 可视坐标的格子x索引
         * @param y - 可视坐标的格子y索引
         * @return - vec3
         */


        getBoxWorldPosByXY(x, y) {
          if (x >= 0 && x < this.boxSum.x && y >= 0 && y < this.boxSum.y) {
            return v3(this.node.worldPosition.x + x * this.boxSize.x + this.boxSize.x / 2, this.node.worldPosition.y + y * this.boxSize.y + this.boxSize.y / 2, 0);
          } else {
            console.log('please input correct getBoxWorldPosByXY');
          }
        }
        /**
         * @zh
         * 通过可视坐标的xy获取可视窗口的格子的相对坐标
         * @param x - 可视坐标的格子x索引
         * @param y - 可视坐标的格子y索引
         * @return - vec3
         */


        getBoxLocalPosByXY(x, y) {
          if (x >= 0 && x < this.boxSum.x && y >= 0 && y < this.boxSum.y) {
            return v3(x * this.boxSize.x + this.boxSize.x / 2, y * this.boxSize.y + this.boxSize.y / 2, 0);
          } else {
            console.log('please input correct getBoxLocalPosByXY');
          }
        }
        /**
         * @zh
         * 通过可视坐标的xy获取可视窗口的格子的卯点相对坐标
         * @param x - 可视坐标的格子x索引
         * @param y - 可视坐标的格子y索引
         * @return - vec3
         */


        getBoxLocalAnchorPosByXY(x, y) {
          if (x >= 0 && x < this.boxSum.x && y >= 0 && y < this.boxSum.y) {
            return v3(x * this.boxSize.x, y * this.boxSize.y, 0);
          } else {
            console.log('please input correct getBoxLocalAnchorPosByXY');
          }
        }
        /**
         * @zh
         * 根据结果表中的索引获取格子世界坐标
         * @param x - 结果表x索引
         * @param y - 结果表y索引
         * @return - vec3
         */


        getBoxWorldPosByRoundIndex(x, y) {
          const deltaBoxYSum = Math.trunc(Math.abs((this.boxSum.y - this.boxViewSum.y) / 2));
          return v3(this.node.worldPosition.x + x * this.boxSize.x + this.boxSize.x / 2, this.node.worldPosition.y + (y - deltaBoxYSum) * this.boxSize.y + this.boxSize.y / 2, 0);
        }
        /**
         * @zh
         * 通过索引获取格子理论位置
         * @param x - 可视坐标的格子x索引
         * @param y - 可视坐标的格子y索引
         * @return - vec3
         */


        getBoxContainerPosByXY(x, y) {
          return v3(x * this.boxSize.x, y * this.boxSize.y + this.getSkewPos(x), 0);
        }
        /**
         * @zh
         * 获取指定列的所有格子
         * @param col - 指定列
         * @return - 格子数组
         */


        GetColAllBoxPos(col) {
          let boxArr = [];

          for (let i = 0; i < this.boxViewSum.y; i++) {
            boxArr.push(v3(col * this.boxSize.x, i * this.boxSize.y, 0));
          }

          return boxArr;
        }
        /**
         * @zh
         * 获取可视窗口内指定列的所有格子
         * @param col - 指定列
         * @return - 格子数组
         */


        GetColAllBox(col) {
          let boxArr = [];

          for (let y = (this.boxSum.y - this.boxViewSum.y) / 2; y < this.boxArray[col].length - (this.boxSum.y - this.boxViewSum.y) / 2; y++) {
            const element = this.boxArray[col][y];
            boxArr.push(element);
          }

          return boxArr;
        }
        /**
         * @zh
         * 触发所有满足条件的所有格子特殊方法
         * @param index - 指定列
         * @param symbol - 条件回调
         * @param actionName - 自定义方法名
         */


        setColumnAllTargetBoxArrayAction(index, symbol, actionName) {
          if (index >= 0 && index < this.boxArray.length) {
            const boxViewY = this.boxArray[index].length - (this.boxSum.y - this.boxViewSum.y) / 2;

            for (let y = 0; y < boxViewY; y++) {
              const element = this.boxArray[index][y];

              if (symbol && symbol(element.SymbolValue)) {
                element.DoEnitityAction(actionName);
              }
            }
          }
        }
        /**
         * @zh
         * 触发所有满足条件的所有可视格子特殊方法
         * @param index - 指定列
         * @param symbol - 条件回调
         * @param actionName - 自定义方法名
         */


        DoAllTargetViewBoxArrayAction(symbol, actionName) {
          if (this.boxArray) {
            const boxViewY = Math.trunc((this.boxSum.y - this.boxViewSum.y) / 2);

            for (let x = 0; x < this.boxSum.x; x++) {
              if (x < this.boxArray.length) {
                for (let y = boxViewY; y < this.boxViewSum.y + boxViewY; y++) {
                  if (y < this.boxArray[x].length) {
                    const element = this.boxArray[x][y];

                    if (element && symbol && symbol(element)) {
                      element.DoEnitityAction(actionName);
                    }
                  }
                }
              }
            }
          }
        }
        /**
         * @zh
         * 触发指定格子的特定行为
         * @param x - 指定行
         * @param x - 指定列
         * @param _name - 动作名称
         */


        DoBoxEntityActonByXY(x, y, _name) {
          const startY = Math.floor((this.boxSum.y - this.boxViewSum.y) / 2);
          const box = this.boxArray[x][y + startY];

          if (box) {
            box.DoEnitityAction(_name);
          }
        }
        /**
         * @zh
         * 触发指定列的所有格子特定行为
         * @param index - 指定列
         * @param _name - 动作名称
         */


        DoBoxEntityActon(index, _name) {
          for (let y = 0; y < this.boxArray[index].length; y++) {
            const element = this.boxArray[index][y];
            element.DoEnitityAction(_name);
          }
        }
        /**
         * @zh
         * 触发所有格子特定行为
         * @param index - 指定列
         * @param _name - 动作名称
         */


        DoAllBoxEntityActon(_name) {
          for (let x = 0; x < this.boxArray.length; x++) {
            for (let y = 0; y < this.boxArray[x].length; y++) {
              const element = this.boxArray[x][y];
              element.DoEnitityAction(_name);
            }
          }
        }
        /**
         * @zh
         * 创建格子实体
         * @param pos - 初始坐标位置postision
         */


        CreateBoxEntity(pos) {
          // console.log('CreateBoxEntity:'+globalThis.currentPlayingGameID);
          let boxEntity = new globalThis.boxEntityCtr[globalThis.currentPlayingGameID]();
          boxEntity.BoxPos = pos;

          for (let x = 0; x < this.boxEntitityAsset.length; x++) {
            const entityAsset = this.boxEntitityAsset[x];
            let obj = instantiate(entityAsset.targetBoxNode);
            obj.setParent(entityAsset.targetBoxNode.parent);
            obj.position = pos;
            obj.setSiblingIndex(0);
            boxEntity.SetAssetToObj(entityAsset.targetBoxName, obj);
          }

          return boxEntity;
        }
        /**
         * @zh
         * 创建格子部分对象
         * @param type - itemBase的名字类别 如 box floatBox floatEffect 等
         */


        CreateBoxItemBaseByType(type) {
          for (let x = 0; x < this.boxEntitityAsset.length; x++) {
            const entityAsset = this.boxEntitityAsset[x];

            if (entityAsset.targetBoxName == type) {
              let obj = instantiate(entityAsset.targetBoxNode);

              if (obj) {
                return obj.getComponent(_crd && sBoxItemBase === void 0 ? (_reportPossibleCrUseOfsBoxItemBase({
                  error: Error()
                }), sBoxItemBase) : sBoxItemBase);
              }
            }
          }

          return null;
        }
        /**
         * @zh
         * 锁定可视区域的格子
         */


        SetBoxEntityLock(x, y, value) {
          const startY = Math.floor((this.boxSum.y - this.boxViewSum.y) / 2);
          const box = this.boxArray[x][y + startY];

          if (box) {
            box.IsLocked = value;
          } else {
            console.error('can not find box:' + x + ',' + y);
          }
        }
        /**
         * @zh
         * 在最上方补充新的格子
         */


        ReplenishBoxEntity() {
          for (let x = 0; x < this.boxArray.length; x++) {
            const colBoxes = this.boxArray[x];
            const bottomItemIndex = this.boxArray[x][0].ItemIndex;
            const bottomViewItemIndex = this.boxArray[x][0].ViewItemIndex;
            const topItemIndex = this.boxArray[x][this.boxArray[x].length - 1].ItemIndex;
            let disAppearBoxNum = 0;
            let lockObj = [];

            for (let y = 0; y < colBoxes.length; y++) {
              const box = colBoxes[y];

              if (box.IsLocked) {
                lockObj.push({
                  _box: box,
                  _indexY: y
                });
              } else {
                if (box.IsDisappearBox) {
                  disAppearBoxNum++;
                }
              }
            }

            if (disAppearBoxNum > 0) {
              let waitRemoveBoxes = [];

              for (let y = 0; y < colBoxes.length; y++) {
                const box = colBoxes[y];

                if (box.IsDisappearBox) {
                  waitRemoveBoxes.push(box);
                  (_crd && sObjPool === void 0 ? (_reportPossibleCrUseOfsObjPool({
                    error: Error()
                  }), sObjPool) : sObjPool).Enqueue('boxItem', box);
                  box.ClearItem('rolling', 'rolling');
                }
              }

              for (let i = 0; i < waitRemoveBoxes.length; i++) {
                const element = waitRemoveBoxes[i];
                this.RemoveEntityBoxByObj(x, element);
              }

              const startY = (this.boxSum.y - this.boxViewSum.y) / 2;
              let nowBoxIndex = 0;

              for (let t = 0; t < colBoxes.length; t++) {
                const colBoxT = colBoxes[t];

                if (!colBoxT.IsLocked) {
                  nowBoxIndex++;
                }
              } // const boxIndexCtr = ()=>{
              //     for (let l = 0; l < lockObj.length; l++) {
              //         const _lockO = lockObj[l];
              //         if(_lockO._indexY == nowBoxIndex){
              //             nowBoxIndex++;
              //         }
              //     }
              // };


              for (let z = 0; z < disAppearBoxNum; z++) {
                let obj = (_crd && sObjPool === void 0 ? (_reportPossibleCrUseOfsObjPool({
                  error: Error()
                }), sObjPool) : sObjPool).Dequeue('boxItem');

                if (obj) {
                  // boxIndexCtr();
                  obj.IsDropBox = true;
                  obj.SetBoxActive(true);
                  obj.SetBoxSiblingIndex(0);
                  obj.UpdateBoxPosition(v3(this.boxSize.x * x, (this.boxSum.y - startY + z) * this.boxSize.y + this.getSkewPos(x), 0)); // obj.ItemIndex = v2(x,bottomItemIndex.y + nowBoxIndex);
                  // obj.ViewItemIndex = v2(x,bottomViewItemIndex.y + nowBoxIndex);
                  // obj.UpdateData('rest','settlement');

                  colBoxes.push(obj);
                  nowBoxIndex++;
                }
              }

              if (lockObj.length > 0) {
                for (let i = 0; i < lockObj.length; i++) {
                  const lockBoxObj = lockObj[i];
                  this.RemoveEntityBoxByObj(x, lockBoxObj._box);
                }

                for (let i = 0; i < lockObj.length; i++) {
                  const lockBoxObj = lockObj[i];
                  colBoxes.splice(lockBoxObj._indexY, 0, lockBoxObj._box);
                }
              }
            }

            for (let y = 0; y < colBoxes.length; y++) {
              const box = colBoxes[y];
              box.ItemIndex = v2(x, bottomItemIndex.y + y); // box.ViewItemIndex = v2(x,bottomViewItemIndex.y + y);

              if (box.IsDropBox) {
                box.UpdateData('rest', 'settlement'); // box.IsDropBox = false;
              }
            }
          } // console.log(this.boxArray);

        }
        /**
         * @zh
         * 让所有win的长格子中的空格子设置为消除标记
         */


        SetEmptyBoxDisappear(symbolCall) {
          if (!symbolCall) {
            return;
          }

          for (let x = 0; x < this.boxArray.length; x++) {
            const colBoxes = this.boxArray[x];
            let isWin = false;

            for (let y = 0; y < colBoxes.length; y++) {
              const box = colBoxes[y];

              if (!box.IsEmptyBox) {
                isWin = false;
              }

              if (box.BoxStateNow == 'win') {
                if (box.IsLongWild) {
                  isWin = true;
                }
              }

              if (isWin && symbolCall(box.SymbolValue)) {
                box.WillDisappearBox = true;
              }
            }
          }
        }
        /**
         * @zh
         * 让移除格子后的队列掉落补充的格子
         */


        DropAllEntityBoxAfterRemove(strikeCallBack = null, delayTime = 0.07) {
          const startY = (this.boxSum.y - this.boxViewSum.y) / 2;
          let dropList = {};
          let isDrop = false;

          for (let x = 0; x < this.boxArray.length; x++) {
            const colBoxes = this.boxArray[x];

            for (let y = 0; y < colBoxes.length; y++) {
              const box = colBoxes[y];
              const targetPos = v3(this.boxSize.x * x, (y - startY) * this.boxSize.y + this.getSkewPos(x), 0);
              const boxNowPos = box.GetBoxPosition();

              if (Math.abs(boxNowPos.x - targetPos.x) > 0.1 || Math.abs(boxNowPos.y - targetPos.y) > 0.1) {
                // console.log('drop the beat:'+x+','+ y);
                if (!dropList[x]) {
                  dropList[x] = [];
                }

                dropList[x].push({
                  _box: box,
                  _targetPos: targetPos
                }); // box.DropEntityBoxToPosAnima(targetPos,0.4,yIndex++ * 0.1 + (xIndex) * 0.1);

                isDrop = true;
              }
            }
          }

          if (isDrop) {
            let xIndex = 0;
            const dropListKeys = Object.keys(dropList);

            if (dropListKeys && dropListKeys.length > 0) {
              for (let x = dropListKeys.length - 1; x >= 0; x--) {
                const yList = dropList[dropListKeys[x]];

                if (yList && yList.length > 0) {
                  for (let y = 0; y < yList.length; y++) {
                    const boxObj = yList[y];

                    if (boxObj) {
                      const tBox = boxObj._box;
                      const tTargetPos = boxObj._targetPos;
                      tBox.DropEntityBoxToPosAnima(tTargetPos, 0.24, y * delayTime + xIndex * delayTime, () => {
                        if (x == 0 && y >= dropList[dropListKeys[x]].length - 1) {
                          // console.log(x+','+y);
                          const deltaY = (this.boxSum.y - this.boxViewSum.y) / 2;

                          for (let tx = 0; tx < this.boxArray.length; tx++) {
                            const tColBoxes = this.boxArray[tx];

                            for (let ty = 0; ty < tColBoxes.length; ty++) {
                              tColBoxes[ty].IsDropBox = false;
                              tColBoxes[ty].ViewItemIndex = v2(tx, ty - deltaY);
                            }
                          }

                          director.emit('dropAllBoxFinish');
                        }
                      }, () => {
                        if (strikeCallBack) {
                          strikeCallBack(tBox);
                        }
                      });
                    }
                  }

                  if (yList && yList.length > 0) {
                    xIndex++;
                  }
                }
              }
            }
          }
        }
        /**
         * @zh
         * 让移除格子后的队列掉落补充的格子
         * strikeCallBack : 格子第一次触底的回调
         * delayTime : 落下来的格子的间隔时间
         * isOrderSort : false为每列从右向左落下,true为每列从左往右落下
         * colDelayTime : 每列开始落下的间隔时间
         * firColStrikeCallBack : 每列第一个格子第一次触底的回调
         */


        DropAllEntityBoxAfterRemoveByConfig(_config = null) {
          let strikeCallBack = null;
          let delayTime = 0.07;
          let isOrderSort = false;
          let colDelayTime = 0.07;
          let firColStrikeCallBack = null;

          if (_config) {
            strikeCallBack = _config.strikeCallBack ? _config.strikeCallBack : strikeCallBack;
            firColStrikeCallBack = _config.firColStrikeCallBack ? _config.firColStrikeCallBack : firColStrikeCallBack;
            delayTime = _config.delayTime >= 0 ? _config.delayTime : delayTime;
            isOrderSort = _config.isOrderSort ? true : isOrderSort;
            colDelayTime = _config.colDelayTime >= 0 ? _config.colDelayTime : colDelayTime;
          }

          const startY = (this.boxSum.y - this.boxViewSum.y) / 2;
          let dropList = {};
          let isDrop = false;

          for (let x = 0; x < this.boxArray.length; x++) {
            const colBoxes = this.boxArray[x];

            for (let y = 0; y < colBoxes.length; y++) {
              const box = colBoxes[y];
              const targetPos = v3(this.boxSize.x * x, (y - startY) * this.boxSize.y + this.getSkewPos(x), 0);
              const boxNowPos = box.GetBoxPosition();

              if (Math.abs(boxNowPos.x - targetPos.x) > 0.1 || Math.abs(boxNowPos.y - targetPos.y) > 0.1) {
                // console.log('drop the beat:'+x+','+ y);
                if (!dropList[x]) {
                  dropList[x] = [];
                }

                dropList[x].push({
                  _box: box,
                  _targetPos: targetPos
                }); // box.DropEntityBoxToPosAnima(targetPos,0.4,yIndex++ * 0.1 + (xIndex) * 0.1);

                isDrop = true;
              }
            }
          }

          if (isDrop) {
            let xIndex = 0;
            const dropListKeys = Object.keys(dropList);

            if (dropListKeys && dropListKeys.length > 0) {
              if (isOrderSort) {
                dropListKeys.reverse();
              }

              for (let x = dropListKeys.length - 1; x >= 0; x--) {
                const yList = dropList[dropListKeys[x]];

                if (yList && yList.length > 0) {
                  for (let y = 0; y < yList.length; y++) {
                    const boxObj = yList[y];

                    if (boxObj) {
                      const tBox = boxObj._box;
                      const tTargetPos = boxObj._targetPos;
                      tBox.DropEntityBoxToPosAnima(tTargetPos, 0.24, y * delayTime + xIndex * colDelayTime, () => {
                        if (x == 0 && y >= dropList[dropListKeys[x]].length - 1) {
                          const deltaY = (this.boxSum.y - this.boxViewSum.y) / 2;

                          for (let tx = 0; tx < this.boxArray.length; tx++) {
                            const tColBoxes = this.boxArray[tx];

                            for (let ty = 0; ty < tColBoxes.length; ty++) {
                              tColBoxes[ty].IsDropBox = false;
                              tColBoxes[ty].ViewItemIndex = v2(tx, ty - deltaY); // console.log('x,y ',tColBoxes[ty].ViewItemIndex);
                            }
                          }

                          director.emit('dropAllBoxFinish');
                        }
                      }, () => {
                        if (strikeCallBack) {
                          strikeCallBack(tBox);
                        }

                        if (y == 0) {
                          if (firColStrikeCallBack) {
                            firColStrikeCallBack();
                          }
                        }
                      });
                    }
                  }

                  if (yList && yList.length > 0) {
                    xIndex++;
                  }
                }
              }
            }
          }
        }
        /**
         * @zh
         * 让移除格子后的队列按列单独掉落补充的格子
         */


        DropColEntityBoxAfterRemove(col, strikeCallBack = null) {
          if (col >= 0 && col < this.boxSum.x) {
            const startY = (this.boxSum.y - this.boxViewSum.y) / 2;
            let dropList = [];
            let isDrop = false;
            dropList[col] = [];
            const colBoxes = this.boxArray[col];

            for (let y = 0; y < colBoxes.length; y++) {
              const box = colBoxes[y];
              const targetPos = v3(this.boxSize.x * col, (y - startY) * this.boxSize.y + this.getSkewPos(col), 0);
              const boxNowPos = box.GetBoxPosition();

              if (Math.abs(boxNowPos.x - targetPos.x) > 0.1 || Math.abs(boxNowPos.y - targetPos.y) > 0.1) {
                // console.log('drop the beat:'+x+','+ y);
                dropList[col].push({
                  _box: box,
                  _targetPos: targetPos
                }); // box.DropEntityBoxToPosAnima(targetPos,0.4,yIndex++ * 0.1 + (xIndex) * 0.1);

                isDrop = true;
              }
            }

            if (isDrop) {
              let xIndex = 0;
              const yList = dropList[col];

              if (yList && yList.length > 0) {
                for (let y = 0; y < yList.length; y++) {
                  const boxObj = yList[y];

                  if (boxObj) {
                    const tBox = boxObj._box;
                    const tTargetPos = boxObj._targetPos;
                    tBox.DropEntityBoxToPosAnima(tTargetPos, 0.3, y * 0.1 + xIndex * 0.1, () => {
                      if (col == 0 && y >= dropList[0].length - 1) {
                        // console.log(x+','+y);
                        const deltaY = (this.boxSum.y - this.boxViewSum.y) / 2;
                        const tColBoxes = this.boxArray[col];

                        for (let ty = 0; ty < tColBoxes.length; ty++) {
                          tColBoxes[ty].IsDropBox = false;
                          tColBoxes[ty].ViewItemIndex = v2(col, ty - deltaY); // console.log('x,y ',tColBoxes[ty].ViewItemIndex);
                        }

                        director.emit('dropAllBoxFinish', col);
                      }
                    }, () => {
                      if (strikeCallBack) {
                        strikeCallBack(tBox);
                      }
                    });
                  }
                }

                if (yList && yList.length > 0) {
                  xIndex++;
                }
              }
            }
          } else {
            console.error('DropColEntityBoxAfterRemove col error :', col);
          }
        }
        /**
         * @zh
         * 移除队列里的格子
         * @param col - 指定列
         * @param box - 格子引用
         */


        RemoveEntityBoxByObj(col, box) {
          for (let i = 0; i < this.boxArray[col].length; i++) {
            const element = this.boxArray[col][i];

            if (element == box) {
              this.boxArray[col].splice(i, 1);
              break;
            }
          }
        }

        quit() {
          (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
            error: Error()
          }), sAudioMgr) : sAudioMgr).clearAudioTween();
          sBoxMgr.instance = null;

          for (let x = 0; x < this.boxArray.length; x++) {
            for (let y = 0; y < this.boxArray[x].length; y++) {
              const element = this.boxArray[x][y];

              if (element) {
                element.EntityDestroy();
              }
            }
          }

          this.boxArray = []; // const keys = Object.keys(this.tweenRollColumnArray);
          // if(keys && keys.length > 0){
          //     for (let i = 0; i < keys.length; i++) {
          //         const element = this.tweenRollColumnArray[keys[i]];
          //         if(element && element['tween']){
          //             element['tween'].stop();
          //         }
          //     }
          // }

          (_crd && assetMgr === void 0 ? (_reportPossibleCrUseOfassetMgr({
            error: Error()
          }), assetMgr) : assetMgr).Clear();
          (_crd && sObjPool === void 0 ? (_reportPossibleCrUseOfsObjPool({
            error: Error()
          }), sObjPool) : sObjPool).Clear();
          (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
            error: Error()
          }), sAudioMgr) : sAudioMgr).Clear();
          (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
            error: Error()
          }), sUtil) : sUtil).Clear();
        }

        getSkewPos(column) {
          let skew = this.skewVec3Arr[column];
          return skew ? skew.y : 0;
        }

        saveBoxData() {
          // return;
          if (this.autoSaveRollResult && this.boxArray && this.boxArray.length > 0) {
            let data = {};
            let dataArr = [];

            for (let x = 0; x < this.boxArray.length; x++) {
              let colArr = this.boxArray[x];

              if (colArr && colArr.length > 0) {
                if (!dataArr[x]) {
                  dataArr[x] = [];
                }

                for (let y = 0; y < colArr.length; y++) {
                  const box = colArr[y];
                  dataArr[x].push(box.SymbolValue);
                }
              }
            }

            data.data = dataArr;
            sys.localStorage.setItem('subGameBoxRoundEndData_' + globalThis.currentPlayingGameID, JSON.stringify(data)); // console.log('dataArr:'+JSON.stringify(dataArr));
          }
        }

        createBoxTipButton() {
          const boxTrans = this.node.getComponent(UITransform);

          if (boxTrans && this.boxViewSum && this.boxViewSum.x > 0 && this.boxViewSum.y > 0 && this.boxSize) {
            const boxTipBtnsNode = new Node('boxTipBtns');
            boxTipBtnsNode.layer = boxTrans.node.layer;
            boxTipBtnsNode.parent = this.node;
            const boxTipBtnsTrans = boxTipBtnsNode.addComponent(UITransform);
            boxTipBtnsTrans.anchorX = 0;
            boxTipBtnsTrans.anchorY = 0;
            boxTipBtnsNode.position = Vec3.ZERO;
            boxTipBtnsTrans.setContentSize(boxTrans.contentSize.width, boxTrans.contentSize.height); // boxTipBtnsTrans.node.addComponent(Button);
            // boxTipBtnsTrans.node.on('click',()=>{
            //     console.log('123123');
            // },this);

            for (let x = 0; x < this.boxViewSum.x; x++) {
              for (let y = 0; y < this.boxViewSum.y; y++) {
                const indexX = x;
                const indexY = y;
                const btnNode = new Node(indexX + '_' + indexY);
                btnNode.layer = boxTipBtnsNode.layer;
                btnNode.parent = boxTipBtnsNode;
                const btnTrans = btnNode.addComponent(UITransform);
                btnTrans.anchorX = 0;
                btnTrans.anchorY = 0;
                btnTrans.setContentSize(this.boxSize.x, this.boxSize.y);
                btnNode.position = v3(x * this.boxSize.x, y * this.boxSize.y + this.getSkewPos(x), 0);
                const btn = btnTrans.addComponent(Button);
                btnNode.on('click', () => {
                  // console.log('createBoxTipButton:'+indexX +','+ indexY);
                  if (globalThis.GameBtnEntity) {
                    if (globalThis.GameBtnEntity.betBtnState == 'normal' && globalThis.GameBtnEntity.gameMode == 'normal') {
                      director.emit('sBoxIntroTipClick', 'vertical', this.getBoxEntityByXY(indexX, indexY), indexX, indexY);
                    }
                  } // sBoxMgr.instance.clearEffectAllBoxArray('rest', 'idle');
                  // this.setAllViewBoxArrayState('rest','idle');
                  // const box = this.getBoxEntityByXY(indexX,indexY);
                  // if(box){
                  //     box.UpdateData('win','idle');
                  // }

                }, this);
              }
            }
          }
        }

        flowDelayCall(waitTime, call) {
          if (call) {
            if (this.rollQuickMode == 'quick') {
              call();
            } else {
              this.pushOneSchedule(call, waitTime);
            }
          }
        }

        singleColumnDelayCall(col, waitTime, call) {
          if (call) {
            // if(this.rollQuickMode == 'quick'){
            //     call();
            // }else{
            //     this.removeIDSchedule('colWaitForStop_' + col);
            //     this.pushIDSchedule(call,waitTime,'colWaitForStop_' + col,);
            // }
            this.removeIDSchedule('colWaitForStop_' + col);
            this.pushIDSchedule(call, waitTime, 'colWaitForStop_' + col);
          }
        }

        wholeFlowDelayCall(waitTime, call) {
          if (call) {
            if (this.rollMotionType == 'go') {
              if (this.rollQuickMode == 'quick') {
                call();
              } else {
                this.removeIDSchedule('wholeDelayCall');
                this.pushIDSchedule(() => {
                  if (call) {
                    call();
                  }

                  this.removeIDSchedule('wholeDelayCall');
                }, waitTime, 'wholeDelayCall');
              }
            } else {
              this.wholeFlowCallList.push(() => {
                this.removeIDSchedule('wholeDelayCall');
                this.pushIDSchedule(() => {
                  if (call) {
                    call();
                  }

                  this.removeIDSchedule('wholeDelayCall');
                }, waitTime, 'wholeDelayCall');
              });
            }
          }
        }

        wholeFlowDelayCallTrigger() {
          if (this.wholeFlowCallList && this.wholeFlowCallList.length > 0) {
            for (let i = 0; i < this.wholeFlowCallList.length; i++) {
              const call = this.wholeFlowCallList[i];

              if (call) {
                call();
              }
            }

            this.wholeFlowCallList = [];
          }
        }

        quickClick() {
          if (this.rollMotionType == 'go' && this.rollSpeedMode == 'normal' && this.rollQuickMode != 'quick') {
            this.setColBoxState(-1, 'idle');
            this.setAllBoxArrayState('rest', 'settlement');
            console.log('quickClick');
            this.rollQuickMode = 'quick';
            this.wholeFlowDelayCallTrigger();
            const wholeFlow = this.getIDSchedule('wholeDelayCall');

            if (wholeFlow) {
              console.log('wholeFlow');
              wholeFlow();
              this.removeIDSchedule('wholeDelayCall');
            }

            for (let item of this.rollMotions.values()) {
              if (item) {
                item.GoEnd();
              }
            }

            this.rollMotions.clear();
            director.emit('sBoxQuickClick');
          }
        }

        rollMotionPush(col, motion) {
          this.rollMotions.set(col, motion);
        }

        rollMotionRemove(col) {
          this.rollMotions.delete(col);
        }

        setColBoxState(index = -1, state) {
          if (this.colBoxState) {
            if (index == -1) {
              for (let i = 0; i < this.boxSum.x; i++) {
                this.colBoxState[i] = state;
              }
            } else {
              if (index >= 0 && index < this.colBoxState.length) {
                this.colBoxState[index] = state;
              }
            }
          }
        }

        rollBoxToPosArraysRightNow(index, posY) {
          let pos = this.boxTargetPosX[index];
          pos.y = posY;

          for (let y = 0; y < this.boxArray[index].length; y++) {
            let obj = this.boxArray[index][y];
            const pos = obj.GetBoxPosition();
            obj.UpdateBoxPosition(v3(pos.x, posY, pos.z)); // obj.node.position = v3(obj.node.position.x,posY,obj.node.position.z);
          }
        }

        cancelAllTween() {
          for (let i = 0; i < this.boxTargetPosX.length; i++) {
            const element = this.boxTargetPosX[i];
            Tween.stopAllByTarget(element);
            const tweenTemp = this.tweenRollColumnArray[i];

            if (tweenTemp && tweenTemp.tween) {
              tweenTemp.tween.stop();
            }
          }

          this.unschedule(this.startScheduleAnima);
        }

        getBoxItemByIndex(column, index) {
          for (let i = 0; i < this.boxArray[column].length; i++) {
            if (this.boxArray[column][i].ItemIndex.y == index) {
              return this.boxArray[column][i];
            }
          }
        }

      }, _defineProperty(_class6, "instance", void 0), _temp2), (_descriptor3 = _applyDecoratedDescriptor(_class5.prototype, "boxSum", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return v2(0, 0);
        }
      }), _descriptor4 = _applyDecoratedDescriptor(_class5.prototype, "boxViewSum", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return v2(0, 0);
        }
      }), _descriptor5 = _applyDecoratedDescriptor(_class5.prototype, "boxSize", [_dec6], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return v2(0, 0);
        }
      }), _descriptor6 = _applyDecoratedDescriptor(_class5.prototype, "skewVec3Arr", [_dec7], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return [];
        }
      }), _descriptor7 = _applyDecoratedDescriptor(_class5.prototype, "boxEntitityAsset", [_dec8], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return [];
        }
      }), _descriptor8 = _applyDecoratedDescriptor(_class5.prototype, "normalSpeed", [_dec9], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return 3000;
        }
      }), _descriptor9 = _applyDecoratedDescriptor(_class5.prototype, "turboSpeed", [_dec10], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return 2000;
        }
      }), _descriptor10 = _applyDecoratedDescriptor(_class5.prototype, "speedAnimaDelayTime", [_dec11], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return 2;
        }
      }), _descriptor11 = _applyDecoratedDescriptor(_class5.prototype, "autoSaveRollResult", [_dec12], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return true;
        }
      }), _descriptor12 = _applyDecoratedDescriptor(_class5.prototype, "canSendBoxTipPos", [_dec13], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return true;
        }
      })), _class5)) || _class4));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=sBoxMgr.js.map
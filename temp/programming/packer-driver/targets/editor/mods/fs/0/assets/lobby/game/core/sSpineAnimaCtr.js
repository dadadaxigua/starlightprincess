System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, sp, sComponent, sObjPool, _dec, _dec2, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _temp, _crd, ccclass, property, sSpineAnimaCtr;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfsComponent(extras) {
    _reporterNs.report("sComponent", "./sComponent", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsObjPool(extras) {
    _reporterNs.report("sObjPool", "./sObjPool", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      sp = _cc.sp;
    }, function (_unresolved_2) {
      sComponent = _unresolved_2.sComponent;
    }, function (_unresolved_3) {
      sObjPool = _unresolved_3.sObjPool;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "33fb2vzTvFBIZ8GWMmemjuY", "sSpineAnimaCtr", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("sSpineAnimaCtr", sSpineAnimaCtr = (_dec = ccclass('sSpineAnimaCtr'), _dec2 = property(sp.Skeleton), _dec(_class = (_class2 = (_temp = class sSpineAnimaCtr extends (_crd && sComponent === void 0 ? (_reportPossibleCrUseOfsComponent({
        error: Error()
      }), sComponent) : sComponent) {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "targetSpine", _descriptor, this);

          _initializerDefineProperty(this, "animaName", _descriptor2, this);

          _initializerDefineProperty(this, "autoPlay", _descriptor3, this);

          _initializerDefineProperty(this, "autoRecycle", _descriptor4, this);
        }

        start() {// this.scheduleOnce(()=>{
          //     this.playSpine();
          // },4);
        }

        onEnable() {
          if (this.autoPlay) {
            this.playSpine();
          }
        }

        playSpine(_animaName = null) {
          let animationName = '';

          if (_animaName) {
            animationName = _animaName;
          } else {
            animationName = this.animaName;
          }

          if (this.targetSpine) {
            this.targetSpine.node.active = true;
            this.targetSpine.animation = animationName;
            this.targetSpine.setCompleteListener(() => {
              if (this.node.active && this.node.isValid) {
                if (this.autoRecycle) {
                  // console.log('autoRecycle spine : ' + this.node.name);
                  (_crd && sObjPool === void 0 ? (_reportPossibleCrUseOfsObjPool({
                    error: Error()
                  }), sObjPool) : sObjPool).Enqueue(this.node.name, this.node);
                  this.node.active = false;
                }
              }
            });
          }
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "targetSpine", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "animaName", [property], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return 'animation';
        }
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "autoPlay", [property], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return true;
        }
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "autoRecycle", [property], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return true;
        }
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=sSpineAnimaCtr.js.map
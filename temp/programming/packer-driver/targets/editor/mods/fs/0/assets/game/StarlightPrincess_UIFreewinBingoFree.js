System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, director, Label, Node, sp, tween, UIOpacity, v3, sComponent, sAudioMgr, _dec, _dec2, _dec3, _dec4, _dec5, _dec6, _dec7, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _descriptor5, _descriptor6, _temp, _crd, ccclass, property, StarlightPrincess_UIFreewinBingoFree;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfsComponent(extras) {
    _reporterNs.report("sComponent", "../lobby/game/core/sComponent", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsAudioMgr(extras) {
    _reporterNs.report("sAudioMgr", "../lobby/game/core/sAudioMgr", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      director = _cc.director;
      Label = _cc.Label;
      Node = _cc.Node;
      sp = _cc.sp;
      tween = _cc.tween;
      UIOpacity = _cc.UIOpacity;
      v3 = _cc.v3;
    }, function (_unresolved_2) {
      sComponent = _unresolved_2.sComponent;
    }, function (_unresolved_3) {
      sAudioMgr = _unresolved_3.default;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "7a980gCgadBD73ei2nAzeFr", "StarlightPrincess_UIFreewinBingoFree", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("StarlightPrincess_UIFreewinBingoFree", StarlightPrincess_UIFreewinBingoFree = (_dec = ccclass('StarlightPrincess_UIFreewinBingoFree'), _dec2 = property(Node), _dec3 = property(Label), _dec4 = property(Node), _dec5 = property(sp.Skeleton), _dec6 = property(sp.Skeleton), _dec7 = property(Node), _dec(_class = (_class2 = (_temp = class StarlightPrincess_UIFreewinBingoFree extends (_crd && sComponent === void 0 ? (_reportPossibleCrUseOfsComponent({
        error: Error()
      }), sComponent) : sComponent) {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "freeBtn", _descriptor, this);

          _initializerDefineProperty(this, "numLabel", _descriptor2, this);

          _initializerDefineProperty(this, "contentNode", _descriptor3, this);

          _initializerDefineProperty(this, "popSpine", _descriptor4, this);

          _initializerDefineProperty(this, "thunderSpine", _descriptor5, this);

          _initializerDefineProperty(this, "LabelNode", _descriptor6, this);

          _defineProperty(this, "touchEnable", true);
        }

        start() {
          this.freeBtn.on('click', () => {
            if (this.touchEnable) {
              this.unscheduleAllCallbacks();
              this.cleanTweenList();
              this.touchEnable = false; //this.popSpine.timeScale = 2.5;

              this.popSpine.setAnimation(0, 'stpr_banner_out', false); //this.scheduleOnce(()=>{

              this.LabelNode.active = true;

              for (let nNode of this.LabelNode.children) {
                nNode.getComponent(UIOpacity).opacity = 255;
                this.pushIDTween(tween(nNode.getComponent(UIOpacity)).to(0.5, {
                  opacity: 0
                }).start(), 'LabelNode1');
              }

              ; //},0.2)

              this.popSpine.setCompleteListener(() => {
                director.emit('bingoFreewinInFreeEnd');
                this.contentNode.active = false;
                this.touchEnable = true; //this.popSpine.setCompleteListener(()=>{

                this.popSpine.node.active = false; //});
                // this.scheduleOnce(()=>{
                //     director.emit('bingoFreewinInFree',15);
                // },2)
              });
            }
          }, this);
          director.on('bingoFreewinInFree', count => {
            this.contentNode.active = true; //sAudioMgr.PlayShotAudio('freegame_frame_open',1);

            this.popSpine.node.active = true;
            this.popSpine.setAnimation(0, 'stpr_banner_in', false);
            this.popSpine.setCompleteListener(() => {
              this.popSpine.timeScale = 1;
              this.popSpine.setAnimation(0, 'stpr_banner_loop', true);
            });
            this.LabelNode.active = true;
            let x = 0;

            for (let nNode of this.LabelNode.children) {
              nNode.getComponent(UIOpacity).opacity = 0;
              this.pushIDTween(tween(nNode.getComponent(UIOpacity)).delay(0.3).call(() => {
                x++;

                if (x == 1) {
                  (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                    error: Error()
                  }), sAudioMgr) : sAudioMgr).PlayShotAudio('freegame_frame_open', 0.8);
                }

                ;
              }).delay(0.5).to(0.2, {
                opacity: 255
              }).start(), 'LabelNode1');
              this.pushIDTween(tween(nNode).delay(0.8).to(0.2, {
                scale: v3(1.5, 1.5, 1.5)
              }).to(0.2, {
                scale: v3(1, 1, 1)
              }).start(), 'LabelNode2');
            }

            ;
            (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
              error: Error()
            }), sAudioMgr) : sAudioMgr).PlayAudio('fsinout_bgm');
            this.setLabelNum(count);
            this.node.setSiblingIndex(this.node.parent.children.length - 1);
            this.unscheduleAllCallbacks();
            this.cleanTweenList();
            this.scheduleOnce(() => {
              this.freeBtn.emit('click');
            }, 3);
          }, this); // this.scheduleOnce(()=>{
          //     director.emit('bingoFreewinInFree',15);
          // },3)
          //
        }

        onEnable() {}

        setLabelNum(num) {
          this.numLabel.string = num.toString();
        }

        playThunderAnima() {
          if (this.thunderSpine) {
            this.thunderSpine.node.active = true; //sAudioMgr.PlayAudio('freegame_transitions');

            this.thunderSpine.setAnimation(0, 'stpr_transition', false);
            this.thunderSpine.setCompleteListener(() => {
              this.thunderSpine.node.active = false;
            });
          }
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "freeBtn", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "numLabel", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "contentNode", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "popSpine", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor5 = _applyDecoratedDescriptor(_class2.prototype, "thunderSpine", [_dec6], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor6 = _applyDecoratedDescriptor(_class2.prototype, "LabelNode", [_dec7], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=StarlightPrincess_UIFreewinBingoFree.js.map
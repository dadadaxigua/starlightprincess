System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2", "__unresolved_3", "__unresolved_4", "__unresolved_5"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, sBoxEntity, sObjPool, sBoxAssetInit, sAudioMgr, StarlightPrincess_Thunder, BoxObj, _dec, _class, _temp, _crd, ccclass, property, StarlightPrincess_Entity;

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _reportPossibleCrUseOfsBoxEntity(extras) {
    _reporterNs.report("sBoxEntity", "../lobby/game/core/sBoxEntity", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsBoxItemBase(extras) {
    _reporterNs.report("sBoxItemBase", "../lobby/game/core/sBoxItemBase", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsObjPool(extras) {
    _reporterNs.report("sObjPool", "../lobby/game/core/sObjPool", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsBoxAssetInit(extras) {
    _reporterNs.report("sBoxAssetInit", "../lobby/game/core/sBoxAssetInit", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsAudioMgr(extras) {
    _reporterNs.report("sAudioMgr", "../lobby/game/core/sAudioMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfStarlightPrincess_Thunder(extras) {
    _reporterNs.report("StarlightPrincess_Thunder", "./StarlightPrincess_Thunder", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
    }, function (_unresolved_2) {
      sBoxEntity = _unresolved_2.sBoxEntity;
    }, function (_unresolved_3) {
      sObjPool = _unresolved_3.sObjPool;
    }, function (_unresolved_4) {
      sBoxAssetInit = _unresolved_4.sBoxAssetInit;
    }, function (_unresolved_5) {
      sAudioMgr = _unresolved_5.default;
    }, function (_unresolved_6) {
      StarlightPrincess_Thunder = _unresolved_6.StarlightPrincess_Thunder;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "209bdCMyGVMkpMNV3gSLDK2", "StarlightPrincess_Entity", undefined);

      ({
        ccclass,
        property
      } = _decorator);
      BoxObj = class BoxObj {
        constructor() {
          _defineProperty(this, "box", void 0);

          _defineProperty(this, "overEffect", void 0);
        }

      };

      _export("StarlightPrincess_Entity", StarlightPrincess_Entity = (_dec = ccclass('StarlightPrincess_Entity'), _dec(_class = (_temp = class StarlightPrincess_Entity extends (_crd && sBoxEntity === void 0 ? (_reportPossibleCrUseOfsBoxEntity({
        error: Error()
      }), sBoxEntity) : sBoxEntity) {
        constructor(...args) {
          super(...args);

          _defineProperty(this, "boxObj", new BoxObj());

          _defineProperty(this, "thundered", false);
        }

        //是否可以消除该盒子
        get IsDisappearBox() {
          if (this.boxStateNow == 'win' && this.SymbolValue != 1 && this.SymbolValue < 2000) {
            return true;
          }

          return false;
        }

        EntityInit() {
          super.EntityInit();
          this.RegisterEnitityAction('thunder', () => {
            //if(!this.thundered){
            //console.log('格子位置：',this.viewItemIndex.x,this.viewItemIndex.y);
            //if(this.viewItemIndex.y >= 0 && this.viewItemIndex.y <= 4){
            //this.thundered = true;
            if (this.BoxStateNow != 'idle') {
              this.UpdateData('idle', 'settlement');
            }

            let spineAnimaName = 'gool_summon_effect_1';
            const rSymbol = this.symbolValue;

            if (rSymbol > 2000 && rSymbol < 2007) {
              spineAnimaName = 'gool_summon_effect_1';
            } else if (rSymbol >= 2007 && rSymbol < 2011) {
              spineAnimaName = 'gool_summon_effect_2';
            } else if (rSymbol >= 2011 && rSymbol < 2014) {
              spineAnimaName = 'gool_summon_effect_3';
            } else {
              spineAnimaName = 'gool_summon_effect_4';
            }

            const pos = this.GetBoxWorldPosition();
            pos.x += 80;
            pos.y += 290;
            const thunder = (_crd && sObjPool === void 0 ? (_reportPossibleCrUseOfsObjPool({
              error: Error()
            }), sObjPool) : sObjPool).Dequeue('thunder');

            if (thunder && thunder.isValid) {
              thunder.parent = (_crd && sBoxAssetInit === void 0 ? (_reportPossibleCrUseOfsBoxAssetInit({
                error: Error()
              }), sBoxAssetInit) : sBoxAssetInit).instance.getTargetNodeByName('effectLayer');
              thunder.worldPosition = pos;
              thunder.active = true;
              (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                error: Error()
              }), sAudioMgr) : sAudioMgr).PlayShotAudio('mult_appear');
              thunder.getComponent(_crd && StarlightPrincess_Thunder === void 0 ? (_reportPossibleCrUseOfStarlightPrincess_Thunder({
                error: Error()
              }), StarlightPrincess_Thunder) : StarlightPrincess_Thunder).play(spineAnimaName);
            } //}
            //}

          });
        } // protected JackpotBingo(){
        //     if(this.IsJackpotBox){
        //         this.UpdateData('win','settlement');
        //     }
        // }


        UpdateBoxPosition(pos) {
          super.UpdateBoxPosition(pos);
          this.boxObj.box.node.position = pos;
          this.boxObj.overEffect.node.position = pos;
        }

        UpdateData(boxState, tableState) {
          this.boxStateNow = boxState;
          this.boxData = this.getSymbolData(this.SymbolValue);

          if (this.boxData) {
            const renderData = this.boxData[boxState]; // if(this.SymbolValue == 88 && boxState == 'idle'){
            //     if(this.ViewItemIndex.y >= 0 && this.ViewItemIndex.y <= 4){
            //         this.boxObj.box.boxItemUpdate(renderData,this.SymbolValue,boxState,tableState);
            //     }
            // }else{
            //     this.boxObj.box.boxItemUpdate(renderData,this.SymbolValue,boxState,tableState);
            // }

            if (this.symbolValue == 1 && this.ViewItemIndex.y >= 0 && this.ViewItemIndex.y <= 4) {
              this.boxObj.box.clearItem(this.SymbolValue, boxState, tableState);
              this.boxObj.box.node.active = false;
              this.boxObj.overEffect.node.active = true;
              this.boxObj.overEffect.boxItemUpdate(renderData, this.SymbolValue, boxState, tableState);
            } else {
              this.boxObj.overEffect.clearItem(this.SymbolValue, boxState, tableState);
              this.boxObj.overEffect.node.active = false;
              this.boxObj.box.boxItemUpdate(renderData, this.SymbolValue, boxState, tableState);
              this.boxObj.box.node.active = true;
            }

            ; // if(boxState == 'win' && this.symbolValue <=2000 && this.symbolValue != 1){
            //     this.boxObj.overEffect.node.position = this.boxPos;
            //     this.boxObj.overEffect.node.active = true;
            //     this.boxObj.overEffect.boxItemUpdate(renderData,this.SymbolValue,boxState,tableState);
            // }

            let pstr = this.SymbolValue.toString();
            pstr = pstr.slice(2);

            if (boxState == 'idle' && this.symbolValue == 1) {
              if (this.ViewItemIndex.y >= 0 && this.ViewItemIndex.y <= 4) {
                (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                  error: Error()
                }), sAudioMgr) : sAudioMgr).PlayShotAudio('scatter_appear');
              }

              ;
            } else if (boxState == 'idle' && this.symbolValue > 2000) {
              if (this.ViewItemIndex.y >= 0 && this.ViewItemIndex.y <= 4) {
                let str = 'scatter_appear_voice' + parseInt(pstr);
                (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                  error: Error()
                }), sAudioMgr) : sAudioMgr).PlayShotAudio(str);
              }

              ;
            }

            ;
          }

          ;
        }

        ClearItem(boxState, tableState) {
          super.ClearItem(boxState, tableState);
          this.thundered = false;

          if (this.boxObj.box) {
            this.boxObj.box.clearItem(null, boxState, tableState);
          }

          if (this.boxObj.overEffect) {
            this.boxObj.overEffect.node.active = false;
            this.boxObj.overEffect.clearItem(null, boxState, tableState);
          }
        }

      }, _temp)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=StarlightPrincess_Entity.js.map
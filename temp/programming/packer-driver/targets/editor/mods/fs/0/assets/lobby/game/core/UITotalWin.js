System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2", "__unresolved_3"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, Node, Label, tween, v3, Vec3, director, sAudioMgr, sUtil, UIBase, _dec, _dec2, _dec3, _dec4, _class, _class2, _descriptor, _descriptor2, _descriptor3, _temp, _crd, ccclass, property, UITotalWin;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfsAudioMgr(extras) {
    _reporterNs.report("sAudioMgr", "./sAudioMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsUtil(extras) {
    _reporterNs.report("sUtil", "./sUtil", _context.meta, extras);
  }

  function _reportPossibleCrUseOfUIBase(extras) {
    _reporterNs.report("UIBase", "./UIBase", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      Node = _cc.Node;
      Label = _cc.Label;
      tween = _cc.tween;
      v3 = _cc.v3;
      Vec3 = _cc.Vec3;
      director = _cc.director;
    }, function (_unresolved_2) {
      sAudioMgr = _unresolved_2.default;
    }, function (_unresolved_3) {
      sUtil = _unresolved_3.sUtil;
    }, function (_unresolved_4) {
      UIBase = _unresolved_4.UIBase;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "6196c8GJBhKQ6Qx46pENcyx", "UITotalWin", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("UITotalWin", UITotalWin = (_dec = ccclass('UITotalWin'), _dec2 = property(Label), _dec3 = property(Node), _dec4 = property(Label), _dec(_class = (_class2 = (_temp = class UITotalWin extends (_crd && UIBase === void 0 ? (_reportPossibleCrUseOfUIBase({
        error: Error()
      }), UIBase) : UIBase) {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "totalLabel", _descriptor, this);

          _initializerDefineProperty(this, "btnNode", _descriptor2, this);

          _initializerDefineProperty(this, "freeCountLabel", _descriptor3, this);

          _defineProperty(this, "endData", null);

          _defineProperty(this, "playing", false);

          _defineProperty(this, "btnCall", null);
        }

        start() {
          this.node.getChildByName('bg').on('click', () => {
            if (this.playing) {
              this.playing = false;

              if (this.totalLabel && this.totalLabel.node && this.totalLabel.node.isValid && this.totalLabel.node['rollAnimaTween']) {
                this.totalLabel.node['rollAnimaTween'].stop();
                this.totalLabel.string = (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                  error: Error()
                }), sUtil) : sUtil).AddCommas(Math.floor(this.endData.coin));
                this.unschedule(this.btnCall);

                if (this.btnCall) {
                  this.btnCall();
                }
              }
            }
          }, this);
          this.btnNode.on('click', () => {
            (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
              error: Error()
            }), sAudioMgr) : sAudioMgr).PlayBG('bgm_mg', true);
            director.emit('freeWinOver');
            director.emit('rollStop');
            (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
              error: Error()
            }), sAudioMgr) : sAudioMgr).PlayShotAudio('freeWinBtnClick');
            this.node.destroy();
          }, this);
        }

        onEnable() {
          this.scheduleOnce(() => {
            if (this.btnNode && this.btnNode.isValid) {
              this.btnNode.emit('click');
            }
          }, 8);
          this.btnNode.active = false;
          this.btnNode.scale = Vec3.ZERO;
          (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
            error: Error()
          }), sAudioMgr) : sAudioMgr).PlayShotAudio('totalWin');

          this.btnCall = () => {
            (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
              error: Error()
            }), sAudioMgr) : sAudioMgr).PlayShotAudio('cheer');
            this.btnNode.active = true;
            tween(this.btnNode).to(0.3, {
              scale: v3(1.8, 1.8, 1)
            }, {
              easing: 'bounceOut'
            }).start();
          };

          this.scheduleOnce(this.btnCall, 3.24);
        }

        DataInit(value) {
          this.playing = true;
          this.endData = value; // const valueOri = label.node['rollAnimaValue'];
          // const mTween = label.node['rollAnimaTween'];
          // if(mTween){
          //     mTween.stop();
          // }

          this.freeCountLabel.string = value.count;
          let tweenTargetVec3 = v3(0, 0, 0); // console.log('tweenTargetVec3:'+tweenTargetVec3);

          this.totalLabel.node['rollAnimaTween'] = tween(tweenTargetVec3).to(3, v3(value.coin, value.coin, value.coin), {
            "onUpdate": target => {
              if (this.totalLabel) {
                this.totalLabel.string = (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                  error: Error()
                }), sUtil) : sUtil).AddCommas(Math.floor(target.x));
              }
            },
            easing: 'quadOut'
          }).call(() => {
            this.playing = false;

            if (this.totalLabel) {
              this.totalLabel.string = (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                error: Error()
              }), sUtil) : sUtil).AddCommas(Math.floor(value.coin));
            }

            this.btnNode.active = true;
          }).start();
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "totalLabel", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "btnNode", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "freeCountLabel", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=UITotalWin.js.map
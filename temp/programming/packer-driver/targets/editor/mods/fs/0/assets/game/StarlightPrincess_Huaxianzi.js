System.register(["cc"], function (_export, _context) {
  "use strict";

  var _cclegacy, _decorator, Component, director, Node, sp, _dec, _dec2, _class, _class2, _descriptor, _temp, _crd, ccclass, property, StarlightPrincess_Huaxianzi;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  return {
    setters: [function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      Component = _cc.Component;
      director = _cc.director;
      Node = _cc.Node;
      sp = _cc.sp;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "9b12a4K96BKX6od9AGgOJxE", "StarlightPrincess_Huaxianzi", undefined);

      ({
        ccclass,
        property
      } = _decorator);
      /**
       * Predefined variables
       * Name = StarlightPrincess_Huaxianzi
       * DateTime = Thu Sep 21 2023 13:46:33 GMT+0800 (中国标准时间)
       * Author = dadadaxigua
       * FileBasename = StarlightPrincess_Huaxianzi.ts
       * FileBasenameNoExtension = StarlightPrincess_Huaxianzi
       * URL = db://assets/game/StarlightPrincess_Huaxianzi.ts
       * ManualUrl = https://docs.cocos.com/creator/3.4/manual/zh/
       *
       */

      _export("StarlightPrincess_Huaxianzi", StarlightPrincess_Huaxianzi = (_dec = ccclass('StarlightPrincess_Huaxianzi'), _dec2 = property(Node), _dec(_class = (_class2 = (_temp = class StarlightPrincess_Huaxianzi extends Component {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "spawnAnima", _descriptor, this);

          _defineProperty(this, "arr1", ["stpr_character_01", "stpr_character_action"]);

          _defineProperty(this, "arr2", ["stpr_character_02"]);

          _defineProperty(this, "arr3", ["stpr_character_03_win"]);
        }

        start() {
          director.on('controlHuaXianZi', str => {
            if (str == 'NormalAction') {
              this.node.children[3].active = true;
              this.node.children[3].getComponent(sp.Skeleton).setAnimation(0, "stpr_character_ol_fx", false);
              this.node.children[3].getComponent(sp.Skeleton).setCompleteListener(() => {
                this.node.children[3].active = false;
              });
              this.node.children[0].active = true;
              this.node.children[1].active = false;
              this.node.children[2].active = false;
              this.scheduleOnce(() => {
                let strs = this.node.children[0].getComponent(sp.Skeleton).animation;
                let num = 1;

                if (strs == "stpr_character_action") {
                  num = 0;
                }

                ;
                this.node.children[0].getComponent(sp.Skeleton).animation = this.arr1[num];
                this.node.children[0].active = true;
                this.node.children[1].active = false;
                this.node.children[2].active = false;
              }, 0.2);
            } else if (str == 'NormalIdle') {
              this.scheduleOnce(() => {
                this.node.children[3].active = false;
                this.node.children[0].active = true;
                this.node.children[1].active = false;
                this.node.children[3].active = true;
                this.node.children[3].getComponent(sp.Skeleton).setAnimation(0, "stpr_character_ol_fx", false);
                this.node.children[3].getComponent(sp.Skeleton).setCompleteListener(() => {
                  this.node.children[3].active = false;
                });
                this.scheduleOnce(() => {
                  this.node.children[0].active = false;
                  this.node.children[2].active = true;
                  this.node.children[2].getComponent(sp.Skeleton).setAnimation(0, this.arr3[0], true);
                }, 0.25);
                this.scheduleOnce(() => {
                  this.node.children[3].active = true;
                  this.node.children[3].getComponent(sp.Skeleton).setAnimation(0, "stpr_character_ol_fx", false);
                  this.node.children[3].getComponent(sp.Skeleton).setCompleteListener(() => {
                    this.node.children[3].active = false;
                  });
                  this.scheduleOnce(() => {
                    this.node.children[1].active = false;
                    this.node.children[2].active = false;
                    this.node.children[0].active = true;
                    this.node.children[0].getComponent(sp.Skeleton).setAnimation(0.1, "stpr_character_01", true);
                  }, 0.25);
                }, 1);
              }, 0.3);
            } else if (str == 'FreeSpin') {
              this.node.children[1].getComponent(sp.Skeleton).animation = this.arr2[0];
              this.node.children[0].active = false;
              this.node.children[1].active = false;
              this.node.children[2].active = false;
              this.node.children[3].active = true;
              this.node.children[3].getComponent(sp.Skeleton).setCompleteListener(() => {
                this.node.children[3].active = false;
              });
              this.node.children[1].active = true;
              this.node.children[0].active = false;
              this.node.children[2].active = false;
            }

            ;
            ;
          }, this);
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "spawnAnima", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class));

      ;

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=StarlightPrincess_Huaxianzi.js.map
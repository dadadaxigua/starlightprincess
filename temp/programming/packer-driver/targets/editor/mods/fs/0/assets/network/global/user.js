System.register(["cc"], function (_export, _context) {
  "use strict";

  var _cclegacy, sys, _crd, info;

  return {
    setters: [function (_cc) {
      _cclegacy = _cc.cclegacy;
      sys = _cc.sys;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "80877sFx2JCe5y6e/xZBKzz", "user", undefined);

      /**
       * 注意：已把原脚本注释，由于脚本变动过大，转换的时候可能有遗落，需要自行手动转换
       */
      info = {
        loginInfo: {
          ID: '',
          pwd: null,
          md5Encry: true,
          src: 0,
          uid: null
        },
        //当前的登录信息
        myInfo: {
          coin: 0,
          nick_name: '',
          token: null,
          //用户凭证
          uid: '0',
          //用户id
          uuid: '0',
          binded: 0,
          //是否绑定手机号
          formal: false,
          mobile: null,
          //手机号
          nickname: '',
          //用户昵称
          nicknameChangedTimes: 0,
          //用户改名次数
          avatar: 0,
          //玩家头像
          artilleryId: 1,
          //炮台id
          avatarBorder: 0,
          //玩家头像框
          vip_level: 0,
          //玩家vip等级
          vip_exp: 0,
          //玩家vip经验
          coupon: 0,
          //玩家礼券数量
          gold: 5000,
          //玩家金币数量
          strongbox: {
            "balance": 0
          },
          //保险箱
          totalOnline: 0,
          //玩家在线时长
          alipay: {},
          //玩家支付宝绑定账号
          lineState: 1,
          //状态
          manifesto: null,
          //个性宣言
          appstorePurchase: '',
          //App Store审核版本
          isMessage: false,
          bindInfo: {},
          authname: false,
          role: 0,
          total_spins: 0,
          total_win: 0,
          max_spins_win: 0,
          unreadAmount: 0,
          user_name: '',
          phone_num: '',
          max_bet: 0,
          income: 0,
          max_rate: 0,
          slots_count: 0,
          relation_types: ''
        },
        //我的用户信息
        unreadAmount: 0,
        mySeatIndex: 0,
        //当前的座位号
        usericonPath: 'images/Head_Picture/Head_Picture-',
        //头像路径前缀
        iconframePath: 'images/Head-frame/Head-frame-',
        //头像框路径前缀
        deviceId: null,
        //机器码
        totalPath: '',
        //通用信息总路径,
        //获取用户信息本地缓存
        getStorage: function (itemName) {
          let localItem = sys.localStorage.getItem(itemName);

          if (localItem && localItem != 'undefined') {
            return JSON.parse(localItem);
          } else {
            return null;
          }
        },
        //设置本地缓存
        setStorage: function (itemName, itemData) {
          //return cc.sys.localStorage.getItem(itemName);
          sys.localStorage.setItem(itemName, JSON.stringify(itemData));
        },
        //本地缓存登录列表数据置顶
        setItemToFirst: function (itemData) {
          var loginList = this.getStorage('IDList');

          if (loginList) {
            var itemIndex = this.getObjIndex(loginList, itemData);

            if (itemIndex == null) {
              loginList.unshift(itemData);
            } else {
              loginList.splice(itemIndex, 1);
              loginList.unshift(itemData);
            }

            if (loginList.length == 4) {
              loginList.pop();
            }

            this.setStorage('IDList', loginList);
          } else {
            loginList = [itemData];
            this.setStorage('IDList', loginList);
          }
        },
        //删除本地登录列表中的信息
        deleteStorageDate: function (itemData) {
          var loginList = this.getStorage('IDList');
          var itemIndex = this.getObjIndex(loginList, itemData);
          loginList.splice(itemIndex, 1);
          this.setStorage('IDList', loginList);
        },

        //获取索引
        getObjIndex(loginList, itemData) {
          for (let i = 0; i < loginList.length; i++) {
            if (loginList[i].uid == itemData.uid) {
              return i;
            }
          }

          return null;
        },

        //清空本地登录缓存
        clearStorageDate() {
          var loginList = this.getStorage('IDList');
          loginList = [];
          this.setStorage('IDList', loginList);
        },

        //清空游客账号缓存
        clearVistorDate(loginList) {
          if (!loginList || loginList.length == 0) {
            return;
          }

          for (let i = 0; i < loginList.length; i++) {
            if (loginList[i].src != 2) {
              loginList.splice(i, 1);
              this.clearVistorDate(loginList);
              return;
            }
          }

          this.setStorage('IDList', loginList);
        }

      };
      globalThis.__Info = info;

      _export("default", info);

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=user.js.map
System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2", "__unresolved_3"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, Node, Sprite, tween, v3, Vec3, Color, assetMgr, sComponent, sObjPool, _dec, _dec2, _dec3, _dec4, _dec5, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _temp, _crd, ccclass, property, sNewPokerCardBase;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfassetMgr(extras) {
    _reporterNs.report("assetMgr", "./sAssetMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsComponent(extras) {
    _reporterNs.report("sComponent", "./sComponent", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsObjPool(extras) {
    _reporterNs.report("sObjPool", "./sObjPool", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      Node = _cc.Node;
      Sprite = _cc.Sprite;
      tween = _cc.tween;
      v3 = _cc.v3;
      Vec3 = _cc.Vec3;
      Color = _cc.Color;
    }, function (_unresolved_2) {
      assetMgr = _unresolved_2.assetMgr;
    }, function (_unresolved_3) {
      sComponent = _unresolved_3.sComponent;
    }, function (_unresolved_4) {
      sObjPool = _unresolved_4.sObjPool;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "a3785aj4UlGYYaI4bNB9ssY", "sNewPokerCardBase", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("sNewPokerCardBase", sNewPokerCardBase = (_dec = ccclass('sNewPokerCardBase'), _dec2 = property(Node), _dec3 = property(Node), _dec4 = property(Sprite), _dec5 = property(Sprite), _dec(_class = (_class2 = (_temp = class sNewPokerCardBase extends (_crd && sComponent === void 0 ? (_reportPossibleCrUseOfsComponent({
        error: Error()
      }), sComponent) : sComponent) {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "foreNode", _descriptor, this);

          _initializerDefineProperty(this, "backNode", _descriptor2, this);

          _initializerDefineProperty(this, "foreRender", _descriptor3, this);

          _initializerDefineProperty(this, "backRender", _descriptor4, this);

          _defineProperty(this, "cardPoint", void 0);

          _defineProperty(this, "state", 'open');

          _defineProperty(this, "viewForm", 'none');
        }

        get CardPoint() {
          return this.cardPoint;
        }

        //open inHand select out close
        get State() {
          return this.state;
        }

        set State(value) {
          this.state = value;
        }

        //fore正面,back背面,none不显示
        onLoad() {}

        start() {}
        /**
         * @zh
         * 更新牌的点数和图片
         * @param point - ex : 101,204,404
         */


        updatePoint(point) {
          this.cardPoint = point;

          if (this.foreRender) {
            const cardSprite = (_crd && assetMgr === void 0 ? (_reportPossibleCrUseOfassetMgr({
              error: Error()
            }), assetMgr) : assetMgr).GetAssetByName('icCard_' + point);

            if (cardSprite) {
              this.foreRender.spriteFrame = cardSprite;
            }
          }
        }
        /**
         * @zh
         * 让牌飞到目标位置
         * @param targetPos - 目标位置
         * @param time - 动画时间
         * @param onCompleteCall - 动画完成后的回调
         * @param ease - 动画曲线
         */


        flyCard(targetPos, time, onCompleteCall = null, ease = 'sineOut') {
          if (time > 0) {
            this.removeIDTween('flyCard');
            this.pushIDTween(tween(this.node).to(time, {
              position: targetPos
            }, {
              easing: ease,
              'onComplete': onCompleteCall
            }).start(), 'flyCard');
          }
        }
        /**
         * @zh
         * 让牌飞到目标位置
         * @param point - ex : 101,204,404
         */


        scaleCard(targetScale, time, onCompleteCall = null, ease = 'sineOut') {
          if (time > 0) {
            this.removeIDTween('scaleCard');
            this.pushIDTween(tween(this.node).to(time, {
              scale: targetScale
            }, {
              easing: ease,
              'onComplete': onCompleteCall
            }).start(), 'scaleCard');
          }
        }
        /**
         * @zh
         * 打开牌的正面
         * @param animaType - none,flip
         * @param delayTime - 延迟动画播放时间
         */


        openForeCard(animaType = 'none', delayTime = 0, startCall = null) {
          this.cleanTweenList();

          if (this.viewForm != 'fore') {
            this.viewForm = 'fore';
            this.backNode.scale = Vec3.ONE;

            if (animaType == 'flip') {
              this.pushOneTween(tween(this.backNode).delay(delayTime).call(startCall).to(0.1, {
                scale: v3(0, 1, 1)
              }, {
                easing: 'sineIn'
              }).call(() => {
                this.backNode.active = false;
                this.foreNode.active = true;
                this.foreNode.scale = v3(0, 1, 1);
                this.pushOneTween(tween(this.foreNode).to(0.1, {
                  scale: Vec3.ONE
                }, {
                  easing: 'sineOut'
                }).start());
              }).start());
            } else if (animaType == 'none') {
              this.foreNode.scale = Vec3.ONE;
              this.backNode.active = false;
              this.foreNode.active = true;
            }
          }
        }
        /**
         * @zh
         * 打开牌的背面
         * @param animaType - none,flip
         * @param delayTime - 延迟动画播放时间
         */


        openBackCard(animaType = 'none', delayTime = 0) {
          this.cleanTweenList();

          if (this.viewForm != 'back') {
            this.viewForm = 'back';
            this.foreNode.scale = Vec3.ONE;

            if (animaType == 'flip') {
              this.pushOneTween(tween(this.foreNode).delay(delayTime).to(0.1, {
                scale: v3(0, 1, 1)
              }, {
                easing: 'sineIn'
              }).call(() => {
                this.foreNode.active = false;
                this.backNode.active = true;
                this.backNode.scale = v3(0, 1, 1);
                this.pushOneTween(tween(this.backNode).to(0.1, {
                  scale: Vec3.ONE
                }, {
                  easing: 'sineOut'
                }).start());
              }).start());
            } else if (animaType == 'none') {
              this.backNode.scale = Vec3.ONE;
              this.backNode.active = true;
              this.foreNode.active = false;
            }
          }
        }
        /**
         * @zh
         * 设置牌正面的颜色叠加值
         * @param _color - 叠加的颜色
         */


        setRenderColor(_color) {
          if (this.foreRender) {
            this.foreRender.color = _color;
          }

          if (this.backRender) {
            this.backRender.color = _color;
          }
        }
        /**
         * @zh
         * 设置牌的世界坐标
         */


        setWorldPosition(pos) {
          this.node.worldPosition = pos;
        }
        /**
         * @zh
         * 设置牌的相对坐标
         */


        setLocalPosition(pos) {
          this.node.position = pos;
        }
        /**
         * @zh
         * 牌入对象池
         */


        recycleCard() {
          this.unscheduleAllCallbacks();
          this.cleanTweenList();
          this.cleanTweenDic();
          this.node.parent = null;
          this.CtrAllChildActive(this.node, false);
          this.State = 'close';
          this.viewForm = 'none';
          this.node.scale = Vec3.ONE;
          this.node.eulerAngles = Vec3.ZERO;
          this.cardPoint = '0';
          this.backNode.scale = Vec3.ONE;
          this.foreNode.scale = Vec3.ONE;

          if (this.foreRender) {
            this.foreRender.color = Color.WHITE;
          }

          if (this.backRender) {
            this.backRender.color = Color.WHITE;
          }

          this.node.active = false;
          (_crd && sObjPool === void 0 ? (_reportPossibleCrUseOfsObjPool({
            error: Error()
          }), sObjPool) : sObjPool).Enqueue('pokerCard', this.node);
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "foreNode", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "backNode", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "foreRender", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "backRender", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=sNewPokerCardBase.js.map
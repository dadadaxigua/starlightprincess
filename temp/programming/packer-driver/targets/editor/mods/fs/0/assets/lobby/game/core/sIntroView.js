System.register(["__unresolved_0", "cc", "__unresolved_1"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, Node, assetManager, SpriteFrame, instantiate, Sprite, Widget, v3, tween, Vec3, UITransform, sComponent, _dec, _dec2, _dec3, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _temp, _crd, ccclass, property, sIntroView;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfsComponent(extras) {
    _reporterNs.report("sComponent", "./sComponent", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      Node = _cc.Node;
      assetManager = _cc.assetManager;
      SpriteFrame = _cc.SpriteFrame;
      instantiate = _cc.instantiate;
      Sprite = _cc.Sprite;
      Widget = _cc.Widget;
      v3 = _cc.v3;
      tween = _cc.tween;
      Vec3 = _cc.Vec3;
      UITransform = _cc.UITransform;
    }, function (_unresolved_2) {
      sComponent = _unresolved_2.sComponent;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "ddc96einkZBzJ1NEVd6vArr", "sIntroView", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("sIntroView", sIntroView = (_dec = ccclass('sIntroView'), _dec2 = property(Node), _dec3 = property(Node), _dec(_class = (_class2 = (_temp = class sIntroView extends (_crd && sComponent === void 0 ? (_reportPossibleCrUseOfsComponent({
        error: Error()
      }), sComponent) : sComponent) {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "imgCopyNode", _descriptor, this);

          _initializerDefineProperty(this, "closeArea", _descriptor2, this);

          _initializerDefineProperty(this, "needAnima", _descriptor3, this);

          _initializerDefineProperty(this, "customLoadPath", _descriptor4, this);

          _defineProperty(this, "mWidth", 1000);

          _defineProperty(this, "mHeight", 600);
        }

        onLoad() {
          const _widget = this.node.getComponent(Widget);

          if (_widget) {
            _widget.updateAlignment();
          }
        }

        start() {
          const close = this.node.getChildByName('closeArea');

          if (close) {
            close.on('click', () => {
              this.node.active = false;
            }, this);
          } else {
            if (this.closeArea) {
              this.closeArea.on('click', () => {
                this.node.active = false;
              }, this);
            }
          }

          if (this.customLoadPath) {
            this.onView(this.customLoadPath + (globalThis.GetLanguageType ? globalThis.GetLanguageType() : 'EN'));
          } else {
            this.onView('intro/rule/' + (globalThis.GetLanguageType ? globalThis.GetLanguageType() : 'EN'));
          }
        }

        onEnable() {
          if (this.needAnima) {
            const mainNode = this.node.getChildByName('main');

            if (mainNode) {
              mainNode.scale = v3(0, 0, 0);
              tween(mainNode).to(0.3, {
                scale: Vec3.ONE
              }, {
                easing: 'backOut'
              }).start();
            }
          }
        }

        changeSize(width) {
          try {
            this.mWidth = width;
            const bg = this.node.getChildByPath('main/bg').getComponent(UITransform);
            bg.setContentSize(this.mWidth, this.mHeight);
            const close = this.node.getChildByPath('main/close');
            close.position = v3((this.mWidth - 100) / 2, close.position.y, 0);
            this.node.getChildByPath('main/ScrollView').getComponent(UITransform).setContentSize(this.mWidth - 10, 510);
            this.node.getChildByPath('main/view').getComponent(UITransform).setContentSize(this.mWidth - 10, 510);
            this.node.getChildByPath('main/content').getComponent(UITransform).setContentSize(this.mWidth - 10, 0);
          } catch (e) {
            console.error(e);
          }
        }

        onView(path) {
          const bundle = assetManager.getBundle(globalThis.currentPlayingGameID);

          if (bundle) {
            bundle.loadDir(path, SpriteFrame, (err, assets) => {
              // console.log(assets);
              if (assets && Array.isArray(assets) && assets.length > 0) {
                let dic = assets.sort((a, b) => {
                  let indexs1 = a.name.split('_');
                  let indexs2 = b.name.split('_');

                  if (indexs1 && indexs2) {
                    let num1 = parseInt(indexs1[indexs1.length - 1]);
                    let num2 = parseInt(indexs2[indexs2.length - 1]);

                    if (!isNaN(num1) && !isNaN(num2)) {
                      return num1 - num2;
                    }
                  }

                  return 0;
                });

                for (let i = 0; i < dic.length; i++) {
                  const asset = dic[i];
                  let img = instantiate(this.imgCopyNode);
                  img.setParent(this.imgCopyNode.parent, false);
                  img.active = true;
                  img.getComponent(Sprite).spriteFrame = asset;
                }
              }
            });
          }
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "imgCopyNode", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "closeArea", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "needAnima", [property], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return false;
        }
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "customLoadPath", [property], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return '';
        }
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=sIntroView.js.map
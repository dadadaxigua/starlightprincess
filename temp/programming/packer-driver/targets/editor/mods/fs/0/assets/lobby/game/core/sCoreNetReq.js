System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2", "__unresolved_3", "__unresolved_4", "__unresolved_5", "__unresolved_6", "__unresolved_7", "__unresolved_8"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, deskClientProtos, deskPushProtos, deskServerProtos, jackpotClientProtos, jackpotServerProtos, slotClientProtos, slotServerProtos, pushProtosSlot, sCoreNetReq, _crd;

  function isObject(item) {
    return item && typeof item === 'object' && !Array.isArray(item);
  }

  function mergeDeep(target, ...sources) {
    if (!sources.length) return target;
    const source = sources.shift();

    if (isObject(target) && isObject(source)) {
      for (const key in source) {
        if (isObject(source[key])) {
          if (!target[key]) Object.assign(target, {
            [key]: {}
          });
          mergeDeep(target[key], source[key]);
        } else {
          Object.assign(target, {
            [key]: source[key]
          });
        }
      }
    }

    return mergeDeep(target, ...sources);
  }

  function _reportPossibleCrUseOfdeskClientProtos(extras) {
    _reporterNs.report("deskClientProtos", "./protos/desk/deskClientProtos", _context.meta, extras);
  }

  function _reportPossibleCrUseOfdeskPushProtos(extras) {
    _reporterNs.report("deskPushProtos", "./protos/desk/deskPushProtos", _context.meta, extras);
  }

  function _reportPossibleCrUseOfdeskServerProtos(extras) {
    _reporterNs.report("deskServerProtos", "./protos/desk/deskServerProtos", _context.meta, extras);
  }

  function _reportPossibleCrUseOfjackpotClientProtos(extras) {
    _reporterNs.report("jackpotClientProtos", "./protos/bonus/jackpotClientProtos", _context.meta, extras);
  }

  function _reportPossibleCrUseOfjackpotServerProtos(extras) {
    _reporterNs.report("jackpotServerProtos", "./protos/bonus/jackpotServerProtos", _context.meta, extras);
  }

  function _reportPossibleCrUseOfslotClientProtos(extras) {
    _reporterNs.report("slotClientProtos", "./protos/slot/slotClientProtos", _context.meta, extras);
  }

  function _reportPossibleCrUseOfslotServerProtos(extras) {
    _reporterNs.report("slotServerProtos", "./protos/slot/slotServerProtos", _context.meta, extras);
  }

  function _reportPossibleCrUseOfpushProtosSlot(extras) {
    _reporterNs.report("pushProtosSlot", "./protos/slot/pushProtos.slot", _context.meta, extras);
  }

  _export("sCoreNetReq", void 0);

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
    }, function (_unresolved_2) {
      deskClientProtos = _unresolved_2.default;
    }, function (_unresolved_3) {
      deskPushProtos = _unresolved_3.default;
    }, function (_unresolved_4) {
      deskServerProtos = _unresolved_4.default;
    }, function (_unresolved_5) {
      jackpotClientProtos = _unresolved_5.default;
    }, function (_unresolved_6) {
      jackpotServerProtos = _unresolved_6.default;
    }, function (_unresolved_7) {
      slotClientProtos = _unresolved_7.default;
    }, function (_unresolved_8) {
      slotServerProtos = _unresolved_8.default;
    }, function (_unresolved_9) {
      pushProtosSlot = _unresolved_9.default;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "fd5784eVuhJwKGaTvcaJodo", "sCoreNetReq", undefined);

      _export("sCoreNetReq", sCoreNetReq = class sCoreNetReq {});

      globalThis._clientProtos = mergeDeep(globalThis._clientProtos || {}, _crd && deskClientProtos === void 0 ? (_reportPossibleCrUseOfdeskClientProtos({
        error: Error()
      }), deskClientProtos) : deskClientProtos);
      globalThis._serverProtos = mergeDeep(globalThis._serverProtos || {}, _crd && deskServerProtos === void 0 ? (_reportPossibleCrUseOfdeskServerProtos({
        error: Error()
      }), deskServerProtos) : deskServerProtos);
      globalThis._pushProtos = mergeDeep(globalThis._pushProtos || {}, _crd && deskPushProtos === void 0 ? (_reportPossibleCrUseOfdeskPushProtos({
        error: Error()
      }), deskPushProtos) : deskPushProtos);
      globalThis._clientProtos = mergeDeep(globalThis._clientProtos || {}, _crd && jackpotClientProtos === void 0 ? (_reportPossibleCrUseOfjackpotClientProtos({
        error: Error()
      }), jackpotClientProtos) : jackpotClientProtos);
      globalThis._serverProtos = mergeDeep(globalThis._serverProtos || {}, _crd && jackpotServerProtos === void 0 ? (_reportPossibleCrUseOfjackpotServerProtos({
        error: Error()
      }), jackpotServerProtos) : jackpotServerProtos);
      globalThis._clientProtos = mergeDeep(globalThis._clientProtos || {}, _crd && slotClientProtos === void 0 ? (_reportPossibleCrUseOfslotClientProtos({
        error: Error()
      }), slotClientProtos) : slotClientProtos);
      globalThis._serverProtos = mergeDeep(globalThis._serverProtos || {}, _crd && slotServerProtos === void 0 ? (_reportPossibleCrUseOfslotServerProtos({
        error: Error()
      }), slotServerProtos) : slotServerProtos);
      globalThis._pushProtos = mergeDeep(globalThis._pushProtos || {}, _crd && pushProtosSlot === void 0 ? (_reportPossibleCrUseOfpushProtosSlot({
        error: Error()
      }), pushProtosSlot) : pushProtosSlot);

      if (globalThis.m_Protobuf) {
        globalThis.decodeIO_encoder = globalThis.m_Protobuf.Root.fromJSON(globalThis._clientProtos);
        globalThis.decodeIO_decoder = globalThis.m_Protobuf.Root.fromJSON(globalThis._serverProtos);
        globalThis.push_decoder = globalThis.m_Protobuf.Root.fromJSON(globalThis._pushProtos);
      }

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=sCoreNetReq.js.map
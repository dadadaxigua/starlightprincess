System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2", "__unresolved_3"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, Button, director, Label, Node, Sprite, tween, UIOpacity, v3, Vec3, Widget, sGameEntity, sAudioMgr, sUtil, _dec, _dec2, _dec3, _dec4, _dec5, _dec6, _dec7, _dec8, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _descriptor5, _descriptor6, _descriptor7, _temp, _crd, ccclass, property, StarlightPrincess_GameEntity;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfsGameEntity(extras) {
    _reporterNs.report("sGameEntity", "../lobby/hyPrefabs/gameRes/external/sGameEntity", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsAudioMgr(extras) {
    _reporterNs.report("sAudioMgr", "../lobby/game/core/sAudioMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsUtil(extras) {
    _reporterNs.report("sUtil", "../lobby/game/core/sUtil", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      Button = _cc.Button;
      director = _cc.director;
      Label = _cc.Label;
      Node = _cc.Node;
      Sprite = _cc.Sprite;
      tween = _cc.tween;
      UIOpacity = _cc.UIOpacity;
      v3 = _cc.v3;
      Vec3 = _cc.Vec3;
      Widget = _cc.Widget;
    }, function (_unresolved_2) {
      sGameEntity = _unresolved_2.sGameEntity;
    }, function (_unresolved_3) {
      sAudioMgr = _unresolved_3.default;
    }, function (_unresolved_4) {
      sUtil = _unresolved_4.sUtil;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "335c9ncRt5EuogPXDve42Y6", "StarlightPrincess_GameEntity", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("StarlightPrincess_GameEntity", StarlightPrincess_GameEntity = (_dec = ccclass('StarlightPrincess_GameEntity'), _dec2 = property(Node), _dec3 = property(Button), _dec4 = property(Node), _dec5 = property(Label), _dec6 = property(Label), _dec7 = property(Label), _dec8 = property({
        type: [Button]
      }), _dec(_class = (_class2 = (_temp = class StarlightPrincess_GameEntity extends (_crd && sGameEntity === void 0 ? (_reportPossibleCrUseOfsGameEntity({
        error: Error()
      }), sGameEntity) : sGameEntity) {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "btnRenderNode", _descriptor, this);

          _initializerDefineProperty(this, "infoBtn", _descriptor2, this);

          _initializerDefineProperty(this, "ruleNode", _descriptor3, this);

          _initializerDefineProperty(this, "betDetailLabel", _descriptor4, this);

          _initializerDefineProperty(this, "winStringLabel", _descriptor5, this);

          _initializerDefineProperty(this, "winStringLabel1", _descriptor6, this);

          _initializerDefineProperty(this, "Btns", _descriptor7, this);

          _defineProperty(this, "betSpeedModeTemp1", 'normal');

          _defineProperty(this, "shopNode", void 0);

          _defineProperty(this, "pTween", null);

          _defineProperty(this, "callBack", null);

          _defineProperty(this, "updateLabTween", void 0);
        }

        onLoad() {
          super.onLoad();
          director.on('betBtnState', state => {
            if (state == 'disable') {
              this.spinViewCtr(false);
            } else if (state == 'enable') {
              this.spinViewCtr(true);
            }
          }, this);
          director.on('betBtnClick', betSpeedMode => {
            (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
              error: Error()
            }), sAudioMgr) : sAudioMgr).PlayShotAudio('button');
            this.spinViewCtr(false);
          }, this);
          director.on('autoBetInfoUpdate', value => {// if(value == -1){
            //     this.spinViewCtr(false);
            // }else if(value > 0){
            //     this.spinViewCtr(false);
            // }else{
            //     this.spinViewCtr(true);
            // }
          }, this);
          director.on('viewChange', (type, value) => {
            if (type == 'autoSpinCancel') {
              const toggle = this.autoPlayBtnNode.node.getChildByName('toggle');
              toggle.getChildByName('on').active = false;
              toggle.getChildByName('off').active = true;
            }

            ;
          }, this);
        }

        start() {
          Promise.all([new Promise((resolve, reject) => {
            globalThis.getSubGamePrefabByPath('prefabs/subGameUserCoin', newObj => {
              if (newObj) {
                newObj.parent = this.node.parent;
                this.ownLabel = newObj.getChildByName('Label').getComponent(Label);
                this.shopNode = newObj.getChildByName('shop').getComponent(Button);
                newObj.getComponent(Widget).isAlignLeft = true;
                newObj.getComponent(Widget).isAlignTop = true;
                newObj.getComponent(Widget).left = -20;
                newObj.getComponent(Widget).top = -10;
              }

              ;
              resolve(1);
            });
          })]).then(res => {
            super.start();

            if (this.shopNode) {
              this.shopNode.node.on('click', () => {
                globalThis.openStore && globalThis.openStore();
              }, this);
            }

            ;
          });
        }

        spinViewCtr(value) {
          this.btnRenderNode.getComponent(Sprite).grayscale = !value;
          this.btnRenderNode.children[0].getComponent(Sprite).grayscale = !value;
        }

        freeWinBegain() {
          // if(this.viewMode == 'record'){
          //     if(this.recordBtns){
          //         this.recordBtns.node.active = true;
          //     }
          // }
          // this.userCoin = this.beforeUserCoin;
          // this.winTotal = this.firstRate > 0 ? this.firstRate * this.betAmount : 0;
          for (let btn of this.Btns) {
            btn.node.getComponent(UIOpacity).opacity = 100;
            btn.getComponent(Button).interactable = false;
          }
        }

        freeWinOver() {
          // if(this.viewMode == 'record'){
          //     if(this.recordBtns){
          //         this.recordBtns.node.active = true;
          //     }
          // }
          // this.userCoin = this.beforeUserCoin;
          // this.winTotal = this.firstRate > 0 ? this.firstRate * this.betAmount : 0;
          for (let btn of this.Btns) {
            btn.node.getComponent(UIOpacity).opacity = 255;
            btn.getComponent(Button).interactable = true;
          }
        } // turboBtnAction() {
        //     globalThis.subGamePlayShotAudio('subGameClick');
        //     if (this.betSpeedModeTemp1 == 'normal') {
        //         this.autoPlayBtnNode.getComponent(UIOpacity).opacity=100;
        //         this.betSpeedModeTemp1 = 'turbo';
        //         this.turboBtnNodeClick(true);
        //     } else {
        //         this.autoPlayBtnNode.getComponent(UIOpacity).opacity=100;
        //         this.betSpeedModeTemp1 = 'normal';
        //         this.turboBtnNodeClick(false);
        //     }
        //     director.emit('uiTipsOpen', this.betSpeedModeTemp1, this.betSpeedModeTemp1 == 'normal' ? btnInternationalManager.GetDataByKey('closeTurboSpinMode') : btnInternationalManager.GetDataByKey('openTurboSpinMode'));
        // }


        turboBtnNodeClick(value) {
          // this.turboBtnNode.getComponent(UIOpacity).opacity=(value)?100:255;
          if (value) {
            this.turboBtnNode.node.getChildByName('off').active = false;
            this.turboBtnNode.node.getChildByName('on').active = true;
          } else {
            this.turboBtnNode.node.getChildByName('off').active = true;
            this.turboBtnNode.node.getChildByName('on').active = false;
          }
        }

        autoPlayBtnClick() {
          if (this.canBet) {
            if (this.betBtnState == 'normal') {
              if (this.betClickMode == 'normal') {
                this.setGameBtnState('disable'); //this.autoPlayBtnNode.getComponent(UIOpacity).opacity=100;

                const toggle = this.autoPlayBtnNode.node.getChildByName('toggle');
                toggle.getChildByName('on').active = true;
                toggle.getChildByName('off').active = false;
                director.emit('viewChange', 'UIAutoSpin', -1);
              } else if (this.betClickMode == 'autoBet') {
                const toggle = this.autoPlayBtnNode.node.getChildByName('toggle'); //this.autoPlayBtnNode.getComponent(UIOpacity).opacity=255;

                toggle.getChildByName('on').active = false;
                toggle.getChildByName('off').active = true;
                director.emit('viewChange', 'autoSpinCancel');
              }
            } else {
              if (this.betClickMode == 'autoBet') {
                //this.autoPlayBtnNode.getComponent(UIOpacity).opacity=255;
                const toggle = this.autoPlayBtnNode.node.getChildByName('toggle');
                toggle.getChildByName('on').active = false;
                toggle.getChildByName('off').active = true;
                director.emit('viewChange', 'autoSpinCancel');
              }
            }
          } else {
            this.betClickCoinNotEnough();
          }
        }

        errToOneRes() {
          const toggle = this.autoPlayBtnNode.node.getChildByName('toggle');
          toggle.getChildByName('on').active = false;
          toggle.getChildByName('off').active = true;
          super.errToOneRes();
        }

        menuBtnClick() {
          director.emit('subGameMenuView', true);
          const audioData = globalThis.getSubGameAudioVolume();

          if (audioData) {
            if (audioData.audioVolume == 1) {
              this.soundBtn.node.getChildByName('open').active = true;
              this.soundBtn.node.getChildByName('close').active = false;
            } else {
              this.soundBtn.node.getChildByName('close').active = true;
              this.soundBtn.node.getChildByName('open').active = false;
            }
          }

          this.menuBtns.node.active = true;
        }

        soundBtnClick() {
          if (this.audioTouchEnable) {
            this.audioTouchEnable = false;
            const audioData = globalThis.getSubGameAudioVolume();

            if (audioData) {
              if (audioData.audioVolume == 1) {
                globalThis.subGameAudioVolumeCtr(false);
                this.soundBtn.node.getChildByName('close').active = true;
                this.soundBtn.node.getChildByName('open').active = false;
              } else {
                globalThis.subGameAudioVolumeCtr(true);
                this.soundBtn.node.getChildByName('open').active = true;
                this.soundBtn.node.getChildByName('close').active = false;
              }
            }

            this.scheduleOnce(() => {
              this.audioTouchEnable = true;
            }, 1);
          }
        }

        closeBtnClick() {
          director.emit('subGameMenuView', false);
          this.menuBtns.node.active = false;
        }

        rulesBtnClick() {
          this.ruleNode.active = true;
        }

        addBtnClick() {
          super.addBtnClick();
          (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
            error: Error()
          }), sAudioMgr) : sAudioMgr).PlayShotAudio('button');
        }

        minusBtnClick() {
          super.minusBtnClick();
          (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
            error: Error()
          }), sAudioMgr) : sAudioMgr).PlayShotAudio('button');
        }

        updateBetInfo() {
          super.updateBetInfo();
          this.betDetailLabel.string = `${this.AddCommas(this.betAmount)} x ${20}`;
        }

        betUserInfoUpdate(own, bet, win) {
          if (this.ownLabel) {
            if (own || own == 0) {
              this.ownLabel.node['rollAnimaValue'] = own;
              this.ownLabel.string = this.AddCommas(Math.trunc(own));
            }
          }

          if (this.betLabel) {
            if (bet || bet == 0) {
              this.betLabel.node['rollAnimaValue'] = bet;
              this.betLabel.string = this.AddCommas(Math.trunc(bet));
            }
          }

          ;

          if (win == null) {
            this.winLabel.node['rollAnimaValue'] = 0;
            console.log('重置winLabel1');
            this.winLabel.node.active = false;
            this.winStringLabel.node.active = true;
            this.winStringLabel1.node.active = false;
            this.winLabel.string = 0 + '';
          } else {
            if (win && win > 0) {
              //console.log('win1:',win);
              this.winLabel.node['rollAnimaValue'] = win;
              console.log('重置winLabel2');
              this.winLabel.string = this.AddCommas(Math.trunc(win));
              this.winStringLabel1.node.active = true;
            } else {//console.log('win2:',win);
              // this.winLabel.string=0+'';
            }
          }

          ;
        }

        betUserInfoUpdateAnima(own, bet, win, animaTime = 1, callBack = null) {
          if (own || own == 0) {
            this.CommasLabelAnima(this.ownLabel, Math.trunc(own), animaTime);
          }

          ;

          if (bet || bet == 0) {
            this.CommasLabelAnima(this.betLabel, Math.trunc(bet), animaTime);
          }

          ;

          if (win == null) {
            this.winLabel.node['rollAnimaValue'] = 0;
            console.log('重置winLabel3');
            this.winLabel.string = 0 + '';
          } else {
            if (win && win > 0) {
              this.winLabel.node.active = true;
              this.winStringLabel.node.active = false;
              this.winStringLabel1.node.active = true; //console.log('win3:',win);

              let time = 0.5;

              if (win >= this.currentBetAmount * 200) {
                time = 12;
              }

              ;

              if (!this.winLabel.node['rollAnimaValue']) {
                //c//onsole.log('重置winLabel4');
                this.winLabel.node['rollAnimaValue'] = 0;
              }

              ; //this.winLabel.string = this.AddCommas(Math.floor(win));

              let num = parseInt(this.winLabel.string); //console.log('win:',win);
              //console.log('num:',num);
              //console.log('this.winLabel.string:',this.winLabel.string);
              //console.log('rollAnimaValue1',this.winLabel.node['rollAnimaValue']);
              //sUtil.TweenLabel(this.winLabel.node['rollAnimaValue'],Math.trunc(win+this.winLabel.node['rollAnimaValue']),this.winLabel,time);

              this.updateWinLab(this.winLabel.node['rollAnimaValue'], this.winLabel, Math.trunc(win + this.winLabel.node['rollAnimaValue']), time, this.callBack); //this.winLabel.node['rollAnimaValue'] += win;

              this.scheduleOnce(() => {
                this.winLabel.node['rollAnimaValue'] += Math.trunc(win); //console.log('rollAnimaValue2',this.winLabel.node['rollAnimaValue']);
              }, time);
              director.off('StopLabelanima');
              (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                error: Error()
              }), sUtil) : sUtil).once('StopLabelanima', () => {
                //this.winLabel.node['rollAnimaValue'] += win;
                this.updateLabTween.stop();
                this.winLabel.string = this.AddCommas(Math.floor(win));
              });
            } else {
              if (!this.winLabel.node['rollAnimaValue']) {
                //console.log('重置winLabel5');
                this.winLabel.node['rollAnimaValue'] = 0;
                this.winLabel.string = 0 + '';
              }

              ; //console.log('win4:',win);
              // this.winLabel.node.active=false;
              // this.winStringLabel.node.active=true;
              // this.winStringLabel1.node.active=false
            }

            ;
          }

          ;

          if (callBack != null) {
            this.scheduleOnce(callBack, animaTime);
          }

          ;
        }

        updateWinLab(lastCoin, winLabel, coin, time, cb) {
          let tweenTargetVec3 = v3(lastCoin, lastCoin, lastCoin);
          this.updateLabTween && this.updateLabTween.stop();
          this.updateLabTween = tween(tweenTargetVec3).to(time, v3(coin, coin, coin), {
            "onUpdate": target => {
              if (winLabel) {
                //winLabel.string = sUtil.AddCommas(Math.floor(target.x));
                winLabel.string = (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                  error: Error()
                }), sUtil) : sUtil).AddCommas(Math.trunc(target.x));
              }

              ;
            },
            easing: undefined
          }).call(() => {
            winLabel.string = this.AddCommas(Math.trunc(coin)); //this.winLabel.node['rollAnimaValue'] += coin;

            if (cb) cb();
          }).start();
        }

        // updateLabTween: Tween<any>;
        // CommasLabelAnima(label, value, time) {
        //     if (label) {
        //         const valueOri = label.node['rollAnimaValue'];
        //         const mTween = this.updateLabTween;
        //         if (mTween) {
        //             mTween.stop();
        //         };
        //         if(value>0){
        //             director.off('StopLabelanima');
        //             sUtil.once('StopLabelanima',()=>{
        //                 if(mTween){
        //                     mTween.stop();
        //                     //this.unscheduleAllCallbacks();
        //                     tween(tweenTargetVec3).stop();
        //                 };
        //                 label.string = this.AddCommas(Math.floor(value));
        //             })
        //         };
        //         let tweenTargetVec3 = v3(valueOri, valueOri, valueOri);
        //         // console.log('tweenTargetVec3:'+tweenTargetVec3);
        //         this.updateLabTween = tween(tweenTargetVec3).to(time, v3(value, value, value), {
        //             "onUpdate": (target: Vec3) => {
        //                 if (label) {
        //                     //label.node['rollAnimaValue'] = Math.floor(target.x)
        //                     label.string = this.AddCommas(Math.floor(target.x));
        //                 };
        //             }, easing: 'quadOut'
        //         }).call(() => {
        //             if (label) {
        //                 //console.log('rollAnimaValue',label.node['rollAnimaValue']);
        //                 //label.node['rollAnimaValue'] = Math.floor(value)
        //                 label.string = this.AddCommas(Math.floor(value));
        //             };
        //         }).start();
        //     };
        // };
        CommasLabelAnima(label, value, time) {
          if (label && (label.node['rollAnimaValue'] || label.node['rollAnimaValue'] == 0)) {
            const valueOri = label.node['rollAnimaValue'];
            const mTween = label.node['rollAnimaTween'];

            if (mTween) {
              mTween.stop();
            }

            ;

            if (value > 0) {//director.off('StopLabelanima');
              // sUtil.once('StopLabelanima',()=>{
              //     if(mTween){
              //         mTween.stop();
              //     };
              //     label.string = this.AddCommas(Math.floor(value));
              // })
            }

            ;
            let tweenTargetVec3 = v3(valueOri, valueOri, valueOri); // console.log('tweenTargetVec3:'+tweenTargetVec3);

            label.node['rollAnimaTween'] = tween(tweenTargetVec3).to(12, v3(value, value, value), {
              "onUpdate": target => {
                if (label) {
                  label.node['rollAnimaValue'] = Math.floor(target.x); //label.string = this.AddCommas(Math.floor(target.x));
                }
              },
              easing: 'quadOut'
            }).call(() => {
              if (label) {
                label.node['rollAnimaValue'] = Math.floor(value);
                label.string = this.AddCommas(Math.floor(value));
              }
            }).start();
          }
        }

        MaxBetBtnClick() {
          if (this.clientConfig && this.betBtnState == 'normal') {
            let findMax = false;
            let bet_types = this.clientConfig.bet_types;

            if (bet_types && bet_types.length > 0) {
              for (let i = 0; i < bet_types.length; i++) {
                const element = bet_types[i];
                console.log('addBtnClick:', JSON.stringify(element));

                if (element.game_mode == 1 && element.game_id == this.gameid) {
                  if (element.bet_amount > this.betAmount) {
                    this.betAmount = element.bet_amount;
                    this.betType = element.bet_type;
                    director.emit('sSlotBetInfoUpdate', {
                      betAmount: this.betAmount,
                      multiple: this.lineAmount
                    });

                    if (this.betLabel) {
                      this.betLabel.node.setScale(1.5, 1.5, 1.5);
                      tween(this.betLabel.node).to(0.01, {
                        scale: Vec3.ONE
                      }, {
                        easing: 'elasticOut'
                      }).start();
                    }

                    this.updateBetInfo();
                    findMax = true;
                    this.saveBetAmountValue(); //break;
                  }

                  ;
                }

                ;
              }

              ;
            }

            ; // if (!findMax) {
            //     director.emit('uiTipsOpen', 'text', btnInternationalManager.GetDataByKey('maximunBet'));
            // }
          }
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "btnRenderNode", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "infoBtn", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "ruleNode", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "betDetailLabel", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor5 = _applyDecoratedDescriptor(_class2.prototype, "winStringLabel", [_dec6], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor6 = _applyDecoratedDescriptor(_class2.prototype, "winStringLabel1", [_dec7], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor7 = _applyDecoratedDescriptor(_class2.prototype, "Btns", [_dec8], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return [];
        }
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=StarlightPrincess_GameEntity.js.map
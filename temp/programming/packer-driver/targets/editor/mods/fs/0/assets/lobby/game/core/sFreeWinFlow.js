System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2", "__unresolved_3"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, director, Prefab, instantiate, sConfigMgr, sGameFlowBase, UIBase, _dec, _dec2, _class, _class2, _descriptor, _descriptor2, _temp, _crd, ccclass, property, sFreeWinFlow;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfsConfigMgr(extras) {
    _reporterNs.report("sConfigMgr", "./sConfigMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsGameFlowBase(extras) {
    _reporterNs.report("sGameFlowBase", "./sGameFlowBase", _context.meta, extras);
  }

  function _reportPossibleCrUseOfUIBase(extras) {
    _reporterNs.report("UIBase", "./UIBase", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      director = _cc.director;
      Prefab = _cc.Prefab;
      instantiate = _cc.instantiate;
    }, function (_unresolved_2) {
      sConfigMgr = _unresolved_2.sConfigMgr;
    }, function (_unresolved_3) {
      sGameFlowBase = _unresolved_3.sGameFlowBase;
    }, function (_unresolved_4) {
      UIBase = _unresolved_4.UIBase;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "0aaf8g/o1ZP+qBPvfiY6u+H", "sFreeWinFlow", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("sFreeWinFlow", sFreeWinFlow = (_dec = ccclass('sFreeWinFlow'), _dec2 = property(Prefab), _dec(_class = (_class2 = (_temp = class sFreeWinFlow extends (_crd && sGameFlowBase === void 0 ? (_reportPossibleCrUseOfsGameFlowBase({
        error: Error()
      }), sGameFlowBase) : sGameFlowBase) {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "totalWinPrefab", _descriptor, this);

          _initializerDefineProperty(this, "roundIntervalTime", _descriptor2, this);

          _defineProperty(this, "freeWinCount", -1);

          _defineProperty(this, "freeWinIndex", 0);

          _defineProperty(this, "isFirstWin", false);
        }

        onLoad() {
          super.onLoad();
          director.on('freeWinBegain', () => {
            this.scheduleOnce(this.rollFreeWinAction, 1);
          }, this);
          director.on('freeWinOneRoundEnd', () => {
            if (globalThis.GameBtnEntity.NeedFreeWinContinue()) {
              this.rollFreeWinAction();
            }
          }, this);
          director.on('recordFreeWinPlayClick', () => {
            this.rollFreeWinAction();
          }, this);
          director.on('freeWinShow', (count, index) => {
            this.freeWinCount = count;
            this.freeWinIndex = index;
            this.scheduleOnce(this.rollFreeWinAction, 1);
          }, this);
        }

        start() {
          super.start();
        }

        flowBegin(rollSpeedMode, clickMode) {
          const round1 = (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
            error: Error()
          }), sConfigMgr) : sConfigMgr).instance.GetOneRoundData(0);
          this.rollFirstRoundAction(round1, rollSpeedMode, clickMode);
        }
        /**
         * @zh
         * 免费游戏的未进入的第一次旋转，此次旋转即为抽中3个scatter的那次,之后的结果都会在round数组里
         * @param round - 当前round数据
         * @param rollSpeedMode - 滚动类型 normal,turbo
         * @param clickMode - 点击类型 normal,autoBet
         */


        rollFirstRoundAction(round, rollSpeedMode, clickMode) {}

        rollStopAction(round, rollSpeedMode, clickMode) {
          super.rollStopAction(round, rollSpeedMode, clickMode);
        }

        rollFreeWinAction() {
          this.freeWinIndex++;
          director.emit('freeWinAction', this.freeWinCount - this.freeWinIndex);

          if (this.freeWinIndex <= this.freeWinCount) {
            director.emit('boardReset');
            director.emit('freeWinRoundBegin'); // sAudioMgr.PlayAudio('roll',true);

            director.emit('soltRollBeagin');
            this.scheduleOnce(() => {
              const roundData = (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
                error: Error()
              }), sConfigMgr) : sConfigMgr).instance.GetOneRoundData(this.freeWinIndex);

              if (roundData) {
                // const round = roundData['round_symbol_map'];
                this.rollStopAction(roundData, '', '');
              } // this.freeWinIndex = this.freeWinCount + 1;

            }, this.roundIntervalTime);
          } else {
            this.freeWinTotleWinCtr();
          }
        }

        freeWinTotleWinCtr() {
          const rounds = (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
            error: Error()
          }), sConfigMgr) : sConfigMgr).instance.GetRoundData();

          if (rounds && rounds.length > 0) {
            let winTotalCoin = 0;
            let winRealTotalCoin = 0;

            for (let i = 0; i < rounds.length; i++) {
              const element = rounds[i];

              if (i > 0) {
                winTotalCoin += element.rate * globalThis.GameBtnEntity.CurrentBetAmount;
              }

              winRealTotalCoin += element.rate * globalThis.GameBtnEntity.CurrentBetAmount;
            }

            this.freeWinTotleViewOpen(winTotalCoin, winRealTotalCoin);
          }
        }

        freeWinTotleViewOpen(winTotalCoin, winRealTotalCoin) {
          if (this.totalWinPrefab) {
            const obj = instantiate(this.totalWinPrefab);
            obj.setParent(this.node.parent);
            obj.getComponent(_crd && UIBase === void 0 ? (_reportPossibleCrUseOfUIBase({
              error: Error()
            }), UIBase) : UIBase).DataInit({
              coin: winTotalCoin,
              count: this.freeWinCount
            });
            director.emit('freeWinEndRes', winTotalCoin, winRealTotalCoin);
          } else {
            director.emit('freeWinOver');
            director.emit('rollStop');
          }
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "totalWinPrefab", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "roundIntervalTime", [property], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return 0.5;
        }
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=sFreeWinFlow.js.map
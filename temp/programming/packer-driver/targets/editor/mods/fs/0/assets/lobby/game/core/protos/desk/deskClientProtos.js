System.register(["cc"], function (_export, _context) {
  "use strict";

  var _cclegacy, _crd;

  return {
    setters: [function (_cc) {
      _cclegacy = _cc.cclegacy;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "98723fednJLsLSxX2nwvbvU", "deskClientProtos", undefined);

      _export("default", {
        "nested": {
          "desk": {
            "nested": {
              "mainHandler": {
                "nested": {
                  "enterDeskByDeskId": {
                    "fields": {
                      "desk_id": {
                        "type": "int64",
                        "id": 1
                      },
                      "transfer_safebox": {
                        "type": "double",
                        "id": 2
                      }
                    }
                  },
                  "enterDeskByDeskGroup": {
                    "fields": {
                      "desk_group": {
                        "type": "int32",
                        "id": 1
                      },
                      "transfer_safebox": {
                        "type": "double",
                        "id": 2
                      }
                    }
                  },
                  "leaveDesk": {
                    "fields": {
                      "desk_id": {
                        "type": "int64",
                        "id": 1
                      }
                    }
                  }
                }
              }
            }
          }
        }
      });

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=deskClientProtos.js.map
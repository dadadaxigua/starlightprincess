System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2", "__unresolved_3", "__unresolved_4"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, Component, assetManager, director, SpriteAtlas, JsonAsset, EditBox, Button, sys, Prefab, instantiate, macro, dynamicAtlasManager, game, req, userData, assetMgr, sConfigMgr, _dec, _dec2, _dec3, _dec4, _dec5, _dec6, _dec7, _dec8, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _descriptor5, _descriptor6, _descriptor7, _temp, _crd, ccclass, property, entryAdapter, index;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfreq(extras) {
    _reporterNs.report("req", "./network/req", _context.meta, extras);
  }

  function _reportPossibleCrUseOfuserData(extras) {
    _reporterNs.report("userData", "./network/userData", _context.meta, extras);
  }

  function _reportPossibleCrUseOfassetMgr(extras) {
    _reporterNs.report("assetMgr", "./lobby/game/core/sAssetMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsConfigMgr(extras) {
    _reporterNs.report("sConfigMgr", "./lobby/game/core/sConfigMgr", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      Component = _cc.Component;
      assetManager = _cc.assetManager;
      director = _cc.director;
      SpriteAtlas = _cc.SpriteAtlas;
      JsonAsset = _cc.JsonAsset;
      EditBox = _cc.EditBox;
      Button = _cc.Button;
      sys = _cc.sys;
      Prefab = _cc.Prefab;
      instantiate = _cc.instantiate;
      macro = _cc.macro;
      dynamicAtlasManager = _cc.dynamicAtlasManager;
      game = _cc.game;
    }, function (_unresolved_2) {
      req = _unresolved_2.default;
    }, function (_unresolved_3) {
      userData = _unresolved_3.default;
    }, function (_unresolved_4) {
      assetMgr = _unresolved_4.assetMgr;
    }, function (_unresolved_5) {
      sConfigMgr = _unresolved_5.sConfigMgr;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "61e05s3+TRJK5vfWWox1Doa", "entryAdapter", undefined);

      ({
        ccclass,
        property
      } = _decorator);
      macro.CLEANUP_IMAGE_CACHE = false;
      dynamicAtlasManager.enabled = true;
      dynamicAtlasManager.maxFrameSize = 4096;
      dynamicAtlasManager.textureSize = 4096;
      game.frameRate = 60;

      _export("entryAdapter", entryAdapter = (_dec = ccclass('entryAdapter'), _dec2 = property(JsonAsset), _dec3 = property(EditBox), _dec4 = property(Button), _dec5 = property(Prefab), _dec6 = property(Prefab), _dec7 = property([Prefab]), _dec8 = property([SpriteAtlas]), _dec(_class = (_class2 = (_temp = class entryAdapter extends Component {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "clientConfig", _descriptor, this);

          _initializerDefineProperty(this, "mEditbox", _descriptor2, this);

          _initializerDefineProperty(this, "loginBtn", _descriptor3, this);

          _initializerDefineProperty(this, "btnPrefab", _descriptor4, this);

          _initializerDefineProperty(this, "recordPrefab", _descriptor5, this);

          _initializerDefineProperty(this, "lobbyPrefab", _descriptor6, this);

          _initializerDefineProperty(this, "languageSpriteAtlas", _descriptor7, this);

          _defineProperty(this, "touchEnable", true);
        }

        start() {
          globalThis.betTypeConfig = this.clientConfig.json;
          globalThis.subGameBtnPrefab = this.btnPrefab;
          globalThis.recordPrefabNode = this.recordPrefab;
          globalThis.lobbyPrefabs = this.lobbyPrefab;
          director.on('authSuc', () => {
            assetManager.loadBundle('149', (err, bundle) => {
              if (bundle) {
                bundle.loadScene('main_hor', (err, scene) => {
                  if (scene) {
                    director.runScene(scene);
                  }
                });
              }
            });
          }, this);
          (_crd && req === void 0 ? (_reportPossibleCrUseOfreq({
            error: Error()
          }), req) : req).init();
          globalThis.testNetWork();
          director.on('socketconnected', e => {
            console.log('socketconnected');
            (_crd && req === void 0 ? (_reportPossibleCrUseOfreq({
              error: Error()
            }), req) : req).auth();
          }, this);
          let deviceID = sys.localStorage.getItem('ddddeviceid');

          if (deviceID) {
            this.mEditbox.string = deviceID;
          }

          let language = 'EN';

          if (globalThis.GetLanguageType) {
            language = globalThis.GetLanguageType();
          }

          if (this.languageSpriteAtlas) {
            for (let i = 0; i < this.languageSpriteAtlas.length; i++) {
              const at = this.languageSpriteAtlas[i];

              if (at.name == language) {
                (_crd && assetMgr === void 0 ? (_reportPossibleCrUseOfassetMgr({
                  error: Error()
                }), assetMgr) : assetMgr).SetSpriteFramesToDic(at);
                break;
              }
            }
          }

          this.loginBtn.node.on('click', () => {
            if (this.mEditbox.string && this.touchEnable) {
              this.touchEnable = false;
              (_crd && req === void 0 ? (_reportPossibleCrUseOfreq({
                error: Error()
              }), req) : req).getAppConfig('2966', (res, err) => {
                (_crd && userData === void 0 ? (_reportPossibleCrUseOfuserData({
                  error: Error()
                }), userData) : userData).SetAppConfigData(res);
                let autoLoginToken = (_crd && userData === void 0 ? (_reportPossibleCrUseOfuserData({
                  error: Error()
                }), userData) : userData).GetAutoLoginToken();
                let hostStr = (_crd && userData === void 0 ? (_reportPossibleCrUseOfuserData({
                  error: Error()
                }), userData) : userData).GetSocketHost();
                let host = JSON.parse(hostStr); // if(autoLoginToken && host){
                //     globalThis.userToken = autoLoginToken;
                //     req.gameServerCon(autoLoginToken,host,'ddddd123');
                // }else{

                (_crd && req === void 0 ? (_reportPossibleCrUseOfreq({
                  error: Error()
                }), req) : req).DeviceLogin(this.mEditbox.string, globalThis.appID, '', '', '', (res, err) => {
                  if (!err) {
                    sys.localStorage.setItem('ddddeviceid', this.mEditbox.string);
                    globalThis.userToken = res.data.loginToken;
                    (_crd && userData === void 0 ? (_reportPossibleCrUseOfuserData({
                      error: Error()
                    }), userData) : userData).SaveAutoLoginToken(res.data.loginToken);
                    (_crd && userData === void 0 ? (_reportPossibleCrUseOfuserData({
                      error: Error()
                    }), userData) : userData).SaveSocketHost(JSON.stringify(res.data.hosts));
                    (_crd && req === void 0 ? (_reportPossibleCrUseOfreq({
                      error: Error()
                    }), req) : req).gameServerCon(res.data.loginToken, res.data.hosts, this.mEditbox.string);
                  }

                  this.touchEnable = true;
                }); // }
              });
            }
          }, this);
          this.loginBtn.node.emit('click');
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "clientConfig", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "mEditbox", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "loginBtn", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "btnPrefab", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor5 = _applyDecoratedDescriptor(_class2.prototype, "recordPrefab", [_dec6], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor6 = _applyDecoratedDescriptor(_class2.prototype, "lobbyPrefab", [_dec7], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return [];
        }
      }), _descriptor7 = _applyDecoratedDescriptor(_class2.prototype, "languageSpriteAtlas", [_dec8], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return [];
        }
      })), _class2)) || _class));

      globalThis.getSubGameBtn = function (gameid, aligmentType, aligmentPixel, gameVersion, customColor, cb) {
        if (cb) {
          let obj = instantiate(globalThis.subGameBtnPrefab);

          if (obj) {
            obj['gameid'] = gameid;
            obj['aligmentType'] = aligmentType;
            obj['aligmentPixel'] = aligmentPixel;
            obj['gameVersion'] = gameVersion;
            obj['customColor'] = customColor;

            if (cb) {
              cb(obj);
            }
          }
        }
      };

      globalThis.getSubGameBtnByName = function (gameid, belongName, aligmentType, aligmentPixel, gameVersion, customColor, cb) {
        if (cb) {
          let obj = instantiate(globalThis.subGameBtnPrefab);

          if (obj) {
            obj['gameid'] = gameid;
            obj['aligmentType'] = aligmentType;
            obj['aligmentPixel'] = aligmentPixel;
            obj['gameVersion'] = gameVersion;
            obj['customColor'] = customColor;

            if (cb) {
              cb(obj);
            }
          }
        }
      };

      globalThis.getSubGamePrefabByPath = function (path, cb) {
        if (path && cb) {
          for (let i = 0; i < globalThis.lobbyPrefabs.length; i++) {
            const p = globalThis.lobbyPrefabs[i];
            const fileName = path.substring(path.lastIndexOf('/') + 1, path.length);

            if (fileName) {
              if (fileName == p.data.name) {
                let obj = instantiate(p);

                if (obj) {
                  cb(obj);
                }

                break;
              }
            }
          }
        }
      };

      globalThis.getClientConfig = function (cb) {
        if (cb) {
          cb(globalThis.betTypeConfig);
        }
      };

      index = 1;

      globalThis.getSubGameOneBetTestData = function () {
        let boxData = (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
          error: Error()
        }), sConfigMgr) : sConfigMgr).instance.getBoxData(); // return boxData[1]['round_type14'][40][1];
        //return boxData[1]['round_type0'][4830][0];
        //return boxData[1]['round_type0'][46][3];
        // return boxData[1]['round_type0'][0][76];
        //return boxData[1]['round_type1'][1903][0];
        //return boxData[1]['round_type0'][4830][0];
        // index++;
        // if(index % 2 == 0){
        //return boxData[1]['round_type0'][400][0];
        // }else{
        //     return boxData[1]['round_type0'][0][index];
        // }
      }; // function randomNum(minNum, maxNum) {
      //     switch (arguments.length) {
      //         case 1:
      //             return parseInt((Math.random() * minNum + 1).toString(), 10);
      //             break;/
      //         case 2:
      //             return parseInt(Math.random() * (maxNum - minNum + 1) + minNum, 10);
      //             //或者 Math.floor(Math.random()*( maxNum - minNum + 1 ) + minNum );
      //             break;
      //         default:
      //             return 0;
      //             break;
      //     }
      // }


      globalThis.toast = function (str) {
        console.log(str);
      };

      globalThis.GetLanguageType = function () {
        return 'PT';
      };

      globalThis.GetDataByKey = function (id, key, key1, key2) {
        return '';
      };

      globalThis.getSoltGameRecord = function (cb) {
        cb(instantiate(globalThis.recordPrefabNode));
      };

      globalThis.getServerTime = function () {
        return Date.now();
      };

      globalThis.popString = function (str, scale = 1) {
        console.log('popString:' + str);
      };

      globalThis.getCurJackPotId = function () {
        return 14601;
      };

      globalThis.getCoinRate = function () {
        return 100;
      };

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=entryAdapter.js.map
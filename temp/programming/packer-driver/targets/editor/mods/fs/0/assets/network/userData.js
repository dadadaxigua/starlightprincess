System.register(["__unresolved_0", "cc", "__unresolved_1"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, koi, sys, userData, _crd, accountLocalData, iApp_Versions, iApp_Datas;

  function _reportPossibleCrUseOfkoi(extras) {
    _reporterNs.report("koi", "./entry", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      sys = _cc.sys;
    }, function (_unresolved_2) {
      koi = _unresolved_2.default;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "b295bGm9kBJs4xIwKCTA4yx", "userData", undefined);

      accountLocalData = [];
      iApp_Versions = {};
      iApp_Datas = {};
      userData = class userData {
        static SaveLocalAccount(_uuid, _loginToken, _phone, _type) {
          return; //_type 0 guest 1 phone 2 wechat 

          if (accountLocalData.length == 0) {
            userData.GetLocalAccount();
          }

          let user = {};
          user.uuid = _uuid;
          user.loginToken = _loginToken;
          user.phone = _phone;
          user.type = _type; // user.nick_name = 

          let index = accountLocalData.findIndex(x => x.uuid == _uuid);

          if (index >= 0) {
            accountLocalData.splice(index, 1);
            accountLocalData.unshift(user);
            let str = JSON.stringify(accountLocalData);
            sys.localStorage.setItem('accountLocalData', str);
            console.log('accountLocalData:' + str);
          } else {
            accountLocalData.unshift(user);
            let str = JSON.stringify(accountLocalData);
            sys.localStorage.setItem('accountLocalData', str);
            console.log('new accountLocalData:' + str);
          }
        }

        static SaveLocalAccountAvatar(_uuid, _avatar, _nick_name) {
          if (accountLocalData.length == 0) {
            userData.GetLocalAccount();
          }

          let index = accountLocalData.findIndex(x => x.uuid == _uuid);

          if (index >= 0) {
            let item = accountLocalData[index];

            if (_avatar) {
              item.avatar = _avatar;
            }

            if (_nick_name) {
              item.nick_name = _nick_name;
            }

            let str = JSON.stringify(accountLocalData);
            sys.localStorage.setItem('accountLocalData', str);
            console.log('accountLocalData:' + str);
          }
        }

        static GetLocalAccount() {
          if (accountLocalData.length == 0) {
            let str = sys.localStorage.getItem('accountLocalData');

            if (str) {
              console.log('localuser:' + str);
              accountLocalData = JSON.parse(str);
            }
          }

          return accountLocalData;
        }

        static GetUserInfo() {
          return (_crd && koi === void 0 ? (_reportPossibleCrUseOfkoi({
            error: Error()
          }), koi) : koi).globalDta.user.myInfo;
        }

        static SetCurrentUserUid(_uid) {
          (_crd && koi === void 0 ? (_reportPossibleCrUseOfkoi({
            error: Error()
          }), koi) : koi).globalDta.user.myInfo.uuid = _uid;
        }

        static SaveAutoLoginToken(token) {
          sys.localStorage.setItem('myLoginToken', token);
        }

        static SaveSocketHost(host) {
          sys.localStorage.setItem('mySocketHost', host);
        }

        static GetSocketHost() {
          let res = sys.localStorage.getItem('mySocketHost');

          if (res) {
            return res;
          }

          return null;
        }

        static GetAutoLoginToken() {
          return sys.localStorage.getItem('myLoginToken');
        }

        static IsUserGuest() {
          return (_crd && koi === void 0 ? (_reportPossibleCrUseOfkoi({
            error: Error()
          }), koi) : koi).globalDta.user.myInfo.role == 2;
        }

        static GetAppVersion(key) {
          return 0;

          if (iApp_Versions[key] > 0) {
            return iApp_Versions[key];
          } else {
            iApp_Versions[key] = sys.localStorage.getItem(key);

            if (iApp_Versions[key] > 0) {
              return iApp_Versions[key];
            } else {
              iApp_Versions[key] = 0;
            }
          }

          return iApp_Versions[key];
        }

        static SetAppConfigVersion(key, value) {
          if (value > 0) {
            iApp_Versions[key] = value;
            sys.localStorage.setItem(key, value);
          }
        }

        static SetAppConfigData(res) {
          try {
            if (res && res.data) {
              // let app_configVer = userData.GetAppVersion('IApp_configVersion');
              // if(res.data.api['sys.app_config'] == app_configVer){
              //     let app_config = sys.localStorage.getItem('IApp_config');
              //     if(app_config){
              //         let localData = JSON.parse(app_config);
              //         localData.data.api = res.data.api;
              //         koi.globalDta.game.allversionInfo = localData;
              //         koi.globalDta.game.currentGameVersion.lobby = localData.data.config.lobby_version.min;
              //     }else{
              //         Resloader.tipsPop(null, '网络发生错误,请重新登录!', () => {
              //             game.restart();
              //         });
              //     }
              // }else{
              //     if(!res.data.config){
              //         Resloader.tipsPop(null, '网络发生错误,请重新登录!', () => {
              //             game.restart();
              //         });
              //     }
              //     userData.SetAppConfigVersion('IApp_configVersion',res.data.api['sys.app_config']);
              //     sys.localStorage.setItem('IApp_config',JSON.stringify(res));
              //     koi.globalDta.game.allversionInfo = res;
              //     koi.globalDta.game.currentGameVersion.lobby = res.data.config.lobby_version.min ? res.data.config.lobby_version.min : 0;
              // }
              (_crd && koi === void 0 ? (_reportPossibleCrUseOfkoi({
                error: Error()
              }), koi) : koi).globalDta.game.allversionInfo = res; // koi.globalDta.game.currentGameVersion.lobby = res.data.config.lobby_version.min ? res.data.config.lobby_version.min : 0;
              // console.log('koi.globalDta.game.allversionInfo:'+JSON.stringify(koi.globalDta.game.allversionInfo));

              if (res.data.config.login_link) {
                globalThis.intHttpUrl.loginUrl = globalThis.intHttpUrl.loginUrl.concat(res.data.config.login_link);
              }

              if (res.data.config.query_link) {
                globalThis.intHttpUrl.queryUrl = globalThis.intHttpUrl.queryUrl.concat(res.data.config.query_link);
              }

              if (res.data.config.cdn_link && Array.isArray(res.data.config.cdn_link)) {
                globalThis.intHttpUrl.CdnUrl = res.data.config.cdn_link.concat(globalThis.intHttpUrl.CdnUrl);
                sys.localStorage.setItem('CdnUrl', JSON.stringify(res.data.config.cdn_link));
              }
            }
          } catch (e) {
            console.error(e);
          }
        }

        static getCdnLink() {
          if (globalThis.intHttpUrl.CdnUrl && Array.isArray(globalThis.intHttpUrl.CdnUrl)) {
            if (globalThis.cdnIndex >= 0 && globalThis.cdnIndex < globalThis.intHttpUrl.CdnUrl.length) {
              return globalThis.intHttpUrl.CdnUrl[globalThis.cdnIndex];
            }
          }

          return '';
        }

        static changeCdnIndex() {
          globalThis.cdnIndex++;

          if (globalThis.intHttpUrl.CdnUrl && Array.isArray(globalThis.intHttpUrl.CdnUrl)) {
            if (globalThis.cdnIndex >= globalThis.intHttpUrl.CdnUrl.length) {
              globalThis.cdnIndex = 0;
            }
          }
        }

        static SaveAppConfigData() {
          sys.localStorage.setItem('IApp_config', JSON.stringify((_crd && koi === void 0 ? (_reportPossibleCrUseOfkoi({
            error: Error()
          }), koi) : koi).globalDta.game.allversionInfo));
        }

        static SetAppLocalData(dataKey, res) {
          if (res && res.data) {
            iApp_Datas[dataKey] = res;
            sys.localStorage.setItem(dataKey, JSON.stringify(res));
          }
        }

        static GetAppLocalData(dataKey) {
          try {
            if (dataKey) {
              if (iApp_Datas[dataKey]) {
                return iApp_Datas[dataKey];
              } else {
                iApp_Datas[dataKey] = JSON.parse(sys.localStorage.getItem(dataKey));
                return iApp_Datas[dataKey];
              }
            }
          } catch (e) {
            console.error(e);
          }
        }

        static LoadAppConfigData() {
          let app_config = sys.localStorage.getItem('IApp_config');

          if (app_config) {
            let localData = JSON.parse(app_config);
            (_crd && koi === void 0 ? (_reportPossibleCrUseOfkoi({
              error: Error()
            }), koi) : koi).globalDta.game.allversionInfo = localData;
            (_crd && koi === void 0 ? (_reportPossibleCrUseOfkoi({
              error: Error()
            }), koi) : koi).globalDta.game.currentGameVersion.lobby = localData.data.config.lobby_version.min;
          }
        }

        static GetAppConfigData() {
          return (_crd && koi === void 0 ? (_reportPossibleCrUseOfkoi({
            error: Error()
          }), koi) : koi).globalDta.game.allversionInfo;
        }

        static GetSubGameInfo(key) {
          try {
            let config = userData.GetAppConfigData();
            let array = config.data.config.game_version_config;

            for (let index = 0; index < array.length; index++) {
              let item = array[index];
              let obj = item[key];

              if (obj) {
                return obj;
              }
            }
          } catch (error) {
            console.error(error);
            return null;
          }
        }

        static GetNextSubGameInfo(key) {
          try {
            let config = userData.GetAppConfigData();
            let array = config.data.config.game_version_config;

            for (let index = 0; index < array.length; index++) {
              let item = array[index];
              let obj = item[key];

              if (obj) {
                if (index == array.length - 1) {
                  let temp = array[0];
                  return temp[Object.keys(temp)[0]];
                } else {
                  let temp = array[index + 1];
                  return temp[Object.keys(temp)[0]];
                }
              }
            }
          } catch (error) {
            console.error(error);
            return null;
          }
        }

        static GetPreviousSubGameInfo(key) {
          try {
            let config = userData.GetAppConfigData();
            let array = config.data.config.game_version_config;

            for (let index = 0; index < array.length; index++) {
              let item = array[index];
              let obj = item[key];

              if (obj) {
                if (index == 0) {
                  let temp = array[array.length - 1];
                  return temp[Object.keys(temp)[0]];
                } else {
                  let temp = array[index - 1];
                  return temp[Object.keys(temp)[0]];
                }
              }
            }
          } catch (error) {
            console.error(error);
            return null;
          }
        }

        static versionCompare(local, latest) {
          if (local && latest && typeof local == 'string' && typeof latest == 'string') {
            let localStr = local.split('_');
            let latestStr = latest.split('_');

            if (localStr && latestStr && localStr.length == 2 && latestStr.length == 2) {
              if (localStr[0] != latestStr[0]) {
                return true;
              } else if (localStr[0] == latestStr[0]) {
                if (localStr[1] != latestStr[1]) {
                  return true;
                }
              }
            }
          }

          return false;
        }

      };

      _export("default", userData);

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=userData.js.map
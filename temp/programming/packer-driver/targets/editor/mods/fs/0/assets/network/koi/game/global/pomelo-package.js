System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, Socket, queue, Package, _crd, __packFunc, __packListenInterval;

  function _checkMessageIn2() {
    if ((_crd && queue === void 0 ? (_reportPossibleCrUseOfqueue({
      error: Error()
    }), queue) : queue).buffer.messageIn2.length > 0) {
      var arr = (_crd && queue === void 0 ? (_reportPossibleCrUseOfqueue({
        error: Error()
      }), queue) : queue).buffer.messageIn2.splice(0);
      var err, res;

      for (let i = 0; i < arr.length; i++) {
        res = arr[i];

        for (let key in __packFunc[res.route]) {
          // console.log('queue.buffer.messageIn3',JSON.stringify(res),__packFunc[res.route][key],'__packFunc  dating','key',key,'res.route',res.route )
          err = res.code == 0 ? null : {
            code: res.code,
            message: res.message
          };

          __packFunc[res.route][key](err, res);
        }
      }
    }
  }

  function _reportPossibleCrUseOfSocket(extras) {
    _reporterNs.report("Socket", "./pomelo-socket", _context.meta, extras);
  }

  function _reportPossibleCrUseOfqueue(extras) {
    _reporterNs.report("queue", "./queue", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
    }, function (_unresolved_2) {
      Socket = _unresolved_2.default;
    }, function (_unresolved_3) {
      queue = _unresolved_3.default;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "2221c6GepxL7KsLJQBG1ikL", "pomelo-package", undefined);

      __packFunc = {};
      __packListenInterval = setInterval(_checkMessageIn2, 2); // console.log('nettestpp __packListenInterval=',__packListenInterval);

      Package = class Package {
        constructor(path, req) {
          this.path = path;
          this.req = req;
        }
        /**
         * 注册包的监听函数
         * 
         * @static
         * @param {any} cmdKind 
         * @param {any} typeObj 
         * @param {any} cb 
         * 
         * @memberof Package
         */


        static init() {
          if (globalThis.inSocket == null) {
            globalThis.inSocket = new (_crd && Socket === void 0 ? (_reportPossibleCrUseOfSocket({
              error: Error()
            }), Socket) : Socket)();
          }
        }

        static addPackListener(path, cb) {
          __packFunc[path] = __packFunc[path] || {}; // console.log('__packFunc addPackListener',JSON.stringify(__packFunc),'__packFunc addPackListener path',path)

          var random = path + '-' + new Date().getTime() + '-' + Math.random() * 100000;
          __packFunc[path][random] = cb;
          return [path, random];
        }

        static addPackListenerArr(that, listener) {
          that.__ListenerUIDs = that.__ListenerUIDs || []; // console.log(JSON.stringify(listener),that.name,'__packFunc addPackListenerArr')

          for (let i = 0; i < listener.length; i++) {
            let path = listener[i].path; // console.log(path,that.name,'__packFunc addPackListenerArr')

            that.__ListenerUIDs.push(Package.addPackListener(path, (err, res) => {
              if (typeof that[listener[i].filter] == 'function') {
                that[listener[i].filter](err, res, listener[i].cb);
              } else if (typeof that[listener[i].cb] == 'function') that[listener[i].cb](err, res);
            }));
          } // console.log('listener  __packFunc',JSON.stringify(listener))

        }

        static removePackListenerArr(that) {
          var obj;

          if (that.__ListenerUIDs && that.__ListenerUIDs.length > 0) {
            for (let i = 0; i < that.__ListenerUIDs.length; i++) {
              obj = that.__ListenerUIDs[i];
              delete __packFunc[obj[0]][obj[1]];
            }
          }
        }

        static removePackListener(path, func) {
          if (func && func.__UID) {
            delete __packFunc[path][func.__UID];
          }
        }

        send(cb) {
          if (globalThis.inSocket) {
            globalThis.inSocket.send(this.path, this.req, cb);
          }
        }

      };

      _export("default", Package);

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=pomelo-package.js.map
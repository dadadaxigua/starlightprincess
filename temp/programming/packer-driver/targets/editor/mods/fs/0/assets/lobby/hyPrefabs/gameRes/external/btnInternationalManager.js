System.register(["cc"], function (_export, _context) {
  "use strict";

  var _cclegacy, btnInternationalManager, _crd;

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  _export("btnInternationalManager", void 0);

  return {
    setters: [function (_cc) {
      _cclegacy = _cc.cclegacy;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "bb204XUz6ZJI7E9jkYHUScv", "btnInternationalManager", undefined);

      _export("btnInternationalManager", btnInternationalManager = class btnInternationalManager {
        static GetDataByKey(key) {
          if (globalThis.GetLanguageType) {
            if (!btnInternationalManager.mData) {
              btnInternationalManager.mData = btnInternationalManager.jsonData.get(globalThis.GetLanguageType());
            }

            if (btnInternationalManager.mData) {
              const value = btnInternationalManager.mData[key];

              if (value) {
                return value.text;
              }
            }
          }

          return '';
        }

        static SetLanguegeData(type, data) {
          if (type && data) {
            btnInternationalManager.jsonData.set(type, data);
          }
        }

        static Clear() {
          btnInternationalManager.mData = null;
          btnInternationalManager.jsonData.clear();
        }

      });

      _defineProperty(btnInternationalManager, "jsonData", new Map());

      _defineProperty(btnInternationalManager, "mData", void 0);

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=btnInternationalManager.js.map
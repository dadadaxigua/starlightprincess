System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2", "__unresolved_3"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, Sprite, assetMgr, sComponent, sObjPool, _dec, _dec2, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _descriptor5, _descriptor6, _descriptor7, _descriptor8, _descriptor9, _temp, _crd, ccclass, property, sAnimationFrame;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfassetMgr(extras) {
    _reporterNs.report("assetMgr", "./sAssetMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsComponent(extras) {
    _reporterNs.report("sComponent", "./sComponent", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsObjPool(extras) {
    _reporterNs.report("sObjPool", "./sObjPool", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      Sprite = _cc.Sprite;
    }, function (_unresolved_2) {
      assetMgr = _unresolved_2.assetMgr;
    }, function (_unresolved_3) {
      sComponent = _unresolved_3.sComponent;
    }, function (_unresolved_4) {
      sObjPool = _unresolved_4.sObjPool;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "c6db3tKKSlHa5SRL3y3UMiD", "sAnimationFrame", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("sAnimationFrame", sAnimationFrame = (_dec = ccclass('sAnimationFrame'), _dec2 = property(Sprite), _dec(_class = (_class2 = (_temp = class sAnimationFrame extends (_crd && sComponent === void 0 ? (_reportPossibleCrUseOfsComponent({
        error: Error()
      }), sComponent) : sComponent) {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "sprite", _descriptor, this);

          _initializerDefineProperty(this, "atlasPath", _descriptor2, this);

          _initializerDefineProperty(this, "startAnima", _descriptor3, this);

          _initializerDefineProperty(this, "speed", _descriptor4, this);

          _initializerDefineProperty(this, "enablePlayOnce", _descriptor5, this);

          _initializerDefineProperty(this, "enableLoop", _descriptor6, this);

          _initializerDefineProperty(this, "autoClose", _descriptor7, this);

          _initializerDefineProperty(this, "autoStopInLastFrame", _descriptor8, this);

          _initializerDefineProperty(this, "autoRecycle", _descriptor9, this);

          _defineProperty(this, "spriteFrames", []);

          _defineProperty(this, "index", void 0);

          _defineProperty(this, "time", void 0);

          _defineProperty(this, "timeDelta", void 0);

          _defineProperty(this, "type", void 0);

          _defineProperty(this, "playCount", 0);

          _defineProperty(this, "playCountLimit", -1);

          _defineProperty(this, "canPlay", false);

          _defineProperty(this, "onCompleteCallBack", null);

          _defineProperty(this, "onFrameCallBack", null);
        }

        onLoad() {
          this.index = 0;
          this.time = 0;
          this.timeDelta = 0.03;
          this.type = 'normal';

          if (!this.sprite) {
            this.sprite = this.node.getComponent(Sprite);
          }

          if (this.atlasPath) {
            this.spriteFrames = (_crd && assetMgr === void 0 ? (_reportPossibleCrUseOfassetMgr({
              error: Error()
            }), assetMgr) : assetMgr).GetAssetByName(this.atlasPath);
          }
        }

        start() {}

        initAtlasPath(path) {
          this.atlasPath = path;
          this.spriteFrames = (_crd && assetMgr === void 0 ? (_reportPossibleCrUseOfassetMgr({
            error: Error()
          }), assetMgr) : assetMgr).GetAssetByName(path);
        }

        onEnable() {
          if (this.enablePlayOnce) {
            this.changeSpritesByStartAnima();
            this.playOnce();
          }
        }

        changeSpritesByStartAnima() {
          if (this.startAnima) {
            this.spriteFrames = (_crd && assetMgr === void 0 ? (_reportPossibleCrUseOfassetMgr({
              error: Error()
            }), assetMgr) : assetMgr).GetAssetByName(this.startAnima);
          }
        }

        setSprites(sprites) {
          this.spriteFrames = sprites;
        }

        playOnce(callback = null, onFrameCall = null) {
          if (this.spriteFrames) {
            this.play(this.spriteFrames, 0, this.speed, 'normal', this.enableLoop, callback, onFrameCall);
          }
        }

        play(sprites, index, speed, type, playcountlimit = -1, onComplete = null, onFrameCall = null) {
          if (!this.sprite) {
            this.sprite = this.node.getComponent(Sprite);
          }

          this.sprite.spriteFrame = this.spriteFrames[index];
          this.spriteFrames = sprites;
          this.index = index + 1;
          this.timeDelta = speed * 0.01;

          if (type) {
            this.type = type;
          }

          this.playCount = 0;
          this.playCountLimit = playcountlimit;
          this.canPlay = true;
          this.onCompleteCallBack = onComplete;
          this.onFrameCallBack = onFrameCall;
        }

        getCurAnimName() {
          return this.atlasPath;
        }

        curAnimIsLoop() {
          return this.enableLoop;
        }

        update(dt) {
          if (this.spriteFrames && this.spriteFrames.length > 0 && this.canPlay) {
            if (this.type == 'normal') {
              this.time += dt;

              if (this.time >= this.timeDelta) {
                this.time = 0;

                if (this.index >= this.spriteFrames.length) {
                  this.index = 0;
                  this.playCount++;

                  if (this.playCountLimit != -1 && this.playCountLimit <= this.playCount) {
                    this.canPlay = false;

                    if (!this.autoStopInLastFrame) {
                      this.sprite.spriteFrame = null;
                    }

                    if (this.autoClose) {
                      this.node.active = false;
                    }

                    if (this.autoRecycle) {
                      (_crd && sObjPool === void 0 ? (_reportPossibleCrUseOfsObjPool({
                        error: Error()
                      }), sObjPool) : sObjPool).Enqueue(this.node.name, this.node);
                      this.node.parent = null;
                      this.node.active = false;
                    }

                    if (this.onCompleteCallBack) {
                      this.onCompleteCallBack();
                    }

                    return;
                  }
                }

                if (this.onFrameCallBack) {
                  this.onFrameCallBack(this.index);
                }

                this.sprite.spriteFrame = this.spriteFrames[this.index]; //    console.log(this.index);

                this.index++;
              }
            } else if (this.type == 'flashback') {
              this.time += dt;

              if (this.time >= this.timeDelta) {
                this.time = 0;

                if (this.index < 0) {
                  this.index = this.spriteFrames.length - 1;
                }

                this.sprite.spriteFrame = this.spriteFrames[this.index];
                this.index--;
              }
            } else if (this.type == 'count') {
              this.time += dt;

              if (this.time >= this.timeDelta) {
                this.time = 0;

                if (this.index >= this.spriteFrames.length) {
                  this.index = 0;
                  this.playCount++;

                  if (this.playCountLimit != -1 && this.playCountLimit <= this.playCount) {
                    this.canPlay = false;
                    return;
                  }
                }

                this.sprite.spriteFrame = this.spriteFrames[this.index];
                this.index++;
              }
            }
          }
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "sprite", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "atlasPath", [property], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return '';
        }
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "startAnima", [property], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return '';
        }
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "speed", [property], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return 5;
        }
      }), _descriptor5 = _applyDecoratedDescriptor(_class2.prototype, "enablePlayOnce", [property], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return false;
        }
      }), _descriptor6 = _applyDecoratedDescriptor(_class2.prototype, "enableLoop", [property], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return 1;
        }
      }), _descriptor7 = _applyDecoratedDescriptor(_class2.prototype, "autoClose", [property], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return false;
        }
      }), _descriptor8 = _applyDecoratedDescriptor(_class2.prototype, "autoStopInLastFrame", [property], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return false;
        }
      }), _descriptor9 = _applyDecoratedDescriptor(_class2.prototype, "autoRecycle", [property], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return false;
        }
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=sAnimationFrame.js.map
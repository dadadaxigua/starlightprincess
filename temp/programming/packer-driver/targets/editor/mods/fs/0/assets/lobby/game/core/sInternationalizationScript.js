System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, sComponent, sInternationalManager, _dec, _class, _class2, _descriptor, _descriptor2, _descriptor3, _temp, _crd, ccclass, property, sInternationalizationScript;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfsComponent(extras) {
    _reporterNs.report("sComponent", "./sComponent", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsInternationalManager(extras) {
    _reporterNs.report("sInternationalManager", "./sInternationalManager", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
    }, function (_unresolved_2) {
      sComponent = _unresolved_2.sComponent;
    }, function (_unresolved_3) {
      sInternationalManager = _unresolved_3.sInternationalManager;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "b1fa4Lcg7pEWoYMQF522OFP", "sInternationalizationScript", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("sInternationalizationScript", sInternationalizationScript = (_dec = ccclass('sInternationalizationScript'), _dec(_class = (_class2 = (_temp = class sInternationalizationScript extends (_crd && sComponent === void 0 ? (_reportPossibleCrUseOfsComponent({
        error: Error()
      }), sComponent) : sComponent) {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "componentName", _descriptor, this);

          _initializerDefineProperty(this, "propertyName", _descriptor2, this);

          _initializerDefineProperty(this, "key", _descriptor3, this);
        }

        onLoad() {}

        init() {
          let _label = this.node.getComponent(this.componentName);

          if (_label) {
            _label[this.propertyName] = (_crd && sInternationalManager === void 0 ? (_reportPossibleCrUseOfsInternationalManager({
              error: Error()
            }), sInternationalManager) : sInternationalManager).GetDataByKey(this.key);
          }
        }

        start() {
          this.init();
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "componentName", [property], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return 'cc.Label';
        }
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "propertyName", [property], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return 'string';
        }
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "key", [property], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return '';
        }
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=sInternationalizationScript.js.map
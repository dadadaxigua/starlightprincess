System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2", "__unresolved_3", "__unresolved_4", "__unresolved_5"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, Node, Sprite, Label, v3, Vec3, tween, Color, color, UIOpacity, director, sAudioMgr, sComponent, sNewPokerCardBase, sObjPool, sUtil, _dec, _dec2, _dec3, _dec4, _dec5, _dec6, _dec7, _dec8, _dec9, _dec10, _dec11, _dec12, _dec13, _dec14, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _descriptor5, _descriptor6, _descriptor7, _descriptor8, _descriptor9, _descriptor10, _descriptor11, _descriptor12, _descriptor13, _temp, _crd, ccclass, property, sNewPokerPlayerEntity;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfsAudioMgr(extras) {
    _reporterNs.report("sAudioMgr", "./sAudioMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsComponent(extras) {
    _reporterNs.report("sComponent", "./sComponent", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsNewPokerCardBase(extras) {
    _reporterNs.report("sNewPokerCardBase", "./sNewPokerCardBase", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsObjPool(extras) {
    _reporterNs.report("sObjPool", "./sObjPool", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsPokerChip(extras) {
    _reporterNs.report("sPokerChip", "./sPokerChip", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsUtil(extras) {
    _reporterNs.report("sUtil", "./sUtil", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      Node = _cc.Node;
      Sprite = _cc.Sprite;
      Label = _cc.Label;
      v3 = _cc.v3;
      Vec3 = _cc.Vec3;
      tween = _cc.tween;
      Color = _cc.Color;
      color = _cc.color;
      UIOpacity = _cc.UIOpacity;
      director = _cc.director;
    }, function (_unresolved_2) {
      sAudioMgr = _unresolved_2.default;
    }, function (_unresolved_3) {
      sComponent = _unresolved_3.sComponent;
    }, function (_unresolved_4) {
      sNewPokerCardBase = _unresolved_4.sNewPokerCardBase;
    }, function (_unresolved_5) {
      sObjPool = _unresolved_5.sObjPool;
    }, function (_unresolved_6) {
      sUtil = _unresolved_6.sUtil;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "55003k9MblBz4jeI2+dRdBH", "sNewPokerPlayerEntity", undefined);

      ({
        ccclass,
        property
      } = _decorator); // export interface PokerUserDataModel {
      //     avatar : string
      //     nickName : string
      //     coin : number
      //     userState : number
      //     state : number
      //     handPokers : string[]
      //     input : number
      // }

      _export("sNewPokerPlayerEntity", sNewPokerPlayerEntity = (_dec = ccclass('sNewPokerPlayerEntity'), _dec2 = property({
        tooltip: '玩家手牌数量'
      }), _dec3 = property({
        tooltip: '玩家手牌的位置间隔',
        min: 10
      }), _dec4 = property({
        type: Sprite,
        tooltip: '玩家头像精灵对象'
      }), _dec5 = property({
        type: Label,
        tooltip: '玩家昵称字体对象'
      }), _dec6 = property({
        type: Label,
        tooltip: '玩家金币字体对象'
      }), _dec7 = property({
        type: Node,
        tooltip: '玩家打出牌的父节点'
      }), _dec8 = property({
        type: Node,
        tooltip: '玩家手牌的父节点'
      }), _dec9 = property({
        type: Node,
        tooltip: '发牌位置节点'
      }), _dec10 = property({
        type: Sprite,
        tooltip: '倒计时精灵'
      }), _dec11 = property({
        type: UIOpacity,
        tooltip: '玩家下注节点'
      }), _dec12 = property({
        type: Node,
        tooltip: '玩家金币数值提示节点'
      }), _dec13 = property({
        type: Node,
        tooltip: '玩家操作的提示动画'
      }), _dec14 = property({
        type: Node,
        tooltip: '牌型提示'
      }), _dec(_class = (_class2 = (_temp = class sNewPokerPlayerEntity extends (_crd && sComponent === void 0 ? (_reportPossibleCrUseOfsComponent({
        error: Error()
      }), sComponent) : sComponent) {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "handCardNum", _descriptor, this);

          _initializerDefineProperty(this, "xSpace", _descriptor2, this);

          _initializerDefineProperty(this, "avatarSprite", _descriptor3, this);

          _initializerDefineProperty(this, "nickNameLabel", _descriptor4, this);

          _initializerDefineProperty(this, "moyLabel", _descriptor5, this);

          _initializerDefineProperty(this, "cardsContainer", _descriptor6, this);

          _initializerDefineProperty(this, "handsCardsPosNode", _descriptor7, this);

          _initializerDefineProperty(this, "sendCardStartPosNode", _descriptor8, this);

          _initializerDefineProperty(this, "headAnima", _descriptor9, this);

          _initializerDefineProperty(this, "betInfo", _descriptor10, this);

          _initializerDefineProperty(this, "coinTipNode", _descriptor11, this);

          _initializerDefineProperty(this, "userActionTypeTipNode", _descriptor12, this);

          _initializerDefineProperty(this, "cardTypeTipNode", _descriptor13, this);

          _defineProperty(this, "userData", null);

          _defineProperty(this, "isSelf", false);

          _defineProperty(this, "pokerCards", []);

          _defineProperty(this, "chipsObj", []);

          _defineProperty(this, "playingMode", '');
        }

        /**
         * @zh
         * 玩家数据模型
         * {uid : 玩家uid,avatar : 头像编号,nick_name : 昵称,coin : 拥有的钱, userState : 0未准备 1准备 2游戏中,state : 0离开 1在桌子上,playMode : throw弃牌 , play游戏中,none无状态,handPokers : 手牌数组}
         */
        get UserData() {
          if (!this.userData) {
            this.userData = {
              coin: 0,
              playMode: 'none'
            };
          }

          return this.userData;
        }
        /**
         * @zh
         * 该对象是否是玩家自己
         */


        get IsSelf() {
          return this.isSelf;
        }

        set IsSelf(value) {
          this.isSelf = value;
        }
        /**
         * @zh
         * 玩家本局下注的金币总额
         */


        get BetCoin() {
          return this.UserData.input;
        }

        set BetCoin(value) {
          this.UserData.input = value;
        }
        /**
         * @zh
         * 玩家在游戏中
         */


        get IsPlaying() {
          return this.UserData.userState == 2;
        }
        /**
         * @zh
         * 玩家是否弃牌
         */


        get IsThrow() {
          return this.UserData.playMode == 'throw';
        }

        onLoad() {}

        start() {}
        /**
         * @zh
         * 玩家数据赋值
         * @param avatarStr - 头像编号
         * @param nickName - 玩家昵称
         * @param coinNum - 玩家金币数值
         */


        userInfoDataSet(avatarStr = null, nickName = null, coinNum = -1) {
          this.UserData.avatar = avatarStr ? avatarStr : '0';
          this.UserData.nick_name = nickName ? nickName : '';
          this.UserData.coin = coinNum == -1 ? 0 : coinNum;
        }
        /**
         * @zh
         * 玩家数据赋值
         * data : {avatar : 头像编号,nickName : 昵称,coin : 拥有的钱,handPokers : 玩家手牌数组,input:本轮下注总数,playMode : 游戏状态}
         */


        userInfoDataSetByModel(data) {
          if (data) {
            this.UserData.avatar = data.avatar ? data.avatar : '0';
            this.UserData.nick_name = data.nickName ? data.nickName : '';
            this.UserData.coin = data.coin || data.coin == 0 ? data.coin : 0;
            this.UserData.handPokers = data.handPokers ? data.handPokers : null;
            this.UserData.userState = data.userState || data.userState == 0 ? data.userState : -1;
            this.UserData.input = data.input || data.input == 0 ? data.input : 0;
            this.UserData.playMode = data.playMode;
            this.UserData.uid = data.uid;
          }
        }
        /**
         * @zh
         * 改变玩家金币数值
         * @param coinNum - 玩家金币数值
         * @param anima - 是否播放动画
         */


        userInfoChangeCoin(coinNum, anima = false, delayTime = 0) {
          this.removeIDTween('userInfoChangeCoin');
          const nowCoin = this.UserData.coin;
          this.UserData.coin += coinNum;

          if (anima) {
            this.pushIDTween((_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
              error: Error()
            }), sUtil) : sUtil).TweenLabel(nowCoin, this.UserData.coin, this.moyLabel, 0.4, 2, 'quadOut', delayTime), 'userInfoChangeCoin');
          } else {
            this.moyLabel.string = (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
              error: Error()
            }), sUtil) : sUtil).changeNumberToKW(this.UserData.coin, 2);
          }
        }
        /**
         * @zh
         * 刷新界面上玩家的信息显示
         */


        userInfoViewUpdate() {
          try {
            if (this.UserData.state == 0) {
              this.node.active = false;
            } else if (this.UserData.state == 1) {
              this.node.active = true;
              this.avatarSprite.spriteFrame = globalThis.getAvatarSprite(this.UserData.avatar);
              this.nickNameLabel.string = this.UserData.nick_name;
              this.moyLabel.string = (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                error: Error()
              }), sUtil) : sUtil).changeNumberToKW(this.UserData.coin, 2);
            }
          } catch (e) {
            console.error(e);
          }
        }
        /**
         * @zh
         * 玩家离桌
         */


        LeaveDesk() {
          this.UserData.state = 0;
          this.isSelf = false;
        }
        /**
         * @zh
         * 清理玩家关联对象
         */


        cleanTable() {
          this.unscheduleAllCallbacks();
          this.cleanTweenDic();
          this.cleanTweenList();
          this.HeadLightAnimaCtr(false);
          this.BetCoin = 0;
          this.UserData.userState = 0;

          if (this.pokerCards && this.pokerCards.length) {
            for (let c = 0; c < this.pokerCards.length; c++) {
              const card = this.pokerCards[c];
              card.recycleCard();
            }

            this.pokerCards = [];
          }

          this.betInfo.node.active = false;
          this.coinTipNode.active = false;
        }
        /**
         * @zh
         * 玩家操作
         * @param actionType - 操作类型
         * @param coin - 操作金币
         */


        UserTurnAction(actionType, coin, base, tableTotalBet = 0) {
          this.HeadLightAnimaCtr(false);

          if (actionType == 'throw') {
            this.UserData.playMode = 'throw';
            this.QuitGame();
          } else if (actionType == 'flow') {
            this.FollowBet(coin, base);
            this.userInfoChangeCoin(coin);
          } else if (actionType == 'add') {
            this.AddBet(coin, base);
            this.userInfoChangeCoin(coin);
          }

          this.BetCoin += coin; // this.userInfoChangeCoin(coin);
          // this.UserData.coin += coin;
        } //-------optional implementation

        /**
         * @zh
         * 玩家进桌
         * @param uid 玩家uid
         * @param stage 桌子状态 0: 空闲状态（桌子里没有人） 1: 等待状态 2：游戏中 3：结算状态 4：结算结束（等待开局）
         */


        EnterDesk(uid, stage) {
          this.UserData.state = 1; // const userInfo = globalThis.getUserInfo();
          // if(userInfo && userInfo.uid == uid){
          //     this.isSelf = true;
          // }else{
          //     this.isSelf = false;
          // }
        }

        GetOneCard(point) {
          let card = (_crd && sObjPool === void 0 ? (_reportPossibleCrUseOfsObjPool({
            error: Error()
          }), sObjPool) : sObjPool).Dequeue('pokerCard');

          if (card) {
            card.setParent(this.cardsContainer, true);
            const pos = this.sendCardStartPosNode ? this.sendCardStartPosNode.worldPosition : Vec3.ZERO;
            card.worldPosition = pos;
            let cardBase = card.getComponent(_crd && sNewPokerCardBase === void 0 ? (_reportPossibleCrUseOfsNewPokerCardBase({
              error: Error()
            }), sNewPokerCardBase) : sNewPokerCardBase);

            if (cardBase) {
              cardBase.cleanTweenList();
              cardBase.updatePoint(point);
              cardBase.State = 'inHand';
              this.pokerCards.push(cardBase);
              return cardBase;
            }
          }
        }
        /**
         * @zh
         * 整理手中的扑克牌
         * @param poseType : string - 发牌到目标位置的摆放类型 anima : 以动画的形式整理牌或者是直接整理好,order:摆放整齐,random:摆放杂乱,rotate:是否在动画中需要旋转
         */


        MakeCardUp(anima = true, poseType = 'order', rotate = false) {
          if (this.pokerCards && this.pokerCards.length > 0) {
            const sendCardStartDelayTime = 0.3; // const del = this.PokerCards.length % 2 == 0 ? this.PokerCards.length / 2 - 0.5 : Math.trunc(this.PokerCards.length / 2);

            for (let c = 0; c < this.pokerCards.length; c++) {
              const card = this.pokerCards[c];
              card.node.setParent(this.cardsContainer, true);
              card.node.active = true;
              card.openBackCard();
              card.State = 'inHand';

              if (anima) {
                const pos = this.sendCardStartPosNode ? this.sendCardStartPosNode.worldPosition : Vec3.ZERO;
                card.node.worldPosition = pos;
                card.node.scale = Vec3.ZERO;

                if (poseType == 'order') {
                  const targetPos = v3(this.xSpace * c, 0, 0).add(this.handsCardsPosNode.position);
                  card.pushOneTween(tween(card.node).delay(c * sendCardStartDelayTime).call(() => {
                    card.node.active = true;

                    if (this.IsSelf) {
                      (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                        error: Error()
                      }), sAudioMgr) : sAudioMgr).PlayShotAudio('sendCard', 0.5);
                    }
                  }).to(0.1, {
                    position: targetPos,
                    scale: Vec3.ONE
                  }, {
                    easing: 'circOut'
                  }).call(() => {
                    if (this.isSelf) {
                      card.openForeCard('flip');
                    }
                  }).start());
                } else if (poseType == 'random') {
                  const randomPos = v3((_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                    error: Error()
                  }), sUtil) : sUtil).Random(-20, 20), (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                    error: Error()
                  }), sUtil) : sUtil).Random(-10, 10), 0).add(this.handsCardsPosNode.position);
                  let rotateValue = 0;

                  if (rotate) {
                    rotateValue = 360;
                    card.pushOneTween(tween(card.node).delay(c * sendCardStartDelayTime).by(0.25, {
                      eulerAngles: v3(0, 0, rotateValue + (poseType == 'random' ? (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                        error: Error()
                      }), sUtil) : sUtil).Random(-60, 60) : 0))
                    }, {
                      easing: 'circOut'
                    }).start());
                  }

                  card.pushOneTween(tween(card.node).delay(c * sendCardStartDelayTime).call(() => {
                    card.node.active = true;

                    if (this.IsSelf) {
                      (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                        error: Error()
                      }), sAudioMgr) : sAudioMgr).PlayShotAudio('sendCard', 0.5);
                    }
                  }).to(0.3, {
                    position: randomPos,
                    scale: Vec3.ONE
                  }, {
                    easing: 'circOut'
                  }).call(() => {
                    if (c == this.pokerCards.length - 1) {
                      for (let z = 0; z < this.pokerCards.length; z++) {
                        const zCard = this.pokerCards[z];
                        const targetPos = v3(this.xSpace * z, 0, 0).add(this.handsCardsPosNode.position);
                        zCard.pushOneTween(tween(zCard.node).to(0.25, {
                          position: targetPos,
                          eulerAngles: v3(0, 0, rotateValue)
                        }).delay(z * 0.1).call(() => {
                          if (this.isSelf) {
                            zCard.openForeCard('flip');
                          }
                        }).start());
                      }
                    }
                  }).start());
                }
              } else {
                card.node.position = v3(this.xSpace * c, 0, 0).add(this.handsCardsPosNode.position);

                if (this.isSelf) {
                  card.openForeCard();
                } else {
                  card.openBackCard();
                }
              }
            }
          }
        }
        /**
         * @zh
         * 游戏开始
         * @param cardData 发到玩家手上的牌 [string] 对于除自己外的玩家 默认发固定数量牌背的牌
         * @param cardAnima 是否需要播放动画
         */


        GameStart(cardData = null, cardAnima = true) {
          this.cleanTweenList();
          this.UserData.playMode = 'playing';
          this.UserData.userState = 2;

          if (this.isSelf) {
            if (cardData && cardData.length > 0) {
              for (let i = 0; i < cardData.length; i++) {
                this.GetOneCard(cardData[i]);
              }
            }
          } else {
            for (let i = 0; i < this.handCardNum; i++) {
              this.GetOneCard('0');
            }
          }

          this.MakeCardUp(cardAnima, 'random', true);
        }
        /**
         * @zh
         * 游戏结束
         * @param handCardData 玩家手牌
         * @param winCoin 输赢金币数
         */


        GameEnd(handCardData, winCoin, endType = '') {
          this.UserData.userState = 0;
          this.HeadLightAnimaCtr(false);
        }
        /**
         * @zh
         * 轮到当前玩家对象操作
         */


        UserGetTurn() {
          this.HeadLightAnimaCtr(true);
        }
        /**
         * @zh
         * 玩家操作倒计时动画
         * @param value true : 开始动画,false : 关闭动画
         */


        HeadLightAnimaCtr(value) {
          if (value) {
            this.headAnima.node.active = true;
            this.headAnima.fillRange = 1;
            this.headAnima.color = color(112, 255, 0);
            this.removeIDTween('HeadLightAnimaCtr');
            const colorTween = (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
              error: Error()
            }), sUtil) : sUtil).TweenColor(Color.RED, this.headAnima, 10);

            if (colorTween) {
              this.pushIDTween(colorTween, 'HeadLightAnimaCtrColor');
            }

            this.pushIDTween(tween(this.headAnima).to(10, {
              fillRange: 0
            }).start(), 'HeadLightAnimaCtr');
            this.pushIDSchedule(() => {
              (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                error: Error()
              }), sAudioMgr) : sAudioMgr).PlayShotAudio('timeUp');
            }, 7, 'HeadLightAnimaCtrSchedule');
          } else {
            this.removeIDSchedule('HeadLightAnimaCtrSchedule');
            this.removeIDTween('HeadLightAnimaCtr');
            this.removeIDTween('HeadLightAnimaCtrColor');
            this.headAnima.node.active = false;
          }
        }
        /**
         * @zh
         * 玩家下注显示动画
         * @param value 金币数额
         * @param base 当前底注
         * @param anima 是否播放动画
         */


        ShowBetInfo(value, base, anima = true) {
          if (this.betInfo && value != 0) {
            const nowBetCoin = this.BetCoin;
            (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
              error: Error()
            }), sAudioMgr) : sAudioMgr).PlayShotAudio('chipOut');
            this.betInfo.node.active = true;
            this.betInfo.opacity = 255;
            const betLabel = this.betInfo.node.getChildByName('Label').getComponent(Label);
            this.removeIDTween('ShowBetInfo');

            if (anima) {
              director.emit('pokerBetChip', Math.trunc(Math.abs(value / base)), v3(this.node.worldPosition));
              this.pushIDTween((_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                error: Error()
              }), sUtil) : sUtil).TweenLabel(Math.abs(nowBetCoin), Math.abs(this.BetCoin + value), betLabel, 0.4, 0), 'ShowBetInfo');
            } else {
              betLabel.string = (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                error: Error()
              }), sUtil) : sUtil).AddCommas(Math.abs(this.BetCoin));
            }
          }
        }
        /**
         * @zh
         * 玩家弃牌
         */


        QuitGame() {}
        /**
         * @zh
         * 玩家加注
         */


        AddBet(coin, base) {
          this.ShowBetInfo(coin, base, true);
        }
        /**
         * @zh
         * 玩家跟住
         */


        FollowBet(coin, base) {
          this.ShowBetInfo(coin, base, true);
        }
        /**
         * @zh
         * 玩家下注金币数额提示动画
         */


        coinTipAnima(coin) {
          if (this.coinTipNode) {
            this.coinTipNode.active = true;
            const lose = this.coinTipNode.getChildByName('lose');
            const win = this.coinTipNode.getChildByName('win');

            if (lose && win) {
              lose.active = false;
              win.active = false;

              if (coin >= 0) {
                win.active = true;
                const label = win.getComponent(Label);

                if (label) {
                  label.string = '+' + (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                    error: Error()
                  }), sUtil) : sUtil).AddCommas(coin);
                }
              } else {
                lose.active = true;
                const label = lose.getComponent(Label);

                if (label) {
                  label.string = (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                    error: Error()
                  }), sUtil) : sUtil).AddCommas(coin);
                }
              }
            }
          }
        } //-------end implementation
        //--------need implementation

        /**
         * @zh
         * 玩家牌型提示动画
         * @param type sanpai：散牌 huangjia：皇家 shunzi：顺子 tonghua：同花顺 baozi：豹子
         * @param delayTime 延迟关闭时间 : 0:不关闭 >0 : 定时关闭
         */


        cardTypeTipTip(type, delayTime = 0) {}
        /**
         * @zh
         * 玩家操作提示动画
         * @param type 弃牌:throw 跟注:flow 加注:add 一倍底注:raiseX1 两倍底注:raiseX2 五倍底注:raiseX5 二分之一注池:pot1_2 全部注池:potAll
         */


        userActionTypeTip(type) {} //--------end implementation


      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "handCardNum", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return 3;
        }
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "xSpace", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return 30;
        }
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "avatarSprite", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "nickNameLabel", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor5 = _applyDecoratedDescriptor(_class2.prototype, "moyLabel", [_dec6], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor6 = _applyDecoratedDescriptor(_class2.prototype, "cardsContainer", [_dec7], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor7 = _applyDecoratedDescriptor(_class2.prototype, "handsCardsPosNode", [_dec8], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor8 = _applyDecoratedDescriptor(_class2.prototype, "sendCardStartPosNode", [_dec9], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor9 = _applyDecoratedDescriptor(_class2.prototype, "headAnima", [_dec10], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor10 = _applyDecoratedDescriptor(_class2.prototype, "betInfo", [_dec11], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor11 = _applyDecoratedDescriptor(_class2.prototype, "coinTipNode", [_dec12], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor12 = _applyDecoratedDescriptor(_class2.prototype, "userActionTypeTipNode", [_dec13], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor13 = _applyDecoratedDescriptor(_class2.prototype, "cardTypeTipNode", [_dec14], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=sNewPokerPlayerEntity.js.map
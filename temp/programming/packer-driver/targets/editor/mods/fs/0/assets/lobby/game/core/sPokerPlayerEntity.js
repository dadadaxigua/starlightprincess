System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2", "__unresolved_3"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, Node, director, sAudioMgr, sComponent, sObjPool, _dec, _dec2, _dec3, _dec4, _dec5, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _temp, _crd, ccclass, property, sPokerPlayerEntity;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfsAudioMgr(extras) {
    _reporterNs.report("sAudioMgr", "./sAudioMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsComponent(extras) {
    _reporterNs.report("sComponent", "./sComponent", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsObjPool(extras) {
    _reporterNs.report("sObjPool", "./sObjPool", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsPokerCardBase(extras) {
    _reporterNs.report("sPokerCardBase", "./sPokerCardBase", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      Node = _cc.Node;
      director = _cc.director;
    }, function (_unresolved_2) {
      sAudioMgr = _unresolved_2.default;
    }, function (_unresolved_3) {
      sComponent = _unresolved_3.sComponent;
    }, function (_unresolved_4) {
      sObjPool = _unresolved_4.sObjPool;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "27a9cLWFohMYr7E1pNqlYIJ", "sPokerPlayerEntity", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("sPokerPlayerEntity", sPokerPlayerEntity = (_dec = ccclass('sPokerPlayerEntity'), _dec2 = property(Node), _dec3 = property(Node), _dec4 = property(Node), _dec5 = property(Node), _dec(_class = (_class2 = (_temp = class sPokerPlayerEntity extends (_crd && sComponent === void 0 ? (_reportPossibleCrUseOfsComponent({
        error: Error()
      }), sComponent) : sComponent) {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "userInfoNode", _descriptor, this);

          _initializerDefineProperty(this, "cardsContainer", _descriptor2, this);

          _initializerDefineProperty(this, "handsCardsPosNode", _descriptor3, this);

          _initializerDefineProperty(this, "tableCardsPosNode", _descriptor4, this);

          _defineProperty(this, "userData", null);

          _defineProperty(this, "pokerCards", []);

          _defineProperty(this, "tablePokerCards", []);

          _defineProperty(this, "actionArr", {});

          _defineProperty(this, "kangUser", false);

          _defineProperty(this, "sexType", 0);

          _defineProperty(this, "playerDeskState", 'none');
        }

        get PokerCards() {
          return this.pokerCards;
        }

        get TableCards() {
          return this.tablePokerCards;
        }

        get UserData() {
          return this.userData;
        }

        set UserData(value) {
          this.userData = value;
        }

        get TableCardsWorldPos() {
          return this.tableCardsPosNode.worldPosition;
        }

        get IsKangUser() {
          return this.kangUser;
        }

        get BetCoin() {
          return 0;
        }

        start() {
          director.on('pokerLeaveSuc', () => {
            this.node.active = false;
          }, this);
        }

        GameStart() {}

        GameStartCd() {}

        ReadyAction(state) {}

        CleanTable() {
          this.kangUser = false;

          if (this.pokerCards && this.pokerCards.length) {
            for (let c = 0; c < this.pokerCards.length; c++) {
              const card = this.pokerCards[c];
              card.clearCard();
              (_crd && sObjPool === void 0 ? (_reportPossibleCrUseOfsObjPool({
                error: Error()
              }), sObjPool) : sObjPool).Enqueue('pokerCard', card.node);
            }

            this.pokerCards = [];
          }

          if (this.tablePokerCards && this.tablePokerCards.length) {
            for (let c = 0; c < this.tablePokerCards.length; c++) {
              const card = this.tablePokerCards[c];
              card.clearCard();
              (_crd && sObjPool === void 0 ? (_reportPossibleCrUseOfsObjPool({
                error: Error()
              }), sObjPool) : sObjPool).Enqueue('pokerCard', card.node);
            }

            this.tablePokerCards = [];
          }
        }

        PlayerEnterDesk(userinfo, state = null) {
          this.userData = userinfo;

          if (state) {
            this.playerDeskState = state;
          } else {
            this.playerDeskState = 'playing';
          }
        }

        UserInfoView(value = null) {}

        UserInfoUpdate() {}

        UserCoinUpdate() {}

        UserGetTurn() {}

        UserDrawCard(res) {}

        InitHandCards() {}

        UserTurnAction(res, lastPlayer) {}

        PokerCardsTouchEnable(value, cdt = null) {}

        SetAutoPlayMode(value) {}

        HasSameCard(cardData) {
          for (let i = 0; i < this.pokerCards.length; i++) {
            const card = this.pokerCards[i];

            if (card) {
              if (card.CardPoint == cardData) {
                return true;
              }
            }
          }

          return false;
        }

        DiscardVoice(cards) {
          if (globalThis.getCountry && globalThis.getCountry() == 'THA') {
            if (cards && Array.isArray(cards) && cards.length > 0) {
              const card = parseInt(cards[0]);

              if (card) {
                const cardType = Math.trunc(card / 100);
                const cardIndex = card % 100;
                const len = cards.length;
                let audioStr = '';

                if (this.sexType == 0) {
                  audioStr += 'man_poker_' + len + '_';
                } else {
                  audioStr += 'woman_poker_' + len + '_';
                }

                if (cardIndex >= 2 && cardIndex <= 10) {
                  audioStr += cardIndex;
                } else {
                  if (cardIndex == 1) {
                    audioStr += 'A';
                  } else if (cardIndex == 11) {
                    audioStr += 'J';
                  } else if (cardIndex == 12) {
                    audioStr += 'Q';
                  } else if (cardIndex == 13) {
                    audioStr += 'K';
                  }
                }

                (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                  error: Error()
                }), sAudioMgr) : sAudioMgr).PlayShotAudio(audioStr);
              }
            }
          }
        }

        LeaveDesk(reason = 0) {
          this.node.active = false;
        }
        /**
         * @zh
         * 注册以名字为key的行为
         * @param _name - 行为名称
         * @param call - 行为回调
         */


        RegisterEnitityAction(_name, call) {
          if (_name && call) {
            this.actionArr[_name] = call;
          }
        }
        /**
         * @zh
         * 触发以名字为key的行为
         * @param _name - 行为名称
         * @param param - 参数
         */


        DoEnitityAction(_name, param = null) {
          const action = this.actionArr[_name];

          if (action) {
            action(param);
          }
        }

        GameStartCardsSend(res) {}

        GetOneCard(point) {
          return null;
        }

        ShowCards(data) {}

        CloseTableCards() {
          if (this.tablePokerCards && this.tablePokerCards.length > 0) {}
        }

        ShowHand(pokers, coin, type = null) {}

        FoldHandCards() {}

        PlayerWin(param = null) {}

        ShowBetInfo(value, rate, anima = true) {}

        CloseBetInfo() {}

        GetBetChipsObj() {
          return null;
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "userInfoNode", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "cardsContainer", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "handsCardsPosNode", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "tableCardsPosNode", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=sPokerPlayerEntity.js.map
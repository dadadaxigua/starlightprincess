System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2", "__unresolved_3", "__unresolved_4", "__unresolved_5", "__unresolved_6"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, director, sAudioMgr, sBoxMgr, sComponent, sConfigMgr, sHorizontalBoxMgr, sUtil, _dec, _class, _class2, _descriptor, _descriptor2, _class3, _temp, _crd, ccclass, property, sGameMgrBase;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfsAudioMgr(extras) {
    _reporterNs.report("sAudioMgr", "./sAudioMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsBoxMgr(extras) {
    _reporterNs.report("sBoxMgr", "./sBoxMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsComponent(extras) {
    _reporterNs.report("sComponent", "./sComponent", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsConfigMgr(extras) {
    _reporterNs.report("sConfigMgr", "./sConfigMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsGameFlowBase(extras) {
    _reporterNs.report("sGameFlowBase", "./sGameFlowBase", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsHorizontalBoxMgr(extras) {
    _reporterNs.report("sHorizontalBoxMgr", "./sHorizontalBoxMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsUtil(extras) {
    _reporterNs.report("sUtil", "./sUtil", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      director = _cc.director;
    }, function (_unresolved_2) {
      sAudioMgr = _unresolved_2.default;
    }, function (_unresolved_3) {
      sBoxMgr = _unresolved_3.sBoxMgr;
    }, function (_unresolved_4) {
      sComponent = _unresolved_4.sComponent;
    }, function (_unresolved_5) {
      sConfigMgr = _unresolved_5.sConfigMgr;
    }, function (_unresolved_6) {
      sHorizontalBoxMgr = _unresolved_6.sHorizontalBoxMgr;
    }, function (_unresolved_7) {
      sUtil = _unresolved_7.sUtil;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "73629A9/ElJp47gnKUGCGJK", "sGameMgrBase", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("sGameMgrBase", sGameMgrBase = (_dec = ccclass('sGameMgrBase'), _dec(_class = (_class2 = (_temp = _class3 = class sGameMgrBase extends (_crd && sComponent === void 0 ? (_reportPossibleCrUseOfsComponent({
        error: Error()
      }), sComponent) : sComponent) {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "needJackpot", _descriptor, this);

          _initializerDefineProperty(this, "jackpotReplaceSymbol", _descriptor2, this);

          _defineProperty(this, "gameFlowBaseObj", {});

          _defineProperty(this, "currentGameFlow", null);
        }

        onLoad() {
          sGameMgrBase.instance = this;
          globalThis.slotJackpotDate = {};

          if (globalThis.getCurJackPotId) {
            const jackpotID = globalThis.getCurJackPotId();

            if (globalThis.jackPotBonusEnable(jackpotID)) {
              globalThis.slotJackpotDate.enable = true;
            }
          }

          director.on('sJackpotEntityViewCtr', (id, enable) => {
            if (globalThis.getCurJackPotId && globalThis.getCurJackPotId() == id) {
              if (enable) {
                globalThis.slotJackpotDate.enable = true;
              } else {
                globalThis.slotJackpotDate.enable = false;
              }
            }
          }, this);
          director.on('betBtnClick', betSpeedMode => {
            this.GameBegin(betSpeedMode);
          }, this);
          director.on('oneBetEndRes', (res, type, clickMode) => {
            this.OneBetEndRes(res, type, clickMode);
          }, this);
          director.on('backToHall', () => {
            sGameMgrBase.instance = null;
          }, this);
          director.on('rollStopEndTrigger', () => {
            if (this.needJackpot && globalThis.slotJackpotDate && globalThis.slotJackpotDate.jackpot_amount > 0 && globalThis.slotJackpotDate.bingo) {
              const jackpotDate = globalThis.slotJackpotDate;
              (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                error: Error()
              }), sAudioMgr) : sAudioMgr).PlayShotAudio('jackpot_voice');
              (_crd && sBoxMgr === void 0 ? (_reportPossibleCrUseOfsBoxMgr({
                error: Error()
              }), sBoxMgr) : sBoxMgr).instance.DoAllBoxEntityActon('jackpotBingo');
              (_crd && sHorizontalBoxMgr === void 0 ? (_reportPossibleCrUseOfsHorizontalBoxMgr({
                error: Error()
              }), sHorizontalBoxMgr) : sHorizontalBoxMgr).instance && (_crd && sHorizontalBoxMgr === void 0 ? (_reportPossibleCrUseOfsHorizontalBoxMgr({
                error: Error()
              }), sHorizontalBoxMgr) : sHorizontalBoxMgr).instance.DoAllBoxEntityActon('jackpotBingo');
              (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                error: Error()
              }), sUtil) : sUtil).once('slotJackPotEtClose', () => {
                director.emit('rollStopEndTriggerFinish');
              });
              this.scheduleOnce(() => {
                director.emit('slotJackPotBingo', jackpotDate.jackpot_amount);
              }, 2);
            } else {
              director.emit('rollStopEndTriggerFinish');
            }
          }, this); // director.on('configLoadSuc',()=>{
          //     sConfigMgr.instance.SetTargetRoundData(1,1,100,0);
          // let boxRoundResData = null;
          // let boxRoundResDataStr = null;
          // boxRoundResDataStr = sys.localStorage.getItem('subGameBoxRoundResData_'+globalThis.currentPlayingGameID);
          // if(boxRoundResDataStr){
          //     boxRoundResData = JSON.parse(boxRoundResDataStr);
          //     const roundRes = sConfigMgr.instance.SetOneRoundData(boxRoundResData);
          //     const rounds = sConfigMgr.instance.GetRoundData();
          //     console.log('boxRoundResData:',rounds);
          // }
          // },this);
        }

        start() {
          if (this.needJackpot) {
            globalThis.getSubGamePrefabByPath('jackpot/UIJackPotEct', newObj => {
              if (newObj) {
                newObj.parent = this.node;
                newObj.active = true;
              }
            });
          }
        } //-----external call begin------
        //set all type of game to mgrbase  ex : normal , freeWin ...


        RegisterGameFlowBase(type, flow) {
          this.gameFlowBaseObj[type] = flow;
        } //-----external call end--------
        //-----internal call begin------


        GameBegin(betSpeedMode) {
          // sAudioMgr.PlayAudio('roll',true);
          director.emit('soltRollBeagin', betSpeedMode);
        } //-----internal call end--------
        //-----message call function begin------


        OneBetEndRes(res, type, clickMode) {
          const roundRes = (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
            error: Error()
          }), sConfigMgr) : sConfigMgr).instance.SetOneRoundData(res);
          const rounds = (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
            error: Error()
          }), sConfigMgr) : sConfigMgr).instance.GetRoundData(); // const seq = sConfigMgr.instance.CurrentSeq;

          if (roundRes && rounds && rounds.length > 0) {
            // res.currentSeq = seq;
            // sys.localStorage.setItem('subGameBoxRoundResData_'+globalThis.currentPlayingGameID,JSON.stringify(res));
            let flowType = 'normalFlow';
            const round = rounds[0];

            if (round) {
              if ((_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
                error: Error()
              }), sConfigMgr) : sConfigMgr).instance.IsMultiArrConfigHitList(round.hit_events_list)) {
                const events = (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
                  error: Error()
                }), sConfigMgr) : sConfigMgr).instance.GetAllEventArrConfigByHitList(round.hit_events_list);

                if (events) {
                  let isFound = false;

                  for (let i = 0; i < events.length; i++) {
                    const _event = events[i];

                    for (let j = 0; j < _event.length; j++) {
                      const element = _event[j];

                      if (element && element.event_type == 'changeRound') {
                        flowType = 'freeWinFlow';
                        isFound = true;
                        break;
                      }
                    }

                    if (isFound) {
                      break;
                    }
                  }
                }
              } else {
                const events = (_crd && sConfigMgr === void 0 ? (_reportPossibleCrUseOfsConfigMgr({
                  error: Error()
                }), sConfigMgr) : sConfigMgr).instance.GetAllEventConfigByHitList(round.hit_events_list);

                if (events) {
                  for (let i = 0; i < events.length; i++) {
                    const _event = events[i];

                    if (_event && _event.event_type == 'changeRound') {
                      flowType = 'freeWinFlow';
                      break;
                    }
                  }
                }
              }

              const flowBase = this.gameFlowBaseObj[flowType];

              if (flowBase) {
                this.currentGameFlow = flowBase;

                if (res && res.data) {
                  if (globalThis.slotJackpotDate) {
                    globalThis.slotJackpotDate.jackpot_amount = res.data.jackpot_amount;
                    globalThis.slotJackpotDate.reward_id = res.data.reward_id;
                    globalThis.slotJackpotDate.rate = res.data.rate; // globalThis.slotJackpotDate.jackpot_amount = 10;
                    // globalThis.slotJackpotDate.reward_id = 1;
                    // globalThis.slotJackpotDate.rate = 10;

                    if (globalThis.slotJackpotDate.rate == 0 && globalThis.slotJackpotDate.jackpot_amount > 0) {
                      globalThis.slotJackpotDate.bingo = true;
                    } else {
                      globalThis.slotJackpotDate.bingo = false;
                    }
                  }

                  this.jackpotDateInit();
                }

                flowBase.flowBegin(type, clickMode);
              }
            }
          }
        }

        jackpotDateInit() {
          if (globalThis.slotJackpotDate) {
            let jackpotNum = 0;

            if (globalThis.slotJackpotDate.bingo) {
              if (globalThis.slotJackpotDate.reward_id == 1) {
                jackpotNum = 3;
              } else if (globalThis.slotJackpotDate.reward_id == 2) {
                jackpotNum = 4;
              } else if (globalThis.slotJackpotDate.reward_id == 3) {
                jackpotNum = 5;
              }
            } else {
              if (globalThis.slotJackpotDate.enable && globalThis.slotJackpotDate.rate == 0) {
                if ((_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                  error: Error()
                }), sUtil) : sUtil).RandomInt(0, 100) == 1) {
                  jackpotNum = (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                    error: Error()
                  }), sUtil) : sUtil).RandomInt(1, 3);
                }
              }
            }

            globalThis.slotJackpotDate.num = jackpotNum;
            globalThis.slotJackpotDate.replace = this.jackpotReplaceSymbol; // globalThis.slotJackpotDate = {num : jackpotNum,replace : _replace};
          }
        } //-----message call function end------


      }, _defineProperty(_class3, "instance", void 0), _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "needJackpot", [property], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return false;
        }
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "jackpotReplaceSymbol", [property], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return 0;
        }
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=sGameMgrBase.js.map
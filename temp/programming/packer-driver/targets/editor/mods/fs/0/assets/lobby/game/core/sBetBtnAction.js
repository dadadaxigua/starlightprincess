System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2", "__unresolved_3"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, Node, director, v3, quat, SpriteFrame, Sprite, tween, UIOpacity, Button, Label, macro, Vec3, sAnimationFrame, sAudioMgr, sComponent, _dec, _dec2, _dec3, _dec4, _dec5, _dec6, _dec7, _dec8, _dec9, _dec10, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _descriptor5, _descriptor6, _descriptor7, _descriptor8, _descriptor9, _temp, _crd, ccclass, property, sBetBtnAction;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfsAnimationFrame(extras) {
    _reporterNs.report("sAnimationFrame", "./sAnimationFrame", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsAudioMgr(extras) {
    _reporterNs.report("sAudioMgr", "./sAudioMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsComponent(extras) {
    _reporterNs.report("sComponent", "./sComponent", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      Node = _cc.Node;
      director = _cc.director;
      v3 = _cc.v3;
      quat = _cc.quat;
      SpriteFrame = _cc.SpriteFrame;
      Sprite = _cc.Sprite;
      tween = _cc.tween;
      UIOpacity = _cc.UIOpacity;
      Button = _cc.Button;
      Label = _cc.Label;
      macro = _cc.macro;
      Vec3 = _cc.Vec3;
    }, function (_unresolved_2) {
      sAnimationFrame = _unresolved_2.sAnimationFrame;
    }, function (_unresolved_3) {
      sAudioMgr = _unresolved_3.default;
    }, function (_unresolved_4) {
      sComponent = _unresolved_4.sComponent;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "30e258Npv5A0LoXNVJxuPE5", "sBetBtnAction", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("sBetBtnAction", sBetBtnAction = (_dec = ccclass('sBetBtnAction'), _dec2 = property(Node), _dec3 = property(Button), _dec4 = property(Node), _dec5 = property(_crd && sAnimationFrame === void 0 ? (_reportPossibleCrUseOfsAnimationFrame({
        error: Error()
      }), sAnimationFrame) : sAnimationFrame), _dec6 = property(SpriteFrame), _dec7 = property(SpriteFrame), _dec8 = property(SpriteFrame), _dec9 = property(SpriteFrame), _dec10 = property(UIOpacity), _dec(_class = (_class2 = (_temp = class sBetBtnAction extends (_crd && sComponent === void 0 ? (_reportPossibleCrUseOfsComponent({
        error: Error()
      }), sComponent) : sComponent) {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "betNode", _descriptor, this);

          _initializerDefineProperty(this, "autoBetBtn", _descriptor2, this);

          _initializerDefineProperty(this, "renderBtnNode", _descriptor3, this);

          _initializerDefineProperty(this, "waterAnima", _descriptor4, this);

          _initializerDefineProperty(this, "normalBtnSprite", _descriptor5, this);

          _initializerDefineProperty(this, "rollingBtnSprite", _descriptor6, this);

          _initializerDefineProperty(this, "diableNormalBtnSprite", _descriptor7, this);

          _initializerDefineProperty(this, "diableRollingBtnSprite", _descriptor8, this);

          _initializerDefineProperty(this, "bloomNode", _descriptor9, this);

          _defineProperty(this, "rotationSpeed", quat(0, 0, -0.01));

          _defineProperty(this, "rollType", 0);

          _defineProperty(this, "rolling", false);

          _defineProperty(this, "selfWorldPos", void 0);
        }

        start() {
          this.selfWorldPos = v3(this.node.worldPosition);
          this.schedule(() => {
            if (this.rolling == false) {
              this.bloomNode.node.active = true;
              this.bloomNode.node.scale = v3(1.4, 1.4, 1);
              this.bloomNode.opacity = 140;
              tween(this.bloomNode.node).to(1, {
                scale: v3(2, 2, 1)
              }, {
                easing: 'cubicOut'
              }).start();
              tween(this.bloomNode).to(1, {
                opacity: 0
              }, {
                easing: 'cubicOut'
              }).call(() => {
                this.bloomNode.node.active = false;
              }).start();
            }
          }, 5, macro.REPEAT_FOREVER, 1);
          director.on('subGameFreeWinBuyBtnClick', () => {
            this.rotationSpeed.z = 0;

            if (this.renderBtnNode) {
              let sprite = this.renderBtnNode.children[0].getComponent(Sprite);

              if (sprite) {
                sprite.spriteFrame = this.diableNormalBtnSprite;
              }
            }
          }, this);
          director.on('subGameBetActionBtnCancel', () => {
            this.rotationSpeed.z = -0.01;

            if (this.renderBtnNode) {
              let sprite = this.renderBtnNode.children[0].getComponent(Sprite);

              if (sprite) {
                sprite.spriteFrame = this.normalBtnSprite;
              }
            }
          }, this);
          director.on('subGameMenuView', value => {
            if (value) {
              this.betNode.active = false;
            } else {
              this.betNode.active = true;
            }
          }, this);
          director.on('subGameBoxRecordView', value => {
            const btn = this.node.getChildByName('btn');

            if (btn) {
              if (value) {
                btn.active = false;
              } else {
                btn.active = true;
              }
            }
          }, this);
          director.on('betBtnEnter', () => {}, this);
          director.on('betBtnLeave', () => {}, this);
          director.on('betBtnClick', betSpeedMode => {
            this.rolling = true;
            (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
              error: Error()
            }), sAudioMgr) : sAudioMgr).PlayShotAudio('betBtnClick');

            if (this.rollType == 0) {
              this.rotationSpeed.z = -0.13;

              if (this.renderBtnNode) {
                tween(this.renderBtnNode).to(0.1, {
                  scale: v3(0.85, 0.85, 0.85)
                }, {
                  easing: 'cubicOut'
                }).to(0.1, {
                  scale: Vec3.ONE
                }, {
                  easing: 'cubicIn'
                }).start();
                let sprite = this.renderBtnNode.children[0].getComponent(Sprite);

                if (sprite) {
                  if (betSpeedMode == 'normal') {
                    sprite.spriteFrame = this.rollingBtnSprite;
                  } else {
                    sprite.spriteFrame = this.diableRollingBtnSprite;
                  }
                }

                let light = this.renderBtnNode.children[0].getChildByName('light');

                if (light) {
                  light.active = true;
                  let opa = light.getComponent(UIOpacity);

                  if (opa) {
                    tween(opa).to(0.2, {
                      opacity: 160
                    }, {
                      'onComplete': () => {
                        tween(opa).to(0.6, {
                          opacity: 0
                        }, {
                          'onComplete': () => {
                            light.active = false;
                          }
                        }).start();
                      }
                    }).start();
                  }
                }

                if (this.waterAnima) {
                  this.waterAnima.node.active = true;
                  this.waterAnima.playOnce(() => {
                    this.waterAnima.node.active = false;
                  });
                }
              }
            } else if (this.rollType == 1) {}
          }, this);
          director.on('readyToBet', () => {
            if (this.rollType == 0) {
              this.rolling = false;
              this.rotationSpeed.z = -0.01;

              if (this.renderBtnNode) {
                let sprite = this.renderBtnNode.children[0].getComponent(Sprite);

                if (sprite) {
                  sprite.spriteFrame = this.normalBtnSprite;
                }
              }
            }
          }, this);
          director.on('betBtnState', state => {
            if (state == 'disable') {
              let sprite = this.renderBtnNode.children[0].getComponent(Sprite);

              if (sprite) {
                sprite.spriteFrame = this.diableNormalBtnSprite;
              }
            } else if (state == 'enable') {
              let sprite = this.renderBtnNode.children[0].getComponent(Sprite);

              if (sprite) {
                sprite.spriteFrame = this.normalBtnSprite;
              }
            }
          }, this);
          director.on('autoBetInfoUpdate', value => {
            if (value == -1) {
              this.rollType = 1;
              const bet = this.betNode;
              const autoBet = this.autoBetBtn.node;

              if (bet && bet.active == true) {
                bet.active = false;
              }

              if (autoBet) {
                if (autoBet.active == false) {
                  autoBet.active = true;
                }

                const label = autoBet.getChildByName('label');

                if (label) {
                  label.active = false;
                }

                const endless = autoBet.getChildByName('endless');

                if (endless) {
                  endless.active = true;
                }
              }
            } else if (value > 0) {
              this.rollType = 1;
              const bet = this.betNode;
              const autoBet = this.autoBetBtn.node;

              if (bet && bet.active == true) {
                bet.active = false;
              }

              if (autoBet) {
                if (autoBet.active == false) {
                  autoBet.active = true;
                }

                const label = autoBet.getChildByName('label');

                if (label) {
                  label.active = true;
                  label.getComponent(Label).string = value;
                }

                const endless = autoBet.getChildByName('endless');

                if (endless) {
                  endless.active = false;
                }
              }
            } else {
              this.rollType = 0;
              const bet = this.betNode;
              const autoBet = this.autoBetBtn.node;

              if (bet && bet.active == false) {
                bet.active = true;
              }

              if (autoBet && autoBet.active == true) {
                autoBet.active = false;
              }
            }
          }, this);
          director.on('boxWinInform', () => {
            if (this.rollType == 0) {
              this.rotationSpeed.z = 0;

              if (this.renderBtnNode) {
                let sprite = this.renderBtnNode.children[0].getComponent(Sprite);

                if (sprite) {
                  sprite.spriteFrame = this.diableNormalBtnSprite;
                }
              }
            }
          }, this);
          this.autoBetBtn.node.on('click', () => {
            console.log('autoBetBtn');
            director.emit('viewChange', 'autoSpinCancel');
          }, this);
        }

        update(deltaTime) {
          if (this.renderBtnNode) {
            this.renderBtnNode.rotate(this.rotationSpeed);
          }
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "betNode", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "autoBetBtn", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "renderBtnNode", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "waterAnima", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor5 = _applyDecoratedDescriptor(_class2.prototype, "normalBtnSprite", [_dec6], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor6 = _applyDecoratedDescriptor(_class2.prototype, "rollingBtnSprite", [_dec7], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor7 = _applyDecoratedDescriptor(_class2.prototype, "diableNormalBtnSprite", [_dec8], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor8 = _applyDecoratedDescriptor(_class2.prototype, "diableRollingBtnSprite", [_dec9], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor9 = _applyDecoratedDescriptor(_class2.prototype, "bloomNode", [_dec10], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=sBetBtnAction.js.map
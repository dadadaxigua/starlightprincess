System.register(["__unresolved_0", "cc", "__unresolved_1"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, Node, director, sp, UIFreeWinBuy, _dec, _dec2, _class, _class2, _descriptor, _temp, _crd, ccclass, property, StarlightPrincess_UIFreeWinBuy;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfUIFreeWinBuy(extras) {
    _reporterNs.report("UIFreeWinBuy", "../lobby/hyPrefabs/gameRes/external/UIFreeWinBuy", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      Node = _cc.Node;
      director = _cc.director;
      sp = _cc.sp;
    }, function (_unresolved_2) {
      UIFreeWinBuy = _unresolved_2.UIFreeWinBuy;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "9259fh+Dw9Pjpq+HuTNd6JZ", "StarlightPrincess_UIFreeWinBuy", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("StarlightPrincess_UIFreeWinBuy", StarlightPrincess_UIFreeWinBuy = (_dec = ccclass('StarlightPrincess_UIFreeWinBuy'), _dec2 = property(Node), _dec(_class = (_class2 = (_temp = class StarlightPrincess_UIFreeWinBuy extends (_crd && UIFreeWinBuy === void 0 ? (_reportPossibleCrUseOfUIFreeWinBuy({
        error: Error()
      }), UIFreeWinBuy) : UIFreeWinBuy) {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "SpinNode", _descriptor, this);

          _defineProperty(this, "mBetType1", -1);

          _defineProperty(this, "mCoin1", 0);

          _defineProperty(this, "canBuy1", false);
        }

        onLoad() {
          director.on('sSlotBetInfoUpdate', info => {
            console.log('sSlotBetInfoUpdate2', info);

            if (info) {
              if (info.betAmount) {
                this.coinLabel.string = this.AddCommas(info.betAmount * 50 * 20);
              }

              ;
            }

            ;
          }, this);
        }

        start() {
          this.startBtn.on('click', () => {
            if (this.canBuy1) {
              this.SpinNode.getComponent(sp.Skeleton).setAnimation(0, "stpr_buy_feature_center_yes", false);
              this.SpinNode.getComponent(sp.Skeleton).setCompleteListener(() => {
                this.scheduleOnce(() => {
                  if (this.mBetType1 != -1) {
                    director.emit('subGameBetActionBtnClick', this.mBetType1, this.mCoin1);
                  }

                  ;
                  this.mainNode.active = false;
                }, 0.5);
                this.SpinNode.getComponent(sp.Skeleton).setAnimation(0, "stpr_buy_feature_center_out", false);
                this.SpinNode.getComponent(sp.Skeleton).setCompleteListener(() => {});
              });
            } else {
              globalThis.goldNotenough && globalThis.goldNotenough();
            }
          }, this);
          this.cancelNode.on('click', () => {
            this.mBetType1 = -1;
            this.SpinNode.getComponent(sp.Skeleton).setAnimation(0, "stpr_buy_feature_center_no", false);
            this.scheduleOnce(() => {
              this.mainNode.active = false;
              director.emit('subGameBetActionBtnCancel');
            }, 0.5);
            this.SpinNode.getComponent(sp.Skeleton).setCompleteListener(() => {
              this.SpinNode.getComponent(sp.Skeleton).setAnimation(0, "stpr_buy_feature_center_out", false);
              this.SpinNode.getComponent(sp.Skeleton).setCompleteListener(() => {});
            });
          }, this); // director.on('sSlotBetInfoUpdate',(info)=>{
          //     console.log('sSlotBetInfoUpdate2',info);
          //     if(info){
          //         if(info.betAmount){
          //             this.coinLabel.string = this.AddCommas(info.betAmount * 50*20);
          //         };
          //     };
          // },this); 
        }

        viewInit(coin, betType, _canBuy, _tipStr) {
          //this.coinLabel.string = this.AddCommas(coin);
          this.mCoin1 = coin;
          this.mBetType1 = betType;
          this.canBuy1 = _canBuy;
          this.mainNode.active = true;
          this.SpinNode.getComponent(sp.Skeleton).setAnimation(0, "stpr_buy_feature_center_in", false);
          this.SpinNode.getComponent(sp.Skeleton).setCompleteListener(() => {//this.SpinNode.getComponent(sp.Skeleton).setAnimation(0,"stpr_buy_feature_center_loop",false);
          });
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "SpinNode", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=StarlightPrincess_UIFreeWinBuy.js.map
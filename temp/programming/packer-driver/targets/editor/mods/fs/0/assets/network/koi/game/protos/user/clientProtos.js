System.register(["cc"], function (_export, _context) {
  "use strict";

  var _cclegacy, _crd;

  return {
    setters: [function (_cc) {
      _cclegacy = _cc.cclegacy;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "df6d2tFQAZMQaBMcVktWSIw", "clientProtos", undefined);

      _export("default", {
        "nested": {
          "connector": {
            "nested": {
              "authHandler": {
                "nested": {
                  "auth": {
                    "fields": {}
                  },
                  "pay": {
                    "fields": {
                      "amount": {
                        "type": "double",
                        "id": 1
                      }
                    }
                  }
                }
              }
            }
          },
          "user": {
            "nested": {
              "awardHandler": {
                "nested": {
                  "exchange": {
                    "fields": {
                      "type": {
                        "type": "int32",
                        "id": 1
                      },
                      "cost": {
                        "rule": "repeated",
                        "type": "ItemData",
                        "id": 2
                      }
                    },
                    "nested": {
                      "ItemData": {
                        "fields": {
                          "item_id": {
                            "type": "int32",
                            "id": 1
                          },
                          "num": {
                            "type": "int32",
                            "id": 2
                          }
                        }
                      }
                    }
                  },
                  "gainAward": {
                    "fields": {}
                  }
                }
              },
              "itemHandler": {
                "nested": {
                  "getItemList": {
                    "fields": {}
                  },
                  "transferItem": {
                    "fields": {
                      "uuid": {
                        "type": "string",
                        "id": 1
                      },
                      "item_id": {
                        "type": "int32",
                        "id": 2
                      },
                      "num": {
                        "type": "int32",
                        "id": 3
                      },
                      "remark": {
                        "type": "string",
                        "id": 4
                      }
                    }
                  }
                }
              },
              "relationHandler": {
                "nested": {
                  "addFriend": {
                    "fields": {
                      "uid": {
                        "type": "string",
                        "id": 1
                      }
                    }
                  },
                  "applyDispose": {
                    "fields": {
                      "uid": {
                        "type": "string",
                        "id": 1
                      },
                      "type": {
                        "type": "int32",
                        "id": 2
                      }
                    }
                  },
                  "deleteFriend": {
                    "fields": {
                      "uid": {
                        "type": "string",
                        "id": 1
                      }
                    }
                  },
                  "changeRemark": {
                    "fields": {
                      "uid": {
                        "type": "string",
                        "id": 1
                      },
                      "remark": {
                        "type": "string",
                        "id": 2
                      }
                    }
                  }
                }
              },
              "taskHandler": {
                "nested": {
                  "gainTaskReward": {
                    "fields": {
                      "task_id": {
                        "type": "int32",
                        "id": 1
                      },
                      "draw_time": {
                        "type": "int64",
                        "id": 2
                      }
                    }
                  },
                  "groupList": {
                    "fields": {}
                  },
                  "groupInfo": {
                    "fields": {
                      "task_group_id": {
                        "type": "int32",
                        "id": 1
                      }
                    }
                  }
                }
              },
              "userHandler": {
                "nested": {
                  "getUserInfo": {
                    "fields": {
                      "query_uid": {
                        "type": "string",
                        "id": 1
                      }
                    }
                  },
                  "getPlayerInfo": {
                    "fields": {
                      "query_uid": {
                        "type": "string",
                        "id": 1
                      }
                    }
                  },
                  "getUserInfoLight": {
                    "fields": {
                      "query_uid": {
                        "type": "string",
                        "id": 1
                      }
                    }
                  },
                  "getOrderRecord": {
                    "fields": {
                      "time_stamp": {
                        "type": "string",
                        "id": 1
                      }
                    }
                  },
                  "userApply": {
                    "fields": {
                      "category": {
                        "type": "string",
                        "id": 1
                      },
                      "type": {
                        "type": "string",
                        "id": 2
                      },
                      "submit_info": {
                        "type": "string",
                        "id": 3
                      }
                    }
                  },
                  "modifyInfo": {
                    "fields": {
                      "nick_name": {
                        "type": "string",
                        "id": 1
                      },
                      "region_mark": {
                        "type": "string",
                        "id": 2
                      },
                      "avatar": {
                        "type": "string",
                        "id": 3
                      }
                    }
                  },
                  "paySubmit": {
                    "fields": {
                      "pay_mode": {
                        "type": "string",
                        "id": 1
                      },
                      "time_stamp": {
                        "type": "int64",
                        "id": 2
                      },
                      "submit_items": {
                        "type": "string",
                        "id": 3
                      },
                      "pay_tag": {
                        "type": "string",
                        "id": 4
                      }
                    }
                  },
                  "gainAward": {
                    "fields": {
                      "award_id": {
                        "type": "string",
                        "id": 1
                      },
                      "type": {
                        "type": "string",
                        "id": 2
                      }
                    }
                  },
                  "transferItem": {
                    "fields": {
                      "target_uuid": {
                        "type": "string",
                        "id": 1
                      },
                      "item_id": {
                        "type": "int32",
                        "id": 2
                      },
                      "num": {
                        "type": "int32",
                        "id": 3
                      },
                      "comment": {
                        "type": "string",
                        "id": 4
                      }
                    }
                  },
                  "deposit": {
                    "fields": {
                      "request": {
                        "type": "string",
                        "id": 1
                      },
                      "time_stamp": {
                        "type": "int64",
                        "id": 2
                      }
                    }
                  }
                }
              },
              "payHandler": {
                "nested": {
                  "seniorPaySubmit": {
                    "fields": {
                      "form_id": {
                        "type": "string",
                        "id": 1
                      },
                      "submit_items": {
                        "type": "string",
                        "id": 2
                      }
                    }
                  },
                  "normalPaySubmitV2": {
                    "fields": {
                      "receipt": {
                        "type": "string",
                        "id": 1
                      },
                      "extra": {
                        "type": "string",
                        "id": 2
                      },
                      "channel_id": {
                        "type": "int32",
                        "id": 3
                      },
                      "currency_amount": {
                        "type": "double",
                        "id": 4
                      },
                      "time_stamp": {
                        "type": "int64",
                        "id": 5
                      },
                      "bank_id": {
                        "type": "string",
                        "id": 6
                      },
                      "app_id": {
                        "type": "int32",
                        "id": 7
                      },
                      "item_id": {
                        "type": "string",
                        "id": 8
                      },
                      "pay_way": {
                        "type": "string",
                        "id": 9
                      },
                      "channel_code": {
                        "type": "string",
                        "id": 10
                      },
                      "ad_id": {
                        "type": "string",
                        "id": 11
                      },
                      "category_id": {
                        "type": "string",
                        "id": 12
                      }
                    }
                  }
                }
              },
              "signInHandler": {
                "nested": {
                  "signIn": {
                    "fields": {}
                  },
                  "getSignInInfo": {
                    "fields": {}
                  }
                }
              },
              "redpackHandler": {
                "nested": {
                  "hit": {
                    "fields": {
                      "id": {
                        "type": "int32",
                        "id": 1
                      }
                    }
                  }
                }
              }
            }
          }
        }
      });

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=clientProtos.js.map
System.register(["cc"], function (_export, _context) {
  "use strict";

  var _cclegacy, _decorator, Component, director, macro, _dec, _class, _temp, _crd, ccclass, property, sComponent;

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  return {
    setters: [function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      Component = _cc.Component;
      director = _cc.director;
      macro = _cc.macro;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "33ba6sp+ltB14pqkpe537kv", "sComponent", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("sComponent", sComponent = (_dec = ccclass('sComponent'), _dec(_class = (_temp = class sComponent extends Component {
        constructor(...args) {
          super(...args);

          _defineProperty(this, "tweenDic", {});

          _defineProperty(this, "tweenList", []);

          _defineProperty(this, "scheduleDic", {});

          _defineProperty(this, "scheduleList", []);

          _defineProperty(this, "msgList", {});
        }

        pushOneTween(t) {
          this.tweenList.push(t);
        }

        pushIDTween(t, id) {
          this.tweenDic[id] = t;
        }

        pushIDSchedule(call, time, id, loop = 1, interval = 0, delay = 0, callByStart = false) {
          if (call) {
            if (loop == 1) {
              this.scheduleOnce(call, time);
            } else if (loop > 1) {
              let repeat = loop;

              if (callByStart) {
                call();
                repeat -= 2;
              } else {
                repeat--;
              }

              this.schedule(call, interval, repeat, delay);
            } else if (loop == -1) {
              let repeat = macro.REPEAT_FOREVER;

              if (callByStart) {
                call();
              }

              this.schedule(call, interval, repeat, delay);
            }

            this.scheduleDic[id] = call;
          }
        }

        pushOneSchedule(call, time, loop = 1, interval = 0, delay = 0, callByStart = false) {
          if (call) {
            if (loop == 1) {
              this.scheduleOnce(call, time);
            } else if (loop > 1) {
              let repeat = loop;

              if (callByStart) {
                call();
                repeat--;
              }

              this.schedule(call, interval, repeat, delay);
            }

            this.scheduleList.push(call);
          }
        }

        removeIDTween(id) {
          const t = this.tweenDic[id];

          if (t) {
            t.stop();
            delete this.tweenDic[id];
          }
        }

        removeIDSchedule(id) {
          const t = this.scheduleDic[id];

          if (t) {
            this.unschedule(t);
            delete this.scheduleDic[id];
          }
        }

        removeAllSchedule() {
          if (this.scheduleList && this.scheduleList.length > 0) {
            for (let i = 0; i < this.scheduleList.length; i++) {
              const _schedule = this.scheduleList[i];

              if (_schedule) {
                this.unschedule(_schedule);
              }
            }

            this.scheduleList = [];
          }
        }

        getIDSchedule(id) {
          const t = this.scheduleDic[id];

          if (t) {
            return t;
          }
        }

        removeScheduleDic() {
          this.unscheduleAllCallbacks();
          this.scheduleDic = {};
        }

        cleanTweenDic() {
          const keys = Object.keys(this.tweenDic);

          if (keys && keys.length > 0) {
            for (let i = 0; i < keys.length; i++) {
              const t = this.tweenDic[keys[i]];

              if (t) {
                t.stop();
              }
            }

            this.tweenDic = {};
          }
        }

        cleanTweenList() {
          if (this.tweenList && this.tweenList.length > 0) {
            for (let i = 0; i < this.tweenList.length; i++) {
              const t = this.tweenList[i];

              if (t) {
                t.stop();
              }
            }

            this.tweenList = [];
          }
        }

        clean() {
          this.unscheduleAllCallbacks();
          this.cleanTweenDic();
          this.cleanTweenList();
        }

        MsgOn(msgName, call) {
          if (msgName && call) {
            if (!this.msgList) {
              this.msgList = {};
            }

            this.msgList[msgName] = call;
            director.on(msgName, call, this);
          }
        }

        MsgOff(msgName) {
          if (msgName && this.msgList) {
            const msg = this.msgList[msgName];

            if (msg) {
              director.off(msgName, msg, this);
              delete this.msgList[msgName];
            }
          }
        }

        MsgOffAll() {
          const keys = Object.keys(this.msgList);

          if (keys && keys.length > 0) {
            for (let i = 0; i < keys.length; i++) {
              const _key = keys[i];
              const call = this.msgList[_key];
              director.off(_key, call, this);
            }

            this.msgList = {};
          }
        }

        AddCommas(money, demc = 0) {
          if (!money) {
            return "0";
          } else {
            let mon;

            if (typeof money == "string") {
              mon = Number(money);
            } else {
              mon = money;
            }

            let coinRate = 1,
                byte = 1,
                rate = 1;

            if (globalThis.getCoinRate && globalThis.getCoinRate() > 0) {
              coinRate = globalThis.getCoinRate();
            }

            if (coinRate >= 10) {
              byte = coinRate.toString().length - 1;
              rate = 100;
            }

            let showMoney = mon; // demc = 0;

            if (byte == 1) {
              if (demc == 0) {
                showMoney = mon;
              } else if (demc == -1) {
                showMoney = Math.trunc(mon);
              }
            } else {
              if (demc == 0) {
                showMoney = mon / coinRate;
              } else if (demc > 0) {
                showMoney = mon / coinRate; // showMoney = (Math.floor(showMoney * Math.pow(10,demc)) / Math.pow(10,demc));

                showMoney = showMoney.toFixed(demc);
              } else if (demc == -1) {
                showMoney /= coinRate;
                showMoney = Math.trunc(showMoney);
              }
            } // console.log('showMoney:'+showMoney);


            let tempmoney = String(showMoney);
            var left = tempmoney.split('.')[0],
                right = tempmoney.split('.')[1];
            right = right ? right.length >= byte ? '.' + right.substring(0, byte) : '.' + right : '';
            var temp = left.split('').reverse().join('').match(/(\d{1,3})/g);

            if (temp) {
              return (Number(tempmoney) < 0 ? "-" : "") + temp.join(',').split('').reverse().join('') + right;
            } else {
              return '';
            }
          }
        }

        SetUIFront() {
          this.node.setSiblingIndex(this.node.parent.children.length - 1);
        }

        SetNodeFront(_node) {
          if (_node.parent) {
            _node.setSiblingIndex(_node.parent.children.length - 1);
          }
        }

        CtrAllChildActive(targetNode, value) {
          if (targetNode) {
            for (let i = 0; i < targetNode.children.length; i++) {
              targetNode.children[i].active = value;
            }
          }
        }

      }, _temp)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=sComponent.js.map
System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2", "__unresolved_3", "__unresolved_4", "__unresolved_5", "__unresolved_6"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, Node, sp, SpriteFrame, SpriteAtlas, AudioClip, instantiate, JsonAsset, assetManager, director, assetMgr, sAudioMgr, sBoxItemBase, sComponent, sInternationalManager, sObjPool, _dec, _dec2, _dec3, _dec4, _dec5, _dec6, _dec7, _dec8, _dec9, _dec10, _dec11, _dec12, _dec13, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _descriptor5, _descriptor6, _descriptor7, _descriptor8, _descriptor9, _descriptor10, _descriptor11, _descriptor12, _class3, _temp, _crd, ccclass, property, sBoxAssetInit, assetObj;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function initAssetObjByName(name, path, parent) {
    if (name && path && parent) {
      getAssetObjByName(name, path, obj => {
        if (obj) {
          obj.parent = parent;
          obj.active = true;
          return obj;
        }
      });
    }

    return null;
  }

  function getAssetObjByName(name, path, cb = null) {
    if (name) {
      let obj = assetObj[name];

      if (!obj) {
        globalThis.getSubGamePrefabByPath(path, newObj => {
          if (newObj) {
            assetObj[name] = newObj;

            if (cb) {
              cb(newObj);
            }
          }
        });
      } else {
        if (cb) {
          cb(obj);
        }
      }
    }
  }

  function _reportPossibleCrUseOfassetMgr(extras) {
    _reporterNs.report("assetMgr", "./sAssetMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsAudioMgr(extras) {
    _reporterNs.report("sAudioMgr", "./sAudioMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsBoxItemBase(extras) {
    _reporterNs.report("sBoxItemBase", "./sBoxItemBase", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsComponent(extras) {
    _reporterNs.report("sComponent", "./sComponent", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsInternationalManager(extras) {
    _reporterNs.report("sInternationalManager", "./sInternationalManager", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsObjPool(extras) {
    _reporterNs.report("sObjPool", "./sObjPool", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      Node = _cc.Node;
      sp = _cc.sp;
      SpriteFrame = _cc.SpriteFrame;
      SpriteAtlas = _cc.SpriteAtlas;
      AudioClip = _cc.AudioClip;
      instantiate = _cc.instantiate;
      JsonAsset = _cc.JsonAsset;
      assetManager = _cc.assetManager;
      director = _cc.director;
    }, function (_unresolved_2) {
      assetMgr = _unresolved_2.assetMgr;
    }, function (_unresolved_3) {
      sAudioMgr = _unresolved_3.default;
    }, function (_unresolved_4) {
      sBoxItemBase = _unresolved_4.sBoxItemBase;
    }, function (_unresolved_5) {
      sComponent = _unresolved_5.sComponent;
    }, function (_unresolved_6) {
      sInternationalManager = _unresolved_6.sInternationalManager;
    }, function (_unresolved_7) {
      sObjPool = _unresolved_7.sObjPool;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "794b8WUCv5KbKMiAP++AmiL", "sBoxAssetInit", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("sBoxAssetInit", sBoxAssetInit = (_dec = ccclass('sBoxAssetInit'), _dec2 = property({
        tooltip: '以单张图为key的资源引用',
        type: [SpriteAtlas]
      }), _dec3 = property({
        tooltip: '以帧动画的大图key的资源引用',
        type: [SpriteAtlas]
      }), _dec4 = property({
        tooltip: '以帧动画的大图key的资源合集引用(将会以图片名字最后一个下划线作为区分)',
        type: [SpriteAtlas]
      }), _dec5 = property({
        tooltip: '音频的资源引用',
        type: [AudioClip]
      }), _dec6 = property({
        tooltip: '以散图的名字为key的资源引用',
        type: [SpriteFrame]
      }), _dec7 = property({
        tooltip: 'spine动画的对象池拷贝节点',
        type: Node
      }), _dec8 = property({
        tooltip: 'spine动画的初始化节点',
        type: Node
      }), _dec9 = property({
        tooltip: 'Node对象池拷贝节点',
        type: Node
      }), _dec10 = property({
        tooltip: '继承自sBoxItemBase的对象池拷贝节点',
        type: [Node]
      }), _dec11 = property({
        tooltip: '需要根据名字访问的全局节点',
        type: [Node]
      }), _dec12 = property({
        tooltip: '多语言配置文件',
        type: [JsonAsset]
      }), _dec13 = property({
        tooltip: '节点所有子节点延迟关闭',
        type: Node
      }), _dec(_class = (_class2 = (_temp = _class3 = class sBoxAssetInit extends (_crd && sComponent === void 0 ? (_reportPossibleCrUseOfsComponent({
        error: Error()
      }), sComponent) : sComponent) {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "plistAtlas", _descriptor, this);

          _initializerDefineProperty(this, "sortPlistAtlas", _descriptor2, this);

          _initializerDefineProperty(this, "assembleSortPlistAtlas", _descriptor3, this);

          _initializerDefineProperty(this, "gameAudioClip", _descriptor4, this);

          _initializerDefineProperty(this, "singleSpriteFrames", _descriptor5, this);

          _initializerDefineProperty(this, "objPoolSpineCopyNodes", _descriptor6, this);

          _initializerDefineProperty(this, "objPoolSpineNodes", _descriptor7, this);

          _initializerDefineProperty(this, "objPoolNodes", _descriptor8, this);

          _initializerDefineProperty(this, "objPoolBoxItemNodes", _descriptor9, this);

          _initializerDefineProperty(this, "targetNodes", _descriptor10, this);

          _initializerDefineProperty(this, "languageConfigs", _descriptor11, this);

          _initializerDefineProperty(this, "nodeDelayActiveClose", _descriptor12, this);

          _defineProperty(this, "targetNodeDic", {});
        }

        onLoad() {
          sBoxAssetInit.instance = this;
          this.assetMsgRegister();
          (_crd && assetMgr === void 0 ? (_reportPossibleCrUseOfassetMgr({
            error: Error()
          }), assetMgr) : assetMgr).SetSpriteFramesArrayToDic(this.plistAtlas);
          (_crd && assetMgr === void 0 ? (_reportPossibleCrUseOfassetMgr({
            error: Error()
          }), assetMgr) : assetMgr).SetSortSpriteFramesArrayToDic(this.sortPlistAtlas);
          (_crd && assetMgr === void 0 ? (_reportPossibleCrUseOfassetMgr({
            error: Error()
          }), assetMgr) : assetMgr).SetAssembleSortSpriteFramesArrayToDic(this.assembleSortPlistAtlas);
          (_crd && assetMgr === void 0 ? (_reportPossibleCrUseOfassetMgr({
            error: Error()
          }), assetMgr) : assetMgr).SetSingleSpriteFramesArrayToDic(this.singleSpriteFrames);

          if (this.gameAudioClip && this.gameAudioClip.length) {
            for (let i = 0; i < this.gameAudioClip.length; i++) {
              const clip = this.gameAudioClip[i];
              (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
                error: Error()
              }), sAudioMgr) : sAudioMgr).SetAudioClip(clip);
            }
          }

          if (this.objPoolSpineCopyNodes && this.objPoolSpineCopyNodes.children.length > 0) {
            for (let i = 0; i < this.objPoolSpineCopyNodes.children.length; i++) {
              const element = this.objPoolSpineCopyNodes.children[i];
              (_crd && sObjPool === void 0 ? (_reportPossibleCrUseOfsObjPool({
                error: Error()
              }), sObjPool) : sObjPool).SetObj(element.name, () => {
                if (element) {
                  const obj = instantiate(element);
                  return obj.getComponent(sp.Skeleton);
                }

                return null;
              }); // element.active = false;
            }
          }

          if (this.objPoolSpineNodes && this.objPoolSpineNodes.children.length > 0) {
            const copySpines = this.objPoolSpineNodes.getComponentsInChildren(sp.Skeleton);

            for (let i = 0; i < copySpines.length; i++) {
              const ske = copySpines[i];

              if (ske) {
                (_crd && sObjPool === void 0 ? (_reportPossibleCrUseOfsObjPool({
                  error: Error()
                }), sObjPool) : sObjPool).Enqueue(ske.node.name, ske);
              }
            }
          }

          if (this.objPoolNodes && this.objPoolNodes.children.length > 0) {
            for (let i = 0; i < this.objPoolNodes.children.length; i++) {
              const element = this.objPoolNodes.children[i];
              (_crd && sObjPool === void 0 ? (_reportPossibleCrUseOfsObjPool({
                error: Error()
              }), sObjPool) : sObjPool).SetObj(element.name, () => {
                if (element) {
                  const obj = instantiate(element);
                  return obj;
                }

                return null;
              });
            }
          }

          if (this.objPoolBoxItemNodes && this.objPoolBoxItemNodes.length > 0) {
            for (let i = 0; i < this.objPoolBoxItemNodes.length; i++) {
              const element = this.objPoolBoxItemNodes[i];
              (_crd && sObjPool === void 0 ? (_reportPossibleCrUseOfsObjPool({
                error: Error()
              }), sObjPool) : sObjPool).SetObj(element.name, () => {
                if (element) {
                  const obj = instantiate(element);
                  obj.parent = element.parent;
                  return obj.getComponent(_crd && sBoxItemBase === void 0 ? (_reportPossibleCrUseOfsBoxItemBase({
                    error: Error()
                  }), sBoxItemBase) : sBoxItemBase);
                }

                return null;
              });
            }
          }

          if (this.languageConfigs && this.languageConfigs.length > 0) {
            for (let i = 0; i < this.languageConfigs.length; i++) {
              const lan = this.languageConfigs[i];
              (_crd && sInternationalManager === void 0 ? (_reportPossibleCrUseOfsInternationalManager({
                error: Error()
              }), sInternationalManager) : sInternationalManager).SetLanguegeData(lan.name, lan.json);
            }
          }

          if (this.nodeDelayActiveClose && this.nodeDelayActiveClose.children.length > 0) {
            this.scheduleOnce(() => {
              for (let i = 0; i < this.nodeDelayActiveClose.children.length; i++) {
                const element = this.nodeDelayActiveClose.children[i];
                element.active = false;
              }
            }, 0.1);
          }

          if (this.targetNodes && this.targetNodes.length > 0) {
            for (let i = 0; i < this.targetNodes.length; i++) {
              const tNode = this.targetNodes[i];

              if (tNode && tNode.isValid) {
                this.targetNodeDic[tNode.name] = tNode;
              }
            }
          }
        }

        start() {
          let language = 'EN';

          if (globalThis.GetLanguageType) {
            language = globalThis.GetLanguageType();
          }

          const bundle = assetManager.getBundle(globalThis.currentPlayingGameID);

          if (bundle) {
            bundle.load('intro/' + language, SpriteAtlas, (err, spriteAtlas) => {
              if (spriteAtlas) {
                (_crd && assetMgr === void 0 ? (_reportPossibleCrUseOfassetMgr({
                  error: Error()
                }), assetMgr) : assetMgr).SetSpriteFramesToDic(spriteAtlas);
                director.emit('subGameLanguageImgLoaded');
              } else {
                console.warn('subGameLanguageImg:' + err);
              }
            });
          } // this.scheduleOnce(()=>{
          //     this.sdasd.animation = null;
          //     this.sdasd.skeletonData = null;
          //     this.sdasd.clearTracks();
          //     this.sdasd.node.active = false;
          //     // this.sdasd.skeletonData = this.skes[10];
          //     // this.sdasd.animation = 'win';
          // },3);
          // this.scheduleOnce(()=>{
          //     this.sdasd.node.active = true;
          //     this.sdasd.skeletonData = this.skes[10];
          //     this.sdasd.animation = 'win';
          // },5);

        }

        getTargetNodeByName(name) {
          return this.targetNodeDic[name];
        }

        assetMsgRegister() {}

      }, _defineProperty(_class3, "instance", void 0), _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "plistAtlas", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return [];
        }
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "sortPlistAtlas", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return [];
        }
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "assembleSortPlistAtlas", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return [];
        }
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "gameAudioClip", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return [];
        }
      }), _descriptor5 = _applyDecoratedDescriptor(_class2.prototype, "singleSpriteFrames", [_dec6], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return [];
        }
      }), _descriptor6 = _applyDecoratedDescriptor(_class2.prototype, "objPoolSpineCopyNodes", [_dec7], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor7 = _applyDecoratedDescriptor(_class2.prototype, "objPoolSpineNodes", [_dec8], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor8 = _applyDecoratedDescriptor(_class2.prototype, "objPoolNodes", [_dec9], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor9 = _applyDecoratedDescriptor(_class2.prototype, "objPoolBoxItemNodes", [_dec10], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return [];
        }
      }), _descriptor10 = _applyDecoratedDescriptor(_class2.prototype, "targetNodes", [_dec11], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return [];
        }
      }), _descriptor11 = _applyDecoratedDescriptor(_class2.prototype, "languageConfigs", [_dec12], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return [];
        }
      }), _descriptor12 = _applyDecoratedDescriptor(_class2.prototype, "nodeDelayActiveClose", [_dec13], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class));

      assetObj = {};
      director.on('backToHall', () => {
        assetObj = {};
      });
      director.on('ristAssetInit', container => {
        if (container && container.isValid) {
          initAssetObjByName('bigwin', 'rist_BigWin/Slots_win/ristBigWin', container);
          initAssetObjByName('totalwin', 'rist_BigWin/Slots_win/ristTotalWin', container);
          initAssetObjByName('freeSpin', 'rist_BigWin/Slots_win/ristFreeSpin', container);
        }
      });
      director.on('slotLineInit', (boxSum, pixelSize, pos, parent) => {
        const scene = director.getScene();

        if (scene && scene.children && scene.children.length > 0) {
          globalThis.getSubGamePrefabByPath('slotLine/slotLinesMgr', obj => {
            if (obj) {
              if (parent && parent.isValid) {
                parent.addChild(obj);
              } else {
                scene.children[0].addChild(obj);
              }

              const com = obj.getComponent('slotLinesMgr');

              if (com && com['Init']) {
                com['Init'](boxSum, pixelSize, pos);
              }
            }
          });
        }
      }, void 0);
      director.on('bonusLabelInit', (gameid, label) => {
        globalThis.jackPotBonusLabelInit && globalThis.jackPotBonusLabelInit(gameid, label);
      });
      director.on('slotBigWinPlay', (coin, rate) => {
        getAssetObjByName('bigwin', 'rist_BigWin/Slots_win/ristBigWin', obj => {
          if (obj) {
            obj.emit('bigWinAnima', coin, rate);
          }
        });
      });
      director.on('slotTotalWinPlay', (coin, waitTime = 5) => {
        if (waitTime > 0) {
          getAssetObjByName('totalwin', 'rist_BigWin/Slots_win/ristTotalWin', obj => {
            if (obj) {
              obj.emit('totalWinAnima', coin, waitTime);
            }
          });
        }
      });
      director.on('slotFreeSpinPlay', num => {
        getAssetObjByName('freeSpin', 'rist_BigWin/Slots_win/ristFreeSpin', obj => {
          if (obj) {
            obj.emit('freeSpinAnima', num);
          }
        });
      });
      director.on('slotRistIntro', parent => {
        if (parent) {
          globalThis.getSubGamePrefabByPath('intro/ristIntro', newObj => {
            if (newObj) {
              newObj.parent = parent;
              newObj.active = true;
            }
          });
        }
      });

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=sBoxAssetInit.js.map
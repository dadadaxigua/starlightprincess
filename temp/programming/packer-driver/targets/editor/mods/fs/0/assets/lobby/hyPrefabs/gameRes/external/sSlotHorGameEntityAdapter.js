System.register(["__unresolved_0", "cc", "__unresolved_1", "__unresolved_2", "__unresolved_3", "__unresolved_4", "__unresolved_5"], function (_export, _context) {
  "use strict";

  var _reporterNs, _cclegacy, _decorator, Button, director, JsonAsset, Label, Sprite, UIOpacity, sGameEntity, sAudioMgr, sUtil, btnInternationalManager, sIntroView, _dec, _class, _class2, _descriptor, _temp, _crd, ccclass, property, sSlotHorGameEntityAdapter;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  function _reportPossibleCrUseOfsGameEntity(extras) {
    _reporterNs.report("sGameEntity", "./sGameEntity", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsAudioMgr(extras) {
    _reporterNs.report("sAudioMgr", "../../../game/core/sAudioMgr", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsUtil(extras) {
    _reporterNs.report("sUtil", "../../../game/core/sUtil", _context.meta, extras);
  }

  function _reportPossibleCrUseOfbtnInternationalManager(extras) {
    _reporterNs.report("btnInternationalManager", "./btnInternationalManager", _context.meta, extras);
  }

  function _reportPossibleCrUseOfsIntroView(extras) {
    _reporterNs.report("sIntroView", "../../../game/core/sIntroView", _context.meta, extras);
  }

  return {
    setters: [function (_unresolved_) {
      _reporterNs = _unresolved_;
    }, function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      Button = _cc.Button;
      director = _cc.director;
      JsonAsset = _cc.JsonAsset;
      Label = _cc.Label;
      Sprite = _cc.Sprite;
      UIOpacity = _cc.UIOpacity;
    }, function (_unresolved_2) {
      sGameEntity = _unresolved_2.sGameEntity;
    }, function (_unresolved_3) {
      sAudioMgr = _unresolved_3.default;
    }, function (_unresolved_4) {
      sUtil = _unresolved_4.sUtil;
    }, function (_unresolved_5) {
      btnInternationalManager = _unresolved_5.btnInternationalManager;
    }, function (_unresolved_6) {
      sIntroView = _unresolved_6.sIntroView;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "8b76cJO5odFUJrl5+aIYPv5", "sSlotHorGameEntityAdapter", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("sSlotHorGameEntityAdapter", sSlotHorGameEntityAdapter = (_dec = ccclass('sSlotHorGameEntityAdapter'), _dec(_class = (_class2 = (_temp = class sSlotHorGameEntityAdapter extends (_crd && sGameEntity === void 0 ? (_reportPossibleCrUseOfsGameEntity({
        error: Error()
      }), sGameEntity) : sGameEntity) {
        constructor(...args) {
          super(...args);

          _defineProperty(this, "btnRenderNode", void 0);

          _defineProperty(this, "ruleNode", void 0);

          _defineProperty(this, "shopNode", void 0);

          _initializerDefineProperty(this, "ruleViewSize", _descriptor, this);
        }

        onLoad() {
          try {
            this.betLabel = this.node.getChildByPath('betInfo/betAmountLabel').getComponent(Label);
            this.winLabel = this.node.getChildByPath('betInfo/winLabel').getComponent(Label);
            this.verLabel = this.node.parent.getChildByPath('ver').getComponent(Label);
            this.turboBtnNode = this.node.getChildByPath('betBtns/turbo').getComponent(Button);
            this.minusBtnNode = this.node.getChildByPath('betBtns/minus').getComponent(Button);
            this.addBtnNode = this.node.getChildByPath('betBtns/add').getComponent(Button);
            this.autoPlayBtnNode = this.node.getChildByPath('betBtns/auto').getComponent(Button);
            this.betBtnNode = this.node.getChildByPath('btn').getComponent(Button);
            this.menuBtn = this.node.parent.getChildByName('menuBtn').getComponent(Button);
            this.rulesBtn = this.node.getChildByPath('betBtns/info').getComponent(Button);
            this.btnRenderNode = this.node.getChildByPath('betBtns/Spin_Btn');
          } catch (error) {
            console.error('sSlotHorGameEntityAdapter node error : ', error);
          }

          super.onLoad();
          director.on('betBtnState', state => {
            if (state == 'disable') {
              this.spinViewCtr(false);
            } else if (state == 'enable') {
              this.spinViewCtr(true);
            }
          }, this);
          director.on('betBtnClick', betSpeedMode => {
            (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
              error: Error()
            }), sAudioMgr) : sAudioMgr).PlayShotAudio('button');
            this.spinViewCtr(false);
          }, this);
          director.on('autoBetInfoUpdate', value => {// if(value == -1){
            //     this.spinViewCtr(false);
            // }else if(value > 0){
            //     this.spinViewCtr(false);
            // }else{
            //     this.spinViewCtr(true);
            // }
          }, this);
          director.on('viewChange', (type, value) => {
            if (type == 'autoSpinCancel') {
              const toggle = this.autoPlayBtnNode.node.getChildByName('toggle');
              toggle.getChildByName('on').active = false;
              toggle.getChildByName('off').active = true;
            }
          }, this);
          director.on('rollStop', () => {
            if (this.shopNode && this.shopNode.node && this.shopNode.node.isValid) {
              this.shopNode.interactable = true;
              this.shopNode.node.children[0].getComponent(Sprite).grayscale = false;
            }
          }, this);
          director.on('slotSubGameRollBegin', () => {
            if (this.shopNode && this.shopNode.node && this.shopNode.node.isValid) {
              this.shopNode.interactable = false;
              this.shopNode.node.children[0].getComponent(Sprite).grayscale = true;
            }
          }, this);
        }

        start() {
          Promise.all([new Promise((resolve, reject) => {
            globalThis.getSubGamePrefabByPath('prefabs/subGameUserCoin', newObj => {
              if (newObj) {
                newObj.parent = this.node.parent;
                this.ownLabel = newObj.getChildByName('Label').getComponent(Label);
                this.shopNode = newObj.getChildByName('shop').getComponent(Button);
              }

              resolve(1);
            });
          }), new Promise((resolve, reject) => {
            globalThis.getSubGamePrefabByPath('prefabs/subGameSettingDetail', newObj => {
              if (newObj) {
                newObj.parent = this.node.parent.parent;
                this.menuBtns = newObj.getComponent(UIOpacity);
                this.soundBtn = this.menuBtns.node.getChildByPath('main/items/sound').getComponent(Button);
                this.closeBtn = this.menuBtns.node.getChildByPath('close').getComponent(Button);
                this.quitBtn = this.menuBtns.node.getChildByPath('main/items/exit').getComponent(Button);
                newObj.active = false;
              }

              resolve(2);
            });
          }), new Promise((resolve, reject) => {
            globalThis.getSubGamePrefabByPath('prefabs/UIFreeWinBuy', newObj => {
              if (newObj) {
                newObj.parent = this.node.parent.parent;
                this.freeWinBuy = newObj;
              }

              resolve(3);
            });
          }), new Promise((resolve, reject) => {
            globalThis.getSubGamePrefabByPath('prefabs/UISubGameIntro', newObj => {
              if (newObj) {
                newObj.parent = this.node.parent.parent;
                newObj.active = false;
                this.ruleNode = newObj;

                if (this.ruleViewSize != 1000) {
                  this.ruleNode.getComponent(_crd && sIntroView === void 0 ? (_reportPossibleCrUseOfsIntroView({
                    error: Error()
                  }), sIntroView) : sIntroView).changeSize(this.ruleViewSize);
                }
              }

              resolve(4);
            });
          }), new Promise((resolve, reject) => {
            const lua = globalThis.GetLanguageType();

            if (lua) {
              (_crd && sUtil === void 0 ? (_reportPossibleCrUseOfsUtil({
                error: Error()
              }), sUtil) : sUtil).getLobbyAssetInSubGame('config/' + lua, JsonAsset, (err, asset) => {
                if (asset) {
                  (_crd && btnInternationalManager === void 0 ? (_reportPossibleCrUseOfbtnInternationalManager({
                    error: Error()
                  }), btnInternationalManager) : btnInternationalManager).SetLanguegeData(asset.name, asset.json);
                }

                resolve(5);
              });
            } else {
              resolve(5);
            }
          })]).then(res => {
            super.start();

            if (this.shopNode) {
              this.shopNode.node.on('click', () => {
                globalThis.openStore && globalThis.openStore();
              }, this);
            }
          });
        }

        spinViewCtr(value) {
          this.btnRenderNode.getComponent(Sprite).grayscale = !value; // this.btnRenderNode.children[0].getComponent(Sprite).grayscale = !value;
        }

        autoPlayBtnClick() {
          if (this.canBet) {
            if (this.betBtnState == 'normal') {
              if (this.betClickMode == 'normal') {
                this.setGameBtnState('disable');
                const toggle = this.autoPlayBtnNode.node.getChildByName('toggle');
                toggle.getChildByName('on').active = true;
                toggle.getChildByName('off').active = false;
                director.emit('viewChange', 'UIAutoSpin', -1);
              } else if (this.betClickMode == 'autoBet') {
                const toggle = this.autoPlayBtnNode.node.getChildByName('toggle');
                toggle.getChildByName('on').active = false;
                toggle.getChildByName('off').active = true;
                director.emit('viewChange', 'autoSpinCancel');
              }
            } else {
              if (this.betClickMode == 'autoBet') {
                const toggle = this.autoPlayBtnNode.node.getChildByName('toggle');
                toggle.getChildByName('on').active = false;
                toggle.getChildByName('off').active = true;
                director.emit('viewChange', 'autoSpinCancel');
              }
            }
          } else {
            this.betClickCoinNotEnough();
          }
        }

        errToOneRes() {
          const toggle = this.autoPlayBtnNode.node.getChildByName('toggle');
          toggle.getChildByName('on').active = false;
          toggle.getChildByName('off').active = true;
          super.errToOneRes();
        }

        menuBtnClick() {
          director.emit('subGameMenuView', true);
          const audioData = globalThis.getSubGameAudioVolume();

          if (audioData) {
            if (audioData.audioVolume == 1) {
              this.soundBtn.node.getChildByName('open').active = true;
              this.soundBtn.node.getChildByName('close').active = false;
            } else {
              this.soundBtn.node.getChildByName('close').active = true;
              this.soundBtn.node.getChildByName('open').active = false;
            }
          }

          this.menuBtns.node.active = true;
        }

        soundBtnClick() {
          if (this.audioTouchEnable) {
            this.audioTouchEnable = false;
            const audioData = globalThis.getSubGameAudioVolume();

            if (audioData) {
              if (audioData.audioVolume == 1) {
                globalThis.subGameAudioVolumeCtr(false);
                this.soundBtn.node.getChildByName('close').active = true;
                this.soundBtn.node.getChildByName('open').active = false;
              } else {
                globalThis.subGameAudioVolumeCtr(true);
                this.soundBtn.node.getChildByName('open').active = true;
                this.soundBtn.node.getChildByName('close').active = false;
              }
            }

            this.scheduleOnce(() => {
              this.audioTouchEnable = true;
            }, 1);
          }
        }

        closeBtnClick() {
          director.emit('subGameMenuView', false);
          this.menuBtns.node.active = false;
        }

        rulesBtnClick() {
          this.ruleNode.active = true;
        }

        addBtnClick() {
          super.addBtnClick();
          (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
            error: Error()
          }), sAudioMgr) : sAudioMgr).PlayShotAudio('button');
        }

        minusBtnClick() {
          super.minusBtnClick();
          (_crd && sAudioMgr === void 0 ? (_reportPossibleCrUseOfsAudioMgr({
            error: Error()
          }), sAudioMgr) : sAudioMgr).PlayShotAudio('button');
        }

        turboBtnNodeClick(value) {
          if (this.turboBtnNode) {
            if (value) {
              const turboOff = this.turboBtnNode.node.getChildByName('turbo_off');
              turboOff.active = false;
              const turboOn = this.turboBtnNode.node.getChildByName('turbo_on');
              turboOn.active = true;
            } else {
              const turboOn = this.turboBtnNode.node.getChildByName('turbo_on');
              turboOn.active = false;
              const turboOff = this.turboBtnNode.node.getChildByName('turbo_off');
              turboOff.active = true;
            }
          }
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "ruleViewSize", [property], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return 1000;
        }
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=sSlotHorGameEntityAdapter.js.map
System.register(["cc"], function (_export, _context) {
  "use strict";

  var _cclegacy, _decorator, Component, Slider, UITransform, Sprite, Label, _dec, _dec2, _dec3, _dec4, _dec5, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _descriptor5, _descriptor6, _descriptor7, _temp, _crd, ccclass, property, subGameSliderFore;

  function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

  function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }

  return {
    setters: [function (_cc) {
      _cclegacy = _cc.cclegacy;
      _decorator = _cc._decorator;
      Component = _cc.Component;
      Slider = _cc.Slider;
      UITransform = _cc.UITransform;
      Sprite = _cc.Sprite;
      Label = _cc.Label;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "c1866ZVO/FIGL5Qm30Km/Jj", "subGameSliderFor", undefined);

      ({
        ccclass,
        property
      } = _decorator);

      _export("subGameSliderFore", subGameSliderFore = (_dec = ccclass('subGameSliderFore'), _dec2 = property(Slider), _dec3 = property(UITransform), _dec4 = property(UITransform), _dec5 = property(Label), _dec(_class = (_class2 = (_temp = class subGameSliderFore extends Component {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "m_Slider", _descriptor, this);

          _initializerDefineProperty(this, "m_ForNode", _descriptor2, this);

          _initializerDefineProperty(this, "m_BackNode", _descriptor3, this);

          _initializerDefineProperty(this, "forNodeWidth", _descriptor4, this);

          _initializerDefineProperty(this, "forNodeHeight", _descriptor5, this);

          _initializerDefineProperty(this, "rollOver", _descriptor6, this);

          _initializerDefineProperty(this, "percent", _descriptor7, this);

          _defineProperty(this, "isFinish", false);

          _defineProperty(this, "finishCallBack", null);
        }

        start() {
          this.SliderForUIAdapter(); // this.m_Slider.node.on('slide',(slider : Slider)=>{
          //     console.log('slider:'+slider.progress);
          // },this);
        }

        InitSlider() {
          this.isFinish = false;
        }

        SetFinishCallBack(call) {
          this.finishCallBack = call;
        }

        SliderForUIAdapter() {
          if (this.m_Slider) {
            const progress = Math.trunc(this.m_Slider.progress * 100) / 100;

            if (this.m_Slider.direction == 1) {
              if (this.forNodeHeight != 0) {
                if (this.rollOver) {
                  this.m_ForNode.width = this.forNodeHeight * progress;
                } else {
                  this.m_ForNode.height = this.forNodeHeight * progress;
                }
              } else {
                if (this.rollOver) {
                  this.m_ForNode.width = this.m_BackNode.height * progress;
                } else {
                  this.m_ForNode.height = this.m_BackNode.height * progress;
                }
              }
            } else if (this.m_Slider.direction == 0) {
              if (this.forNodeWidth != 0) {
                if (this.rollOver) {
                  this.m_ForNode.height = this.forNodeWidth * progress;
                } else {
                  this.m_ForNode.width = this.forNodeWidth * progress;
                }
              } else {
                if (this.rollOver) {
                  this.m_ForNode.height = this.m_BackNode.width * progress;
                } else {
                  this.m_ForNode.width = this.m_BackNode.width * progress;
                }
              }
            }
          }
        }

        setValue(value, title = "") {
          value = Math.floor(value * 100);

          if (value < 0) {
            value = 0;
          }

          if (value > 100) {
            value = 100;
          }

          this.m_Slider.progress = value / 100;
          this.percent.string = `${title}:${value}%`;
          this.SliderForUIAdapter();

          if (Math.abs(1 - value / 100) <= 0.001) {
            if (!this.isFinish) {
              this.isFinish = true;

              if (this.finishCallBack != null) {
                this.finishCallBack();
              }
            }
          }
        }

        setRealValue(value) {
          value = value < 0 ? 0 : value > 1 ? 1 : value; // console.log('value:'+value);

          this.m_Slider.progress = value;
          this.SliderForUIAdapter();

          if (Math.abs(1 - value) <= 0.001) {
            if (!this.isFinish) {
              this.isFinish = true;

              if (this.finishCallBack != null) {
                this.finishCallBack();
              }
            }
          }
        }

        setForeColor(_color) {
          this.m_ForNode.getComponent(Sprite).color = _color;
        }

        getValue() {
          return this.m_Slider.progress;
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "m_Slider", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return null;
        }
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "m_ForNode", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return null;
        }
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "m_BackNode", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return null;
        }
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "forNodeWidth", [property], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return 0;
        }
      }), _descriptor5 = _applyDecoratedDescriptor(_class2.prototype, "forNodeHeight", [property], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return 0;
        }
      }), _descriptor6 = _applyDecoratedDescriptor(_class2.prototype, "rollOver", [property], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return false;
        }
      }), _descriptor7 = _applyDecoratedDescriptor(_class2.prototype, "percent", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return null;
        }
      })), _class2)) || _class));

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=subGameSliderFor.js.map
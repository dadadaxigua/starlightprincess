System.register(["cc"], function (_export, _context) {
  "use strict";

  var _cclegacy, _crd, loadInfo;

  return {
    setters: [function (_cc) {
      _cclegacy = _cc.cclegacy;
    }],
    execute: function () {
      _crd = true;

      _cclegacy._RF.push({}, "8c8cc4NtIZKbKbQev40rYGH", "load-back", undefined);

      loadInfo = {
        ischeck: false,
        iserror: false,
        isLogin: false,
        isLobbyAct: false
      };

      _export("default", loadInfo);

      _cclegacy._RF.pop();

      _crd = false;
    }
  };
});
//# sourceMappingURL=load-back.js.map
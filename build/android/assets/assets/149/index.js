System.register("chunks:///_virtual/StarlightPrincess_Entity.ts", ['./_rollupPluginModLoBabelHelpers.js', 'cc', './sAudioMgr.ts', './sObjPool.ts', './sBoxEntity.ts', './sBoxAssetInit.ts', './StarlightPrincess_Thunder.ts'], function (exports) {
  'use strict';

  var _defineProperty, cclegacy, _decorator, sAudioMgr, sObjPool, sBoxEntity, sBoxAssetInit, StarlightPrincess_Thunder;

  return {
    setters: [function (module) {
      _defineProperty = module.defineProperty;
    }, function (module) {
      cclegacy = module.cclegacy;
      _decorator = module._decorator;
    }, function (module) {
      sAudioMgr = module.default;
    }, function (module) {
      sObjPool = module.sObjPool;
    }, function (module) {
      sBoxEntity = module.sBoxEntity;
    }, function (module) {
      sBoxAssetInit = module.sBoxAssetInit;
    }, function (module) {
      StarlightPrincess_Thunder = module.StarlightPrincess_Thunder;
    }],
    execute: function () {
      var _dec, _class, _temp;

      cclegacy._RF.push({}, "209bdCMyGVMkpMNV3gSLDK2", "StarlightPrincess_Entity", undefined);

      const {
        ccclass,
        property
      } = _decorator;

      class BoxObj {
        constructor() {
          _defineProperty(this, "box", void 0);

          _defineProperty(this, "overEffect", void 0);
        }

      }

      let StarlightPrincess_Entity = exports('StarlightPrincess_Entity', (_dec = ccclass('StarlightPrincess_Entity'), _dec(_class = (_temp = class StarlightPrincess_Entity extends sBoxEntity {
        constructor(...args) {
          super(...args);

          _defineProperty(this, "boxObj", new BoxObj());

          _defineProperty(this, "thundered", false);
        } //是否可以消除该盒子


        get IsDisappearBox() {
          if (this.boxStateNow == 'win' && this.SymbolValue != 1 && this.SymbolValue < 2000) {
            return true;
          }

          return false;
        }

        EntityInit() {
          super.EntityInit();
          this.RegisterEnitityAction('thunder', () => {
            //if(!this.thundered){
            //console.log('格子位置：',this.viewItemIndex.x,this.viewItemIndex.y);
            //if(this.viewItemIndex.y >= 0 && this.viewItemIndex.y <= 4){
            //this.thundered = true;
            if (this.BoxStateNow != 'idle') {
              this.UpdateData('idle', 'settlement');
            }

            let spineAnimaName = 'gool_summon_effect_1';
            const rSymbol = this.symbolValue;

            if (rSymbol > 2000 && rSymbol < 2007) {
              spineAnimaName = 'gool_summon_effect_1';
            } else if (rSymbol >= 2007 && rSymbol < 2011) {
              spineAnimaName = 'gool_summon_effect_2';
            } else if (rSymbol >= 2011 && rSymbol < 2014) {
              spineAnimaName = 'gool_summon_effect_3';
            } else {
              spineAnimaName = 'gool_summon_effect_4';
            }

            const pos = this.GetBoxWorldPosition();
            pos.x += 80;
            pos.y += 290;
            const thunder = sObjPool.Dequeue('thunder');

            if (thunder && thunder.isValid) {
              thunder.parent = sBoxAssetInit.instance.getTargetNodeByName('effectLayer');
              thunder.worldPosition = pos;
              thunder.active = true;
              sAudioMgr.PlayShotAudio('mult_appear');
              thunder.getComponent(StarlightPrincess_Thunder).play(spineAnimaName);
            } //}
            //}

          });
        } // protected JackpotBingo(){
        //     if(this.IsJackpotBox){
        //         this.UpdateData('win','settlement');
        //     }
        // }


        UpdateBoxPosition(pos) {
          super.UpdateBoxPosition(pos);
          this.boxObj.box.node.position = pos;
          this.boxObj.overEffect.node.position = pos;
        }

        UpdateData(boxState, tableState) {
          this.boxStateNow = boxState;
          this.boxData = this.getSymbolData(this.SymbolValue);

          if (this.boxData) {
            const renderData = this.boxData[boxState]; // if(this.SymbolValue == 88 && boxState == 'idle'){
            //     if(this.ViewItemIndex.y >= 0 && this.ViewItemIndex.y <= 4){
            //         this.boxObj.box.boxItemUpdate(renderData,this.SymbolValue,boxState,tableState);
            //     }
            // }else{
            //     this.boxObj.box.boxItemUpdate(renderData,this.SymbolValue,boxState,tableState);
            // }

            if (this.symbolValue == 1 && this.ViewItemIndex.y >= 0 && this.ViewItemIndex.y <= 4) {
              this.boxObj.box.clearItem(this.SymbolValue, boxState, tableState);
              this.boxObj.box.node.active = false;
              this.boxObj.overEffect.node.active = true;
              this.boxObj.overEffect.boxItemUpdate(renderData, this.SymbolValue, boxState, tableState);
            } else {
              this.boxObj.overEffect.clearItem(this.SymbolValue, boxState, tableState);
              this.boxObj.overEffect.node.active = false;
              this.boxObj.box.boxItemUpdate(renderData, this.SymbolValue, boxState, tableState);
              this.boxObj.box.node.active = true;
            } //     this.boxObj.overEffect.node.position = this.boxPos;
            //     this.boxObj.overEffect.node.active = true;
            //     this.boxObj.overEffect.boxItemUpdate(renderData,this.SymbolValue,boxState,tableState);
            // }


            let pstr = this.SymbolValue.toString();
            pstr = pstr.slice(2);

            if (boxState == 'idle' && this.symbolValue == 1) {
              if (this.ViewItemIndex.y >= 0 && this.ViewItemIndex.y <= 4) {
                sAudioMgr.PlayShotAudio('scatter_appear');
              }
            } else if (boxState == 'idle' && this.symbolValue > 2000) {
              if (this.ViewItemIndex.y >= 0 && this.ViewItemIndex.y <= 4) {
                let str = 'scatter_appear_voice' + parseInt(pstr);
                sAudioMgr.PlayShotAudio(str);
              }
            }
          }
        }

        ClearItem(boxState, tableState) {
          super.ClearItem(boxState, tableState);
          this.thundered = false;

          if (this.boxObj.box) {
            this.boxObj.box.clearItem(null, boxState, tableState);
          }

          if (this.boxObj.overEffect) {
            this.boxObj.overEffect.node.active = false;
            this.boxObj.overEffect.clearItem(null, boxState, tableState);
          }
        }

      }, _temp)) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/StarlightPrincess_SceneAdater.ts", ['./_rollupPluginModLoBabelHelpers.js', 'cc', './sComponent.ts', './sUtil.ts', './sAudioMgr.ts', './sObjPool.ts', './sBoxMgr.ts', './sBoxAssetInit.ts', './StarlightPrincess_Entity.ts', './soltGameCheckLine_149.ts', './StarlightPrincess_WinTipLabel.ts'], function (exports) {
  'use strict';

  var _applyDecoratedDescriptor, _initializerDefineProperty, cclegacy, Prefab, Node, _decorator, view, ResolutionPolicy, director, assetManager, instantiate, Color, sp, v3, sComponent, sUtil, sAudioMgr, sObjPool, sBoxMgr, sBoxAssetInit, StarlightPrincess_Entity, soltGameCheckUtil_149, StarlightPrincess_WinTipLabel;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _initializerDefineProperty = module.initializerDefineProperty;
    }, function (module) {
      cclegacy = module.cclegacy;
      Prefab = module.Prefab;
      Node = module.Node;
      _decorator = module._decorator;
      view = module.view;
      ResolutionPolicy = module.ResolutionPolicy;
      director = module.director;
      assetManager = module.assetManager;
      instantiate = module.instantiate;
      Color = module.Color;
      sp = module.sp;
      v3 = module.v3;
    }, function (module) {
      sComponent = module.sComponent;
    }, function (module) {
      sUtil = module.sUtil;
    }, function (module) {
      sAudioMgr = module.default;
    }, function (module) {
      sObjPool = module.sObjPool;
    }, function (module) {
      sBoxMgr = module.sBoxMgr;
    }, function (module) {
      sBoxAssetInit = module.sBoxAssetInit;
    }, function (module) {
      StarlightPrincess_Entity = module.StarlightPrincess_Entity;
    }, function (module) {
      soltGameCheckUtil_149 = module.soltGameCheckUtil_149;
    }, function (module) {
      StarlightPrincess_WinTipLabel = module.StarlightPrincess_WinTipLabel;
    }],
    execute: function () {
      var _dec, _dec2, _dec3, _dec4, _dec5, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _temp;

      cclegacy._RF.push({}, "2c880eHBwpCe4TNJRL+L4E0", "StarlightPrincess_SceneAdater", undefined);

      const {
        ccclass,
        property
      } = _decorator;
      let StarlightPrincess_SceneAdater = exports('StarlightPrincess_SceneAdater', (_dec = ccclass('StarlightPrincess_SceneAdater'), _dec2 = property(Prefab), _dec3 = property([Node]), _dec4 = property([Node]), _dec5 = property(Node), _dec(_class = (_class2 = (_temp = class StarlightPrincess_SceneAdater extends sComponent {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "freebuyNode", _descriptor, this);

          _initializerDefineProperty(this, "normalNodes", _descriptor2, this);

          _initializerDefineProperty(this, "freeWinNodes", _descriptor3, this);

          _initializerDefineProperty(this, "freebuybutton", _descriptor4, this);
        } // @property(Label)
        // jackpotLabel : Label;


        onLoad() {
          globalThis.currentPlayingGameID = 149;

          if (!globalThis.boxEntityCtr) {
            globalThis.boxEntityCtr = {};
          }

          globalThis.boxEntityCtr[globalThis.currentPlayingGameID] = StarlightPrincess_Entity;
        }

        start() {
          globalThis.soltGameCheckWon = soltGameCheckUtil_149;
          sAudioMgr.AudioInit();
          sAudioMgr.SetBGVolume(0.5);
          sAudioMgr.PlayBG('bgm_mg', true);
          view.setDesignResolutionSize(1280, 720, ResolutionPolicy.FIXED_HEIGHT);
          director.on('CorrectResolution', () => {
            view.setDesignResolutionSize(1280, 720, ResolutionPolicy.FIXED_HEIGHT);
          }, this); // director.emit('bonusLabelInit',globalThis.currentPlayingGameID,[this.jackpotLabel]);
          // console.log(this.node.getChildByName('Camera').getComponent(Camera).orthoHeight);
          // sUtil.getSubGameBtnByName(this.bottomPrefab,globalThis.currentPlayingGameID,'subGameBtn_Rist','none',0,'20231011_7',color(251,224,76,255),(subGameBtn : Node)=>{
          //     if(subGameBtn){
          //         subGameBtn.setParent(this.node);
          //         director.emit('getSubGameBtnByPathEnd');
          //     };
          // });
          // sUtil.getSubGameBtnByPath('subGameBtn_StarlightPrincess',globalThis.currentPlayingGameID,'none',0,'20230726_4',Color.WHITE,subGameBtn=>{
          //     if(subGameBtn){
          //         subGameBtn.setParent(this.node);
          //         this.SetNodeFront(subGameBtn);
          //         director.emit('getSubGameBtnByPathEnd');
          //     };
          // });

          const bundle = assetManager.getBundle('149');

          if (bundle) {
            console.log('bundle');
            bundle.load('subGameBtn_Starlight', Prefab, (err, _prefab) => {
              console.log('err:', JSON.stringify(err));
              console.log('_prefab', _prefab);

              if (_prefab) {
                console.log('_prefab');
                let obj = instantiate(_prefab);

                if (obj) {
                  obj['gameid'] = 149;
                  obj['aligmentType'] = 'none';
                  obj['aligmentPixel'] = 0;
                  obj['gameVersion'] = '20231019_5';
                  obj['customColor'] = Color.WHITE;

                  if (_prefab) {
                    console.log('加载btn成功');
                    obj.setParent(this.node);
                    this.SetNodeFront(obj);
                    director.emit('getSubGameBtnByPathEnd');
                  } else {
                    console.log('加载btn失败');
                  }
                }
              }
            });
          } //     try { 
          //         sUtil.getSubGameBtnByPath('subGameBtn_toa', globalThis.currentPlayingGameID, 'none', 0, '20231018_7', Color.WHITE, subGameBtn => {
          //                     if (subGameBtn) {
          //                         console.log('加载btn1成功');
          //                         subGameBtn.setParent(this.node);
          //                         this.SetNodeFront(subGameBtn);
          //                         director.emit('getSubGameBtnByPathEnd');
          //                     } else {
          //                         console.log('加载btn1失败');
          //                     };
          //         });
          //     } catch (error) {
          //         console.log('加载错误1' + error);
          //     };


          director.on('freeWinBegain', () => {
            this.scheduleOnce(() => {
              for (let i = 0; i < this.normalNodes.length; i++) {
                const element = this.normalNodes[i];
                element.active = false;
              }

              for (let i = 0; i < this.freeWinNodes.length; i++) {
                const element = this.freeWinNodes[i];
                element.active = true;

                if (element.name == 'stpr_freegame_idle') {
                  console.log('免费模式地图', element);
                  element.getComponent(sp.Skeleton).setAnimation(0, 'stpr_freegame_idle', true);
                }
              }
            }, 0.6);
          }, this);
          director.on('freeWinOver', () => {
            for (let i = 0; i < this.normalNodes.length; i++) {
              const element = this.normalNodes[i];
              element.active = true;
            }

            for (let i = 0; i < this.freeWinNodes.length; i++) {
              const element = this.freeWinNodes[i];
              element.active = false;
            }
          }, this);
          director.on('uiTipsOpen', (mode, tex) => {
            sUtil.toast(tex);
          }, this);
          director.on('slotWinBoxItemInfo', obj => {
            if (obj) {
              //console.log('boxPos:',obj.boxPos);
              if (obj.boxPos) {
                const posStr = obj.boxPos.split('_');

                if (posStr && posStr.length == 2) {
                  const boxPos = sBoxMgr.instance.getBoxWorldPosByXY(parseInt(posStr[0]), parseInt(posStr[1]));

                  if (boxPos) {
                    const boxWinTip = sObjPool.Dequeue('boxWinTipLabel');

                    if (boxWinTip && boxWinTip.isValid) {
                      boxWinTip.parent = sBoxAssetInit.instance.getTargetNodeByName('effectLayer');
                      boxWinTip.worldPosition = v3(boxPos.x, boxPos.y - 48, 0);
                      boxWinTip.active = true;
                      boxWinTip.getComponent(StarlightPrincess_WinTipLabel).play(this.AddCommas(obj.rate * globalThis.GameBtnEntity.CurrentBetAmount));
                    }
                  }
                }
              }
            }
          }, this);
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "freebuyNode", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "normalNodes", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return [];
        }
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "freeWinNodes", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return [];
        }
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "freebuybutton", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/StarlightPrincess_GameEntity.ts", ['./_rollupPluginModLoBabelHelpers.js', 'cc', './sUtil.ts', './sAudioMgr.ts', './sGameEntity.ts'], function (exports) {
  'use strict';

  var _applyDecoratedDescriptor, _initializerDefineProperty, _defineProperty, cclegacy, Node, Button, Label, _decorator, director, Widget, Sprite, UIOpacity, v3, tween, Vec3, sUtil, sAudioMgr, sGameEntity;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _initializerDefineProperty = module.initializerDefineProperty;
      _defineProperty = module.defineProperty;
    }, function (module) {
      cclegacy = module.cclegacy;
      Node = module.Node;
      Button = module.Button;
      Label = module.Label;
      _decorator = module._decorator;
      director = module.director;
      Widget = module.Widget;
      Sprite = module.Sprite;
      UIOpacity = module.UIOpacity;
      v3 = module.v3;
      tween = module.tween;
      Vec3 = module.Vec3;
    }, function (module) {
      sUtil = module.sUtil;
    }, function (module) {
      sAudioMgr = module.default;
    }, function (module) {
      sGameEntity = module.sGameEntity;
    }],
    execute: function () {
      var _dec, _dec2, _dec3, _dec4, _dec5, _dec6, _dec7, _dec8, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _descriptor5, _descriptor6, _descriptor7, _temp;

      cclegacy._RF.push({}, "335c9ncRt5EuogPXDve42Y6", "StarlightPrincess_GameEntity", undefined);

      const {
        ccclass,
        property
      } = _decorator;
      let StarlightPrincess_GameEntity = exports('StarlightPrincess_GameEntity', (_dec = ccclass('StarlightPrincess_GameEntity'), _dec2 = property(Node), _dec3 = property(Button), _dec4 = property(Node), _dec5 = property(Label), _dec6 = property(Label), _dec7 = property(Label), _dec8 = property({
        type: [Button]
      }), _dec(_class = (_class2 = (_temp = class StarlightPrincess_GameEntity extends sGameEntity {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "btnRenderNode", _descriptor, this);

          _initializerDefineProperty(this, "infoBtn", _descriptor2, this);

          _initializerDefineProperty(this, "ruleNode", _descriptor3, this);

          _initializerDefineProperty(this, "betDetailLabel", _descriptor4, this);

          _initializerDefineProperty(this, "winStringLabel", _descriptor5, this);

          _initializerDefineProperty(this, "winStringLabel1", _descriptor6, this);

          _initializerDefineProperty(this, "Btns", _descriptor7, this);

          _defineProperty(this, "betSpeedModeTemp1", 'normal');

          _defineProperty(this, "shopNode", void 0);

          _defineProperty(this, "pTween", null);

          _defineProperty(this, "callBack", null);

          _defineProperty(this, "updateLabTween", void 0);
        }

        onLoad() {
          super.onLoad();
          director.on('betBtnState', state => {
            if (state == 'disable') {
              this.spinViewCtr(false);
            } else if (state == 'enable') {
              this.spinViewCtr(true);
            }
          }, this);
          director.on('betBtnClick', betSpeedMode => {
            sAudioMgr.PlayShotAudio('button');
            this.spinViewCtr(false);
          }, this);
          director.on('autoBetInfoUpdate', value => {// if(value == -1){
            //     this.spinViewCtr(false);
            // }else if(value > 0){
            //     this.spinViewCtr(false);
            // }else{
            //     this.spinViewCtr(true);
            // }
          }, this);
          director.on('viewChange', (type, value) => {
            if (type == 'autoSpinCancel') {
              const toggle = this.autoPlayBtnNode.node.getChildByName('toggle');
              toggle.getChildByName('on').active = false;
              toggle.getChildByName('off').active = true;
            }
          }, this);
        }

        start() {
          Promise.all([new Promise((resolve, reject) => {
            globalThis.getSubGamePrefabByPath('prefabs/subGameUserCoin', newObj => {
              if (newObj) {
                newObj.parent = this.node.parent;
                this.ownLabel = newObj.getChildByName('Label').getComponent(Label);
                this.shopNode = newObj.getChildByName('shop').getComponent(Button);
                newObj.getComponent(Widget).isAlignLeft = true;
                newObj.getComponent(Widget).isAlignTop = true;
                newObj.getComponent(Widget).left = -20;
                newObj.getComponent(Widget).top = -10;
              }

              resolve(1);
            });
          })]).then(res => {
            super.start();

            if (this.shopNode) {
              this.shopNode.node.on('click', () => {
                globalThis.openStore && globalThis.openStore();
              }, this);
            }
          });
        }

        spinViewCtr(value) {
          this.btnRenderNode.getComponent(Sprite).grayscale = !value;
          this.btnRenderNode.children[0].getComponent(Sprite).grayscale = !value;
        }

        freeWinBegain() {
          // if(this.viewMode == 'record'){
          //     if(this.recordBtns){
          //         this.recordBtns.node.active = true;
          //     }
          // }
          // this.userCoin = this.beforeUserCoin;
          // this.winTotal = this.firstRate > 0 ? this.firstRate * this.betAmount : 0;
          for (let btn of this.Btns) {
            btn.node.getComponent(UIOpacity).opacity = 100;
            btn.getComponent(Button).interactable = false;
          }
        }

        freeWinOver() {
          // if(this.viewMode == 'record'){
          //     if(this.recordBtns){
          //         this.recordBtns.node.active = true;
          //     }
          // }
          // this.userCoin = this.beforeUserCoin;
          // this.winTotal = this.firstRate > 0 ? this.firstRate * this.betAmount : 0;
          for (let btn of this.Btns) {
            btn.node.getComponent(UIOpacity).opacity = 255;
            btn.getComponent(Button).interactable = true;
          }
        } // turboBtnAction() {
        //     globalThis.subGamePlayShotAudio('subGameClick');
        //     if (this.betSpeedModeTemp1 == 'normal') {
        //         this.autoPlayBtnNode.getComponent(UIOpacity).opacity=100;
        //         this.betSpeedModeTemp1 = 'turbo';
        //         this.turboBtnNodeClick(true);
        //     } else {
        //         this.autoPlayBtnNode.getComponent(UIOpacity).opacity=100;
        //         this.betSpeedModeTemp1 = 'normal';
        //         this.turboBtnNodeClick(false);
        //     }
        //     director.emit('uiTipsOpen', this.betSpeedModeTemp1, this.betSpeedModeTemp1 == 'normal' ? btnInternationalManager.GetDataByKey('closeTurboSpinMode') : btnInternationalManager.GetDataByKey('openTurboSpinMode'));
        // }


        turboBtnNodeClick(value) {
          // this.turboBtnNode.getComponent(UIOpacity).opacity=(value)?100:255;
          if (value) {
            this.turboBtnNode.node.getChildByName('off').active = false;
            this.turboBtnNode.node.getChildByName('on').active = true;
          } else {
            this.turboBtnNode.node.getChildByName('off').active = true;
            this.turboBtnNode.node.getChildByName('on').active = false;
          }
        }

        autoPlayBtnClick() {
          if (this.canBet) {
            if (this.betBtnState == 'normal') {
              if (this.betClickMode == 'normal') {
                this.setGameBtnState('disable'); //this.autoPlayBtnNode.getComponent(UIOpacity).opacity=100;

                const toggle = this.autoPlayBtnNode.node.getChildByName('toggle');
                toggle.getChildByName('on').active = true;
                toggle.getChildByName('off').active = false;
                director.emit('viewChange', 'UIAutoSpin', -1);
              } else if (this.betClickMode == 'autoBet') {
                const toggle = this.autoPlayBtnNode.node.getChildByName('toggle'); //this.autoPlayBtnNode.getComponent(UIOpacity).opacity=255;

                toggle.getChildByName('on').active = false;
                toggle.getChildByName('off').active = true;
                director.emit('viewChange', 'autoSpinCancel');
              }
            } else {
              if (this.betClickMode == 'autoBet') {
                //this.autoPlayBtnNode.getComponent(UIOpacity).opacity=255;
                const toggle = this.autoPlayBtnNode.node.getChildByName('toggle');
                toggle.getChildByName('on').active = false;
                toggle.getChildByName('off').active = true;
                director.emit('viewChange', 'autoSpinCancel');
              }
            }
          } else {
            this.betClickCoinNotEnough();
          }
        }

        errToOneRes() {
          const toggle = this.autoPlayBtnNode.node.getChildByName('toggle');
          toggle.getChildByName('on').active = false;
          toggle.getChildByName('off').active = true;
          super.errToOneRes();
        }

        menuBtnClick() {
          director.emit('subGameMenuView', true);
          const audioData = globalThis.getSubGameAudioVolume();

          if (audioData) {
            if (audioData.audioVolume == 1) {
              this.soundBtn.node.getChildByName('open').active = true;
              this.soundBtn.node.getChildByName('close').active = false;
            } else {
              this.soundBtn.node.getChildByName('close').active = true;
              this.soundBtn.node.getChildByName('open').active = false;
            }
          }

          this.menuBtns.node.active = true;
        }

        soundBtnClick() {
          if (this.audioTouchEnable) {
            this.audioTouchEnable = false;
            const audioData = globalThis.getSubGameAudioVolume();

            if (audioData) {
              if (audioData.audioVolume == 1) {
                globalThis.subGameAudioVolumeCtr(false);
                this.soundBtn.node.getChildByName('close').active = true;
                this.soundBtn.node.getChildByName('open').active = false;
              } else {
                globalThis.subGameAudioVolumeCtr(true);
                this.soundBtn.node.getChildByName('open').active = true;
                this.soundBtn.node.getChildByName('close').active = false;
              }
            }

            this.scheduleOnce(() => {
              this.audioTouchEnable = true;
            }, 1);
          }
        }

        closeBtnClick() {
          director.emit('subGameMenuView', false);
          this.menuBtns.node.active = false;
        }

        rulesBtnClick() {
          this.ruleNode.active = true;
        }

        addBtnClick() {
          super.addBtnClick();
          sAudioMgr.PlayShotAudio('button');
        }

        minusBtnClick() {
          super.minusBtnClick();
          sAudioMgr.PlayShotAudio('button');
        }

        updateBetInfo() {
          super.updateBetInfo();
          this.betDetailLabel.string = `${this.AddCommas(this.betAmount)} x ${20}`;
        }

        betUserInfoUpdate(own, bet, win) {
          if (this.ownLabel) {
            if (own || own == 0) {
              this.ownLabel.node['rollAnimaValue'] = own;
              this.ownLabel.string = this.AddCommas(Math.trunc(own));
            }
          }

          if (this.betLabel) {
            if (bet || bet == 0) {
              this.betLabel.node['rollAnimaValue'] = bet;
              this.betLabel.string = this.AddCommas(Math.trunc(bet));
            }
          }

          if (win == null) {
            this.winLabel.node['rollAnimaValue'] = 0;
            console.log('重置winLabel1');
            this.winLabel.node.active = false;
            this.winStringLabel.node.active = true;
            this.winStringLabel1.node.active = false;
            this.winLabel.string = 0 + '';
          } else {
            if (win && win > 0) {
              //console.log('win1:',win);
              this.winLabel.node['rollAnimaValue'] = win;
              console.log('重置winLabel2');
              this.winLabel.string = this.AddCommas(Math.trunc(win));
              this.winStringLabel1.node.active = true;
            }
          }
        }

        betUserInfoUpdateAnima(own, bet, win, animaTime = 1, callBack = null) {
          if (own || own == 0) {
            this.CommasLabelAnima(this.ownLabel, Math.trunc(own), animaTime);
          }

          if (bet || bet == 0) {
            this.CommasLabelAnima(this.betLabel, Math.trunc(bet), animaTime);
          }

          if (win == null) {
            this.winLabel.node['rollAnimaValue'] = 0;
            console.log('重置winLabel3');
            this.winLabel.string = 0 + '';
          } else {
            if (win && win > 0) {
              this.winLabel.node.active = true;
              this.winStringLabel.node.active = false;
              this.winStringLabel1.node.active = true; //console.log('win3:',win);

              let time = 0.5;

              if (win >= this.currentBetAmount * 200) {
                time = 12;
              }

              if (!this.winLabel.node['rollAnimaValue']) {
                //c//onsole.log('重置winLabel4');
                this.winLabel.node['rollAnimaValue'] = 0;
              }

              let num = parseInt(this.winLabel.string); //console.log('win:',win);
              //console.log('num:',num);
              //console.log('this.winLabel.string:',this.winLabel.string);
              //console.log('rollAnimaValue1',this.winLabel.node['rollAnimaValue']);
              //sUtil.TweenLabel(this.winLabel.node['rollAnimaValue'],Math.trunc(win+this.winLabel.node['rollAnimaValue']),this.winLabel,time);

              this.updateWinLab(this.winLabel.node['rollAnimaValue'], this.winLabel, Math.trunc(win + this.winLabel.node['rollAnimaValue']), time, this.callBack); //this.winLabel.node['rollAnimaValue'] += win;

              this.scheduleOnce(() => {
                this.winLabel.node['rollAnimaValue'] += Math.trunc(win); //console.log('rollAnimaValue2',this.winLabel.node['rollAnimaValue']);
              }, time);
              director.off('StopLabelanima');
              sUtil.once('StopLabelanima', () => {
                //this.winLabel.node['rollAnimaValue'] += win;
                this.updateLabTween.stop();
                this.winLabel.string = this.AddCommas(Math.floor(win));
              });
            } else {
              if (!this.winLabel.node['rollAnimaValue']) {
                //console.log('重置winLabel5');
                this.winLabel.node['rollAnimaValue'] = 0;
                this.winLabel.string = 0 + '';
              } // this.winLabel.node.active=false;
              // this.winStringLabel.node.active=true;
              // this.winStringLabel1.node.active=false

            }
          }

          if (callBack != null) {
            this.scheduleOnce(callBack, animaTime);
          }
        }

        updateWinLab(lastCoin, winLabel, coin, time, cb) {
          let tweenTargetVec3 = v3(lastCoin, lastCoin, lastCoin);
          this.updateLabTween && this.updateLabTween.stop();
          this.updateLabTween = tween(tweenTargetVec3).to(time, v3(coin, coin, coin), {
            "onUpdate": target => {
              if (winLabel) {
                //winLabel.string = sUtil.AddCommas(Math.floor(target.x));
                winLabel.string = sUtil.AddCommas(Math.trunc(target.x));
              }
            },
            easing: undefined
          }).call(() => {
            winLabel.string = this.AddCommas(Math.trunc(coin)); //this.winLabel.node['rollAnimaValue'] += coin;

            if (cb) cb();
          }).start();
        } // updateLabTween: Tween<any>;
        // CommasLabelAnima(label, value, time) {
        //     if (label) {
        //         const valueOri = label.node['rollAnimaValue'];
        //         const mTween = this.updateLabTween;
        //         if (mTween) {
        //             mTween.stop();
        //         };
        //         if(value>0){
        //             director.off('StopLabelanima');
        //             sUtil.once('StopLabelanima',()=>{
        //                 if(mTween){
        //                     mTween.stop();
        //                     //this.unscheduleAllCallbacks();
        //                     tween(tweenTargetVec3).stop();
        //                 };
        //                 label.string = this.AddCommas(Math.floor(value));
        //             })
        //         };
        //         let tweenTargetVec3 = v3(valueOri, valueOri, valueOri);
        //         // console.log('tweenTargetVec3:'+tweenTargetVec3);
        //         this.updateLabTween = tween(tweenTargetVec3).to(time, v3(value, value, value), {
        //             "onUpdate": (target: Vec3) => {
        //                 if (label) {
        //                     //label.node['rollAnimaValue'] = Math.floor(target.x)
        //                     label.string = this.AddCommas(Math.floor(target.x));
        //                 };
        //             }, easing: 'quadOut'
        //         }).call(() => {
        //             if (label) {
        //                 //console.log('rollAnimaValue',label.node['rollAnimaValue']);
        //                 //label.node['rollAnimaValue'] = Math.floor(value)
        //                 label.string = this.AddCommas(Math.floor(value));
        //             };
        //         }).start();
        //     };
        // };


        CommasLabelAnima(label, value, time) {
          if (label && (label.node['rollAnimaValue'] || label.node['rollAnimaValue'] == 0)) {
            const valueOri = label.node['rollAnimaValue'];
            const mTween = label.node['rollAnimaTween'];

            if (mTween) {
              mTween.stop();
            }

            let tweenTargetVec3 = v3(valueOri, valueOri, valueOri); // console.log('tweenTargetVec3:'+tweenTargetVec3);

            label.node['rollAnimaTween'] = tween(tweenTargetVec3).to(12, v3(value, value, value), {
              "onUpdate": target => {
                if (label) {
                  label.node['rollAnimaValue'] = Math.floor(target.x); //label.string = this.AddCommas(Math.floor(target.x));
                }
              },
              easing: 'quadOut'
            }).call(() => {
              if (label) {
                label.node['rollAnimaValue'] = Math.floor(value);
                label.string = this.AddCommas(Math.floor(value));
              }
            }).start();
          }
        }

        MaxBetBtnClick() {
          if (this.clientConfig && this.betBtnState == 'normal') {
            let bet_types = this.clientConfig.bet_types;

            if (bet_types && bet_types.length > 0) {
              for (let i = 0; i < bet_types.length; i++) {
                const element = bet_types[i];
                console.log('addBtnClick:', JSON.stringify(element));

                if (element.game_mode == 1 && element.game_id == this.gameid) {
                  if (element.bet_amount > this.betAmount) {
                    this.betAmount = element.bet_amount;
                    this.betType = element.bet_type;
                    director.emit('sSlotBetInfoUpdate', {
                      betAmount: this.betAmount,
                      multiple: this.lineAmount
                    });

                    if (this.betLabel) {
                      this.betLabel.node.setScale(1.5, 1.5, 1.5);
                      tween(this.betLabel.node).to(0.01, {
                        scale: Vec3.ONE
                      }, {
                        easing: 'elasticOut'
                      }).start();
                    }

                    this.updateBetInfo();
                    this.saveBetAmountValue(); //break;
                  }
                }
              }
            } //     director.emit('uiTipsOpen', 'text', btnInternationalManager.GetDataByKey('maximunBet'));
            // }

          }
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "btnRenderNode", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "infoBtn", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "ruleNode", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "betDetailLabel", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor5 = _applyDecoratedDescriptor(_class2.prototype, "winStringLabel", [_dec6], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor6 = _applyDecoratedDescriptor(_class2.prototype, "winStringLabel1", [_dec7], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor7 = _applyDecoratedDescriptor(_class2.prototype, "Btns", [_dec8], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return [];
        }
      })), _class2)) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/StarlightPrincess_sSlotHorGameEntityAdapter.ts", ['./_rollupPluginModLoBabelHelpers.js', 'cc', './UIFreeWinBuy.ts', './sUtil.ts', './sSlotHorGameEntityAdapter.ts'], function (exports) {
  'use strict';

  var _applyDecoratedDescriptor, _initializerDefineProperty, _defineProperty, cclegacy, Label, Node, Prefab, _decorator, instantiate, director, v3, tween, UIFreeWinBuy, sUtil, sSlotHorGameEntityAdapter;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _initializerDefineProperty = module.initializerDefineProperty;
      _defineProperty = module.defineProperty;
    }, function (module) {
      cclegacy = module.cclegacy;
      Label = module.Label;
      Node = module.Node;
      Prefab = module.Prefab;
      _decorator = module._decorator;
      instantiate = module.instantiate;
      director = module.director;
      v3 = module.v3;
      tween = module.tween;
    }, function (module) {
      UIFreeWinBuy = module.UIFreeWinBuy;
    }, function (module) {
      sUtil = module.sUtil;
    }, function (module) {
      sSlotHorGameEntityAdapter = module.sSlotHorGameEntityAdapter;
    }],
    execute: function () {
      var _dec, _dec2, _dec3, _dec4, _class, _class2, _descriptor, _descriptor2, _descriptor3, _temp;

      cclegacy._RF.push({}, "414a5EiJXxDRaAml7YxJpEh", "StarlightPrincess_sSlotHorGameEntityAdapter", undefined);

      const {
        ccclass,
        property
      } = _decorator;
      /**
       * Predefined variables
       * Name = Starlight_sSlotHorGameEntityAdapter
       * DateTime = Thu Oct 12 2023 14:54:31 GMT+0800 (中国标准时间)
       * Author = dadadaxigua
       * FileBasename = Starlight_sSlotHorGameEntityAdapter.ts
       * FileBasenameNoExtension = Starlight_sSlotHorGameEntityAdapter
       * URL = db://assets/game/Starlight_sSlotHorGameEntityAdapter.ts
       * ManualUrl = https://docs.cocos.com/creator/3.4/manual/zh/
       *
       */

      let Starlight_sSlotHorGameEntityAdapter = exports('Starlight_sSlotHorGameEntityAdapter', (_dec = ccclass('Starlight_sSlotHorGameEntityAdapter'), _dec2 = property(Label), _dec3 = property(Node), _dec4 = property(Prefab), _dec(_class = (_class2 = (_temp = class Starlight_sSlotHorGameEntityAdapter extends sSlotHorGameEntityAdapter {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "winStringLabel", _descriptor, this);

          _initializerDefineProperty(this, "winStringLabel1", _descriptor2, this);

          _initializerDefineProperty(this, "freebuyNode", _descriptor3, this);

          _defineProperty(this, "callBack", null);

          _defineProperty(this, "updateLabTween", void 0);
        }

        onLoad() {
          super.onLoad(); //this.currentBetAmount=this.betAmount;
          // const freebuy = instantiate(this.node.parent.getChildByPath('UIFreeWinBuy1'));
          // freebuy.parent = this.node.parent;
          // freebuy.active = true;
          // this.selfFreeWinBuyView = freebuy.getComponent(sFreeWinBuyView);
          // this.winLabel = this.node.getChildByPath('betInfo/Node/winLabel').getComponent(Label);
          //this.ruleViewSize = 1200;
        }

        start() {
          super.start(); // Promise.all([new Promise((resolve,reject)=>{
          //     globalThis.getSubGamePrefabByPath('prefabs/subGameUserCoin',(newObj : Node)=>{
          //         if(newObj){
          //             newObj.parent = this.node.parent;
          //             this.ownLabel = newObj.getChildByName('Label').getComponent(Label);
          //             this.shopNode = newObj.getChildByName('shop').getComponent(Button);
          //         }
          //         resolve(1);
          //     });
          // }),new Promise((resolve,reject)=>{
          //     globalThis.getSubGamePrefabByPath('prefabs/subGameSettingDetail',(newObj : Node)=>{
          //         if(newObj){
          //             newObj.parent = this.node.parent.parent;
          //             this.menuBtns = newObj.getComponent(UIOpacity);
          //             this.soundBtn = this.menuBtns.node.getChildByPath('main/items/sound').getComponent(Button);
          //             this.closeBtn = this.menuBtns.node.getChildByPath('close').getComponent(Button);
          //             this.quitBtn = this.menuBtns.node.getChildByPath('main/items/exit').getComponent(Button);
          //             newObj.active = false;
          //         }
          //         resolve(2);
          //     });
          // }),new Promise((resolve,reject)=>{
          //     const lua = globalThis.GetLanguageType();
          //     if(lua){
          //         sUtil.getLobbyAssetInSubGame('config/'+lua,JsonAsset,(err,asset)=>{
          //             if(asset){
          //                 btnInternationalManager.SetLanguegeData(asset.name, asset.json);
          //             }
          //             resolve(5);
          //         });
          //     }else{
          //         resolve(5);
          //     }
          // })]).then((res)=>{
          //     super.super.start();
          //     if(this.shopNode){
          //         this.shopNode.node.on('click',()=>{
          //             globalThis.openStore && globalThis.openStore();
          //         },this);
          //     }
          // });
          // [3]
          // director.off('rollStop',null,sGameEntity);
          // director.off('rollStopEndTrigger',null,sGameEntity);
          //director.targetOff()
          ///this.freeWinBuy=this.node.parent.getChildByPath('UIFreeWinBuy1');
          // director.off('betUserInfoUpdateWinAnima');
          // director.on('betUserInfoUpdateWinAnima', (rate) => {
          //     this.currentBetAmount=this.betAmount;
          //     this.betUserInfoUpdateAnima(null, null, rate * this.currentBetAmount, 0.2);
          // }, this);

          this.freeWinBuy = instantiate(this.freebuyNode); //this.ruleNode=this.node.parent.getChildByName('UIIntro');
          //this.loadFreeBuyView();

          const freebuy = instantiate(this.freebuyNode);
          freebuy.parent = this.node.parent;
          freebuy.active = true;
          this.selfFreeWinBuyView = freebuy.getComponent(UIFreeWinBuy); // director.off('subGameFreeWinBuyBtnClick');
          // director.on('subGameFreeWinBuyBtnClick', () => {
          //     if (this.betBtnState == 'normal' && this.betClickMode == 'normal') {
          //         if (this.clientConfig) {
          //             let bet_types = this.clientConfig.bet_types;
          //             if (bet_types && bet_types.length > 0) {
          //                 for (let i = 0; i < bet_types.length; i++) {
          //                     const element = bet_types[i];
          //                     if (element.game_id == this.gameid) {
          //                         if (element.bet_amount == this.betAmount && element.game_mode == 2) {
          //                             let mUserInfo = this.getUserInfo();
          //                             if (mUserInfo) {
          //                                 let tipStr = '';
          //                                 let canBuy = true;
          //                                 let maxBet = mUserInfo.max_bet;
          //                                 let userCoin = mUserInfo.coin;
          //                                 let sumAmount = element.bet_amount * element.bet_multiple;
          //                                 let sum = element.bet_amount;
          //                                 if (sumAmount > userCoin) {
          //                                     console.log('no userCoin');
          //                                     canBuy = false;
          //                                     tipStr = globalThis.GetDataByKey && globalThis.GetDataByKey("mulTiLN", "common", '5001');
          //                                 } else if (sum > maxBet) {
          //                                     console.log('no maxBet');
          //                                     canBuy = false;
          //                                     tipStr = btnInternationalManager.GetDataByKey('maxBetNotEnough');
          //                                 }
          //                                 this.freeBuyBetAmount1 = element.bet_amount;
          //                                 this.freeBuyLineMultiple1 = element.bet_multiple;
          //                                 this.currentBetAmount=element.bet_amount;
          //                                 console.log('subGameFreeWinBuyBtnClick');
          //                                 if(this.selfFreeWinBuyView){
          //                                     this.selfFreeWinBuyView.viewInit(element.bet_amount * element.bet_multiple, element.bet_type, canBuy, tipStr);
          //                                 }else if(this.freeWinBuy){
          //                                     this.freeWinBuy=this.node.parent.getChildByPath('UIFreeWinBuy');
          //                                     const freeWinBuy = this.freeWinBuy.getComponent(UIFreeWinBuy);
          //                                     if(freeWinBuy){
          //                                        // this.currentBetAmount=this.freeBuyBetAmount;
          //                                         freeWinBuy.viewInit(element.bet_amount * element.bet_multiple, element.bet_type, canBuy, tipStr);
          //                                     };
          //                                 };
          //                             };
          //                             break;
          //                         };
          //                     };
          //                 };
          //             };
          //         };
          //     };
          // }, this); 
        } // rulesBtnClick(){
        //     this.ruleNode.active = true;
        // };


        rulesBtnClick() {
          //this.ruleNode.scale=v3(1.28,1.28,0.78125);
          // let pNodeimg=this.ruleNode.getChildByPath('main/ScrollView/view/content/img')
          // let contNode=this.ruleNode.getChildByPath('main/ScrollView/view/content');
          // let pviewNod=this.ruleNode.getChildByPath('main/ScrollView/view');
          // let pcloseNode=this.ruleNode.getChildByPath('main/close');
          // let pbgNode=this.ruleNode.getChildByPath('main/bg');
          // let ScrollViewNode=this.ruleNode.getChildByPath('main/ScrollView');
          // pcloseNode.addComponent(Widget);
          // pcloseNode.getComponent(Widget).isAlignRight=true;
          // pcloseNode.getComponent(Widget).isAlignTop=true;
          // pcloseNode.getComponent(Widget)!.isAbsoluteRight= false;
          // pcloseNode.getComponent(Widget)!.isAbsoluteTop = false;
          // pcloseNode.getComponent(Widget)!.right=0.11;
          // pcloseNode.getComponent(Widget)!.top=0.1;
          // this.setNodeWidget(pbgNode,0.11,0.11,0.08,0.08);
          // //this.setNodeWidget(ScrollViewNode,0.11,0.11,0.2,0.1);
          // pviewNod.active=false;
          // pviewNod.active=true;
          // let pviewHeight=pviewNod.getComponent(UITransform).contentSize.x;
          // contNode.getComponent(Layout).affectedByScale=true;
          // console.log(pbgNode);
          // pNodeimg.scale=v3((pviewHeight/1280),(pviewHeight/1280),0.78125);
          this.ruleNode.active = true;
        }

        betUserInfoUpdate(own, bet, win) {
          if (this.ownLabel) {
            if (own || own == 0) {
              this.ownLabel.node['rollAnimaValue'] = own;
              this.ownLabel.string = this.AddCommas(Math.trunc(own));
            }
          }

          if (this.betLabel) {
            if (bet || bet == 0) {
              this.betLabel.node['rollAnimaValue'] = bet;
              this.betLabel.string = this.AddCommas(Math.trunc(bet));
            }
          }

          if (win == null) {
            this.winLabel.node['rollAnimaValue'] = 0;
            console.log('重置winLabel1');
            this.winLabel.node.active = false;
            this.winStringLabel.node.active = true;
            this.winStringLabel1.active = false;
            this.winLabel.string = 0 + '';
          } else {
            if (win && win > 0) {
              //console.log('win1:',win);
              this.winLabel.node['rollAnimaValue'] = win;
              console.log('重置winLabel2');
              this.winLabel.string = this.AddCommas(Math.trunc(win));
              this.winStringLabel1.active = true;
            } else {
              this.winLabel.node['rollAnimaValue'] = 0;
              console.log('重置winLabel-1'); //console.log('win2:',win);
              // this.winLabel.string=0+'';
            }
          }
        }

        betUserInfoUpdateAnima(own, bet, win, animaTime = 1, callBack = null) {
          if (own || own == 0) {
            this.CommasLabelAnima(this.ownLabel, Math.trunc(own), animaTime);
          }

          if (bet || bet == 0) {
            this.CommasLabelAnima(this.betLabel, Math.trunc(bet), animaTime);
          }

          if (own == null) {
            if (win == null) {
              this.winLabel.node['rollAnimaValue'] = 0;
              console.log('重置winLabel3');
              this.winLabel.string = 0 + '';
            } else {
              if (win && win > 0) {
                this.winLabel.node.active = true;
                this.winStringLabel.node.active = false;
                this.winStringLabel1.active = true; //console.log('win3:',win);

                let time = 0.6;

                if (win >= this.currentBetAmount * 200) {
                  time = 12;
                }

                if (!this.winLabel.node['rollAnimaValue']) {
                  console.log('重置winLabel4');
                  this.winLabel.node['rollAnimaValue'] = 0;
                }

                let num = parseInt(this.winLabel.string);
                console.log('win:', win);
                console.log('num:', num);
                console.log('this.winLabel.string:', this.winLabel.string);
                console.log('rollAnimaValue', this.winLabel.node['rollAnimaValue']); //sUtil.TweenLabel(this.winLabel.node['rollAnimaValue'],Math.trunc(win+this.winLabel.node['rollAnimaValue']),this.winLabel,time);

                this.updateWinLab(this.winLabel.node['rollAnimaValue'], this.winLabel, Math.trunc(win + this.winLabel.node['rollAnimaValue']), time, this.callBack); //this.winLabel.node['rollAnimaValue'] += win;

                this.winLabel.node['rollAnimaValue'] += Math.trunc(win); //this.CommasLabelAnima(this.winLabel, Math.trunc(bet), animaTime);

                director.off('StopLabelanima');
                sUtil.once('StopLabelanima', () => {
                  //this.winLabel.node['rollAnimaValue'] += win;
                  this.updateLabTween.stop();
                  this.winLabel.string = this.AddCommas(Math.floor(this.winLabel.node['rollAnimaValue']));
                });
              } else {
                if (!this.winLabel.node['rollAnimaValue']) {
                  console.log('重置winLabel5');
                  this.winLabel.node['rollAnimaValue'] = 0;
                  this.winLabel.string = 0 + '';
                } else {
                  this.winLabel.node['rollAnimaValue'] = 0;
                  console.log('重置winLabel6');
                } // this.winLabel.node.active=false;
                // this.winStringLabel.node.active=true;
                // this.winStringLabel1.node.active=false

              }
            }
          }

          if (callBack != null) {
            this.scheduleOnce(callBack, animaTime);
          }
        }

        updateWinLab(lastCoin, winLabel, coin, time, cb) {
          let tweenTargetVec3 = v3(lastCoin, lastCoin, lastCoin);
          let demc = -1;

          if (globalThis.getCoinRate && globalThis.getCoinRate() > 1) {
            demc = 2;
          }

          this.updateLabTween && this.updateLabTween.stop();
          this.updateLabTween = tween(tweenTargetVec3).to(time, v3(coin, coin, coin), {
            "onUpdate": target => {
              if (winLabel) {
                //winLabel.string = sUtil.AddCommas(Math.floor(target.x));
                winLabel.string = sUtil.AddCommas(target.x, demc).toString(); //winLabel.string = sUtil.AddCommas(Math.trunc(target.x));
              }
            },
            easing: undefined
          }).call(() => {
            if (winLabel) {
              winLabel.string = sUtil.AddCommas(coin, demc).toString();
            }

            if (cb) cb();
          }).start();
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "winStringLabel", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "winStringLabel1", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "freebuyNode", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class)); //     tween(tweenTargetVec3).to(_time, v3(value, value, value), {
      //         "onUpdate": (target: Vec3) => {
      //             if (targetLabel) {
      //                 targetLabel.string = isInt ? Math.trunc(target.x).toString() : (needKWNum > 0 ? sUtil.changeNumberToKW((target.x),needKWNum) : (sUtil.AddCommas(target.x,demc)).toString());
      //             }
      //         }, easing: ease
      //     }).call(() => {
      //         if (targetLabel) {
      //             targetLabel.string = isInt ? Math.trunc(value).toString() : (needKWNum > 0 ? sUtil.changeNumberToKW((value),needKWNum) : (sUtil.AddCommas(value,demc)).toString());
      //         }
      //         if(finishCall){
      //             finishCall();
      //         }
      //     }).start();
      // }

      /**
       * [1] Class member could be defined like this.
       * [2] Use `property` decorator if your want the member to be serializable.
       * [3] Your initialization goes here.
       * [4] Your update function goes here.
       *
       * Learn more about scripting: https://docs.cocos.com/creator/3.4/manual/zh/scripting/
       * Learn more about CCClass: https://docs.cocos.com/creator/3.4/manual/zh/scripting/decorator.html
       * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.4/manual/zh/scripting/life-cycle-callbacks.html
       */

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/StarlightPrincess_OverEffect.ts", ['./_rollupPluginModLoBabelHelpers.js', 'cc', './sUtil.ts', './sBoxItemBase.ts'], function (exports) {
  'use strict';

  var _applyDecoratedDescriptor, _initializerDefineProperty, cclegacy, Sprite, _decorator, color, Color, sUtil, sBoxItemBase;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _initializerDefineProperty = module.initializerDefineProperty;
    }, function (module) {
      cclegacy = module.cclegacy;
      Sprite = module.Sprite;
      _decorator = module._decorator;
      color = module.color;
      Color = module.Color;
    }, function (module) {
      sUtil = module.sUtil;
    }, function (module) {
      sBoxItemBase = module.sBoxItemBase;
    }],
    execute: function () {
      var _dec, _dec2, _dec3, _class, _class2, _descriptor, _descriptor2, _temp;

      cclegacy._RF.push({}, "5592cDESr1CDIHOQ0yzsZid", "StarlightPrincess_OverEffect", undefined);

      const {
        ccclass,
        property
      } = _decorator;
      let StarlightPrincess_OverEffect = exports('StarlightPrincess_OverEffect', (_dec = ccclass('StarlightPrincess_OverEffect'), _dec2 = property(Sprite), _dec3 = property(Sprite), _dec(_class = (_class2 = (_temp = class StarlightPrincess_OverEffect extends sBoxItemBase {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "lightNode", _descriptor, this);

          _initializerDefineProperty(this, "boomNode", _descriptor2, this);
        }

        start() {
          super.start();
        }

        boxItemUpdate(renderData, symbolValue, boxState, tableState) {
          if (boxState == 'win') {
            this.lightNode.color = color(255, 255, 255, 0);
            this.lightNode.node.active = true;
            sUtil.TweenColor(Color.WHITE, this.lightNode, 0.2, 0);
            this.pushOneSchedule(() => {
              sUtil.TweenColor(color(255, 255, 255, 0), this.lightNode, 0.2, 0);
            }, 1);
            this.pushOneSchedule(() => {
              this.lightNode.node.active = false;
              this.boomNode.node.active = true;
            }, 1.2);
          }
        }

        clearItem(symbolValue, rollState, tableState) {
          this.removeAllSchedule();
          this.lightNode.node.active = false;
          this.boomNode.node.active = false;
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "lightNode", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "boomNode", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/StarlightPrincess_Board.ts", ['./_rollupPluginModLoBabelHelpers.js', 'cc', './sComponent.ts', './sUtil.ts', './sAudioMgr.ts', './sObjPool.ts', './sAssetMgr.ts'], function (exports) {
  'use strict';

  var _applyDecoratedDescriptor, _initializerDefineProperty, _defineProperty, cclegacy, UIOpacity, Label, Node, SpriteFrame, _decorator, director, tween, v3, Sprite, Color, UITransform, Vec3, sComponent, sUtil, sAudioMgr, sObjPool, assetMgr;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _initializerDefineProperty = module.initializerDefineProperty;
      _defineProperty = module.defineProperty;
    }, function (module) {
      cclegacy = module.cclegacy;
      UIOpacity = module.UIOpacity;
      Label = module.Label;
      Node = module.Node;
      SpriteFrame = module.SpriteFrame;
      _decorator = module._decorator;
      director = module.director;
      tween = module.tween;
      v3 = module.v3;
      Sprite = module.Sprite;
      Color = module.Color;
      UITransform = module.UITransform;
      Vec3 = module.Vec3;
    }, function (module) {
      sComponent = module.sComponent;
    }, function (module) {
      sUtil = module.sUtil;
    }, function (module) {
      sAudioMgr = module.default;
    }, function (module) {
      sObjPool = module.sObjPool;
    }, function (module) {
      assetMgr = module.assetMgr;
    }],
    execute: function () {
      var _dec, _dec2, _dec3, _dec4, _dec5, _dec6, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _descriptor5, _temp;

      cclegacy._RF.push({}, "5d16emtYXZMXbgD011qoznr", "StarlightPrincess_Board", undefined);

      const {
        ccclass,
        property
      } = _decorator;
      let StarlightPrincess_Board = exports('StarlightPrincess_Board', (_dec = ccclass('StarlightPrincess_Board'), _dec2 = property(UIOpacity), _dec3 = property(UIOpacity), _dec4 = property(Label), _dec5 = property(Node), _dec6 = property([SpriteFrame]), _dec(_class = (_class2 = (_temp = class StarlightPrincess_Board extends sComponent {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "normalInfoNode", _descriptor, this);

          _initializerDefineProperty(this, "tableOpa", _descriptor2, this);

          _initializerDefineProperty(this, "baseLabel", _descriptor3, this);

          _initializerDefineProperty(this, "multiNode", _descriptor4, this);

          _initializerDefineProperty(this, "boradSprites", _descriptor5, this);

          _defineProperty(this, "index", 0);

          _defineProperty(this, "mode", 'idle');

          _defineProperty(this, "targetImg", null);

          _defineProperty(this, "totalRate", 0);

          _defineProperty(this, "coinLabelValue", 0);

          _defineProperty(this, "oneRoundMultiValue", 0);
        }

        start() {
          this.BoardAction(); // this.scheduleOnce(()=>{
          //     director.emit('winBetRes',1000);
          //     // this.scheduleOnce(()=>{
          //     //     // director.emit('winBetRes',60);
          //     //     this.multiLabelAnima(v3(0,0,0),10);
          //     // },2);
          // },2);

          director.on('winMultiBox', data => {
            if (data) {
              this.multiLabelAnima(data.actionType, data.pos, data.multi, data.fontSize);
            }
          }, this);
          director.on('winBetRes', rate => {
            if (rate > 0) {
              // console.log('winBetRes:',rate);
              this.totalRate += rate;
              this.showWinCoin(this.totalRate * globalThis.GameBtnEntity.CurrentBetAmount); // this.showWinCoin(this.totalRate * 10);
            } else if (rate == -1) {
              director.emit('titleWinSettleEnd');
            } else ;
          }, this);
          director.on('slotSubGameRollBegin', () => {
            this.totalRate = 0;
            this.coinLabelValue = 0;
            this.oneRoundMultiValue = 0;

            if (this.mode != 'idle') {
              this.cleanTweenDic();
              this.removeIDTween('imgSprite');
              this.removeIDSchedule('showBoard');
              this.BoardAction();
              this.removeIDTween('table');
              this.pushIDTween(tween(this.tableOpa).to(0.3, {
                opacity: 0
              }).call(() => {
                this.tableOpa.node.active = false;
              }).start(), 'table');
            }
          }, this);
        }

        BoardAction() {
          if (this.boradSprites && this.boradSprites.length > 0) {
            this.mode = 'idle';
            this.normalInfoNode.node.active = true;
            this.removeIDTween('normalInfoNode');
            this.pushIDTween(tween(this.normalInfoNode).to(0.3, {
              opacity: 255
            }).start(), 'normalInfoNode');

            if (this.index >= this.boradSprites.length) {
              this.index = 0;
            }

            this.showBoard(this.boradSprites[this.index]);
            this.index++;
          }
        }

        showWinCoin(coinNum) {
          this.removeIDSchedule('showBoard');

          if (coinNum > 0) {
            this.mode = 'win'; // if(this.targetImg){
            //     const imgTemp = this.targetImg;
            //     this.pushIDTween(sUtil.TweenColor(Color.TRANSPARENT,imgTemp,0.3,0,()=>{
            //         sObjPool.Enqueue(imgTemp.node.name,imgTemp.node);
            //     }),'imgSprite2');
            // }

            this.removeIDTween('normalInfoNode');
            this.pushIDTween(tween(this.normalInfoNode).to(0.3, {
              opacity: 0
            }).start(), 'normalInfoNode');

            if (!this.tableOpa.node.active) {
              this.removeIDTween('table');
              this.tableOpa.opacity = 0;
              this.tableOpa.node.active = true;
              this.pushIDTween(tween(this.tableOpa).to(0.3, {
                opacity: 255
              }).start(), 'table');
            }

            const lastCoin = this.coinLabelValue;
            this.coinLabelValue = coinNum;
            this.baseLabel.node.position = v3(0, -3, 0);
            this.multiNode.active = false;
            sUtil.TweenLabel(lastCoin, this.coinLabelValue, this.baseLabel, 1);
          }
        }

        showBoard(spriteFrame) {
          if (this.targetImg) {
            const tempImg = this.targetImg;
            sObjPool.Enqueue('bannerImg', tempImg.node);
            tempImg.node.active = false;
            this.targetImg = null;
          }

          if (this.normalInfoNode && spriteFrame) {
            // console.log('bannerImgcoun : ',sObjPool.Count('bannerImg'));
            const img = sObjPool.Dequeue('bannerImg');

            if (img && img.isValid) {
              img.parent = this.normalInfoNode.node;
              img.position = v3(0, 0, 0);
              img.active = true;
              const imgSprite = img.getComponent(Sprite);

              if (imgSprite) {
                this.tableOpa.node.active = false;
                this.targetImg = imgSprite;
                imgSprite.spriteFrame = assetMgr.GetAssetByName(spriteFrame.name + '_int');
                imgSprite.color = Color.TRANSPARENT;
                this.pushIDTween(sUtil.TweenColor(Color.WHITE, imgSprite, 0.3, 0), 'imgSprite');
                this.pushIDSchedule(() => {
                  this.BoardAction();
                }, 5, 'showBoard');
              }
            }
          }
        }

        multiLabelAnima(actionType, pos, multiValue, fontSize) {
          //console.log('倍数飞行');
          if (pos && multiValue) {
            console.log('倍数符号：', this.oneRoundMultiValue, multiValue);
            this.oneRoundMultiValue += multiValue;
            const baseLabelWidth = this.baseLabel.getComponent(UITransform).contentSize;
            const multi = this.multiNode.getChildByName('multi');
            const notation = this.multiNode.getChildByName('notation');
            const multiTran = multi.getComponent(UITransform);
            const multiScript = multi.getComponent(Label);
            const multiLabel = sObjPool.Dequeue('boardMultiLabel');

            if (multiLabel && multiLabel.isValid) {
              multiLabel.scale = Vec3.ONE;
              multiLabel.parent = this.node;
              multiLabel.worldPosition = pos;
              this.SetNodeFront(multiLabel);
              multiLabel.active = true;
              let endPos = v3(multi.worldPosition.x + multiTran.contentSize.x / 2, multi.worldPosition.y, 0);
              const multiLabelScript = multiLabel.getComponent(Label);
              multiLabelScript.fontSize = fontSize;
              multiLabelScript.string = multiValue + 'x'; // this.removeIDTween('multiLabel');

              sAudioMgr.PlayShotAudio('mult_fly', 1);
              this.pushIDTween(tween(this.baseLabel.node).delay(0.8).to(0.2, {
                position: v3(-35, -3, 0)
              }, {
                easing: 'cubicOut'
              }).call(() => {}).start(), 'multiLabelAnima');
              this.pushIDTween(tween(multiLabel).sequence(tween(multiLabel).to(0.53, {
                scale: v3(2, 2, 2)
              }, {
                easing: 'cubicOut'
              }), tween(multiLabel).parallel(tween(multiLabel).to(0.5, {
                scale: v3(0.8, 0.8, 1)
              }, {
                easing: 'linear'
              }), tween(multiLabel).call(() => {}).to(0.5, {
                worldPosition: endPos
              }, {
                easing: 'sineIn'
              }))).call(() => {
                if (this.mode != 'multi') {
                  this.mode = 'multi';
                  const baseLabelWidth = this.baseLabel.getComponent(UITransform).contentSize;
                  this.multiNode.position = v3(baseLabelWidth.x / 2 + 20, 5, 0);
                  this.removeIDTween('multiLabelAnima'); //sAudioMgr.PlayShotAudio('total multiplier_result_petvoice',1);

                  this.multiNode.active = true;
                  notation.active = true;
                }

                multiScript.string = this.oneRoundMultiValue.toString(); //},0.4);

                console.log('倍数符号：', multiScript.string);
                tween(multi).to(0.15, {
                  scale: v3(1.6, 1.6, 1.6)
                }, {
                  easing: 'sineOut'
                }).to(0.1, {
                  scale: Vec3.ONE
                }).start();
                sObjPool.Enqueue('boardMultiLabel', multiLabel);
                multiLabel.active = false;

                if (actionType == 'close') {
                  tween(this.multiNode).delay(0.3).call(() => {
                    notation.active = false;
                  }).to(0.3, {
                    position: Vec3.ZERO
                  }, {
                    easing: 'cubicIn'
                  }).call(() => {
                    this.multiNode.active = false;
                    sAudioMgr.PlayShotAudio('total multiplier_result_petvoice');
                  }).start();
                  tween(this.baseLabel.node).delay(0.3).call(() => {}).to(0.3, {
                    position: v3(0, -3, 0)
                  }, {
                    easing: 'cubicIn'
                  }).call(() => {
                    this.baseLabel.string = this.AddCommas(this.coinLabelValue * this.oneRoundMultiValue);
                    tween(this.baseLabel.node).to(0.15, {
                      scale: v3(1.3, 1.3, 1.3)
                    }, {
                      easing: 'sineOut'
                    }).to(0.1, {
                      scale: Vec3.ONE
                    }).call(() => {
                      sAudioMgr.PlayShotAudio('total multiplier_result', 1);
                      director.emit('StarlightPrincessLabelEnd');
                    }).start();
                  }).start();
                }
              }).start(), 'multiLabel');
            }
          }
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "normalInfoNode", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "tableOpa", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "baseLabel", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "multiNode", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor5 = _applyDecoratedDescriptor(_class2.prototype, "boradSprites", [_dec6], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return [];
        }
      })), _class2)) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/StarlightPrincess_BigWin.ts", ['./_rollupPluginModLoBabelHelpers.js', 'cc', './sComponent.ts', './sUtil.ts', './sAudioMgr.ts'], function (exports) {
  'use strict';

  var _applyDecoratedDescriptor, _initializerDefineProperty, _defineProperty, cclegacy, Node, sp, UIOpacity, Button, _decorator, director, ParticleSystem, Label, tween, v3, sComponent, sUtil, sAudioMgr;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _initializerDefineProperty = module.initializerDefineProperty;
      _defineProperty = module.defineProperty;
    }, function (module) {
      cclegacy = module.cclegacy;
      Node = module.Node;
      sp = module.sp;
      UIOpacity = module.UIOpacity;
      Button = module.Button;
      _decorator = module._decorator;
      director = module.director;
      ParticleSystem = module.ParticleSystem;
      Label = module.Label;
      tween = module.tween;
      v3 = module.v3;
    }, function (module) {
      sComponent = module.sComponent;
    }, function (module) {
      sUtil = module.sUtil;
    }, function (module) {
      sAudioMgr = module.default;
    }],
    execute: function () {
      var _dec, _dec2, _dec3, _dec4, _dec5, _dec6, _dec7, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _descriptor5, _descriptor6, _temp;

      cclegacy._RF.push({}, "5ff1erVvRhEGadjpmXApWD3", "StarlightPrincess_BigWin", undefined);

      const {
        ccclass,
        property
      } = _decorator;
      let StarlightPrincess_BigWin = exports('StarlightPrincess_BigWin', (_dec = ccclass('StarlightPrincess_BigWin'), _dec2 = property(Node), _dec3 = property(Node), _dec4 = property(sp.Skeleton), _dec5 = property(UIOpacity), _dec6 = property(UIOpacity), _dec7 = property(Button), _dec(_class = (_class2 = (_temp = class StarlightPrincess_BigWin extends sComponent {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "mainNode", _descriptor, this);

          _initializerDefineProperty(this, "CoinNode", _descriptor2, this);

          _initializerDefineProperty(this, "targetSpine", _descriptor3, this);

          _initializerDefineProperty(this, "borderNode", _descriptor4, this);

          _initializerDefineProperty(this, "maskOpa", _descriptor5, this);

          _initializerDefineProperty(this, "closeBtn", _descriptor6, this);

          _defineProperty(this, "touchEnable", false);

          _defineProperty(this, "targetCoinValue", 0);

          _defineProperty(this, "animaType", 'nice');

          _defineProperty(this, "bgmp", 'bgm_mg');
        }

        start() {
          director.on('freeWinBegain', () => {
            this.bgmp = 'bgm_fs';
          }, this);
          director.on('freeWinOver', () => {
            this.bgmp = 'bgm_mg';
          }, this);
          director.on('StarlightPrincessBigWin', rate => {
            let spineAnimanName = '_nice_';
            sAudioMgr.StopBGAudio();

            if (rate >= 200 && rate < 400) {
              sAudioMgr.PlayBG('bgm_nicewin');
              spineAnimanName = '_nice_';
              this.animaType = 'nice';
            } else if (rate >= 400 && rate < 600) {
              sAudioMgr.PlayBG('bgm_megawin');
              spineAnimanName = '_mega_';
              this.animaType = 'mega';
            } else if (rate >= 600 && rate < 1000) {
              sAudioMgr.PlayBG('bgm_superwin');
              sAudioMgr.PlayShotAudio('bgm_superwin_voice');
              spineAnimanName = '_superb_';
              this.animaType = 'super';
            } else if (rate >= 1000) {
              sAudioMgr.PlayBG('bgm_sensationalwin');
              sAudioMgr.PlayShotAudio('bgm_sensationalwin_voice');
              spineAnimanName = '_sensational_';
              this.animaType = 'sensational';
            } else {
              director.emit('StarlightPrincessBigWinEnd');
            }

            this.viewOpen(spineAnimanName, rate * globalThis.GameBtnEntity.CurrentBetAmount); //this.viewOpen(spineAnimanName,rate* 20);
          }, this); // this.scheduleOnce(()=>{
          //    director.emit('StarlightPrincessBigWin',200);
          // },2);

          this.closeBtn.node.on('click', () => {
            if (this.touchEnable) {
              this.touchEnable = false;
              this.cleanTweenList();

              for (let i = 0; i < this.CoinNode.children.length; i++) {
                this.CoinNode.children[i].getComponent(ParticleSystem).loop = false;
              }

              this.removeIDSchedule('coinLable');
              const coinLabelNode = this.borderNode.node.getChildByName('label');

              if (coinLabelNode) {
                sAudioMgr.StopBGAudio();

                if (this.animaType == 'nice') {
                  sAudioMgr.PlayBG('bgm_nicewin_end', false);
                } else if (this.animaType == 'mega') {
                  sAudioMgr.PlayBG('bgm_megawin_end', false);
                } else if (this.animaType == 'super') {
                  sAudioMgr.PlayBG('bgm_superwin_end', false);
                } else if (this.animaType == 'sensational') {
                  sAudioMgr.PlayBG('bgm_sensationalwin_end', false);
                }

                const coinLable = coinLabelNode.getComponent(Label);
                coinLable.string = this.AddCommas(this.targetCoinValue); //this.pushOneTween(tween(coinLable.node).to(0.3,{scale : v3(1.3,1.3,1.3)},{easing : 'sineOut'}).to(0.3,{scale : Vec3.ONE},{easing : 'sineIn'}).union().repeatForever().start());
                //this.pushOneTween(tween(this.borderNode.node).to(0.6,{scale : v3(1.2,1.2,1.2)},{easing : 'sineOut'}).to(0.6,{scale : Vec3.ONE},{easing : 'sineIn'}).union().repeatForever().start());
              }

              this.scheduleOnce(() => {
                director.emit('StopLabelanima');
                this.viewClose();
              }, 3);
            }
          }, this);
        }

        viewOpen(spineAnima, coin) {
          this.targetCoinValue = coin;

          if (this.targetSpine && this.borderNode) {
            this.SetNodeFront(this.node);
            this.removeIDSchedule('touch');
            this.touchEnable = false;
            this.cleanTweenList();
            this.SetUIFront();
            this.mainNode.active = true;
            this.targetSpine.node.active = true;
            this.targetSpine.setAnimation(0, 'stpr_bigwin' + spineAnima + 'in', false);

            for (let i = 0; i < this.CoinNode.children.length; i++) {
              this.CoinNode.children[i].getComponent(ParticleSystem).loop = true;
            }

            this.maskOpa.opacity = 0;
            this.pushOneTween(tween(this.maskOpa).to(0.3, {
              opacity: 255
            }).start());
            let IsCom = 0;
            IsCom += 1;
            const coinLabelNode = this.borderNode.node.getChildByName('label');

            if (coinLabelNode && IsCom == 1) {
              const coinLable = coinLabelNode.getComponent(Label);
              let bigWinTime = 12;

              if (this.animaType == 'nice') {
                this.CoinNode.active = true;
                this.CoinNode.children[0].active = false;
                this.CoinNode.children[1].active = false;
                this.CoinNode.children[2].active = true;
              } else if (this.animaType == 'mega') {
                bigWinTime = 12;
                this.CoinNode.active = true;
                this.CoinNode.children[0].active = false;
                this.CoinNode.children[1].active = false;
                this.CoinNode.children[2].active = true;
              } else if (this.animaType == 'super') {
                bigWinTime = 12;
                this.CoinNode.active = true;
                this.CoinNode.children[0].active = true;
                this.CoinNode.children[1].active = true;
                this.CoinNode.children[2].active = true;
              } else if (this.animaType == 'sensational') {
                bigWinTime = 12;
                this.CoinNode.active = true;
                this.CoinNode.children[0].active = true;
                this.CoinNode.children[1].active = true;
                this.CoinNode.children[2].active = true;
              }

              coinLable.string = '0';
              this.scheduleOnce(() => {
                this.pushOneTween(sUtil.TweenLabel(0, coin, coinLable, bigWinTime, 0, 'quadOut', 0, () => {
                  sAudioMgr.StopBGAudio();

                  if (this.animaType == 'nice') {
                    sAudioMgr.PlayBG('bgm_nicewin_end', false);
                  } else if (this.animaType == 'mega') {
                    sAudioMgr.PlayBG('bgm_megawin_end', false);
                  } else if (this.animaType == 'super') {
                    sAudioMgr.PlayBG('bgm_superwin_end', false);
                  } else if (this.animaType == 'sensational') {
                    sAudioMgr.PlayBG('bgm_sensationalwin_end', false);
                  }

                  for (let i = 0; i < this.CoinNode.children.length; i++) {
                    this.CoinNode.children[i].getComponent(ParticleSystem).loop = false; //this.CoinNode.children[i].getComponent(ParticleSystem).stop();
                  }

                  this.pushIDSchedule(() => {
                    this.viewClose();
                  }, 1, 'coinLable');
                }));
              }, 0.7);
            }

            this.targetSpine.setCompleteListener(() => {
              this.targetSpine.setAnimation(0, 'stpr_bigwin' + spineAnima + 'loop', true);
            });
            this.borderNode.opacity = 0;
            this.borderNode.node.position = v3(0, -120, 0);
            this.borderNode.node.scale = v3(1.4, 1.4, 1.4);
            this.pushOneTween(tween(this.borderNode).delay(0.6).to(0.2, {
              opacity: 255
            }).start());
            this.pushOneTween(tween(this.borderNode.node).delay(0.6).to(0.2, {
              position: v3(0, -240, 0)
            }).call(() => {// this.pushOneTween(tween(this.borderNode.node).to(0.6,{scale : v3(1.2,1.2,1.2)},{easing : 'sineOut'}).to(0.6,{scale : Vec3.ONE},{easing : 'sineIn'}).union().repeatForever().start());
            }).start());
            this.pushIDSchedule(() => {
              this.touchEnable = true;
            }, 0.5, 'touch');
          }
        }

        viewClose() {
          this.CoinNode.active = false;

          if (this.targetSpine && this.borderNode) {
            const animaName = this.targetSpine.animation;

            if (animaName.length > 1) {
              //const rName = animaName.slice(0,2);
              //if(rName){
              if (this.animaType == 'nice') {
                this.targetSpine.setAnimation(0, 'stpr_bigwin_nice_out', false);
              } else if (this.animaType == 'mega') {
                this.targetSpine.setAnimation(0, 'stpr_bigwin_mega_out', false);
              } else if (this.animaType == 'super') {
                this.targetSpine.setAnimation(0, 'stpr_bigwin_superb_out', false);
              } else if (this.animaType == 'sensational') {
                this.targetSpine.setAnimation(0, 'stpr_bigwin_sensational_out', false);
              }

              this.targetSpine.setCompleteListener(() => {
                this.scheduleOnce(() => {
                  for (let i = 0; i < this.CoinNode.children.length; i++) {
                    //this.CoinNode.children[i].getComponent(ParticleSystem).loop=false;
                    this.CoinNode.children[i].getComponent(ParticleSystem).clear();
                    this.CoinNode.children[i].getComponent(ParticleSystem).stop();
                  }

                  sAudioMgr.StopAudio();
                  sAudioMgr.StopBGAudio();
                  sAudioMgr.PlayBG(this.bgmp);
                  this.scheduleOnce(() => {
                    this.mainNode.active = false;
                    director.emit('StarlightPrincessBigWinEnd');
                  }, 0.5);
                }, 0.5);
              }); //};
            }

            this.cleanTweenList();
            this.removeIDSchedule('coinLable');
            this.pushOneTween(tween(this.maskOpa).to(0.3, {
              opacity: 0
            }).start());
            this.pushOneTween(tween(this.borderNode).to(0.3, {
              opacity: 0
            }).call(() => {
              this.borderNode.node.getChildByName('label').getComponent(Label).string = '0';
            }).start()); // this.pushOneTween(tween(this.borderNode.node).to(0.2,{position : v3(0,-120,0),scale : v3(1.6,1.6,1.6)}).call(()=>{
            // }).start());
            //},0.5);
          }
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "mainNode", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "CoinNode", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "targetSpine", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "borderNode", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor5 = _applyDecoratedDescriptor(_class2.prototype, "maskOpa", [_dec6], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor6 = _applyDecoratedDescriptor(_class2.prototype, "closeBtn", [_dec7], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/StarlightPrincess_UIFreewinBingoFree.ts", ['./_rollupPluginModLoBabelHelpers.js', 'cc', './sComponent.ts', './sAudioMgr.ts'], function (exports) {
  'use strict';

  var _applyDecoratedDescriptor, _initializerDefineProperty, _defineProperty, cclegacy, Node, Label, sp, _decorator, UIOpacity, tween, director, v3, sComponent, sAudioMgr;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _initializerDefineProperty = module.initializerDefineProperty;
      _defineProperty = module.defineProperty;
    }, function (module) {
      cclegacy = module.cclegacy;
      Node = module.Node;
      Label = module.Label;
      sp = module.sp;
      _decorator = module._decorator;
      UIOpacity = module.UIOpacity;
      tween = module.tween;
      director = module.director;
      v3 = module.v3;
    }, function (module) {
      sComponent = module.sComponent;
    }, function (module) {
      sAudioMgr = module.default;
    }],
    execute: function () {
      var _dec, _dec2, _dec3, _dec4, _dec5, _dec6, _dec7, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _descriptor5, _descriptor6, _temp;

      cclegacy._RF.push({}, "7a980gCgadBD73ei2nAzeFr", "StarlightPrincess_UIFreewinBingoFree", undefined);

      const {
        ccclass,
        property
      } = _decorator;
      let StarlightPrincess_UIFreewinBingoFree = exports('StarlightPrincess_UIFreewinBingoFree', (_dec = ccclass('StarlightPrincess_UIFreewinBingoFree'), _dec2 = property(Node), _dec3 = property(Label), _dec4 = property(Node), _dec5 = property(sp.Skeleton), _dec6 = property(sp.Skeleton), _dec7 = property(Node), _dec(_class = (_class2 = (_temp = class StarlightPrincess_UIFreewinBingoFree extends sComponent {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "freeBtn", _descriptor, this);

          _initializerDefineProperty(this, "numLabel", _descriptor2, this);

          _initializerDefineProperty(this, "contentNode", _descriptor3, this);

          _initializerDefineProperty(this, "popSpine", _descriptor4, this);

          _initializerDefineProperty(this, "thunderSpine", _descriptor5, this);

          _initializerDefineProperty(this, "LabelNode", _descriptor6, this);

          _defineProperty(this, "touchEnable", true);
        }

        start() {
          this.freeBtn.on('click', () => {
            if (this.touchEnable) {
              this.unscheduleAllCallbacks();
              this.cleanTweenList();
              this.touchEnable = false; //this.popSpine.timeScale = 2.5;

              this.popSpine.setAnimation(0, 'stpr_banner_out', false); //this.scheduleOnce(()=>{

              this.LabelNode.active = true;

              for (let nNode of this.LabelNode.children) {
                nNode.getComponent(UIOpacity).opacity = 255;
                this.pushIDTween(tween(nNode.getComponent(UIOpacity)).to(0.5, {
                  opacity: 0
                }).start(), 'LabelNode1');
              }

              this.popSpine.setCompleteListener(() => {
                director.emit('bingoFreewinInFreeEnd');
                this.contentNode.active = false;
                this.touchEnable = true; //this.popSpine.setCompleteListener(()=>{

                this.popSpine.node.active = false; //});
                // this.scheduleOnce(()=>{
                //     director.emit('bingoFreewinInFree',15);
                // },2)
              });
            }
          }, this);
          director.on('bingoFreewinInFree', count => {
            this.contentNode.active = true; //sAudioMgr.PlayShotAudio('freegame_frame_open',1);

            this.popSpine.node.active = true;
            this.popSpine.setAnimation(0, 'stpr_banner_in', false);
            this.popSpine.setCompleteListener(() => {
              this.popSpine.timeScale = 1;
              this.popSpine.setAnimation(0, 'stpr_banner_loop', true);
            });
            this.LabelNode.active = true;
            let x = 0;

            for (let nNode of this.LabelNode.children) {
              nNode.getComponent(UIOpacity).opacity = 0;
              this.pushIDTween(tween(nNode.getComponent(UIOpacity)).delay(0.3).call(() => {
                x++;

                if (x == 1) {
                  sAudioMgr.PlayShotAudio('freegame_frame_open', 0.8);
                }
              }).delay(0.5).to(0.2, {
                opacity: 255
              }).start(), 'LabelNode1');
              this.pushIDTween(tween(nNode).delay(0.8).to(0.2, {
                scale: v3(1.5, 1.5, 1.5)
              }).to(0.2, {
                scale: v3(1, 1, 1)
              }).start(), 'LabelNode2');
            }

            sAudioMgr.PlayAudio('fsinout_bgm');
            this.setLabelNum(count);
            this.node.setSiblingIndex(this.node.parent.children.length - 1);
            this.unscheduleAllCallbacks();
            this.cleanTweenList();
            this.scheduleOnce(() => {
              this.freeBtn.emit('click');
            }, 3);
          }, this); // this.scheduleOnce(()=>{
          //     director.emit('bingoFreewinInFree',15);
          // },3)
          //
        }

        onEnable() {}

        setLabelNum(num) {
          this.numLabel.string = num.toString();
        }

        playThunderAnima() {
          if (this.thunderSpine) {
            this.thunderSpine.node.active = true; //sAudioMgr.PlayAudio('freegame_transitions');

            this.thunderSpine.setAnimation(0, 'stpr_transition', false);
            this.thunderSpine.setCompleteListener(() => {
              this.thunderSpine.node.active = false;
            });
          }
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "freeBtn", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "numLabel", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "contentNode", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "popSpine", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor5 = _applyDecoratedDescriptor(_class2.prototype, "thunderSpine", [_dec6], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor6 = _applyDecoratedDescriptor(_class2.prototype, "LabelNode", [_dec7], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/StarlightPrincess_History.ts", ['./_rollupPluginModLoBabelHelpers.js', 'cc', './sComponent.ts', './sConfigMgr.ts', './sObjPool.ts', './sAssetMgr.ts'], function (exports) {
  'use strict';

  var _applyDecoratedDescriptor, _initializerDefineProperty, _defineProperty, cclegacy, Node, Button, Sprite, _decorator, director, Vec3, tween, v3, Label, sComponent, sConfigMgr, sObjPool, assetMgr;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _initializerDefineProperty = module.initializerDefineProperty;
      _defineProperty = module.defineProperty;
    }, function (module) {
      cclegacy = module.cclegacy;
      Node = module.Node;
      Button = module.Button;
      Sprite = module.Sprite;
      _decorator = module._decorator;
      director = module.director;
      Vec3 = module.Vec3;
      tween = module.tween;
      v3 = module.v3;
      Label = module.Label;
    }, function (module) {
      sComponent = module.sComponent;
    }, function (module) {
      sConfigMgr = module.sConfigMgr;
    }, function (module) {
      sObjPool = module.sObjPool;
    }, function (module) {
      assetMgr = module.assetMgr;
    }],
    execute: function () {
      var _dec, _dec2, _dec3, _dec4, _dec5, _dec6, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _descriptor5, _temp;

      cclegacy._RF.push({}, "7f90fqRFR5DJ5gedyPUEE+x", "StarlightPrincess_History", undefined);

      const {
        ccclass,
        property
      } = _decorator;
      let StarlightPrincess_History = exports('StarlightPrincess_History', (_dec = ccclass('StarlightPrincess_History'), _dec2 = property(Node), _dec3 = property(Button), _dec4 = property(Button), _dec5 = property(Sprite), _dec6 = property(Sprite), _dec(_class = (_class2 = (_temp = class StarlightPrincess_History extends sComponent {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "itemsNode", _descriptor, this);

          _initializerDefineProperty(this, "upBtn", _descriptor2, this);

          _initializerDefineProperty(this, "downBtn", _descriptor3, this);

          _initializerDefineProperty(this, "upSprite", _descriptor4, this);

          _initializerDefineProperty(this, "downSprite", _descriptor5, this);

          _defineProperty(this, "itemsList", []);

          _defineProperty(this, "viewItenMaxNum", 5);

          _defineProperty(this, "rollIndex", 0);

          _defineProperty(this, "touchEnable", false);
        }

        start() {
          director.on('slotSubGameRollBegin', () => {
            this.removeAllItem();
            this.removeIDTween('itemsNode');
            this.touchEnable = false;
            this.rollIndex = 0;
            this.itemsNode.position = Vec3.ZERO;
            this.btnStateCtr();
          }, this);
          director.on('slotWinBoxItemInfo', obj => {
            if (obj) {
              this.createItem(obj.symbol, obj.rate, obj.num);
            }
          }, this);
          this.upBtn.node.on('click', () => {
            if (this.touchEnable) {
              this.ScrollItemView(1);
            }
          }, this);
          this.downBtn.node.on('click', () => {
            if (this.touchEnable) {
              this.ScrollItemView(-1);
            }
          }, this);
        }

        createItem(symbol, rate, num) {
          this.touchEnable = false; // if(this.itemsList.length >= 5){
          //     for (let i = 0; i < this.itemsList.length; i++) {
          //         const tItem = this.itemsList[i];
          //         this.removeIDTween('createItem_'+tItem.uuid);
          //         this.pushIDTween(tween(tItem).to(0.2,{position : v3(0,(4 - this.itemsList.length + i) * 46,0)},{easing : 'cubicIn'}).start(),'createItem_'+tItem.uuid);
          //     }
          // }

          if (this.itemsList.length >= this.viewItenMaxNum) {
            this.removeIDTween('itemsNode');
            this.pushIDTween(tween(this.itemsNode).to(0.2, {
              position: v3(0, (this.viewItenMaxNum - 1 - this.itemsList.length) * 40, 0)
            }).start(), 'itemsNode');
          }

          const historyItem = sObjPool.Dequeue('historyItem');

          if (historyItem && historyItem.isValid) {
            historyItem.parent = this.itemsNode; // const index = this.itemsList.length >= 4 ? 4 : this.itemsList.length;

            const index = this.itemsList.length;
            historyItem.position = v3(0, 240 + index * 40 + 15, 0);
            historyItem.active = true;
            const renderData = sConfigMgr.instance.GetSymbolBoxImg(symbol);

            if (renderData) {
              const icon = historyItem.getChildByName('icon');

              if (icon) {
                icon.getComponent(Sprite).spriteFrame = assetMgr.GetAssetByName(renderData.rest.render_name);
              }

              historyItem.getChildByName('num').getComponent(Label).string = num;
              historyItem.getChildByName('moy').getComponent(Label).string = this.AddCommas(Math.floor(rate * globalThis.GameBtnEntity.CurrentBetAmount)); //sUtil.AddCommas(rate * globalThis.GameBtnEntity.CurrentBetAmount);
            }

            this.removeIDTween('createItem_' + historyItem.uuid);
            this.pushIDTween(tween(historyItem).delay(index * 0.05).to(0.3, {
              position: v3(0, index * 40 + 15, 0)
            }, {
              easing: 'cubicIn'
            }).by(0.1, {
              position: v3(0, 10, 0)
            }, {
              easing: 'cubicOut'
            }).by(0.1, {
              position: v3(0, -10, 0)
            }, {
              easing: 'cubicIn'
            }).call(() => {
              this.btnStateCtr();
              this.touchEnable = true;
            }).start(), 'createItem_' + historyItem.uuid);
            this.itemsList.push(historyItem);
            this.rollIndex = this.viewItenMaxNum - this.itemsList.length;
          }
        }

        removeAllItem() {
          for (let i = 0; i < this.itemsList.length; i++) {
            const tItem = this.itemsList[i];
            this.removeIDTween('createItem_' + tItem.uuid);
            this.pushIDTween(tween(tItem).delay(i * 0.05).by(0.3, {
              position: v3(0, -200, 0)
            }, {
              easing: 'cubicIn'
            }).call(() => {
              sObjPool.Enqueue('historyItem', tItem);
              tItem.active = false;
            }).start(), 'createItem_' + tItem.uuid);
          }

          this.itemsList = [];
        }

        ScrollItemView(value) {
          this.touchEnable = false;
          this.rollIndex += value;

          if (this.rollIndex > 0) {
            this.rollIndex = 0;
          } else if (this.rollIndex < this.viewItenMaxNum - this.itemsList.length) {
            this.rollIndex = this.viewItenMaxNum - this.itemsList.length;
          }

          this.removeIDTween('itemsNode');
          this.pushIDTween(tween(this.itemsNode).to(0.2, {
            position: v3(0, this.rollIndex * 40, 0)
          }).call(() => {
            this.touchEnable = true;
            this.btnStateCtr();
          }).start(), 'itemsNode');
        }

        btnStateCtr() {
          this.upSprite.grayscale = true;
          this.downSprite.grayscale = true;
          this.upBtn.interactable = false;
          this.downBtn.interactable = false;

          if (this.itemsList && this.itemsList.length > this.viewItenMaxNum) {
            if (this.itemsNode.position.y >= 0) {
              this.downSprite.grayscale = false;
              this.downBtn.interactable = true;
            } else if (this.itemsNode.position.y <= (this.viewItenMaxNum - this.itemsList.length) * 40) {
              this.upSprite.grayscale = false;
              this.upBtn.interactable = true;
            } else {
              this.upSprite.grayscale = false;
              this.downSprite.grayscale = false;
              this.upBtn.interactable = true;
              this.downBtn.interactable = true;
            }
          }
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "itemsNode", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "upBtn", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "downBtn", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "upSprite", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor5 = _applyDecoratedDescriptor(_class2.prototype, "downSprite", [_dec6], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/StarlightPrincess_FreeWinBuyCtr.ts", ['./_rollupPluginModLoBabelHelpers.js', 'cc', './sSoltFreeWinBuyCtr.ts'], function (exports) {
  'use strict';

  var _applyDecoratedDescriptor, _initializerDefineProperty, _defineProperty, cclegacy, Label, _decorator, director, sSoltFreeWinBuyCtr;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _initializerDefineProperty = module.initializerDefineProperty;
      _defineProperty = module.defineProperty;
    }, function (module) {
      cclegacy = module.cclegacy;
      Label = module.Label;
      _decorator = module._decorator;
      director = module.director;
    }, function (module) {
      sSoltFreeWinBuyCtr = module.sSoltFreeWinBuyCtr;
    }],
    execute: function () {
      var _dec, _dec2, _class, _class2, _descriptor, _temp;

      cclegacy._RF.push({}, "80112nCce5DEa5GO25ihG9M", "StarlightPrincess_FreeWinBuyCtr", undefined);

      const {
        ccclass,
        property
      } = _decorator;
      let StarlightPrincess_FreeWinBuyCtr = exports('StarlightPrincess_FreeWinBuyCtr', (_dec = ccclass('StarlightPrincess_FreeWinBuyCtr'), _dec2 = property(Label), _dec(_class = (_class2 = (_temp = class StarlightPrincess_FreeWinBuyCtr extends sSoltFreeWinBuyCtr {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "coinLabel", _descriptor, this);

          _defineProperty(this, "touchEnabel", true);
        }

        onLoad() {
          director.on('sSlotBetInfoUpdate', info => {
            console.log('sSlotBetInfoUpdate1', info);

            if (info) {
              if (info.betAmount) {
                this.coinLabel.string = this.AddCommas(info.betAmount * 50 * 20);
              }
            }
          }, this);
        }

        start() {
          super.start();
          this.freeWinButton.node.on('click', () => {
            //director.emit('slotJackPotBingo',1004223);
            if (this.touchEnabel) {
              this.touchEnabel = false;
              this.cleanTweenList(); // this.node.scale = Vec3.ONE;
              // this.pushOneTween(tween(this.node).to(0.15,{scale : Vec3.ZERO}).start());
            }
          }, this);
          director.on('subGameBetActionBtnCancel', () => {
            this.touchEnabel = true;
            this.cleanTweenList(); // this.pushOneTween(tween(this.node).to(0.15,{scale : Vec3.ONE}).start());
          }, this);
          director.on('subGameBetActionBtnClick', () => {
            this.touchEnabel = true;
            this.cleanTweenList(); // this.pushOneTween(tween(this.node).to(0.15,{scale : Vec3.ONE}).start());
          }, this);
        }

      }, _temp), _descriptor = _applyDecoratedDescriptor(_class2.prototype, "coinLabel", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _class2)) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/StarlightPrincess_UIFreeWinBuy.ts", ['./_rollupPluginModLoBabelHelpers.js', 'cc', './UIFreeWinBuy.ts'], function (exports) {
  'use strict';

  var _applyDecoratedDescriptor, _initializerDefineProperty, _defineProperty, cclegacy, Node, _decorator, director, sp, UIFreeWinBuy;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _initializerDefineProperty = module.initializerDefineProperty;
      _defineProperty = module.defineProperty;
    }, function (module) {
      cclegacy = module.cclegacy;
      Node = module.Node;
      _decorator = module._decorator;
      director = module.director;
      sp = module.sp;
    }, function (module) {
      UIFreeWinBuy = module.UIFreeWinBuy;
    }],
    execute: function () {
      var _dec, _dec2, _class, _class2, _descriptor, _temp;

      cclegacy._RF.push({}, "9259fh+Dw9Pjpq+HuTNd6JZ", "StarlightPrincess_UIFreeWinBuy", undefined);

      const {
        ccclass,
        property
      } = _decorator;
      let StarlightPrincess_UIFreeWinBuy = exports('StarlightPrincess_UIFreeWinBuy', (_dec = ccclass('StarlightPrincess_UIFreeWinBuy'), _dec2 = property(Node), _dec(_class = (_class2 = (_temp = class StarlightPrincess_UIFreeWinBuy extends UIFreeWinBuy {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "SpinNode", _descriptor, this);

          _defineProperty(this, "mBetType1", -1);

          _defineProperty(this, "mCoin1", 0);

          _defineProperty(this, "canBuy1", false);
        }

        onLoad() {
          director.on('sSlotBetInfoUpdate', info => {
            console.log('sSlotBetInfoUpdate2', info);

            if (info) {
              if (info.betAmount) {
                this.coinLabel.string = this.AddCommas(info.betAmount * 50 * 20);
              }
            }
          }, this);
        }

        start() {
          this.startBtn.on('click', () => {
            if (this.canBuy1) {
              this.SpinNode.getComponent(sp.Skeleton).setAnimation(0, "stpr_buy_feature_center_yes", false);
              this.SpinNode.getComponent(sp.Skeleton).setCompleteListener(() => {
                this.scheduleOnce(() => {
                  if (this.mBetType1 != -1) {
                    director.emit('subGameBetActionBtnClick', this.mBetType1, this.mCoin1);
                  }

                  this.mainNode.active = false;
                }, 0.5);
                this.SpinNode.getComponent(sp.Skeleton).setAnimation(0, "stpr_buy_feature_center_out", false);
                this.SpinNode.getComponent(sp.Skeleton).setCompleteListener(() => {});
              });
            } else {
              globalThis.goldNotenough && globalThis.goldNotenough();
            }
          }, this);
          this.cancelNode.on('click', () => {
            this.mBetType1 = -1;
            this.SpinNode.getComponent(sp.Skeleton).setAnimation(0, "stpr_buy_feature_center_no", false);
            this.scheduleOnce(() => {
              this.mainNode.active = false;
              director.emit('subGameBetActionBtnCancel');
            }, 0.5);
            this.SpinNode.getComponent(sp.Skeleton).setCompleteListener(() => {
              this.SpinNode.getComponent(sp.Skeleton).setAnimation(0, "stpr_buy_feature_center_out", false);
              this.SpinNode.getComponent(sp.Skeleton).setCompleteListener(() => {});
            });
          }, this); // director.on('sSlotBetInfoUpdate',(info)=>{
          //     console.log('sSlotBetInfoUpdate2',info);
          //     if(info){
          //         if(info.betAmount){
          //             this.coinLabel.string = this.AddCommas(info.betAmount * 50*20);
          //         };
          //     };
          // },this); 
        }

        viewInit(coin, betType, _canBuy, _tipStr) {
          //this.coinLabel.string = this.AddCommas(coin);
          this.mCoin1 = coin;
          this.mBetType1 = betType;
          this.canBuy1 = _canBuy;
          this.mainNode.active = true;
          this.SpinNode.getComponent(sp.Skeleton).setAnimation(0, "stpr_buy_feature_center_in", false);
          this.SpinNode.getComponent(sp.Skeleton).setCompleteListener(() => {//this.SpinNode.getComponent(sp.Skeleton).setAnimation(0,"stpr_buy_feature_center_loop",false);
          });
        }

      }, _temp), _descriptor = _applyDecoratedDescriptor(_class2.prototype, "SpinNode", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _class2)) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/StarlightPrincess_weightNode.ts", ['./_rollupPluginModLoBabelHelpers.js', 'cc'], function (exports) {
  'use strict';

  var _applyDecoratedDescriptor, _initializerDefineProperty, cclegacy, Node, _decorator, Component, view, UITransform, v3;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _initializerDefineProperty = module.initializerDefineProperty;
    }, function (module) {
      cclegacy = module.cclegacy;
      Node = module.Node;
      _decorator = module._decorator;
      Component = module.Component;
      view = module.view;
      UITransform = module.UITransform;
      v3 = module.v3;
    }],
    execute: function () {
      var _dec, _dec2, _dec3, _dec4, _class, _class2, _descriptor, _descriptor2, _descriptor3, _temp;

      cclegacy._RF.push({}, "94a07E6hDVMlYiqQSEdmJhZ", "StarlightPrincess_weightNode", undefined);

      const {
        ccclass,
        property
      } = _decorator;
      /**
       * Predefined variables
       * Name = StarlightPrincess_weightNode
       * DateTime = Tue Oct 10 2023 17:23:55 GMT+0800 (中国标准时间)
       * Author = dadadaxigua
       * FileBasename = StarlightPrincess_weightNode.ts
       * FileBasenameNoExtension = StarlightPrincess_weightNode
       * URL = db://assets/game/StarlightPrincess_weightNode.ts
       * ManualUrl = https://docs.cocos.com/creator/3.4/manual/zh/
       *
       */

      let StarlightPrincess_weightNode = exports('StarlightPrincess_weightNode', (_dec = ccclass('StarlightPrincess_weightNode'), _dec2 = property(Node), _dec3 = property(Node), _dec4 = property(Node), _dec(_class = (_class2 = (_temp = class StarlightPrincess_weightNode extends Component {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "leftNode", _descriptor, this);

          _initializerDefineProperty(this, "RightNode", _descriptor2, this);

          _initializerDefineProperty(this, "stpr_freegame_idle", _descriptor3, this);
        }

        onLoad() {
          let posX1 = view.getVisibleSize().x / 2 - 415;
          let posX2 = view.getVisibleSize().x / 2 - 415;
          this.leftNode.getComponent(UITransform).setContentSize(posX1, 600);
          this.RightNode.getComponent(UITransform).setContentSize(posX2, 600);
          let stpr_freeNodeX = this.stpr_freegame_idle.getComponent(UITransform).contentSize.x * 0.72;
          let scaleX = view.getVisibleSize().x / stpr_freeNodeX + 0.4;
          this.stpr_freegame_idle.scale = v3(scaleX, scaleX, scaleX); // if(scaleX>1){
          //     for(let i=1;i<=10;i++){
          //         if(stpr_freeNodeX*(scaleX+i*0.1)>view.getVisibleSize().x){
          //             scaleX=scaleX+i*0.1;
          //             this.stpr_freegame_idle.scale=v3(scaleX,scaleX,scaleX);
          //         }else{
          //         };
          //     };
          // };
        }

        start() {// [3]
        } // update (deltaTime: number) {
        //     // [4]
        // }


      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "leftNode", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "RightNode", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "stpr_freegame_idle", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class));
      /**
       * [1] Class member could be defined like this.
       * [2] Use `property` decorator if your want the member to be serializable.
       * [3] Your initialization goes here.
       * [4] Your update function goes here.
       *
       * Learn more about scripting: https://docs.cocos.com/creator/3.4/manual/zh/scripting/
       * Learn more about CCClass: https://docs.cocos.com/creator/3.4/manual/zh/scripting/decorator.html
       * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.4/manual/zh/scripting/life-cycle-callbacks.html
       */

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/StarlightPrincess_Huaxianzi.ts", ['./_rollupPluginModLoBabelHelpers.js', 'cc'], function (exports) {
  'use strict';

  var _applyDecoratedDescriptor, _initializerDefineProperty, _defineProperty, cclegacy, Node, _decorator, Component, director, sp;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _initializerDefineProperty = module.initializerDefineProperty;
      _defineProperty = module.defineProperty;
    }, function (module) {
      cclegacy = module.cclegacy;
      Node = module.Node;
      _decorator = module._decorator;
      Component = module.Component;
      director = module.director;
      sp = module.sp;
    }],
    execute: function () {
      var _dec, _dec2, _class, _class2, _descriptor, _temp;

      cclegacy._RF.push({}, "9b12a4K96BKX6od9AGgOJxE", "StarlightPrincess_Huaxianzi", undefined);

      const {
        ccclass,
        property
      } = _decorator;
      /**
       * Predefined variables
       * Name = StarlightPrincess_Huaxianzi
       * DateTime = Thu Sep 21 2023 13:46:33 GMT+0800 (中国标准时间)
       * Author = dadadaxigua
       * FileBasename = StarlightPrincess_Huaxianzi.ts
       * FileBasenameNoExtension = StarlightPrincess_Huaxianzi
       * URL = db://assets/game/StarlightPrincess_Huaxianzi.ts
       * ManualUrl = https://docs.cocos.com/creator/3.4/manual/zh/
       *
       */

      let StarlightPrincess_Huaxianzi = exports('StarlightPrincess_Huaxianzi', (_dec = ccclass('StarlightPrincess_Huaxianzi'), _dec2 = property(Node), _dec(_class = (_class2 = (_temp = class StarlightPrincess_Huaxianzi extends Component {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "spawnAnima", _descriptor, this);

          _defineProperty(this, "arr1", ["stpr_character_01", "stpr_character_action"]);

          _defineProperty(this, "arr2", ["stpr_character_02"]);

          _defineProperty(this, "arr3", ["stpr_character_03_win"]);
        }

        start() {
          director.on('controlHuaXianZi', str => {
            if (str == 'NormalAction') {
              this.node.children[3].active = true;
              this.node.children[3].getComponent(sp.Skeleton).setAnimation(0, "stpr_character_ol_fx", false);
              this.node.children[3].getComponent(sp.Skeleton).setCompleteListener(() => {
                this.node.children[3].active = false;
              });
              this.node.children[0].active = true;
              this.node.children[1].active = false;
              this.node.children[2].active = false;
              this.scheduleOnce(() => {
                let strs = this.node.children[0].getComponent(sp.Skeleton).animation;
                let num = 1;

                if (strs == "stpr_character_action") {
                  num = 0;
                }

                this.node.children[0].getComponent(sp.Skeleton).animation = this.arr1[num];
                this.node.children[0].active = true;
                this.node.children[1].active = false;
                this.node.children[2].active = false;
              }, 0.2);
            } else if (str == 'NormalIdle') {
              this.scheduleOnce(() => {
                this.node.children[3].active = false;
                this.node.children[0].active = true;
                this.node.children[1].active = false;
                this.node.children[3].active = true;
                this.node.children[3].getComponent(sp.Skeleton).setAnimation(0, "stpr_character_ol_fx", false);
                this.node.children[3].getComponent(sp.Skeleton).setCompleteListener(() => {
                  this.node.children[3].active = false;
                });
                this.scheduleOnce(() => {
                  this.node.children[0].active = false;
                  this.node.children[2].active = true;
                  this.node.children[2].getComponent(sp.Skeleton).setAnimation(0, this.arr3[0], true);
                }, 0.25);
                this.scheduleOnce(() => {
                  this.node.children[3].active = true;
                  this.node.children[3].getComponent(sp.Skeleton).setAnimation(0, "stpr_character_ol_fx", false);
                  this.node.children[3].getComponent(sp.Skeleton).setCompleteListener(() => {
                    this.node.children[3].active = false;
                  });
                  this.scheduleOnce(() => {
                    this.node.children[1].active = false;
                    this.node.children[2].active = false;
                    this.node.children[0].active = true;
                    this.node.children[0].getComponent(sp.Skeleton).setAnimation(0.1, "stpr_character_01", true);
                  }, 0.25);
                }, 1);
              }, 0.3);
            } else if (str == 'FreeSpin') {
              this.node.children[1].getComponent(sp.Skeleton).animation = this.arr2[0];
              this.node.children[0].active = false;
              this.node.children[1].active = false;
              this.node.children[2].active = false;
              this.node.children[3].active = true;
              this.node.children[3].getComponent(sp.Skeleton).setCompleteListener(() => {
                this.node.children[3].active = false;
              });
              this.node.children[1].active = true;
              this.node.children[0].active = false;
              this.node.children[2].active = false;
            }
          }, this);
        }

      }, _temp), _descriptor = _applyDecoratedDescriptor(_class2.prototype, "spawnAnima", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _class2)) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/soltGameCheckLine_149.ts", ['cc'], function (exports) {
  'use strict';

  var cclegacy;
  return {
    setters: [function (module) {
      cclegacy = module.cclegacy;
    }],
    execute: function () {
      cclegacy._RF.push({}, "ae5f0M1rxpIr7VtHVzHD2do", "soltGameCheckLine_149", undefined);

      let odds = {
        "1": {},
        "1101": {
          "8": 200,
          "9": 200,
          "10": 500,
          "11": 500,
          "12": 1000,
          "13": 1000,
          "14": 1000,
          "15": 1000,
          "16": 1000,
          "17": 1000,
          "18": 1000,
          "19": 1000,
          "20": 1000,
          "21": 1000,
          "22": 1000,
          "23": 1000,
          "24": 1000,
          "25": 1000,
          "26": 1000,
          "27": 1000,
          "28": 1000,
          "29": 1000,
          "30": 1000
        },
        "1201": {
          "8": 50,
          "9": 50,
          "10": 200,
          "11": 200,
          "12": 500,
          "13": 500,
          "14": 500,
          "15": 500,
          "16": 500,
          "17": 500,
          "18": 500,
          "19": 500,
          "20": 500,
          "21": 500,
          "22": 500,
          "23": 500,
          "24": 500,
          "25": 500,
          "26": 500,
          "27": 500,
          "28": 500,
          "29": 500,
          "30": 500
        },
        "1301": {
          "8": 40,
          "9": 40,
          "10": 100,
          "11": 100,
          "12": 300,
          "13": 300,
          "14": 300,
          "15": 300,
          "16": 300,
          "17": 300,
          "18": 300,
          "19": 300,
          "20": 300,
          "21": 300,
          "22": 300,
          "23": 300,
          "24": 300,
          "25": 300,
          "26": 300,
          "27": 300,
          "28": 300,
          "29": 300,
          "30": 300
        },
        "1401": {
          "8": 30,
          "9": 30,
          "10": 40,
          "11": 40,
          "12": 240,
          "13": 240,
          "14": 240,
          "15": 240,
          "16": 240,
          "17": 240,
          "18": 240,
          "19": 240,
          "20": 240,
          "21": 240,
          "22": 240,
          "23": 240,
          "24": 240,
          "25": 240,
          "26": 240,
          "27": 240,
          "28": 240,
          "29": 240,
          "30": 240
        },
        "1501": {
          "8": 20,
          "9": 20,
          "10": 30,
          "11": 30,
          "12": 200,
          "13": 200,
          "14": 200,
          "15": 200,
          "16": 200,
          "17": 200,
          "18": 200,
          "19": 200,
          "20": 200,
          "21": 200,
          "22": 200,
          "23": 200,
          "24": 200,
          "25": 200,
          "26": 200,
          "27": 200,
          "28": 200,
          "29": 200,
          "30": 200
        },
        "1601": {
          "8": 16,
          "9": 16,
          "10": 24,
          "11": 24,
          "12": 160,
          "13": 160,
          "14": 160,
          "15": 160,
          "16": 160,
          "17": 160,
          "18": 160,
          "19": 160,
          "20": 160,
          "21": 160,
          "22": 160,
          "23": 160,
          "24": 160,
          "25": 160,
          "26": 160,
          "27": 160,
          "28": 160,
          "29": 160,
          "30": 160
        },
        "1701": {
          "8": 10,
          "9": 10,
          "10": 20,
          "11": 20,
          "12": 100,
          "13": 100,
          "14": 100,
          "15": 100,
          "16": 100,
          "17": 100,
          "18": 100,
          "19": 100,
          "20": 100,
          "21": 100,
          "22": 100,
          "23": 100,
          "24": 100,
          "25": 100,
          "26": 100,
          "27": 100,
          "28": 100,
          "29": 100,
          "30": 100
        },
        "1801": {
          "8": 8,
          "9": 8,
          "10": 18,
          "11": 18,
          "12": 80,
          "13": 80,
          "14": 80,
          "15": 80,
          "16": 80,
          "17": 80,
          "18": 80,
          "19": 80,
          "20": 80,
          "21": 80,
          "22": 80,
          "23": 80,
          "24": 80,
          "25": 80,
          "26": 80,
          "27": 80,
          "28": 80,
          "29": 80,
          "30": 80
        },
        "1901": {
          "8": 5,
          "9": 5,
          "10": 15,
          "11": 15,
          "12": 40,
          "13": 40,
          "14": 40,
          "15": 40,
          "16": 40,
          "17": 40,
          "18": 40,
          "19": 40,
          "20": 40,
          "21": 40,
          "22": 40,
          "23": 40,
          "24": 40,
          "25": 40,
          "26": 40,
          "27": 40,
          "28": 40,
          "29": 40,
          "30": 40
        },
        "2001": {},
        "2002": {},
        "2003": {},
        "2004": {},
        "2005": {},
        "2006": {},
        "2007": {},
        "2008": {},
        "2009": {},
        "2010": {},
        "2011": {},
        "2012": {},
        "2013": {},
        "2014": {},
        "2015": {}
      };
      let topSymbols = [];
      let bottom_symbol_length = 1;
      let row = 5;
      let wildSymbols = [];
      let unableWinSymbol = [];
      let conversIDToMulti = {
        2001: 2,
        2002: 3,
        2003: 4,
        2004: 5,
        2005: 6,
        2006: 8,
        2007: 10,
        2008: 12,
        2009: 15,
        2010: 20,
        2011: 25,
        2012: 50,
        2013: 100,
        2014: 250,
        2015: 500
      };
      const soltGameCheckUtil_149 = exports('soltGameCheckUtil_149', {
        checkWon(mapArr) {
          // mapArr = _.cloneDeep(mapArr)
          let originalArr = [];

          for (let i = 0; i < mapArr.length; i++) {
            originalArr[i] = [];

            for (let j = bottom_symbol_length; j < row + bottom_symbol_length; j++) {
              originalArr[i].push(mapArr[i][j]);
            }
          } //特殊处理
          // let lastMapArr = mapArr[mapArr.length - 1]
          // lastMapArr = lastMapArr.slice(bottom_symbol_length, lastMapArr.length - top_symbol_length - 1)
          // for (let i = 0; i < 4; i++) {
          //     originalArr[i + 1].push(lastMapArr[i])
          // }


          let arr = this.conversSymbolId(originalArr);
          let colMap = {};
          let curCol = 0;
          let wonItemMap = {};
          let indexArr = [];

          for (let i = 0; i < row; i++) {
            indexArr.push(i * arr.length);
          } // arr[0][0] = 16;
          // arr[1][1] = 16;
          // arr[2][2] = 11;


          while (curCol < arr.length) {
            let curColArr = arr[curCol]; // colMap[curCol] = {};

            for (let i = 0; i < curColArr.length; i++) {
              let item = curColArr[i];

              if (unableWinSymbol.includes(+item)) {
                //不计入连线的ID
                continue;
              }

              if (!colMap[item]) {
                colMap[item] = [];
              }

              colMap[item].push(indexArr[i] + curCol);
            }

            curCol++;
          }

          let wonPosMap = {};
          let multiPosMap = {};
          const colMapKeys = Object.keys(colMap);

          if (colMapKeys && colMapKeys.length > 0) {
            for (let k = 0; k < colMapKeys.length; k++) {
              const wonValues = colMap[colMapKeys[k]];

              if (wonValues && wonValues.length >= 8) {
                wonItemMap[colMapKeys[k]] = wonValues;
                wonPosMap[colMapKeys[k]] = [];
              }

              if (parseInt(colMapKeys[k]) >= 2000) {
                //console.log('大于2000',colMap[item]);
                multiPosMap[colMapKeys[k]] = wonValues;
              }
            }
          } //找出赢的图标 图标出现在第三列说明至少3连
          // for (let iconKey in colMap[2]) {
          //     wonPosMap[iconKey] = [];
          // }


          for (let key in colMap) {
            if (wonPosMap[key]) {
              wonPosMap[key] = [...wonPosMap[key], ...colMap[key]];
            }
          }

          let wonPosArr = Object.values(wonPosMap);
          let categoryData = this.symbolCategory(arr, wonPosArr);
          categoryData.bonus_line_num = 0;
          categoryData.bonus_line_length = 0;
          let symbolWins = [];
          let allRate = 0;
          let clear = false;

          for (let item in wonItemMap) {
            if (+item <= 2000) {
              let itemArr = wonItemMap[item];
              let linkCount = 1;
              let winItem = {};
              let odd = odds[item][itemArr.length];
              let won = linkCount * odd;
              winItem['rate'] = won;
              winItem['pos'] = this.conversActPos(wonPosMap[item]); //console.log('中奖格子',item,itemArr.length,odd)

              let row = Math.floor(wonPosMap[item][0] / 6);
              let col = wonPosMap[item][0] % 6;
              winItem['bonus_symbol'] = originalArr[col][row];
              symbolWins.push(winItem);
              clear = true;
              categoryData.bonus_line_num += linkCount;
              categoryData.bonus_line_length = Math.max(itemArr.length, categoryData.bonus_line_length); // console.log(`连线${item}------${linkCount}条--------${itemArr.length}列--------赢得${won}`)

              allRate += won;
            }
          }

          let opt = {
            multiData: []
          }; //opt.multiData = [];

          for (let items in multiPosMap) {
            const itemArr = multiPosMap[items];
            const multi = conversIDToMulti[items];

            if (multi > 0) {
              let winItem = {};
              winItem['symbol'] = items;
              winItem['pos'] = this.conversActPos(itemArr);
              winItem['multi'] = itemArr.length * multi;
              opt.multiData.push(winItem);
            }
          }

          return {
            symbolWins: symbolWins,
            allRate: allRate,
            wonPosArr: wonPosArr,
            clear: clear,
            categoryData: categoryData,
            opt: opt
          };
        },

        conversSymbolId(mapArr) {
          let arr = [];

          for (let i = 0; i < mapArr.length; i++) {
            let rowArr = mapArr[i];
            arr[i] = [];

            for (let j = 0; j < rowArr.length; j++) {
              let index = rowArr[j];

              if (wildSymbols.includes(index)) {
                //百搭符号不处理
                arr[i].push(index);
                continue;
              } // let tempIndex = j;
              // while (index == 10000) {
              //     tempIndex--;
              //     if (tempIndex < 0) {
              //         throw console.error('error conversSymbolId:', JSON.stringify(mapArr));
              //     }
              //     index = rowArr[tempIndex]
              // }


              arr[i].push(index);
            }
          }

          return arr;
        },

        conversActPos(winPos) {
          let arr = [];

          for (let i = 0; i < winPos.length; i++) {
            let row = Math.floor(winPos[i] / 6);
            let col = winPos[i] % 6;
            arr.push(`${col}_${row}`);
          }

          return arr;
        },

        symbolCategory(arr, wonPosArr) {
          let winArr = [];

          for (let i = 0; i < wonPosArr.length; i++) {
            winArr = [...winArr, ...this.conversActPos(wonPosArr[i])];
          }

          let bonusSymbolCategoryMap = {};
          let symbolCategoryMap = {};
          let topSymbolNum = 0;

          for (let i = 0; i < arr.length; i++) {
            let colArr = arr[i];

            for (let j = 0; j < colArr.length; j++) {
              let pos = `${i}_${j}`;
              let symbolId = arr[i][j];
              if (topSymbols.includes(symbolId)) topSymbolNum++;

              if (winArr.includes(pos)) {
                if (!bonusSymbolCategoryMap[symbolId]) {
                  bonusSymbolCategoryMap[symbolId] = 0;
                }

                bonusSymbolCategoryMap[symbolId]++;
              }

              if (!symbolCategoryMap[symbolId]) {
                symbolCategoryMap[symbolId] = 0;
              }

              symbolCategoryMap[symbolId]++;
            }
          }

          return {
            symbolCategoryArr: Object.values(symbolCategoryMap),
            bonusSymbolCategoryArr: Object.values(bonusSymbolCategoryMap),
            bonus_symbol_toprate_num: topSymbolNum
          };
        }

      });

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/StarlightPrincess_NormalFlow.ts", ['./_rollupPluginModLoBabelHelpers.js', 'cc', './sUtil.ts', './sConfigMgr.ts', './sAudioMgr.ts', './sBoxMgr.ts', './sNormalFlow.ts'], function (exports) {
  'use strict';

  var _applyDecoratedDescriptor, _initializerDefineProperty, _defineProperty, cclegacy, Node, _decorator, director, sUtil, sConfigMgr, sAudioMgr, sBoxMgr, sNormalFlow;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _initializerDefineProperty = module.initializerDefineProperty;
      _defineProperty = module.defineProperty;
    }, function (module) {
      cclegacy = module.cclegacy;
      Node = module.Node;
      _decorator = module._decorator;
      director = module.director;
    }, function (module) {
      sUtil = module.sUtil;
    }, function (module) {
      sConfigMgr = module.sConfigMgr;
    }, function (module) {
      sAudioMgr = module.default;
    }, function (module) {
      sBoxMgr = module.sBoxMgr;
    }, function (module) {
      sNormalFlow = module.sNormalFlow;
    }],
    execute: function () {
      var _dec, _dec2, _class, _class2, _descriptor, _temp;

      cclegacy._RF.push({}, "bdad29Hn5lHQaNLb0iHVpSm", "StarlightPrincess_NormalFlow", undefined);

      const {
        ccclass,
        property
      } = _decorator;
      let StarlightPrincess_NormalFlow = exports('StarlightPrincess_NormalFlow', (_dec = ccclass('StarlightPrincess_NormalFlow'), _dec2 = property(Node), _dec(_class = (_class2 = (_temp = class StarlightPrincess_NormalFlow extends sNormalFlow {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "effectLayer", _descriptor, this);

          _defineProperty(this, "tatalWin", 0);
        }

        start() {
          super.start(); // this.scheduleOnce(()=>{
          //     sBoxMgr.instance.updateBoxDataByXY(1, 0, 'win', 'settlement');
          //     sBoxMgr.instance.updateBoxDataByXY(1, 1, 'win', 'settlement');
          //     sBoxMgr.instance.updateBoxDataByXY(1, 2, 'win', 'settlement');
          //     sBoxMgr.instance.updateBoxDataByXY(1, 3, 'win', 'settlement');
          //     sBoxMgr.instance.updateBoxDataByXY(1, 4, 'win', 'settlement');
          // },2);

          director.on('betBtnClick', betSpeedMode => {
            if (betSpeedMode != 'turbo') {
              sAudioMgr.PlayAudio('normal_spinning', true);
            }
          }, this);
        }

        clearUI() {
          super.clearUI();
          this.removeScheduleDic();
        }

        rollStopTurboAction(round, rollSpeedMode, clickMode) {
          director.off('rollOneColumnBump');
          director.off('byWayWinLightEnd');
          director.off('titleWinSettleEnd');
          director.off('rollStopOneRoundCall');
          director.off('StarlightPrincessBigWinEnd'); // const isJackpot = this.jackpotAmount > 0;
          // const isJackpot = true;

          this.tatalWin = 0;

          const _boxViewSize = sBoxMgr.instance.getBoxViewSize();

          const _boxSize = sBoxMgr.instance.getBoxSize();

          const round_symbol_map = round['round_symbol_map'];
          director.on('rollOneColumnStop', col => {
            if (col == 5) {
              sAudioMgr.PlayShotAudio('normal_spin_end', 1);
              sAudioMgr.StopAudio();
            }
          }, this);

          if (round) {
            let timestop = 0;
            const round_symbol_map = round['round_symbol_map'];

            for (let xx of round_symbol_map[0]) {
              for (let yy = 0; yy < xx.length; yy++) {
                //console.log('symbel:',xx[yy]);
                if (yy >= 1 && yy <= 5 && xx[yy] > 2000) {
                  timestop = 0;
                  director.emit('controlHuaXianZi', 'NormalIdle');
                  break;
                }
              }

              if (timestop > 0) break;
            }

            this.normalFlowDelayCall(() => {
              this.scheduleOnce(() => {
                let needWinLight = round.rate > 0 ? true : false;
                const round_symbol_map = round['round_symbol_map'];
                sConfigMgr.instance.SetResData(round_symbol_map);
                const hits = round.hit_events_list;
                const config = sConfigMgr.instance.getConfig();
                const events = config['event_config'];

                for (let i = 0; i < hits.length; i++) {
                  const _event = hits[i];

                  if (_event && Array.isArray(_event)) {
                    for (let j = 0; j < _event.length; j++) {
                      const eve = _event[j];
                      const hitEvent = events[eve];
                      if (hitEvent && hitEvent.event_type == 'bonusGame') ;
                    }
                  }
                }

                director.on('rollOneColumnBump', col => {
                  sAudioMgr.PlayShotAudio('luodi', 0.4);
                  const colBoxs = sBoxMgr.instance.GetColAllBox(col);
                  let shunxu = 0;
                  let shunxu1 = 0;

                  if (colBoxs) {
                    for (let rY = 0; rY < colBoxs.length; rY++) {
                      const rSymbol = colBoxs[rY];

                      if (rSymbol.SymbolValue > 2000) {
                        if (rSymbol.ViewItemIndex.y >= 0 && rSymbol.ViewItemIndex.y <= 4) {
                          rSymbol.DoEnitityAction('thunder');
                          console.log('召唤一道落雷4');

                          if (rSymbol.SymbolValue == 1) {
                            shunxu1++;

                            if (shunxu1 == 1) {
                              this.playmultMp();
                            }
                          }
                        }
                      } else if (rSymbol.SymbolValue == 1) {
                        shunxu++;
                        sAudioMgr.PlayShotAudio('scatter_appear' + shunxu);

                        if (shunxu == 1) {
                          this.playscatterMp();
                        }
                      }
                    }
                  }
                }, this);
                sUtil.once('byWayWinLightEnd', () => {
                  sUtil.once('titleWinSettleEnd', () => {
                    //     if(sbox.SymbolValue>2000){
                    //         sBoxMax=true;
                    //     };
                    //     return (sbox.SymbolValue>2000);
                    // },'win','settlement');
                    let times = 0.5;
                    this.scheduleOnce(() => {
                      const rate = round.rate;

                      if (rate >= 200) {
                        director.emit('betUserInfoUpdateWinAnima', rate - this.tatalWin);
                        director.emit('StarlightPrincessBigWin', rate);
                        sUtil.once('StarlightPrincessBigWinEnd', () => {
                          this.scheduleOnce(() => {
                            console.log('rollstop1');
                            director.emit('rollStop');
                          }, 0.5);
                        });
                      } else {
                        this.scheduleOnce(() => {
                          console.log('rollstop2');
                          director.emit('rollStop');
                        }, 0.5);
                      }
                    }, times);
                  });
                  director.emit('winBetRes', -1);
                });
                sUtil.once('rollStopOneRoundCall', () => {
                  director.off('rollOneColumnStop');
                  director.off('rollOneColumnBump');

                  if (needWinLight) {
                    // this.scheduleOnce(()=>{
                    //     this.byWayBoxLightCtr(round,'normal');
                    // },1);
                    this.byWayBoxLightCtr(round, 'normal');
                  } else {
                    console.log('rollstop3');
                    director.emit('rollStop');
                  }
                });
                sBoxMgr.instance.rollStopAllColumn();
              }, timestop);
            });
          }
        }

        rollStopAction(round, rollSpeedMode, clickMode) {
          director.off('rollOneColumnBump');
          director.off('byWayWinLightEnd');
          director.off('titleWinSettleEnd');
          director.off('rollStopOneRoundCall');
          director.off('StarlightPrincessBigWinEnd'); // const isJackpot = this.jackpotAmount > 0;
          // const isJackpot = true;

          this.tatalWin = 0;

          const _boxViewSize = sBoxMgr.instance.getBoxViewSize();

          const _boxSize = sBoxMgr.instance.getBoxSize();

          const round_symbol_map = round['round_symbol_map'];
          director.on('rollOneColumnStop', col => {
            if (col == 0) {
              sAudioMgr.StopAudio();
            }
          }, this);

          if (round) {
            this.normalFlowDelayCall(() => {
              let timestop = 0;
              const round_symbol_map = round['round_symbol_map'];

              for (let xx of round_symbol_map[0]) {
                for (let yy = 0; yy < xx.length; yy++) {
                  //console.log('symbel:',xx[yy]);
                  if (yy >= 1 && yy <= 5 && xx[yy] > 2000) {
                    timestop = 0;
                    director.emit('controlHuaXianZi', 'NormalIdle');
                    break;
                  }
                }

                if (timestop > 0) break;
              }

              this.scheduleOnce(() => {
                let needWinLight = round.rate > 0 ? true : false;
                sConfigMgr.instance.SetResData(round_symbol_map);
                const hits = round.hit_events_list;
                const config = sConfigMgr.instance.getConfig();
                const events = config['event_config'];

                for (let i = 0; i < hits.length; i++) {
                  const _event = hits[i];

                  if (_event && Array.isArray(_event)) {
                    for (let j = 0; j < _event.length; j++) {
                      const eve = _event[j];
                      const hitEvent = events[eve];
                      if (hitEvent && hitEvent.event_type == 'bonusGame') ;
                    }
                  }
                }

                director.on('rollOneColumnBump', col => {
                  sAudioMgr.PlayShotAudio('normal_spin_col_end', 1);
                  const colBoxs = sBoxMgr.instance.GetColAllBox(col);
                  let shunxu = 0;
                  let shunxu1 = 0;

                  if (colBoxs) {
                    for (let rY = 0; rY < colBoxs.length; rY++) {
                      const rSymbol = colBoxs[rY];

                      if (rSymbol.SymbolValue > 2000) {
                        if (rSymbol.ViewItemIndex.y >= 0 && rSymbol.ViewItemIndex.y <= 4) {
                          rSymbol.DoEnitityAction('thunder');
                          console.log('召唤一道落雷5');

                          if (rSymbol.SymbolValue == 1) {
                            shunxu1++;

                            if (shunxu1 == 1) {
                              this.playmultMp();
                            }
                          }
                        }
                      } else if (rSymbol.SymbolValue == 1) {
                        shunxu++;
                        sAudioMgr.PlayShotAudio('scatter_appear' + shunxu);

                        if (shunxu == 1) {
                          this.playscatterMp();
                        }
                      }
                    }
                  }
                }, this);
                sUtil.once('byWayWinLightEnd', () => {
                  sUtil.once('titleWinSettleEnd', () => {
                    //     if(sbox.SymbolValue>2000){
                    //         sBoxMax=true;
                    //     };
                    //     return (sbox.SymbolValue>2000);
                    // },'win','settlement');
                    let times = 0.5;
                    this.scheduleOnce(() => {
                      const rate = round.rate;

                      if (rate >= 200) {
                        director.emit('betUserInfoUpdateWinAnima', rate - this.tatalWin);
                        director.emit('StarlightPrincessBigWin', rate);
                        sUtil.once('StarlightPrincessBigWinEnd', () => {
                          director.emit('rollStop');
                        });
                      } else {
                        this.scheduleOnce(() => {
                          console.log('rollstop2');
                          director.emit('rollStop');
                        }, 0.5);
                      }
                    }, times);
                  });
                  director.emit('winBetRes', -1);
                });
                sUtil.once('rollStopOneRoundCall', () => {
                  director.off('rollOneColumnStop');
                  director.off('rollOneColumnBump');

                  if (needWinLight) {
                    // this.scheduleOnce(()=>{
                    //     this.byWayBoxLightCtr(round,'normal');
                    // },1);
                    this.byWayBoxLightCtr(round, 'normal');
                  } else {
                    //director.emit('betUserInfoUpdateWinAnima',round.rate);
                    director.emit('rollStop');
                  }
                });
                sBoxMgr.instance.rollStopAllColumn();
              }, timestop);
            });
          }
        }

        byWayBoxLightCtr(round, rollSpeedMode) {
          director.off('dropAllBoxFinish');
          const eventArr = sConfigMgr.instance.GetAllEventOptConfigByHitListOnCheck(round); // console.log('eventArr:'+JSON.stringify(eventArr));

          const boxSum = sBoxMgr.instance.boxSum;
          let isEnd = false;
          let isTotalWin = false;
          let IsMulit = false;
          let winRate = 0;

          if (eventArr && Array.isArray(eventArr)) {
            let eventIndex = 0;

            const boxAnimaCall = () => {
              let isWin = false;

              if (eventIndex < eventArr.length) {
                isEnd = eventIndex == eventArr.length - 1;
                const nowData = eventArr[eventIndex++];

                if (nowData && nowData.eventData && nowData.eventData.length > 0) {
                  const events = nowData.eventData;
                  let resAct = {};
                  let winSymbol = {};
                  winRate = 0;

                  for (let i = 0; i < events.length; i++) {
                    const _event = events[i];

                    if (_event.event_type == 'boxAnima') {
                      if (_event.rate_change && _event.rate_change.num) {
                        winRate += _event.rate_change.num;
                      }

                      if (_event.bonus_symbol) {
                        winSymbol[_event.bonus_symbol] = true;
                      }

                      isWin = true;
                      isTotalWin = true;
                      let act = resAct[_event.bonus_symbol];

                      if (!act) {
                        act = {};
                        act.bonus_symbol = _event.bonus_symbol;
                        act.act_pos = {};
                        act.rate = 0;
                      }

                      if (_event.rate_change) {
                        if (act.rate == 0) {
                          act.rate = _event.rate_change.num;
                        } else {
                          if (_event.rate_change.type == 0) {
                            act.rate += _event.rate_change.num;
                          } else if (_event.rate_change.type == 1) {
                            act.rate *= _event.rate_change.num;
                          }
                        }
                      }

                      if (_event.act_pos) {
                        const poss = _event['act_pos'].split(',');

                        if (poss) {
                          for (let a = 0; a < poss.length; a++) {
                            const value = poss[a];
                            act.act_pos[value] = true;
                          }
                        }
                      }

                      resAct[_event.bonus_symbol] = act;
                    }
                  }

                  const resActKeys = Object.keys(resAct);
                  let allActPos = {};

                  if (resActKeys) {
                    for (let i = 0; i < resActKeys.length; i++) {
                      const element = resAct[resActKeys[i]];
                      const itemKeys = Object.keys(element.act_pos);

                      for (let j = 0; j < itemKeys.length; j++) {
                        const key = itemKeys[j];
                        allActPos[key] = true;
                      }

                      director.emit('slotWinBoxItemInfo', {
                        symbol: element.bonus_symbol,
                        rate: element.rate,
                        num: itemKeys.length,
                        boxPos: itemKeys[Math.trunc(itemKeys.length / 2)]
                      }); // this.byWayboxResWinAction(element);
                    } // let boxActArr: any[][] = [];


                    const allKeys = Object.keys(allActPos);
                    let xx = 0;

                    for (let i = 0; i < allKeys.length; i++) {
                      const element = allKeys[i];
                      const act = element.split('_');

                      if (act && act.length == 2) {
                        const act1 = parseInt(act[0]);
                        const act2 = parseInt(act[1]); //console.log('消除的格子',act1,act2);

                        if (act1 && act2 || act1 == 0 || act2 == 0) {
                          if (sBoxMgr.instance.getBoxEntityByXY(act1, act2).SymbolValue > 2000) {
                            xx++;
                            this.scheduleOnce(() => {
                              sBoxMgr.instance.updateBoxDataByXY(act1, act2, 'win', 'settlement');
                            }, 0.1 * xx);
                          } else {
                            sBoxMgr.instance.updateBoxDataByXY(act1, act2, 'win', 'settlement');
                          }
                        }
                      }
                    }

                    for (let i = 0; i < sBoxMgr.instance.boxArray.length; i++) {
                      const arr = sBoxMgr.instance.boxArray[i];

                      for (let m = 0; m < arr.length; m++) {
                        if (arr[m].ViewItemIndex.y >= 0 && arr[m].ViewItemIndex.y <= 4) {
                          //console.log('symbolvalue:',sBoxMgr.instance.boxArray,i,m,sBoxMgr.instance.getBoxEntityByXY(i,m),sBoxMgr.instance.getBoxEntityByXY(i,m).SymbolValue);
                          if (arr[m].SymbolValue > 2000) {
                            IsMulit = true;
                            console.log('加倍存在：', IsMulit);
                            break;
                          }
                        }
                      }

                      if (IsMulit) {
                        break;
                      }
                    }

                    director.emit('winBetRes', winRate); //director.emit('betUserInfoUpdateWinAnima',winRate);
                    //this.tatalWin+=winRate;

                    if (round.rate < 200 && !IsMulit) {
                      console.log('小于200：无加倍倍数显示3', winRate);
                      director.emit('betUserInfoUpdateWinAnima', winRate);
                    } else if (!IsMulit) {
                      if (eventIndex != eventArr.length - 1) {
                        this.tatalWin += winRate;
                        director.emit('betUserInfoUpdateWinAnima', winRate);
                      }
                    } else if (IsMulit) {
                      this.tatalWin += winRate;
                      director.emit('betUserInfoUpdateWinAnima', winRate);
                    }

                    if (isWin) {
                      const winSymbolKeys = Object.keys(winSymbol);

                      if (winSymbolKeys && winSymbolKeys.length > 0) {
                        for (let i = 0; i < winSymbolKeys.length; i++) {
                          const winSymbolKey = winSymbolKeys[i];
                        }
                      }

                      sAudioMgr.PlayAudio('small_win');
                      this.scheduleOnce(() => {
                        sAudioMgr.StopAudio();
                      }, 0.6);
                      sAudioMgr.PlayShotAudio('small_win_effect');

                      if (rollSpeedMode == 'trubo') {
                        sAudioMgr.StopAudio();
                        sAudioMgr.PlayShotAudio('small_win_end');
                        this.scheduleOnce(() => {
                          sAudioMgr.PlayShotAudio('symbol_clear' + eventIndex);
                        }, 1);
                      } else {
                        this.scheduleOnce(() => {
                          sAudioMgr.PlayShotAudio('small_win_end');
                          this.scheduleOnce(() => {
                            sAudioMgr.PlayShotAudio('symbol_clear' + eventIndex);
                          }, 1);
                        }, 0.6);
                      }

                      sBoxMgr.instance.blackCurtainAnima();
                      sBoxMgr.instance.blackCurtainAnima(-1, false);
                      this.scheduleOnce(() => {
                        // director.emit('titleSumLightMsg','normal',eventIndex);
                        sConfigMgr.instance.TurnNextResRoundData();
                        sBoxMgr.instance.ReplenishBoxEntity();
                        let conturhua = 0;
                        sBoxMgr.instance.DropAllEntityBoxAfterRemove(strikeBox => {
                          sAudioMgr.PlayShotAudio('normal_dropgrid');

                          if (strikeBox) {
                            if (strikeBox.SymbolValue > 2000) {
                              if (strikeBox.ViewItemIndex.y > 4) {
                                conturhua++;
                                console.log('召唤一道落雷6');
                                strikeBox.DoEnitityAction('thunder');

                                if (conturhua == 1) {
                                  director.emit('controlHuaXianZi', 'NormalIdle');
                                }
                              }
                            }
                          }
                        }, 0.02);
                      }, 2.5);
                    }
                  }
                } else {
                  if (isEnd) {
                    let multiTotal = 0;

                    if (isTotalWin && nowData.opt && nowData.opt.multiData && nowData.opt.multiData.length > 0) {
                      const multiboxes = [];

                      for (let o = 0; o < nowData.opt.multiData.length; o++) {
                        const multiData = nowData.opt.multiData[o];

                        if (multiData && multiData.pos && multiData.pos.length > 0) {
                          multiTotal += multiData.multi;

                          for (let l = 0; l < multiData.pos.length; l++) {
                            const mPos = multiData.pos[l];

                            if (mPos) {
                              const mPosArr = mPos.split('_');

                              if (mPosArr && mPosArr.length == 2) {
                                multiboxes.push(sBoxMgr.instance.getBoxEntityByXY(parseInt(mPosArr[0]), parseInt(mPosArr[1])));
                              }
                            }
                          }
                        }
                      }

                      if (multiboxes.length > 0) {
                        let mulIndex = 0;
                        this.pushOneSchedule(() => {
                          const mBox = multiboxes[mulIndex++];

                          if (mBox) {
                            mBox.UpdateData('win', mulIndex == multiboxes.length ? 'close' : 'open');
                          }
                        }, 0, multiboxes.length, 1.5, 0, true);
                        this.scheduleOnce(() => {
                          if (round.rate > 200) ;else {
                            console.log('小于200：有加倍倍数显示4', round.rate);
                            director.emit('betUserInfoUpdateWinAnima', round.rate - this.tatalWin);
                          }
                          director.emit('byWayWinLightEnd');
                        }, multiboxes.length * 1.5);
                      } else {
                        director.emit('byWayWinLightEnd');
                      }
                    } else {
                      if (eventArr.length > 1) {
                        director.emit('byWayWinLightEnd');
                      }
                    }
                  }
                }
              }
            };

            boxAnimaCall();
            director.on('dropAllBoxFinish', () => {
              // console.log('dropAllBoxFinish');
              boxAnimaCall(); // console.log('isEnd:',isEnd);
            }, this);
          }
        }

        playscatterMp() {
          let num = sUtil.RandomInt(1, 11);

          if (num >= 1 && num < 9) {
            sAudioMgr.PlayShotAudio('scatter_appear_voice' + num);
          }
        }

        playmultMp() {
          let num = sUtil.RandomInt(1, 11);

          if (num >= 0 && num < 10) {
            sAudioMgr.PlayShotAudio('mult_appear_voice' + num);
          }
        }

      }, _temp), _descriptor = _applyDecoratedDescriptor(_class2.prototype, "effectLayer", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _class2)) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/StarlightPrincess_TotalWin.ts", ['./_rollupPluginModLoBabelHelpers.js', 'cc', './sUtil.ts', './sAudioMgr.ts', './UIBase.ts'], function (exports) {
  'use strict';

  var _applyDecoratedDescriptor, _initializerDefineProperty, _defineProperty, cclegacy, Node, Label, sp, _decorator, tween, UIOpacity, director, v3, sUtil, sAudioMgr, UIBase;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _initializerDefineProperty = module.initializerDefineProperty;
      _defineProperty = module.defineProperty;
    }, function (module) {
      cclegacy = module.cclegacy;
      Node = module.Node;
      Label = module.Label;
      sp = module.sp;
      _decorator = module._decorator;
      tween = module.tween;
      UIOpacity = module.UIOpacity;
      director = module.director;
      v3 = module.v3;
    }, function (module) {
      sUtil = module.sUtil;
    }, function (module) {
      sAudioMgr = module.default;
    }, function (module) {
      UIBase = module.UIBase;
    }],
    execute: function () {
      var _dec, _dec2, _dec3, _dec4, _dec5, _dec6, _dec7, _dec8, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _descriptor5, _descriptor6, _descriptor7, _temp;

      cclegacy._RF.push({}, "be9ae2E+HpIFIfONvyWtCdy", "StarlightPrincess_TotalWin", undefined);

      const {
        ccclass,
        property
      } = _decorator;
      let StarlightPrincess_TotalWin = exports('StarlightPrincess_TotalWin', (_dec = ccclass('StarlightPrincess_TotalWin'), _dec2 = property(Node), _dec3 = property(Label), _dec4 = property(Label), _dec5 = property(Node), _dec6 = property(sp.Skeleton), _dec7 = property(sp.Skeleton), _dec8 = property({
        type: [Node]
      }), _dec(_class = (_class2 = (_temp = class StarlightPrincess_TotalWin extends UIBase {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "freeBtn", _descriptor, this);

          _initializerDefineProperty(this, "numLabel", _descriptor2, this);

          _initializerDefineProperty(this, "freenum", _descriptor3, this);

          _initializerDefineProperty(this, "contentNode", _descriptor4, this);

          _initializerDefineProperty(this, "popSpine", _descriptor5, this);

          _initializerDefineProperty(this, "thunderSpine", _descriptor6, this);

          _initializerDefineProperty(this, "LabelNode", _descriptor7, this);

          _defineProperty(this, "touchEnable", true);
        }

        start() {
          this.freeBtn.on('click', () => {
            if (this.touchEnable) {
              sAudioMgr.PlayShotAudio('thunder');
              this.cleanTweenList();
              this.touchEnable = false;
              sAudioMgr.PlayShotAudio('freeWinTransitBtn');
              this.popSpine.setAnimation(0, 'stpr_banner_out', false);
              this.scheduleOnce(() => {
                this.playThunderAnima();
              }, 0.4);

              for (let jindex = 0; jindex < this.LabelNode.length; jindex++) {
                this.LabelNode[jindex].active = true;
                this.pushIDTween(tween(this.LabelNode[jindex].getComponent(UIOpacity)).to(0.4, {
                  opacity: 0
                }).start(), 'LabelNode2');
              }

              this.popSpine.setCompleteListener(() => {
                this.popSpine.node.active = false;
                this.contentNode.active = false;
                this.scheduleOnce(() => {
                  director.emit('freeWinOver');
                  director.emit('rollStop');
                  sAudioMgr.StopBGAudio();
                  sAudioMgr.PlayBG('bgm_mg'); // this.scheduleOnce(()=>{
                  //     this.touchEnable=true;
                  //     director.emit('StarlightTotleViewOpen',800000,13);
                  // },2)
                }, 1);
              });
            }
          }, this);
          director.on('StarlightTotleViewOpen', (coin, num) => {
            let demc = -1;

            if (globalThis.getCoinRate && globalThis.getCoinRate() > 1) {
              demc = 2;
            }

            sAudioMgr.bgMusicOut(0.4, 0);
            this.contentNode.active = true;
            this.freenum.string = num; //this.popSpine.timeScale = 2.5;

            this.popSpine.node.active = true;
            this.popSpine.setAnimation(0, 'stpr_banner_in', false);
            sAudioMgr.PlayShotAudio('freegame_end');
            sAudioMgr.PlayShotAudio('freegame_voice_congratulations'); //this.scheduleOnce(()=>{

            let fs_Node = this.LabelNode[3];
            let fslabel = this.LabelNode[4];

            if (globalThis.GetLanguageType() == 'EN') {
              fslabel.position = v3(-103, -151, 0);
            } else if (globalThis.GetLanguageType() == 'VN') {
              fslabel.position = v3(-164, -154, 0);
            } else if (globalThis.GetLanguageType() == 'PT') {
              fslabel.position = v3(-215, -152, 0);
            } else if (globalThis.GetLanguageType() == 'TH') {
              fslabel.position = v3(-55, -152, 0);
            }

            fslabel.active = true;
            fslabel.getComponent(UIOpacity).opacity = 0; // this.scheduleOnce(()=>{
            // },0.3);

            for (let jindexx = 0; jindexx < this.LabelNode.length; jindexx++) {
              //this.scheduleOnce(()=>{
              //this.pushIDSchedule(()=>{
              this.LabelNode[jindexx].active = true;
              this.LabelNode[jindexx].getComponent(UIOpacity).opacity = 0;

              if (jindexx == 3) {
                this.pushIDTween(tween(this.LabelNode[jindexx]).delay(0.4).call(() => {
                  sAudioMgr.PlayBG('bgm_interlude');
                  sAudioMgr.PlayShotAudio('freegame_frame_open');
                }).to(0.2, {
                  scale: v3(1.2, 1.2, 1.2)
                }).to(0.1, {
                  scale: v3(0.8, 0.8, 0.8)
                }).start(), 'LabelNode3'); //console.log('this.LabelNode',this.LabelNode);

                this.pushIDTween(tween(this.LabelNode[jindexx].getComponent(UIOpacity)).delay(0.6).to(0.2 * jindexx + 0.1, {
                  opacity: 255
                }).start(), 'LabelNode1');
              } else {
                this.pushIDTween(tween(this.LabelNode[jindexx]).delay(0.4).to(0.2, {
                  scale: v3(1.4, 1.4, 1.4)
                }).to(0.1, {
                  scale: v3(1, 1, 1)
                }).start(), 'LabelNode3'); //console.log('this.LabelNode',this.LabelNode);

                this.pushIDTween(tween(this.LabelNode[jindexx].getComponent(UIOpacity)).delay(0.6).to(0.2 * jindexx + 0.1, {
                  opacity: 255
                }).start(), 'LabelNode1');
              } //},0.03*jindexx+0.3,'xx');
              //},0.03*jindexx+0.3);

            }

            this.popSpine.setCompleteListener(() => {
              this.popSpine.timeScale = 1;
              this.popSpine.setAnimation(0, 'stpr_banner_loop', true);
            });
            this.numLabel.string = sUtil.AddCommas(coin, demc);
            this.node.setSiblingIndex(this.node.parent.children.length - 1);
            this.unscheduleAllCallbacks();
            this.cleanTweenList();
            this.scheduleOnce(() => {
              this.freeBtn.emit('click');
            }, 5);
          }, this);
          director.on('freeWinBegain', () => {
            this.touchEnable = true;
          }, this); // this.scheduleOnce(()=>{
          //     director.emit('StarlightTotleViewOpen',800000,13);
          // },2)
        }

        playThunderAnima() {
          if (this.thunderSpine) {
            this.thunderSpine.node.active = true;
            this.thunderSpine.setAnimation(0, 'stpr_transition', false);
            sAudioMgr.PlayShotAudio('freegame_transitions', 1);
            this.thunderSpine.setCompleteListener(() => {
              this.thunderSpine.node.active = false;
            });
          }
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "freeBtn", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "numLabel", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "freenum", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "contentNode", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor5 = _applyDecoratedDescriptor(_class2.prototype, "popSpine", [_dec6], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor6 = _applyDecoratedDescriptor(_class2.prototype, "thunderSpine", [_dec7], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor7 = _applyDecoratedDescriptor(_class2.prototype, "LabelNode", [_dec8], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function () {
          return [];
        }
      })), _class2)) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/StarlightPrincess_WinTipLabel.ts", ['./_rollupPluginModLoBabelHelpers.js', 'cc', './sComponent.ts', './sObjPool.ts'], function (exports) {
  'use strict';

  var _applyDecoratedDescriptor, _initializerDefineProperty, cclegacy, UIOpacity, Label, _decorator, Vec3, tween, v3, sComponent, sObjPool;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _initializerDefineProperty = module.initializerDefineProperty;
    }, function (module) {
      cclegacy = module.cclegacy;
      UIOpacity = module.UIOpacity;
      Label = module.Label;
      _decorator = module._decorator;
      Vec3 = module.Vec3;
      tween = module.tween;
      v3 = module.v3;
    }, function (module) {
      sComponent = module.sComponent;
    }, function (module) {
      sObjPool = module.sObjPool;
    }],
    execute: function () {
      var _dec, _dec2, _dec3, _class, _class2, _descriptor, _descriptor2, _temp;

      cclegacy._RF.push({}, "c07b0tn0WtFJrWicNNwJ/QN", "StarlightPrincess_WinTipLabel", undefined);

      const {
        ccclass,
        property
      } = _decorator;
      let StarlightPrincess_WinTipLabel = exports('StarlightPrincess_WinTipLabel', (_dec = ccclass('StarlightPrincess_WinTipLabel'), _dec2 = property(UIOpacity), _dec3 = property(Label), _dec(_class = (_class2 = (_temp = class StarlightPrincess_WinTipLabel extends sComponent {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "mainOpa", _descriptor, this);

          _initializerDefineProperty(this, "coinLabel", _descriptor2, this);
        }

        start() {// this.scheduleOnce(()=>{
          //     this.play('1.2');
          // },2);
        }

        play(str) {
          this.cleanTweenList();
          this.coinLabel.string = str;
          this.mainOpa.opacity = 0;
          this.node.scale = Vec3.ZERO;
          this.pushOneTween(tween(this.mainOpa).to(0.3, {
            opacity: 255
          }).start());
          this.pushOneTween(tween(this.node).by(0.3, {
            scale: v3(1, 1, 1),
            position: v3(0, 15, 0)
          }).call(() => {
            this.pushOneTween(tween(this.mainOpa).to(1, {
              opacity: 0
            }, {
              easing: 'cubicIn'
            }).start());
            this.pushOneTween(tween(this.node).by(1, {
              scale: v3(-0.6, -0.6, -0.6),
              position: v3(0, 80, 0)
            }).call(() => {
              this.node.active = false;
              sObjPool.Enqueue(this.node.name, this.node);
            }).start());
          }).start());
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "mainOpa", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "coinLabel", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/StarlightPrincess_UIFreeSpinEntry.ts", ['./_rollupPluginModLoBabelHelpers.js', 'cc', './sComponent.ts', './sAudioMgr.ts'], function (exports) {
  'use strict';

  var _applyDecoratedDescriptor, _initializerDefineProperty, _defineProperty, cclegacy, Node, Label, sp, _decorator, tween, UIOpacity, director, v3, sComponent, sAudioMgr;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _initializerDefineProperty = module.initializerDefineProperty;
      _defineProperty = module.defineProperty;
    }, function (module) {
      cclegacy = module.cclegacy;
      Node = module.Node;
      Label = module.Label;
      sp = module.sp;
      _decorator = module._decorator;
      tween = module.tween;
      UIOpacity = module.UIOpacity;
      director = module.director;
      v3 = module.v3;
    }, function (module) {
      sComponent = module.sComponent;
    }, function (module) {
      sAudioMgr = module.default;
    }],
    execute: function () {
      var _dec, _dec2, _dec3, _dec4, _dec5, _dec6, _dec7, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _descriptor5, _descriptor6, _temp;

      cclegacy._RF.push({}, "c2437P5eb5OUa71Dy4YC+E6", "StarlightPrincess_UIFreeSpinEntry", undefined);

      const {
        ccclass,
        property
      } = _decorator;
      let StarlightPrincess_UIFreeSpinEntry = exports('StarlightPrincess_UIFreeSpinEntry', (_dec = ccclass('StarlightPrincess_UIFreeSpinEntry'), _dec2 = property(Node), _dec3 = property(Label), _dec4 = property(Node), _dec5 = property(Node), _dec6 = property(sp.Skeleton), _dec7 = property(sp.Skeleton), _dec(_class = (_class2 = (_temp = class StarlightPrincess_UIFreeSpinEntry extends sComponent {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "freeBtn", _descriptor, this);

          _initializerDefineProperty(this, "numLabel", _descriptor2, this);

          _initializerDefineProperty(this, "contentNode", _descriptor3, this);

          _initializerDefineProperty(this, "LabelNode", _descriptor4, this);

          _initializerDefineProperty(this, "popSpine", _descriptor5, this);

          _initializerDefineProperty(this, "thunderSpine", _descriptor6, this);

          _defineProperty(this, "touchEnable", true);
        }

        start() {
          this.freeBtn.on('click', () => {
            if (this.touchEnable) {
              this.unscheduleAllCallbacks();
              this.cleanTweenList();
              this.touchEnable = false;
              sAudioMgr.PlayShotAudio('thunder');
              sAudioMgr.StopAudio(1);
              this.popSpine.setAnimation(0, 'stpr_banner_out', false);
              this.LabelNode.active = true;

              for (let nNode of this.LabelNode.children) {
                this.pushIDTween(tween(nNode.getComponent(UIOpacity)).delay(0.3).to(0.5, {
                  opacity: 0
                }).start(), 'LabelNode22');
              }

              this.scheduleOnce(() => {
                //this.contentNode.active = false;
                sAudioMgr.StopBGAudio();
                this.playThunderAnima();
              }, 0.2);
              this.popSpine.setCompleteListener(() => {
                this.popSpine.node.active = false;
              });
            }
          }, this);
          director.on('freeWinOver', () => {
            this.touchEnable = true;
          }, this);
          director.on('freeWinBingo', (count, rate) => {
            this.contentNode.active = true; // sAudioMgr.StopBGAudio();
            //    this.scheduleOnce(()=>{
            //     sAudioMgr.PlayShotAudio('freegame_frame_open');
            //     },0.3);

            sAudioMgr.PlayShotAudio('freegame_voice_freespins');
            this.popSpine.node.active = true;
            this.popSpine.setAnimation(0, 'stpr_banner_in', false);
            this.popSpine.setCompleteListener(() => {
              this.popSpine.timeScale = 1;
              this.popSpine.setAnimation(0, 'stpr_banner_loop', true);
              sAudioMgr.PlayAudio('fsinout_bgm');
            });
            this.LabelNode.active = true;
            let x = 0;

            for (let nNode of this.LabelNode.children) {
              nNode.getComponent(UIOpacity).opacity = 0;
              this.pushIDTween(tween(nNode.getComponent(UIOpacity)).delay(0.3).call(() => {
                x++;

                if (x == 1) {
                  sAudioMgr.PlayShotAudio('freegame_frame_open', 0.8);
                }
              }).delay(0.5).to(0.2, {
                opacity: 255
              }).start(), 'LabelNode1');
              this.pushIDTween(tween(nNode).delay(0.8).to(0.2, {
                scale: v3(1.5, 1.5, 1.5)
              }).to(0.2, {
                scale: v3(1, 1, 1)
              }).start(), 'LabelNode2');
            }

            director.emit('freeWinBtnInfo', rate);
            this.setLabelNum(count);
            this.node.setSiblingIndex(this.node.parent.children.length - 1);
            this.unscheduleAllCallbacks();
            this.cleanTweenList();
            this.scheduleOnce(() => {
              this.freeBtn.emit('click');
            }, 3);
          }, this); // this.scheduleOnce(()=>{
          //     director.emit('freeWinBingo',15,50000);
          // },2);

          director.on('bingoFreewinInFree', () => {}, this);
        }

        onEnable() {}

        setLabelNum(num) {
          this.numLabel.string = num.toString();
        }

        playThunderAnima() {
          if (this.thunderSpine) {
            this.thunderSpine.node.active = true;
            this.thunderSpine.setAnimation(0, 'stpr_transition', false);
            sAudioMgr.PlayShotAudio('freegame_transitions', 1);
            this.thunderSpine.setCompleteListener(() => {
              this.thunderSpine.node.active = false;
              this.contentNode.active = false;
              sAudioMgr.PlayShotAudio('freegame_voice_Letgo');
              sAudioMgr.PlayBG('bgm_fs');
              director.emit('freeWinBegain'); // this.scheduleOnce(()=>{
              //     director.emit('freeWinBingo',15,50000);
              // },2);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              1   
            });
          }
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "freeBtn", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "numLabel", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "contentNode", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "LabelNode", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor5 = _applyDecoratedDescriptor(_class2.prototype, "popSpine", [_dec6], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor6 = _applyDecoratedDescriptor(_class2.prototype, "thunderSpine", [_dec7], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/StarlightPrincess_BoxItem.ts", ['./_rollupPluginModLoBabelHelpers.js', 'cc', './sUtil.ts', './sConfigMgr.ts', './sObjPool.ts', './sBoxItemBase.ts', './sAssetMgr.ts'], function (exports) {
  'use strict';

  var _applyDecoratedDescriptor, _initializerDefineProperty, _defineProperty, cclegacy, Sprite, UITransform, Label, Node, _decorator, director, color, sp, v3, Color, sUtil, sConfigMgr, sObjPool, sBoxItemBase, assetMgr;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _initializerDefineProperty = module.initializerDefineProperty;
      _defineProperty = module.defineProperty;
    }, function (module) {
      cclegacy = module.cclegacy;
      Sprite = module.Sprite;
      UITransform = module.UITransform;
      Label = module.Label;
      Node = module.Node;
      _decorator = module._decorator;
      director = module.director;
      color = module.color;
      sp = module.sp;
      v3 = module.v3;
      Color = module.Color;
    }, function (module) {
      sUtil = module.sUtil;
    }, function (module) {
      sConfigMgr = module.sConfigMgr;
    }, function (module) {
      sObjPool = module.sObjPool;
    }, function (module) {
      sBoxItemBase = module.sBoxItemBase;
    }, function (module) {
      assetMgr = module.assetMgr;
    }],
    execute: function () {
      var _dec, _dec2, _dec3, _dec4, _dec5, _dec6, _dec7, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _descriptor5, _descriptor6, _temp;

      cclegacy._RF.push({}, "d98c4K2k8NEjZuFK+++V/Yk", "StarlightPrincess_BoxItem", undefined);

      const {
        ccclass,
        property
      } = _decorator;
      let StarlightPrincess_BoxItem = exports('StarlightPrincess_BoxItem', (_dec = ccclass('StarlightPrincess_BoxItem'), _dec2 = property(Sprite), _dec3 = property(UITransform), _dec4 = property(Label), _dec5 = property(Node), _dec6 = property(Node), _dec7 = property(Node), _dec(_class = (_class2 = (_temp = class StarlightPrincess_BoxItem extends sBoxItemBase {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "renderSprite", _descriptor, this);

          _initializerDefineProperty(this, "renderTransform", _descriptor2, this);

          _initializerDefineProperty(this, "multiLabel", _descriptor3, this);

          _initializerDefineProperty(this, "SpineLayer", _descriptor4, this);

          _initializerDefineProperty(this, "winFrame", _descriptor5, this);

          _initializerDefineProperty(this, "disappearAnimation", _descriptor6, this);

          _defineProperty(this, "skeleton", void 0);

          _defineProperty(this, "isThundered", false);

          _defineProperty(this, "isMultied", false);
        }

        boxItemUpdate(renderData, symbolValue, boxState, tableState) {
          if (renderData) {
            if (renderData.type == 'img') {
              this.imgRenderSet(boxState, symbolValue, renderData, tableState);
            } else if (renderData.type == 'spine') {
              this.spineRenderSet(boxState, symbolValue, renderData, tableState);
            }

            if (symbolValue > 2000) {
              if (renderData.multi && !this.isMultied) {
                this.multiLabel.node.active = true;
                this.SetNodeFront(this.multiLabel.node);
                this.multiLabel.string = renderData.multi + 'x'; //let pos=this.multiLabel.node.position;
                //this.multiLabel.node.setPosition(pos.x,pos.y+2,pos.z);
              }
            } else {
              this.isMultied = false;
            }

            if (boxState == 'win') {
              if (symbolValue > 2000) {
                this.isMultied = true;
                director.emit('winMultiBox', {
                  actionType: tableState,
                  pos: this.multiLabel.node.worldPosition,
                  multi: renderData.multi,
                  fontSize: this.multiLabel.fontSize
                });
                this.multiLabel.node.active = false;
              } else {
                this.isMultied = false;

                if (symbolValue != 1) {
                  this.winFrame.active = true;
                  this.winFrame.getComponent(Sprite).color = color(255, 255, 255, 0);
                  sUtil.TweenColor(color(255, 255, 255, 255), this.winFrame.getComponent(Sprite), 0.2, 0);
                  this.scheduleOnce(() => {
                    this.winFrame.active = false;
                    this.scheduleOnce(() => {
                      this.disappearAnimation.active = true;
                      this.disappearAnimation;
                      this.disappearAnimation.getComponent(sp.Skeleton).setAnimation(0, "stpr_symbol_all_fx_disappearance", false);
                      this.disappearAnimation.getComponent(sp.Skeleton).setCompleteListener(() => {
                        this.disappearAnimation.active = false;
                      });
                      const boxDatab = this.getSymbolData(symbolValue);
                      renderData = boxDatab['out'];
                      this.skeleton.timeScale = 2.5;
                      this.skeleton.setAnimation(0, renderData.animation_name, false);
                      this.skeleton.setCompleteListener(() => {
                        this.skeleton.node.active = false;
                      });
                    }, 0.2);
                  }, 1.4); // this.scheduleOnce(()=>{
                  // },0.2);
                }
              }
            } else {
              this.winFrame.active = false;
            }
          } //     if(boxState == 'rolling'){
          //         this.isMultied = false;
          //     }
          // }

        }

        imgRenderSet(boxState, symbolValue, renderData, tableState) {
          const spriteFrame = assetMgr.GetAssetByName(renderData.render_name);
          this.renderSprite.node.active = true;
          this.renderSprite.spriteFrame = spriteFrame; // renderSize = size(spriteFrame.rect.width,spriteFrame.rect.height);
          // console.log('spriteFrame:'+spriteFrame.name +','+renderSize.x+','+renderSize.y);

          if (renderData.position) {
            this.renderSprite.node.position = v3(renderData.position.x, renderData.position.y, 0);
          }

          if (renderData.contentSize) ;

          if (renderData.scale) {
            this.renderSprite.node.scale = v3(renderData.scale, renderData.scale, renderData.scale);
          }
        }

        spineRenderSet(rollState, symbolValue, renderData, tableState) {
          if (this.skeleton) {
            this.skeleton.node.active = false;
            sObjPool.Enqueue(renderData.render_name, this.skeleton);
            this.skeleton = null;
          }

          this.skeleton = sObjPool.Dequeue(renderData.render_name);

          if (this.skeleton) {
            this.skeleton.node.setParent(this.SpineLayer, false);

            if (renderData.loop && renderData.loop == -1) {
              this.skeleton.loop = true;
            } else {
              this.skeleton.loop = false;
            }

            if (symbolValue > 2000 && rollState == 'win') {
              this.skeleton.getComponent(sp.Skeleton).timeScale = 0.8;
            } else {
              this.skeleton.getComponent(sp.Skeleton).timeScale = 1;
            }

            if (this.skeleton.animation == 'out') ;

            if (renderData.position) {
              this.skeleton.node.position = v3(renderData.position.x, renderData.position.y, 0);
            } //     if(this.skeleton.node.scale.x != renderData.scale){
            //         this.skeleton.node.scale = v3(renderData.scale,renderData.scale,1);
            //     }
            // }else{
            //     if(this.skeleton.node.scale.x != 1){
            //         this.skeleton.node.scale = v3(1,1,1);
            //     }
            // }


            this.skeleton.node.active = true; // if(renderData.timeScale){
            //     this.skeleton.timeScale = renderData.timeScale;
            // };

            this.skeleton.animation = renderData.animation_name;

            if (symbolValue <= 2000 && symbolValue != 1) {
              if (this.skeleton.animation == 'out') {
                this.skeleton.setCompleteListener(() => {
                  this.skeleton.node.active = false;
                });
              }
            }
          }

          this.renderSprite.node.active = false;
        }

        clearItem(symbolValue, rollState, tableState) {
          this.cleanTweenList();
          this.multiLabel.node.active = false;
          this.renderSprite.color = Color.WHITE;

          if (this.skeleton) {
            this.skeleton.setCompleteListener(null);
            this.skeleton.node.active = false;
            sObjPool.Enqueue(this.skeleton.node.name, this.skeleton);
            this.skeleton = null;
          }
        }

        getSymbolData(symbolValue) {
          try {
            if (isNaN(symbolValue)) {
              return sConfigMgr.instance.GetSymbolBoxImg(symbolValue.OLD);
            } else {
              return sConfigMgr.instance.GetSymbolBoxImg(symbolValue);
            }

            ;
          } catch (e) {
            console.error('getSymbolData error' + e);
          }
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "renderSprite", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "renderTransform", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "multiLabel", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "SpineLayer", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor5 = _applyDecoratedDescriptor(_class2.prototype, "winFrame", [_dec6], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor6 = _applyDecoratedDescriptor(_class2.prototype, "disappearAnimation", [_dec7], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/StarlightPrincess_Thunder.ts", ['./_rollupPluginModLoBabelHelpers.js', 'cc', './sComponent.ts', './sAudioMgr.ts', './sObjPool.ts'], function (exports) {
  'use strict';

  var _applyDecoratedDescriptor, _initializerDefineProperty, cclegacy, sp, _decorator, sComponent, sAudioMgr, sObjPool;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _initializerDefineProperty = module.initializerDefineProperty;
    }, function (module) {
      cclegacy = module.cclegacy;
      sp = module.sp;
      _decorator = module._decorator;
    }, function (module) {
      sComponent = module.sComponent;
    }, function (module) {
      sAudioMgr = module.default;
    }, function (module) {
      sObjPool = module.sObjPool;
    }],
    execute: function () {
      var _dec, _dec2, _class, _class2, _descriptor, _temp;

      cclegacy._RF.push({}, "e97b3ACxzhGd5K70oJ8Xk5s", "StarlightPrincess_Thunder", undefined);

      const {
        ccclass,
        property
      } = _decorator;
      let StarlightPrincess_Thunder = exports('StarlightPrincess_Thunder', (_dec = ccclass('StarlightPrincess_Thunder'), _dec2 = property(sp.Skeleton), _dec(_class = (_class2 = (_temp = class StarlightPrincess_Thunder extends sComponent {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "thunder", _descriptor, this);
        }

        start() {}

        play(animaName) {
          if (this.thunder && animaName) {
            sAudioMgr.PlayShotAudio('multiStop');
            this.thunder.setAnimation(0, animaName, false);
            this.thunder.setCompleteListener(() => {
              sObjPool.Enqueue(this.node.name, this.node);
              this.node.active = false;
            });
          }
        }

      }, _temp), _descriptor = _applyDecoratedDescriptor(_class2.prototype, "thunder", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _class2)) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/StarlightPrincess_FreewinFlow.ts", ['./_rollupPluginModLoBabelHelpers.js', 'cc', './sUtil.ts', './sConfigMgr.ts', './sAudioMgr.ts', './sBoxMgr.ts', './sFreeWinFlow.ts'], function (exports) {
  'use strict';

  var _applyDecoratedDescriptor, _initializerDefineProperty, _defineProperty, cclegacy, Node, Label, _decorator, director, tween, v3, Vec3, sUtil, sConfigMgr, sAudioMgr, sBoxMgr, sFreeWinFlow;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _initializerDefineProperty = module.initializerDefineProperty;
      _defineProperty = module.defineProperty;
    }, function (module) {
      cclegacy = module.cclegacy;
      Node = module.Node;
      Label = module.Label;
      _decorator = module._decorator;
      director = module.director;
      tween = module.tween;
      v3 = module.v3;
      Vec3 = module.Vec3;
    }, function (module) {
      sUtil = module.sUtil;
    }, function (module) {
      sConfigMgr = module.sConfigMgr;
    }, function (module) {
      sAudioMgr = module.default;
    }, function (module) {
      sBoxMgr = module.sBoxMgr;
    }, function (module) {
      sFreeWinFlow = module.sFreeWinFlow;
    }],
    execute: function () {
      var _dec, _dec2, _dec3, _dec4, _class, _class2, _descriptor, _descriptor2, _descriptor3, _temp;

      cclegacy._RF.push({}, "fdc726Gwu9Kd7Gs4brByQRX", "StarlightPrincess_FreewinFlow", undefined);

      const {
        ccclass,
        property
      } = _decorator;
      let StarlightPrincess_FreewinFlow = exports('StarlightPrincess_FreewinFlow', (_dec = ccclass('StarlightPrincess_FreewinFlow'), _dec2 = property(Node), _dec3 = property(Label), _dec4 = property(Label), _dec(_class = (_class2 = (_temp = class StarlightPrincess_FreewinFlow extends sFreeWinFlow {
        constructor(...args) {
          super(...args);

          _initializerDefineProperty(this, "effectLayer", _descriptor, this);

          _initializerDefineProperty(this, "multiLabel", _descriptor2, this);

          _initializerDefineProperty(this, "leftFreeCountLabel", _descriptor3, this);

          _defineProperty(this, "nowMode", 'normal');

          _defineProperty(this, "multiValue", 0);

          _defineProperty(this, "freeTotalRate", 0);

          _defineProperty(this, "symbolToMulti", {
            2001: 2,
            2002: 3,
            2003: 4,
            2004: 5,
            2005: 6,
            2006: 8,
            2007: 10,
            2008: 12,
            2009: 15,
            2010: 20,
            2011: 25,
            2012: 50,
            2013: 100,
            2014: 250,
            2015: 500
          });

          _defineProperty(this, "tatalWin", 0);
        }

        start() {
          super.start();
          director.on('freeWinBegain', () => {
            this.multiLabel.string = '';
            this.multiValue = 0;
          }, this);
          director.on('freeWinOver', () => {
            this.multiLabel.string = '';
            this.multiValue = 0;
          }, this);
          director.on('StarlightPrincessLabelEnd', () => {
            this.multiLabel.string = this.multiValue + 'x';
            this.removeIDTween('multiLabel');
            this.pushIDTween(tween(this.multiLabel.node).to(0.15, {
              scale: v3(1.4, 1.4, 1.4)
            }, {
              easing: 'sineOut'
            }).to(0.1, {
              scale: Vec3.ONE
            }, {
              easing: 'sineIn'
            }).start(), 'multiLabel');
          }, this);
          director.on('freeWinAction', value => {
            this.leftFreeCountLabel.string = (value < 0 ? 0 : value).toString();
          }, this);
          this.scheduleOnce(() => {//director.emit('StarlightTotleViewOpen',2000);
          }, 2);
        }

        clearUI() {
          super.clearUI();
          this.removeScheduleDic();
        }

        rollFirstRoundAction(round, speedMode, clickMode) {
          director.off('rollOneColumnBump');
          director.off('byWayWinLightEnd');
          director.off('titleWinSettleEnd');
          director.off('rollStopOneRoundCall');
          this.freeWinIndex = 0;
          this.freeTotalRate = 0;
          this.tatalWin = 0;

          if (round) {
            let timestop = 0;
            const round_symbol_map = round['round_symbol_map'];

            for (let xx of round_symbol_map[0]) {
              for (let yy = 0; yy < xx.length; yy++) {
                //console.log('symbel:',xx[yy]);
                if (yy >= 1 && yy <= 5 && xx[yy] > 2000) {
                  timestop = 0;
                  director.emit('controlHuaXianZi', 'NormalIdle');
                  break;
                }
              }

              if (timestop > 0) break;
            }

            this.normalFlowDelayCall(() => {
              this.scheduleOnce(() => {
                let needWinLight = round.rate > 0 ? true : false;
                const round_symbol_map = round['round_symbol_map'];
                sConfigMgr.instance.SetResData(round_symbol_map);
                const events = sConfigMgr.instance.GetAllEventConfigByHitListOnCheck(round);

                if (events) {
                  const lastEvents = events[events.length - 1];

                  for (let i = 0; i < lastEvents.length; i++) {
                    const _event = lastEvents[i];

                    if (_event.event_type == 'changeRound') {
                      if (_event['round_change']) {
                        this.freeWinCount = _event['round_change'].num;
                      }

                      this.leftFreeCountLabel.string = (this.freeWinCount - this.freeWinIndex).toString();
                    }
                  }
                }

                director.on('rollOneColumnBump', col => {
                  //sAudioMgr.PlayShotAudio('luodi',0.4);
                  const colBoxs = sBoxMgr.instance.GetColAllBox(col);
                  let shunxu = 0;
                  let shunxu1 = 0;

                  if (colBoxs) {
                    for (let rY = 0; rY < colBoxs.length; rY++) {
                      const rSymbol = colBoxs[rY];

                      if (rSymbol.SymbolValue > 2000) {
                        if (rSymbol.ViewItemIndex.y >= 0 && rSymbol.ViewItemIndex.y <= 4) {
                          rSymbol.DoEnitityAction('thunder');
                          console.log('召唤一道落雷1');

                          if (rSymbol.SymbolValue == 1) {
                            shunxu1++;

                            if (shunxu1 == 1) {
                              director.emit('controlHuaXianZi', 'NormalIdle');
                              this.playmultMp();
                            }
                          }
                        }
                      } else if (rSymbol.SymbolValue == 1) {
                        shunxu++;
                        sAudioMgr.PlayShotAudio('scatter_appear' + shunxu);

                        if (shunxu == 1) {
                          this.playscatterMp();
                        }
                      }
                    }
                  }
                }, this);
                sUtil.once('byWayWinLightEnd', () => {
                  sUtil.once('titleWinSettleEnd', () => {
                    const rate = round.rate;
                    this.freeTotalRate += rate;

                    if (rate > 200) {
                      this.scheduleOnce(() => {
                        director.emit('StarlightPrincessBigWin', rate);
                        console.log('大于200：无加倍倍数显示2', rate);
                        director.emit('betUserInfoUpdateWinAnima', rate - this.tatalWin);
                      }, 0.8);
                      sUtil.once('StarlightPrincessBigWinEnd', () => {
                        sBoxMgr.instance.setAllViewTargetBoxArrayState(id => id == 1, 'win', 'settlement');
                        sAudioMgr.PlayShotAudio('scatter_anima', 0.8);
                        sAudioMgr.bgMusicOut(0.3, 0);
                        this.scheduleOnce(() => {
                          sAudioMgr.PlayBG('bgm_interlude');
                          director.emit('freeWinBingo', this.freeWinCount, round.rate);
                        }, 3);
                      });
                    } else {
                      //director.emit('betUserInfoUpdateWinAnima',this.freeTotalRate);
                      sBoxMgr.instance.setAllViewTargetBoxArrayState(id => id == 1, 'win', 'settlement');
                      sAudioMgr.PlayShotAudio('scatter_anima', 0.8);
                      sAudioMgr.bgMusicOut(0.3, 0);
                      this.scheduleOnce(() => {
                        sAudioMgr.PlayBG('bgm_interlude');
                        director.emit('freeWinBingo', this.freeWinCount, round.rate);
                      }, 3);
                    }
                  });
                  director.emit('winBetRes', -1);
                });
                sUtil.once('rollStopOneRoundCall', () => {
                  director.off('rollOneColumnStop');
                  director.off('rollOneColumnBump');

                  if (needWinLight) {
                    this.byWayBoxLightCtr(round, 'normal');
                  } else {
                    sBoxMgr.instance.setAllViewTargetBoxArrayState(id => id == 1, 'win', 'settlement');
                    sAudioMgr.PlayShotAudio('scatter_anima', 0.8);
                    sAudioMgr.bgMusicOut(0.3, 0);
                    this.scheduleOnce(() => {
                      sAudioMgr.PlayBG('bgm_interlude');
                      director.emit('freeWinBingo', this.freeWinCount, 0);
                    }, 3);
                  }
                });

                if (speedMode == 'turbo') {
                  sBoxMgr.instance.rollStopAllColumn();
                } else if (speedMode == 'normal') {
                  sBoxMgr.instance.rollStopAllColumn();
                }
              }, timestop);
            });
          }
        }

        rollStopAction(round, rollSpeedMode, clickMode) {
          this.nowMode = 'freeWin';
          director.off('rollOneColumnBump');
          director.off('byWayWinLightEnd');
          director.off('titleWinSettleEnd');
          director.off('rollStopOneRoundCall');

          if (round) {
            let timestop = 0;
            const round_symbol_map = round['round_symbol_map'];

            for (let xx of round_symbol_map[0]) {
              for (let yy = 0; yy < xx.length; yy++) {
                //console.log('symbel:',xx[yy]);
                if (yy >= 1 && yy <= 5 && xx[yy] > 2000) {
                  timestop = 0;
                  director.emit('controlHuaXianZi', 'NormalIdle');
                  break;
                }
              }

              if (timestop > 0) break;
            }

            this.normalFlowDelayCall(() => {
              this.scheduleOnce(() => {
                this.tatalWin = 0;
                let needWinLight = round.rate > 0 ? true : false;
                const round_symbol_map = round['round_symbol_map'];
                sConfigMgr.instance.SetResData(round_symbol_map); // const events = sConfigMgr.instance.GetAllEventConfigByHitListOnCheck(round);
                // if(events){
                //     const lastEvents = events[events.length - 1];
                //     for (let i = 0; i < lastEvents.length; i++) {
                //         const _event = lastEvents[i];
                //         if (_event.event_type == 'changeRound') {
                //             this.freeWinCount = _event['round_change'].num;
                //         }
                //     }
                // }

                const allEvents = sConfigMgr.instance.GetAllEventConfigByHitListOnCheck(round);
                let freeCount = 0;

                if (allEvents && allEvents.length > 0) {
                  for (let e = 0; e < allEvents.length; e++) {
                    const eventArr = allEvents[e];

                    for (let f = 0; f < eventArr.length; f++) {
                      const _fEvent = eventArr[f];

                      if (e == allEvents.length - 1) {
                        if (_fEvent.event_type == 'changeRound') {
                          if (_fEvent['round_change']) {
                            freeCount = _fEvent['round_change'].num;
                          }
                        }
                      } //     needWinLight = true;
                      // }

                    }
                  }
                }

                director.on('rollOneColumnBump', col => {
                  sAudioMgr.PlayShotAudio('luodi', 0.4);
                  const colBoxs = sBoxMgr.instance.GetColAllBox(col);
                  let shunxu = 0;
                  let shunxu1 = 0;

                  if (colBoxs) {
                    for (let rY = 0; rY < colBoxs.length; rY++) {
                      const rSymbol = colBoxs[rY];

                      if (rSymbol.SymbolValue > 2000) {
                        if (rSymbol.ViewItemIndex.y >= 0 && rSymbol.ViewItemIndex.y <= 4) {
                          rSymbol.DoEnitityAction('thunder');
                          console.log('召唤一道落雷2');

                          if (rSymbol.SymbolValue == 1) {
                            shunxu1++;

                            if (shunxu1 == 1) {
                              director.emit('controlHuaXianZi', 'NormalIdle');
                              this.playmultMp();
                            }
                          }
                        }
                      } else if (rSymbol.SymbolValue == 1) {
                        shunxu++;
                        sAudioMgr.PlayShotAudio('scatter_appear' + shunxu);

                        if (shunxu == 1) {
                          this.playscatterMp();
                        }
                      }
                    }
                  }
                }, this);
                sUtil.once('byWayWinLightEnd', () => {
                  sUtil.once('titleWinSettleEnd', () => {
                    const rate = round.rate;
                    this.freeTotalRate += rate;

                    if (rate >= 200) {
                      this.scheduleOnce(() => {
                        director.emit('StarlightPrincessBigWin', rate);
                        console.log('大于200：无加倍倍数显示2', rate);
                        director.emit('betUserInfoUpdateWinAnima', rate - this.tatalWin);
                      }, 0.8);
                      sUtil.once('StarlightPrincessBigWinEnd', () => {
                        if (freeCount > 0) {
                          sUtil.once('bingoFreewinInFreeEnd', () => {
                            this.freeWinCount += freeCount;
                            this.leftFreeCountLabel.string = (this.freeWinCount - this.freeWinIndex).toString();
                            this.scheduleOnce(() => {
                              //console.log('免费游戏round结束freeCount1')
                              director.emit('freeWinOneRoundEnd');
                            }, 0.5);
                          });
                          sAudioMgr.PlayShotAudio('scatter_anima');
                          sBoxMgr.instance.setAllViewTargetBoxArrayState(id => id == 1, 'win', 'settlement');
                          this.scheduleOnce(() => {
                            director.emit('bingoFreewinInFree', freeCount);
                          }, 2.5);
                        } else {
                          this.scheduleOnce(() => {
                            //console.log('免费游戏round结束freeCount2')
                            director.emit('freeWinOneRoundEnd');
                          }, 0.5);
                        }
                      });
                    } else {
                      //director.emit('betUserInfoUpdateWinAnima',this.freeTotalRate);
                      if (freeCount > 0) {
                        sUtil.once('bingoFreewinInFreeEnd', () => {
                          this.freeWinCount += freeCount;
                          this.leftFreeCountLabel.string = (this.freeWinCount - this.freeWinIndex).toString();
                          this.scheduleOnce(() => {
                            console.log('免费游戏round结束freeCount1');
                            director.emit('freeWinOneRoundEnd');
                          }, 0.5);
                        });
                        sAudioMgr.PlayShotAudio('scatter_anima');
                        sBoxMgr.instance.setAllViewTargetBoxArrayState(id => id == 1, 'win', 'settlement');
                        this.scheduleOnce(() => {
                          director.emit('bingoFreewinInFree', freeCount);
                        }, 2.5);
                      } else {
                        this.scheduleOnce(() => {
                          console.log('免费游戏round结束freeCount2');
                          director.emit('freeWinOneRoundEnd');
                        }, 0.5);
                      }
                    }
                  });
                  director.emit('winBetRes', -1);
                });
                sUtil.once('rollStopOneRoundCall', () => {
                  director.off('rollOneColumnStop');
                  director.off('rollOneColumnBump');

                  if (needWinLight) {
                    this.byWayBoxLightCtr(round, 'normal', false);
                  } else {
                    if (freeCount > 0) {
                      sUtil.once('bingoFreewinInFreeEnd', () => {
                        this.freeWinCount += freeCount;
                        this.leftFreeCountLabel.string = (this.freeWinCount - this.freeWinIndex).toString();
                        this.scheduleOnce(() => {
                          console.log('免费游戏round结束3');
                          director.emit('freeWinOneRoundEnd');
                        }, 0.5);
                      });
                      sAudioMgr.PlayShotAudio('scatter_anima');
                      sBoxMgr.instance.setAllViewTargetBoxArrayState(id => id == 1, 'win', 'settlement');
                      this.scheduleOnce(() => {
                        director.emit('bingoFreewinInFree', freeCount);
                      }, 2.5);
                    } else {
                      this.scheduleOnce(() => {
                        console.log('免费游戏round结束4');
                        director.emit('freeWinOneRoundEnd');
                      }, 0.5);
                    }
                  }
                });
                sBoxMgr.instance.rollStopAllColumn();
              }, timestop);
            });
          }
        }

        byWayBoxLightCtr(round, rollSpeedMode, isFirst = true) {
          director.off('dropAllBoxFinish');
          const eventArr = sConfigMgr.instance.GetAllEventOptConfigByHitListOnCheck(round); // console.log('eventArr:'+JSON.stringify(eventArr));

          const boxSum = sBoxMgr.instance.boxSum;
          let isTotalWin = false;
          let isEnd = false;
          let IsMulit = false;

          if (eventArr && Array.isArray(eventArr)) {
            let eventIndex = 0;

            const boxAnimaCall = () => {
              if (eventIndex < eventArr.length) {
                isEnd = eventIndex == eventArr.length - 1;
                const nowData = eventArr[eventIndex++];

                if (nowData && nowData.eventData && nowData.eventData.length > 0) {
                  let isWin = false;
                  const events = nowData.eventData;
                  let resAct = {};
                  let winSymbol = {};
                  let winRate = 0;

                  for (let i = 0; i < events.length; i++) {
                    const _event = events[i];

                    if (_event.event_type == 'boxAnima') {
                      if (_event.rate_change && _event.rate_change.num) {
                        winRate += _event.rate_change.num;
                      }

                      if (_event.bonus_symbol) {
                        winSymbol[_event.bonus_symbol] = true;
                      }

                      isWin = true;
                      isTotalWin = true;
                      let act = resAct[_event.bonus_symbol];

                      if (!act) {
                        act = {};
                        act.bonus_symbol = _event.bonus_symbol;
                        act.act_pos = {};
                        act.rate = 0;
                      }

                      if (_event.rate_change) {
                        if (act.rate == 0) {
                          act.rate = _event.rate_change.num;
                        } else {
                          if (_event.rate_change.type == 0) {
                            act.rate += _event.rate_change.num;
                          } else if (_event.rate_change.type == 1) {
                            act.rate *= _event.rate_change.num;
                          }
                        }
                      }

                      if (_event.act_pos) {
                        const poss = _event['act_pos'].split(',');

                        if (poss) {
                          for (let a = 0; a < poss.length; a++) {
                            const value = poss[a];
                            act.act_pos[value] = true;
                          }
                        }
                      }

                      resAct[_event.bonus_symbol] = act;
                    } else if (_event.event_type == 'changeRound') {
                      //isWin = true;
                      if (_event['rate_change']) {
                        isTotalWin = true;
                        winRate += _event['rate_change'].num;
                      }
                    }
                  }

                  const resActKeys = Object.keys(resAct);
                  let allActPos = {};

                  if (resActKeys) {
                    for (let i = 0; i < resActKeys.length; i++) {
                      const element = resAct[resActKeys[i]];
                      const itemKeys = Object.keys(element.act_pos);
                      director.emit('ws', {
                        symbol: element.bonus_symbol,
                        rate: element.rate,
                        num: itemKeys.length,
                        boxPos: itemKeys[Math.trunc(itemKeys.length / 2)]
                      });

                      for (let j = 0; j < itemKeys.length; j++) {
                        const key = itemKeys[j];
                        allActPos[key] = true;
                      }

                      director.emit('slotWinBoxItemInfo', {
                        symbol: element.bonus_symbol,
                        rate: element.rate,
                        num: itemKeys.length,
                        boxPos: itemKeys[Math.trunc(itemKeys.length / 2)]
                      }); // this.byWayboxResWinAction(element);
                    }

                    for (let i = 0; i < sBoxMgr.instance.boxArray.length; i++) {
                      const arr = sBoxMgr.instance.boxArray[i];

                      for (let m = 0; m < arr.length; m++) {
                        if (arr[m].ViewItemIndex.y >= 0 && arr[m].ViewItemIndex.y <= 4) {
                          //console.log('symbolvalue:',sBoxMgr.instance.boxArray,i,m,sBoxMgr.instance.getBoxEntityByXY(i,m),sBoxMgr.instance.getBoxEntityByXY(i,m).SymbolValue);
                          if (arr[m].SymbolValue > 2000) {
                            IsMulit = true;
                            console.log('加倍存在：', IsMulit);
                            break;
                          }
                        }
                      }

                      if (IsMulit) {
                        break;
                      }
                    }

                    const allKeys = Object.keys(allActPos);
                    let xx = 0;

                    for (let i = 0; i < allKeys.length; i++) {
                      const element = allKeys[i];
                      const act = element.split('_');

                      if (act && act.length == 2) {
                        const act1 = parseInt(act[0]);
                        const act2 = parseInt(act[1]); //console.log('消除的格子',act1,act2);

                        if (act1 && act2 || act1 == 0 || act2 == 0) {
                          if (sBoxMgr.instance.getBoxEntityByXY(act1, act2).SymbolValue > 2000) {
                            xx++;
                            this.scheduleOnce(() => {
                              console.log('星星图案mult', act1, act2);
                              sBoxMgr.instance.updateBoxDataByXY(act1, act2, 'win', 'settlement');
                            }, 0.1 * xx);
                          } else {
                            sBoxMgr.instance.updateBoxDataByXY(act1, act2, 'win', 'settlement');
                          }
                        }
                      }
                    }

                    director.emit('winBetRes', winRate);

                    if (round.rate < 200 && !IsMulit) {
                      console.log('小于200：无加倍倍数显示3', winRate);
                      director.emit('betUserInfoUpdateWinAnima', winRate);
                    } else if (!IsMulit) {
                      if (eventIndex != eventArr.length - 1) {
                        this.tatalWin += winRate;
                        director.emit('betUserInfoUpdateWinAnima', winRate);
                      }
                    } else if (IsMulit) {
                      this.tatalWin += winRate;
                      director.emit('betUserInfoUpdateWinAnima', winRate);
                    }

                    if (isWin) {
                      const winSymbolKeys = Object.keys(winSymbol);

                      if (winSymbolKeys && winSymbolKeys.length > 0) {
                        for (let i = 0; i < winSymbolKeys.length; i++) {
                          const winSymbolKey = winSymbolKeys[i];
                        }
                      } //     sAudioMgr.PlayShotAudio('boxWinTurnRound');
                      // },1);
                      // director.emit('winBetRateRes',winRate * this.getMultiple(eventIndex - 1));


                      sBoxMgr.instance.blackCurtainAnima();
                      sAudioMgr.PlayAudio('freegame_small_win'); //sAudioMgr.PlayShotAudio('small_win_effect');

                      this.scheduleOnce(() => {
                        sAudioMgr.StopAudio();
                        sAudioMgr.PlayShotAudio('freegame_small_win_end' + eventIndex);
                        this.scheduleOnce(() => {
                          sAudioMgr.PlayShotAudio('symbol_clear' + eventIndex);
                        }, 1);
                      }, 0.6);
                      sBoxMgr.instance.blackCurtainAnima(-1, false); //}, 0.5);

                      this.scheduleOnce(() => {
                        // director.emit('titleSumLightMsg','normal',eventIndex);
                        sConfigMgr.instance.TurnNextResRoundData();
                        sBoxMgr.instance.ReplenishBoxEntity();
                        let xx = 0;
                        sBoxMgr.instance.DropAllEntityBoxAfterRemove(strikeBox => {
                          sAudioMgr.PlayShotAudio('boxDropStrike');

                          if (strikeBox) {
                            if (strikeBox.SymbolValue > 2000) {
                              if (strikeBox.ViewItemIndex.y > 4) {
                                xx++;
                                strikeBox.DoEnitityAction('thunder');
                                console.log('召唤一道落雷3');

                                if (xx == 1) {
                                  director.emit('controlHuaXianZi', 'NormalIdle');
                                }
                              }
                            }
                          }
                        }, 0.02);
                      }, 2.5);
                    }
                  }
                }

                if (isEnd) {
                  if (isTotalWin && nowData.opt && nowData.opt.multiData && nowData.opt.multiData.length > 0) {
                    let multiValue = this.multiValue;
                    const multiboxes = [];

                    for (let o = 0; o < nowData.opt.multiData.length; o++) {
                      const multiData = nowData.opt.multiData[o];

                      if (multiData.multi > 0) {
                        multiValue += multiData.multi;
                      }

                      if (multiData && multiData.pos && multiData.pos.length > 0) {
                        for (let l = 0; l < multiData.pos.length; l++) {
                          const mPos = multiData.pos[l];

                          if (mPos) {
                            const mPosArr = mPos.split('_');

                            if (mPosArr && mPosArr.length == 2) {
                              multiboxes.push(sBoxMgr.instance.getBoxEntityByXY(parseInt(mPosArr[0]), parseInt(mPosArr[1])));
                            }
                          }
                        }
                      }
                    }

                    if (multiboxes.length > 0) {
                      const mulCall = () => {
                        let mulIndex = 0;
                        this.pushOneSchedule(() => {
                          const mBox = multiboxes[mulIndex++];

                          if (mBox) {
                            mBox.UpdateData('win', mulIndex == multiboxes.length ? 'close' : 'open');
                          }
                        }, 0, multiboxes.length, 1.5, 0, true);
                        this.scheduleOnce(() => {
                          if (round.rate > 200) ;else {
                            console.log('小于200：有加倍倍数显示4', round.rate);
                            director.emit('betUserInfoUpdateWinAnima', round.rate - this.tatalWin);
                          }
                          director.emit('byWayWinLightEnd');
                        }, multiboxes.length * 1.5);
                      };

                      if (multiValue > 0) {
                        if (this.multiValue > 0) {
                          sAudioMgr.PlayShotAudio('fs_Multifly');
                          this.playtotalMultMp3();
                          let str = this.multiLabel.string.slice(0, this.multiLabel.string.length);
                          director.emit('winMultiBox', {
                            actionType: 'open',
                            pos: this.multiLabel.node.worldPosition,
                            multi: parseInt(str),
                            fontSize: 25
                          });
                        }

                        this.scheduleOnce(mulCall, this.multiValue == 0 ? 0 : 1.5);
                      } else {
                        mulCall();
                      }
                    } else {
                      director.emit('byWayWinLightEnd');
                    }

                    if (!isFirst) {
                      this.multiValue = multiValue;
                    }
                  } else {
                    //if (eventArr.length > 1) {
                    director.emit('byWayWinLightEnd'); //};
                  }
                }
              }
            };

            boxAnimaCall();
            director.on('dropAllBoxFinish', () => {
              // console.log('dropAllBoxFinish');
              boxAnimaCall(); // console.log('isEnd:',isEnd);
            }, this);
          }
        }

        freeWinTotleViewOpen(winTotalCoin, winRealTotalCoin) {
          director.emit('StarlightTotleViewOpen', winRealTotalCoin, this.freeWinCount); // if(this.totalWinPrefab){
          //     const obj = instantiate(this.totalWinPrefab);
          //     obj.setParent(this.node.parent);
          //     obj.getComponent(UIBase).DataInit({coin:winTotalCoin,count : this.freeWinCount});
          //     director.emit('freeWinEndRes',winTotalCoin,winRealTotalCoin);
          // }else{
          //     director.emit('freeWinOver');
          //     director.emit('rollStop');
          // }
        }

        playscatterMp() {
          let num = sUtil.RandomInt(0, 11);

          if (num >= 0 && num < 9) {
            sAudioMgr.PlayShotAudio('scatter_appear_voice' + num);
          }
        }

        playmultMp() {
          let num = sUtil.RandomInt(1, 11);

          if (num >= 0 && num < 10) {
            sAudioMgr.PlayShotAudio('mult_appear_voice' + num);
          }
        }

        playtotalMultMp3() {
          let num = sUtil.RandomInt(1, 8);

          if (num >= 0 && num < 9) {
            sAudioMgr.PlayShotAudio('total multiplier_voice' + num);
          }
        }

      }, _temp), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "effectLayer", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "multiLabel", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "leftFreeCountLabel", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/149", ['./StarlightPrincess_Thunder.ts', './StarlightPrincess_Entity.ts', './soltGameCheckLine_149.ts', './StarlightPrincess_WinTipLabel.ts', './StarlightPrincess_SceneAdater.ts', './StarlightPrincess_GameEntity.ts', './StarlightPrincess_sSlotHorGameEntityAdapter.ts', './StarlightPrincess_OverEffect.ts', './StarlightPrincess_Board.ts', './StarlightPrincess_BigWin.ts', './StarlightPrincess_UIFreewinBingoFree.ts', './StarlightPrincess_History.ts', './StarlightPrincess_FreeWinBuyCtr.ts', './StarlightPrincess_UIFreeWinBuy.ts', './StarlightPrincess_weightNode.ts', './StarlightPrincess_Huaxianzi.ts', './StarlightPrincess_NormalFlow.ts', './StarlightPrincess_TotalWin.ts', './StarlightPrincess_UIFreeSpinEntry.ts', './StarlightPrincess_BoxItem.ts', './StarlightPrincess_FreewinFlow.ts'], function () {
  'use strict';

  return {
    setters: [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
    execute: function () {}
  };
});

(function(r) {
  r('virtual:///prerequisite-imports/149', 'chunks:///_virtual/149'); 
})(function(mid, cid) {
    System.register(mid, [cid], function (_export, _context) {
    return {
        setters: [function(_m) {
            var _exportObj = {};

            for (var _key in _m) {
              if (_key !== "default" && _key !== "__esModule") _exportObj[_key] = _m[_key];
            }
      
            _export(_exportObj);
        }],
        execute: function () { }
    };
    });
});